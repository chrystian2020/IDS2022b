﻿Imports System.Resources

Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("LivExpress")> 
<Assembly: AssemblyDescription("LivExpress")> 
<Assembly: AssemblyCompany("LivExpress")> 
<Assembly: AssemblyProduct("LivExpress")> 
<Assembly: AssemblyCopyright("Copyright LivExpress 2018")> 
<Assembly: AssemblyTrademark("Merchant Information System  ©POSWEST 2016-2017")> 

<Assembly: ComVisible(True)> 

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("d77aa46f-d87a-4ce4-8091-784b6a2b6ce2")> 

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")>

<Assembly: AssemblyVersion("18.0.0.0")> 
<Assembly: AssemblyFileVersion("18.0.0.0")> 

<Assembly: NeutralResourcesLanguageAttribute("en-CA")> 