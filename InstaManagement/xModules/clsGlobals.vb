﻿Imports System.Data.SqlClient
Imports System.Data.Odbc


Public Class clsGlobals

    Private m_ConnectionString As String
    Private m_Skin As String
    Private m_OutGoingServer As String
    Private m_Port As Nullable(Of Int32)

    Private m_EmailUsername As String
    Private m_EmailPassword As String
    Private m_UseSSL As Boolean
    Private m_DatabaseID As Nullable(Of Int32)
    Private m_DatabaseName As String
    Private m_Version As String
    Private m_HaveInternetConnection As Boolean
    Private m_EmailErreur As String
    Private m_Debit As Nullable(Of Double)
    Private m_Credit As Nullable(Of Double)
    Private m_Glacier As Nullable(Of Int32)
    Private m_NombreGlacier As Nullable(Of Int32)
    Private m_LocationFacture As String
    Private m_LocationPaie As String
    Private m_PeriodeDateFrom As DateTime
    Private m_PeriodeDateTo As DateTime
    Private m_TPS As Nullable(Of Decimal)
    Private m_TVQ As Nullable(Of Decimal)
    Private m_TPSNumero As String
    Private m_TVQNumero As String
    Private m_CompanyName As String
    Private m_CompanyAdresse As String
    Private m_CompanyContact As String
    Private m_CompanyContactTelephone As String
    Private m_Server As String
    Private m_Username As String
    Private m_Password As String
    Private m_TotalEchange35 As Nullable(Of Int32)
    Private m_TotalEchange305 As Nullable(Of Int32)
    Private m_IDPeriode As Nullable(Of Int32)
    Private m_IDSelectedFacture As Integer
    Private m_IDSelectedPaye As Integer
    Private m_LocationConsolidation As String
    Private m_LocationAchats As String
    Private m_LocationVentes As String
    Private m_Year As Nullable(Of Int32)
    Private m_LocationCommissions As String
    Private m_LocationRapportLivreur As String
    Private m_LocationRapportRestaurant As String
    Private m_IDTypePeriode As Nullable(Of Int32)
    Private m_iNombreFree30535 As Nullable(Of Int32)
    Private m_TotalEchange As Nullable(Of Int32)
    Private m_LocationRapportDeTaxation As String
    Private m_LocationRapportDeTaxationIDS As String
    Private m_RapportTaxationLivreurPaye As String


    Public Property RapportTaxationLivreurPaye() As String
        Get
            Return m_RapportTaxationLivreurPaye
        End Get
        Set(ByVal value As String)
            m_RapportTaxationLivreurPaye = value
        End Set
    End Property

    Public Property LocationRapportDeTaxationIDS() As String
        Get
            Return m_LocationRapportDeTaxationIDS
        End Get
        Set(ByVal value As String)
            m_LocationRapportDeTaxationIDS = value
        End Set
    End Property

    Public Property LocationRapportDeTaxation() As String
        Get
            Return m_LocationRapportDeTaxation
        End Get
        Set(ByVal value As String)
            m_LocationRapportDeTaxation = value
        End Set
    End Property
    Public Property LocationCommissions() As String
        Get
            Return m_LocationCommissions
        End Get
        Set(ByVal value As String)
            m_LocationCommissions = value
        End Set
    End Property
    Public Property TotalEchange() As Nullable(Of Int32)
        Get
            Return m_TotalEchange
        End Get
        Set(ByVal value As Nullable(Of Int32))
            m_TotalEchange = value
        End Set
    End Property

    Public Property iNombreFree30535() As Nullable(Of Int32)
        Get
            Return m_iNombreFree30535
        End Get
        Set(ByVal value As Nullable(Of Int32))
            m_iNombreFree30535 = value
        End Set
    End Property
    Public Property Year() As Nullable(Of Int32)
        Get
            Return m_Year
        End Get
        Set(ByVal value As Nullable(Of Int32))
            m_Year = value
        End Set
    End Property



    Public Property LocationAchats() As String
        Get
            Return m_LocationAchats
        End Get
        Set(ByVal value As String)
            m_LocationAchats = value
        End Set
    End Property

    Public Property LocationVentes() As String
        Get
            Return m_LocationVentes
        End Get
        Set(ByVal value As String)
            m_LocationVentes = value
        End Set
    End Property

    Public Property LocationConsolidation() As String
        Get
            Return m_LocationConsolidation
        End Get
        Set(ByVal value As String)
            m_LocationConsolidation = value
        End Set
    End Property


    Public Property IDSelectedFacture() As Int32
        Get
            Return m_IDSelectedFacture
        End Get
        Set(ByVal value As Int32)
            m_IDSelectedFacture = value
        End Set
    End Property

    Public Property IDSelectedPaye() As Int32
        Get
            Return m_IDSelectedPaye
        End Get
        Set(ByVal value As Int32)
            m_IDSelectedPaye = value
        End Set
    End Property
    Public Property IDPeriode() As Nullable(Of Int32)
        Get
            Return m_IDPeriode
        End Get
        Set(ByVal value As Nullable(Of Int32))
            m_IDPeriode = value
        End Set
    End Property

    Public Property TotalEchange35() As Nullable(Of Int32)
        Get
            Return m_TotalEchange35
        End Get
        Set(ByVal value As Nullable(Of Int32))
            m_TotalEchange35 = value
        End Set
    End Property

    Public Property TotalEchange305() As Nullable(Of Int32)
        Get
            Return m_TotalEchange305
        End Get
        Set(ByVal value As Nullable(Of Int32))
            m_TotalEchange305 = value
        End Set
    End Property



    Public Property TPS() As Nullable(Of Decimal)
        Get
            Return m_TPS
        End Get
        Set(ByVal value As Nullable(Of Decimal))
            m_TPS = value
        End Set
    End Property

    Public Property TVQ() As Nullable(Of Decimal)
        Get
            Return m_TVQ
        End Get
        Set(ByVal value As Nullable(Of Decimal))
            m_TVQ = value
        End Set
    End Property

    Public Property TPSNumero() As String
        Get
            Return m_TPSNumero
        End Get
        Set(ByVal value As String)
            m_TPSNumero = value
        End Set
    End Property

    Public Property TVQNumero() As String
        Get
            Return m_TVQNumero
        End Get
        Set(ByVal value As String)
            m_TVQNumero = value
        End Set
    End Property

    Public Property CompanyName() As String
        Get
            Return m_CompanyName
        End Get
        Set(ByVal value As String)
            m_CompanyName = value
        End Set
    End Property

    Public Property CompanyAdresse() As String
        Get
            Return m_CompanyAdresse
        End Get
        Set(ByVal value As String)
            m_CompanyAdresse = value
        End Set
    End Property

    Public Property CompanyContact() As String
        Get
            Return m_CompanyContact
        End Get
        Set(ByVal value As String)
            m_CompanyContact = value
        End Set
    End Property

    Public Property CompanyContactTelephone() As String
        Get
            Return m_CompanyContactTelephone
        End Get
        Set(ByVal value As String)
            m_CompanyContactTelephone = value
        End Set
    End Property


    Public Property Server() As String
        Get
            Return m_Server
        End Get
        Set(ByVal value As String)
            m_Server = value
        End Set
    End Property

    Public Property Username() As String
        Get
            Return m_Username
        End Get
        Set(ByVal value As String)
            m_Username = value
        End Set
    End Property

    Public Property Password() As String
        Get
            Return m_Password
        End Get
        Set(ByVal value As String)
            m_Password = value
        End Set
    End Property
    Public Property Port() As Nullable(Of Int32)
        Get
            Return m_Port
        End Get
        Set(ByVal value As Nullable(Of Int32))
            m_Port = value
        End Set
    End Property

    Public Property PeriodeDateFrom() As DateTime
        Get
            Return m_PeriodeDateFrom
        End Get
        Set(ByVal value As DateTime)
            m_PeriodeDateFrom = value
        End Set
    End Property


    Public Property PeriodeDateTo() As DateTime
        Get
            Return m_PeriodeDateTo
        End Get
        Set(ByVal value As DateTime)
            m_PeriodeDateTo = value
        End Set
    End Property


    Public Property LocationFacture() As String
        Get
            Return m_LocationFacture
        End Get
        Set(ByVal value As String)
            m_LocationFacture = value
        End Set
    End Property

    Public Property LocationPaie() As String
        Get
            Return m_LocationPaie
        End Get
        Set(ByVal value As String)
            m_LocationPaie = value
        End Set
    End Property

    Public Property Glacier() As Nullable(Of Int32)
        Get
            Return m_Glacier
        End Get
        Set(ByVal value As Nullable(Of Int32))
            m_Glacier = value
        End Set
    End Property

    Public Property NombreGlacier() As Nullable(Of Int32)
        Get
            Return m_NombreGlacier
        End Get
        Set(ByVal value As Nullable(Of Int32))
            m_NombreGlacier = value
        End Set
    End Property
    Public Property Debit() As Nullable(Of Double)
        Get
            Return m_Debit
        End Get
        Set(ByVal value As Nullable(Of Double))
            m_Debit = value
        End Set
    End Property
    Public Property Credit() As Nullable(Of Double)
        Get
            Return m_Credit
        End Get
        Set(ByVal value As Nullable(Of Double))
            m_Credit = value
        End Set
    End Property

    Public Property HaveInternetConnection() As Boolean
        Get
            Return m_HaveInternetConnection
        End Get
        Set(ByVal value As Boolean)
            m_HaveInternetConnection = value
        End Set
    End Property
    Public Property DatabaseID() As Nullable(Of Int32)
        Get
            Return m_DatabaseID
        End Get
        Set(ByVal value As Nullable(Of Int32))
            m_DatabaseID = value
        End Set
    End Property

    Public Property DatabaseName() As String
        Get
            Return m_DatabaseName
        End Get
        Set(ByVal value As String)
            m_DatabaseName = value
        End Set
    End Property
    Public Property Version() As String
        Get
            Return m_Version
        End Get
        Set(ByVal value As String)
            m_Version = value
        End Set
    End Property
    Public Property UseSSL() As Boolean
        Get
            Return m_UseSSL
        End Get
        Set(ByVal value As Boolean)
            m_UseSSL = value
        End Set
    End Property

    Public Property OutGoingServer() As String
        Get
            Return m_OutGoingServer
        End Get
        Set(ByVal value As String)
            m_OutGoingServer = value
        End Set
    End Property

    Public Property EmailUsername() As String
        Get
            Return m_EmailUsername
        End Get
        Set(ByVal value As String)
            m_EmailUsername = value
        End Set
    End Property

    Public Property EmailPassword() As String
        Get
            Return m_EmailPassword
        End Get
        Set(ByVal value As String)
            m_EmailPassword = value
        End Set
    End Property
    Public Property EmailErreur() As String
        Get
            Return m_EmailErreur
        End Get
        Set(ByVal value As String)
            m_EmailErreur = value
        End Set
    End Property


    Public Property LocationRapportLivreur() As String
        Get
            Return m_LocationRapportLivreur
        End Get
        Set(ByVal value As String)
            m_LocationRapportLivreur = value
        End Set
    End Property


    Public Property LocationRapportRestaurant() As String
        Get
            Return m_LocationRapportRestaurant
        End Get
        Set(ByVal value As String)
            m_LocationRapportRestaurant = value
        End Set
    End Property

    Public Property IDTypePeriode() As Nullable(Of Int32)
        Get
            Return m_IDTypePeriode
        End Get
        Set(ByVal value As Nullable(Of Int32))
            m_IDTypePeriode = value
        End Set
    End Property

    Public Sub New()

    End Sub



    Public Function GetConnection() As OdbcConnection
        Dim connectionString As String = go_Globals.ConnectionString

        Debug.Print(go_Globals.ConnectionString)
        Return New OdbcConnection(connectionString)
    End Function


    Public Property Skin() As String
        Get
            Return m_Skin
        End Get
        Set(ByVal value As String)
            m_Skin = value
        End Set
    End Property




    Public Property ConnectionString() As String
        Get
            Return m_ConnectionString
        End Get
        Set(ByVal value As String)
            m_ConnectionString = value
        End Set
    End Property




End Class
