﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports System

Imports System.Data.OleDb
Imports System.Diagnostics
Imports System.Threading
Imports Microsoft.VisualBasic
Imports System.Text.RegularExpressions
Imports System.Globalization





Module SysTemGlobals


    Private cn As Object
    Public gTPS As Double
    Public gTVQ As Double
    Public go_Globals As New clsGlobals
    Public Sub ExecuteStoredProcedure(ByVal vStoredProc As String, ByVal vParameter As String, ByVal vParamValue As Integer)
        OpenCnConn()

        Dim cmd As Odbc.OdbcCommand = New Odbc.OdbcCommand(vStoredProc, cn)
        cmd.Connection = cn
        cmd.CommandType = CommandType.StoredProcedure
        If vParameter.ToString.Trim <> "" Then
            cmd.Parameters.Add(vParameter, Odbc.OdbcType.Int).Value = vParamValue
        End If

        cmd.ExecuteNonQuery()





    End Sub

    Public Sub OpenCnConn()
        cn = New SqlConnection(cnString)
        cn.Open()
    End Sub

    Public Sub CloseCnConn()
        cn.Close()
    End Sub
    Public Sub FillListboxStoredProcedure(ByVal vListbox As System.Windows.Forms.ListBox, ByVal vStoredProc As String, ByVal vDisplayVALUE As String, ByVal vDisplayMember As String, ByVal vParameter As String, ByVal vParamValue As Integer)

        Try

            Dim cmd As SqlCommand = New SqlCommand(vStoredProc, cn)
            cmd.CommandType = CommandType.StoredProcedure
            If vParamValue > 0 Then
                cmd.Parameters.Add(vParameter, SqlDbType.Int).Value = vParamValue
            End If
            Dim da As SqlDataAdapter = New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds, "MyTable")
            vListbox.DataSource = ds.Tables(0)
            vListbox.DisplayMember = vDisplayMember
            vListbox.ValueMember = vDisplayVALUE
            vListbox.SelectedIndex = -1

        Catch ex As Exception
            MessageBox.Show(ex.Message, "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        Finally

        End Try

    End Sub
    Public Sub FillGridStoredProcedure(ByVal vStoredProc As String, ByVal ogrdGrid As Object, ByVal oGridView As Object, ByVal vParameter As String, ByVal vParamValue As Integer)


        Try


            Dim cmd As SqlCommand = New SqlCommand(vStoredProc, cn)
            cmd.CommandType = CommandType.StoredProcedure
            If vParamValue > 0 Then
                cmd.Parameters.Add(vParameter, SqlDbType.Int).Value = vParamValue
            End If



            Dim da As SqlDataAdapter = New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds, "MyTable")
            ogrdGrid.DataSource = ds.Tables(0)
            oGridView.BestFitColumns()




        Catch ex As Exception
            MessageBox.Show(ex.Message, "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        Finally

        End Try



    End Sub

    Public Sub FillComboBoxStoredProcedureDevExpress(ByVal vComboBox As Object, ByVal vStoredProc As String, ByVal vDisplayVALUE As String, ByVal vDisplayMember As String, ByVal vParameter As String, ByVal vParamValue As Integer)

        Try
            OpenCnConn()
            Dim cmd As SqlCommand = New SqlCommand(vStoredProc, cn)
            cmd.CommandType = CommandType.StoredProcedure
            If vParamValue > 0 Then
                cmd.Parameters.Add(vParameter, SqlDbType.Int).Value = vParamValue
            End If
            Dim da As SqlDataAdapter = New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds, "MyTable")
            vComboBox.DataSource = ds.Tables(0)
            vComboBox.DisplayMember = vDisplayMember
            vComboBox.ValueMember = vDisplayVALUE
            vComboBox.SelectedIndex = -1

        Catch ex As Exception
            MessageBox.Show(ex.Message, "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        Finally

        End Try

    End Sub

    Public Sub FillComboBoxStoredProcedure(ByVal vComboBox As ComboBox, ByVal vStoredProc As String, ByVal vDisplayVALUE As String, ByVal vDisplayMember As String, ByVal vParameter As String, ByVal vParamValue As Integer)

        Try
            OpenCnConn()
            Dim cmd As SqlCommand = New SqlCommand(vStoredProc, cn)
            cmd.CommandType = CommandType.StoredProcedure
            If vParamValue > 0 Then
                cmd.Parameters.Add(vParameter, SqlDbType.Int).Value = vParamValue
            End If
            Dim da As SqlDataAdapter = New SqlDataAdapter(cmd)
            Dim ds As New DataSet
            da.Fill(ds, "MyTable")
            vComboBox.DataSource = ds.Tables(0)
            vComboBox.DisplayMember = vDisplayMember
            vComboBox.ValueMember = vDisplayVALUE
            vComboBox.SelectedIndex = -1

        Catch ex As Exception
            MessageBox.Show(ex.Message, "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        Finally

        End Try

    End Sub

    Public Function GetConnection() As SqlConnection
        Dim connectionString As String _
            = cnString
        Return New SqlConnection(connectionString)
    End Function

    Public Function GetConnectionString() As String
        Dim connectionString As String _
            = cnString
        Return connectionString
    End Function
    Public Function App_Path() As String
        Return System.AppDomain.CurrentDomain.BaseDirectory()
    End Function
    Public Function SingleQuote(ByVal vSTring As String) As String

        Return Replace(vSTring, "'", "'")
    End Function

    Public Function DoubleQuote(ByVal vSTring As String) As String

        Return Replace(vSTring, "'", "''")
    End Function
    Function Chr(ByVal CharCode As Integer) As Char
        Return Convert.ToChar(CharCode)
    End Function


    Public Sub Load_Combo(ByVal cmb_box As ComboBox, ByVal SQL_String As String, ByVal Id_Field As String, ByVal DisplayValue As String)

        Dim oAdapter As New SqlClient.SqlDataAdapter
        Dim oData_Set As New DataSet
        Try

            oAdapter = New SqlClient.SqlDataAdapter(SQL_String, GetConnection)
            oAdapter.Fill(oData_Set, "Table")
            With cmb_box
                .DataSource = oData_Set.Tables("Table")
                .DisplayMember = DisplayValue
                .ValueMember = Id_Field
                .SelectedIndex = -1
            End With

        Catch oExcept As Exception
            MessageBox.Show(oExcept.Message)
        End Try
    End Sub

    Public Sub Load_List(ByVal List_Box As System.Windows.Forms.ListBox, ByVal SQL_String As String, ByVal Id_Field As String, ByVal DisplayValue As String)

        Dim oAdapter As New SqlClient.SqlDataAdapter
        Dim oData_Set As New DataSet
        Try

            oAdapter = New SqlClient.SqlDataAdapter(SQL_String, GetConnection)
            oAdapter.Fill(oData_Set, "Table")
            With List_Box
                .DataSource = oData_Set.Tables("Table")
                .DisplayMember = DisplayValue
                .ValueMember = Id_Field
            End With
            List_Box.SelectedIndex = -1

        Catch oExcept As Exception
            MessageBox.Show(oExcept.Message)
        End Try
    End Sub
    Public Function ReplaceQuotes(ByVal stringToCleanUp As String, ByVal characterToRemove As String) As String
        ' replace the target with nothing
        ' Replace() returns a new String and does not modify the current one
        Return stringToCleanUp.Replace("'", "''")
    End Function
    Public Sub ExecuteSQL(ByVal vsql As String)
        Dim myConnection As New SqlConnection
        Dim myCommand As SqlCommand
        Dim ra As Integer

        Try


            myConnection = New SqlConnection(cnString)

            myConnection.Open()
            myCommand = New SqlCommand(vsql, myConnection)


            ra = myCommand.ExecuteNonQuery()
            myConnection.Close()

        Catch oExcept As Exception
            MessageBox.Show(oExcept.Message)
        End Try

    End Sub
    Public Sub UpdateSQL(ByVal vsql As String)

        Dim myConnection As New SqlConnection
        Dim myCommand As SqlCommand
        Dim ra As Integer


        Try
            myConnection = New SqlConnection(cnString)

            myConnection.Open()
            myCommand = New SqlCommand(vsql, myConnection)
            ra = myCommand.ExecuteNonQuery()

            myConnection.Close()

        Catch ex As Exception
            MessageBox.Show(ex.Message, "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)

        End Try



    End Sub
    Public Sub FillComboBox(ByVal vComboBox As ComboBox, ByVal vSQL As String, ByVal vDisplayVALUE As String, ByVal vDisplayMember As String)

        Dim con As New SqlConnection(cnString)
        Try
            con.Open()
            Dim dat As SqlDataAdapter = New SqlDataAdapter(vSQL, GetConnection)
            Dim dt As New DataSet
            dat.Fill(dt, "table")
            vComboBox.DataSource = dt.Tables("table").DefaultView
            vComboBox.DisplayMember = vDisplayMember
            vComboBox.ValueMember = vDisplayVALUE
            vComboBox.SelectedIndex = -1
        Catch ex As Exception
            MessageBox.Show(ex.Message, "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        Finally
            con.Close()
        End Try



    End Sub
    Public Sub FillList(ByVal vComboBox As ListBox, ByVal vSQL As String, ByVal vDisplayMember As String, ByVal vDisplayVALUE As String)

        Dim con As New SqlConnection(cnString)
        Try
            con.Open()
            Dim dat As SqlDataAdapter = New SqlDataAdapter(vSQL, con)
            Dim dt As New DataSet
            dat.Fill(dt, "table")
            vComboBox.DataSource = dt.Tables("table").DefaultView
            vComboBox.DisplayMember = vDisplayMember
            vComboBox.ValueMember = vDisplayVALUE
            vComboBox.SelectedIndex = -1
        Catch ex As Exception
            MessageBox.Show(ex.Message, "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        Finally
            con.Close()
        End Try




    End Sub

    Sub KillAllExcels()
        Dim proc As System.Diagnostics.Process
        For Each proc In System.Diagnostics.Process.GetProcessesByName("EXCEL")
            proc.Kill()
        Next
    End Sub
    Private Sub wait(ByVal interval As Integer)
        Dim sw As New Stopwatch
        sw.Start()
        Do While sw.ElapsedMilliseconds < interval
            ' Allows UI to remain responsive
            Application.DoEvents()
        Loop
        sw.Stop()
    End Sub
    Public Function IsConnectionAvailable() As Boolean
        'Call url
        Dim url As New System.Uri("http://www.google.com/")
        'Request for request
        Dim req As System.Net.WebRequest
        req = System.Net.WebRequest.Create(url)
        Dim resp As System.Net.WebResponse
        Try
            resp = req.GetResponse()
            resp.Close()
            req = Nothing
            Return True
        Catch ex As Exception
            req = Nothing
            Return False
        End Try
    End Function
End Module
