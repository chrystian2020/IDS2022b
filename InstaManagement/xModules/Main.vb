﻿
Imports System.Globalization
Imports System.Threading
Module Main

    ' ...

    Public Sub Main()
        ' Create a new object, representing the German culture. 
        Dim Culture = CultureInfo.CreateSpecificCulture("en-US")

        ' The following line provides localization for the application's user interface.
        Thread.CurrentThread.CurrentUICulture = Culture

        ' The following line provides localization for data formats.
        Thread.CurrentThread.CurrentCulture = Culture

        ' Set this culture as the default culture for all threads in this application. 
        ' Note: The following properties are supported in the .NET Framework 4.5+
        CultureInfo.DefaultThreadCurrentCulture = Culture
        CultureInfo.DefaultThreadCurrentUICulture = Culture


        ' Note that the above code should be added BEFORE calling the Run() method.
        Application.Run(New frmMain())
    End Sub


End Module
