﻿Module ModCon

    Public openedFileStream As System.IO.Stream
    Public cnString As String
    Public dataBytes() As Byte
    Public tmpStr As String
    Public Main()





    Public Function checkServer() As Boolean
        Try

            With frmServerSetting
                .OpenFileDialog1.FileName = Application.StartupPath & "\Config.ini"
                openedFileStream = .OpenFileDialog1.OpenFile()
            End With

            ReDim dataBytes(openedFileStream.Length - 1) 'Init 
            openedFileStream.Read(dataBytes, 0, openedFileStream.Length)
            openedFileStream.Close()
            tmpStr = System.Text.Encoding.Unicode.GetString(dataBytes)

            If tmpStr.ToString.Trim = "" Then

                Return False


            Else

                frmServerSetting.Close()
                cnString = tmpStr


                Return True

            End If


            Exit Function

        Catch ex As Exception
            MessageBox.Show(ex.Message, "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            checkServer = False
        End Try
    End Function


End Module
