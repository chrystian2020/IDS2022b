Public Class Invoice

    Private m_IDInvoice As Int32
    Private m_InvoiceDate As Nullable(Of DateTime)
    Private m_InvoiceNumber As String
    Private m_OrganisationID As Nullable(Of Int32)
    Private m_Organisation As String
    Private m_DateFrom As Nullable(Of DateTime)
    Private m_DateTo As Nullable(Of DateTime)
    Private m_IDPeriode As Nullable(Of Int32)
    Private m_Amount As Nullable(Of Decimal)
    Private m_TPS As Nullable(Of Decimal)
    Private m_TVQ As Nullable(Of Decimal)
    Private m_Paye As Nullable(Of Boolean)
    Private m_Total As Nullable(Of Decimal)


    Public Sub New()

    End Sub

    Public Property IDInvoice() As Int32
        Get
            Return m_IDInvoice
        End Get
        Set(ByVal value As Int32)
            m_IDInvoice = value 
        End Set
    End Property

    Public Property InvoiceDate() As Nullable(Of DateTime)
        Get
            Return m_InvoiceDate
        End Get
        Set(ByVal value As Nullable(Of DateTime))
            m_InvoiceDate = value 
        End Set
    End Property

    Public Property InvoiceNumber() As String
        Get
            Return m_InvoiceNumber
        End Get
        Set(ByVal value As String)
            m_InvoiceNumber = value 
        End Set
    End Property

    Public Property OrganisationID() As Nullable(Of Int32)
        Get
            Return m_OrganisationID
        End Get
        Set(ByVal value As Nullable(Of Int32))
            m_OrganisationID = value 
        End Set
    End Property

    Public Property Organisation() As String
        Get
            Return m_Organisation
        End Get
        Set(ByVal value As String)
            m_Organisation = value 
        End Set
    End Property

    Public Property DateFrom() As Nullable(Of DateTime)
        Get
            Return m_DateFrom
        End Get
        Set(ByVal value As Nullable(Of DateTime))
            m_DateFrom = value 
        End Set
    End Property

    Public Property DateTo() As Nullable(Of DateTime)
        Get
            Return m_DateTo
        End Get
        Set(ByVal value As Nullable(Of DateTime))
            m_DateTo = value 
        End Set
    End Property

    Public Property IDPeriode() As Nullable(Of Int32)
        Get
            Return m_IDPeriode
        End Get
        Set(ByVal value As Nullable(Of Int32))
            m_IDPeriode = value 
        End Set
    End Property

    Public Property Amount() As Nullable(Of Decimal)
        Get
            Return m_Amount
        End Get
        Set(ByVal value As Nullable(Of Decimal))
            m_Amount = value 
        End Set
    End Property

    Public Property TPS() As Nullable(Of Decimal)
        Get
            Return m_TPS
        End Get
        Set(ByVal value As Nullable(Of Decimal))
            m_TPS = value 
        End Set
    End Property

    Public Property TVQ() As Nullable(Of Decimal)
        Get
            Return m_TVQ
        End Get
        Set(ByVal value As Nullable(Of Decimal))
            m_TVQ = value 
        End Set
    End Property

    Public Property Paye() As Nullable(Of Boolean)
        Get
            Return m_Paye
        End Get
        Set(ByVal value As Nullable(Of Boolean))
            m_Paye = value 
        End Set
    End Property

    Public Property Total() As Nullable(Of Decimal)
        Get
            Return m_Total
        End Get
        Set(ByVal value As Nullable(Of Decimal))
            m_Total = value 
        End Set
    End Property



End Class
 
