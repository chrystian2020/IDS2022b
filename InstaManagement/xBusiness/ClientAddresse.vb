Public Class ClientAddresse

    Private m_IDClientAddress As Int32
    Private m_IDClient As Nullable(Of Int32)
    Private m_Nom As String
    Private m_AdresseNo As String
    Private m_Adresse As String
    Private m_Appartement As String
    Private m_CodeEntree As String
    Private m_CodePostale As String
    Private m_VilleID As Nullable(Of Int32)
    Private m_Contact As String
    Private m_Telephone As String
    Private m_Mobile As String
    Private m_Courriel As String
    Private m_Titre As String
    Private m_ClientKM As String

    Public Sub New()

    End Sub

    Public Property IDClientAddress() As Int32
        Get
            Return m_IDClientAddress
        End Get
        Set(ByVal value As Int32)
            m_IDClientAddress = value 
        End Set
    End Property

    Public Property IDClient() As Nullable(Of Int32)
        Get
            Return m_IDClient
        End Get
        Set(ByVal value As Nullable(Of Int32))
            m_IDClient = value 
        End Set
    End Property

    Public Property Nom() As String
        Get
            Return m_Nom
        End Get
        Set(ByVal value As String)
            m_Nom = value 
        End Set
    End Property

    Public Property AdresseNo() As String
        Get
            Return m_AdresseNo
        End Get
        Set(ByVal value As String)
            m_AdresseNo = value 
        End Set
    End Property

    Public Property Adresse() As String
        Get
            Return m_Adresse
        End Get
        Set(ByVal value As String)
            m_Adresse = value 
        End Set
    End Property

    Public Property Appartement() As String
        Get
            Return m_Appartement
        End Get
        Set(ByVal value As String)
            m_Appartement = value 
        End Set
    End Property

    Public Property CodeEntree() As String
        Get
            Return m_CodeEntree
        End Get
        Set(ByVal value As String)
            m_CodeEntree = value 
        End Set
    End Property

    Public Property CodePostale() As String
        Get
            Return m_CodePostale
        End Get
        Set(ByVal value As String)
            m_CodePostale = value 
        End Set
    End Property

    Public Property VilleID() As Nullable(Of Int32)
        Get
            Return m_VilleID
        End Get
        Set(ByVal value As Nullable(Of Int32))
            m_VilleID = value 
        End Set
    End Property

    Public Property Contact() As String
        Get
            Return m_Contact
        End Get
        Set(ByVal value As String)
            m_Contact = value 
        End Set
    End Property

    Public Property Telephone() As String
        Get
            Return m_Telephone
        End Get
        Set(ByVal value As String)
            m_Telephone = value 
        End Set
    End Property

    Public Property Mobile() As String
        Get
            Return m_Mobile
        End Get
        Set(ByVal value As String)
            m_Mobile = value 
        End Set
    End Property

    Public Property Courriel() As String
        Get
            Return m_Courriel
        End Get
        Set(ByVal value As String)
            m_Courriel = value 
        End Set
    End Property

    Public Property Titre() As String
        Get
            Return m_Titre
        End Get
        Set(ByVal value As String)
            m_Titre = value 
        End Set
    End Property

    Public Property ClientKM() As String
        Get
            Return m_ClientKM
        End Get
        Set(ByVal value As String)
            m_ClientKM = value 
        End Set
    End Property

End Class
 
