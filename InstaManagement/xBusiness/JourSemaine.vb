Public Class JourSemaine

    Private m_IDJourSemaine As Int32
    Private m_Jour As String

    Public Sub New()

    End Sub

    Public Property IDJourSemaine() As Int32
        Get
            Return m_IDJourSemaine
        End Get
        Set(ByVal value As Int32)
            m_IDJourSemaine = value 
        End Set
    End Property

    Public Property Jour() As String
        Get
            Return m_Jour
        End Get
        Set(ByVal value As String)
            m_Jour = value 
        End Set
    End Property

End Class
 
