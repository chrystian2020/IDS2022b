Public Class NomFichierImport

    Private m_IDNomFichierImport As Int32
    Private m_NomFichierImport As String
    Private m_IDPeriode As Nullable(Of Int32)
    Private m_DateFrom As Nullable(Of DateTime)
    Private m_DateTo As Nullable(Of DateTime)
    Public Sub New()

    End Sub
    Public Property DateFrom() As Nullable(Of DateTime)
        Get
            Return m_DateFrom
        End Get
        Set(ByVal value As Nullable(Of DateTime))
            m_DateFrom = value
        End Set
    End Property

    Public Property DateTo() As Nullable(Of DateTime)
        Get
            Return m_DateTo
        End Get
        Set(ByVal value As Nullable(Of DateTime))
            m_DateTo = value
        End Set
    End Property
    Public Property IDNomFichierImport() As Int32
        Get
            Return m_IDNomFichierImport
        End Get
        Set(ByVal value As Int32)
            m_IDNomFichierImport = value 
        End Set
    End Property

    Public Property NomFichierImport() As String
        Get
            Return m_NomFichierImport
        End Get
        Set(ByVal value As String)
            m_NomFichierImport = value 
        End Set
    End Property

    Public Property IDPeriode() As Nullable(Of Int32)
        Get
            Return m_IDPeriode
        End Get
        Set(ByVal value As Nullable(Of Int32))
            m_IDPeriode = value 
        End Set
    End Property

End Class
 
