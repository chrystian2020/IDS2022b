Public Class Client

    Private m_IDClient As Int32
    Private m_Nom As String
    Private m_Contact As String
    Private m_Telephone As String
    Private m_Mobile As String
    Private m_Courriel As String

    Public Sub New()

    End Sub

    Public Property IDClient() As Int32
        Get
            Return m_IDClient
        End Get
        Set(ByVal value As Int32)
            m_IDClient = value 
        End Set
    End Property

    Public Property Nom() As String
        Get
            Return m_Nom
        End Get
        Set(ByVal value As String)
            m_Nom = value 
        End Set
    End Property

    Public Property Contact() As String
        Get
            Return m_Contact
        End Get
        Set(ByVal value As String)
            m_Contact = value 
        End Set
    End Property

    Public Property Telephone() As String
        Get
            Return m_Telephone
        End Get
        Set(ByVal value As String)
            m_Telephone = value 
        End Set
    End Property

    Public Property Mobile() As String
        Get
            Return m_Mobile
        End Get
        Set(ByVal value As String)
            m_Mobile = value 
        End Set
    End Property

    Public Property Courriel() As String
        Get
            Return m_Courriel
        End Get
        Set(ByVal value As String)
            m_Courriel = value 
        End Set
    End Property

End Class
 
