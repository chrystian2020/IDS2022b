Public Class Organisations

    Private m_OrganisationID As Int32
    Private m_Organisation As String
    Private m_Adresse As String
    Private m_Telephone As String
    Private m_KMMax As Nullable(Of Decimal)
    Private m_MontantBonus As Nullable(Of Decimal)
    Private m_MontantContrat As Nullable(Of Decimal)
    Private m_Livadd As Nullable(Of Decimal)
    Private m_MetodeCalculationOrganisation As Nullable(Of Int32)
    Private m_OrganisationImport As String
    Private m_MetodeCalculationLivreur As Nullable(Of Int32)
    Private m_QuantiteLivraison As Nullable(Of Int32)
    Private m_CreditParLivraison As Nullable(Of Decimal)
    Private m_PayeLivreur As Nullable(Of Decimal)
    Private m_MontantGlacierOrganisation As Nullable(Of Decimal)
    Private m_MontantGlacierLivreur As Nullable(Of Decimal)
    Private m_Email As String
    Private m_Email1 As String
    Private m_Email2 As String
    Private m_Email3 As String
    Private m_MontantContraHeure As Nullable(Of Decimal)
    Private m_MontantContraJour As Nullable(Of Decimal)
    Private m_MontantContraJourLivreur As Nullable(Of Decimal)
    Private m_Code As String
    Private m_MontantMobilus As Nullable(Of Decimal)
    Private m_NombreFree30535 As Nullable(Of Int32)
    Private m_CacheEmail1 As Nullable(Of Boolean)
    Private m_CacheEmail2 As Nullable(Of Boolean)
    Private m_CacheEmail3 As Nullable(Of Boolean)
    Private m_CacheEmail4 As Nullable(Of Boolean)
    Private m_IDVendeur As Nullable(Of Int32)
    Private m_Restaurant As Nullable(Of Boolean)
    Private m_CommissionVendeur As Nullable(Of Decimal)
    Private m_MontantFixe As Nullable(Of Decimal)
    Private m_MessageFacture As String
    Private m_MessageEtat As String
    Private m_IsCommission As Nullable(Of Boolean)
    Private m_isEtatDeCompte As Nullable(Of Boolean)
    Private m_MontantPrixFixeSem As Nullable(Of Decimal)
    Private m_MontantMaxLivraisoneExtra As Nullable(Of Decimal)
    Private m_MaximumLivraison As Nullable(Of Int32)
    Private m_Vendeur As String
    Private m_Addresse As String
    Private m_Courriel As String
    Private m_VendeurTelephone As String
    Private m_MontantParPorteOrganisation As Nullable(Of Decimal)
    Private m_MontantParPorteLivreur As Nullable(Of Decimal)



    Public Sub New()

    End Sub


    Public Property MontantParPorteOrganisation() As Nullable(Of Decimal)
        Get
            Return m_MontantParPorteOrganisation
        End Get
        Set(ByVal value As Nullable(Of Decimal))
            m_MontantParPorteOrganisation = value
        End Set
    End Property

    Public Property MontantParPorteLivreur() As Nullable(Of Decimal)
        Get
            Return m_MontantParPorteLivreur
        End Get
        Set(ByVal value As Nullable(Of Decimal))
            m_MontantParPorteLivreur = value
        End Set
    End Property


    Public Property Vendeur() As String
        Get
            Return m_Vendeur
        End Get
        Set(ByVal value As String)
            m_Vendeur = value
        End Set
    End Property

    Public Property Addresse() As String
        Get
            Return m_Addresse
        End Get
        Set(ByVal value As String)
            m_Addresse = value
        End Set
    End Property



    Public Property Courriel() As String
        Get
            Return m_Courriel
        End Get
        Set(ByVal value As String)
            m_Courriel = value
        End Set
    End Property
    Public Property VendeurTelephone() As String
        Get
            Return m_VendeurTelephone
        End Get
        Set(ByVal value As String)
            m_VendeurTelephone = value
        End Set
    End Property
    Public Property OrganisationID() As Int32
        Get
            Return m_OrganisationID
        End Get
        Set(ByVal value As Int32)
            m_OrganisationID = value 
        End Set
    End Property

    Public Property Organisation() As String
        Get
            Return m_Organisation
        End Get
        Set(ByVal value As String)
            m_Organisation = value 
        End Set
    End Property

    Public Property Adresse() As String
        Get
            Return m_Adresse
        End Get
        Set(ByVal value As String)
            m_Adresse = value 
        End Set
    End Property

    Public Property Telephone() As String
        Get
            Return m_Telephone
        End Get
        Set(ByVal value As String)
            m_Telephone = value 
        End Set
    End Property




    Public Property KMMax() As Nullable(Of Decimal)
        Get
            Return m_KMMax
        End Get
        Set(ByVal value As Nullable(Of Decimal))
            m_KMMax = value 
        End Set
    End Property

    Public Property MontantBonus() As Nullable(Of Decimal)
        Get
            Return m_MontantBonus
        End Get
        Set(ByVal value As Nullable(Of Decimal))
            m_MontantBonus = value 
        End Set
    End Property

    Public Property MontantContrat() As Nullable(Of Decimal)
        Get
            Return m_MontantContrat
        End Get
        Set(ByVal value As Nullable(Of Decimal))
            m_MontantContrat = value 
        End Set
    End Property

    Public Property Livadd() As Nullable(Of Decimal)
        Get
            Return m_Livadd
        End Get
        Set(ByVal value As Nullable(Of Decimal))
            m_Livadd = value 
        End Set
    End Property

    Public Property MetodeCalculationOrganisation() As Nullable(Of Int32)
        Get
            Return m_MetodeCalculationOrganisation
        End Get
        Set(ByVal value As Nullable(Of Int32))
            m_MetodeCalculationOrganisation = value 
        End Set
    End Property

    Public Property OrganisationImport() As String
        Get
            Return m_OrganisationImport
        End Get
        Set(ByVal value As String)
            m_OrganisationImport = value 
        End Set
    End Property

    Public Property MetodeCalculationLivreur() As Nullable(Of Int32)
        Get
            Return m_MetodeCalculationLivreur
        End Get
        Set(ByVal value As Nullable(Of Int32))
            m_MetodeCalculationLivreur = value 
        End Set
    End Property

    Public Property QuantiteLivraison() As Nullable(Of Int32)
        Get
            Return m_QuantiteLivraison
        End Get
        Set(ByVal value As Nullable(Of Int32))
            m_QuantiteLivraison = value 
        End Set
    End Property

    Public Property CreditParLivraison() As Nullable(Of Decimal)
        Get
            Return m_CreditParLivraison
        End Get
        Set(ByVal value As Nullable(Of Decimal))
            m_CreditParLivraison = value 
        End Set
    End Property

    Public Property PayeLivreur() As Nullable(Of Decimal)
        Get
            Return m_PayeLivreur
        End Get
        Set(ByVal value As Nullable(Of Decimal))
            m_PayeLivreur = value 
        End Set
    End Property

    Public Property MontantGlacierOrganisation() As Nullable(Of Decimal)
        Get
            Return m_MontantGlacierOrganisation
        End Get
        Set(ByVal value As Nullable(Of Decimal))
            m_MontantGlacierOrganisation = value 
        End Set
    End Property

    Public Property MontantGlacierLivreur() As Nullable(Of Decimal)
        Get
            Return m_MontantGlacierLivreur
        End Get
        Set(ByVal value As Nullable(Of Decimal))
            m_MontantGlacierLivreur = value 
        End Set
    End Property

    Public Property Email() As String
        Get
            Return m_Email
        End Get
        Set(ByVal value As String)
            m_Email = value 
        End Set
    End Property

    Public Property Email1() As String
        Get
            Return m_Email1
        End Get
        Set(ByVal value As String)
            m_Email1 = value 
        End Set
    End Property

    Public Property Email2() As String
        Get
            Return m_Email2
        End Get
        Set(ByVal value As String)
            m_Email2 = value 
        End Set
    End Property

    Public Property Email3() As String
        Get
            Return m_Email3
        End Get
        Set(ByVal value As String)
            m_Email3 = value 
        End Set
    End Property

    Public Property MontantContraHeure() As Nullable(Of Decimal)
        Get
            Return m_MontantContraHeure
        End Get
        Set(ByVal value As Nullable(Of Decimal))
            m_MontantContraHeure = value 
        End Set
    End Property

    Public Property MontantContraJour() As Nullable(Of Decimal)
        Get
            Return m_MontantContraJour
        End Get
        Set(ByVal value As Nullable(Of Decimal))
            m_MontantContraJour = value 
        End Set
    End Property

    Public Property MontantContraJourLivreur() As Nullable(Of Decimal)
        Get
            Return m_MontantContraJourLivreur
        End Get
        Set(ByVal value As Nullable(Of Decimal))
            m_MontantContraJourLivreur = value 
        End Set
    End Property

    Public Property Code() As String
        Get
            Return m_Code
        End Get
        Set(ByVal value As String)
            m_Code = value 
        End Set
    End Property

    Public Property MontantMobilus() As Nullable(Of Decimal)
        Get
            Return m_MontantMobilus
        End Get
        Set(ByVal value As Nullable(Of Decimal))
            m_MontantMobilus = value 
        End Set
    End Property

    Public Property NombreFree30535() As Nullable(Of Int32)
        Get
            Return m_NombreFree30535
        End Get
        Set(ByVal value As Nullable(Of Int32))
            m_NombreFree30535 = value 
        End Set
    End Property

    Public Property CacheEmail1() As Nullable(Of Boolean)
        Get
            Return m_CacheEmail1
        End Get
        Set(ByVal value As Nullable(Of Boolean))
            m_CacheEmail1 = value 
        End Set
    End Property

    Public Property CacheEmail2() As Nullable(Of Boolean)
        Get
            Return m_CacheEmail2
        End Get
        Set(ByVal value As Nullable(Of Boolean))
            m_CacheEmail2 = value 
        End Set
    End Property

    Public Property CacheEmail3() As Nullable(Of Boolean)
        Get
            Return m_CacheEmail3
        End Get
        Set(ByVal value As Nullable(Of Boolean))
            m_CacheEmail3 = value 
        End Set
    End Property

    Public Property CacheEmail4() As Nullable(Of Boolean)
        Get
            Return m_CacheEmail4
        End Get
        Set(ByVal value As Nullable(Of Boolean))
            m_CacheEmail4 = value 
        End Set
    End Property

    Public Property IDVendeur() As Nullable(Of Int32)
        Get
            Return m_IDVendeur
        End Get
        Set(ByVal value As Nullable(Of Int32))
            m_IDVendeur = value 
        End Set
    End Property

    Public Property Restaurant() As Nullable(Of Boolean)
        Get
            Return m_Restaurant
        End Get
        Set(ByVal value As Nullable(Of Boolean))
            m_Restaurant = value 
        End Set
    End Property

    Public Property CommissionVendeur() As Nullable(Of Decimal)
        Get
            Return m_CommissionVendeur
        End Get
        Set(ByVal value As Nullable(Of Decimal))
            m_CommissionVendeur = value 
        End Set
    End Property

    Public Property MontantFixe() As Nullable(Of Decimal)
        Get
            Return m_MontantFixe
        End Get
        Set(ByVal value As Nullable(Of Decimal))
            m_MontantFixe = value 
        End Set
    End Property

    Public Property MessageFacture() As String
        Get
            Return m_MessageFacture
        End Get
        Set(ByVal value As String)
            m_MessageFacture = value 
        End Set
    End Property

    Public Property MessageEtat() As String
        Get
            Return m_MessageEtat
        End Get
        Set(ByVal value As String)
            m_MessageEtat = value 
        End Set
    End Property

    Public Property IsCommission() As Nullable(Of Boolean)
        Get
            Return m_IsCommission
        End Get
        Set(ByVal value As Nullable(Of Boolean))
            m_IsCommission = value 
        End Set
    End Property

    Public Property isEtatDeCompte() As Nullable(Of Boolean)
        Get
            Return m_isEtatDeCompte
        End Get
        Set(ByVal value As Nullable(Of Boolean))
            m_isEtatDeCompte = value 
        End Set
    End Property

    Public Property MontantPrixFixeSem() As Nullable(Of Decimal)
        Get
            Return m_MontantPrixFixeSem
        End Get
        Set(ByVal value As Nullable(Of Decimal))
            m_MontantPrixFixeSem = value 
        End Set
    End Property

    Public Property MontantMaxLivraisoneExtra() As Nullable(Of Decimal)
        Get
            Return m_MontantMaxLivraisoneExtra
        End Get
        Set(ByVal value As Nullable(Of Decimal))
            m_MontantMaxLivraisoneExtra = value 
        End Set
    End Property

    Public Property MaximumLivraison() As Nullable(Of Int32)
        Get
            Return m_MaximumLivraison
        End Get
        Set(ByVal value As Nullable(Of Int32))
            m_MaximumLivraison = value 
        End Set
    End Property

End Class
 
