Public Class Paye

    Private m_IDPaye As Int32
    Private m_PayeNumber As String
    Private m_PayeDate As Nullable(Of DateTime)
    Private m_IDLivreur As Nullable(Of Int32)
    Private m_Livreur As String
    Private m_DateFrom As Nullable(Of DateTime)
    Private m_DateTo As Nullable(Of DateTime)
    Private m_IDPeriode As Nullable(Of Int32)
    Private m_Amount As Nullable(Of Decimal)
    Private m_TPS As Nullable(Of Decimal)
    Private m_TVQ As Nullable(Of Decimal)
    Private m_IsCommissionVendeur As Nullable(Of Boolean)

    Public Sub New()

    End Sub

    Public Property IDPaye() As Int32
        Get
            Return m_IDPaye
        End Get
        Set(ByVal value As Int32)
            m_IDPaye = value 
        End Set
    End Property

    Public Property PayeNumber() As String
        Get
            Return m_PayeNumber
        End Get
        Set(ByVal value As String)
            m_PayeNumber = value 
        End Set
    End Property

    Public Property PayeDate() As Nullable(Of DateTime)
        Get
            Return m_PayeDate
        End Get
        Set(ByVal value As Nullable(Of DateTime))
            m_PayeDate = value 
        End Set
    End Property

    Public Property IDLivreur() As Nullable(Of Int32)
        Get
            Return m_IDLivreur
        End Get
        Set(ByVal value As Nullable(Of Int32))
            m_IDLivreur = value 
        End Set
    End Property

    Public Property Livreur() As String
        Get
            Return m_Livreur
        End Get
        Set(ByVal value As String)
            m_Livreur = value 
        End Set
    End Property

    Public Property DateFrom() As Nullable(Of DateTime)
        Get
            Return m_DateFrom
        End Get
        Set(ByVal value As Nullable(Of DateTime))
            m_DateFrom = value 
        End Set
    End Property

    Public Property DateTo() As Nullable(Of DateTime)
        Get
            Return m_DateTo
        End Get
        Set(ByVal value As Nullable(Of DateTime))
            m_DateTo = value 
        End Set
    End Property

    Public Property IDPeriode() As Nullable(Of Int32)
        Get
            Return m_IDPeriode
        End Get
        Set(ByVal value As Nullable(Of Int32))
            m_IDPeriode = value 
        End Set
    End Property

    Public Property Amount() As Nullable(Of Decimal)
        Get
            Return m_Amount
        End Get
        Set(ByVal value As Nullable(Of Decimal))
            m_Amount = value 
        End Set
    End Property

    Public Property TPS() As Nullable(Of Decimal)
        Get
            Return m_TPS
        End Get
        Set(ByVal value As Nullable(Of Decimal))
            m_TPS = value 
        End Set
    End Property

    Public Property TVQ() As Nullable(Of Decimal)
        Get
            Return m_TVQ
        End Get
        Set(ByVal value As Nullable(Of Decimal))
            m_TVQ = value 
        End Set
    End Property

    Public Property IsCommissionVendeur() As Nullable(Of Boolean)
        Get
            Return m_IsCommissionVendeur
        End Get
        Set(ByVal value As Nullable(Of Boolean))
            m_IsCommissionVendeur = value 
        End Set
    End Property

End Class
 
