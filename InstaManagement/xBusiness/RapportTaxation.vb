Public Class RapportTaxation

    Private m_IDRapportTaxation As Int32
    Private m_IDPeriode As Nullable(Of Int32)
    Private m_IDLivreur As Nullable(Of Int32)
    Private m_Livreur As String
    Private m_CheminRapport As String
    Private m_NomFichier As String
    Private m_Email As String
    Private m_DateFrom As Nullable(Of DateTime)
    Private m_DateTo As Nullable(Of DateTime)

    Public Sub New()

    End Sub

    Public Property IDRapportTaxation() As Int32
        Get
            Return m_IDRapportTaxation
        End Get
        Set(ByVal value As Int32)
            m_IDRapportTaxation = value 
        End Set
    End Property

    Public Property IDPeriode() As Nullable(Of Int32)
        Get
            Return m_IDPeriode
        End Get
        Set(ByVal value As Nullable(Of Int32))
            m_IDPeriode = value 
        End Set
    End Property

    Public Property IDLivreur() As Nullable(Of Int32)
        Get
            Return m_IDLivreur
        End Get
        Set(ByVal value As Nullable(Of Int32))
            m_IDLivreur = value 
        End Set
    End Property

    Public Property Livreur() As String
        Get
            Return m_Livreur
        End Get
        Set(ByVal value As String)
            m_Livreur = value 
        End Set
    End Property

    Public Property CheminRapport() As String
        Get
            Return m_CheminRapport
        End Get
        Set(ByVal value As String)
            m_CheminRapport = value 
        End Set
    End Property

    Public Property NomFichier() As String
        Get
            Return m_NomFichier
        End Get
        Set(ByVal value As String)
            m_NomFichier = value 
        End Set
    End Property

    Public Property Email() As String
        Get
            Return m_Email
        End Get
        Set(ByVal value As String)
            m_Email = value 
        End Set
    End Property

    Public Property DateFrom() As Nullable(Of DateTime)
        Get
            Return m_DateFrom
        End Get
        Set(ByVal value As Nullable(Of DateTime))
            m_DateFrom = value 
        End Set
    End Property

    Public Property DateTo() As Nullable(Of DateTime)
        Get
            Return m_DateTo
        End Get
        Set(ByVal value As Nullable(Of DateTime))
            m_DateTo = value 
        End Set
    End Property

End Class
 
