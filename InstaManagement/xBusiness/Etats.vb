Public Class Etats

    Private m_IDEtats As Int32
    Private m_IDPeriode As Nullable(Of Int32)
    Private m_IDOrganisation As Nullable(Of Int32)
    Private m_IDVendeur As Nullable(Of Int32)
    Private m_CheminRapport As String
    Private m_NomFichier As String
    Private m_Email As String
    Private m_Email1 As String
    Private m_Email2 As String
    Private m_Organisation As String
    Private m_Vendeur As String
    Private m_DateFrom As Nullable(Of DateTime)
    Private m_DateTo As Nullable(Of DateTime)

    Public Sub New()

    End Sub

    Public Property IDEtats() As Int32
        Get
            Return m_IDEtats
        End Get
        Set(ByVal value As Int32)
            m_IDEtats = value 
        End Set
    End Property

    Public Property IDPeriode() As Nullable(Of Int32)
        Get
            Return m_IDPeriode
        End Get
        Set(ByVal value As Nullable(Of Int32))
            m_IDPeriode = value 
        End Set
    End Property

    Public Property IDOrganisation() As Nullable(Of Int32)
        Get
            Return m_IDOrganisation
        End Get
        Set(ByVal value As Nullable(Of Int32))
            m_IDOrganisation = value 
        End Set
    End Property

    Public Property IDVendeur() As Nullable(Of Int32)
        Get
            Return m_IDVendeur
        End Get
        Set(ByVal value As Nullable(Of Int32))
            m_IDVendeur = value 
        End Set
    End Property

    Public Property CheminRapport() As String
        Get
            Return m_CheminRapport
        End Get
        Set(ByVal value As String)
            m_CheminRapport = value 
        End Set
    End Property

    Public Property NomFichier() As String
        Get
            Return m_NomFichier
        End Get
        Set(ByVal value As String)
            m_NomFichier = value 
        End Set
    End Property

    Public Property Email() As String
        Get
            Return m_Email
        End Get
        Set(ByVal value As String)
            m_Email = value 
        End Set
    End Property

    Public Property Email1() As String
        Get
            Return m_Email1
        End Get
        Set(ByVal value As String)
            m_Email1 = value 
        End Set
    End Property

    Public Property Email2() As String
        Get
            Return m_Email2
        End Get
        Set(ByVal value As String)
            m_Email2 = value 
        End Set
    End Property

    Public Property Organisation() As String
        Get
            Return m_Organisation
        End Get
        Set(ByVal value As String)
            m_Organisation = value 
        End Set
    End Property

    Public Property Vendeur() As String
        Get
            Return m_Vendeur
        End Get
        Set(ByVal value As String)
            m_Vendeur = value 
        End Set
    End Property

    Public Property DateFrom() As Nullable(Of DateTime)
        Get
            Return m_DateFrom
        End Get
        Set(ByVal value As Nullable(Of DateTime))
            m_DateFrom = value 
        End Set
    End Property

    Public Property DateTo() As Nullable(Of DateTime)
        Get
            Return m_DateTo
        End Get
        Set(ByVal value As Nullable(Of DateTime))
            m_DateTo = value 
        End Set
    End Property

End Class
 
