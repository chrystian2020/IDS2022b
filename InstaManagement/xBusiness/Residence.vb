Public Class Residence

    Private m_IDResidence As Int32
    Private m_Residence As String

    Public Sub New()

    End Sub

    Public Property IDResidence() As Int32
        Get
            Return m_IDResidence
        End Get
        Set(ByVal value As Int32)
            m_IDResidence = value 
        End Set
    End Property

    Public Property Residence() As String
        Get
            Return m_Residence
        End Get
        Set(ByVal value As String)
            m_Residence = value 
        End Set
    End Property

End Class
 
