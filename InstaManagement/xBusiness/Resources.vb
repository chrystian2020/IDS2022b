Public Class Resources

    Private m_UniqueID As Int32
    Private m_ResourceID As Int32
    Private m_ResourceName As String
    Private m_Color As Nullable(Of Int32)
    Private m_CustomField1 As String

    Public Sub New()

    End Sub

    Public Property UniqueID() As Int32
        Get
            Return m_UniqueID
        End Get
        Set(ByVal value As Int32)
            m_UniqueID = value 
        End Set
    End Property

    Public Property ResourceID() As Int32
        Get
            Return m_ResourceID
        End Get
        Set(ByVal value As Int32)
            m_ResourceID = value 
        End Set
    End Property

    Public Property ResourceName() As String
        Get
            Return m_ResourceName
        End Get
        Set(ByVal value As String)
            m_ResourceName = value 
        End Set
    End Property

    Public Property Color() As Nullable(Of Int32)
        Get
            Return m_Color
        End Get
        Set(ByVal value As Nullable(Of Int32))
            m_Color = value 
        End Set
    End Property

    Public Property CustomField1() As String
        Get
            Return m_CustomField1
        End Get
        Set(ByVal value As String)
            m_CustomField1 = value 
        End Set
    End Property

End Class
 
