Public Class EpargnesPaiements

    Private m_IDEpargnePaiment As Int32
    Private m_IDEpargne As Int32
    Private m_IDLivreur As Nullable(Of Int32)
    Private m_Livreur As String
    Private m_DateEpargne As Nullable(Of DateTime)
    Private m_Epargne As Nullable(Of Decimal)
    Private m_Periode As Nullable(Of Int32)

    Public Sub New()

    End Sub

    Public Property IDEpargnePaiment() As Int32
        Get
            Return m_IDEpargnePaiment
        End Get
        Set(ByVal value As Int32)
            m_IDEpargnePaiment = value 
        End Set
    End Property

    Public Property IDEpargne() As Int32
        Get
            Return m_IDEpargne
        End Get
        Set(ByVal value As Int32)
            m_IDEpargne = value 
        End Set
    End Property

    Public Property IDLivreur() As Nullable(Of Int32)
        Get
            Return m_IDLivreur
        End Get
        Set(ByVal value As Nullable(Of Int32))
            m_IDLivreur = value 
        End Set
    End Property

    Public Property Livreur() As String
        Get
            Return m_Livreur
        End Get
        Set(ByVal value As String)
            m_Livreur = value 
        End Set
    End Property

    Public Property DateEpargne() As Nullable(Of DateTime)
        Get
            Return m_DateEpargne
        End Get
        Set(ByVal value As Nullable(Of DateTime))
            m_DateEpargne = value
        End Set
    End Property

    Public Property Epargne() As Nullable(Of Decimal)
        Get
            Return m_Epargne
        End Get
        Set(ByVal value As Nullable(Of Decimal))
            m_Epargne = value 
        End Set
    End Property

    Public Property Periode() As Nullable(Of Int32)
        Get
            Return m_Periode
        End Get
        Set(ByVal value As Nullable(Of Int32))
            m_Periode = value 
        End Set
    End Property

End Class
 
