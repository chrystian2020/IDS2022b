Public Class Ventes

    Private m_IDVente As Int32
    Private m_DateVente As Nullable(Of DateTime)
    Private m_DateFrom As Nullable(Of DateTime)
    Private m_DateTo As Nullable(Of DateTime)
    Private m_IDPeriode As Nullable(Of Int32)
    Private m_OrganisationID As Nullable(Of Int32)
    Private m_Organisation As String
    Private m_Description As String
    Private m_Unite As Nullable(Of Decimal)

    Private m_Montant As Nullable(Of Decimal)


    Public Sub New()

    End Sub

    Public Property IDVente() As Int32
        Get
            Return m_IDVente
        End Get
        Set(ByVal value As Int32)
            m_IDVente = value 
        End Set
    End Property

    Public Property DateVente() As Nullable(Of DateTime)
        Get
            Return m_DateVente
        End Get
        Set(ByVal value As Nullable(Of DateTime))
            m_DateVente = value
        End Set
    End Property

    Public Property DateFrom() As Nullable(Of DateTime)
        Get
            Return m_DateFrom
        End Get
        Set(ByVal value As Nullable(Of DateTime))
            m_DateFrom = value 
        End Set
    End Property

    Public Property DateTo() As Nullable(Of DateTime)
        Get
            Return m_DateTo
        End Get
        Set(ByVal value As Nullable(Of DateTime))
            m_DateTo = value 
        End Set
    End Property

    Public Property IDPeriode() As Nullable(Of Int32)
        Get
            Return m_IDPeriode
        End Get
        Set(ByVal value As Nullable(Of Int32))
            m_IDPeriode = value 
        End Set
    End Property

    Public Property OrganisationID() As Nullable(Of Int32)
        Get
            Return m_OrganisationID
        End Get
        Set(ByVal value As Nullable(Of Int32))
            m_OrganisationID = value 
        End Set
    End Property

    Public Property Organisation() As String
        Get
            Return m_Organisation
        End Get
        Set(ByVal value As String)
            m_Organisation = value 
        End Set
    End Property

    Public Property Description() As String
        Get
            Return m_Description
        End Get
        Set(ByVal value As String)
            m_Description = value 
        End Set
    End Property

    Public Property Unite() As Nullable(Of Decimal)
        Get
            Return m_Unite
        End Get
        Set(ByVal value As Nullable(Of Decimal))
            m_Unite = value 
        End Set
    End Property



    Public Property Montant() As Nullable(Of Decimal)
        Get
            Return m_Montant
        End Get
        Set(ByVal value As Nullable(Of Decimal))
            m_Montant = value 
        End Set
    End Property


End Class
 
