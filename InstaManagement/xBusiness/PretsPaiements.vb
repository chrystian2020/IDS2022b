Public Class PretsPaiements

    Private m_IDPretPaiment As Int32
    Private m_IDPret As Int32
    Private m_IDLivreur As Nullable(Of Int32)
    Private m_Livreur As String
    Private m_DatePaiement As Nullable(Of DateTime)
    Private m_Paiement As Nullable(Of Decimal)
    Private m_Periode As Nullable(Of Int32)

    Public Sub New()

    End Sub

    Public Property IDPretPaiment() As Int32
        Get
            Return m_IDPretPaiment
        End Get
        Set(ByVal value As Int32)
            m_IDPretPaiment = value 
        End Set
    End Property

    Public Property IDPret() As Int32
        Get
            Return m_IDPret
        End Get
        Set(ByVal value As Int32)
            m_IDPret = value 
        End Set
    End Property

    Public Property IDLivreur() As Nullable(Of Int32)
        Get
            Return m_IDLivreur
        End Get
        Set(ByVal value As Nullable(Of Int32))
            m_IDLivreur = value 
        End Set
    End Property

    Public Property Livreur() As String
        Get
            Return m_Livreur
        End Get
        Set(ByVal value As String)
            m_Livreur = value 
        End Set
    End Property

    Public Property DatePaiement() As Nullable(Of DateTime)
        Get
            Return m_DatePaiement
        End Get
        Set(ByVal value As Nullable(Of DateTime))
            m_DatePaiement = value 
        End Set
    End Property

    Public Property Paiement() As Nullable(Of Decimal)
        Get
            Return m_Paiement
        End Get
        Set(ByVal value As Nullable(Of Decimal))
            m_Paiement = value 
        End Set
    End Property

    Public Property Periode() As Nullable(Of Int32)
        Get
            Return m_Periode
        End Get
        Set(ByVal value As Nullable(Of Int32))
            m_Periode = value 
        End Set
    End Property

End Class
 
