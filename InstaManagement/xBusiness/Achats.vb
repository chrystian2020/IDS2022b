Public Class Achats

    Private m_IDAchat As Int32
    Private m_DateAchat As Nullable(Of DateTime)
    Private m_DateFrom As Nullable(Of DateTime)
    Private m_DateTo As Nullable(Of DateTime)
    Private m_IDPeriode As Nullable(Of Int32)
    Private m_IDFournisseur As Nullable(Of Int32)
    Private m_Fournisseur As String
    Private m_Unite As Nullable(Of Decimal)
    Private m_Prix As Nullable(Of Decimal)
    Private m_Montant As Nullable(Of Decimal)
    Private m_TPS As Nullable(Of Decimal)
    Private m_TVQ As Nullable(Of Decimal)
    Private m_Total As Nullable(Of Decimal)
    Private m_Description As String

    Public Sub New()

    End Sub

    Public Property IDAchat() As Int32
        Get
            Return m_IDAchat
        End Get
        Set(ByVal value As Int32)
            m_IDAchat = value 
        End Set
    End Property

    Public Property DateAchat() As Nullable(Of DateTime)
        Get
            Return m_DateAchat
        End Get
        Set(ByVal value As Nullable(Of DateTime))
            m_DateAchat = value 
        End Set
    End Property

    Public Property DateFrom() As Nullable(Of DateTime)
        Get
            Return m_DateFrom
        End Get
        Set(ByVal value As Nullable(Of DateTime))
            m_DateFrom = value 
        End Set
    End Property

    Public Property DateTo() As Nullable(Of DateTime)
        Get
            Return m_DateTo
        End Get
        Set(ByVal value As Nullable(Of DateTime))
            m_DateTo = value 
        End Set
    End Property

    Public Property IDPeriode() As Nullable(Of Int32)
        Get
            Return m_IDPeriode
        End Get
        Set(ByVal value As Nullable(Of Int32))
            m_IDPeriode = value 
        End Set
    End Property

    Public Property IDFournisseur() As Nullable(Of Int32)
        Get
            Return m_IDFournisseur
        End Get
        Set(ByVal value As Nullable(Of Int32))
            m_IDFournisseur = value 
        End Set
    End Property

    Public Property Fournisseur() As String
        Get
            Return m_Fournisseur
        End Get
        Set(ByVal value As String)
            m_Fournisseur = value 
        End Set
    End Property

    Public Property Unite() As Nullable(Of Decimal)
        Get
            Return m_Unite
        End Get
        Set(ByVal value As Nullable(Of Decimal))
            m_Unite = value 
        End Set
    End Property

    Public Property Prix() As Nullable(Of Decimal)
        Get
            Return m_Prix
        End Get
        Set(ByVal value As Nullable(Of Decimal))
            m_Prix = value 
        End Set
    End Property

    Public Property Montant() As Nullable(Of Decimal)
        Get
            Return m_Montant
        End Get
        Set(ByVal value As Nullable(Of Decimal))
            m_Montant = value 
        End Set
    End Property

    Public Property TPS() As Nullable(Of Decimal)
        Get
            Return m_TPS
        End Get
        Set(ByVal value As Nullable(Of Decimal))
            m_TPS = value 
        End Set
    End Property

    Public Property TVQ() As Nullable(Of Decimal)
        Get
            Return m_TVQ
        End Get
        Set(ByVal value As Nullable(Of Decimal))
            m_TVQ = value 
        End Set
    End Property

    Public Property Total() As Nullable(Of Decimal)
        Get
            Return m_Total
        End Get
        Set(ByVal value As Nullable(Of Decimal))
            m_Total = value 
        End Set
    End Property

    Public Property Description() As String
        Get
            Return m_Description
        End Get
        Set(ByVal value As String)
            m_Description = value 
        End Set
    End Property

End Class
 
