Public Class Epargnes

    Private m_IDEpargne As Int32
    Private m_IDLivreur As Nullable(Of Int32)
    Private m_Livreur As String
    Private m_DateEpargne As Nullable(Of DateTime)
    Private m_EpargneParPeriode As Nullable(Of Decimal)
    Private m_EpargneActif As Nullable(Of Boolean)
    Private m_Balance As Nullable(Of Decimal)

    Public Sub New()

    End Sub

    Public Property IDEpargne() As Int32
        Get
            Return m_IDEpargne
        End Get
        Set(ByVal value As Int32)
            m_IDEpargne = value 
        End Set
    End Property

    Public Property IDLivreur() As Nullable(Of Int32)
        Get
            Return m_IDLivreur
        End Get
        Set(ByVal value As Nullable(Of Int32))
            m_IDLivreur = value 
        End Set
    End Property

    Public Property Livreur() As String
        Get
            Return m_Livreur
        End Get
        Set(ByVal value As String)
            m_Livreur = value 
        End Set
    End Property

    Public Property DateEpargne() As Nullable(Of DateTime)
        Get
            Return m_DateEpargne
        End Get
        Set(ByVal value As Nullable(Of DateTime))
            m_DateEpargne = value 
        End Set
    End Property

    Public Property EpargneParPeriode() As Nullable(Of Decimal)
        Get
            Return m_EpargneParPeriode
        End Get
        Set(ByVal value As Nullable(Of Decimal))
            m_EpargneParPeriode = value 
        End Set
    End Property

    Public Property EpargneActif() As Nullable(Of Boolean)
        Get
            Return m_EpargneActif
        End Get
        Set(ByVal value As Nullable(Of Boolean))
            m_EpargneActif = value 
        End Set
    End Property

    Public Property Balance() As Nullable(Of Decimal)
        Get
            Return m_Balance
        End Get
        Set(ByVal value As Nullable(Of Decimal))
            m_Balance = value 
        End Set
    End Property

End Class
 
