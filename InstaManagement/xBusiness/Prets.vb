Public Class Prets

    Private m_IDPret As Int32
    Private m_IDLivreur As Nullable(Of Int32)
    Private m_Livreur As String
    Private m_DatePret As Nullable(Of DateTime)
    Private m_MontantPret As Nullable(Of Decimal)
    Private m_BalancePret As Nullable(Of Decimal)
    Private m_NombreSemaine As Nullable(Of Int32)
    Private m_PaiementParPeriode As Nullable(Of Decimal)

    Public Sub New()

    End Sub

    Public Property IDPret() As Int32
        Get
            Return m_IDPret
        End Get
        Set(ByVal value As Int32)
            m_IDPret = value 
        End Set
    End Property

    Public Property IDLivreur() As Nullable(Of Int32)
        Get
            Return m_IDLivreur
        End Get
        Set(ByVal value As Nullable(Of Int32))
            m_IDLivreur = value 
        End Set
    End Property

    Public Property Livreur() As String
        Get
            Return m_Livreur
        End Get
        Set(ByVal value As String)
            m_Livreur = value 
        End Set
    End Property

    Public Property DatePret() As Nullable(Of DateTime)
        Get
            Return m_DatePret
        End Get
        Set(ByVal value As Nullable(Of DateTime))
            m_DatePret = value 
        End Set
    End Property

    Public Property MontantPret() As Nullable(Of Decimal)
        Get
            Return m_MontantPret
        End Get
        Set(ByVal value As Nullable(Of Decimal))
            m_MontantPret = value 
        End Set
    End Property

    Public Property BalancePret() As Nullable(Of Decimal)
        Get
            Return m_BalancePret
        End Get
        Set(ByVal value As Nullable(Of Decimal))
            m_BalancePret = value 
        End Set
    End Property

    Public Property NombreSemaine() As Nullable(Of Int32)
        Get
            Return m_NombreSemaine
        End Get
        Set(ByVal value As Nullable(Of Int32))
            m_NombreSemaine = value 
        End Set
    End Property

    Public Property PaiementParPeriode() As Nullable(Of Decimal)
        Get
            Return m_PaiementParPeriode
        End Get
        Set(ByVal value As Nullable(Of Decimal))
            m_PaiementParPeriode = value 
        End Set
    End Property

End Class
 
