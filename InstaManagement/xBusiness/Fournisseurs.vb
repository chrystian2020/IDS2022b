Public Class Fournisseurs

    Private m_IDFournisseur As Int32
    Private m_Fournisseur As String
    Private m_Description As String

    Public Sub New()

    End Sub

    Public Property IDFournisseur() As Int32
        Get
            Return m_IDFournisseur
        End Get
        Set(ByVal value As Int32)
            m_IDFournisseur = value 
        End Set
    End Property

    Public Property Fournisseur() As String
        Get
            Return m_Fournisseur
        End Get
        Set(ByVal value As String)
            m_Fournisseur = value 
        End Set
    End Property

    Public Property Description() As String
        Get
            Return m_Description
        End Get
        Set(ByVal value As String)
            m_Description = value 
        End Set
    End Property

End Class
 
