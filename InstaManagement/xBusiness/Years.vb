Public Class Years

    Private m_IDYear As Int32
    Private m_Year As Nullable(Of Int32)

    Public Sub New()

    End Sub

    Public Property IDYear() As Int32
        Get
            Return m_IDYear
        End Get
        Set(ByVal value As Int32)
            m_IDYear = value 
        End Set
    End Property

    Public Property Year() As Nullable(Of Int32)
        Get
            Return m_Year
        End Get
        Set(ByVal value As Nullable(Of Int32))
            m_Year = value 
        End Set
    End Property

End Class
 
