Public Class CadeauOrganisation

    Private m_IDCadeauOrganisation As Int32
    Private m_IDOrganisation As Nullable(Of Int32)
    Private m_Organisation As String
    Private m_DateFrom As Nullable(Of DateTime)
    Private m_DateTo As Nullable(Of DateTime)
    Private m_Montant As Nullable(Of Decimal)
    Private m_IDPeriode As Nullable(Of Int32)

    Public Sub New()

    End Sub

    Public Property IDCadeauOrganisation() As Int32
        Get
            Return m_IDCadeauOrganisation
        End Get
        Set(ByVal value As Int32)
            m_IDCadeauOrganisation = value 
        End Set
    End Property

    Public Property IDOrganisation() As Nullable(Of Int32)
        Get
            Return m_IDOrganisation
        End Get
        Set(ByVal value As Nullable(Of Int32))
            m_IDOrganisation = value 
        End Set
    End Property

    Public Property Organisation() As String
        Get
            Return m_Organisation
        End Get
        Set(ByVal value As String)
            m_Organisation = value 
        End Set
    End Property

    Public Property DateFrom() As Nullable(Of DateTime)
        Get
            Return m_DateFrom
        End Get
        Set(ByVal value As Nullable(Of DateTime))
            m_DateFrom = value 
        End Set
    End Property

    Public Property DateTo() As Nullable(Of DateTime)
        Get
            Return m_DateTo
        End Get
        Set(ByVal value As Nullable(Of DateTime))
            m_DateTo = value 
        End Set
    End Property

    Public Property Montant() As Nullable(Of Decimal)
        Get
            Return m_Montant
        End Get
        Set(ByVal value As Nullable(Of Decimal))
            m_Montant = value 
        End Set
    End Property

    Public Property IDPeriode() As Nullable(Of Int32)
        Get
            Return m_IDPeriode
        End Get
        Set(ByVal value As Nullable(Of Int32))
            m_IDPeriode = value 
        End Set
    End Property

End Class
 
