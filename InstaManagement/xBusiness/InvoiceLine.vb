Public Class InvoiceLine

    Private m_IDInvoiceLine As Int32
    Private m_IDInvoice As Nullable(Of Int32)
    Private m_InvoiceNumber As String
    Private m_InvoiceDate As Nullable(Of DateTime)
    Private m_DisplayDate As Nullable(Of DateTime)
    Private m_OrganisationID As Nullable(Of Int32)
    Private m_Organisation As String
    Private m_DateFrom As Nullable(Of DateTime)
    Private m_DateTo As Nullable(Of DateTime)
    Private m_Code As String
    Private m_Km As Nullable(Of Decimal)
    Private m_Description As String
    Private m_Unite As Nullable(Of Decimal)
    Private m_Prix As Nullable(Of Decimal)
    Private m_Montant As Nullable(Of Decimal)
    Private m_Credit As Nullable(Of Decimal)
    Private m_Debit As Nullable(Of Decimal)
    Private m_NombreGlacier As Nullable(Of Int32)
    Private m_Glacier As Nullable(Of Decimal)
    Private m_KmExtra As Nullable(Of Decimal)
    Private m_LivreurID As Nullable(Of Int32)
    Private m_IDPeriode As Nullable(Of Int32)
    Private m_TypeAjout As Nullable(Of Int32)
    Private m_Ajout As Nullable(Of Boolean)


    Public Sub New()

    End Sub

    Public Property IDInvoiceLine() As Int32
        Get
            Return m_IDInvoiceLine
        End Get
        Set(ByVal value As Int32)
            m_IDInvoiceLine = value 
        End Set
    End Property

    Public Property IDInvoice() As Nullable(Of Int32)
        Get
            Return m_IDInvoice
        End Get
        Set(ByVal value As Nullable(Of Int32))
            m_IDInvoice = value 
        End Set
    End Property

    Public Property InvoiceNumber() As String
        Get
            Return m_InvoiceNumber
        End Get
        Set(ByVal value As String)
            m_InvoiceNumber = value 
        End Set
    End Property

    Public Property InvoiceDate() As Nullable(Of DateTime)
        Get
            Return m_InvoiceDate
        End Get
        Set(ByVal value As Nullable(Of DateTime))
            m_InvoiceDate = value 
        End Set
    End Property

    Public Property DisplayDate() As Nullable(Of DateTime)
        Get
            Return m_DisplayDate
        End Get
        Set(ByVal value As Nullable(Of DateTime))
            m_DisplayDate = value 
        End Set
    End Property

    Public Property OrganisationID() As Nullable(Of Int32)
        Get
            Return m_OrganisationID
        End Get
        Set(ByVal value As Nullable(Of Int32))
            m_OrganisationID = value 
        End Set
    End Property

    Public Property Organisation() As String
        Get
            Return m_Organisation
        End Get
        Set(ByVal value As String)
            m_Organisation = value 
        End Set
    End Property

    Public Property DateFrom() As Nullable(Of DateTime)
        Get
            Return m_DateFrom
        End Get
        Set(ByVal value As Nullable(Of DateTime))
            m_DateFrom = value 
        End Set
    End Property

    Public Property DateTo() As Nullable(Of DateTime)
        Get
            Return m_DateTo
        End Get
        Set(ByVal value As Nullable(Of DateTime))
            m_DateTo = value 
        End Set
    End Property

    Public Property Code() As String
        Get
            Return m_Code
        End Get
        Set(ByVal value As String)
            m_Code = value 
        End Set
    End Property

    Public Property Km() As Nullable(Of Decimal)
        Get
            Return m_Km
        End Get
        Set(ByVal value As Nullable(Of Decimal))
            m_Km = value 
        End Set
    End Property

    Public Property Description() As String
        Get
            Return m_Description
        End Get
        Set(ByVal value As String)
            m_Description = value 
        End Set
    End Property

    Public Property Unite() As Nullable(Of Decimal)
        Get
            Return m_Unite
        End Get
        Set(ByVal value As Nullable(Of Decimal))
            m_Unite = value 
        End Set
    End Property

    Public Property Prix() As Nullable(Of Decimal)
        Get
            Return m_Prix
        End Get
        Set(ByVal value As Nullable(Of Decimal))
            m_Prix = value 
        End Set
    End Property

    Public Property Montant() As Nullable(Of Decimal)
        Get
            Return m_Montant
        End Get
        Set(ByVal value As Nullable(Of Decimal))
            m_Montant = value 
        End Set
    End Property

    Public Property Credit() As Nullable(Of Decimal)
        Get
            Return m_Credit
        End Get
        Set(ByVal value As Nullable(Of Decimal))
            m_Credit = value 
        End Set
    End Property

    Public Property Debit() As Nullable(Of Decimal)
        Get
            Return m_Debit
        End Get
        Set(ByVal value As Nullable(Of Decimal))
            m_Debit = value 
        End Set
    End Property

    Public Property NombreGlacier() As Nullable(Of Int32)
        Get
            Return m_NombreGlacier
        End Get
        Set(ByVal value As Nullable(Of Int32))
            m_NombreGlacier = value 
        End Set
    End Property

    Public Property Glacier() As Nullable(Of Decimal)
        Get
            Return m_Glacier
        End Get
        Set(ByVal value As Nullable(Of Decimal))
            m_Glacier = value 
        End Set
    End Property

    Public Property KmExtra() As Nullable(Of Decimal)
        Get
            Return m_KmExtra
        End Get
        Set(ByVal value As Nullable(Of Decimal))
            m_KmExtra = value 
        End Set
    End Property

    Public Property LivreurID() As Nullable(Of Int32)
        Get
            Return m_LivreurID
        End Get
        Set(ByVal value As Nullable(Of Int32))
            m_LivreurID = value 
        End Set
    End Property

    Public Property IDPeriode() As Nullable(Of Int32)
        Get
            Return m_IDPeriode
        End Get
        Set(ByVal value As Nullable(Of Int32))
            m_IDPeriode = value 
        End Set
    End Property

    Public Property TypeAjout() As Nullable(Of Int32)
        Get
            Return m_TypeAjout
        End Get
        Set(ByVal value As Nullable(Of Int32))
            m_TypeAjout = value 
        End Set
    End Property

    Public Property Ajout() As Nullable(Of Boolean)
        Get
            Return m_Ajout
        End Get
        Set(ByVal value As Nullable(Of Boolean))
            m_Ajout = value 
        End Set
    End Property



End Class
 
