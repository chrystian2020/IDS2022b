Public Class livreurs

    Private m_LivreurID As Int32
    Private m_LivreurCache As String
    Private m_Livreur As String
    Private m_Telephone As String
    Private m_Adresse As String
    Private m_CalculerTaxes As Nullable(Of Boolean)
    Private m_Email As String
    Private m_Email1 As String
    Private m_Email2 As String
    Private m_IsVendeur As Nullable(Of Boolean)
    Private m_Actif As Nullable(Of Boolean)
    Private m_IDVendeur As Nullable(Of Int32)
    Private m_CommissionVendeur As Nullable(Of Decimal)
    Private m_NomCompagnie As String
    Private m_NoTPS As String
    Private m_NoTVQ As String

    Public Sub New()

    End Sub

    Public Property LivreurID() As Int32
        Get
            Return m_LivreurID
        End Get
        Set(ByVal value As Int32)
            m_LivreurID = value
        End Set
    End Property

    Public Property LivreurCache() As String
        Get
            Return m_LivreurCache
        End Get
        Set(ByVal value As String)
            m_LivreurCache = value
        End Set
    End Property

    Public Property Livreur() As String
        Get
            Return m_Livreur
        End Get
        Set(ByVal value As String)
            m_Livreur = value
        End Set
    End Property

    Public Property Telephone() As String
        Get
            Return m_Telephone
        End Get
        Set(ByVal value As String)
            m_Telephone = value
        End Set
    End Property

    Public Property Adresse() As String
        Get
            Return m_Adresse
        End Get
        Set(ByVal value As String)
            m_Adresse = value
        End Set
    End Property

    Public Property CalculerTaxes() As Nullable(Of Boolean)
        Get
            Return m_CalculerTaxes
        End Get
        Set(ByVal value As Nullable(Of Boolean))
            m_CalculerTaxes = value
        End Set
    End Property

    Public Property Email() As String
        Get
            Return m_Email
        End Get
        Set(ByVal value As String)
            m_Email = value
        End Set
    End Property
    Public Property Email1() As String
        Get
            Return m_Email1
        End Get
        Set(ByVal value As String)
            m_Email1 = value
        End Set
    End Property
    Public Property Email2() As String
        Get
            Return m_Email2
        End Get
        Set(ByVal value As String)
            m_Email2 = value
        End Set
    End Property
    Public Property IsVendeur() As Nullable(Of Boolean)
        Get
            Return m_IsVendeur
        End Get
        Set(ByVal value As Nullable(Of Boolean))
            m_IsVendeur = value
        End Set
    End Property

    Public Property Actif() As Nullable(Of Boolean)
        Get
            Return m_Actif
        End Get
        Set(ByVal value As Nullable(Of Boolean))
            m_Actif = value
        End Set
    End Property

    Public Property IDVendeur() As Nullable(Of Int32)
        Get
            Return m_IDVendeur
        End Get
        Set(ByVal value As Nullable(Of Int32))
            m_IDVendeur = value
        End Set
    End Property

    Public Property CommissionVendeur() As Nullable(Of Decimal)
        Get
            Return m_CommissionVendeur
        End Get
        Set(ByVal value As Nullable(Of Decimal))
            m_CommissionVendeur = value
        End Set
    End Property

    Public Property NomCompagnie() As String
        Get
            Return m_NomCompagnie
        End Get
        Set(ByVal value As String)
            m_NomCompagnie = value
        End Set
    End Property

    Public Property NoTPS() As String
        Get
            Return m_NoTPS
        End Get
        Set(ByVal value As String)
            m_NoTPS = value
        End Set
    End Property

    Public Property NoTVQ() As String
        Get
            Return m_NoTVQ
        End Get
        Set(ByVal value As String)
            m_NoTVQ = value
        End Set
    End Property


End Class
 
