Public Class Paiements

    Private m_IDPaiement As Int32
    Private m_IDInvoice As Int32
    Private m_InvoiceNumber As String
    Private m_IDPeriode As Nullable(Of Int32)
    Private m_DatePaiement As Nullable(Of DateTime)
    Private m_OrganisationID As Nullable(Of Int32)
    Private m_Organisation As String
    Private m_Montant As Nullable(Of Decimal)

    Public Sub New()

    End Sub

    Public Property IDPaiement() As Int32
        Get
            Return m_IDPaiement
        End Get
        Set(ByVal value As Int32)
            m_IDPaiement = value 
        End Set
    End Property

    Public Property IDInvoice() As Int32
        Get
            Return m_IDInvoice
        End Get
        Set(ByVal value As Int32)
            m_IDInvoice = value 
        End Set
    End Property

    Public Property InvoiceNumber() As String
        Get
            Return m_InvoiceNumber
        End Get
        Set(ByVal value As String)
            m_InvoiceNumber = value 
        End Set
    End Property

    Public Property IDPeriode() As Nullable(Of Int32)
        Get
            Return m_IDPeriode
        End Get
        Set(ByVal value As Nullable(Of Int32))
            m_IDPeriode = value 
        End Set
    End Property

    Public Property DatePaiement() As Nullable(Of DateTime)
        Get
            Return m_DatePaiement
        End Get
        Set(ByVal value As Nullable(Of DateTime))
            m_DatePaiement = value 
        End Set
    End Property

    Public Property OrganisationID() As Nullable(Of Int32)
        Get
            Return m_OrganisationID
        End Get
        Set(ByVal value As Nullable(Of Int32))
            m_OrganisationID = value 
        End Set
    End Property

    Public Property Organisation() As String
        Get
            Return m_Organisation
        End Get
        Set(ByVal value As String)
            m_Organisation = value 
        End Set
    End Property

    Public Property Montant() As Nullable(Of Decimal)
        Get
            Return m_Montant
        End Get
        Set(ByVal value As Nullable(Of Decimal))
            m_Montant = value 
        End Set
    End Property

End Class
 
