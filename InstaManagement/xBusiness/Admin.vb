Public Class Admin

    Private m_TPS As Nullable(Of Decimal)
    Private m_TVQ As Nullable(Of Decimal)
    Private m_TPSNumero As String
    Private m_TVQNumero As String
    Private m_CompanyName As String
    Private m_CompanyAdresse As String
    Private m_CompanyContact As String
    Private m_CompanyContactTelephone As String
    Private m_LocationPaie As String
    Private m_LocationFacture As String
    Private m_Server As String
    Private m_Username As String
    Private m_Password As String
    Private m_Port As Nullable(Of Int32)
    Private m_UseSSL As Nullable(Of Boolean)
    Private m_LocationConsolidation As String
    Private m_LocationVentes As String
    Private m_LocationAchats As String
    Private m_LocationCommissions As String
    Private m_LocationRapportLivreur As String
    Private m_LocationRapportRestaurant As String
    Private m_LocationRapportDeTaxation As String
    Private m_LocationRapportDeTaxationIDS As String
    Private m_RapportTaxationLivreurPaye As String



    Public Sub New()

    End Sub

    Public Property RapportTaxationLivreurPaye() As String
        Get
            Return m_RapportTaxationLivreurPaye
        End Get
        Set(ByVal value As String)
            m_RapportTaxationLivreurPaye = value
        End Set
    End Property

    Public Property LocationRapportDeTaxationIDS() As String
        Get
            Return m_LocationRapportDeTaxationIDS
        End Get
        Set(ByVal value As String)
            m_LocationRapportDeTaxationIDS = value
        End Set
    End Property

    Public Property LocationRapportDeTaxation() As String
        Get
            Return m_LocationRapportDeTaxation
        End Get
        Set(ByVal value As String)
            m_LocationRapportDeTaxation = value
        End Set
    End Property

    Public Property TPS() As Nullable(Of Decimal)
        Get
            Return m_TPS
        End Get
        Set(ByVal value As Nullable(Of Decimal))
            m_TPS = value 
        End Set
    End Property

    Public Property TVQ() As Nullable(Of Decimal)
        Get
            Return m_TVQ
        End Get
        Set(ByVal value As Nullable(Of Decimal))
            m_TVQ = value 
        End Set
    End Property

    Public Property TPSNumero() As String
        Get
            Return m_TPSNumero
        End Get
        Set(ByVal value As String)
            m_TPSNumero = value 
        End Set
    End Property

    Public Property TVQNumero() As String
        Get
            Return m_TVQNumero
        End Get
        Set(ByVal value As String)
            m_TVQNumero = value 
        End Set
    End Property

    Public Property CompanyName() As String
        Get
            Return m_CompanyName
        End Get
        Set(ByVal value As String)
            m_CompanyName = value 
        End Set
    End Property

    Public Property CompanyAdresse() As String
        Get
            Return m_CompanyAdresse
        End Get
        Set(ByVal value As String)
            m_CompanyAdresse = value 
        End Set
    End Property

    Public Property CompanyContact() As String
        Get
            Return m_CompanyContact
        End Get
        Set(ByVal value As String)
            m_CompanyContact = value 
        End Set
    End Property

    Public Property CompanyContactTelephone() As String
        Get
            Return m_CompanyContactTelephone
        End Get
        Set(ByVal value As String)
            m_CompanyContactTelephone = value 
        End Set
    End Property

    Public Property LocationPaie() As String
        Get
            Return m_LocationPaie
        End Get
        Set(ByVal value As String)
            m_LocationPaie = value 
        End Set
    End Property

    Public Property LocationFacture() As String
        Get
            Return m_LocationFacture
        End Get
        Set(ByVal value As String)
            m_LocationFacture = value 
        End Set
    End Property

    Public Property Server() As String
        Get
            Return m_Server
        End Get
        Set(ByVal value As String)
            m_Server = value 
        End Set
    End Property

    Public Property Username() As String
        Get
            Return m_Username
        End Get
        Set(ByVal value As String)
            m_Username = value 
        End Set
    End Property

    Public Property Password() As String
        Get
            Return m_Password
        End Get
        Set(ByVal value As String)
            m_Password = value 
        End Set
    End Property

    Public Property Port() As Nullable(Of Int32)
        Get
            Return m_Port
        End Get
        Set(ByVal value As Nullable(Of Int32))
            m_Port = value 
        End Set
    End Property

    Public Property UseSSL() As Nullable(Of Boolean)
        Get
            Return m_UseSSL
        End Get
        Set(ByVal value As Nullable(Of Boolean))
            m_UseSSL = value 
        End Set
    End Property

    Public Property LocationConsolidation() As String
        Get
            Return m_LocationConsolidation
        End Get
        Set(ByVal value As String)
            m_LocationConsolidation = value 
        End Set
    End Property

    Public Property LocationVentes() As String
        Get
            Return m_LocationVentes
        End Get
        Set(ByVal value As String)
            m_LocationVentes = value 
        End Set
    End Property

    Public Property LocationAchats() As String
        Get
            Return m_LocationAchats
        End Get
        Set(ByVal value As String)
            m_LocationAchats = value 
        End Set
    End Property
    Public Property LocationCommissions() As String
        Get
            Return m_LocationCommissions
        End Get
        Set(ByVal value As String)
            m_LocationCommissions = value
        End Set
    End Property


    Public Property LocationRapportLivreur() As String
        Get
            Return m_LocationRapportLivreur
        End Get
        Set(ByVal value As String)
            m_LocationRapportLivreur = value
        End Set
    End Property


    Public Property LocationRapportRestaurant() As String
        Get
            Return m_LocationRapportRestaurant
        End Get
        Set(ByVal value As String)
            m_LocationRapportRestaurant = value
        End Set
    End Property


End Class
 
