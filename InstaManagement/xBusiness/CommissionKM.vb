Public Class CommissionKM

    Private m_IDCommissionKM As Int32
    Private m_OrganisationID As Nullable(Of Int32)
    Private m_Organisation As String
    Private m_KMFrom As Nullable(Of Decimal)
    Private m_KMTo As Nullable(Of Decimal)
    Private m_Montant As Nullable(Of Decimal)
    Private m_ExtraChargeParKM As Nullable(Of Decimal)
    Private m_PerKMNumber As Nullable(Of Decimal)
    Private m_MetodeCalculation As Nullable(Of Int32)
    Private m_HeureDepart As Nullable(Of DateTime)
    Private m_HeureFin As Nullable(Of DateTime)

    Public Sub New()

    End Sub

    Public Property IDCommissionKM() As Int32
        Get
            Return m_IDCommissionKM
        End Get
        Set(ByVal value As Int32)
            m_IDCommissionKM = value 
        End Set
    End Property

    Public Property OrganisationID() As Nullable(Of Int32)
        Get
            Return m_OrganisationID
        End Get
        Set(ByVal value As Nullable(Of Int32))
            m_OrganisationID = value 
        End Set
    End Property

    Public Property Organisation() As String
        Get
            Return m_Organisation
        End Get
        Set(ByVal value As String)
            m_Organisation = value 
        End Set
    End Property

    Public Property KMFrom() As Nullable(Of Decimal)
        Get
            Return m_KMFrom
        End Get
        Set(ByVal value As Nullable(Of Decimal))
            m_KMFrom = value 
        End Set
    End Property

    Public Property KMTo() As Nullable(Of Decimal)
        Get
            Return m_KMTo
        End Get
        Set(ByVal value As Nullable(Of Decimal))
            m_KMTo = value 
        End Set
    End Property

    Public Property Montant() As Nullable(Of Decimal)
        Get
            Return m_Montant
        End Get
        Set(ByVal value As Nullable(Of Decimal))
            m_Montant = value 
        End Set
    End Property

    Public Property ExtraChargeParKM() As Nullable(Of Decimal)
        Get
            Return m_ExtraChargeParKM
        End Get
        Set(ByVal value As Nullable(Of Decimal))
            m_ExtraChargeParKM = value 
        End Set
    End Property

    Public Property PerKMNumber() As Nullable(Of Decimal)
        Get
            Return m_PerKMNumber
        End Get
        Set(ByVal value As Nullable(Of Decimal))
            m_PerKMNumber = value
        End Set
    End Property

    Public Property MetodeCalculation() As Nullable(Of Int32)
        Get
            Return m_MetodeCalculation
        End Get
        Set(ByVal value As Nullable(Of Int32))
            m_MetodeCalculation = value 
        End Set
    End Property

    Public Property HeureDepart() As Nullable(Of DateTime)
        Get
            Return m_HeureDepart
        End Get
        Set(ByVal value As Nullable(Of DateTime))
            m_HeureDepart = value 
        End Set
    End Property

    Public Property HeureFin() As Nullable(Of DateTime)
        Get
            Return m_HeureFin
        End Get
        Set(ByVal value As Nullable(Of DateTime))
            m_HeureFin = value 
        End Set
    End Property

End Class
 
