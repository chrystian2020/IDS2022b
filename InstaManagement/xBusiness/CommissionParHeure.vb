Public Class CommissionParHeure

    Private m_IDCommissionParHeure As Int32
    Private m_OrganisationID As Nullable(Of Int32)
    Private m_Organisation As String
    Private m_LivreurID As Nullable(Of Int32)
    Private m_Livreur As String
    Private m_MontantParHeure As Nullable(Of Decimal)

    Public Sub New()

    End Sub

    Public Property IDCommissionParHeure() As Int32
        Get
            Return m_IDCommissionParHeure
        End Get
        Set(ByVal value As Int32)
            m_IDCommissionParHeure = value 
        End Set
    End Property

    Public Property OrganisationID() As Nullable(Of Int32)
        Get
            Return m_OrganisationID
        End Get
        Set(ByVal value As Nullable(Of Int32))
            m_OrganisationID = value 
        End Set
    End Property

    Public Property Organisation() As String
        Get
            Return m_Organisation
        End Get
        Set(ByVal value As String)
            m_Organisation = value 
        End Set
    End Property

    Public Property LivreurID() As Nullable(Of Int32)
        Get
            Return m_LivreurID
        End Get
        Set(ByVal value As Nullable(Of Int32))
            m_LivreurID = value 
        End Set
    End Property

    Public Property Livreur() As String
        Get
            Return m_Livreur
        End Get
        Set(ByVal value As String)
            m_Livreur = value 
        End Set
    End Property

    Public Property MontantParHeure() As Nullable(Of Decimal)
        Get
            Return m_MontantParHeure
        End Get
        Set(ByVal value As Nullable(Of Decimal))
            m_MontantParHeure = value 
        End Set
    End Property

End Class
 
