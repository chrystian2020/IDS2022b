Public Class ImportBackup

    Private m_ImportID As Int32
    Private m_Reference As String
    Private m_Code As String
    Private m_Status As String
    Private m_Client As String
    Private m_ClientAddress As String
    Private m_MomentDePassage As String
    Private m_PreparePar As String
    Private m_HeureAppel As Nullable(Of DateTime)
    Private m_HeurePrep As Nullable(Of DateTime)
    Private m_HeureDepart As Nullable(Of DateTime)
    Private m_HeureLivraison As Nullable(Of DateTime)
    Private m_MessagePassage As String
    Private m_TotalTempService As String
    Private m_Livreur As String
    Private m_Distance As Nullable(Of Decimal)
    Private m_Montant As Nullable(Of Decimal)
    Private m_Extra As Nullable(Of Decimal)
    Private m_Organisation As String
    Private m_OrganisationAddress As String
    Private m_IDNomFichierImport As Nullable(Of Int32)
    Private m_Debit As Nullable(Of Decimal)
    Private m_Credit As Nullable(Of Decimal)
    Private m_Glacier As Nullable(Of Boolean)
    Private m_NombreGlacier As Nullable(Of Int32)
    Private m_MontantContrat As Nullable(Of Decimal)
    Private m_OrganisationID As Nullable(Of Int32)
    Private m_LivreurID As Nullable(Of Int32)
    Private m_MontantLivreur As Nullable(Of Decimal)
    Private m_MontantGlacierOrganisation As Nullable(Of Decimal)
    Private m_MontantGlacierLivreur As Nullable(Of Decimal)
    Private m_MontantLivreurFinal As Nullable(Of Decimal)
    Private m_MontantLivreurPharmaplus As Nullable(Of Decimal)
    Private m_ExtraKM As Nullable(Of Decimal)
    Private m_Echange As Nullable(Of Boolean)
    Private m_IDPeriode As Nullable(Of Int32)
    Private m_ExtraLivreur As Nullable(Of Decimal)
    Private m_ExtraKMLivreur As Nullable(Of Decimal)
    Private m_IDSAmount As Nullable(Of Decimal)
    Private m_DateImport As Nullable(Of DateTime)
    Private m_IsMobilus As Nullable(Of Boolean)
    Private m_IDClient As Nullable(Of Int32)
    Private m_IDClientAddresse As Nullable(Of Int32)

    Public Sub New()

    End Sub

    Public Property ImportID() As Int32
        Get
            Return m_ImportID
        End Get
        Set(ByVal value As Int32)
            m_ImportID = value 
        End Set
    End Property

    Public Property Reference() As String
        Get
            Return m_Reference
        End Get
        Set(ByVal value As String)
            m_Reference = value 
        End Set
    End Property

    Public Property Code() As String
        Get
            Return m_Code
        End Get
        Set(ByVal value As String)
            m_Code = value 
        End Set
    End Property

    Public Property Status() As String
        Get
            Return m_Status
        End Get
        Set(ByVal value As String)
            m_Status = value 
        End Set
    End Property

    Public Property Client() As String
        Get
            Return m_Client
        End Get
        Set(ByVal value As String)
            m_Client = value 
        End Set
    End Property

    Public Property ClientAddress() As String
        Get
            Return m_ClientAddress
        End Get
        Set(ByVal value As String)
            m_ClientAddress = value 
        End Set
    End Property

    Public Property MomentDePassage() As String
        Get
            Return m_MomentDePassage
        End Get
        Set(ByVal value As String)
            m_MomentDePassage = value 
        End Set
    End Property

    Public Property PreparePar() As String
        Get
            Return m_PreparePar
        End Get
        Set(ByVal value As String)
            m_PreparePar = value 
        End Set
    End Property

    Public Property HeureAppel() As Nullable(Of DateTime)
        Get
            Return m_HeureAppel
        End Get
        Set(ByVal value As Nullable(Of DateTime))
            m_HeureAppel = value 
        End Set
    End Property

    Public Property HeurePrep() As Nullable(Of DateTime)
        Get
            Return m_HeurePrep
        End Get
        Set(ByVal value As Nullable(Of DateTime))
            m_HeurePrep = value 
        End Set
    End Property

    Public Property HeureDepart() As Nullable(Of DateTime)
        Get
            Return m_HeureDepart
        End Get
        Set(ByVal value As Nullable(Of DateTime))
            m_HeureDepart = value 
        End Set
    End Property

    Public Property HeureLivraison() As Nullable(Of DateTime)
        Get
            Return m_HeureLivraison
        End Get
        Set(ByVal value As Nullable(Of DateTime))
            m_HeureLivraison = value 
        End Set
    End Property

    Public Property MessagePassage() As String
        Get
            Return m_MessagePassage
        End Get
        Set(ByVal value As String)
            m_MessagePassage = value 
        End Set
    End Property

    Public Property TotalTempService() As String
        Get
            Return m_TotalTempService
        End Get
        Set(ByVal value As String)
            m_TotalTempService = value 
        End Set
    End Property

    Public Property Livreur() As String
        Get
            Return m_Livreur
        End Get
        Set(ByVal value As String)
            m_Livreur = value 
        End Set
    End Property

    Public Property Distance() As Nullable(Of Decimal)
        Get
            Return m_Distance
        End Get
        Set(ByVal value As Nullable(Of Decimal))
            m_Distance = value 
        End Set
    End Property

    Public Property Montant() As Nullable(Of Decimal)
        Get
            Return m_Montant
        End Get
        Set(ByVal value As Nullable(Of Decimal))
            m_Montant = value 
        End Set
    End Property

    Public Property Extra() As Nullable(Of Decimal)
        Get
            Return m_Extra
        End Get
        Set(ByVal value As Nullable(Of Decimal))
            m_Extra = value 
        End Set
    End Property

    Public Property Organisation() As String
        Get
            Return m_Organisation
        End Get
        Set(ByVal value As String)
            m_Organisation = value 
        End Set
    End Property

    Public Property OrganisationAddress() As String
        Get
            Return m_OrganisationAddress
        End Get
        Set(ByVal value As String)
            m_OrganisationAddress = value 
        End Set
    End Property

    Public Property IDNomFichierImport() As Nullable(Of Int32)
        Get
            Return m_IDNomFichierImport
        End Get
        Set(ByVal value As Nullable(Of Int32))
            m_IDNomFichierImport = value 
        End Set
    End Property

    Public Property Debit() As Nullable(Of Decimal)
        Get
            Return m_Debit
        End Get
        Set(ByVal value As Nullable(Of Decimal))
            m_Debit = value 
        End Set
    End Property

    Public Property Credit() As Nullable(Of Decimal)
        Get
            Return m_Credit
        End Get
        Set(ByVal value As Nullable(Of Decimal))
            m_Credit = value 
        End Set
    End Property

    Public Property Glacier() As Nullable(Of Boolean)
        Get
            Return m_Glacier
        End Get
        Set(ByVal value As Nullable(Of Boolean))
            m_Glacier = value 
        End Set
    End Property

    Public Property NombreGlacier() As Nullable(Of Int32)
        Get
            Return m_NombreGlacier
        End Get
        Set(ByVal value As Nullable(Of Int32))
            m_NombreGlacier = value 
        End Set
    End Property

    Public Property MontantContrat() As Nullable(Of Decimal)
        Get
            Return m_MontantContrat
        End Get
        Set(ByVal value As Nullable(Of Decimal))
            m_MontantContrat = value 
        End Set
    End Property

    Public Property OrganisationID() As Nullable(Of Int32)
        Get
            Return m_OrganisationID
        End Get
        Set(ByVal value As Nullable(Of Int32))
            m_OrganisationID = value 
        End Set
    End Property

    Public Property LivreurID() As Nullable(Of Int32)
        Get
            Return m_LivreurID
        End Get
        Set(ByVal value As Nullable(Of Int32))
            m_LivreurID = value 
        End Set
    End Property

    Public Property MontantLivreur() As Nullable(Of Decimal)
        Get
            Return m_MontantLivreur
        End Get
        Set(ByVal value As Nullable(Of Decimal))
            m_MontantLivreur = value 
        End Set
    End Property

    Public Property MontantGlacierOrganisation() As Nullable(Of Decimal)
        Get
            Return m_MontantGlacierOrganisation
        End Get
        Set(ByVal value As Nullable(Of Decimal))
            m_MontantGlacierOrganisation = value 
        End Set
    End Property

    Public Property MontantGlacierLivreur() As Nullable(Of Decimal)
        Get
            Return m_MontantGlacierLivreur
        End Get
        Set(ByVal value As Nullable(Of Decimal))
            m_MontantGlacierLivreur = value 
        End Set
    End Property

    Public Property MontantLivreurFinal() As Nullable(Of Decimal)
        Get
            Return m_MontantLivreurFinal
        End Get
        Set(ByVal value As Nullable(Of Decimal))
            m_MontantLivreurFinal = value 
        End Set
    End Property

    Public Property MontantLivreurPharmaplus() As Nullable(Of Decimal)
        Get
            Return m_MontantLivreurPharmaplus
        End Get
        Set(ByVal value As Nullable(Of Decimal))
            m_MontantLivreurPharmaplus = value 
        End Set
    End Property

    Public Property ExtraKM() As Nullable(Of Decimal)
        Get
            Return m_ExtraKM
        End Get
        Set(ByVal value As Nullable(Of Decimal))
            m_ExtraKM = value 
        End Set
    End Property

    Public Property Echange() As Nullable(Of Boolean)
        Get
            Return m_Echange
        End Get
        Set(ByVal value As Nullable(Of Boolean))
            m_Echange = value 
        End Set
    End Property

    Public Property IDPeriode() As Nullable(Of Int32)
        Get
            Return m_IDPeriode
        End Get
        Set(ByVal value As Nullable(Of Int32))
            m_IDPeriode = value 
        End Set
    End Property

    Public Property ExtraLivreur() As Nullable(Of Decimal)
        Get
            Return m_ExtraLivreur
        End Get
        Set(ByVal value As Nullable(Of Decimal))
            m_ExtraLivreur = value 
        End Set
    End Property

    Public Property ExtraKMLivreur() As Nullable(Of Decimal)
        Get
            Return m_ExtraKMLivreur
        End Get
        Set(ByVal value As Nullable(Of Decimal))
            m_ExtraKMLivreur = value 
        End Set
    End Property

    Public Property IDSAmount() As Nullable(Of Decimal)
        Get
            Return m_IDSAmount
        End Get
        Set(ByVal value As Nullable(Of Decimal))
            m_IDSAmount = value 
        End Set
    End Property

    Public Property DateImport() As Nullable(Of DateTime)
        Get
            Return m_DateImport
        End Get
        Set(ByVal value As Nullable(Of DateTime))
            m_DateImport = value 
        End Set
    End Property

    Public Property IsMobilus() As Nullable(Of Boolean)
        Get
            Return m_IsMobilus
        End Get
        Set(ByVal value As Nullable(Of Boolean))
            m_IsMobilus = value 
        End Set
    End Property

    Public Property IDClient() As Nullable(Of Int32)
        Get
            Return m_IDClient
        End Get
        Set(ByVal value As Nullable(Of Int32))
            m_IDClient = value 
        End Set
    End Property

    Public Property IDClientAddresse() As Nullable(Of Int32)
        Get
            Return m_IDClientAddresse
        End Get
        Set(ByVal value As Nullable(Of Int32))
            m_IDClientAddresse = value 
        End Set
    End Property

End Class
 
