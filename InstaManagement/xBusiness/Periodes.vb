Public Class Periodes

    Private m_IDPeriode As Int32
    Private m_Periode As String
    Private m_DateFrom As Nullable(Of DateTime)
    Private m_DateTo As Nullable(Of DateTime)
    Private m_YearFrom As Nullable(Of Int32)
    Private m_YearTo As Nullable(Of Int32)
    Private m_IDTypePeriode As Nullable(Of Int32)

    Public Sub New()

    End Sub

    Public Property IDPeriode() As Int32
        Get
            Return m_IDPeriode
        End Get
        Set(ByVal value As Int32)
            m_IDPeriode = value 
        End Set
    End Property

    Public Property Periode() As String
        Get
            Return m_Periode
        End Get
        Set(ByVal value As String)
            m_Periode = value 
        End Set
    End Property

    Public Property DateFrom() As Nullable(Of DateTime)
        Get
            Return m_DateFrom
        End Get
        Set(ByVal value As Nullable(Of DateTime))
            m_DateFrom = value 
        End Set
    End Property

    Public Property DateTo() As Nullable(Of DateTime)
        Get
            Return m_DateTo
        End Get
        Set(ByVal value As Nullable(Of DateTime))
            m_DateTo = value 
        End Set
    End Property

    Public Property YearFrom() As Nullable(Of Int32)
        Get
            Return m_YearFrom
        End Get
        Set(ByVal value As Nullable(Of Int32))
            m_YearFrom = value 
        End Set
    End Property

    Public Property YearTo() As Nullable(Of Int32)
        Get
            Return m_YearTo
        End Get
        Set(ByVal value As Nullable(Of Int32))
            m_YearTo = value 
        End Set
    End Property

    Public Property IDTypePeriode() As Nullable(Of Int32)
        Get
            Return m_IDTypePeriode
        End Get
        Set(ByVal value As Nullable(Of Int32))
            m_IDTypePeriode = value 
        End Set
    End Property

End Class
 
