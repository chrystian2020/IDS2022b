Public Class Appointments

    Private m_UniqueID As Int32
    Private m_Type As Nullable(Of Int32)
    Private m_StartDate As Nullable(Of DateTime)
    Private m_EndDate As Nullable(Of DateTime)
    Private m_AllDay As Nullable(Of Boolean)
    Private m_Subject As String
    Private m_Location As String
    Private m_Description As String
    Private m_Status As Nullable(Of Int32)
    Private m_Label As Nullable(Of Int32)
    Private m_ResourceID As Nullable(Of Int32)
    Private m_ResourceIDs As String
    Private m_ReminderInfo As String
    Private m_RecurrenceInfo As String
    Private m_CustomField1 As String

    Public Sub New()

    End Sub

    Public Property UniqueID() As Int32
        Get
            Return m_UniqueID
        End Get
        Set(ByVal value As Int32)
            m_UniqueID = value 
        End Set
    End Property

    Public Property Type() As Nullable(Of Int32)
        Get
            Return m_Type
        End Get
        Set(ByVal value As Nullable(Of Int32))
            m_Type = value 
        End Set
    End Property

    Public Property StartDate() As Nullable(Of DateTime)
        Get
            Return m_StartDate
        End Get
        Set(ByVal value As Nullable(Of DateTime))
            m_StartDate = value 
        End Set
    End Property

    Public Property EndDate() As Nullable(Of DateTime)
        Get
            Return m_EndDate
        End Get
        Set(ByVal value As Nullable(Of DateTime))
            m_EndDate = value 
        End Set
    End Property

    Public Property AllDay() As Nullable(Of Boolean)
        Get
            Return m_AllDay
        End Get
        Set(ByVal value As Nullable(Of Boolean))
            m_AllDay = value 
        End Set
    End Property

    Public Property Subject() As String
        Get
            Return m_Subject
        End Get
        Set(ByVal value As String)
            m_Subject = value 
        End Set
    End Property

    Public Property Location() As String
        Get
            Return m_Location
        End Get
        Set(ByVal value As String)
            m_Location = value 
        End Set
    End Property

    Public Property Description() As String
        Get
            Return m_Description
        End Get
        Set(ByVal value As String)
            m_Description = value 
        End Set
    End Property

    Public Property Status() As Nullable(Of Int32)
        Get
            Return m_Status
        End Get
        Set(ByVal value As Nullable(Of Int32))
            m_Status = value 
        End Set
    End Property

    Public Property Label() As Nullable(Of Int32)
        Get
            Return m_Label
        End Get
        Set(ByVal value As Nullable(Of Int32))
            m_Label = value 
        End Set
    End Property

    Public Property ResourceID() As Nullable(Of Int32)
        Get
            Return m_ResourceID
        End Get
        Set(ByVal value As Nullable(Of Int32))
            m_ResourceID = value 
        End Set
    End Property

    Public Property ResourceIDs() As String
        Get
            Return m_ResourceIDs
        End Get
        Set(ByVal value As String)
            m_ResourceIDs = value 
        End Set
    End Property

    Public Property ReminderInfo() As String
        Get
            Return m_ReminderInfo
        End Get
        Set(ByVal value As String)
            m_ReminderInfo = value 
        End Set
    End Property

    Public Property RecurrenceInfo() As String
        Get
            Return m_RecurrenceInfo
        End Get
        Set(ByVal value As String)
            m_RecurrenceInfo = value 
        End Set
    End Property

    Public Property CustomField1() As String
        Get
            Return m_CustomField1
        End Get
        Set(ByVal value As String)
            m_CustomField1 = value 
        End Set
    End Property

End Class
 
