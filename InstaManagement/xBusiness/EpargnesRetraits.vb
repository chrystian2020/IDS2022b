Public Class EpargnesRetraits

    Private m_IDRetrait As Int32
    Private m_IDEpargne As Int32
    Private m_IDLivreur As Nullable(Of Int32)
    Private m_Livreur As String
    Private m_DateRetrait As Nullable(Of DateTime)
    Private m_Retrait As Nullable(Of Decimal)
    Private m_Periode As Nullable(Of Int32)
    Public Sub New()

    End Sub
    Public Property Periode() As Nullable(Of Int32)
        Get
            Return m_Periode
        End Get
        Set(ByVal value As Nullable(Of Int32))
            m_Periode = value
        End Set
    End Property
    Public Property IDRetrait() As Int32
        Get
            Return m_IDRetrait
        End Get
        Set(ByVal value As Int32)
            m_IDRetrait = value 
        End Set
    End Property

    Public Property IDEpargne() As Int32
        Get
            Return m_IDEpargne
        End Get
        Set(ByVal value As Int32)
            m_IDEpargne = value 
        End Set
    End Property

    Public Property IDLivreur() As Nullable(Of Int32)
        Get
            Return m_IDLivreur
        End Get
        Set(ByVal value As Nullable(Of Int32))
            m_IDLivreur = value 
        End Set
    End Property

    Public Property Livreur() As String
        Get
            Return m_Livreur
        End Get
        Set(ByVal value As String)
            m_Livreur = value 
        End Set
    End Property

    Public Property DateRetrait() As Nullable(Of DateTime)
        Get
            Return m_DateRetrait
        End Get
        Set(ByVal value As Nullable(Of DateTime))
            m_DateRetrait = value 
        End Set
    End Property

    Public Property Retrait() As Nullable(Of Decimal)
        Get
            Return m_Retrait
        End Get
        Set(ByVal value As Nullable(Of Decimal))
            m_Retrait = value 
        End Set
    End Property

End Class
 
