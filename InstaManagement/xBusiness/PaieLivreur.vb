Public Class PaieLivreur

    Private m_IDPaie As Int32
    Private m_Livreur As String
    Private m_Organisation As String
    Private m_MontantPaieNormal As Nullable(Of Decimal)
    Private m_MontantReajuste As Nullable(Of Decimal)
    Private m_Paie As Nullable(Of Decimal)
    Private m_PayDate As Nullable(Of DateTime)
    Private m_IDPeriode As Nullable(Of Int32)

    Public Sub New()

    End Sub

    Public Property IDPaie() As Int32
        Get
            Return m_IDPaie
        End Get
        Set(ByVal value As Int32)
            m_IDPaie = value 
        End Set
    End Property

    Public Property Livreur() As String
        Get
            Return m_Livreur
        End Get
        Set(ByVal value As String)
            m_Livreur = value 
        End Set
    End Property

    Public Property Organisation() As String
        Get
            Return m_Organisation
        End Get
        Set(ByVal value As String)
            m_Organisation = value 
        End Set
    End Property

    Public Property MontantPaieNormal() As Nullable(Of Decimal)
        Get
            Return m_MontantPaieNormal
        End Get
        Set(ByVal value As Nullable(Of Decimal))
            m_MontantPaieNormal = value 
        End Set
    End Property

    Public Property MontantReajuste() As Nullable(Of Decimal)
        Get
            Return m_MontantReajuste
        End Get
        Set(ByVal value As Nullable(Of Decimal))
            m_MontantReajuste = value 
        End Set
    End Property

    Public Property Paie() As Nullable(Of Decimal)
        Get
            Return m_Paie
        End Get
        Set(ByVal value As Nullable(Of Decimal))
            m_Paie = value 
        End Set
    End Property

    Public Property PayDate() As Nullable(Of DateTime)
        Get
            Return m_PayDate
        End Get
        Set(ByVal value As Nullable(Of DateTime))
            m_PayDate = value 
        End Set
    End Property

    Public Property IDPeriode() As Nullable(Of Int32)
        Get
            Return m_IDPeriode
        End Get
        Set(ByVal value As Nullable(Of Int32))
            m_IDPeriode = value 
        End Set
    End Property

End Class
 
