Public Class PrixFixe

    Private m_IDPrixFixe As Int32
    Private m_IDOrganisation As Nullable(Of Int32)
    Private m_IDLivreur As Nullable(Of Int32)
    Private m_Livreur As String
    Private m_Organisation As String
    Private m_MontantLivreur As Nullable(Of Decimal)
    Private m_IDPeriode As Nullable(Of Int32)

    Public Sub New()

    End Sub

    Public Property IDPrixFixe() As Int32
        Get
            Return m_IDPrixFixe
        End Get
        Set(ByVal value As Int32)
            m_IDPrixFixe = value 
        End Set
    End Property

    Public Property IDOrganisation() As Nullable(Of Int32)
        Get
            Return m_IDOrganisation
        End Get
        Set(ByVal value As Nullable(Of Int32))
            m_IDOrganisation = value 
        End Set
    End Property

    Public Property IDLivreur() As Nullable(Of Int32)
        Get
            Return m_IDLivreur
        End Get
        Set(ByVal value As Nullable(Of Int32))
            m_IDLivreur = value 
        End Set
    End Property

    Public Property Livreur() As String
        Get
            Return m_Livreur
        End Get
        Set(ByVal value As String)
            m_Livreur = value 
        End Set
    End Property

    Public Property Organisation() As String
        Get
            Return m_Organisation
        End Get
        Set(ByVal value As String)
            m_Organisation = value 
        End Set
    End Property

    Public Property MontantLivreur() As Nullable(Of Decimal)
        Get
            Return m_MontantLivreur
        End Get
        Set(ByVal value As Nullable(Of Decimal))
            m_MontantLivreur = value 
        End Set
    End Property

    Public Property IDPeriode() As Nullable(Of Int32)
        Get
            Return m_IDPeriode
        End Get
        Set(ByVal value As Nullable(Of Int32))
            m_IDPeriode = value 
        End Set
    End Property

End Class
 
