Public Class MaxPayable

    Private m_IDMaxPayable As Int32
    Private m_NombreLivraison As Nullable(Of Int32)
    Private m_MaxPayable As Nullable(Of Decimal)
    Private m_OrganisationID As Nullable(Of Int32)
    Private m_Organisation As String

    Public Sub New()

    End Sub

    Public Property IDMaxPayable() As Int32
        Get
            Return m_IDMaxPayable
        End Get
        Set(ByVal value As Int32)
            m_IDMaxPayable = value 
        End Set
    End Property

    Public Property NombreLivraison() As Nullable(Of Int32)
        Get
            Return m_NombreLivraison
        End Get
        Set(ByVal value As Nullable(Of Int32))
            m_NombreLivraison = value 
        End Set
    End Property

    Public Property MaxPayable() As Nullable(Of Decimal)
        Get
            Return m_MaxPayable
        End Get
        Set(ByVal value As Nullable(Of Decimal))
            m_MaxPayable = value 
        End Set
    End Property

    Public Property OrganisationID() As Nullable(Of Int32)
        Get
            Return m_OrganisationID
        End Get
        Set(ByVal value As Nullable(Of Int32))
            m_OrganisationID = value 
        End Set
    End Property

    Public Property Organisation() As String
        Get
            Return m_Organisation
        End Get
        Set(ByVal value As String)
            m_Organisation = value 
        End Set
    End Property

End Class
 
