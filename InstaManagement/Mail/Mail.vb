﻿Imports System.Net.Mail
Imports MailBee.SmtpMail

Public Class Mail

    Public Function SendEmail(vEmail As String, vSubject As String, vMessage As String, vEmail1 As String, vEmail2 As String, vEmail3 As String, pAttachment As String) As Boolean


        Try

            MailBee.Global.LicenseKey = "MN120-41758AFC57B80F0DB4000537FD74-3500"

            Dim sserver As String = go_Globals.Server.ToString.Trim
            Dim sUsername As String = go_Globals.Username.ToString.Trim
            Dim sPassword As String = go_Globals.Password.ToString.Trim
            Dim spUsername = Split(sUsername, "@")
            Dim mailer As New Smtp
            Dim server As New SmtpServer(sserver, sUsername, sPassword)


            server.Timeout = 1200000
            server.Port = go_Globals.Port
            server.SslMode = MailBee.Security.SslStartupMode.Manual
            mailer.SmtpServers.Add(server)

            mailer.Connect()
            mailer.Hello()
            mailer.Login()

            mailer.Message.From.AsString = "info@idslivraisonexpress.com"
            mailer.Message.To.AsString = vEmail
            mailer.Message.Subject = vSubject
            mailer.Message.BodyHtmlText = vMessage

            mailer.Message.Cc.AddFromString("idslivraisonexpress@gmail.com")

            If vEmail1.ToString.Trim <> "" Then
                mailer.Message.Cc.AddFromString(vEmail1)
            End If

            If vEmail2.ToString.Trim <> "" Then
                mailer.Message.Cc.AddFromString(vEmail3)
            End If

            If vEmail3.ToString.Trim <> "" Then
                mailer.Message.Cc.AddFromString(vEmail2)
            End If


            If Not IsNothing(pAttachment) Then
                mailer.AddAttachment(pAttachment)
            End If

            mailer.Send()

            Return True


        Catch ex As Exception

            Debug.Print(ex.Message, "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            MessageBox.Show(ex.Message, "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Return False


        End Try



    End Function



End Class
