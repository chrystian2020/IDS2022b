﻿Imports System.Data.SqlClient

Public Class GenerationCommissions

    Public Function generate_CommissionNumero() As String

        Dim id_tmp As String = ""
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String = "select top 1 IDCommission from Commission order by IDCommission desc "
        Dim dt As New DataTable
        Dim dr As DataRow
        Dim iID As Integer = 0
        Dim selectCommand As New SqlCommand(selectStatement, connection)



        selectCommand.CommandType = CommandType.Text

        Dim oInvoice As New Invoice
        Try

            connection.Open()

            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then

                dt.Load(reader)
                For Each dr In dt.Rows
                    iID = dr("IDCommission") + 1
                    id_tmp = DateTime.Now.Year.ToString & DateTime.Now.Month.ToString & "-" & iID.ToString
                Next
                Return id_tmp

            Else
                id_tmp = DateTime.Now.Year.ToString & DateTime.Now.Month.ToString & "-" & 1
                Return id_tmp
            End If
            reader.Close()



        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()

        End Try
        Return id_tmp
    End Function


End Class
