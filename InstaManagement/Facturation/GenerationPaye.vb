﻿Imports System.Data.SqlClient
Public Class GenerationPaye
    Dim oImportData As New ImportData
    Dim oCommissionKMLivreurData As New CommissionKMLivreurData
    Dim oLivreurData As New LivreursData
    Dim oOrganisationData As New OrganisationsData
    Dim oAdministrateurData As New AdminData
    Dim oMaxPayable As New MaxPayableData
    Dim oPaye As New Paye
    Dim oPayeData As New PayeData
    Dim oInvoiceLineData As New InvoiceLineData
    Dim oCommissionKMData As New CommissionKMData
    Dim oComissionKM As New CommissionKM
    Dim oCadeauLivreurData As New CadeauLivreurData
    Dim oCadeauLivreur As New CadeauLivreur
    Dim oCadeauOrganisationData As New CadeauOrganisationData
    Dim oCadeauOrganisation As New CadeauOrganisation
    Dim oPayeLineData As New PayeLineData


    Public Function generate_PayeNumero() As String

        Dim id_tmp As String = ""
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String = "select top 1 IDPaye from Paye order by IDPaye desc "
        Dim dt As New DataTable
        Dim dr As DataRow
        Dim iID As Integer = 0
        Dim selectCommand As New SqlCommand(selectStatement, connection)



        selectCommand.CommandType = CommandType.Text

        Dim oInvoice As New Invoice
        Try

            connection.Open()

            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then

                dt.Load(reader)
                For Each dr In dt.Rows
                    iID = dr("IDPaye") + 1
                    id_tmp = DateTime.Now.Year.ToString & DateTime.Now.Month.ToString & "-" & iID.ToString
                Next
                Return id_tmp

            Else
                id_tmp = DateTime.Now.Year.ToString & DateTime.Now.Month.ToString & "-" & 1
                Return id_tmp
            End If
            reader.Close()



        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()

        End Try
        Return id_tmp
    End Function
    Public Function CalculerPharmaplusMassicote(ByRef drOrganisation As DataRow, pDateFrom As DateTime, pDateTo As DateTime, ByRef drLivreur As DataRow, ByRef Opaye As Paye) As Boolean

        Try


            Dim dtImport As DataTable
            Dim dtCommissionKM As DataTable
            Dim drCommissionKM As DataRow
            Dim iOrganisationID As Integer = drOrganisation("OrganisationID")
            Dim iLivreurID As Integer = drLivreur("LivreurID")
            Dim dblDistance As Double = 0
            Dim dblMontant As Double
            Dim oImport As New Import
            Dim oPaieLigne As New PayeLine
            Dim dblTotalCourant As Double = 0
            Dim dblTotalCalculer As Double = 0
            Dim dblTotalFinal As Double = 0
            Dim dblTotalMaxPayable As Double = 0
            Dim dblGrandTotalPayableParJour As Double = 0
            Dim dblGrandTotalPayableParPeriode As Double = 0
            Dim iTotalGlacier As Integer = 0
            Dim dblTotalMontant As Double = 0
            Dim dblTotalExtra As Double = 0
            Dim iTotalExtra As Integer = 0
            Dim dblTotalKmExtra As Double = 0
            Dim dblKMextra As Double = 0
            Dim dblExtraCharge As Double = 0
            Dim iTotalLivraisons As Integer = 0
            Dim dblPrixDeBase As Double = 0



            '----------------------------------------------------------------------------------------
            ' Genere la colonne MontantLivreurFinal de l'import pour la facturation base sur la distance KM
            '----------------------------------------------------------------------------------------
            dtImport = oImportData.SelectAllFromDateNameLivreur(pDateFrom, pDateTo, iOrganisationID, iLivreurID)
            If dtImport.Rows.Count > 0 Then
                iTotalLivraisons = dtImport.Rows.Count
                For Each drImport In dtImport.Rows
                    iTotalLivraisons = dtImport.Rows.Count

                    dblDistance = If(IsDBNull(drImport("Distance")), 0, drImport("Distance"))
                    dblDistance = Math.Round(dblDistance, 2)
                    dtCommissionKM = oCommissionKMLivreurData.SelectAllByKM(iOrganisationID, dblDistance)

                    If dtCommissionKM.Rows.Count = 1 Then

                        For Each drCommissionKM In dtCommissionKM.Rows


                            dblKMextra = If(IsDBNull(drCommissionKM("PerKMNumber")), 0, drCommissionKM("PerKMNumber"))
                            dblExtraCharge = If(IsDBNull(drCommissionKM("ExtraChargeParKM")), 0, CType(drCommissionKM("ExtraChargeParKM"), Decimal?))
                            dblMontant = If(IsDBNull(drCommissionKM("Montant")), 0, CType(drCommissionKM("Montant"), Decimal?))
                            dblPrixDeBase = dblMontant

                            If (dblExtraCharge > 0 And dblKMextra > 0) And (dblDistance > drCommissionKM("KMTo")) Then
                                oImport.ImportID = drImport("ImportID")
                                dblTotalKmExtra = Math.Round(dblDistance) - drCommissionKM("KMTo")
                                dblTotalKmExtra = dblTotalKmExtra / dblKMextra
                                dblTotalKmExtra = Math.Round(dblTotalKmExtra)
                                oImport.ExtraKMLivreur = dblTotalKmExtra
                                oImport.ExtraLivreur = dblTotalKmExtra * dblExtraCharge
                                oImport.Montant = If(IsDBNull(dblMontant), 0, CType(dblMontant, Decimal?))
                                dblTotalCourant = dblTotalCourant + dblMontant
                            ElseIf (dblExtraCharge > 0 And dblKMextra = 0) And (dblDistance > drCommissionKM("KMFrom")) Then

                                oImport.ImportID = drImport("ImportID")

                                dblTotalKmExtra = Math.Round(dblDistance) - drCommissionKM("KMFrom")
                                oImport.ExtraKMLivreur = dblTotalKmExtra
                                oImport.ExtraLivreur = dblExtraCharge
                                oImport.Montant = If(IsDBNull(dblMontant), 0, CType(dblMontant, Decimal?))
                                dblTotalCourant = dblTotalCourant + dblMontant

                            Else
                                oImport.ImportID = drImport("ImportID")
                                oImport.ExtraLivreur = 0
                                oImport.ExtraKMLivreur = 0
                                oImport.Montant = If(IsDBNull(dblMontant), 0, CType(dblMontant, Decimal?))
                                dblTotalCourant = dblTotalCourant + dblMontant
                            End If

                            oImportData.UpdateAmountByIDLivreurPharmaplus(oImport)
                            dblMontant = 0
                        Next

                    Else
                        Dim smessage = "La distance " & dblDistance.ToString.Trim & " Ne retourne aucunes valeur pour l'organisation " & drOrganisation("Organisation").ToString & ", " & vbLf
                        smessage = smessage & " Veuillez verifier les configuration calcul Pharmaplus/Massicote"
                        MessageBox.Show(smessage)
                        Return False
                        Exit Function
                    End If
                Next
            End If



            '----------------------------------------------------------------------------------------
            ' Loop a travers chaque jour
            '----------------------------------------------------------------------------------------
            Dim dDateTo2 As DateTime
            Dim dDateFrom2 As DateTime
            Dim ICurrentDay As Integer = 0
            dblMontant = 0
            dblTotalCourant = 0
            Dim iTotalRows As Integer = 0
            dblGrandTotalPayableParJour = 0
            dblTotalMaxPayable = 0
            dblTotalFinal = 0
            dblTotalCourant = 0
            dblTotalCalculer = 0
            dblMontant = 0
            Dim iNumDays As Integer = pDateFrom.Subtract(pDateTo).Days
            For ICurrentDay = 0 To Math.Abs(iNumDays)
                dDateFrom2 = pDateFrom.AddDays(ICurrentDay)
                dDateTo2 = pDateTo.AddDays(iNumDays + ICurrentDay)


                dtImport = oImportData.SelectAllFromDateNameLivreur(dDateFrom2, dDateTo2, iOrganisationID, iLivreurID)
                If dtImport.Rows.Count > 0 Then
                    iTotalRows = dtImport.Rows.Count
                    For Each drImport In dtImport.Rows
                        dblMontant = If(IsDBNull(drImport("MontantLivreurFinal")), 0, CType(drImport("MontantLivreurFinal"), Decimal?))
                        dblTotalCourant = dblTotalCourant + dblMontant
                    Next
                Else
                    iTotalRows = 0
                End If

                If iTotalRows = 0 Then GoTo NEXTRECORD


                Dim iIndex = 1
                dtImport = oImportData.SelectAllFromDateNameLivreurPharmaplus(dDateFrom2, dDateTo2, iOrganisationID, iLivreurID)
                If dtImport.Rows.Count > 0 Then

                    For Each drImport In dtImport.Rows

                        oImport.ImportID = drImport("ImportID")

                        If iIndex = 1 Then
                            dblMontant = If(IsDBNull(drImport("MontantLivreurFinal")), 0, CType(drImport("MontantLivreurFinal"), Decimal?))
                            dblTotalCalculer = dblTotalCalculer + dblMontant
                            oImport.Montant = dblMontant
                            oImportData.UpdateAmountByIDLivreurPharmaplusApresCalcul(oImport)
                            dblMontant = 0
                        Else
                            dblMontant = If(IsDBNull(drOrganisation("PayeLivreur")), 0, CType(drOrganisation("PayeLivreur"), Decimal?))
                            dblTotalCalculer = dblTotalCalculer + dblMontant

                            oImport.Montant = dblMontant
                            oImportData.UpdateAmountByIDLivreurPharmaplusApresCalcul(oImport)
                            dblMontant = 0
                        End If
                        iIndex = iIndex + 1

                    Next
                End If

                dblTotalFinal = (dblTotalCourant + dblTotalCalculer)
                dblTotalFinal = dblTotalFinal / 2


                '----------------------------------------------------------------------------------------
                ' Selectionne Maximum payable
                '----------------------------------------------------------------------------------------
                Dim oMaximumPayableData As New MaxPayableData
                Dim dtMaximumPayable As DataTable
                Dim drMaximumPayable As DataRow

                dtMaximumPayable = oMaximumPayableData.SelectAllByQuantiteLivraison(dtImport.Rows.Count, drOrganisation("OrganisationID"))

                If dtMaximumPayable.Rows.Count > 0 Then
                    For Each drMaximumPayable In dtMaximumPayable.Rows
                        dblTotalMaxPayable = dblTotalMaxPayable + If(IsDBNull(drMaximumPayable("MaxPayable")), 0, CType(drMaximumPayable("MaxPayable"), Decimal?))
                    Next
                End If

                If dblTotalMaxPayable > dblTotalFinal Then
                    dblGrandTotalPayableParJour = dblTotalFinal
                Else
                    dblGrandTotalPayableParJour = dblTotalMaxPayable
                End If

                dblGrandTotalPayableParPeriode = dblGrandTotalPayableParPeriode + dblGrandTotalPayableParJour

                dblGrandTotalPayableParJour = 0
                dblTotalMaxPayable = 0
                dblTotalFinal = 0
                dblTotalCourant = 0
                dblTotalCalculer = 0
                dblMontant = 0
                iTotalRows = 0

NEXTRECORD:

            Next




            '----------------------------------------------------------------------------------------
            ' Update and calculate Import table Glaciers
            '----------------------------------------------------------------------------------------

            oImportData.UpdateImportGlaciersOrganisationLivreur(pDateFrom, pDateTo, iOrganisationID, iLivreurID, drOrganisation("MontantGlacierLivreur"))


            '----------------------------------------------------------------------------------------
            ' Cree le detail de la facture
            '----------------------------------------------------------------------------------------
            Dim dtImportC As DataTable
            Dim drImportC As DataRow
            Dim dblTotalKm As Double = 0
            Dim dblTotalGlacier As Double = 0
            Dim iTotalLivraison As Integer = 0

            dtImportC = oImportData.SelectDeliveryByDateLivreur(pDateFrom, pDateTo, iOrganisationID, iLivreurID)

            If dtImportC.Rows.Count > 0 Then
                For Each drImportC In dtImportC.Rows
                    dblMontant = dblMontant + If(IsDBNull(drImportC("TotalCommandeLivreur")), 0, CType(drImportC("TotalCommandeLivreur"), Decimal?))
                    dblTotalKm = dblTotalKm + If(IsDBNull(drImportC("TotalKM")), 0, CType(drImportC("TotalKM"), Double?))
                Next
            End If

            oPaieLigne.IDPaye = Opaye.IDPaye
            oPaieLigne.PayeNumber = Opaye.PayeNumber
            oPaieLigne.PayeDate = Now.ToShortDateString
            oPaieLigne.IDLivreur = drLivreur("LivreurID")
            oPaieLigne.Livreur = drLivreur("Livreur")
            oPaieLigne.OrganisationID = drOrganisation("OrganisationID")
            oPaieLigne.Organisation = drOrganisation("Organisation")
            oPaieLigne.Code = Nothing
            oPaieLigne.Km = Math.Round(dblTotalKm, 1)
            oPaieLigne.Description = iTotalLivraisons & " Livraisons"
            oPaieLigne.Unite = 1
            oPaieLigne.Prix = dblGrandTotalPayableParPeriode
            oPaieLigne.Montant = dblGrandTotalPayableParPeriode
            oPaieLigne.Credit = 0
            oPaieLigne.DateFrom = pDateFrom
            oPaieLigne.DateTo = pDateTo
            oPaieLigne.NombreGlacier = 0
            oPaieLigne.Glacier = 0
            oPaieLigne.IDPeriode = go_Globals.IDPeriode
            oPaieLigne.TypeAjout = 0
            oPaieLigne.Ajout = False
            oPaieLigne.IsCommissionVendeur = False
            oPayeLineData.Add(oPaieLigne)

            Return True

        Catch ex As Exception
            MessageBox.Show(ex.Message, "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Return False
        End Try




    End Function

    Public Function PrixFixe(ByRef drOrganisation As DataRow, pDateFrom As DateTime, pDateTo As DateTime, ByRef drLivreur As DataRow, ByRef Opaye As Paye) As Boolean

        Try


            Dim oImport As New Import
            Dim iTotalImport As Integer = 0
            Dim oInvoiceline As New InvoiceLine
            Dim iOrganisationID As Integer = drOrganisation("OrganisationID")
            Dim iLivreurID As Integer = drLivreur("LivreurID")
            Dim oPaieLigne As New PayeLine
            Dim dtImport As New DataTable
            Dim drimport As DataRow
            Dim dblMontantLivreur As Double = 0
            '--------------------------------------------
            ' Prend les valeurs pour les Credit par livraison et les Glaciers
            '--------------------------------------------
            Dim dblPrixFixe As Double = If(IsDBNull(drOrganisation("PayeLivreur")), 0, CType(drOrganisation("PayeLivreur"), Decimal?))
            Dim dblPrixFixeParPorte As Double = If(IsDBNull(drOrganisation("MontantParPorteLivreur")), 0, CType(drOrganisation("MontantParPorteLivreur"), Decimal?))
            '--------------------------------------------
            ' Genere la colonne montant de l'import pour la facturation
            '--------------------------------------------
            oImport.OrganisationID = iOrganisationID
            oImport.Organisation = drOrganisation("Organisation").ToString.Trim




            ' Genere la colonne MontantLivreurFinal de l'import pour la facturation base sur la distance KM
            '----------------------------------------------------------------------------------------
            dtImport = oImportData.SelectAllFromDateNameLivreur(pDateFrom, pDateTo, iOrganisationID, iLivreurID)
            If dtImport.Rows.Count > 0 Then
                For Each drimport In dtImport.Rows

                    If drimport("IsMobilus") = False Then

                        Dim dblNouveauMontant As Double = 0
                        dblNouveauMontant = If(IsDBNull(drimport("MontantLivreur")), 0, CType(drimport("MontantLivreur"), Decimal?))

                        If dblPrixFixe <> dblNouveauMontant And dblNouveauMontant > 0 Then
                            oImport.Montant = If(IsDBNull(dblNouveauMontant), 0, CType(dblNouveauMontant, Decimal?))
                        Else
                            oImport.Montant = If(IsDBNull(dblPrixFixe), 0, CType(dblPrixFixe, Decimal?))

                        End If

                    Else

                        oImport.Montant = dblPrixFixe


                    End If

                    Dim ssql = "UPDATE Import SET MontantLivreur = " & oImport.Montant & " WHERE ImportID = " & drimport("ImportID")
                    SysTemGlobals.ExecuteSQL(ssql)




                Next
            End If


            'oImportData.UpdateAmountByNameLivreur(pDateFrom, pDateTo, oImport, iLivreurID)



            '----------------------------------------------------------------------------------------
            ' Cree le detail de la paye pour les livraisons
            '----------------------------------------------------------------------------------------
            '& " Count(*)                        As TotalLivraison, " _
            '& " Round(SUM(distance), 1)         As TotalKM, " _
            '& " SUM(MontantLivreur)             As TotalCommandeLivreur, " _
            '& " Cast(heureprep As Date)         As PreparationDate " _

            Dim dtImportC As DataTable
            Dim drImportC As DataRow
            Dim dblTotalKm As Double = 0
            Dim dblMontant As Double = 0
            Dim dblTotalGlacier As Double = 0
            Dim iTotalLivraison As Integer = 0
            dtImportC = oImportData.SelectDeliveryByDateLivreur(pDateFrom, pDateTo, iOrganisationID, iLivreurID)

            If dtImportC.Rows.Count > 0 Then
                For Each drImportC In dtImportC.Rows
                    dblMontant = dblMontant + If(IsDBNull(drImportC("TotalCommandeLivreur")), 0, CType(drImportC("TotalCommandeLivreur"), Decimal?))
                    dblTotalKm = dblTotalKm + If(IsDBNull(drImportC("TotalKM")), 0, CType(drImportC("TotalKM"), Double?))
                    iTotalLivraison = iTotalLivraison + If(IsDBNull(drImportC("TotalLivraison")), 0, CType(drImportC("TotalLivraison"), Int32?))
                Next
            End If

            oPaieLigne.IDPaye = Opaye.IDPaye
            oPaieLigne.PayeNumber = Opaye.PayeNumber
            oPaieLigne.PayeDate = Now.ToShortDateString
            oPaieLigne.IDLivreur = drLivreur("LivreurID")
            oPaieLigne.Livreur = drLivreur("Livreur").ToString.Trim
            oPaieLigne.OrganisationID = drOrganisation("OrganisationID")
            oPaieLigne.Organisation = drOrganisation("Organisation")
            oPaieLigne.Code = Nothing
            oPaieLigne.Km = Math.Round(dblTotalKm, 1)
            oPaieLigne.Description = iTotalLivraison.ToString & " Livraisons"
            oPaieLigne.Unite = 1
            oPaieLigne.Prix = dblMontant
            oPaieLigne.Montant = dblMontant
            oPaieLigne.Credit = 0
            oPaieLigne.DateFrom = pDateFrom
            oPaieLigne.DateTo = pDateTo
            oPaieLigne.NombreGlacier = 0
            oPaieLigne.Glacier = 0
            oPaieLigne.IDPeriode = go_Globals.IDPeriode
            oPaieLigne.TypeAjout = 0
            oPaieLigne.Ajout = False
            oPaieLigne.IsCommissionVendeur = False
            oPayeLineData.Add(oPaieLigne)


            Return True

        Catch ex As Exception
            MessageBox.Show(ex.Message, "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Return False
        End Try

    End Function

    Public Function ParResidence(ByRef drOrganisation As DataRow, pDateFrom As DateTime, pDateTo As DateTime, ByRef drLivreur As DataRow, ByRef Opaye As Paye) As Boolean

        Try


            Dim oImport As New Import
            Dim iTotalImport As Integer = 0
            Dim oInvoiceline As New InvoiceLine
            Dim iOrganisationID As Integer = drOrganisation("OrganisationID")
            Dim iLivreurID As Integer = drLivreur("LivreurID")
            Dim oPaieLigne As New PayeLine
            Dim dtImport As New DataTable
            Dim dblMontantLivreur As Double = 0

            Dim dblTotalPaye As Double = 0
            Dim dblTotalLivraison As Integer = 0


            '--------------------------------------------
            ' Prend les valeurs pour les Credit par livraison et les Glaciers
            '--------------------------------------------
            Dim dblPrixFixe As Double = If(IsDBNull(drOrganisation("PayeLivreur")), 0, CType(drOrganisation("PayeLivreur"), Decimal?))
            Dim dblPrixFixeParPorte As Double = If(IsDBNull(drOrganisation("MontantParPorteLivreur")), 0, CType(drOrganisation("MontantParPorteLivreur"), Decimal?))


            'If drLivreur("LivreurID") = 59 Then

            '    Debug.Print("fff")


            'End If


            '--------------------------------------------
            ' Genere la colonne montant de l'import pour la facturation
            '--------------------------------------------
            oImport.OrganisationID = iOrganisationID
            oImport.Organisation = drOrganisation("Organisation").ToString.Trim



            'Loop a travers chaque jours de la periode.

            Dim SD As DateTime = pDateFrom.ToShortDateString
            Dim ED As DateTime = pDateTo.ToShortDateString
            SD = SD.ToString.Replace("12:00", "12:01")

            Do While SD <= ED


                Dim dtImportR As DataTable
                Dim drImportR As DataRow
                Dim dtImportD As DataTable

                dtImportR = oImportData.SelectDistincResidences(SD, iOrganisationID)
                If dtImportR.Rows.Count > 0 Then

                    'loop a travers les residences
                    For Each drImportR In dtImportR.Rows


                        ' SELECTIONNE TOUS LES BUILDING QUI ONT UNE RESIDENCE
                        dtImportD = oImportData.SelectAllFromDateOrganisationResidenceLivreur(SD, iOrganisationID, drImportR("IDResidence"), drLivreur("LivreurID"))

                        If dtImportD.Rows.Count > 1 Then

                            dblTotalLivraison = dblTotalLivraison + dtImportD.Rows.Count
                            dblTotalPaye = dblTotalPaye + dblPrixFixe + (dtImportD.Rows.Count * dblPrixFixeParPorte)
                        ElseIf dtImportD.Rows.Count = 1 Then


                            dblTotalPaye = dblTotalPaye + dblPrixFixe
                            dblTotalLivraison = dblTotalLivraison + 1


                        End If
                    Next
                End If


                ' SELECTIONNE TOUS LES BUILDING QUI ONT  PAS UNE RESIDENCE (prix fixe)
                Dim dtImportR2 As DataTable

                dtImportR2 = oImportData.SelectAllFromDateOrganisationNOResidenceLivreur(SD, iOrganisationID, drLivreur("LivreurID"))
                If dtImportR2.Rows.Count > 0 Then
                    dblTotalPaye = dblTotalPaye + dblPrixFixe * dtImportR2.Rows.Count
                    dblTotalLivraison = dblTotalLivraison + dtImportR2.Rows.Count

                End If



                SD = SD.AddDays(1)

            Loop

            oPaieLigne.IDPaye = Opaye.IDPaye
            oPaieLigne.PayeNumber = Opaye.PayeNumber
            oPaieLigne.PayeDate = Now.ToShortDateString
            oPaieLigne.IDLivreur = drLivreur("LivreurID")
            oPaieLigne.Livreur = drLivreur("Livreur").ToString.Trim
            oPaieLigne.OrganisationID = drOrganisation("OrganisationID")
            oPaieLigne.Organisation = drOrganisation("Organisation")
            oPaieLigne.Code = Nothing
            oPaieLigne.Km = 0
            oPaieLigne.Description = dblTotalLivraison.ToString & " Livraisons incluant résidences"
            oPaieLigne.Unite = 1
            oPaieLigne.Prix = dblTotalPaye
            oPaieLigne.Montant = dblTotalPaye
            oPaieLigne.Credit = 0
            oPaieLigne.DateFrom = pDateFrom
            oPaieLigne.DateTo = pDateTo
            oPaieLigne.NombreGlacier = 0
            oPaieLigne.Glacier = 0
            oPaieLigne.IDPeriode = go_Globals.IDPeriode
            oPaieLigne.TypeAjout = 0
            oPaieLigne.Ajout = False
            oPaieLigne.IsCommissionVendeur = False
            oPayeLineData.Add(oPaieLigne)

            Return True

        Catch ex As Exception
            MessageBox.Show(ex.Message, "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Return False
        End Try

    End Function

    Public Function ParKilometrageXtraHeure(ByRef drOrganisation As DataRow, pDateFrom As DateTime, pDateTo As DateTime, ByRef drLivreur As DataRow, ByRef Opaye As Paye) As Boolean

        Try


            Dim iOrganisationID As Integer = drOrganisation("OrganisationID")
            Dim iLivreurID As Integer = drLivreur("LivreurID")
            Dim dblTotalKmExtra2 As Double = 0
            Dim dblTotalMontantExtra As Double = 0
            Dim dblKMextra As Double = 0
            Dim dblExtraCharge As Double = 0
            Dim dblMontant As Double = 0
            Dim dblDistance As Double
            Dim dtCommissionKM As DataTable
            Dim dtImport As DataTable
            Dim dtHeureDepart As DateTime
            Dim oImport As New Import
            Dim oPaieLigne As New PayeLine
            Dim dblTotalMontant As Double = 0
            Dim dblTotalExtra As Double = 0
            Dim iTotalExtra As Integer = 0
            Dim dblTotalKmExtra As Double = 0
            Dim iTotalGlacier As Integer = 0

            If iOrganisationID = 8 Then

                Debug.Print("")

            End If

                '----------------------------------------------------------------------------------------
                ' Genere la colonne montant la table import pour la facturation
                '----------------------------------------------------------------------------------------
                dtImport = oImportData.SelectAllFromDateNameLivreur(pDateFrom, pDateTo, iOrganisationID, iLivreurID)

            If dtImport.Rows.Count > 0 Then

                For Each drImport In dtImport.Rows


                    dtHeureDepart = If(IsDBNull(drImport("HeureLivraison")), Nothing, CType(drImport("HeureLivraison"), DateTime?))
                    dblDistance = If(IsDBNull(drImport("Distance")), 0, drImport("Distance"))
                    dblDistance = Math.Round(dblDistance, 2)
                    dtCommissionKM = oCommissionKMLivreurData.SelectAllByTimeKM(iOrganisationID, dtHeureDepart, dblDistance)

                    If dtCommissionKM.Rows.Count = 1 Then
                        For Each drCommissionKM In dtCommissionKM.Rows
                            dblKMextra = If(IsDBNull(drCommissionKM("PerKMNumber")), 0, drCommissionKM("PerKMNumber"))
                            dblExtraCharge = If(IsDBNull(drCommissionKM("ExtraChargeParKM")), 0, CType(drCommissionKM("ExtraChargeParKM"), Decimal?))
                            dblMontant = If(IsDBNull(drCommissionKM("Montant")), 0, CType(drCommissionKM("Montant"), Decimal?))

                            If (dblExtraCharge > 0 And dblKMextra > 0) And (dblDistance > Math.Round(drCommissionKM("KMFrom"))) Then
                                oImport.ImportID = drImport("ImportID")
                                dblTotalKmExtra = Math.Round(dblDistance) - Math.Round(drCommissionKM("KMFrom"))
                                dblTotalKmExtra = dblTotalKmExtra / dblKMextra
                                dblTotalKmExtra = Math.Round(dblTotalKmExtra)
                                oImport.ExtraKMLivreur = dblTotalKmExtra
                                oImport.ExtraLivreur = dblTotalKmExtra * dblExtraCharge
                                oImport.MontantLivreur = If(IsDBNull(dblMontant), 0, CType(dblMontant, Decimal?))
                            ElseIf (dblExtraCharge > 0 And dblKMextra = 0) And (dblDistance > Math.Round(drCommissionKM("KMFrom"))) Then
                                oImport.ImportID = drImport("ImportID")
                                dblTotalKmExtra = Math.Round(dblDistance) - Math.Round(drCommissionKM("KMFrom"))
                                oImport.ExtraKMLivreur = dblTotalKmExtra
                                oImport.ExtraLivreur = dblExtraCharge
                                oImport.MontantLivreur = If(IsDBNull(dblMontant), 0, CType(dblMontant, Decimal?))
                            Else
                                oImport.ImportID = drImport("ImportID")
                                oImport.ExtraLivreur = 0
                                oImport.ExtraKMLivreur = 0
                                oImport.MontantLivreur = If(IsDBNull(dblMontant), 0, CType(dblMontant, Decimal?))
                            End If

                            oImportData.UpdateAmountXtraExtraKMByIDLivreur(oImport)

                            dblKMextra = 0
                            dblExtraCharge = 0
                            dblMontant = 0
                        Next
                    Else
                        Dim smessage = "La distance " & dblDistance.ToString.Trim & " Ne retourne aucunes valeur pour l'organisation " & drOrganisation("Organisation").ToString & ", " & vbLf
                        smessage = smessage & " Veuillez verifier les configuration calcul par kilometrage + Extra/Heure"
                        MessageBox.Show(smessage)
                        Return False
                        Exit Function
                    End If

                Next
            End If

            '----------------------------------------------------------------------------------------
            ' Cree le detail de la paye pour les livraisons
            '----------------------------------------------------------------------------------------
            '& " Count(*)                        As TotalLivraison, " _
            '& " Round(SUM(distance), 1)         As TotalKM, " _
            '& " SUM(MontantLivreur)             As TotalCommandeLivreur, " _
            '& " Cast(heureprep As Date)         As PreparationDate " _

            Dim dtImportC As DataTable
            Dim drImportC As DataRow
            Dim dblTotalKm As Double = 0
            Dim dblTotalGlacier As Double = 0
            Dim iTotalLivraison As Integer = 0
            dtImportC = oImportData.SelectDeliveryByDateLivreur(pDateFrom, pDateTo, iOrganisationID, iLivreurID)

            If dtImportC.Rows.Count > 0 Then
                For Each drImportC In dtImportC.Rows
                    dblMontant = dblMontant + If(IsDBNull(drImportC("TotalCommandeLivreur")), 0, CType(drImportC("TotalCommandeLivreur"), Decimal?))
                    dblTotalKm = dblTotalKm + If(IsDBNull(drImportC("TotalKM")), 0, CType(drImportC("TotalKM"), Double?))
                    iTotalLivraison = iTotalLivraison + If(IsDBNull(drImportC("TotalLivraison")), 0, CType(drImportC("TotalLivraison"), Int32?))
                Next
            End If


            oPaieLigne.IDPaye = Opaye.IDPaye
            oPaieLigne.PayeNumber = Opaye.PayeNumber
            oPaieLigne.PayeDate = Now.ToShortDateString
            oPaieLigne.IDLivreur = drLivreur("LivreurID")
            oPaieLigne.Livreur = drLivreur("Livreur")
            oPaieLigne.OrganisationID = drOrganisation("OrganisationID")
            oPaieLigne.Organisation = drOrganisation("Organisation")
            oPaieLigne.Code = Nothing
            oPaieLigne.Km = Math.Round(dblTotalKm, 1)
            oPaieLigne.Description = iTotalLivraison.ToString & " Livraisons"
            oPaieLigne.Unite = 1
            oPaieLigne.Prix = dblMontant
            oPaieLigne.Montant = dblMontant
            oPaieLigne.Credit = 0
            oPaieLigne.DateFrom = pDateFrom
            oPaieLigne.DateTo = pDateTo
            oPaieLigne.NombreGlacier = 0
            oPaieLigne.Glacier = 0
            oPaieLigne.IDPeriode = go_Globals.IDPeriode
            oPaieLigne.TypeAjout = 0
            oPaieLigne.Ajout = False
            oPaieLigne.IsCommissionVendeur = False
            oPayeLineData.Add(oPaieLigne)

            Return True

        Catch ex As Exception
            MessageBox.Show(ex.Message, "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Return False
        End Try

    End Function
    Public Function ParKilometrage(ByRef drOrganisation As DataRow, pDateFrom As DateTime, pDateTo As DateTime, ByRef drLivreur As DataRow, ByRef Opaye As Paye) As Boolean


        Try


            Dim iOrganisationID As Integer = drOrganisation("OrganisationID")
            Dim iLivreurID As Integer = drLivreur("LivreurID")
            Dim dtImport As DataTable
            Dim dtCommissionKM As DataTable
            Dim drCommissionKM As DataRow
            Dim dblDistance As Double = 0
            Dim dtHeureDepart As DateTime
            Dim dblMontant As Double
            Dim oImport As New Import
            Dim oPaieLigne As New PayeLine
            Dim oInvoiceline As New InvoiceLine
            Dim dblTotalMontant As Double = 0
            Dim dblTotalExtra As Double = 0
            Dim iTotalExtra As Integer = 0
            Dim dblTotalKmExtra As Double = 0
            Dim iTotalGlacier As Integer = 0

            '----------------------------------------------------------------------------------------
            ' Genere la colonne montant de l'import pour la facturation base sur la distance KM
            '----------------------------------------------------------------------------------------
            dtImport = oImportData.SelectAllFromDateNameLivreur(pDateFrom, pDateTo, iOrganisationID, iLivreurID)
            If dtImport.Rows.Count > 0 Then
                For Each drImport In dtImport.Rows


                    dtHeureDepart = If(IsDBNull(drImport("HeureLivraison")), Nothing, CType(drImport("HeureLivraison"), DateTime?))
                    dblDistance = If(IsDBNull(drImport("Distance")), 0, drImport("Distance"))
                    dblDistance = Math.Round(dblDistance, 2)
                    dtCommissionKM = oCommissionKMLivreurData.SelectAllByKM(iOrganisationID, dblDistance)

                    If dtCommissionKM.Rows.Count = 1 Then
                        For Each drCommissionKM In dtCommissionKM.Rows
                            dblMontant = If(IsDBNull(drCommissionKM("Montant")), 0, CType(drCommissionKM("Montant"), Decimal?))
                            oImport.ImportID = drImport("ImportID")
                            oImport.MontantLivreur = dblMontant
                            oImport.ExtraLivreur = 0
                            oImport.ExtraKMLivreur = 0
                            oImportData.UpdateAmountXtraExtraKMByIDLivreur(oImport)
                            dblMontant = 0
                        Next
                    Else

                        Dim smessage = "La distance " & dblDistance.ToString.Trim & " Ne retourne aucunes valeur pour l'organisation " & drOrganisation("Organisation").ToString & ", " & vbLf
                        smessage = smessage & " Veuillez verifier les configuration calcul par kilometrage"
                        MessageBox.Show(smessage)
                        Return False
                        Exit Function

                    End If
                Next
            End If



            '----------------------------------------------------------------------------------------
            ' Cree le detail de la facture pour les livraisons
            '----------------------------------------------------------------------------------------
            '& " Count(*)                        As TotalLivraison, " _
            '& " Round(SUM(distance), 1)         As TotalKM, " _
            '& " SUM(MontantLivreur)             As TotalCommandeLivreur, " _
            '& " Cast(heureprep As Date)         As PreparationDate " _

            Dim dtImportC As DataTable
            Dim drImportC As DataRow
            Dim dblTotalKm As Double = 0
            Dim dblTotalGlacier As Double = 0
            Dim iTotalLivraison As Integer = 0
            dtImportC = oImportData.SelectDeliveryByDateLivreur(pDateFrom, pDateTo, iOrganisationID, iLivreurID)

            If dtImportC.Rows.Count > 0 Then
                For Each drImportC In dtImportC.Rows
                    dblMontant = dblMontant + If(IsDBNull(drImportC("TotalCommandeLivreur")), 0, CType(drImportC("TotalCommandeLivreur"), Decimal?))
                    dblTotalKm = dblTotalKm + If(IsDBNull(drImportC("TotalKM")), 0, CType(drImportC("TotalKM"), Double?))
                    iTotalLivraison = iTotalLivraison + If(IsDBNull(drImportC("TotalLivraison")), 0, CType(drImportC("TotalLivraison"), Int32?))
                Next
            End If


            oPaieLigne.IDPaye = Opaye.IDPaye
            oPaieLigne.PayeNumber = Opaye.PayeNumber
            oPaieLigne.PayeDate = Now.ToShortDateString
            oPaieLigne.IDLivreur = drLivreur("LivreurID")
            oPaieLigne.Livreur = drLivreur("Livreur")
            oPaieLigne.OrganisationID = drOrganisation("OrganisationID")
            oPaieLigne.Organisation = drOrganisation("Organisation")
            oPaieLigne.Code = Nothing
            oPaieLigne.Km = Math.Round(dblTotalKm, 1)
            oPaieLigne.Description = iTotalLivraison & " Livraisons"
            oPaieLigne.Unite = 1
            oPaieLigne.Prix = dblMontant
            oPaieLigne.Montant = dblMontant
            oPaieLigne.Credit = 0
            oPaieLigne.DateFrom = pDateFrom
            oPaieLigne.DateTo = pDateTo
            oPaieLigne.NombreGlacier = 0
            oPaieLigne.Glacier = 0
            oPaieLigne.IDPeriode = go_Globals.IDPeriode
            oPaieLigne.TypeAjout = 0
            oPaieLigne.Ajout = False
            oPaieLigne.IsCommissionVendeur = False
            oPayeLineData.Add(oPaieLigne)
            Return True

        Catch ex As Exception
            MessageBox.Show(ex.Message, "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Return False
        End Try


    End Function


    Public Function ParKilometrageXtra(ByRef drOrganisation As DataRow, pDateFrom As DateTime, pDateTo As DateTime, ByRef drLivreur As DataRow, ByRef Opaye As Paye) As Boolean
        Try


            Dim oImport As New Import
            Dim dtImport As DataTable
            Dim drImport As DataRow
            Dim dtCommissionKM As DataTable
            Dim drCommissionKM As DataRow
            Dim iOrganisationID As Integer = drOrganisation("OrganisationID")
            Dim iLivreurID As Integer = drLivreur("LivreurID")
            Dim dtHeureDepart As DateTime = Nothing
            Dim dblDistance As Double = 0
            Dim dblMontant As Double = 0
            Dim dblKMextra As Double = 0
            Dim dblExtraCharge As Double = 0
            Dim dblTotalKmExtra As Double
            Dim dblTotalKmExtra2 As Double
            Dim dblTotalMontantExtra As Double
            Dim iTotalImport As Integer
            Dim oPaieLigne As New PayeLine
            Dim iTotalGlacier As Integer = 0

            '----------------------------------------------------------------------------------------
            ' Genere la colonne montant de l'import pour la facturation
            '----------------------------------------------------------------------------------------
            dtImport = oImportData.SelectAllFromDateNameLivreur(pDateFrom, pDateTo, iOrganisationID, iLivreurID)

            If dtImport.Rows.Count > 0 Then
                iTotalImport = dtImport.Rows.Count
                For Each drImport In dtImport.Rows

                    oImport.ImportID = drImport("ImportID")


                    dtHeureDepart = If(IsDBNull(drImport("HeureLivraison")), Nothing, CType(drImport("HeureLivraison"), DateTime?))
                    dblDistance = If(IsDBNull(drImport("Distance")), 0, drImport("Distance"))
                    dblDistance = Math.Round(dblDistance, 2)

                    dtCommissionKM = oCommissionKMLivreurData.SelectAllByKM(iOrganisationID, dblDistance)

                    If dtCommissionKM.Rows.Count = 1 Then
                        For Each drCommissionKM In dtCommissionKM.Rows
                            dblKMextra = If(IsDBNull(drCommissionKM("PerKMNumber")), 0, drCommissionKM("PerKMNumber"))
                            dblExtraCharge = If(IsDBNull(drCommissionKM("ExtraChargeParKM")), 0, CType(drCommissionKM("ExtraChargeParKM"), Decimal?))
                            dblMontant = If(IsDBNull(drCommissionKM("Montant")), 0, CType(drCommissionKM("Montant"), Decimal?))


                            If (dblExtraCharge > 0 And dblKMextra > 0) And (dblDistance > Math.Round(drCommissionKM("KMFrom"))) Then

                                dblTotalKmExtra = Math.Round(dblDistance) - Math.Round(drCommissionKM("KMFrom"))
                                dblTotalKmExtra = dblTotalKmExtra / dblKMextra
                                dblTotalKmExtra = Math.Round(dblTotalKmExtra)
                                oImport.ExtraKMLivreur = dblTotalKmExtra
                                oImport.ExtraLivreur = dblTotalKmExtra * dblExtraCharge
                                oImport.MontantLivreur = If(IsDBNull(dblMontant), 0, CType(dblMontant, Decimal?))
                            ElseIf (dblExtraCharge > 0 And dblKMextra = 0) And (dblDistance > drCommissionKM("KMFrom")) Then

                                dblTotalKmExtra = Math.Round(dblDistance) - Math.Round(drCommissionKM("KMFrom"))
                                oImport.ExtraKMLivreur = dblTotalKmExtra
                                oImport.ExtraLivreur = dblExtraCharge
                                oImport.MontantLivreur = If(IsDBNull(dblMontant), 0, CType(dblMontant, Decimal?))
                            Else

                                oImport.ExtraLivreur = 0
                                oImport.ExtraKMLivreur = 0
                                oImport.MontantLivreur = If(IsDBNull(dblMontant), 0, CType(dblMontant, Decimal?))
                            End If

                            oImportData.UpdateAmountXtraExtraKMByIDLivreur(oImport)
                            dblTotalKmExtra = 0
                            dblTotalKmExtra2 = 0
                            dblTotalMontantExtra = 0
                            dblKMextra = 0
                            dblExtraCharge = 0
                            dblMontant = 0
                        Next
                    Else
                        Dim smessage = "La distance " & dblDistance.ToString.Trim & " Ne retourne aucunes valeur pour l'organisation " & drOrganisation("Organisation").ToString & ", " & vbLf
                        smessage = smessage & " Veuillez verifier les configuration calcul par kilometrage + Extra"
                        MessageBox.Show(smessage)
                        Return False
                        Exit Function
                    End If

                Next
            End If





            '----------------------------------------------------------------------------------------
            ' Cree le detail de la paye pour les livraisons
            '----------------------------------------------------------------------------------------
            '& " Count(*)                        As TotalLivraison, " _
            '& " Round(SUM(distance), 1)         As TotalKM, " _
            '& " SUM(MontantLivreur)             As TotalCommandeLivreur, " _
            '& " Cast(heureprep As Date)         As PreparationDate " _

            Dim dtImportC As DataTable
            Dim drImportC As DataRow
            Dim dblTotalKm As Double = 0
            Dim dblTotalGlacier As Double = 0
            Dim iTotalLivraison As Integer = 0
            dtImportC = oImportData.SelectDeliveryByDateLivreur(pDateFrom, pDateTo, iOrganisationID, iLivreurID)

            If dtImportC.Rows.Count > 0 Then
                For Each drImportC In dtImportC.Rows
                    dblMontant = dblMontant + If(IsDBNull(drImportC("TotalCommandeLivreur")), 0, CType(drImportC("TotalCommandeLivreur"), Decimal?))
                    dblTotalKm = dblTotalKm + If(IsDBNull(drImportC("TotalKM")), 0, CType(drImportC("TotalKM"), Double?))
                    iTotalLivraison = iTotalLivraison + If(IsDBNull(drImportC("TotalLivraison")), 0, CType(drImportC("TotalLivraison"), Int32?))
                Next
            End If


            oPaieLigne.IDPaye = Opaye.IDPaye
            oPaieLigne.PayeNumber = Opaye.PayeNumber
            oPaieLigne.PayeDate = Now.ToShortDateString
            oPaieLigne.IDLivreur = drLivreur("LivreurID")
            oPaieLigne.Livreur = drLivreur("Livreur")
            oPaieLigne.OrganisationID = drOrganisation("OrganisationID")
            oPaieLigne.Organisation = drOrganisation("Organisation")
            oPaieLigne.Code = Nothing
            oPaieLigne.Km = Math.Round(dblTotalKm, 1)
            oPaieLigne.Description = iTotalLivraison & " Livraisons"
            oPaieLigne.Unite = 1
            oPaieLigne.Prix = dblMontant
            oPaieLigne.Montant = dblMontant
            oPaieLigne.Credit = 0
            oPaieLigne.DateFrom = pDateFrom
            oPaieLigne.DateTo = pDateTo
            oPaieLigne.NombreGlacier = 0
            oPaieLigne.Glacier = 0
            oPaieLigne.IDPeriode = go_Globals.IDPeriode
            oPaieLigne.TypeAjout = 0
            oPaieLigne.Ajout = False
            oPaieLigne.IsCommissionVendeur = False
            oPayeLineData.Add(oPaieLigne)

            Return True

        Catch ex As Exception
            MessageBox.Show(ex.Message, "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Return False
        End Try

    End Function



    Public Function ParNombreKMFixeExtra(ByRef drOrganisation As DataRow, pDateFrom As DateTime, pDateTo As DateTime, ByRef drLivreur As DataRow, ByRef Opaye As Paye) As Boolean

        Try


            Dim oImport As New Import
            Dim dtImport As DataTable
            Dim drImport As DataRow
            Dim dtCommissionKM As DataTable
            Dim drCommissionKM As DataRow
            Dim dtHeureDepart As DateTime = Nothing
            Dim dblDistance As Double = 0
            Dim dblMontant As Double = 0
            Dim dblKMextra As Double = 0
            Dim dblExtraCharge As Double = 0
            Dim dblTotalKmExtra As Double = 0
            Dim dblTotalKmExtra2 As Double = 0
            Dim dblTotalMontantExtra As Double = 0
            Dim iTotalImport As Integer = 0
            Dim oInvoiceline As New InvoiceLine
            Dim iOrganisationID As Integer = drOrganisation("OrganisationID")
            Dim iLivreurID As Integer = drLivreur("LivreurID")
            Dim oPaieLigne As New PayeLine


            '----------------------------------------------------------------------------------------
            ' Genere la colonne montant de l'import pour la facturation
            '----------------------------------------------------------------------------------------
            dtImport = oImportData.SelectAllFromDateNameLivreur(pDateFrom, pDateTo, iOrganisationID, iLivreurID)

            If dtImport.Rows.Count > 0 Then

                iTotalImport = dtImport.Rows.Count

                For Each drImport In dtImport.Rows

                    dtHeureDepart = If(IsDBNull(drImport("HeureLivraison")), Nothing, CType(drImport("HeureLivraison"), DateTime?))
                    dblDistance = If(IsDBNull(drImport("Distance")), 0, drImport("Distance"))
                    dblDistance = Math.Round(dblDistance, 2)
                    dtCommissionKM = oCommissionKMLivreurData.SelectAllByKM(iOrganisationID, dblDistance)
                    If dtCommissionKM.Rows.Count = 1 Then
                        For Each drCommissionKM In dtCommissionKM.Rows
                            dblKMextra = If(IsDBNull(drCommissionKM("PerKMNumber")), 0, drCommissionKM("PerKMNumber"))
                            dblExtraCharge = If(IsDBNull(drCommissionKM("ExtraChargeParKM")), 0, CType(drCommissionKM("ExtraChargeParKM"), Decimal?))
                            dblMontant = If(IsDBNull(drCommissionKM("Montant")), 0, CType(drCommissionKM("Montant"), Decimal?))

                            If (dblExtraCharge > 0 And dblKMextra > 0) And (dblDistance > Math.Round(drCommissionKM("KMFrom"))) Then
                                oImport.ImportID = drImport("ImportID")
                                dblTotalKmExtra = Math.Round(dblDistance) - Math.Round(drCommissionKM("KMFrom"))
                                dblTotalKmExtra = dblTotalKmExtra / dblKMextra
                                dblTotalKmExtra = Math.Round(dblTotalKmExtra)
                                oImport.ExtraKMLivreur = dblTotalKmExtra
                                oImport.ExtraLivreur = dblTotalKmExtra * dblExtraCharge
                                oImport.MontantLivreur = If(IsDBNull(dblMontant), 0, CType(dblMontant, Decimal?))
                            ElseIf (dblExtraCharge > 0 And dblKMextra = 0) And (dblDistance > Math.Round(drCommissionKM("KMFrom"))) Then
                                oImport.ImportID = drImport("ImportID")
                                dblTotalKmExtra = Math.Round(dblDistance) - Math.Round(drCommissionKM("KMFrom"))
                                oImport.ExtraKMLivreur = dblTotalKmExtra
                                oImport.ExtraLivreur = dblExtraCharge
                                oImport.MontantLivreur = If(IsDBNull(dblMontant), 0, CType(dblMontant, Decimal?))
                            Else
                                oImport.ImportID = drImport("ImportID")
                                oImport.ExtraLivreur = 0
                                oImport.ExtraKMLivreur = 0
                                oImport.MontantLivreur = If(IsDBNull(dblMontant), 0, CType(dblMontant, Decimal?))
                            End If

                            oImportData.UpdateAmountXtraExtraKMByIDLivreur(oImport)

                            dblKMextra = 0
                            dblExtraCharge = 0
                            dblMontant = 0
                        Next
                    Else
                        Dim smessage = "La distance " & dblDistance.ToString.Trim & " Ne retourne aucunes valeur pour l'organisation " & drOrganisation("Organisation").ToString & ", " & vbLf
                        smessage = smessage & " Veuillez verifier les configuration calcul par kilometrage fixe + Extra"
                        MessageBox.Show(smessage)
                        Return False
                        Exit Function
                    End If

                Next
            End If


            '----------------------------------------------------------------------------------------
            ' Cree le detail de la paye pour les livraisons
            '----------------------------------------------------------------------------------------
            '& " Count(*)                        As TotalLivraison, " _
            '& " Round(SUM(distance), 1)         As TotalKM, " _
            '& " SUM(MontantLivreur)             As TotalCommandeLivreur, " _
            '& " Cast(heureprep As Date)         As PreparationDate " _

            Dim dtImportC As DataTable
            Dim drImportC As DataRow
            Dim dblTotalKm As Double = 0
            Dim dblTotalGlacier As Double = 0
            Dim iTotalLivraison As Integer = 0
            dtImportC = oImportData.SelectDeliveryByDateLivreur(pDateFrom, pDateTo, iOrganisationID, iLivreurID)

            If dtImportC.Rows.Count > 0 Then
                For Each drImportC In dtImportC.Rows
                    dblMontant = dblMontant + If(IsDBNull(drImportC("TotalCommandeLivreur")), 0, CType(drImportC("TotalCommandeLivreur"), Decimal?))
                    dblTotalKm = dblTotalKm + If(IsDBNull(drImportC("TotalKM")), 0, CType(drImportC("TotalKM"), Double?))
                    iTotalLivraison = iTotalLivraison + If(IsDBNull(drImportC("TotalLivraison")), 0, CType(drImportC("TotalLivraison"), Int32?))
                Next
            End If


            oPaieLigne.IDPaye = Opaye.IDPaye
            oPaieLigne.PayeNumber = Opaye.PayeNumber
            oPaieLigne.PayeDate = Now.ToShortDateString
            oPaieLigne.IDLivreur = drLivreur("LivreurID")
            oPaieLigne.Livreur = drLivreur("Livreur")
            oPaieLigne.OrganisationID = drOrganisation("OrganisationID")
            oPaieLigne.Organisation = drOrganisation("Organisation")
            oPaieLigne.Code = Nothing
            oPaieLigne.Km = Math.Round(dblTotalKm, 1)
            oPaieLigne.Description = iTotalLivraison.ToString & " Livraisons"
            oPaieLigne.Unite = 1
            oPaieLigne.Prix = dblMontant
            oPaieLigne.Montant = dblMontant
            oPaieLigne.Credit = 0
            oPaieLigne.DateFrom = pDateFrom
            oPaieLigne.DateTo = pDateTo
            oPaieLigne.NombreGlacier = 0
            oPaieLigne.Glacier = 0
            oPaieLigne.IDPeriode = go_Globals.IDPeriode
            oPaieLigne.TypeAjout = 0
            oPaieLigne.Ajout = False
            oPaieLigne.IsCommissionVendeur = False
            oPayeLineData.Add(oPaieLigne)


            Return True

        Catch ex As Exception
            MessageBox.Show(ex.Message, "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Return False
        End Try


    End Function


    Public Function ParJour(ByRef drOrganisation As DataRow, pDateFrom As DateTime, pDateTo As DateTime, ByRef drLivreur As DataRow, ByRef Opaye As Paye) As Boolean

        Try


            Dim iOrganisationID As Integer = drOrganisation("OrganisationID")
            Dim iLivreurID As Integer = drLivreur("LivreurID")
            Dim dblMontant As Double = 0

            Dim dblTotalMontant As Double = 0
            Dim dtHeureDepart As DateTime = Opaye.PayeDate
            Dim dtHeureFin As DateTime = Nothing
            Dim oHoraireJourData As New HoraireJourData
            Dim dtHoraireJour As DataTable


            ' select tous les horaires pour cet organisation
            dtHoraireJour = oHoraireJourData.SelectAllByIDOrganisationIDLivreurParJour(iOrganisationID, iLivreurID)

            If dtHoraireJour.Rows.Count > 0 Then
                For Each drHoraireJour In dtHoraireJour.Rows

                    Dim oPaieLigne As New PayeLine
                    Dim oInvoiceline2 As New InvoiceLine
                    Dim oLivreur As New livreurs
                    Dim dblHeures As Double = 0

                    oLivreur.LivreurID = drHoraireJour("IDLivreur")
                    oLivreur = oLivreurData.Select_Record(oLivreur)

                    oPaieLigne.IDPaye = Opaye.IDPaye
                    oPaieLigne.PayeNumber = Opaye.PayeNumber.ToString.Trim
                    oPaieLigne.PayeDate = dtHeureDepart.ToShortDateString
                    oPaieLigne.IDLivreur = oLivreur.LivreurID
                    oPaieLigne.Livreur = oLivreur.Livreur.ToString.Trim
                    oPaieLigne.OrganisationID = drOrganisation("OrganisationID").ToString.Trim
                    oPaieLigne.Organisation = drOrganisation("Organisation").ToString.Trim & "/Jour"
                    oPaieLigne.Code = Nothing
                    oPaieLigne.Km = 0
                    oPaieLigne.Description = drOrganisation("Organisation").ToString.Trim & "/Jour"
                    oPaieLigne.Unite = Convert.ToDouble(drHoraireJour("TotalDay"))
                    oPaieLigne.Prix = Convert.ToDouble(drHoraireJour("MontantLivreur"))
                    oPaieLigne.Montant = Convert.ToDouble(drHoraireJour("MontantTotal"))
                    oPaieLigne.Credit = 0
                    oPaieLigne.DateFrom = pDateFrom
                    oPaieLigne.DateTo = pDateTo
                    oPaieLigne.NombreGlacier = 0
                    oPaieLigne.Glacier = 0
                    oPaieLigne.IDPeriode = go_Globals.IDPeriode
                    oPaieLigne.TypeAjout = 0
                    oPaieLigne.Ajout = False
                    oPaieLigne.IsCommissionVendeur = False
                    oPayeLineData.Add(oPaieLigne)

                Next

            End If










            Return True

        Catch ex As Exception
            MessageBox.Show(ex.Message, "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Return False
        End Try


    End Function
    Public Function ParHeure(ByRef drOrganisation As DataRow, pDateFrom As DateTime, pDateTo As DateTime, ByRef drLivreur As DataRow, ByRef Opaye As Paye) As Boolean


        Try


            Dim iOrganisationID As Integer = drOrganisation("OrganisationID")
            Dim iLivreurID As Integer = drLivreur("LivreurID")
            Dim dblMontant As Double = 0

            Dim dblTotalMontant As Double = 0
            Dim dtHeureDepart As DateTime = Opaye.PayeDate
            Dim dtHeureFin As DateTime = Nothing
            Dim oHoraireData As New HoraireData
            Dim dtHoraire As DataTable
            Dim drHoraire As DataRow



            ' select tous les horaires pour cet organisation
            dtHoraire = oHoraireData.SelectAllByIDOrganisationIDLivreurParHeure(iOrganisationID, iLivreurID)

            If dtHoraire.Rows.Count > 0 Then
                For Each drHoraire In dtHoraire.Rows

                    Dim oPaieLigne As New PayeLine
                    Dim oInvoiceline2 As New InvoiceLine
                    Dim oLivreur As New livreurs
                    Dim dblHeures As Double = 0

                    oLivreur.LivreurID = drHoraire("IDLivreur")
                    oLivreur = oLivreurData.Select_Record(oLivreur)

                    oPaieLigne.IDPaye = Opaye.IDPaye
                    oPaieLigne.PayeNumber = Opaye.PayeNumber.ToString.Trim
                    oPaieLigne.PayeDate = dtHeureDepart.ToShortDateString
                    oPaieLigne.IDLivreur = oLivreur.LivreurID
                    oPaieLigne.Livreur = oLivreur.Livreur.ToString.Trim
                    oPaieLigne.OrganisationID = drOrganisation("OrganisationID").ToString.Trim
                    oPaieLigne.Organisation = drOrganisation("Organisation").ToString.Trim & "/Heure"
                    oPaieLigne.Code = Nothing
                    oPaieLigne.Km = 0
                    oPaieLigne.Description = drOrganisation("Organisation").ToString.Trim & "/Heure"
                    oPaieLigne.Unite = Convert.ToDouble(drHoraire("TotalHour"))
                    oPaieLigne.Prix = Convert.ToDouble(drHoraire("MontantLivreur"))
                    oPaieLigne.Montant = Convert.ToDouble(drHoraire("MontantTotal"))
                    oPaieLigne.Credit = 0
                    oPaieLigne.DateFrom = pDateFrom
                    oPaieLigne.DateTo = pDateTo
                    oPaieLigne.NombreGlacier = 0
                    oPaieLigne.Glacier = 0
                    oPaieLigne.IDPeriode = go_Globals.IDPeriode
                    oPaieLigne.TypeAjout = 0
                    oPaieLigne.Ajout = False
                    oPaieLigne.IsCommissionVendeur = False
                    oPayeLineData.Add(oPaieLigne)

                Next

            End If










            Return True

        Catch ex As Exception
            MessageBox.Show(ex.Message, "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Return False
        End Try


    End Function




End Class
