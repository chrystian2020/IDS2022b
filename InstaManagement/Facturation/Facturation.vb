﻿Imports System.Data.SqlClient
Imports GemBox.Spreadsheet
Imports MailBee
Imports MailBee.DnsMX
Imports MailBee.Mime
Imports MailBee.SmtpMail
Imports MailBee.Pop3Mail
Imports MailBee.ImapMail
Imports MailBee.Security
Imports MailBee.AntiSpam
Imports MailBee.AddressCheck
Imports MailBee.Outlook

Public Class Facturation


    Dim oImportData As New ImportData
    Dim oComissionKMData As New CommissionKMData
    Dim oLivreurData As New LivreursData
    Dim oOrganisationData As New OrganisationsData
    Dim oAdministrateurData As New AdminData
    Dim oMaxPayable As New MaxPayableData
    Dim oInvoice As New Invoice
    Dim oInvoiceData As New InvoiceData
    Dim oInvoiceLineData As New InvoiceLineData
    Dim oCommissionKMData As New CommissionKMData
    Dim oComissionKM As New CommissionKM
    Dim oCadeauLivreurData As New CadeauLivreurData
    Dim oCadeauLivreur As New CadeauLivreur
    Dim oCadeauOrganisationData As New CadeauOrganisationData
    Dim oCadeauOrganisation As New CadeauOrganisation


    Public Function generate_inv() As String

        Dim id_tmp As String = ""
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String = "select top 1 IDInvoice from Invoice order by IDInvoice desc "
        Dim dt As New DataTable
        Dim dr As DataRow
        Dim iID As Integer = 0
        Dim selectCommand As New SqlCommand(selectStatement, connection)



        selectCommand.CommandType = CommandType.Text

        Dim oInvoice As New Invoice
        Try

            connection.Open()

            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then

                dt.Load(reader)
                For Each dr In dt.Rows
                    iID = dr("IDInvoice") + 1
                    id_tmp = DateTime.Now.Year.ToString & DateTime.Now.Month.ToString & "-" & iID.ToString
                Next
                Return id_tmp

            Else
                id_tmp = DateTime.Now.Year.ToString & DateTime.Now.Month.ToString & "-" & 1
                Return id_tmp
            End If
            reader.Close()



        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()

        End Try
        Return id_tmp
    End Function
    Public Function CalculerPharmaplusMassicote(ByRef drOrganisation As DataRow, pDateFrom As DateTime, pDateTo As DateTime) As Boolean

        Try


            Dim oImport As New Import
            Dim dtImport As DataTable
            Dim drImport As DataRow
            Dim dtCommissionKM As DataTable
            Dim drCommissionKM As DataRow
            Dim dtHeureDepart As DateTime = Nothing
            Dim dblDistance As Double = 0
            Dim dblMontant As Double = 0
            Dim dblKMextra As Double = 0
            Dim dblExtraCharge As Double = 0
            Dim dblExtraChargeGrandTotal As Double = 0
            Dim iTotalImport As Integer
            Dim oInvoiceline As New InvoiceLine
            Dim iOrganisationID As Integer
            Dim dblTotalMontant As Double = 0
            Dim dblTotalExtra As Double = 0
            Dim iTotalExtra As Integer = 0
            Dim dblTotalKmExtra As Double = 0
            Dim iTotalLivraisons As Integer = 0
            Dim dblExtraGrandTotal As Double = 0
            Dim iTotalGlacier As Integer = 0
            Dim iGlacierGrandTotal As Integer = 0
            Dim dblGlacierGrandTotal As Double = 0
            Dim dblTotalKM As Double = 0
            Dim dblTotalExtraKM As Double = 0
            Dim dblTotalGlacier As Double = 0
            Dim dtImportC As DataTable
            Dim drImportC As DataRow
            Dim dblExtraKMGrandTotal As Double = 0
            Dim iExtraGrandTotal As Integer = 0
            Dim dblPrixCourant As Double = 0
            Dim dtInvoice As DataTable
            Dim drInvoice As DataRow


            iOrganisationID = drOrganisation("OrganisationID")
            '--------------------------------------------
            ' Cree l'entree de la facture
            '--------------------------------------------


            ' check if invoice exist before recreating it
            dtInvoice = oInvoiceData.SelectAllByPeriodOrganisationID(go_Globals.IDPeriode, iOrganisationID)

            If dtInvoice.Rows.Count > 0 Then
                For Each drInvoice In dtInvoice.Rows
                    oInvoiceline.IDInvoice = drInvoice("IDInvoice")
                    oInvoice.IDInvoice = drInvoice("IDInvoice")
                    oInvoice.InvoiceNumber = drInvoice("InvoiceNumber")
                    oInvoice.IDPeriode = drInvoice("IDPeriode")
                    oInvoice.InvoiceDate = pDateTo.ToShortDateString
                    oInvoice.Paye = False
                    oInvoiceData.UpdateInvoiceDate(oInvoice, oInvoice)
                Next
            Else
                oInvoice.InvoiceDate = pDateTo.ToShortDateString
                oInvoice.IDPeriode = go_Globals.IDPeriode
                oInvoice.DateFrom = pDateFrom
                oInvoice.DateTo = pDateTo
                oInvoice.OrganisationID = iOrganisationID
                oInvoice.Organisation = drOrganisation("Organisation")
                oInvoice.InvoiceNumber = Me.generate_inv()
                oInvoice.Amount = 0
                oInvoice.TVQ = 0
                oInvoice.TPS = 0
                oInvoice.Paye = False

                oInvoiceline.IDInvoice = oInvoiceData.Add(oInvoice)
            End If





            '----------------------------------------------------------------------------------------
            ' Prend les valeurs pour les Credit par livraison et les Glaciers
            '----------------------------------------------------------------------------------------
            Dim iQuantiteLivraison As Integer = If(IsDBNull(drOrganisation("QuantiteLivraison")), 0, CType(drOrganisation("QuantiteLivraison"), Int32?))
            Dim dblCreditParLivraison As Double = If(IsDBNull(drOrganisation("CreditParLivraison")), 0, CType(drOrganisation("CreditParLivraison"), Decimal?))
            Dim dblPrixGlacier As Double = If(IsDBNull(drOrganisation("MontantGlacierOrganisation")), 0, CType(drOrganisation("MontantGlacierOrganisation"), Decimal?))

            Dim dblTotalCredits As Double = 0
            Dim dblTotalDebits As Double = 0




            '----------------------------------------------------------------------------------------
            ' Genere la colonne montant de l'import pour la facturation
            '----------------------------------------------------------------------------------------
            dtImport = oImportData.SelectAllFromDateName(pDateFrom, pDateTo, iOrganisationID)

            If dtImport.Rows.Count > 0 Then
                iTotalImport = dtImport.Rows.Count
                iTotalLivraisons = dtImport.Rows.Count
                For Each drImport In dtImport.Rows
                    oImport.ImportID = drImport("ImportID")

                    iTotalGlacier = iTotalGlacier + If(IsDBNull(drImport("NombreGlacier")), 0, CType(drImport("NombreGlacier"), Int32?))


                    If iTotalGlacier > 0 Then
                        iGlacierGrandTotal = iGlacierGrandTotal + iTotalGlacier
                        oImport.MontantGlacierOrganisation = iTotalGlacier * If(IsDBNull(drOrganisation("MontantGlacierOrganisation")), 0, CType(drOrganisation("MontantGlacierOrganisation"), Decimal?))
                        dblGlacierGrandTotal = dblGlacierGrandTotal + oImport.MontantGlacierOrganisation
                    Else
                        oImport.MontantGlacierOrganisation = 0
                    End If


                    dtHeureDepart = If(IsDBNull(drImport("HeureLivraison")), Nothing, CType(drImport("HeureLivraison"), DateTime?))
                    dblDistance = If(IsDBNull(drImport("Distance")), 0, drImport("Distance"))
                    dblDistance = Math.Round(dblDistance, 2)

                    dtCommissionKM = oCommissionKMData.SelectAllByKM(iOrganisationID, dblDistance)
                    If dtCommissionKM.Rows.Count = 1 Then
                        For Each drCommissionKM In dtCommissionKM.Rows


                            dblKMextra = If(IsDBNull(drCommissionKM("PerKMNumber")), 0, drCommissionKM("PerKMNumber"))
                            dblExtraCharge = If(IsDBNull(drCommissionKM("ExtraChargeParKM")), 0, CType(drCommissionKM("ExtraChargeParKM"), Decimal?))
                            If dblExtraCharge > 0 Then
                                dblExtraChargeGrandTotal = dblExtraCharge
                            End If
                            dblMontant = If(IsDBNull(drCommissionKM("Montant")), 0, CType(drCommissionKM("Montant"), Decimal?))

                            If (dblExtraCharge > 0 And dblKMextra > 0) And (dblDistance > drCommissionKM("KMTo")) Then
                                oImport.ImportID = drImport("ImportID")
                                dblTotalKmExtra = Math.Round(dblDistance) - drCommissionKM("KMTo")
                                dblTotalKmExtra = dblTotalKmExtra / dblKMextra
                                dblTotalKmExtra = Math.Round(dblTotalKmExtra)
                                oImport.ExtraKM = dblTotalKmExtra
                                dblExtraKMGrandTotal = dblExtraKMGrandTotal + oImport.ExtraKM
                                oImport.Extra = dblTotalKmExtra * dblExtraCharge
                                dblExtraGrandTotal = dblExtraGrandTotal + oImport.Extra
                                iExtraGrandTotal = iExtraGrandTotal + 1
                                oImport.Montant = If(IsDBNull(dblMontant), 0, CType(dblMontant, Decimal?))



                            ElseIf (dblExtraCharge > 0 And dblKMextra = 0) And (dblDistance > drCommissionKM("KMFrom")) Then

                                oImport.ImportID = drImport("ImportID")

                                dblTotalKmExtra = Math.Round(dblDistance) - drCommissionKM("KMFrom")
                                oImport.ExtraKM = dblTotalKmExtra
                                dblExtraKMGrandTotal = dblExtraKMGrandTotal + oImport.ExtraKM
                                oImport.Extra = dblExtraCharge
                                dblExtraGrandTotal = dblExtraGrandTotal + oImport.Extra
                                iExtraGrandTotal = iExtraGrandTotal + 1
                                oImport.Montant = If(IsDBNull(dblMontant), 0, CType(dblMontant, Decimal?))

                            Else
                                oImport.ImportID = drImport("ImportID")
                                oImport.Extra = 0
                                oImport.ExtraKM = 0
                                oImport.Montant = If(IsDBNull(dblMontant), 0, CType(dblMontant, Decimal?))
                                dblPrixCourant = oImport.Montant
                            End If

                            oImportData.UpdateAmountExtraGlacierKMByID(oImport)
                            dblKMextra = 0
                            dblExtraCharge = 0
                            dblMontant = 0
                            iTotalGlacier = 0
                            dblTotalKmExtra = 0
                            dblDistance = 0
                        Next
                    Else

                        Dim smessage = "La distance " & dblDistance.ToString.Trim & " Ne retourne aucunes valeur pour l'organisation " & drOrganisation("Organisation").ToString & ", " & vbLf
                        smessage = smessage & " Veuillez verifier les configuration calcul Pharmaplus/Massicote"
                        MessageBox.Show(smessage)
                        Return False

                    End If
                Next
            End If

            '----------------------------------------------------------------------------------------
            ' Cree le detail de la facture
            '
            '
            '
            ' Extra
            '----------------------------------------------------------------------------------------


            dtImportC = oImportData.SelectDeliveryByDate(pDateFrom, pDateTo, iOrganisationID)


            If iExtraGrandTotal > 0 Then
                oInvoiceline.OrganisationID = iOrganisationID
                oInvoiceline.Organisation = drOrganisation("Organisation").ToString.Trim
                oInvoiceline.IDInvoice = oInvoiceline.IDInvoice
                oInvoiceline.InvoiceNumber = oInvoice.InvoiceNumber
                oInvoiceline.InvoiceDate = oInvoice.InvoiceDate
                oInvoiceline.DisplayDate = oInvoice.InvoiceDate
                oInvoiceline.DateFrom = pDateFrom
                oInvoiceline.DateTo = pDateTo
                oInvoiceline.Code = Nothing
                oInvoiceline.Credit = 0
                oInvoiceline.Debit = 0
                oInvoiceline.Description = "Extras"
                oInvoiceline.Glacier = 0
                oInvoiceline.NombreGlacier = 0
                oInvoiceline.Km = dblExtraKMGrandTotal
                oInvoiceline.KmExtra = dblExtraKMGrandTotal
                oInvoiceline.Unite = iExtraGrandTotal
                oInvoiceline.Prix = dblExtraChargeGrandTotal
                oInvoiceline.Montant = dblExtraGrandTotal
                oInvoiceline.IDPeriode = oInvoice.IDPeriode
                oInvoiceline.Ajout = False
                oInvoiceline.TypeAjout = 0

                oInvoiceLineData.Add(oInvoiceline)

            End If

            '----------------------------------------------------------------------------------------
            ' Livraisons
            '----------------------------------------------------------------------------------------
            Dim dPreparationDate As Date
            For Each drImportC In dtImportC.Rows



                oInvoiceline.OrganisationID = iOrganisationID
                oInvoiceline.Organisation = drOrganisation("Organisation")
                oInvoiceline.InvoiceNumber = oInvoice.InvoiceNumber
                oInvoiceline.IDInvoice = oInvoiceline.IDInvoice
                oInvoiceline.InvoiceDate = oInvoice.InvoiceDate
                dPreparationDate = drImportC("PreparationDate")
                oInvoiceline.DisplayDate = dPreparationDate.ToShortDateString
                oInvoiceline.DateFrom = pDateFrom
                oInvoiceline.DateTo = pDateTo
                oInvoiceline.Code = Nothing
                oInvoiceline.Debit = If(IsDBNull(drImportC("totalDebit")), 0, CType(drImportC("totalDebit"), Decimal?))
                oInvoiceline.Description = drImportC("TotalLivraison").ToString & " Liv"
                oInvoiceline.Glacier = 0
                oInvoiceline.NombreGlacier = 0
                oInvoiceline.Km = If(IsDBNull(drImportC("TotalKM")), 0, CType(drImportC("TotalKM"), Double?))
                oInvoiceline.Unite = 1
                oInvoiceline.Prix = If(IsDBNull(drImportC("TotalCommande")), 0, CType(drImportC("TotalCommande"), Decimal?))
                oInvoiceline.Montant = If(IsDBNull(drImportC("TotalCommande")), 0, CType(drImportC("TotalCommande"), Decimal?))
                oInvoiceline.IDPeriode = oInvoice.IDPeriode
                oInvoiceline.IDInvoice = oInvoiceline.IDInvoice
                oInvoiceline.TypeAjout = 0
                oInvoiceline.Ajout = False

                oInvoiceLineData.Add(oInvoiceline)

            Next



            '----------------------------------------------------------------------------------------
            ' Rabais par nb. livraisons
            '----------------------------------------------------------------------------------------
            Dim dblCreditParLivraisonTotal As Double = 0
            If (iTotalImport >= iQuantiteLivraison) And iQuantiteLivraison > 0 Then
                dblCreditParLivraisonTotal = iTotalImport * dblCreditParLivraison
                oInvoiceline.OrganisationID = iOrganisationID
                oInvoiceline.Organisation = drOrganisation("Organisation").ToString.Trim
                oInvoiceline.InvoiceNumber = oInvoice.InvoiceNumber
                oInvoiceline.InvoiceDate = oInvoice.InvoiceDate
                oInvoiceline.DisplayDate = oInvoice.InvoiceDate
                oInvoiceline.DateFrom = pDateFrom
                oInvoiceline.DateTo = pDateTo
                oInvoiceline.Code = Nothing
                oInvoiceline.Credit = 0
                oInvoiceline.Debit = 0
                oInvoiceline.Description = "Rabais par nb. livraisons"
                oInvoiceline.Glacier = 0
                oInvoiceline.NombreGlacier = 0
                oInvoiceline.Km = 0
                oInvoiceline.KmExtra = 0
                oInvoiceline.Unite = iTotalImport
                oInvoiceline.Prix = dblCreditParLivraison
                oInvoiceline.Montant = dblCreditParLivraisonTotal
                oInvoiceline.IDPeriode = oInvoice.IDPeriode
                oInvoiceline.TypeAjout = 2
                oInvoiceline.Ajout = False
                oInvoiceline.IDInvoice = oInvoiceline.IDInvoice
                oInvoiceLineData.Add(oInvoiceline)
            End If



            '----------------------------------------------------------------------------------------
            ' Cadeau IDS
            '----------------------------------------------------------------------------------------
            Dim dblTotalCadeau As Double = 0

            dblTotalCadeau = oCadeauOrganisationData.SelectTotalAmountByOrganisation(pDateFrom, pDateTo, iOrganisationID)

            If dblTotalCadeau > 0 Then

                oInvoiceline.OrganisationID = iOrganisationID
                oInvoiceline.Organisation = drOrganisation("Organisation").ToString.Trim
                oInvoiceline.InvoiceNumber = oInvoice.InvoiceNumber
                oInvoiceline.InvoiceDate = oInvoice.InvoiceDate
                oInvoiceline.DisplayDate = oInvoice.InvoiceDate
                oInvoiceline.DateFrom = pDateFrom
                oInvoiceline.DateTo = pDateTo
                oInvoiceline.Code = Nothing
                oInvoiceline.Credit = 0
                oInvoiceline.Debit = 0
                oInvoiceline.Description = "Cadeau IDS"
                oInvoiceline.Glacier = 0
                oInvoiceline.NombreGlacier = 0
                oInvoiceline.Km = 0
                oInvoiceline.KmExtra = 0
                oInvoiceline.Unite = 1
                oInvoiceline.Prix = dblTotalCadeau
                oInvoiceline.Montant = dblTotalCadeau
                oInvoiceline.IDPeriode = oInvoice.IDPeriode
                oInvoiceline.IDInvoice = oInvoiceline.IDInvoice
                oInvoiceline.TypeAjout = 2
                oInvoiceline.Ajout = False
                oInvoiceLineData.Add(oInvoiceline)
            End If



            Dim dblMobilus As Double = If(IsDBNull(drOrganisation("MontantMobilus")), 0, CType(drOrganisation("MontantMobilus"), Decimal?))

            If dblMobilus > 0 Then

                oInvoiceline.OrganisationID = iOrganisationID
                oInvoiceline.Organisation = drOrganisation("Organisation").ToString.Trim
                oInvoiceline.InvoiceNumber = oInvoice.InvoiceNumber
                oInvoiceline.InvoiceDate = oInvoice.InvoiceDate
                oInvoiceline.DisplayDate = oInvoice.InvoiceDate
                oInvoiceline.DateFrom = pDateFrom
                oInvoiceline.DateTo = pDateTo
                oInvoiceline.Code = Nothing
                oInvoiceline.Credit = 0
                oInvoiceline.Debit = 0
                oInvoiceline.Description = "Charge Mobilus"
                oInvoiceline.Glacier = 0
                oInvoiceline.NombreGlacier = 0
                oInvoiceline.Km = 0
                oInvoiceline.KmExtra = 0
                oInvoiceline.Unite = 1
                oInvoiceline.Prix = dblMobilus
                oInvoiceline.Montant = dblMobilus
                oInvoiceline.IDPeriode = oInvoice.IDPeriode
                oInvoiceline.IDInvoice = oInvoiceline.IDInvoice
                oInvoiceline.TypeAjout = 0
                oInvoiceline.Ajout = False
                oInvoiceLineData.Add(oInvoiceline)
            End If
            '----------------------------------------------------------------------------------------
            ' fin de semain
            '----------------------------------------------------------------------------------------
            Dim oooInvoiceline As New InvoiceLine
            Dim dblMontantPrixFixe As Double = If(IsDBNull(drOrganisation("MontantFixe")), 0, CType(drOrganisation("MontantFixe"), Decimal?))

            If dblMontantPrixFixe > 0 Then

                oooInvoiceline.OrganisationID = iOrganisationID
                oooInvoiceline.Organisation = drOrganisation("Organisation").ToString.Trim
                oooInvoiceline.InvoiceNumber = oInvoice.InvoiceNumber
                oooInvoiceline.InvoiceDate = oInvoice.InvoiceDate
                oooInvoiceline.DisplayDate = oInvoice.InvoiceDate
                oooInvoiceline.DateFrom = pDateFrom
                oooInvoiceline.DateTo = pDateTo
                oooInvoiceline.Code = Nothing
                oooInvoiceline.Credit = 0
                oooInvoiceline.Debit = 0
                oooInvoiceline.Description = "Montant de fin de semaine"
                oooInvoiceline.Glacier = 0
                oooInvoiceline.NombreGlacier = 0
                oooInvoiceline.Km = 0
                oooInvoiceline.KmExtra = 0
                oooInvoiceline.Unite = 1
                oooInvoiceline.Prix = dblMontantPrixFixe
                oooInvoiceline.Montant = dblMontantPrixFixe
                oooInvoiceline.IDPeriode = oInvoice.IDPeriode
                oooInvoiceline.IDInvoice = oInvoiceline.IDInvoice
                oooInvoiceline.TypeAjout = 0
                oooInvoiceline.Ajout = False
                oInvoiceLineData.Add(oooInvoiceline)
            End If


            Return True
            Exit Function

        Catch ex As Exception
            MessageBox.Show(ex.Message, "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Return False
        End Try


    End Function

    Public Function PrixFixeAvecExtras(ByRef drOrganisation As DataRow, pDateFrom As DateTime, pDateTo As DateTime) As Boolean


        Try

            Dim oImport As New Import
            Dim iTotalImport As Integer = 0
            Dim oInvoiceline As New InvoiceLine
            Dim iOrganisationID As Integer
            Dim dtImport As DataTable
            go_Globals.iNombreFree30535 = If(IsDBNull(drOrganisation("NombreFree30535")), 0, CType(drOrganisation("NombreFree30535"), Int32?))
            Dim dtInvoice As DataTable
            Dim drInvoice As DataRow


            iOrganisationID = drOrganisation("OrganisationID")
            '--------------------------------------------
            ' Cree l'entree de la facture
            '--------------------------------------------


            ' check if invoice exist before recreating it
            dtInvoice = oInvoiceData.SelectAllByPeriodOrganisationID(go_Globals.IDPeriode, iOrganisationID)

            If dtInvoice.Rows.Count > 0 Then
                For Each drInvoice In dtInvoice.Rows
                    oInvoiceline.IDInvoice = drInvoice("IDInvoice")
                    oInvoice.IDInvoice = drInvoice("IDInvoice")
                    oInvoice.InvoiceNumber = drInvoice("InvoiceNumber")
                    oInvoice.IDPeriode = drInvoice("IDPeriode")
                    oInvoice.InvoiceDate = pDateTo.ToShortDateString
                    oInvoice.Paye = False
                    oInvoiceData.UpdateInvoiceDate(oInvoice, oInvoice)
                Next
            Else
                oInvoice.InvoiceDate = pDateTo.ToShortDateString
                oInvoice.IDPeriode = go_Globals.IDPeriode
                oInvoice.DateFrom = pDateFrom
                oInvoice.DateTo = pDateTo
                oInvoice.OrganisationID = iOrganisationID
                oInvoice.Organisation = drOrganisation("Organisation").ToString.Trim
                oInvoice.InvoiceNumber = Me.generate_inv()
                oInvoice.Amount = 0
                oInvoice.TVQ = 0
                oInvoice.TPS = 0
                oInvoice.Paye = False
                oInvoiceline.IDInvoice = oInvoiceData.Add(oInvoice)
            End If



            '--------------------------------------------
            ' Prend les valeurs pour les Credit par livraison et les Glaciers
            '--------------------------------------------
            Dim dblPrixFixe As Double = If(IsDBNull(drOrganisation("MontantPrixFixeSem")), 0, CType(drOrganisation("MontantPrixFixeSem"), Decimal?))
            Dim iQuantiteLivraison As Integer = If(IsDBNull(drOrganisation("QuantiteLivraison")), 0, CType(drOrganisation("QuantiteLivraison"), Int32?))
            Dim iMaximumLivraison As Integer = If(IsDBNull(drOrganisation("MaximumLivraison")), 0, CType(drOrganisation("MaximumLivraison"), Int32?))
            Dim dblCreditParLivraison As Double = If(IsDBNull(drOrganisation("CreditParLivraison")), 0, CType(drOrganisation("CreditParLivraison"), Decimal?))
            Dim dblMontantMaxLivraisoneExtra As Double = If(IsDBNull(drOrganisation("MontantMaxLivraisoneExtra")), 0, CType(drOrganisation("MontantMaxLivraisoneExtra"), Decimal?))
            Dim dblPrixGlacier As Double = If(IsDBNull(drOrganisation("MontantGlacierOrganisation")), 0, CType(drOrganisation("MontantGlacierOrganisation"), Decimal?))
            Dim iTotalGlacier As Integer = 0
            Dim dblTotalCredits As Double = oImportData.SelectTotalCredit(pDateFrom, pDateTo, iOrganisationID)
            Dim dblTotalDebit As Double = oImportData.SelectTotalDebit(pDateFrom, pDateTo, iOrganisationID)
            Dim dblTotaL As Double = 0



            '----------------------------------------------------------------------------------------
            ' Genere la colonne montant de l'import pour la facturation
            '----------------------------------------------------------------------------------------

            dtImport = oImportData.SelectAllFromDateName(pDateFrom, pDateTo, iOrganisationID)

            If dtImport.Rows.Count > 0 Then

                If dtImport.Rows.Count <= iMaximumLivraison Then

                    dblPrixFixe = dblPrixFixe

                    oInvoiceline.OrganisationID = iOrganisationID
                    oInvoiceline.Organisation = drOrganisation("Organisation")
                    oInvoiceline.InvoiceNumber = oInvoice.InvoiceNumber
                    oInvoiceline.InvoiceDate = oInvoice.InvoiceDate
                    Dim dPreparationDate As Date = oInvoice.InvoiceDate
                    oInvoiceline.DisplayDate = dPreparationDate.ToShortDateString
                    oInvoiceline.DateFrom = pDateFrom
                    oInvoiceline.DateTo = pDateTo
                    oInvoiceline.Code = Nothing
                    oInvoiceline.Credit = 0
                    oInvoiceline.Debit = 0
                    oInvoiceline.Description = dtImport.Rows.Count.ToString & " Liv"
                    oInvoiceline.Glacier = 0
                    oInvoiceline.NombreGlacier = 0
                    oInvoiceline.Km = 0
                    oInvoiceline.Unite = dtImport.Rows.Count
                    oInvoiceline.Prix = dblPrixFixe
                    oInvoiceline.Montant = dblPrixFixe
                    oInvoiceline.IDPeriode = oInvoice.IDPeriode
                    oInvoiceline.IDInvoice = oInvoiceline.IDInvoice
                    oInvoiceline.TypeAjout = 0
                    oInvoiceline.Ajout = False
                    oInvoiceLineData.Add(oInvoiceline)




                Else

                    iQuantiteLivraison = dtImport.Rows.Count - iMaximumLivraison
                    '-----------------------------------------------------------------------------
                    ' premiere ligne prix normal
                    '-----------------------------------------------------------------------------
                    oInvoiceline.OrganisationID = iOrganisationID
                    oInvoiceline.Organisation = drOrganisation("Organisation")
                    oInvoiceline.InvoiceNumber = oInvoice.InvoiceNumber
                    oInvoiceline.InvoiceDate = oInvoice.InvoiceDate
                    Dim dPreparationDate As Date = oInvoice.InvoiceDate
                    oInvoiceline.DisplayDate = dPreparationDate.ToShortDateString
                    oInvoiceline.DateFrom = pDateFrom
                    oInvoiceline.DateTo = pDateTo
                    oInvoiceline.Code = Nothing
                    oInvoiceline.Credit = 0
                    oInvoiceline.Debit = 0
                    oInvoiceline.Description = iMaximumLivraison.ToString & " Liv"
                    oInvoiceline.Glacier = 0
                    oInvoiceline.NombreGlacier = 0
                    oInvoiceline.Km = 0
                    oInvoiceline.Unite = iMaximumLivraison
                    oInvoiceline.Prix = dblPrixFixe
                    oInvoiceline.Montant = dblPrixFixe
                    oInvoiceline.IDPeriode = oInvoice.IDPeriode
                    oInvoiceline.IDInvoice = oInvoiceline.IDInvoice
                    oInvoiceline.TypeAjout = 0
                    oInvoiceline.Ajout = False
                    oInvoiceLineData.Add(oInvoiceline)

                    '-----------------------------------------------------------------------------
                    ' 2ieme EXTRAS
                    '-----------------------------------------------------------------------------
                    oInvoiceline.OrganisationID = iOrganisationID
                    oInvoiceline.Organisation = drOrganisation("Organisation")
                    oInvoiceline.InvoiceNumber = oInvoice.InvoiceNumber
                    oInvoiceline.InvoiceDate = oInvoice.InvoiceDate
                    dPreparationDate = oInvoice.InvoiceDate
                    oInvoiceline.DisplayDate = dPreparationDate.ToShortDateString
                    oInvoiceline.DateFrom = pDateFrom
                    oInvoiceline.DateTo = pDateTo
                    oInvoiceline.Code = Nothing
                    oInvoiceline.Credit = 0
                    oInvoiceline.Debit = 0
                    oInvoiceline.Description = "Extras"
                    oInvoiceline.Glacier = 0
                    oInvoiceline.NombreGlacier = 0
                    oInvoiceline.Km = 0
                    oInvoiceline.Unite = iQuantiteLivraison
                    oInvoiceline.Prix = Convert.ToDouble(dblMontantMaxLivraisoneExtra)
                    oInvoiceline.Montant = Convert.ToDouble(dblMontantMaxLivraisoneExtra * iQuantiteLivraison)
                    oInvoiceline.IDPeriode = oInvoice.IDPeriode
                    oInvoiceline.IDInvoice = oInvoiceline.IDInvoice
                    oInvoiceline.TypeAjout = 0
                    oInvoiceline.Ajout = False
                    oInvoiceLineData.Add(oInvoiceline)

                End If

            End If


            Dim oooInvoiceline As New InvoiceLine

            '----------------------------------------------------------------------------------------
            ' Rabais par nb. livraisons
            '----------------------------------------------------------------------------------------
            Dim dblCreditParLivraisonTotal As Double = 0
            If (iTotalImport >= iQuantiteLivraison) And iQuantiteLivraison > 0 Then
                dblCreditParLivraisonTotal = iTotalImport * dblCreditParLivraison
                oooInvoiceline.OrganisationID = iOrganisationID
                oooInvoiceline.Organisation = drOrganisation("Organisation").ToString.Trim
                oooInvoiceline.InvoiceNumber = oInvoice.InvoiceNumber
                oooInvoiceline.InvoiceDate = oInvoice.InvoiceDate
                oooInvoiceline.DisplayDate = oInvoice.InvoiceDate
                oooInvoiceline.DateFrom = pDateFrom
                oooInvoiceline.DateTo = pDateTo
                oooInvoiceline.Code = Nothing
                oooInvoiceline.Credit = 0
                oooInvoiceline.Debit = 0
                oooInvoiceline.Description = "Rabais par nb. livraisons"
                oooInvoiceline.Glacier = 0
                oooInvoiceline.NombreGlacier = 0
                oooInvoiceline.Km = 0
                oooInvoiceline.KmExtra = 0
                oooInvoiceline.Unite = iTotalImport
                oooInvoiceline.Prix = dblCreditParLivraison
                oooInvoiceline.Montant = dblCreditParLivraisonTotal
                oooInvoiceline.IDPeriode = oInvoice.IDPeriode
                oooInvoiceline.IDInvoice = oInvoiceline.IDInvoice
                oooInvoiceline.TypeAjout = 2
                oooInvoiceline.Ajout = False
                oInvoiceLineData.Add(oooInvoiceline)
            End If



            '----------------------------------------------------------------------------------------
            ' Credits
            '----------------------------------------------------------------------------------------

            dblTotalCredits = oImportData.SelectTotalCredit(pDateFrom, pDateTo, iOrganisationID)
            If dblTotalCredits > 0 Then
                oooInvoiceline.OrganisationID = iOrganisationID
                oooInvoiceline.Organisation = drOrganisation("Organisation").ToString.Trim
                oooInvoiceline.InvoiceNumber = oInvoice.InvoiceNumber
                oooInvoiceline.InvoiceDate = oInvoice.InvoiceDate
                oooInvoiceline.DisplayDate = oInvoice.InvoiceDate
                oooInvoiceline.DateFrom = pDateFrom
                oooInvoiceline.DateTo = pDateTo
                oooInvoiceline.Code = Nothing
                oooInvoiceline.Credit = 0
                oooInvoiceline.Debit = 0
                oooInvoiceline.Description = "Echanges gratuit"
                oooInvoiceline.Glacier = 0
                oooInvoiceline.NombreGlacier = 0
                oooInvoiceline.Km = 0
                oooInvoiceline.KmExtra = 0
                oooInvoiceline.Unite = 1
                oooInvoiceline.Prix = dblTotalCredits
                oooInvoiceline.Montant = dblTotalCredits
                oooInvoiceline.IDPeriode = oInvoice.IDPeriode
                oooInvoiceline.IDInvoice = oInvoiceline.IDInvoice
                oooInvoiceline.TypeAjout = 2
                oooInvoiceline.Ajout = False
                oInvoiceLineData.Add(oooInvoiceline)
            End If




            '----------------------------------------------------------------------------------------
            ' Cadeau IDS
            '----------------------------------------------------------------------------------------
            Dim dblTotalCadeau As Double = 0

            dblTotalCadeau = oCadeauOrganisationData.SelectTotalAmountByOrganisation(pDateFrom, pDateTo, iOrganisationID)

            If dblTotalCadeau > 0 Then

                oooInvoiceline.OrganisationID = iOrganisationID
                oooInvoiceline.Organisation = drOrganisation("Organisation").ToString.Trim
                oooInvoiceline.InvoiceNumber = oInvoice.InvoiceNumber
                oooInvoiceline.InvoiceDate = oInvoice.InvoiceDate
                oooInvoiceline.DisplayDate = oInvoice.InvoiceDate
                oooInvoiceline.DateFrom = pDateFrom
                oooInvoiceline.DateTo = pDateTo
                oooInvoiceline.Code = Nothing
                oooInvoiceline.Credit = 0
                oooInvoiceline.Debit = 0
                oooInvoiceline.Description = "Cadeau IDS"
                oooInvoiceline.Glacier = 0
                oooInvoiceline.NombreGlacier = 0
                oooInvoiceline.Km = 0
                oooInvoiceline.KmExtra = 0
                oooInvoiceline.Unite = 1
                oooInvoiceline.Prix = dblTotalCadeau
                oooInvoiceline.Montant = dblTotalCadeau
                oooInvoiceline.IDPeriode = oInvoice.IDPeriode
                oooInvoiceline.IDInvoice = oInvoiceline.IDInvoice
                oooInvoiceline.TypeAjout = 2
                oooInvoiceline.Ajout = False
                oInvoiceLineData.Add(oooInvoiceline)
            End If



            '----------------------------------------------------------------------------------------
            ' Montant Mobilus
            '----------------------------------------------------------------------------------------

            Dim dblMobilus As Double = If(IsDBNull(drOrganisation("MontantMobilus")), 0, CType(drOrganisation("MontantMobilus"), Decimal?))

            If dblMobilus > 0 Then

                oooInvoiceline.OrganisationID = iOrganisationID
                oooInvoiceline.Organisation = drOrganisation("Organisation").ToString.Trim
                oooInvoiceline.InvoiceNumber = oInvoice.InvoiceNumber
                oooInvoiceline.InvoiceDate = oInvoice.InvoiceDate
                oooInvoiceline.DisplayDate = oInvoice.InvoiceDate
                oooInvoiceline.DateFrom = pDateFrom
                oooInvoiceline.DateTo = pDateTo
                oooInvoiceline.Code = Nothing
                oooInvoiceline.Credit = 0
                oooInvoiceline.Debit = 0
                oooInvoiceline.Description = "Charge Mobilus"
                oooInvoiceline.Glacier = 0
                oooInvoiceline.NombreGlacier = 0
                oooInvoiceline.Km = 0
                oooInvoiceline.KmExtra = 0
                oooInvoiceline.Unite = 1
                oooInvoiceline.Prix = dblMobilus
                oooInvoiceline.Montant = dblMobilus
                oooInvoiceline.IDPeriode = oInvoice.IDPeriode
                oooInvoiceline.IDInvoice = oInvoiceline.IDInvoice
                oooInvoiceline.TypeAjout = 0
                oooInvoiceline.Ajout = False
                oInvoiceLineData.Add(oooInvoiceline)
            End If


            '----------------------------------------------------------------------------------------
            ' fin de semain
            '----------------------------------------------------------------------------------------

            Dim dblMontantPrixFixe As Double = If(IsDBNull(drOrganisation("MontantFixe")), 0, CType(drOrganisation("MontantFixe"), Decimal?))

            If dblMontantPrixFixe > 0 Then

                oooInvoiceline.OrganisationID = iOrganisationID
                oooInvoiceline.Organisation = drOrganisation("Organisation").ToString.Trim
                oooInvoiceline.InvoiceNumber = oInvoice.InvoiceNumber
                oooInvoiceline.InvoiceDate = oInvoice.InvoiceDate
                oooInvoiceline.DisplayDate = oInvoice.InvoiceDate
                oooInvoiceline.DateFrom = pDateFrom
                oooInvoiceline.DateTo = pDateTo
                oooInvoiceline.Code = Nothing
                oooInvoiceline.Credit = 0
                oooInvoiceline.Debit = 0
                oooInvoiceline.Description = "Montant de fin de semaine"
                oooInvoiceline.Glacier = 0
                oooInvoiceline.NombreGlacier = 0
                oooInvoiceline.Km = 0
                oooInvoiceline.KmExtra = 0
                oooInvoiceline.Unite = 1
                oooInvoiceline.Prix = dblMontantPrixFixe
                oooInvoiceline.Montant = dblMontantPrixFixe
                oooInvoiceline.IDPeriode = oInvoice.IDPeriode
                oooInvoiceline.IDInvoice = oInvoiceline.IDInvoice
                oooInvoiceline.TypeAjout = 0
                oooInvoiceline.Ajout = False
                oInvoiceLineData.Add(oooInvoiceline)
            End If

            Return True

        Catch ex As Exception
            MessageBox.Show(ex.Message, "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Return False
        End Try


    End Function


    Public Function ParResidence(ByRef drOrganisation As DataRow, pDateFrom As DateTime, pDateTo As DateTime) As Boolean

        'Tous les livraisons au prix fixe du contrat
        'Par contre si les livraisons sont a la meme addresse et differente portes, chaque porte au prix du contrat plus le prix par porte

        Try

            Dim oImport As New Import
            Dim iTotalImport As Integer = 0
            Dim oInvoiceline As New InvoiceLine
            Dim iOrganisationID As Integer
            Dim dtInvoice As DataTable
            Dim drInvoice As DataRow
            Dim dblTotalCredits As Double


            iOrganisationID = drOrganisation("OrganisationID")
            '--------------------------------------------
            ' Cree l'entree de la facture
            '--------------------------------------------


            ' check if invoice exist before recreating it
            dtInvoice = oInvoiceData.SelectAllByPeriodOrganisationID(go_Globals.IDPeriode, iOrganisationID)

            If dtInvoice.Rows.Count > 0 Then
                For Each drInvoice In dtInvoice.Rows
                    oInvoiceline.IDInvoice = drInvoice("IDInvoice")
                    oInvoice.IDInvoice = drInvoice("IDInvoice")
                    oInvoice.InvoiceNumber = drInvoice("InvoiceNumber")
                    oInvoice.IDPeriode = drInvoice("IDPeriode")
                    oInvoice.InvoiceDate = pDateTo.ToShortDateString
                    oInvoice.Paye = False
                    oInvoiceData.UpdateInvoiceDate(oInvoice, oInvoice)
                Next
            Else
                oInvoice.InvoiceDate = pDateTo.ToShortDateString
                oInvoice.IDPeriode = go_Globals.IDPeriode
                oInvoice.DateFrom = pDateFrom
                oInvoice.DateTo = pDateTo
                oInvoice.OrganisationID = iOrganisationID
                oInvoice.Organisation = drOrganisation("Organisation").ToString.Trim
                oInvoice.InvoiceNumber = Me.generate_inv()
                oInvoice.Amount = 0
                oInvoice.TVQ = 0
                oInvoice.TPS = 0
                oInvoice.Paye = False
                oInvoiceline.IDInvoice = oInvoiceData.Add(oInvoice)
            End If



            '--------------------------------------------
            ' Prend les valeurs pour les Credit par livraison et les Glaciers
            '--------------------------------------------
            Dim dblPrixFixe As Double = If(IsDBNull(drOrganisation("MontantContrat")), 0, CType(drOrganisation("MontantContrat"), Decimal?))
            Dim dblPrixFixeParPorte As Double = If(IsDBNull(drOrganisation("MontantParPorteOrganisation")), 0, CType(drOrganisation("MontantParPorteOrganisation"), Decimal?))
            Dim iQuantiteLivraison As Integer = If(IsDBNull(drOrganisation("QuantiteLivraison")), 0, CType(drOrganisation("QuantiteLivraison"), Int32?))
            Dim dblCreditParLivraison As Double = If(IsDBNull(drOrganisation("CreditParLivraison")), 0, CType(drOrganisation("CreditParLivraison"), Decimal?))
            Dim dblPrixGlacier As Double = If(IsDBNull(drOrganisation("MontantGlacierOrganisation")), 0, CType(drOrganisation("MontantGlacierOrganisation"), Decimal?))
            Dim iTotalGlacier As Integer = 0



            'dblTotalCredits = oImportData.SelectTotalCredit(pDateFrom, pDateTo, iOrganisationID)



            'Dim dblTotalDebit As Double = oImportData.SelectTotalDebit(pDateFrom, pDateTo, iOrganisationID)
            'Loop a travers chaque jours de la periode.

            Dim SD As DateTime = pDateFrom.ToShortDateString
            Dim ED As DateTime = pDateTo.ToShortDateString
            SD = SD.ToString.Replace("12:00", "12:01")

            Do While SD <= ED


                Dim dtImportR As DataTable
                Dim drImportR As DataRow
                Dim dtImportD As DataTable

                dtImportR = oImportData.SelectDistincResidences(SD, iOrganisationID)
                If dtImportR.Rows.Count > 0 Then

                    'loop a travers les residences
                    For Each drImportR In dtImportR.Rows


                        ' SELECTIONNE TOUS LES BUILDING QUI ONT UNE RESIDENCE
                        dtImportD = oImportData.SelectAllFromDateOrganisationResidence(SD, iOrganisationID, drImportR("IDResidence"))

                        If dtImportD.Rows.Count > 1 Then
                            oInvoiceline.OrganisationID = iOrganisationID
                            oInvoiceline.Organisation = drOrganisation("Organisation").ToString.Trim
                            oInvoiceline.InvoiceNumber = oInvoice.InvoiceNumber.ToString.Trim
                            oInvoiceline.IDPeriode = oInvoice.IDPeriode
                            oInvoiceline.IDInvoice = oInvoiceline.IDInvoice
                            oInvoiceline.InvoiceDate = oInvoice.InvoiceDate
                            oInvoiceline.DisplayDate = SD
                            oInvoiceline.DateFrom = pDateFrom
                            oInvoiceline.DateTo = pDateTo
                            oInvoiceline.Code = Nothing
                            oInvoiceline.Credit = 0
                            oInvoiceline.Debit = 0
                            oInvoiceline.Description = drImportR("Residence").ToString & " (" & dblPrixFixe.ToString & "$ + Portes à " & dblPrixFixeParPorte.ToString & "$)"
                            oInvoiceline.Glacier = 0
                            oInvoiceline.NombreGlacier = 0
                            oInvoiceline.Km = 0
                            oInvoiceline.KmExtra = 0
                            oInvoiceline.Unite = dtImportD.Rows.Count
                            oInvoiceline.Prix = dblPrixFixe + (dtImportD.Rows.Count * dblPrixFixeParPorte)
                            oInvoiceline.Montant = dblPrixFixe + (dtImportD.Rows.Count * dblPrixFixeParPorte)
                            oInvoiceline.IDPeriode = oInvoice.IDPeriode
                            oInvoiceline.TypeAjout = 0
                            oInvoiceline.Ajout = False
                            oInvoiceLineData.Add(oInvoiceline)

                        ElseIf dtImportD.Rows.Count = 1 Then

                            oInvoiceline.OrganisationID = iOrganisationID
                            oInvoiceline.Organisation = drOrganisation("Organisation").ToString.Trim
                            oInvoiceline.InvoiceNumber = oInvoice.InvoiceNumber.ToString.Trim
                            oInvoiceline.IDPeriode = oInvoice.IDPeriode
                            oInvoiceline.IDInvoice = oInvoiceline.IDInvoice
                            oInvoiceline.InvoiceDate = oInvoice.InvoiceDate
                            oInvoiceline.DisplayDate = SD
                            oInvoiceline.DateFrom = pDateFrom
                            oInvoiceline.DateTo = pDateTo
                            oInvoiceline.Code = Nothing
                            oInvoiceline.Credit = 0
                            oInvoiceline.Debit = 0
                            oInvoiceline.Description = drImportR("Residence").ToString
                            oInvoiceline.Glacier = 0
                            oInvoiceline.NombreGlacier = 0
                            oInvoiceline.Km = 0
                            oInvoiceline.KmExtra = 0
                            oInvoiceline.Unite = 1
                            oInvoiceline.Prix = dblPrixFixe
                            oInvoiceline.Montant = dblPrixFixe
                            oInvoiceline.IDPeriode = oInvoice.IDPeriode
                            oInvoiceline.IDInvoice = oInvoiceline.IDInvoice
                            oInvoiceline.TypeAjout = 0
                            oInvoiceline.Ajout = False
                            oInvoiceLineData.Add(oInvoiceline)


                        End If
                    Next
                End If


                ' SELECTIONNE TOUS LES BUILDING QUI ONT  PAS UNE RESIDENCE
                Dim dtImportR2 As DataTable

                dtImportR2 = oImportData.SelectAllFromDateOrganisationNOResidence(SD, iOrganisationID)
                If dtImportR2.Rows.Count > 0 Then
                    oInvoiceline.OrganisationID = iOrganisationID
                    oInvoiceline.Organisation = drOrganisation("Organisation").ToString.Trim
                    oInvoiceline.InvoiceNumber = oInvoice.InvoiceNumber.ToString.Trim
                    oInvoiceline.InvoiceDate = oInvoice.InvoiceDate
                    oInvoiceline.IDPeriode = oInvoice.IDPeriode
                    oInvoiceline.IDInvoice = oInvoiceline.IDInvoice
                    oInvoiceline.DisplayDate = SD
                    oInvoiceline.DateFrom = pDateFrom
                    oInvoiceline.DateTo = pDateTo
                    oInvoiceline.Code = Nothing
                    oInvoiceline.Credit = 0
                    oInvoiceline.Debit = 0
                    oInvoiceline.Description = "Livraisons à " & dblPrixFixe.ToString & "$"
                    oInvoiceline.Glacier = 0
                    oInvoiceline.NombreGlacier = 0
                    oInvoiceline.Km = 0
                    oInvoiceline.KmExtra = 0
                    oInvoiceline.Unite = dtImportR2.Rows.Count
                    oInvoiceline.Prix = dblPrixFixe
                    oInvoiceline.Prix = dblPrixFixe * dtImportR2.Rows.Count
                    oInvoiceline.Montant = dblPrixFixe * dtImportR2.Rows.Count
                    oInvoiceline.TypeAjout = 0
                    oInvoiceline.Ajout = False
                    oInvoiceLineData.Add(oInvoiceline)

                End If



                SD = SD.AddDays(1)

            Loop





            Dim oooInvoiceline As New InvoiceLine

            '----------------------------------------------------------------------------------------
            ' Rabais par nb. livraisons
            '----------------------------------------------------------------------------------------
            Dim dblCreditParLivraisonTotal As Double = 0
            If (iTotalImport >= iQuantiteLivraison) And iQuantiteLivraison > 0 Then
                dblCreditParLivraisonTotal = iTotalImport * dblCreditParLivraison
                oooInvoiceline.OrganisationID = iOrganisationID
                oooInvoiceline.Organisation = drOrganisation("Organisation").ToString.Trim
                oooInvoiceline.InvoiceNumber = oInvoice.InvoiceNumber.ToString.Trim
                oooInvoiceline.InvoiceDate = oInvoice.InvoiceDate
                oooInvoiceline.DisplayDate = oInvoice.InvoiceDate
                oooInvoiceline.DateFrom = pDateFrom
                oooInvoiceline.DateTo = pDateTo
                oooInvoiceline.Code = Nothing
                oooInvoiceline.Credit = 0
                oooInvoiceline.Debit = 0
                oooInvoiceline.Description = "Rabais par nb. livraisons"
                oooInvoiceline.Glacier = 0
                oooInvoiceline.NombreGlacier = 0
                oooInvoiceline.Km = 0
                oooInvoiceline.KmExtra = 0
                oooInvoiceline.Unite = iTotalImport
                oooInvoiceline.Prix = dblCreditParLivraison
                oooInvoiceline.Montant = dblCreditParLivraisonTotal
                oooInvoiceline.IDPeriode = oInvoice.IDPeriode
                oooInvoiceline.IDInvoice = oInvoiceline.IDInvoice
                oooInvoiceline.TypeAjout = 2
                oooInvoiceline.Ajout = False
                oInvoiceLineData.Add(oooInvoiceline)
            End If



            '----------------------------------------------------------------------------------------
            ' Credits
            '----------------------------------------------------------------------------------------

            dblTotalCredits = oImportData.SelectTotalCredit(pDateFrom, pDateTo, iOrganisationID)
            If dblTotalCredits > 0 Then
                oooInvoiceline.OrganisationID = iOrganisationID
                oooInvoiceline.Organisation = drOrganisation("Organisation").ToString.Trim
                oooInvoiceline.InvoiceNumber = oInvoice.InvoiceNumber.ToString.Trim
                oooInvoiceline.InvoiceDate = oInvoice.InvoiceDate
                oooInvoiceline.DisplayDate = oInvoice.InvoiceDate
                oooInvoiceline.DateFrom = pDateFrom
                oooInvoiceline.DateTo = pDateTo
                oooInvoiceline.Code = Nothing
                oooInvoiceline.Credit = 0
                oooInvoiceline.Debit = 0
                oooInvoiceline.Description = "Echanges gratuit"
                oooInvoiceline.Glacier = 0
                oooInvoiceline.NombreGlacier = 0
                oooInvoiceline.Km = 0
                oooInvoiceline.KmExtra = 0
                oooInvoiceline.Unite = 1
                oooInvoiceline.Prix = dblTotalCredits

                oooInvoiceline.Montant = dblTotalCredits
                oooInvoiceline.IDPeriode = oInvoice.IDPeriode
                oooInvoiceline.IDInvoice = oInvoiceline.IDInvoice
                oooInvoiceline.TypeAjout = 2
                oooInvoiceline.Ajout = False
                oInvoiceLineData.Add(oooInvoiceline)
            End If




            '----------------------------------------------------------------------------------------
            ' Cadeau IDS
            '----------------------------------------------------------------------------------------
            Dim dblTotalCadeau As Double = 0

            dblTotalCadeau = oCadeauOrganisationData.SelectTotalAmountByOrganisation(pDateFrom, pDateTo, iOrganisationID)

            If dblTotalCadeau > 0 Then

                oooInvoiceline.OrganisationID = iOrganisationID
                oooInvoiceline.Organisation = drOrganisation("Organisation").ToString.Trim
                oooInvoiceline.InvoiceNumber = oInvoice.InvoiceNumber.ToString.Trim
                oooInvoiceline.InvoiceDate = oInvoice.InvoiceDate
                oooInvoiceline.DisplayDate = oInvoice.InvoiceDate
                oooInvoiceline.DateFrom = pDateFrom
                oooInvoiceline.DateTo = pDateTo
                oooInvoiceline.Code = Nothing
                oooInvoiceline.Credit = 0
                oooInvoiceline.Debit = 0
                oooInvoiceline.Description = "Cadeau IDS"
                oooInvoiceline.Glacier = 0
                oooInvoiceline.NombreGlacier = 0
                oooInvoiceline.Km = 0
                oooInvoiceline.KmExtra = 0
                oooInvoiceline.Unite = 1
                oooInvoiceline.Prix = dblTotalCadeau
                oooInvoiceline.Montant = dblTotalCadeau
                oooInvoiceline.IDPeriode = oInvoice.IDPeriode
                oooInvoiceline.IDInvoice = oInvoiceline.IDInvoice
                oooInvoiceline.TypeAjout = 2
                oooInvoiceline.Ajout = False
                oInvoiceLineData.Add(oooInvoiceline)
            End If



            '----------------------------------------------------------------------------------------
            ' Montant Mobilus
            '----------------------------------------------------------------------------------------

            Dim dblMobilus As Double = If(IsDBNull(drOrganisation("MontantMobilus")), 0, CType(drOrganisation("MontantMobilus"), Decimal?))

            If dblMobilus > 0 Then

                oooInvoiceline.OrganisationID = iOrganisationID
                oooInvoiceline.Organisation = drOrganisation("Organisation").ToString.Trim
                oooInvoiceline.InvoiceNumber = oInvoice.InvoiceNumber
                oooInvoiceline.InvoiceDate = oInvoice.InvoiceDate
                oooInvoiceline.DisplayDate = oInvoice.InvoiceDate
                oooInvoiceline.DateFrom = pDateFrom
                oooInvoiceline.DateTo = pDateTo
                oooInvoiceline.Code = Nothing
                oooInvoiceline.Credit = 0
                oooInvoiceline.Debit = 0
                oooInvoiceline.Description = "Charge Mobilus"
                oooInvoiceline.Glacier = 0
                oooInvoiceline.NombreGlacier = 0
                oooInvoiceline.Km = 0
                oooInvoiceline.KmExtra = 0
                oooInvoiceline.Unite = 1
                oooInvoiceline.Prix = dblMobilus
                oooInvoiceline.Montant = dblMobilus
                oooInvoiceline.IDPeriode = oInvoice.IDPeriode
                oooInvoiceline.IDInvoice = oInvoiceline.IDInvoice
                oooInvoiceline.TypeAjout = 0
                oooInvoiceline.Ajout = False
                oInvoiceLineData.Add(oooInvoiceline)
            End If


            '----------------------------------------------------------------------------------------
            ' fin de semain
            '----------------------------------------------------------------------------------------

            Dim dblMontantPrixFixe As Double = If(IsDBNull(drOrganisation("MontantFixe")), 0, CType(drOrganisation("MontantFixe"), Decimal?))

            If dblMontantPrixFixe > 0 Then

                oooInvoiceline.OrganisationID = iOrganisationID
                oooInvoiceline.Organisation = drOrganisation("Organisation").ToString.Trim
                oooInvoiceline.InvoiceNumber = oInvoice.InvoiceNumber
                oooInvoiceline.InvoiceDate = oInvoice.InvoiceDate
                oooInvoiceline.DisplayDate = oInvoice.InvoiceDate
                oooInvoiceline.DateFrom = pDateFrom
                oooInvoiceline.DateTo = pDateTo
                oooInvoiceline.Code = Nothing
                oooInvoiceline.Credit = 0
                oooInvoiceline.Debit = 0
                oooInvoiceline.Description = "Montant de fin de semaine"
                oooInvoiceline.Glacier = 0
                oooInvoiceline.NombreGlacier = 0
                oooInvoiceline.Km = 0
                oooInvoiceline.KmExtra = 0
                oooInvoiceline.Unite = 1
                oooInvoiceline.Prix = dblMontantPrixFixe
                oooInvoiceline.Montant = dblMontantPrixFixe
                oooInvoiceline.IDPeriode = oInvoice.IDPeriode
                oooInvoiceline.IDInvoice = oInvoiceline.IDInvoice
                oooInvoiceline.TypeAjout = 0
                oooInvoiceline.Ajout = False
                oInvoiceLineData.Add(oooInvoiceline)
            End If

            Return True

        Catch ex As Exception
            MessageBox.Show(ex.Message, "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Return False
        End Try


    End Function

    Public Function PrixFixe(ByRef drOrganisation As DataRow, pDateFrom As DateTime, pDateTo As DateTime) As Boolean


        Try

            Dim oImport As New Import
            Dim iTotalImport As Integer = 0
            Dim oInvoiceline As New InvoiceLine
            Dim iOrganisationID As Integer
            Dim dtImport As DataTable
            go_Globals.iNombreFree30535 = If(IsDBNull(drOrganisation("NombreFree30535")), 0, CType(drOrganisation("NombreFree30535"), Int32?))
            Dim dtInvoice As DataTable
            Dim drInvoice As DataRow
            Dim dblTotalCredits As Double


            iOrganisationID = drOrganisation("OrganisationID")
            '--------------------------------------------
            ' Cree l'entree de la facture
            '--------------------------------------------


            ' check if invoice exist before recreating it
            dtInvoice = oInvoiceData.SelectAllByPeriodOrganisationID(go_Globals.IDPeriode, iOrganisationID)

            If dtInvoice.Rows.Count > 0 Then
                For Each drInvoice In dtInvoice.Rows
                    oInvoiceline.IDInvoice = drInvoice("IDInvoice")
                    oInvoice.IDInvoice = drInvoice("IDInvoice")
                    oInvoice.InvoiceNumber = drInvoice("InvoiceNumber")
                    oInvoice.IDPeriode = drInvoice("IDPeriode")
                    oInvoice.InvoiceDate = pDateTo.ToShortDateString
                    oInvoice.Paye = False
                    oInvoiceData.UpdateInvoiceDate(oInvoice, oInvoice)
                Next
            Else
                oInvoice.InvoiceDate = pDateTo.ToShortDateString
                oInvoice.IDPeriode = go_Globals.IDPeriode
                oInvoice.DateFrom = pDateFrom
                oInvoice.DateTo = pDateTo
                oInvoice.OrganisationID = iOrganisationID
                oInvoice.Organisation = drOrganisation("Organisation").ToString.Trim
                oInvoice.InvoiceNumber = Me.generate_inv()
                oInvoice.Amount = 0
                oInvoice.TVQ = 0
                oInvoice.TPS = 0
                oInvoice.Paye = False
                oInvoiceline.IDInvoice = oInvoiceData.Add(oInvoice)
            End If



            '--------------------------------------------
            ' Prend les valeurs pour les Credit par livraison et les Glaciers
            '--------------------------------------------
            Dim dblPrixFixe As Double = If(IsDBNull(drOrganisation("MontantContrat")), 0, CType(drOrganisation("MontantContrat"), Decimal?))
            Dim iQuantiteLivraison As Integer = If(IsDBNull(drOrganisation("QuantiteLivraison")), 0, CType(drOrganisation("QuantiteLivraison"), Int32?))
            Dim dblCreditParLivraison As Double = If(IsDBNull(drOrganisation("CreditParLivraison")), 0, CType(drOrganisation("CreditParLivraison"), Decimal?))
            Dim dblPrixGlacier As Double = If(IsDBNull(drOrganisation("MontantGlacierOrganisation")), 0, CType(drOrganisation("MontantGlacierOrganisation"), Decimal?))
            Dim iTotalGlacier As Integer = 0



            'dblTotalCredits = oImportData.SelectTotalCredit(pDateFrom, pDateTo, iOrganisationID)



            Dim dblTotalDebit As Double = oImportData.SelectTotalDebit(pDateFrom, pDateTo, iOrganisationID)



            '----------------------------------------------------------------------------------------
            ' Genere la colonne montant de l'import pour la facturation
            '----------------------------------------------------------------------------------------

            Select Case iOrganisationID
                Case 4
                    SysTemGlobals.ExecuteSQL("Update Import Set Credit = 0 WHERE IDPeriode =" & go_Globals.IDPeriode & " And OrganisationID = " & 4)
                Case 6

                    SysTemGlobals.ExecuteSQL("Update Import Set Credit = 0 WHERE IDPeriode =" & go_Globals.IDPeriode & " And OrganisationID = " & 6)
            End Select



            dtImport = oImportData.SelectAllFromDateName(pDateFrom, pDateTo, iOrganisationID)

            If dtImport.Rows.Count > 0 Then
                For Each drImport In dtImport.Rows

                    oImport.ImportID = drImport("ImportID")



                    ' echange entre 2 pharmacie (35-305
                    If drImport("Echange") = True Then

                        Select Case drImport("OrganisationID")
                            Case 4
                                go_Globals.TotalEchange305 = go_Globals.TotalEchange305 + 1
                            Case 6
                                go_Globals.TotalEchange35 = go_Globals.TotalEchange35 + 1
                        End Select

                        go_Globals.TotalEchange = go_Globals.TotalEchange305 + go_Globals.TotalEchange35
                        If go_Globals.TotalEchange <= go_Globals.iNombreFree30535 Then
                            oImport.ImportID = drImport("ImportID")
                            oImport.Credit = If(IsDBNull(dblPrixFixe), 0, CType(dblPrixFixe, Decimal?))
                            oImportData.UpdateCredit(oImport)
                        End If


                    End If




                    iTotalGlacier = iTotalGlacier + If(IsDBNull(drImport("NombreGlacier")), 0, CType(drImport("NombreGlacier"), Int32?))

                    If iTotalGlacier > 0 Then
                        oImport.MontantGlacierOrganisation = iTotalGlacier * If(IsDBNull(drOrganisation("MontantGlacierOrganisation")), 0, CType(drOrganisation("MontantGlacierOrganisation"), Decimal?))
                    Else
                        oImport.MontantGlacierOrganisation = 0
                    End If

                    oImport.Montant = If(IsDBNull(dblPrixFixe), 0, CType(dblPrixFixe, Decimal?))

                    If drImport("IsMobilus") = False Then

                        Dim dblNouveauMontant As Double = 0
                        dblNouveauMontant = If(IsDBNull(drImport("Montant")), 0, CType(drImport("Montant"), Decimal?))

                        If oImport.Montant <> dblNouveauMontant And dblNouveauMontant > 0 Then
                            oImport.Montant = If(IsDBNull(dblNouveauMontant), 0, CType(dblNouveauMontant, Decimal?))
                        Else
                            oImport.Montant = If(IsDBNull(dblPrixFixe), 0, CType(dblPrixFixe, Decimal?))

                        End If

                    End If

                    oImport.ExtraKM = 0
                    oImport.Extra = 0
                    oImportData.UpdateAmountExtraGlacierKMByID(oImport)

                Next
            End If




            '----------------------------------------------------------------------------------------
            ' Cree le detail de la facture
            '----------------------------------------------------------------------------------------

            iTotalGlacier = 0
            Dim dblTotalKM As Double = 0
            Dim dblTotalExtraKM As Double = 0
            Dim dblTotalGlacier As Double = 0
            Dim iTotalLivraison As Integer = 0
            Dim dtImportC As DataTable
            Dim drImportC As DataRow
            Dim oooInvoiceline As New InvoiceLine

            dtImportC = oImportData.SelectDeliveryByDatePrixFixe(pDateFrom, pDateTo, iOrganisationID)

            If dtImportC.Rows.Count > 0 Then

                For Each drImportC In dtImportC.Rows
                    iTotalLivraison = iTotalLivraison + If(IsDBNull(drImportC("TotalLivraison")), 0, CType(drImportC("TotalLivraison"), Int32?))
                    iTotalGlacier = iTotalGlacier + If(IsDBNull(drImportC("TotalGlacier")), 0, CType(drImportC("TotalGlacier"), Int32?))
                    dblTotalGlacier = dblTotalGlacier + If(IsDBNull(drImportC("TotalMontantGlacierOrganisation")), 0, CType(drImportC("TotalMontantGlacierOrganisation"), Decimal?))
                Next

                '----------------------------------------------------------------------------------------
                ' Livraisons
                '----------------------------------------------------------------------------------------
                Dim dPreparationDate As Date
                For Each drImportC In dtImportC.Rows

                    iTotalImport = If(IsDBNull(drImportC("TotalLivraison")), 0, CType(drImportC("TotalLivraison"), Integer?))

                    oInvoiceline.OrganisationID = iOrganisationID
                    oInvoiceline.Organisation = drOrganisation("Organisation")
                    oInvoiceline.InvoiceNumber = oInvoice.InvoiceNumber
                    oInvoiceline.InvoiceDate = oInvoice.InvoiceDate
                    dPreparationDate = drImportC("PreparationDate")
                    oInvoiceline.DisplayDate = dPreparationDate.ToShortDateString
                    oInvoiceline.DateFrom = pDateFrom
                    oInvoiceline.DateTo = pDateTo
                    oInvoiceline.Code = Nothing
                    oInvoiceline.Credit = If(IsDBNull(drImportC("TotalCredit")), 0, CType(drImportC("TotalCredit"), Decimal?))
                    oInvoiceline.Debit = If(IsDBNull(drImportC("totalDebit")), 0, CType(drImportC("totalDebit"), Decimal?))
                    oInvoiceline.Description = drImportC("TotalLivraison").ToString & " Liv"
                    oInvoiceline.Glacier = 0
                    oInvoiceline.NombreGlacier = 0
                    oInvoiceline.Km = If(IsDBNull(drImportC("TotalKM")), 0, CType(drImportC("TotalKM"), Double?))
                    oInvoiceline.Unite = iTotalImport

                    If drImportC("TotalCommande") > dblPrixFixe Then
                        oInvoiceline.Prix = drImportC("TotalCommande")
                    Else
                        oInvoiceline.Prix = dblPrixFixe

                    End If



                    Dim dblTotalCommande As Double = drImportC("TotalCommande")

                    oInvoiceline.Montant = dblTotalCommande
                    oInvoiceline.IDPeriode = oInvoice.IDPeriode
                    oInvoiceline.IDInvoice = oInvoiceline.IDInvoice
                    oInvoiceline.TypeAjout = 0
                    oInvoiceline.Ajout = False
                    oInvoiceLineData.Add(oInvoiceline)

                Next



            End If









            '----------------------------------------------------------------------------------------
            ' Rabais par nb. livraisons
            '----------------------------------------------------------------------------------------
            Dim dblCreditParLivraisonTotal As Double = 0
            If (iTotalImport >= iQuantiteLivraison) And iQuantiteLivraison > 0 Then
                dblCreditParLivraisonTotal = iTotalImport * dblCreditParLivraison
                oooInvoiceline.OrganisationID = iOrganisationID
                oooInvoiceline.Organisation = drOrganisation("Organisation").ToString.Trim
                oooInvoiceline.InvoiceNumber = oInvoice.InvoiceNumber
                oooInvoiceline.InvoiceDate = oInvoice.InvoiceDate
                oooInvoiceline.DisplayDate = oInvoice.InvoiceDate
                oooInvoiceline.DateFrom = pDateFrom
                oooInvoiceline.DateTo = pDateTo
                oooInvoiceline.Code = Nothing
                oooInvoiceline.Credit = 0
                oooInvoiceline.Debit = 0
                oooInvoiceline.Description = "Rabais par nb. livraisons"
                oooInvoiceline.Glacier = 0
                oooInvoiceline.NombreGlacier = 0
                oooInvoiceline.Km = 0
                oooInvoiceline.KmExtra = 0
                oooInvoiceline.Unite = iTotalImport
                oooInvoiceline.Prix = dblCreditParLivraison
                oooInvoiceline.Montant = dblCreditParLivraisonTotal
                oooInvoiceline.IDPeriode = oInvoice.IDPeriode
                oooInvoiceline.IDInvoice = oInvoiceline.IDInvoice
                oooInvoiceline.TypeAjout = 2
                oooInvoiceline.Ajout = False
                oInvoiceLineData.Add(oooInvoiceline)
            End If



            '----------------------------------------------------------------------------------------
            ' Credits
            '----------------------------------------------------------------------------------------

            dblTotalCredits = oImportData.SelectTotalCredit(pDateFrom, pDateTo, iOrganisationID)
            If dblTotalCredits > 0 Then
                oooInvoiceline.OrganisationID = iOrganisationID
                oooInvoiceline.Organisation = drOrganisation("Organisation").ToString.Trim
                oooInvoiceline.InvoiceNumber = oInvoice.InvoiceNumber
                oooInvoiceline.InvoiceDate = oInvoice.InvoiceDate
                oooInvoiceline.DisplayDate = oInvoice.InvoiceDate
                oooInvoiceline.DateFrom = pDateFrom
                oooInvoiceline.DateTo = pDateTo
                oooInvoiceline.Code = Nothing
                oooInvoiceline.Credit = 0
                oooInvoiceline.Debit = 0
                oooInvoiceline.Description = "Echanges gratuit"
                oooInvoiceline.Glacier = 0
                oooInvoiceline.NombreGlacier = 0
                oooInvoiceline.Km = 0
                oooInvoiceline.KmExtra = 0
                oooInvoiceline.Unite = 1
                oooInvoiceline.Prix = dblTotalCredits

                oooInvoiceline.Montant = dblTotalCredits
                oooInvoiceline.IDPeriode = oInvoice.IDPeriode
                oooInvoiceline.IDInvoice = oInvoiceline.IDInvoice
                oooInvoiceline.TypeAjout = 2
                oooInvoiceline.Ajout = False
                oInvoiceLineData.Add(oooInvoiceline)
            End If




            '----------------------------------------------------------------------------------------
            ' Cadeau IDS
            '----------------------------------------------------------------------------------------
            Dim dblTotalCadeau As Double = 0

            dblTotalCadeau = oCadeauOrganisationData.SelectTotalAmountByOrganisation(pDateFrom, pDateTo, iOrganisationID)

            If dblTotalCadeau > 0 Then

                oooInvoiceline.OrganisationID = iOrganisationID
                oooInvoiceline.Organisation = drOrganisation("Organisation").ToString.Trim
                oooInvoiceline.InvoiceNumber = oInvoice.InvoiceNumber
                oooInvoiceline.InvoiceDate = oInvoice.InvoiceDate
                oooInvoiceline.DisplayDate = oInvoice.InvoiceDate
                oooInvoiceline.DateFrom = pDateFrom
                oooInvoiceline.DateTo = pDateTo
                oooInvoiceline.Code = Nothing
                oooInvoiceline.Credit = 0
                oooInvoiceline.Debit = 0
                oooInvoiceline.Description = "Cadeau IDS"
                oooInvoiceline.Glacier = 0
                oooInvoiceline.NombreGlacier = 0
                oooInvoiceline.Km = 0
                oooInvoiceline.KmExtra = 0
                oooInvoiceline.Unite = 1
                oooInvoiceline.Prix = dblTotalCadeau
                oooInvoiceline.Montant = dblTotalCadeau
                oooInvoiceline.IDPeriode = oInvoice.IDPeriode
                oooInvoiceline.IDInvoice = oInvoiceline.IDInvoice
                oooInvoiceline.TypeAjout = 2
                oooInvoiceline.Ajout = False
                oInvoiceLineData.Add(oooInvoiceline)
            End If



            '----------------------------------------------------------------------------------------
            ' Montant Mobilus
            '----------------------------------------------------------------------------------------

            Dim dblMobilus As Double = If(IsDBNull(drOrganisation("MontantMobilus")), 0, CType(drOrganisation("MontantMobilus"), Decimal?))

            If dblMobilus > 0 Then

                oooInvoiceline.OrganisationID = iOrganisationID
                oooInvoiceline.Organisation = drOrganisation("Organisation").ToString.Trim
                oooInvoiceline.InvoiceNumber = oInvoice.InvoiceNumber
                oooInvoiceline.InvoiceDate = oInvoice.InvoiceDate
                oooInvoiceline.DisplayDate = oInvoice.InvoiceDate
                oooInvoiceline.DateFrom = pDateFrom
                oooInvoiceline.DateTo = pDateTo
                oooInvoiceline.Code = Nothing
                oooInvoiceline.Credit = 0
                oooInvoiceline.Debit = 0
                oooInvoiceline.Description = "Charge Mobilus"
                oooInvoiceline.Glacier = 0
                oooInvoiceline.NombreGlacier = 0
                oooInvoiceline.Km = 0
                oooInvoiceline.KmExtra = 0
                oooInvoiceline.Unite = 1
                oooInvoiceline.Prix = dblMobilus
                oooInvoiceline.Montant = dblMobilus
                oooInvoiceline.IDPeriode = oInvoice.IDPeriode
                oooInvoiceline.IDInvoice = oInvoiceline.IDInvoice
                oooInvoiceline.TypeAjout = 0
                oooInvoiceline.Ajout = False
                oInvoiceLineData.Add(oooInvoiceline)
            End If


            '----------------------------------------------------------------------------------------
            ' fin de semain
            '----------------------------------------------------------------------------------------

            Dim dblMontantPrixFixe As Double = If(IsDBNull(drOrganisation("MontantFixe")), 0, CType(drOrganisation("MontantFixe"), Decimal?))

            If dblMontantPrixFixe > 0 Then

                oooInvoiceline.OrganisationID = iOrganisationID
                oooInvoiceline.Organisation = drOrganisation("Organisation").ToString.Trim
                oooInvoiceline.InvoiceNumber = oInvoice.InvoiceNumber
                oooInvoiceline.InvoiceDate = oInvoice.InvoiceDate
                oooInvoiceline.DisplayDate = oInvoice.InvoiceDate
                oooInvoiceline.DateFrom = pDateFrom
                oooInvoiceline.DateTo = pDateTo
                oooInvoiceline.Code = Nothing
                oooInvoiceline.Credit = 0
                oooInvoiceline.Debit = 0
                oooInvoiceline.Description = "Montant de fin de semaine"
                oooInvoiceline.Glacier = 0
                oooInvoiceline.NombreGlacier = 0
                oooInvoiceline.Km = 0
                oooInvoiceline.KmExtra = 0
                oooInvoiceline.Unite = 1
                oooInvoiceline.Prix = dblMontantPrixFixe
                oooInvoiceline.Montant = dblMontantPrixFixe
                oooInvoiceline.IDPeriode = oInvoice.IDPeriode
                oooInvoiceline.IDInvoice = oInvoiceline.IDInvoice
                oooInvoiceline.TypeAjout = 0
                oooInvoiceline.Ajout = False
                oInvoiceLineData.Add(oooInvoiceline)
            End If

            Return True

        Catch ex As Exception
            MessageBox.Show(ex.Message, "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Return False
        End Try


    End Function
    Public Function ParKilometrageXtraHeure(ByRef drOrganisation As DataRow, pDateFrom As DateTime, pDateTo As DateTime) As Boolean


        Try

            Dim oImport As New Import
            Dim dtImport As DataTable
            Dim drImport As DataRow
            Dim dtCommissionKM As DataTable
            Dim drCommissionKM As DataRow
            Dim dtHeureDepart As DateTime = Nothing
            Dim dblDistance As Double = 0
            Dim dblMontant As Double = 0
            Dim dblKMextra As Double = 0
            Dim dblExtraCharge As Double = 0
            Dim iTotalImport As Integer
            Dim oInvoiceline As New InvoiceLine
            Dim iOrganisationID As Integer
            Dim dblTotalMontant As Double = 0
            Dim dblTotalExtra As Double = 0
            Dim iTotalExtra As Integer = 0
            Dim dblTotalKmExtra As Double = 0
            Dim xtraKMGrandTotal As Double = 0
            Dim dblExtraChargeGrandTotal As Double = 0
            Dim iExtraGrandTotal As Integer = 0
            Dim dblExtraKMGrandTotal As Double = 0
            Dim dtImportC As DataTable
            Dim dblGlacierGrandTotal As Double = 0
            Dim dblTotalDebits As Double = 0
            Dim iGlacierGrandTotal As Integer = 0
            Dim dtInvoice As DataTable
            Dim drInvoice As DataRow


            iOrganisationID = drOrganisation("OrganisationID")
            '--------------------------------------------
            ' Cree l'entree de la facture
            '--------------------------------------------


            ' check if invoice exist before recreating it
            dtInvoice = oInvoiceData.SelectAllByPeriodOrganisationID(go_Globals.IDPeriode, iOrganisationID)

            If dtInvoice.Rows.Count > 0 Then
                For Each drInvoice In dtInvoice.Rows
                    oInvoiceline.IDInvoice = drInvoice("IDInvoice")
                    oInvoice.IDInvoice = drInvoice("IDInvoice")
                    oInvoice.InvoiceNumber = drInvoice("InvoiceNumber")
                    oInvoice.IDPeriode = drInvoice("IDPeriode")
                    oInvoice.InvoiceDate = pDateTo.ToShortDateString
                    oInvoice.Paye = False
                    oInvoiceData.UpdateInvoiceDate(oInvoice, oInvoice)
                Next
            Else
                oInvoice.InvoiceDate = pDateTo.ToShortDateString
                oInvoice.IDPeriode = go_Globals.IDPeriode
                oInvoice.DateFrom = pDateFrom
                oInvoice.DateTo = pDateTo
                oInvoice.OrganisationID = iOrganisationID
                oInvoice.Organisation = drOrganisation("Organisation")
                oInvoice.InvoiceNumber = Me.generate_inv()
                oInvoice.Amount = 0
                oInvoice.TVQ = 0
                oInvoice.TPS = 0
                oInvoice.Paye = False
                oInvoiceline.IDInvoice = oInvoiceData.Add(oInvoice)
            End If



            '----------------------------------------------------------------------------------------
            ' Prend les valeurs pour les Credit par livraison et les Glaciers
            '----------------------------------------------------------------------------------------
            Dim iQuantiteLivraison As Integer = If(IsDBNull(drOrganisation("QuantiteLivraison")), 0, CType(drOrganisation("QuantiteLivraison"), Int32?))
            Dim dblCreditParLivraison As Double = If(IsDBNull(drOrganisation("CreditParLivraison")), 0, CType(drOrganisation("CreditParLivraison"), Decimal?))
            Dim dblPrixGlacier As Double = If(IsDBNull(drOrganisation("MontantGlacierOrganisation")), 0, CType(drOrganisation("MontantGlacierOrganisation"), Decimal?))
            Dim iTotalGlacier As Integer = 0
            Dim dblTotalCredits As Double = oImportData.SelectTotalCredit(pDateFrom, pDateTo, iOrganisationID)
            Dim dblTotalDebit As Double = oImportData.SelectTotalDebit(pDateFrom, pDateTo, iOrganisationID)




            '----------------------------------------------------------------------------------------
            ' Genere la colonne montant de l'import pour la facturation
            '----------------------------------------------------------------------------------------
            dtImport = oImportData.SelectAllFromDateName(pDateFrom, pDateTo, iOrganisationID)

            If dtImport.Rows.Count > 0 Then
                iTotalImport = dtImport.Rows.Count
                For Each drImport In dtImport.Rows
                    oImport.ImportID = drImport("ImportID")

                    iTotalGlacier = iTotalGlacier + If(IsDBNull(drImport("NombreGlacier")), 0, CType(drImport("NombreGlacier"), Int32?))

                    If iTotalGlacier > 0 Then
                        iGlacierGrandTotal = iGlacierGrandTotal + 1
                        oImport.MontantGlacierOrganisation = iTotalGlacier * If(IsDBNull(drOrganisation("MontantGlacierOrganisation")), 0, CType(drOrganisation("MontantGlacierOrganisation"), Decimal?))
                        dblGlacierGrandTotal = dblGlacierGrandTotal + oImport.MontantGlacierOrganisation
                    Else
                        oImport.MontantGlacierOrganisation = 0
                    End If

                    dtHeureDepart = If(IsDBNull(drImport("HeureLivraison")), Nothing, CType(drImport("HeureLivraison"), DateTime?))
                    dblDistance = If(IsDBNull(drImport("Distance")), 0, drImport("Distance"))
                    dblDistance = Math.Round(dblDistance, 2)

                    dtCommissionKM = oCommissionKMData.SelectAllByTimeKM(iOrganisationID, dtHeureDepart, dblDistance)
                    If dtCommissionKM.Rows.Count = 1 Then
                        For Each drCommissionKM In dtCommissionKM.Rows
                            dblKMextra = If(IsDBNull(drCommissionKM("PerKMNumber")), 0, drCommissionKM("PerKMNumber"))
                            dblExtraCharge = If(IsDBNull(drCommissionKM("ExtraChargeParKM")), 0, CType(drCommissionKM("ExtraChargeParKM"), Decimal?))
                            dblMontant = If(IsDBNull(drCommissionKM("Montant")), 0, CType(drCommissionKM("Montant"), Decimal?))

                            If (dblExtraCharge > 0 And dblKMextra > 0) And (dblDistance > Math.Round(drCommissionKM("KMFrom"))) Then
                                oImport.ImportID = drImport("ImportID")
                                dblTotalKmExtra = Math.Round(dblDistance) - Math.Round(drCommissionKM("KMFrom"))
                                dblTotalKmExtra = dblTotalKmExtra / dblKMextra
                                dblTotalKmExtra = Math.Round(dblTotalKmExtra)
                                oImport.ExtraKM = dblTotalKmExtra
                                dblExtraKMGrandTotal = dblExtraKMGrandTotal + oImport.ExtraKM
                                xtraKMGrandTotal = xtraKMGrandTotal + oImport.ExtraKM
                                oImport.Extra = dblTotalKmExtra * dblExtraCharge
                                dblExtraChargeGrandTotal = dblExtraChargeGrandTotal + oImport.Extra
                                oImport.Montant = If(IsDBNull(dblMontant), 0, CType(dblMontant, Decimal?))
                                iExtraGrandTotal = iExtraGrandTotal + 1


                            ElseIf (dblExtraCharge > 0 And dblKMextra = 0) And (dblDistance > Math.Round(drCommissionKM("KMFrom"))) Then
                                oImport.ImportID = drImport("ImportID")
                                dblTotalKmExtra = Math.Round(dblDistance) - Math.Round(drCommissionKM("KMFrom"))
                                oImport.ExtraKM = dblTotalKmExtra
                                dblExtraKMGrandTotal = dblExtraKMGrandTotal + oImport.ExtraKM
                                oImport.Extra = dblExtraCharge
                                dblExtraChargeGrandTotal = dblExtraChargeGrandTotal + oImport.Extra
                                oImport.Montant = If(IsDBNull(dblMontant), 0, CType(dblMontant, Decimal?))
                                iExtraGrandTotal = iExtraGrandTotal + 1

                            Else
                                oImport.ImportID = drImport("ImportID")
                                oImport.Extra = 0
                                oImport.ExtraKM = 0
                                oImport.ExtraKMLivreur = 0
                                oImport.ExtraLivreur = 0

                                oImport.Montant = If(IsDBNull(dblMontant), 0, CType(dblMontant, Decimal?))
                            End If

                            oImportData.UpdateAmountExtraGlacierKMByID(oImport)

                            dblKMextra = 0
                            dblExtraCharge = 0
                            dblMontant = 0
                        Next
                    Else
                        Dim smessage = "La distance " & dblDistance.ToString.Trim & " Ne retourne aucunes valeur pour l'organisation " & drOrganisation("Organisation").ToString & ", " & vbLf
                    smessage = smessage & "Veuillez verifier les configuration calcul par kilometrage + Extra/Heure"
                        MessageBox.Show(smessage)

                        Return False
                        Exit Function
                    End If
                Next
            End If

            dtImportC = oImportData.SelectDeliveryByDate(pDateFrom, pDateTo, iOrganisationID)


            If iExtraGrandTotal > 0 Then
                oInvoiceline.OrganisationID = iOrganisationID
                oInvoiceline.Organisation = drOrganisation("Organisation").ToString.Trim
                oInvoiceline.IDInvoice = oInvoiceline.IDInvoice
                oInvoiceline.InvoiceNumber = oInvoice.InvoiceNumber
                oInvoiceline.InvoiceDate = oInvoice.InvoiceDate
                oInvoiceline.DisplayDate = oInvoice.InvoiceDate
                oInvoiceline.DateFrom = pDateFrom
                oInvoiceline.DateTo = pDateTo
                oInvoiceline.Code = Nothing
                oInvoiceline.Credit = 0
                oInvoiceline.Debit = 0
                oInvoiceline.Description = "Extras"
                oInvoiceline.Glacier = 0
                oInvoiceline.NombreGlacier = 0
                oInvoiceline.Km = dblExtraKMGrandTotal
                oInvoiceline.KmExtra = dblExtraKMGrandTotal
                oInvoiceline.Unite = 1
                oInvoiceline.Prix = dblExtraChargeGrandTotal
                oInvoiceline.Montant = dblExtraChargeGrandTotal
                oInvoiceline.IDPeriode = oInvoice.IDPeriode
                oInvoiceline.TypeAjout = 0
                oInvoiceline.Ajout = False
                oInvoiceLineData.Add(oInvoiceline)

            End If

            '----------------------------------------------------------------------------------------
            ' Livraisons
            '----------------------------------------------------------------------------------------
            Dim dPreparationDate As Date
            For Each drImportC In dtImportC.Rows

                oInvoiceline.OrganisationID = iOrganisationID
                oInvoiceline.Organisation = drOrganisation("Organisation")
                oInvoiceline.InvoiceNumber = oInvoice.InvoiceNumber
                oInvoiceline.IDInvoice = oInvoiceline.IDInvoice
                oInvoiceline.InvoiceDate = oInvoice.InvoiceDate
                dPreparationDate = drImportC("PreparationDate")
                oInvoiceline.DisplayDate = dPreparationDate.ToShortDateString
                oInvoiceline.DateFrom = pDateFrom
                oInvoiceline.DateTo = pDateTo
                oInvoiceline.Code = Nothing
                oInvoiceline.Debit = If(IsDBNull(drImportC("totalDebit")), 0, CType(drImportC("totalDebit"), Decimal?))
                oInvoiceline.Description = drImportC("TotalLivraison").ToString & " Liv"
                oInvoiceline.Glacier = 0
                oInvoiceline.NombreGlacier = 0
                oInvoiceline.Km = If(IsDBNull(drImportC("TotalKM")), 0, CType(drImportC("TotalKM"), Double?))
                oInvoiceline.Unite = 1
                oInvoiceline.Prix = If(IsDBNull(drImportC("TotalCommande")), 0, CType(drImportC("TotalCommande"), Decimal?))
                oInvoiceline.Montant = If(IsDBNull(drImportC("TotalCommande")), 0, CType(drImportC("TotalCommande"), Decimal?))
                oInvoiceline.IDPeriode = oInvoice.IDPeriode
                oInvoiceline.IDInvoice = oInvoiceline.IDInvoice
                oInvoiceline.TypeAjout = 0
                oInvoiceline.Ajout = False
                oInvoiceLineData.Add(oInvoiceline)

            Next


            '----------------------------------------------------------------------------------------
            ' Glaciers
            '----------------------------------------------------------------------------------------
            Dim oooInvoiceline As New InvoiceLine


            '----------------------------------------------------------------------------------------
            ' Rabais par nb. livraisons
            '----------------------------------------------------------------------------------------
            Dim dblCreditParLivraisonTotal As Double = 0
            If (iTotalImport >= iQuantiteLivraison) And iQuantiteLivraison > 0 Then
                dblCreditParLivraisonTotal = iTotalImport * dblCreditParLivraison
                oooInvoiceline.OrganisationID = iOrganisationID
                oooInvoiceline.Organisation = drOrganisation("Organisation").ToString.Trim
                oooInvoiceline.InvoiceNumber = oInvoice.InvoiceNumber
                oooInvoiceline.InvoiceDate = oInvoice.InvoiceDate
                oooInvoiceline.DisplayDate = oInvoice.InvoiceDate
                oooInvoiceline.DateFrom = pDateFrom
                oooInvoiceline.DateTo = pDateTo
                oooInvoiceline.Code = Nothing
                oooInvoiceline.Credit = 0
                oooInvoiceline.Debit = 0
                oooInvoiceline.Description = "Rabais par nb. livraisons"
                oooInvoiceline.Glacier = 0
                oooInvoiceline.NombreGlacier = 0
                oooInvoiceline.Km = 0
                oooInvoiceline.KmExtra = 0
                oooInvoiceline.Unite = iTotalImport
                oooInvoiceline.Prix = dblCreditParLivraison
                oooInvoiceline.Montant = dblCreditParLivraisonTotal
                oooInvoiceline.IDPeriode = oInvoice.IDPeriode
                oooInvoiceline.IDInvoice = oInvoiceline.IDInvoice
                oooInvoiceline.TypeAjout = 2
                oooInvoiceline.Ajout = False
                oInvoiceLineData.Add(oooInvoiceline)
            End If



            '----------------------------------------------------------------------------------------
            ' Cadeau IDS
            '----------------------------------------------------------------------------------------
            Dim dblTotalCadeau As Double = 0

            dblTotalCadeau = oCadeauOrganisationData.SelectTotalAmountByOrganisation(pDateFrom, pDateTo, iOrganisationID)

            If dblTotalCadeau > 0 Then

                oooInvoiceline.OrganisationID = iOrganisationID
                oooInvoiceline.Organisation = drOrganisation("Organisation").ToString.Trim
                oooInvoiceline.InvoiceNumber = oInvoice.InvoiceNumber
                oooInvoiceline.InvoiceDate = oInvoice.InvoiceDate
                oooInvoiceline.DisplayDate = oInvoice.InvoiceDate
                oooInvoiceline.DateFrom = pDateFrom
                oooInvoiceline.DateTo = pDateTo
                oooInvoiceline.Code = Nothing
                oooInvoiceline.Credit = 0
                oooInvoiceline.Debit = 0
                oooInvoiceline.Description = "Cadeau IDS"
                oooInvoiceline.Glacier = 0
                oooInvoiceline.NombreGlacier = 0
                oooInvoiceline.Km = 0
                oooInvoiceline.KmExtra = 0
                oooInvoiceline.Unite = 1
                oooInvoiceline.Prix = dblTotalCadeau
                oooInvoiceline.Montant = dblTotalCadeau
                oooInvoiceline.IDPeriode = oInvoice.IDPeriode
                oooInvoiceline.IDInvoice = oInvoiceline.IDInvoice
                oooInvoiceline.TypeAjout = 2
                oooInvoiceline.Ajout = False
                oInvoiceLineData.Add(oooInvoiceline)
            End If


            '----------------------------------------------------------------------------------------
            ' Montant Mobilus
            '----------------------------------------------------------------------------------------

            Dim dblMobilus As Double = If(IsDBNull(drOrganisation("MontantMobilus")), 0, CType(drOrganisation("MontantMobilus"), Decimal?))

            If dblMobilus > 0 Then

                oooInvoiceline.OrganisationID = iOrganisationID
                oooInvoiceline.Organisation = drOrganisation("Organisation").ToString.Trim
                oooInvoiceline.InvoiceNumber = oInvoice.InvoiceNumber
                oooInvoiceline.InvoiceDate = oInvoice.InvoiceDate
                oooInvoiceline.DisplayDate = oInvoice.InvoiceDate
                oooInvoiceline.DateFrom = pDateFrom
                oooInvoiceline.DateTo = pDateTo
                oooInvoiceline.Code = Nothing
                oooInvoiceline.Credit = 0
                oooInvoiceline.Debit = 0
                oooInvoiceline.Description = "Charge Mobilus"
                oooInvoiceline.Glacier = 0
                oooInvoiceline.NombreGlacier = 0
                oooInvoiceline.Km = 0
                oooInvoiceline.KmExtra = 0
                oooInvoiceline.Unite = 1
                oooInvoiceline.Prix = dblMobilus
                oooInvoiceline.Montant = dblMobilus
                oooInvoiceline.IDPeriode = oInvoice.IDPeriode
                oooInvoiceline.IDInvoice = oInvoiceline.IDInvoice
                oooInvoiceline.TypeAjout = 0
                oooInvoiceline.Ajout = False
                oInvoiceLineData.Add(oooInvoiceline)
            End If



            Dim dblMontantPrixFixe As Double = If(IsDBNull(drOrganisation("MontantFixe")), 0, CType(drOrganisation("MontantFixe"), Decimal?))

            If dblMontantPrixFixe > 0 Then

                oooInvoiceline.OrganisationID = iOrganisationID
                oooInvoiceline.Organisation = drOrganisation("Organisation").ToString.Trim
                oooInvoiceline.InvoiceNumber = oInvoice.InvoiceNumber
                oooInvoiceline.InvoiceDate = oInvoice.InvoiceDate
                oooInvoiceline.DisplayDate = oInvoice.InvoiceDate
                oooInvoiceline.DateFrom = pDateFrom
                oooInvoiceline.DateTo = pDateTo
                oooInvoiceline.Code = Nothing
                oooInvoiceline.Credit = 0
                oooInvoiceline.Debit = 0
                oooInvoiceline.Description = "Montant de fin de semaine"
                oooInvoiceline.Glacier = 0
                oooInvoiceline.NombreGlacier = 0
                oooInvoiceline.Km = 0
                oooInvoiceline.KmExtra = 0
                oooInvoiceline.Unite = 1
                oooInvoiceline.Prix = dblMontantPrixFixe
                oooInvoiceline.Montant = dblMontantPrixFixe
                oooInvoiceline.IDPeriode = oInvoice.IDPeriode
                oooInvoiceline.IDInvoice = oInvoiceline.IDInvoice
                oooInvoiceline.TypeAjout = 0
                oooInvoiceline.Ajout = False
                oInvoiceLineData.Add(oooInvoiceline)
            End If



            Return True

        Catch ex As Exception
            MessageBox.Show(ex.Message, "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Return False
        End Try

    End Function
    Public Function ParKilometrage(ByRef drOrganisation As DataRow, pDateFrom As DateTime, pDateTo As DateTime) As Boolean

        Try



            Dim oImport As New Import
            Dim dtImport As DataTable
            Dim drImport As DataRow
            Dim dtCommissionKM As DataTable
            Dim drCommissionKM As DataRow
            Dim dtHeureDepart As DateTime = Nothing
            Dim dblDistance As Double = 0
            Dim dblMontant As Double = 0
            Dim iTotalImport As Integer
            Dim oInvoiceline As New InvoiceLine
            Dim iOrganisationID As Integer
            Dim dblKMextra As Double = 0
            Dim dblExtraCharge As Double = 0
            Dim dblTotalKmExtra As Double = 0
            Dim dblExtraKMGrandTotal As Double = 0
            Dim dblExtraChargeGrandTotal As Double = 0
            Dim dtImportC As DataTable
            Dim dblGlacierGrandTotal As Double = 0
            Dim dblTotalDebits As Double = 0
            Dim iGlacierGrandTotal As Integer = 0
            Dim dtInvoice As DataTable
            Dim drInvoice As DataRow


            iOrganisationID = drOrganisation("OrganisationID")
            '--------------------------------------------
            ' Cree l'entree de la facture
            '--------------------------------------------


            ' check if invoice exist before recreating it
            dtInvoice = oInvoiceData.SelectAllByPeriodOrganisationID(go_Globals.IDPeriode, iOrganisationID)

            If dtInvoice.Rows.Count > 0 Then
                For Each drInvoice In dtInvoice.Rows
                    oInvoiceline.IDInvoice = drInvoice("IDInvoice")
                    oInvoice.IDInvoice = drInvoice("IDInvoice")
                    oInvoice.InvoiceNumber = drInvoice("InvoiceNumber")
                    oInvoice.IDPeriode = drInvoice("IDPeriode")
                    oInvoice.InvoiceDate = pDateTo.ToShortDateString
                    oInvoice.Paye = False
                    oInvoiceData.UpdateInvoiceDate(oInvoice, oInvoice)
                Next
            Else
                oInvoice.InvoiceDate = pDateTo.ToShortDateString
                oInvoice.IDPeriode = go_Globals.IDPeriode
                oInvoice.DateFrom = pDateFrom
                oInvoice.DateTo = pDateTo
                oInvoice.OrganisationID = iOrganisationID
                oInvoice.Organisation = drOrganisation("Organisation")
                oInvoice.InvoiceNumber = Me.generate_inv()
                oInvoice.Amount = 0
                oInvoice.TVQ = 0
                oInvoice.TPS = 0
                oInvoice.Paye = False
                oInvoiceline.IDInvoice = oInvoiceData.Add(oInvoice)
            End If


            '--------------------------------------------
            ' Prend les valeurs pour les Credit par livraison et les Glaciers
            '--------------------------------------------
            Dim iQuantiteLivraison As Integer = If(IsDBNull(drOrganisation("QuantiteLivraison")), 0, CType(drOrganisation("QuantiteLivraison"), Int32?))
            Dim dblCreditParLivraison As Double = If(IsDBNull(drOrganisation("CreditParLivraison")), 0, CType(drOrganisation("CreditParLivraison"), Decimal?))
            Dim dblPrixGlacier As Double = If(IsDBNull(drOrganisation("MontantGlacierOrganisation")), 0, CType(drOrganisation("MontantGlacierOrganisation"), Decimal?))
            Dim iTotalGlacier As Integer = 0
            Dim dblTotalCredits As Double = oImportData.SelectTotalCredit(pDateFrom, pDateTo, iOrganisationID)
            Dim dblTotalDebit As Double = oImportData.SelectTotalDebit(pDateFrom, pDateTo, iOrganisationID)


            '----------------------------------------------------------------------------------------
            ' Genere la colonne montant de l'import pour la facturation
            '----------------------------------------------------------------------------------------
            dtImport = oImportData.SelectAllFromDateName(pDateFrom, pDateTo, iOrganisationID)

            If dtImport.Rows.Count > 0 Then
                iTotalImport = dtImport.Rows.Count
                For Each drImport In dtImport.Rows
                    oImport.ImportID = drImport("ImportID")

                    iTotalGlacier = iTotalGlacier + If(IsDBNull(drImport("NombreGlacier")), 0, CType(drImport("NombreGlacier"), Int32?))

                    If iTotalGlacier > 0 Then
                        oImport.MontantGlacierOrganisation = iTotalGlacier * If(IsDBNull(drOrganisation("MontantGlacierOrganisation")), 0, CType(drOrganisation("MontantGlacierOrganisation"), Decimal?))
                        dblGlacierGrandTotal = dblGlacierGrandTotal + oImport.MontantGlacierOrganisation
                        iGlacierGrandTotal = iGlacierGrandTotal + 1
                    Else
                        oImport.MontantGlacierOrganisation = 0
                    End If

                    dtHeureDepart = If(IsDBNull(drImport("HeureLivraison")), Nothing, CType(drImport("HeureLivraison"), DateTime?))
                    dblDistance = If(IsDBNull(drImport("Distance")), 0, drImport("Distance"))
                    dblDistance = Math.Round(dblDistance, 2)

                    dtCommissionKM = oCommissionKMData.SelectAllByKM(iOrganisationID, dblDistance)
                    If dtCommissionKM.Rows.Count = 1 Then
                        For Each drCommissionKM In dtCommissionKM.Rows
                            dblMontant = If(IsDBNull(drCommissionKM("Montant")), 0, CType(drCommissionKM("Montant"), Decimal?))
                            oImport.ImportID = drImport("ImportID")
                            oImport.Montant = dblMontant
                            oImportData.UpdateAmountByID(oImport)
                            dblMontant = 0
                            dblExtraKMGrandTotal = 0
                        Next
                    Else
                        Dim smessage = "La distance " & dblDistance.ToString.Trim & " Ne retourne aucunes valeur pour l'organisation " & drOrganisation("Organisation").ToString & ", " & vbLf
                        smessage = smessage & " Veuillez verifier les configuration calcul par kilometrage"
                        MessageBox.Show(smessage)
                        Return False
                        Exit Function
                    End If
                Next
            End If




            dtImportC = oImportData.SelectDeliveryByDate(pDateFrom, pDateTo, iOrganisationID)


            '----------------------------------------------------------------------------------------
            ' Livraisons
            '----------------------------------------------------------------------------------------
            Dim dPreparationDate As Date
            For Each drImportC In dtImportC.Rows

                oInvoiceline.OrganisationID = iOrganisationID
                oInvoiceline.Organisation = drOrganisation("Organisation")
                oInvoiceline.InvoiceNumber = oInvoice.InvoiceNumber
                oInvoiceline.IDInvoice = oInvoiceline.IDInvoice
                oInvoiceline.InvoiceDate = oInvoice.InvoiceDate
                dPreparationDate = drImportC("PreparationDate")
                oInvoiceline.DisplayDate = dPreparationDate.ToShortDateString
                oInvoiceline.DateFrom = pDateFrom
                oInvoiceline.DateTo = pDateTo
                oInvoiceline.Code = Nothing
                oInvoiceline.Debit = If(IsDBNull(drImportC("totalDebit")), 0, CType(drImportC("totalDebit"), Decimal?))
                oInvoiceline.Description = drImportC("TotalLivraison").ToString & " Liv"
                oInvoiceline.Glacier = 0
                oInvoiceline.NombreGlacier = 0
                oInvoiceline.Km = If(IsDBNull(drImportC("TotalKM")), 0, CType(drImportC("TotalKM"), Double?))
                oInvoiceline.Unite = 1
                oInvoiceline.Prix = If(IsDBNull(drImportC("TotalCommande")), 0, CType(drImportC("TotalCommande"), Decimal?))
                oInvoiceline.Montant = If(IsDBNull(drImportC("TotalCommande")), 0, CType(drImportC("TotalCommande"), Decimal?))
                oInvoiceline.IDPeriode = oInvoice.IDPeriode
                oInvoiceline.TypeAjout = 0
                oInvoiceline.Ajout = False
                oInvoiceline.IDInvoice = oInvoiceline.IDInvoice
                oInvoiceLineData.Add(oInvoiceline)

            Next


            Dim oooInvoiceline As New InvoiceLine


            '----------------------------------------------------------------------------------------
            ' Rabais par nb. livraisons
            '----------------------------------------------------------------------------------------
            Dim dblCreditParLivraisonTotal As Double = 0
            If (iTotalImport >= iQuantiteLivraison) And iQuantiteLivraison > 0 Then
                dblCreditParLivraisonTotal = iTotalImport * dblCreditParLivraison
                oooInvoiceline.OrganisationID = iOrganisationID
                oooInvoiceline.Organisation = drOrganisation("Organisation").ToString.Trim
                oooInvoiceline.InvoiceNumber = oInvoice.InvoiceNumber
                oooInvoiceline.InvoiceDate = oInvoice.InvoiceDate
                oooInvoiceline.DisplayDate = oInvoice.InvoiceDate
                oooInvoiceline.DateFrom = pDateFrom
                oooInvoiceline.DateTo = pDateTo
                oooInvoiceline.Code = Nothing
                oooInvoiceline.Credit = 0
                oooInvoiceline.Debit = 0
                oooInvoiceline.Description = "Rabais par nb. livraisons"
                oooInvoiceline.Glacier = 0
                oooInvoiceline.NombreGlacier = 0
                oooInvoiceline.Km = 0
                oooInvoiceline.KmExtra = 0
                oooInvoiceline.Unite = iTotalImport
                oooInvoiceline.Prix = dblCreditParLivraison
                oooInvoiceline.Montant = dblCreditParLivraisonTotal
                oooInvoiceline.IDPeriode = oInvoice.IDPeriode
                oooInvoiceline.IDInvoice = oInvoiceline.IDInvoice
                oooInvoiceline.TypeAjout = 2
                oooInvoiceline.Ajout = False
                oInvoiceLineData.Add(oooInvoiceline)
            End If



            '----------------------------------------------------------------------------------------
            ' Cadeau IDS
            '----------------------------------------------------------------------------------------
            Dim dblTotalCadeau As Double = 0

            dblTotalCadeau = oCadeauOrganisationData.SelectTotalAmountByOrganisation(pDateFrom, pDateTo, iOrganisationID)

            If dblTotalCadeau > 0 Then

                oooInvoiceline.OrganisationID = iOrganisationID
                oooInvoiceline.Organisation = drOrganisation("Organisation").ToString.Trim
                oooInvoiceline.InvoiceNumber = oInvoice.InvoiceNumber
                oooInvoiceline.InvoiceDate = oInvoice.InvoiceDate
                oooInvoiceline.DisplayDate = oInvoice.InvoiceDate
                oooInvoiceline.DateFrom = pDateFrom
                oooInvoiceline.DateTo = pDateTo
                oooInvoiceline.Code = Nothing
                oooInvoiceline.Credit = 0
                oooInvoiceline.Debit = 0
                oooInvoiceline.Description = "Cadeau IDS"
                oooInvoiceline.Glacier = 0
                oooInvoiceline.NombreGlacier = 0
                oooInvoiceline.Km = 0
                oooInvoiceline.KmExtra = 0
                oooInvoiceline.Unite = 1
                oooInvoiceline.Prix = dblTotalCadeau
                oooInvoiceline.Montant = dblTotalCadeau
                oooInvoiceline.IDPeriode = oInvoice.IDPeriode
                oooInvoiceline.IDInvoice = oInvoiceline.IDInvoice
                oooInvoiceline.TypeAjout = 2
                oooInvoiceline.Ajout = False
                oInvoiceLineData.Add(oooInvoiceline)
            End If


            '----------------------------------------------------------------------------------------
            ' Montant Mobilus
            '----------------------------------------------------------------------------------------
            Dim dblMobilus As Double = If(IsDBNull(drOrganisation("MontantMobilus")), 0, CType(drOrganisation("MontantMobilus"), Decimal?))

            If dblMobilus > 0 Then

                oooInvoiceline.OrganisationID = iOrganisationID
                oooInvoiceline.Organisation = drOrganisation("Organisation").ToString.Trim
                oooInvoiceline.InvoiceNumber = oInvoice.InvoiceNumber
                oooInvoiceline.InvoiceDate = oInvoice.InvoiceDate
                oooInvoiceline.DisplayDate = oInvoice.InvoiceDate
                oooInvoiceline.DateFrom = pDateFrom
                oooInvoiceline.DateTo = pDateTo
                oooInvoiceline.Code = Nothing
                oooInvoiceline.Credit = 0
                oooInvoiceline.Debit = 0
                oooInvoiceline.Description = "Charge Mobilus"
                oooInvoiceline.Glacier = 0
                oooInvoiceline.NombreGlacier = 0
                oooInvoiceline.Km = 0
                oooInvoiceline.KmExtra = 0
                oooInvoiceline.Unite = 1
                oooInvoiceline.Prix = dblMobilus
                oooInvoiceline.Montant = dblMobilus
                oooInvoiceline.IDPeriode = oInvoice.IDPeriode
                oooInvoiceline.IDInvoice = oInvoiceline.IDInvoice
                oooInvoiceline.TypeAjout = 0
                oooInvoiceline.Ajout = False
                oInvoiceLineData.Add(oooInvoiceline)
            End If


            Dim dblMontantPrixFixe As Double = If(IsDBNull(drOrganisation("MontantFixe")), 0, CType(drOrganisation("MontantFixe"), Decimal?))

            If dblMontantPrixFixe > 0 Then

                oooInvoiceline.OrganisationID = iOrganisationID
                oooInvoiceline.Organisation = drOrganisation("Organisation").ToString.Trim
                oooInvoiceline.InvoiceNumber = oInvoice.InvoiceNumber
                oooInvoiceline.InvoiceDate = oInvoice.InvoiceDate
                oooInvoiceline.DisplayDate = oInvoice.InvoiceDate
                oooInvoiceline.DateFrom = pDateFrom
                oooInvoiceline.DateTo = pDateTo
                oooInvoiceline.Code = Nothing
                oooInvoiceline.Credit = 0
                oooInvoiceline.Debit = 0
                oooInvoiceline.Description = "Montant de fin de semaine"
                oooInvoiceline.Glacier = 0
                oooInvoiceline.NombreGlacier = 0
                oooInvoiceline.Km = 0
                oooInvoiceline.KmExtra = 0
                oooInvoiceline.Unite = 1
                oooInvoiceline.Prix = dblMontantPrixFixe
                oooInvoiceline.Montant = dblMontantPrixFixe
                oooInvoiceline.IDPeriode = oInvoice.IDPeriode
                oooInvoiceline.IDInvoice = oInvoiceline.IDInvoice
                oooInvoiceline.TypeAjout = 0
                oooInvoiceline.Ajout = False
                oInvoiceLineData.Add(oooInvoiceline)
            End If


            Return True

        Catch ex As Exception
            MessageBox.Show(ex.Message, "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Return False
        End Try


    End Function


    Public Function ParKilometrageXtra(ByRef drOrganisation As DataRow, pDateFrom As DateTime, pDateTo As DateTime) As Boolean

        Try


            Dim oImport As New Import
            Dim dtImport As DataTable
            Dim drImport As DataRow
            Dim dtCommissionKM As DataTable
            Dim drCommissionKM As DataRow
            Dim dtHeureDepart As DateTime = Nothing
            Dim dblDistance As Double = 0
            Dim dblMontant As Double = 0
            Dim dblKMextra As Double = 0
            Dim dblExtraCharge As Double = 0
            Dim iTotalImport As Integer
            Dim oInvoiceline As New InvoiceLine
            Dim iOrganisationID As Integer
            Dim dblTotalMontant As Double = 0
            Dim dblTotalExtra As Double = 0
            Dim iTotalExtra As Integer = 0
            Dim dblTotalKmExtra As Double = 0
            Dim dblExtraKMGrandTotal As Double = 0
            Dim dblExtraChargeGrandTotal As Double = 0
            Dim iExtraGrandTotal As Integer = 0
            Dim dblGlacierGrandTotal As Double = 0
            Dim dblTotalDebits As Double = 0
            Dim dtImportC As New DataTable
            Dim iGlacierGrandTotal As Integer = 0
            Dim dtInvoice As DataTable
            Dim drInvoice As DataRow


            iOrganisationID = drOrganisation("OrganisationID")
            '--------------------------------------------
            ' Cree l'entree de la facture
            '--------------------------------------------


            ' check if invoice exist before recreating it
            dtInvoice = oInvoiceData.SelectAllByPeriodOrganisationID(go_Globals.IDPeriode, iOrganisationID)

            If dtInvoice.Rows.Count > 0 Then
                For Each drInvoice In dtInvoice.Rows
                    oInvoiceline.IDInvoice = drInvoice("IDInvoice")
                    oInvoice.IDInvoice = drInvoice("IDInvoice")
                    oInvoice.InvoiceNumber = drInvoice("InvoiceNumber")
                    oInvoice.IDPeriode = drInvoice("IDPeriode")
                    oInvoice.InvoiceDate = pDateTo.ToShortDateString
                    oInvoice.Paye = False
                    oInvoiceData.UpdateInvoiceDate(oInvoice, oInvoice)
                Next
            Else
                oInvoice.InvoiceDate = pDateTo.ToShortDateString
                oInvoice.IDPeriode = go_Globals.IDPeriode
                oInvoice.DateFrom = pDateFrom
                oInvoice.DateTo = pDateTo
                oInvoice.OrganisationID = iOrganisationID
                oInvoice.Organisation = drOrganisation("Organisation")
                oInvoice.InvoiceNumber = Me.generate_inv()
                oInvoice.Amount = 0
                oInvoice.TVQ = 0
                oInvoice.TPS = 0
                oInvoice.Paye = False
                oInvoiceline.IDInvoice = oInvoiceData.Add(oInvoice)
            End If
            '------------------------------------
            ' Prend les valeurs pour les Credit par livraison et les Glaciers
            '----------------------------------------------------------------------------------------
            Dim iQuantiteLivraison As Integer = If(IsDBNull(drOrganisation("QuantiteLivraison")), 0, CType(drOrganisation("QuantiteLivraison"), Int32?))
            Dim dblCreditParLivraison As Double = If(IsDBNull(drOrganisation("CreditParLivraison")), 0, CType(drOrganisation("CreditParLivraison"), Decimal?))
            Dim dblPrixGlacier As Double = If(IsDBNull(drOrganisation("MontantGlacierOrganisation")), 0, CType(drOrganisation("MontantGlacierOrganisation"), Decimal?))
            Dim iTotalGlacier As Integer = 0
            Dim dblTotalCredits As Double = oImportData.SelectTotalCredit(pDateFrom, pDateTo, iOrganisationID)
            Dim dblTotalDebit As Double = oImportData.SelectTotalDebit(pDateFrom, pDateTo, iOrganisationID)

            '----------------------------------------------------------------------------------------
            ' Genere la colonne montant de l'import pour la facturation
            '----------------------------------------------------------------------------------------
            dtImport = oImportData.SelectAllFromDateName(pDateFrom, pDateTo, iOrganisationID)

            If dtImport.Rows.Count > 0 Then
                iTotalImport = dtImport.Rows.Count
                For Each drImport In dtImport.Rows
                    oImport.ImportID = drImport("ImportID")

                    iTotalGlacier = iTotalGlacier + If(IsDBNull(drImport("NombreGlacier")), 0, CType(drImport("NombreGlacier"), Int32?))

                    If iTotalGlacier > 0 Then
                        iGlacierGrandTotal = iGlacierGrandTotal + 1
                        oImport.MontantGlacierOrganisation = iTotalGlacier * If(IsDBNull(drOrganisation("MontantGlacierOrganisation")), 0, CType(drOrganisation("MontantGlacierOrganisation"), Decimal?))
                        dblGlacierGrandTotal = dblGlacierGrandTotal + oImport.MontantGlacierOrganisation
                    Else
                        oImport.MontantGlacierOrganisation = 0
                    End If

                    dtHeureDepart = If(IsDBNull(drImport("HeureLivraison")), Nothing, CType(drImport("HeureLivraison"), DateTime?))
                    dblDistance = If(IsDBNull(drImport("Distance")), 0, drImport("Distance"))
                    dblDistance = Math.Round(dblDistance, 2)

                    dtCommissionKM = oCommissionKMData.SelectAllByKM(iOrganisationID, dblDistance)
                    If dtCommissionKM.Rows.Count = 1 Then
                        For Each drCommissionKM In dtCommissionKM.Rows
                            dblKMextra = If(IsDBNull(drCommissionKM("PerKMNumber")), 0, drCommissionKM("PerKMNumber"))
                            dblExtraCharge = If(IsDBNull(drCommissionKM("ExtraChargeParKM")), 0, CType(drCommissionKM("ExtraChargeParKM"), Decimal?))
                            dblMontant = If(IsDBNull(drCommissionKM("Montant")), 0, CType(drCommissionKM("Montant"), Decimal?))



                            If (dblExtraCharge > 0 And dblKMextra > 0) And (dblDistance > Math.Round(drCommissionKM("KMFrom"))) Then
                                oImport.ImportID = drImport("ImportID")
                                dblTotalKmExtra = Math.Round(dblDistance) - Math.Round(drCommissionKM("KMFrom"))
                                dblTotalKmExtra = dblTotalKmExtra / dblKMextra
                                dblTotalKmExtra = Math.Round(dblTotalKmExtra)
                                oImport.ExtraKM = dblTotalKmExtra
                                dblExtraKMGrandTotal = dblExtraKMGrandTotal + dblKMextra
                                oImport.Extra = dblTotalKmExtra * dblExtraCharge
                                dblExtraChargeGrandTotal = dblExtraChargeGrandTotal + oImport.Extra
                                oImport.Montant = If(IsDBNull(dblMontant), 0, CType(dblMontant, Decimal?))
                                iExtraGrandTotal = iExtraGrandTotal + 1




                            ElseIf (dblExtraCharge > 0 And dblKMextra = 0) And (dblDistance > drCommissionKM("KMFrom")) Then
                                oImport.ImportID = drImport("ImportID")
                                dblTotalKmExtra = Math.Round(dblDistance) - Math.Round(drCommissionKM("KMFrom"))
                                oImport.ExtraKM = dblTotalKmExtra
                                oImport.Extra = dblExtraCharge
                                dblExtraChargeGrandTotal = dblExtraChargeGrandTotal + oImport.Extra
                                dblExtraKMGrandTotal = dblExtraKMGrandTotal + dblKMextra
                                oImport.Montant = If(IsDBNull(dblMontant), 0, CType(dblMontant, Decimal?))
                                iExtraGrandTotal = iExtraGrandTotal + 1
                            Else
                                oImport.ImportID = drImport("ImportID")
                                oImport.Extra = 0
                                oImport.ExtraKM = 0
                                oImport.Montant = If(IsDBNull(dblMontant), 0, CType(dblMontant, Decimal?))

                            End If

                            oImportData.UpdateAmountExtraGlacierKMByID(oImport)

                            dblKMextra = 0
                            dblExtraCharge = 0
                            dblMontant = 0
                        Next
                    Else
                        Dim smessage = "La distance " & dblDistance.ToString.Trim & " Ne retourne aucunes valeur pour l'organisation " & drOrganisation("Organisation").ToString & ", " & vbLf
                        smessage = smessage & "Veuillez verifier les configuration calcul par kilometrage + Extra"
                        MessageBox.Show(smessage)
                        Return False
                        Exit Function
                    End If
                Next
            End If

            dtImportC = oImportData.SelectDeliveryByDate(pDateFrom, pDateTo, iOrganisationID)


            If iExtraGrandTotal > 0 Then
                oInvoiceline.OrganisationID = iOrganisationID
                oInvoiceline.Organisation = drOrganisation("Organisation").ToString.Trim
                oInvoiceline.IDInvoice = oInvoiceline.IDInvoice
                oInvoiceline.InvoiceNumber = oInvoice.InvoiceNumber
                oInvoiceline.InvoiceDate = oInvoice.InvoiceDate
                oInvoiceline.DisplayDate = oInvoice.InvoiceDate
                oInvoiceline.DateFrom = pDateFrom
                oInvoiceline.DateTo = pDateTo
                oInvoiceline.Code = Nothing
                oInvoiceline.Credit = 0
                oInvoiceline.Debit = 0
                oInvoiceline.Description = "Extras"
                oInvoiceline.Glacier = 0
                oInvoiceline.NombreGlacier = 0
                oInvoiceline.Km = dblExtraKMGrandTotal
                oInvoiceline.KmExtra = dblExtraKMGrandTotal
                oInvoiceline.Unite = 1
                oInvoiceline.Prix = dblExtraChargeGrandTotal
                oInvoiceline.Montant = dblExtraChargeGrandTotal
                oInvoiceline.IDPeriode = oInvoice.IDPeriode
                oInvoiceline.TypeAjout = 0
                oInvoiceline.Ajout = False
                oInvoiceLineData.Add(oInvoiceline)

            End If

            '----------------------------------------------------------------------------------------
            ' Livraisons
            '----------------------------------------------------------------------------------------
            Dim dPreparationDate As Date
            For Each drImportC In dtImportC.Rows

                oInvoiceline.OrganisationID = iOrganisationID
                oInvoiceline.Organisation = drOrganisation("Organisation")
                oInvoiceline.InvoiceNumber = oInvoice.InvoiceNumber
                oInvoiceline.IDInvoice = oInvoiceline.IDInvoice
                oInvoiceline.InvoiceDate = oInvoice.InvoiceDate
                dPreparationDate = drImportC("PreparationDate")
                oInvoiceline.DisplayDate = dPreparationDate.ToShortDateString
                oInvoiceline.DateFrom = pDateFrom
                oInvoiceline.DateTo = pDateTo
                oInvoiceline.Code = Nothing
                oInvoiceline.Debit = If(IsDBNull(drImportC("totalDebit")), 0, CType(drImportC("totalDebit"), Decimal?))
                oInvoiceline.Description = drImportC("TotalLivraison").ToString & " Liv"
                oInvoiceline.Glacier = 0
                oInvoiceline.NombreGlacier = 0
                oInvoiceline.Km = If(IsDBNull(drImportC("TotalKM")), 0, CType(drImportC("TotalKM"), Double?))
                oInvoiceline.Unite = 1
                oInvoiceline.Prix = If(IsDBNull(drImportC("TotalCommande")), 0, CType(drImportC("TotalCommande"), Decimal?))
                oInvoiceline.Montant = If(IsDBNull(drImportC("TotalCommande")), 0, CType(drImportC("TotalCommande"), Decimal?))
                oInvoiceline.IDPeriode = oInvoice.IDPeriode
                oInvoiceline.IDInvoice = oInvoiceline.IDInvoice
                oInvoiceline.TypeAjout = 0
                oInvoiceline.Ajout = False
                oInvoiceLineData.Add(oInvoiceline)

            Next


            '----------------------------------------------------------------------------------------
            ' Glaciers
            '----------------------------------------------------------------------------------------
            Dim oooInvoiceline As New InvoiceLine

            '----------------------------------------------------------------------------------------
            ' Rabais par nb. livraisons
            '----------------------------------------------------------------------------------------
            Dim dblCreditParLivraisonTotal As Double = 0
            If (iTotalImport >= iQuantiteLivraison) And iQuantiteLivraison > 0 Then
                dblCreditParLivraisonTotal = iTotalImport * dblCreditParLivraison
                oooInvoiceline.OrganisationID = iOrganisationID
                oooInvoiceline.Organisation = drOrganisation("Organisation").ToString.Trim
                oooInvoiceline.InvoiceNumber = oInvoice.InvoiceNumber
                oooInvoiceline.InvoiceDate = oInvoice.InvoiceDate
                oooInvoiceline.DisplayDate = oInvoice.InvoiceDate
                oooInvoiceline.DateFrom = pDateFrom
                oooInvoiceline.DateTo = pDateTo
                oooInvoiceline.Code = Nothing
                oooInvoiceline.Credit = 0
                oooInvoiceline.Debit = 0
                oooInvoiceline.Description = "Rabais par nb. livraisons"
                oooInvoiceline.Glacier = 0
                oooInvoiceline.NombreGlacier = 0
                oooInvoiceline.Km = 0
                oooInvoiceline.KmExtra = 0
                oooInvoiceline.Unite = iTotalImport
                oooInvoiceline.Prix = dblCreditParLivraison
                oooInvoiceline.Montant = dblCreditParLivraisonTotal
                oooInvoiceline.IDPeriode = oInvoice.IDPeriode
                oooInvoiceline.IDInvoice = oInvoiceline.IDInvoice
                oooInvoiceline.TypeAjout = 2
                oooInvoiceline.Ajout = False
                oInvoiceLineData.Add(oooInvoiceline)
            End If



            '----------------------------------------------------------------------------------------
            ' Cadeau IDS
            '----------------------------------------------------------------------------------------
            Dim dblTotalCadeau As Double = 0

            dblTotalCadeau = oCadeauOrganisationData.SelectTotalAmountByOrganisation(pDateFrom, pDateTo, iOrganisationID)

            If dblTotalCadeau > 0 Then

                oooInvoiceline.OrganisationID = iOrganisationID
                oooInvoiceline.Organisation = drOrganisation("Organisation").ToString.Trim
                oooInvoiceline.InvoiceNumber = oInvoice.InvoiceNumber
                oooInvoiceline.InvoiceDate = oInvoice.InvoiceDate
                oooInvoiceline.DisplayDate = oInvoice.InvoiceDate
                oooInvoiceline.DateFrom = pDateFrom
                oooInvoiceline.DateTo = pDateTo
                oooInvoiceline.Code = Nothing
                oooInvoiceline.Credit = 0
                oooInvoiceline.Debit = 0
                oooInvoiceline.Description = "Cadeau IDS"
                oooInvoiceline.Glacier = 0
                oooInvoiceline.NombreGlacier = 0
                oooInvoiceline.Km = 0
                oooInvoiceline.KmExtra = 0
                oooInvoiceline.Unite = 1
                oooInvoiceline.Prix = dblTotalCadeau
                oooInvoiceline.Montant = dblTotalCadeau
                oooInvoiceline.IDPeriode = oInvoice.IDPeriode
                oooInvoiceline.IDInvoice = oInvoiceline.IDInvoice
                oooInvoiceline.TypeAjout = 2
                oooInvoiceline.Ajout = False
                oInvoiceLineData.Add(oooInvoiceline)
            End If

            Dim dblMobilus As Double = If(IsDBNull(drOrganisation("MontantMobilus")), 0, CType(drOrganisation("MontantMobilus"), Decimal?))

            If dblMobilus > 0 Then

                oooInvoiceline.OrganisationID = iOrganisationID
                oooInvoiceline.Organisation = drOrganisation("Organisation").ToString.Trim
                oooInvoiceline.InvoiceNumber = oInvoice.InvoiceNumber
                oooInvoiceline.InvoiceDate = oInvoice.InvoiceDate
                oooInvoiceline.DisplayDate = oInvoice.InvoiceDate
                oooInvoiceline.DateFrom = pDateFrom
                oooInvoiceline.DateTo = pDateTo
                oooInvoiceline.Code = Nothing
                oooInvoiceline.Credit = 0
                oooInvoiceline.Debit = 0
                oooInvoiceline.Description = "Charge Mobilus"
                oooInvoiceline.Glacier = 0
                oooInvoiceline.NombreGlacier = 0
                oooInvoiceline.Km = 0
                oooInvoiceline.KmExtra = 0
                oooInvoiceline.Unite = 1
                oooInvoiceline.Prix = dblMobilus
                oooInvoiceline.Montant = dblMobilus
                oooInvoiceline.IDPeriode = oInvoice.IDPeriode
                oooInvoiceline.IDInvoice = oInvoiceline.IDInvoice
                oooInvoiceline.TypeAjout = 0
                oooInvoiceline.Ajout = False
                oInvoiceLineData.Add(oooInvoiceline)
            End If


            Dim dblMontantPrixFixe As Double = If(IsDBNull(drOrganisation("MontantFixe")), 0, CType(drOrganisation("MontantFixe"), Decimal?))

            If dblMontantPrixFixe > 0 Then

                oooInvoiceline.OrganisationID = iOrganisationID
                oooInvoiceline.Organisation = drOrganisation("Organisation").ToString.Trim
                oooInvoiceline.InvoiceNumber = oInvoice.InvoiceNumber
                oooInvoiceline.InvoiceDate = oInvoice.InvoiceDate
                oooInvoiceline.DisplayDate = oInvoice.InvoiceDate
                oooInvoiceline.DateFrom = pDateFrom
                oooInvoiceline.DateTo = pDateTo
                oooInvoiceline.Code = Nothing
                oooInvoiceline.Credit = 0
                oooInvoiceline.Debit = 0
                oooInvoiceline.Description = "Montant de fin de semaine"
                oooInvoiceline.Glacier = 0
                oooInvoiceline.NombreGlacier = 0
                oooInvoiceline.Km = 0
                oooInvoiceline.KmExtra = 0
                oooInvoiceline.Unite = 1
                oooInvoiceline.Prix = dblMontantPrixFixe
                oooInvoiceline.Montant = dblMontantPrixFixe
                oooInvoiceline.IDPeriode = oInvoice.IDPeriode
                oooInvoiceline.IDInvoice = oInvoiceline.IDInvoice
                oooInvoiceline.TypeAjout = 0
                oooInvoiceline.Ajout = False
                oInvoiceLineData.Add(oooInvoiceline)
            End If



            Return True


        Catch ex As Exception
            MessageBox.Show(ex.Message, "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Return False
        End Try

    End Function



    Public Function ParNombreKMFixeExtra(ByRef drOrganisation As DataRow, pDateFrom As DateTime, pDateTo As DateTime) As Boolean



        Try


            Dim oImport As New Import
            Dim dtImport As DataTable
            Dim drImport As DataRow
            Dim dtCommissionKM As DataTable
            Dim drCommissionKM As DataRow
            Dim dtHeureDepart As DateTime = Nothing
            Dim dblDistance As Double = 0
            Dim dblMontant As Double = 0
            Dim dblKMextra As Double = 0
            Dim dblExtraCharge As Double = 0
            Dim iTotalImport As Integer
            Dim oInvoiceline As New InvoiceLine
            Dim iOrganisationID As Integer
            Dim dblTotalMontant As Double = 0
            Dim dblTotalExtra As Double = 0
            Dim iTotalExtra As Integer = 0
            Dim dblTotalKmExtra As Double = 0
            Dim dblExtraChargeGrandTotal As Double = 0
            Dim iExtraGrandTotal As Integer = 0
            Dim dblExtraKMGrandTotal As Double = 0
            Dim dblGlacierGrandTotal As Double = 0
            Dim dblTotalDebits As Double = 0
            Dim dtImportC As New DataTable
            Dim iGlacierGrandTotal As Integer = 0
            Dim dblPrixCourant As Double = 0
            Dim dblExtraChargePrix As Double = 0

            Dim dtInvoice As DataTable
            Dim drInvoice As DataRow


            iOrganisationID = drOrganisation("OrganisationID")
            '--------------------------------------------
            ' Cree l'entree de la facture
            '--------------------------------------------


            ' check if invoice exist before recreating it
            dtInvoice = oInvoiceData.SelectAllByPeriodOrganisationID(go_Globals.IDPeriode, iOrganisationID)

            If dtInvoice.Rows.Count > 0 Then
                For Each drInvoice In dtInvoice.Rows
                    oInvoiceline.IDInvoice = drInvoice("IDInvoice")
                    oInvoice.IDInvoice = drInvoice("IDInvoice")
                    oInvoice.InvoiceNumber = drInvoice("InvoiceNumber")
                    oInvoice.IDPeriode = drInvoice("IDPeriode")
                    oInvoice.InvoiceDate = pDateTo.ToShortDateString
                    oInvoice.Paye = False
                    oInvoiceData.UpdateInvoiceDate(oInvoice, oInvoice)
                Next
            Else
                oInvoice.InvoiceDate = pDateTo.ToShortDateString
                oInvoice.IDPeriode = go_Globals.IDPeriode
                oInvoice.DateFrom = pDateFrom
                oInvoice.DateTo = pDateTo
                oInvoice.OrganisationID = iOrganisationID
                oInvoice.Organisation = drOrganisation("Organisation")
                oInvoice.InvoiceNumber = Me.generate_inv()
                oInvoice.Amount = 0
                oInvoice.TVQ = 0
                oInvoice.TPS = 0
                oInvoice.Paye = False

                oInvoiceline.IDInvoice = oInvoiceData.Add(oInvoice)
            End If



            '----------------------------------------------------------------------------------------
            ' Prend les valeurs pour les Credit par livraison et les Glaciers
            '----------------------------------------------------------------------------------------
            Dim iQuantiteLivraison As Integer = If(IsDBNull(drOrganisation("QuantiteLivraison")), 0, CType(drOrganisation("QuantiteLivraison"), Int32?))
            Dim dblCreditParLivraison As Double = If(IsDBNull(drOrganisation("CreditParLivraison")), 0, CType(drOrganisation("CreditParLivraison"), Decimal?))
            Dim dblPrixGlacier As Double = If(IsDBNull(drOrganisation("MontantGlacierOrganisation")), 0, CType(drOrganisation("MontantGlacierOrganisation"), Decimal?))
            Dim iTotalGlacier As Integer = 0
            Dim dblTotalCredits As Double = oImportData.SelectTotalCredit(pDateFrom, pDateTo, iOrganisationID)
            Dim dblTotalDebit As Double = oImportData.SelectTotalDebit(pDateFrom, pDateTo, iOrganisationID)




            '----------------------------------------------------------------------------------------
            ' Genere la colonne montant de l'import pour la facturation
            '----------------------------------------------------------------------------------------
            dtImport = oImportData.SelectAllFromDateName(pDateFrom, pDateTo, iOrganisationID)

            If dtImport.Rows.Count > 0 Then
                iTotalImport = dtImport.Rows.Count
                For Each drImport In dtImport.Rows
                    oImport.ImportID = drImport("ImportID")

                    iTotalGlacier = iTotalGlacier + If(IsDBNull(drImport("NombreGlacier")), 0, CType(drImport("NombreGlacier"), Int32?))

                    If iTotalGlacier > 0 Then
                        oImport.MontantGlacierOrganisation = iTotalGlacier * If(IsDBNull(drOrganisation("MontantGlacierOrganisation")), 0, CType(drOrganisation("MontantGlacierOrganisation"), Decimal?))
                        dblGlacierGrandTotal = dblGlacierGrandTotal + oImport.MontantGlacierOrganisation
                    Else
                        oImport.MontantGlacierOrganisation = 0
                    End If

                    dtHeureDepart = If(IsDBNull(drImport("HeureLivraison")), Nothing, CType(drImport("HeureLivraison"), DateTime?))
                    dblDistance = If(IsDBNull(drImport("Distance")), 0, drImport("Distance"))
                    dblDistance = Math.Round(dblDistance, 2)

                    dtCommissionKM = oCommissionKMData.SelectAllByKM(iOrganisationID, dblDistance)
                    If dtCommissionKM.Rows.Count = 1 Then
                        For Each drCommissionKM In dtCommissionKM.Rows
                            dblKMextra = If(IsDBNull(drCommissionKM("PerKMNumber")), 0, drCommissionKM("PerKMNumber"))
                            dblExtraCharge = If(IsDBNull(drCommissionKM("ExtraChargeParKM")), 0, CType(drCommissionKM("ExtraChargeParKM"), Decimal?))
                            dblMontant = If(IsDBNull(drCommissionKM("Montant")), 0, CType(drCommissionKM("Montant"), Decimal?))
                            dblExtraChargePrix = dblExtraCharge
                            If (dblExtraCharge > 0 And dblKMextra > 0) And (dblDistance > Math.Round(drCommissionKM("KMTo"))) Then
                                oImport.ImportID = drImport("ImportID")
                                dblTotalKmExtra = Math.Round(dblDistance) - Math.Round(drCommissionKM("KMTo"))
                                dblTotalKmExtra = dblTotalKmExtra / dblKMextra
                                dblTotalKmExtra = Math.Round(dblTotalKmExtra)
                                oImport.ExtraKM = dblTotalKmExtra
                                dblExtraKMGrandTotal = dblExtraKMGrandTotal + oImport.ExtraKM
                                oImport.Extra = dblTotalKmExtra * dblExtraCharge
                                dblExtraChargeGrandTotal = dblExtraChargeGrandTotal + oImport.Extra
                                oImport.Montant = If(IsDBNull(dblMontant), 0, CType(dblMontant, Decimal?))
                                iExtraGrandTotal = iExtraGrandTotal + 1

                            ElseIf (dblExtraCharge > 0 And dblKMextra = 0) And (dblDistance > Math.Round(drCommissionKM("KMFrom"))) Then
                                oImport.ImportID = drImport("ImportID")
                                dblTotalKmExtra = Math.Round(dblDistance) - Math.Round(drCommissionKM("KMFrom"))
                                oImport.ExtraKM = dblTotalKmExtra
                                dblExtraKMGrandTotal = dblExtraKMGrandTotal + oImport.ExtraKM
                                oImport.Extra = dblExtraCharge
                                dblExtraChargeGrandTotal = dblExtraChargeGrandTotal + oImport.Extra
                                oImport.Montant = If(IsDBNull(dblMontant), 0, CType(dblMontant, Decimal?))
                                iExtraGrandTotal = iExtraGrandTotal + 1

                            Else
                                oImport.ImportID = drImport("ImportID")
                                oImport.Extra = 0
                                oImport.ExtraKM = 0
                                oImport.Montant = If(IsDBNull(dblMontant), 0, CType(dblMontant, Decimal?))

                            End If

                            dblPrixCourant = oImport.Montant
                            oImportData.UpdateAmountExtraGlacierKMByID(oImport)

                            dblKMextra = 0
                            dblExtraCharge = 0
                            dblMontant = 0
                        Next
                    Else
                        Dim smessage = "La distance " & dblDistance.ToString.Trim & " Ne retourne aucunes valeur pour l'organisation " & drOrganisation("Organisation").ToString & ", " & vbLf
                        smessage = smessage & "Veuillez verifier les configuration calcul par kilometrage fixe + Extra"
                        MessageBox.Show(smessage)

                        Return False
                        Exit Function
                    End If
                Next
            End If

            dtImportC = oImportData.SelectDeliveryByDate(pDateFrom, pDateTo, iOrganisationID)


            If iExtraGrandTotal > 0 Then
                oInvoiceline.OrganisationID = iOrganisationID
                oInvoiceline.Organisation = drOrganisation("Organisation").ToString.Trim
                oInvoiceline.IDInvoice = oInvoiceline.IDInvoice
                oInvoiceline.InvoiceNumber = oInvoice.InvoiceNumber
                oInvoiceline.InvoiceDate = oInvoice.InvoiceDate
                oInvoiceline.DisplayDate = oInvoice.InvoiceDate
                oInvoiceline.DateFrom = pDateFrom
                oInvoiceline.DateTo = pDateTo
                oInvoiceline.Code = Nothing
                oInvoiceline.Credit = 0
                oInvoiceline.Debit = 0
                oInvoiceline.Description = "Extras"
                oInvoiceline.Glacier = 0
                oInvoiceline.NombreGlacier = 0
                oInvoiceline.Km = dblExtraKMGrandTotal
                oInvoiceline.KmExtra = dblExtraKMGrandTotal
                oInvoiceline.Unite = 1
                oInvoiceline.Prix = dblExtraChargeGrandTotal
                oInvoiceline.Montant = dblExtraChargeGrandTotal
                oInvoiceline.IDPeriode = oInvoice.IDPeriode
                oInvoiceline.TypeAjout = 0
                oInvoiceline.Ajout = False
                oInvoiceLineData.Add(oInvoiceline)

            End If

            '----------------------------------------------------------------------------------------
            ' Livraisons
            '----------------------------------------------------------------------------------------
            Dim dPreparationDate As Date
            For Each drImportC In dtImportC.Rows

                oInvoiceline.OrganisationID = iOrganisationID
                oInvoiceline.Organisation = drOrganisation("Organisation")
                oInvoiceline.InvoiceNumber = oInvoice.InvoiceNumber
                oInvoiceline.IDInvoice = oInvoiceline.IDInvoice
                oInvoiceline.InvoiceDate = oInvoice.InvoiceDate
                dPreparationDate = drImportC("PreparationDate")
                oInvoiceline.DisplayDate = dPreparationDate.ToShortDateString
                oInvoiceline.DateFrom = pDateFrom
                oInvoiceline.DateTo = pDateTo
                oInvoiceline.Code = Nothing
                oInvoiceline.Debit = If(IsDBNull(drImportC("totalDebit")), 0, CType(drImportC("totalDebit"), Decimal?))
                oInvoiceline.Description = drImportC("TotalLivraison").ToString & " Liv"
                oInvoiceline.Glacier = 0
                oInvoiceline.NombreGlacier = 0
                oInvoiceline.Km = If(IsDBNull(drImportC("TotalKM")), 0, CType(drImportC("TotalKM"), Double?))
                oInvoiceline.Unite = 1
                oInvoiceline.Prix = If(IsDBNull(drImportC("TotalCommande")), 0, CType(drImportC("TotalCommande"), Decimal?))
                oInvoiceline.Montant = If(IsDBNull(drImportC("TotalCommande")), 0, CType(drImportC("TotalCommande"), Decimal?))
                oInvoiceline.IDPeriode = oInvoice.IDPeriode
                oInvoiceline.IDInvoice = oInvoiceline.IDInvoice
                oInvoiceline.TypeAjout = 0
                oInvoiceline.Ajout = False
                oInvoiceLineData.Add(oInvoiceline)

            Next


            '----------------------------------------------------------------------------------------
            ' Glaciers
            '----------------------------------------------------------------------------------------
            Dim oooInvoiceline As New InvoiceLine


            '----------------------------------------------------------------------------------------
            ' Rabais par nb. livraisons
            '----------------------------------------------------------------------------------------
            Dim dblCreditParLivraisonTotal As Double = 0
            If (iTotalImport >= iQuantiteLivraison) And iQuantiteLivraison > 0 Then
                dblCreditParLivraisonTotal = iTotalImport * dblCreditParLivraison
                oooInvoiceline.OrganisationID = iOrganisationID
                oooInvoiceline.Organisation = drOrganisation("Organisation").ToString.Trim
                oooInvoiceline.InvoiceNumber = oInvoice.InvoiceNumber
                oooInvoiceline.InvoiceDate = oInvoice.InvoiceDate
                oooInvoiceline.DisplayDate = oInvoice.InvoiceDate
                oooInvoiceline.DateFrom = pDateFrom
                oooInvoiceline.DateTo = pDateTo
                oooInvoiceline.Code = Nothing
                oooInvoiceline.Credit = 0
                oooInvoiceline.Debit = 0
                oooInvoiceline.Description = "Rabais par nb. livraisons"
                oooInvoiceline.Glacier = 0
                oooInvoiceline.NombreGlacier = 0
                oooInvoiceline.Km = 0
                oooInvoiceline.KmExtra = 0
                oooInvoiceline.Unite = iTotalImport
                oooInvoiceline.Prix = dblCreditParLivraison
                oooInvoiceline.Montant = dblCreditParLivraisonTotal
                oooInvoiceline.IDPeriode = oInvoice.IDPeriode
                oooInvoiceline.IDInvoice = oInvoiceline.IDInvoice
                oooInvoiceline.TypeAjout = 2
                oooInvoiceline.Ajout = False
                oInvoiceLineData.Add(oooInvoiceline)
            End If



            '----------------------------------------------------------------------------------------
            ' Cadeau IDS
            '----------------------------------------------------------------------------------------
            Dim dblTotalCadeau As Double = 0

            dblTotalCadeau = oCadeauOrganisationData.SelectTotalAmountByOrganisation(pDateFrom, pDateTo, iOrganisationID)

            If dblTotalCadeau > 0 Then

                oooInvoiceline.OrganisationID = iOrganisationID
                oooInvoiceline.Organisation = drOrganisation("Organisation").ToString.Trim
                oooInvoiceline.InvoiceNumber = oInvoice.InvoiceNumber
                oooInvoiceline.InvoiceDate = oInvoice.InvoiceDate
                oooInvoiceline.DisplayDate = oInvoice.InvoiceDate
                oooInvoiceline.DateFrom = pDateFrom
                oooInvoiceline.DateTo = pDateTo
                oooInvoiceline.Code = Nothing
                oooInvoiceline.Credit = 0
                oooInvoiceline.Debit = 0
                oooInvoiceline.Description = "Cadeau IDS"
                oooInvoiceline.Glacier = 0
                oooInvoiceline.NombreGlacier = 0
                oooInvoiceline.Km = 0
                oooInvoiceline.KmExtra = 0
                oooInvoiceline.Unite = 1
                oooInvoiceline.Prix = dblTotalCadeau
                oooInvoiceline.Montant = dblTotalCadeau
                oooInvoiceline.IDPeriode = oInvoice.IDPeriode
                oooInvoiceline.IDInvoice = oInvoiceline.IDInvoice
                oooInvoiceline.TypeAjout = 2
                oooInvoiceline.Ajout = False
                oInvoiceLineData.Add(oooInvoiceline)
            End If


            '----------------------------------------------------------------------------------------
            ' Montant Mobilus
            '----------------------------------------------------------------------------------------

            Dim dblMobilus As Double = If(IsDBNull(drOrganisation("MontantMobilus")), 0, CType(drOrganisation("MontantMobilus"), Decimal?))

            If dblMobilus > 0 Then

                oooInvoiceline.OrganisationID = iOrganisationID
                oooInvoiceline.Organisation = drOrganisation("Organisation").ToString.Trim
                oooInvoiceline.InvoiceNumber = oInvoice.InvoiceNumber
                oooInvoiceline.InvoiceDate = oInvoice.InvoiceDate
                oooInvoiceline.DisplayDate = oInvoice.InvoiceDate
                oooInvoiceline.DateFrom = pDateFrom
                oooInvoiceline.DateTo = pDateTo
                oooInvoiceline.Code = Nothing
                oooInvoiceline.Credit = 0
                oooInvoiceline.Debit = 0
                oooInvoiceline.Description = "Charge Mobilus"
                oooInvoiceline.Glacier = 0
                oooInvoiceline.NombreGlacier = 0
                oooInvoiceline.Km = 0
                oooInvoiceline.KmExtra = 0
                oooInvoiceline.Unite = 1
                oooInvoiceline.Prix = dblMobilus
                oooInvoiceline.Montant = dblMobilus
                oooInvoiceline.IDPeriode = oInvoice.IDPeriode
                oooInvoiceline.IDInvoice = oInvoiceline.IDInvoice
                oooInvoiceline.TypeAjout = 0
                oooInvoiceline.Ajout = False
                oInvoiceLineData.Add(oooInvoiceline)
            End If



            Dim dblMontantPrixFixe As Double = If(IsDBNull(drOrganisation("MontantFixe")), 0, CType(drOrganisation("MontantFixe"), Decimal?))

            If dblMontantPrixFixe > 0 Then

                oooInvoiceline.OrganisationID = iOrganisationID
                oooInvoiceline.Organisation = drOrganisation("Organisation").ToString.Trim
                oooInvoiceline.InvoiceNumber = oInvoice.InvoiceNumber
                oooInvoiceline.InvoiceDate = oInvoice.InvoiceDate
                oooInvoiceline.DisplayDate = oInvoice.InvoiceDate
                oooInvoiceline.DateFrom = pDateFrom
                oooInvoiceline.DateTo = pDateTo
                oooInvoiceline.Code = Nothing
                oooInvoiceline.Credit = 0
                oooInvoiceline.Debit = 0
                oooInvoiceline.Description = "Montant de fin de semaine"
                oooInvoiceline.Glacier = 0
                oooInvoiceline.NombreGlacier = 0
                oooInvoiceline.Km = 0
                oooInvoiceline.KmExtra = 0
                oooInvoiceline.Unite = 1
                oooInvoiceline.Prix = dblMontantPrixFixe
                oooInvoiceline.Montant = dblMontantPrixFixe
                oooInvoiceline.IDPeriode = oInvoice.IDPeriode
                oooInvoiceline.IDInvoice = oInvoiceline.IDInvoice
                oooInvoiceline.TypeAjout = 0
                oooInvoiceline.Ajout = False
                oInvoiceLineData.Add(oooInvoiceline)
            End If




            Return True

        Catch ex As Exception
            MessageBox.Show(ex.Message, "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Return False
        End Try


    End Function

    Private Function GetJourSemaine(d As DateTime, ci As Globalization.CultureInfo) As String
        Dim sDay As String
        sDay = ci.DateTimeFormat.GetDayName(d.Day)
        Return sDay
    End Function


    Private Function GetMonthName(d As DateTime, ci As Globalization.CultureInfo) As String

        Dim sMonth As String


        sMonth = ci.DateTimeFormat.GetMonthName(d.Month)

        Select Case sMonth
            Case "janvier"
                sMonth = "Janv"
            Case "février"
                sMonth = "Fév"
            Case "mars"
                sMonth = "Mars"
            Case "avril"
                sMonth = "Avr"
            Case "mai"
                sMonth = "Mai"
            Case "juin"
                sMonth = "Juin"
            Case "juillet"
                sMonth = "Juil"
            Case "août"
                sMonth = "Août"
            Case "septembre"
                sMonth = "Sept"
            Case "octobre"
                sMonth = "Oct"
            Case "novembre"
                sMonth = "Nov"
            Case "décembre"
                sMonth = "Déc"

        End Select

        Return sMonth
    End Function
    Public Function GenererFactures(pDateFrom As DateTime, pDateTo As DateTime, oProgressbar As DevExpress.XtraEditors.ProgressBarBaseControl) As Boolean

        Dim frenchCultureInfo = New Globalization.CultureInfo("fr-CA")
        Dim dtorganisation As DataTable
        Dim drOrganisation As DataRow
        Dim dtInvoice As DataTable
        Dim drInvoice As DataRow
        Dim dtInvoiceLine As DataTable
        Dim drInvoiceLine As DataRow
        Dim strPeriode As String = vbNullString



        Dim dDateFrom As Date = pDateFrom
        Dim dDateTo As Date = pDateTo
        Dim oFacturation As New Facturation
        Dim irow As Integer = 0
        Dim dtInvoiceDate As Date
        SpreadsheetInfo.SetLicense("ERDC-FN4O-YKYN-4DBM")
        Dim dblCredits As Double = 0
        Dim dblDebits As Double = 0
        Dim dblTotalAmount As Double = 0
        Dim dblCadeauOrganisation As Double = 0
        Dim oinv As New Facturation


        '--------------------------------------------
        ' Genere la periode
        '--------------------------------------------
        strPeriode = pDateFrom.Day & "-" & pDateTo.Day & " " & GetMonthName(pDateTo, frenchCultureInfo) & " " & pDateTo.Year.ToString

        dtorganisation = oOrganisationData.SelectAll()
        For Each drOrganisation In dtorganisation.Rows



            Dim sExcelPath As String = Application.StartupPath() & "\Facture.xlsx"
            Dim ef As ExcelFile = ExcelFile.Load(sExcelPath)

            '--------------------------------------------
            ' Genere le header de la facture
            '--------------------------------------------
            ef.Worksheets(0).Cells(2, 2).Value = IIf(drOrganisation.IsNull("Organisation"), "", drOrganisation("Organisation").ToString.Trim)
            ef.Worksheets(0).Cells(3, 2).Value = IIf(drOrganisation.IsNull("Adresse"), "", drOrganisation("Adresse").ToString.Trim)
            ef.Worksheets(0).Cells(4, 2).Value = IIf(drOrganisation.IsNull("Telephone"), "", drOrganisation("Telephone").ToString.Trim)
            ef.Worksheets(0).Cells(5, 2).Value = IIf(drOrganisation.IsNull("Email"), "", drOrganisation("Email").ToString.Trim) & ", " & IIf(drOrganisation.IsNull("Email1"), "", drOrganisation("Email1").ToString.Trim) & ", " & IIf(drOrganisation.IsNull("Email2"), "", drOrganisation("Email2").ToString.Trim)


            '--------------------------------------------
            ' Selectionne les info de la facture
            '--------------------------------------------
            dtInvoice = oInvoiceData.SelectInvoiceByDate(dDateFrom, pDateTo, drOrganisation("OrganisationID"))
            If dtInvoice.Rows.Count > 0 Then

                For Each drInvoice In dtInvoice.Rows


                    dtInvoiceDate = drInvoice("InvoiceDate")

                    irow = 10
                    ef.Worksheets(0).Cells(4, 6).Value = dtInvoiceDate.Day.ToString & " " & GetMonthName(dtInvoiceDate, frenchCultureInfo) & " " & dtInvoiceDate.Year
                    ef.Worksheets(0).Cells(5, 6).Value = drInvoice("InvoiceNumber").ToString
                    ef.Worksheets(0).Cells(8, 1).Value = strPeriode.ToString


                    '--------------------------------------------
                    ' Selectionne le detail de la facture
                    '--------------------------------------------
                    dtInvoiceLine = oInvoiceLineData.SelectAllByDate(dDateFrom, pDateTo, drOrganisation("OrganisationID"))
                    If dtInvoiceLine.Rows.Count > 0 Then
                        For Each drInvoiceLine In dtInvoiceLine.Rows

                            Dim dtInvDate As Date = drInvoiceLine("InvoiceDate")
                            ef.Worksheets(0).Cells(irow, 1).Value = dtInvDate.Day.ToString & " " & GetMonthName(dtInvDate, frenchCultureInfo)
                            ef.Worksheets(0).Cells(irow, 2).Value = If(IsDBNull(drInvoiceLine("Description")), " ", drInvoiceLine("Description"))
                            ef.Worksheets(0).Cells(irow, 3).Value = If(IsDBNull(drInvoiceLine("Km")), 0, drInvoiceLine("Km"))
                            ef.Worksheets(0).Cells(irow, 4).Value = IIf(drInvoiceLine.IsNull("Unite"), 0, drInvoiceLine("Unite"))
                            ef.Worksheets(0).Cells(irow, 5).Value = If(IsDBNull(drInvoiceLine("Prix")), 0, CType(drInvoiceLine("Prix"), Decimal?))
                            ef.Worksheets(0).Cells(irow, 6).Value = If(IsDBNull(drInvoiceLine("Montant")), 0, CType(drInvoiceLine("Montant"), Decimal?))

                            dblTotalAmount = dblTotalAmount + If(IsDBNull(drInvoiceLine("Montant")), 0, CType(drInvoiceLine("Montant"), Decimal?))
                            dblCredits = dblCredits + If(IsDBNull(drInvoiceLine("Credit")), 0, CType(drInvoiceLine("Credit"), Decimal?))
                            dblDebits = dblDebits + If(IsDBNull(drInvoiceLine("Debit")), 0, CType(drInvoiceLine("Debit"), Decimal?))

                            irow = irow + 1



                        Next
                    End If

                Next

            End If

            ef.Worksheets(0).Cells(26, 6).Value = If(IsDBNull(dblTotalAmount), 0, CType(dblTotalAmount, Decimal?))
            ef.Worksheets(0).Cells(27, 6).Value = If(IsDBNull(dblDebits), 0, CType(dblDebits, Decimal?))
            ef.Worksheets(0).Cells(28, 6).Value = If(IsDBNull(dblCredits), 0, CType(dblCredits, Decimal?))
            ef.Worksheets(0).Cells(28, 6).Value = If(IsDBNull(dblCadeauOrganisation), 0, CType(dblCadeauOrganisation, Decimal?))


            Dim sFile As String
            Dim Icount As Integer = 0
            ef.Worksheets(0).PrintOptions.FitWorksheetWidthToPages = 1
            sFile = drOrganisation("Organisation").ToString.Trim & "-" & dtInvoiceDate.Day.ToString & " " & GetMonthName(dtInvoiceDate, frenchCultureInfo) & " " & dtInvoiceDate.Year.ToString & ".pdf"
            sFile = sFile.Replace("'", "")

CHECKFILE:
            ' if the file name already exist, create a copy
            If System.IO.File.Exists(go_Globals.LocationFacture & "\" & sFile) Then
                Icount = Icount + 1
                sFile = drOrganisation("Organisation").ToString.Trim & "-" & dtInvoiceDate.Day.ToString & " " & GetMonthName(dtInvoiceDate, frenchCultureInfo) & " " & dtInvoiceDate.Year.ToString & "_COPY" & Icount.ToString & ".pdf"
                GoTo CHECKFILE
            End If


            ef.Save(go_Globals.LocationFacture & "\" & sFile)
            ef = Nothing

            dblCredits = 0
            dblDebits = 0
            dblTotalAmount = 0
            dblCadeauOrganisation = 0

        Next




    End Function

    Public Function ParJour(ByRef drOrganisation As DataRow, pDateFrom As DateTime, pDateTo As DateTime) As Boolean


        Try


            Dim dtHeureDepart As DateTime = Nothing
            Dim dtHeureFin As DateTime = Nothing
            Dim dblMontant As Double = 0
            Dim iOrganisationID As Integer
            Dim dtInvoice As DataTable
            Dim drInvoice As DataRow
            Dim oHoraireJourData As New HoraireJourData
            Dim dtHoraireJour As DataTable


            iOrganisationID = drOrganisation("OrganisationID")
            '--------------------------------------------
            ' Cree l'entree de la facture
            '--------------------------------------------
            Dim oInvoiceline As New InvoiceLine

            ' check if invoice exist before recreating it
            dtInvoice = oInvoiceData.SelectAllByPeriodOrganisationID(go_Globals.IDPeriode, iOrganisationID)

            If dtInvoice.Rows.Count > 0 Then
                For Each drInvoice In dtInvoice.Rows
                    oInvoiceline.IDInvoice = drInvoice("IDInvoice")
                    oInvoice.IDInvoice = drInvoice("IDInvoice")
                    oInvoice.InvoiceNumber = drInvoice("InvoiceNumber")
                    oInvoice.IDPeriode = drInvoice("IDPeriode")
                    oInvoice.InvoiceDate = pDateTo.ToShortDateString
                    oInvoice.Paye = False
                    'oInvoiceData.UpdateInvoiceDate(oInvoice, oInvoice)
                Next
            Else
                oInvoice.InvoiceDate = pDateTo.ToShortDateString
                oInvoice.IDPeriode = go_Globals.IDPeriode
                oInvoice.DateFrom = pDateFrom
                oInvoice.DateTo = pDateTo
                oInvoice.OrganisationID = iOrganisationID
                oInvoice.Organisation = drOrganisation("Organisation")
                oInvoice.InvoiceNumber = Me.generate_inv()
                oInvoice.Amount = 0
                oInvoice.TVQ = 0
                oInvoice.TPS = 0
                oInvoice.Paye = False
                oInvoiceline.IDInvoice = oInvoiceData.Add(oInvoice)
            End If

            ' select tous les horaires pour cet organisation
            dtHoraireJour = oHoraireJourData.SelectAllByIDOrganisationParJour(iOrganisationID)

            If dtHoraireJour.Rows.Count > 0 Then
                For Each drdtHoraireJour In dtHoraireJour.Rows

                    Dim oInvoiceline2 As New InvoiceLine
                    Dim dDisplayDate As DateTime = oInvoice.InvoiceDate
                    Dim oLivreur As New livreurs
                    Dim dblHeures As Double = 0

                    oLivreur.LivreurID = drdtHoraireJour("IDLivreur")
                    oLivreur = oLivreurData.Select_Record(oLivreur)

                    oInvoiceline2.OrganisationID = iOrganisationID
                    oInvoiceline2.Organisation = drOrganisation("Organisation").ToString.Trim
                    oInvoiceline2.InvoiceNumber = oInvoice.InvoiceNumber
                    oInvoiceline2.IDInvoice = oInvoiceline2.IDInvoice
                    oInvoiceline2.InvoiceDate = dDisplayDate.ToShortDateString
                    oInvoiceline2.DisplayDate = dDisplayDate.ToShortDateString
                    oInvoiceline2.DateFrom = pDateFrom
                    oInvoiceline2.DateTo = pDateTo
                    oInvoiceline2.Code = Nothing
                    oInvoiceline2.Debit = 0
                    oInvoiceline2.Description = oLivreur.Livreur.ToString.Trim & "/Jour"
                    oInvoiceline2.Glacier = 0
                    oInvoiceline2.NombreGlacier = 0
                    oInvoiceline2.Km = 0
                    oInvoiceline2.Unite = Convert.ToDouble(drdtHoraireJour("TotalDay"))
                    oInvoiceline2.Prix = Convert.ToDouble(drdtHoraireJour("MontantOrganisation"))
                    oInvoiceline2.Montant = Convert.ToDouble(drdtHoraireJour("MontantTotal"))
                    oInvoiceline2.IDPeriode = oInvoice.IDPeriode
                    oInvoiceline2.TypeAjout = 0
                    oInvoiceline2.Ajout = False
                    oInvoiceline2.IDInvoice = oInvoiceline.IDInvoice
                    oInvoiceLineData.Add(oInvoiceline2)

                Next

            End If





            '----------------------------------------------------------------------------------------
            ' Cadeau IDS
            '----------------------------------------------------------------------------------------
            Dim dblTotalCadeau As Double = 0
            Dim oooInvoiceline As New InvoiceLine
            dblTotalCadeau = oCadeauOrganisationData.SelectTotalAmountByOrganisation(pDateFrom, pDateTo, iOrganisationID)

            If dblTotalCadeau > 0 Then

                oooInvoiceline.OrganisationID = iOrganisationID
                oooInvoiceline.Organisation = drOrganisation("Organisation").ToString.Trim
                oooInvoiceline.InvoiceNumber = oInvoice.InvoiceNumber
                oooInvoiceline.InvoiceDate = oInvoice.InvoiceDate
                oooInvoiceline.DisplayDate = oInvoice.InvoiceDate
                oooInvoiceline.DateFrom = pDateFrom
                oooInvoiceline.DateTo = pDateTo
                oooInvoiceline.Code = Nothing
                oooInvoiceline.Credit = 0
                oooInvoiceline.Debit = 0
                oooInvoiceline.Description = "Cadeau IDS"
                oooInvoiceline.Glacier = 0
                oooInvoiceline.NombreGlacier = 0
                oooInvoiceline.Km = 0
                oooInvoiceline.KmExtra = 0
                oooInvoiceline.Unite = 1
                oooInvoiceline.Prix = dblTotalCadeau
                oooInvoiceline.Montant = dblTotalCadeau
                oooInvoiceline.IDPeriode = oInvoice.IDPeriode
                oooInvoiceline.IDInvoice = oInvoiceline.IDInvoice
                oooInvoiceline.TypeAjout = 2
                oooInvoiceline.Ajout = False
                oInvoiceLineData.Add(oooInvoiceline)
            End If


            '----------------------------------------------------------------------------------------
            ' Montant Mobilus
            '----------------------------------------------------------------------------------------
            Dim dblMobilus As Double = If(IsDBNull(drOrganisation("MontantMobilus")), 0, CType(drOrganisation("MontantMobilus"), Decimal?))

            If dblMobilus > 0 Then

                oooInvoiceline.OrganisationID = iOrganisationID
                oooInvoiceline.Organisation = drOrganisation("Organisation").ToString.Trim
                oooInvoiceline.InvoiceNumber = oInvoice.InvoiceNumber
                oooInvoiceline.InvoiceDate = oInvoice.InvoiceDate
                oooInvoiceline.DisplayDate = oInvoice.InvoiceDate
                oooInvoiceline.DateFrom = pDateFrom
                oooInvoiceline.DateTo = pDateTo
                oooInvoiceline.Code = Nothing
                oooInvoiceline.Credit = 0
                oooInvoiceline.Debit = 0
                oooInvoiceline.Description = "Charge Mobilus"
                oooInvoiceline.Glacier = 0
                oooInvoiceline.NombreGlacier = 0
                oooInvoiceline.Km = 0
                oooInvoiceline.KmExtra = 0
                oooInvoiceline.Unite = 1
                oooInvoiceline.Prix = dblMobilus
                oooInvoiceline.Montant = dblMobilus
                oooInvoiceline.IDPeriode = oInvoice.IDPeriode
                oooInvoiceline.IDInvoice = oInvoiceline.IDInvoice
                oooInvoiceline.TypeAjout = 0
                oooInvoiceline.Ajout = False
                oInvoiceLineData.Add(oooInvoiceline)
            End If


            Dim dblMontantPrixFixe As Double = If(IsDBNull(drOrganisation("MontantFixe")), 0, CType(drOrganisation("MontantFixe"), Decimal?))

            If dblMontantPrixFixe > 0 Then

                oooInvoiceline.OrganisationID = iOrganisationID
                oooInvoiceline.Organisation = drOrganisation("Organisation").ToString.Trim
                oooInvoiceline.InvoiceNumber = oInvoice.InvoiceNumber
                oooInvoiceline.InvoiceDate = oInvoice.InvoiceDate
                oooInvoiceline.DisplayDate = oInvoice.InvoiceDate
                oooInvoiceline.DateFrom = pDateFrom
                oooInvoiceline.DateTo = pDateTo
                oooInvoiceline.Code = Nothing
                oooInvoiceline.Credit = 0
                oooInvoiceline.Debit = 0
                oooInvoiceline.Description = "Montant de fin de semaine"
                oooInvoiceline.Glacier = 0
                oooInvoiceline.NombreGlacier = 0
                oooInvoiceline.Km = 0
                oooInvoiceline.KmExtra = 0
                oooInvoiceline.Unite = 1
                oooInvoiceline.Prix = dblMontantPrixFixe
                oooInvoiceline.Montant = dblMontantPrixFixe
                oooInvoiceline.IDPeriode = oInvoice.IDPeriode
                oooInvoiceline.IDInvoice = oInvoiceline.IDInvoice
                oooInvoiceline.TypeAjout = 0
                oooInvoiceline.Ajout = False
                oInvoiceLineData.Add(oooInvoiceline)
            End If


            Return True

        Catch ex As Exception
            MessageBox.Show(ex.Message, "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Return False
        End Try
    End Function
    Public Function ParHeure(ByRef drOrganisation As DataRow, pDateFrom As DateTime, pDateTo As DateTime) As Boolean


        Try


            Dim dtHeureDepart As DateTime = Nothing
            Dim dtHeureFin As DateTime = Nothing
            Dim dblMontant As Double = 0
            Dim iOrganisationID As Integer
            Dim dtInvoice As DataTable
            Dim drInvoice As DataRow
            Dim oHoraireData As New HoraireData
            Dim dtHoraire As DataTable
            Dim drHoraire As DataRow


            iOrganisationID = drOrganisation("OrganisationID")
            '--------------------------------------------
            ' Cree l'entree de la facture
            '--------------------------------------------
            Dim oInvoiceline As New InvoiceLine

            ' check if invoice exist before recreating it
            dtInvoice = oInvoiceData.SelectAllByPeriodOrganisationID(go_Globals.IDPeriode, iOrganisationID)

            If dtInvoice.Rows.Count > 0 Then
                For Each drInvoice In dtInvoice.Rows
                    oInvoiceline.IDInvoice = drInvoice("IDInvoice")
                    oInvoice.IDInvoice = drInvoice("IDInvoice")
                    oInvoice.InvoiceNumber = drInvoice("InvoiceNumber")
                    oInvoice.IDPeriode = drInvoice("IDPeriode")
                    oInvoice.InvoiceDate = pDateTo.ToShortDateString
                    oInvoice.Paye = False
                    'oInvoiceData.UpdateInvoiceDate(oInvoice, oInvoice)
                Next
            Else
                oInvoice.InvoiceDate = pDateTo.ToShortDateString
                oInvoice.IDPeriode = go_Globals.IDPeriode
                oInvoice.DateFrom = pDateFrom
                oInvoice.DateTo = pDateTo
                oInvoice.OrganisationID = iOrganisationID
                oInvoice.Organisation = drOrganisation("Organisation")
                oInvoice.InvoiceNumber = Me.generate_inv()
                oInvoice.Amount = 0
                oInvoice.TVQ = 0
                oInvoice.TPS = 0
                oInvoice.Paye = False
                oInvoiceline.IDInvoice = oInvoiceData.Add(oInvoice)
            End If

            ' select tous les horaires pour cet organisation
            dtHoraire = oHoraireData.SelectAllByIDOrganisationParHeure(iOrganisationID)

            If dtHoraire.Rows.Count > 0 Then
                For Each drHoraire In dtHoraire.Rows

                    Dim oInvoiceline2 As New InvoiceLine
                    Dim dDisplayDate As DateTime = oInvoice.InvoiceDate
                    Dim oLivreur As New livreurs
                    Dim dblHeures As Double = 0

                    oLivreur.LivreurID = drHoraire("IDLivreur")
                    oLivreur = oLivreurData.Select_Record(oLivreur)

                    oInvoiceline2.OrganisationID = iOrganisationID
                    oInvoiceline2.Organisation = drOrganisation("Organisation").ToString.Trim
                    oInvoiceline2.InvoiceNumber = oInvoice.InvoiceNumber
                    oInvoiceline2.IDInvoice = oInvoice.IDInvoice
                    oInvoiceline2.InvoiceDate = dDisplayDate.ToShortDateString
                    oInvoiceline2.DisplayDate = dDisplayDate.ToShortDateString
                    oInvoiceline2.DateFrom = pDateFrom
                    oInvoiceline2.DateTo = pDateTo
                    oInvoiceline2.Code = Nothing
                    oInvoiceline2.Debit = 0

                    If Not IsNothing(oLivreur) Then
                        oInvoiceline2.Description = oLivreur.Livreur.ToString.Trim & "/Heure"

                    Else
                        oInvoiceline2.Description = drHoraire("Livreur").ToString.Trim & "/Heure"


                    End If

                    oInvoiceline2.Glacier = 0
                    oInvoiceline2.NombreGlacier = 0
                    oInvoiceline2.Km = 0
                    oInvoiceline2.Unite = Convert.ToDouble(drHoraire("TotalHour"))
                    oInvoiceline2.Prix = Convert.ToDouble(drHoraire("MontantOrganisation"))
                    oInvoiceline2.Montant = Convert.ToDouble(drHoraire("MontantTotal"))
                    oInvoiceline2.IDPeriode = oInvoice.IDPeriode
                    oInvoiceline2.TypeAjout = 0
                    oInvoiceline2.Ajout = False
                    oInvoiceline2.IDInvoice = oInvoiceline.IDInvoice
                    oInvoiceLineData.Add(oInvoiceline2)

                Next

            End If

            Dim oooInvoiceline As New InvoiceLine





            '----------------------------------------------------------------------------------------
            ' Cadeau IDS
            '----------------------------------------------------------------------------------------
            Dim dblTotalCadeau As Double = 0

            dblTotalCadeau = oCadeauOrganisationData.SelectTotalAmountByOrganisation(pDateFrom, pDateTo, iOrganisationID)

            If dblTotalCadeau > 0 Then

                oooInvoiceline.OrganisationID = iOrganisationID
                oooInvoiceline.Organisation = drOrganisation("Organisation").ToString.Trim
                oooInvoiceline.InvoiceNumber = oInvoice.InvoiceNumber
                oooInvoiceline.InvoiceDate = oInvoice.InvoiceDate
                oooInvoiceline.DisplayDate = oInvoice.InvoiceDate
                oooInvoiceline.DateFrom = pDateFrom
                oooInvoiceline.DateTo = pDateTo
                oooInvoiceline.Code = Nothing
                oooInvoiceline.Credit = 0
                oooInvoiceline.Debit = 0
                oooInvoiceline.Description = "Cadeau IDS"
                oooInvoiceline.Glacier = 0
                oooInvoiceline.NombreGlacier = 0
                oooInvoiceline.Km = 0
                oooInvoiceline.KmExtra = 0
                oooInvoiceline.Unite = 1
                oooInvoiceline.Prix = dblTotalCadeau
                oooInvoiceline.Montant = dblTotalCadeau
                oooInvoiceline.IDPeriode = oInvoice.IDPeriode
                oooInvoiceline.IDInvoice = oInvoiceline.IDInvoice
                oooInvoiceline.TypeAjout = 2
                oooInvoiceline.Ajout = False
                oInvoiceLineData.Add(oooInvoiceline)
            End If


            '----------------------------------------------------------------------------------------
            ' Montant Mobilus
            '----------------------------------------------------------------------------------------
            Dim dblMobilus As Double = If(IsDBNull(drOrganisation("MontantMobilus")), 0, CType(drOrganisation("MontantMobilus"), Decimal?))

            If dblMobilus > 0 Then

                oooInvoiceline.OrganisationID = iOrganisationID
                oooInvoiceline.Organisation = drOrganisation("Organisation").ToString.Trim
                oooInvoiceline.InvoiceNumber = oInvoice.InvoiceNumber
                oooInvoiceline.InvoiceDate = oInvoice.InvoiceDate
                oooInvoiceline.DisplayDate = oInvoice.InvoiceDate
                oooInvoiceline.DateFrom = pDateFrom
                oooInvoiceline.DateTo = pDateTo
                oooInvoiceline.Code = Nothing
                oooInvoiceline.Credit = 0
                oooInvoiceline.Debit = 0
                oooInvoiceline.Description = "Charge Mobilus"
                oooInvoiceline.Glacier = 0
                oooInvoiceline.NombreGlacier = 0
                oooInvoiceline.Km = 0
                oooInvoiceline.KmExtra = 0
                oooInvoiceline.Unite = 1
                oooInvoiceline.Prix = dblMobilus
                oooInvoiceline.Montant = dblMobilus
                oooInvoiceline.IDPeriode = oInvoice.IDPeriode
                oooInvoiceline.IDInvoice = oInvoiceline.IDInvoice
                oooInvoiceline.TypeAjout = 0
                oooInvoiceline.Ajout = False
                oInvoiceLineData.Add(oooInvoiceline)
            End If


            Dim dblMontantPrixFixe As Double = If(IsDBNull(drOrganisation("MontantFixe")), 0, CType(drOrganisation("MontantFixe"), Decimal?))

            If dblMontantPrixFixe > 0 Then

                oooInvoiceline.OrganisationID = iOrganisationID
                oooInvoiceline.Organisation = drOrganisation("Organisation").ToString.Trim
                oooInvoiceline.InvoiceNumber = oInvoice.InvoiceNumber
                oooInvoiceline.InvoiceDate = oInvoice.InvoiceDate
                oooInvoiceline.DisplayDate = oInvoice.InvoiceDate
                oooInvoiceline.DateFrom = pDateFrom
                oooInvoiceline.DateTo = pDateTo
                oooInvoiceline.Code = Nothing
                oooInvoiceline.Credit = 0
                oooInvoiceline.Debit = 0
                oooInvoiceline.Description = "Montant de fin de semaine"
                oooInvoiceline.Glacier = 0
                oooInvoiceline.NombreGlacier = 0
                oooInvoiceline.Km = 0
                oooInvoiceline.KmExtra = 0
                oooInvoiceline.Unite = 1
                oooInvoiceline.Prix = dblMontantPrixFixe
                oooInvoiceline.Montant = dblMontantPrixFixe
                oooInvoiceline.IDPeriode = oInvoice.IDPeriode
                oooInvoiceline.IDInvoice = oInvoiceline.IDInvoice
                oooInvoiceline.TypeAjout = 0
                oooInvoiceline.Ajout = False
                oInvoiceLineData.Add(oooInvoiceline)
            End If



            Return True

        Catch ex As Exception
            MessageBox.Show(ex.Message, "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Return False
        End Try
    End Function

End Class
