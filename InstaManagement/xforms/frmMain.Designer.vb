﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmMain
    Inherits DevExpress.XtraBars.Ribbon.RibbonForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMain))
        Dim GridFormatRule1 As DevExpress.XtraGrid.GridFormatRule = New DevExpress.XtraGrid.GridFormatRule()
        Dim FormatConditionRuleValue1 As DevExpress.XtraEditors.FormatConditionRuleValue = New DevExpress.XtraEditors.FormatConditionRuleValue()
        Dim GridFormatRule2 As DevExpress.XtraGrid.GridFormatRule = New DevExpress.XtraGrid.GridFormatRule()
        Dim FormatConditionRuleValue2 As DevExpress.XtraEditors.FormatConditionRuleValue = New DevExpress.XtraEditors.FormatConditionRuleValue()
        Dim GridFormatRule3 As DevExpress.XtraGrid.GridFormatRule = New DevExpress.XtraGrid.GridFormatRule()
        Dim FormatConditionRuleValue3 As DevExpress.XtraEditors.FormatConditionRuleValue = New DevExpress.XtraEditors.FormatConditionRuleValue()
        Me.Distance = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.IsMobilus = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RibbonControl = New DevExpress.XtraBars.Ribbon.RibbonControl()
        Me.BarButtonItem1 = New DevExpress.XtraBars.BarButtonItem()
        Me.bOrganisations = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem3 = New DevExpress.XtraBars.BarButtonItem()
        Me.bMiseaJour = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem5 = New DevExpress.XtraBars.BarButtonItem()
        Me.bFermer = New DevExpress.XtraBars.BarButtonItem()
        Me.bLivreurs = New DevExpress.XtraBars.BarButtonItem()
        Me.bFermer2 = New DevExpress.XtraBars.BarButtonItem()
        Me.SkinRibbonGalleryBarItem1 = New DevExpress.XtraBars.SkinRibbonGalleryBarItem()
        Me.bSettings = New DevExpress.XtraBars.BarButtonItem()
        Me.bFactures = New DevExpress.XtraBars.BarButtonItem()
        Me.bPayes = New DevExpress.XtraBars.BarButtonItem()
        Me.bPrintAll = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem4 = New DevExpress.XtraBars.BarButtonItem()
        Me.bConsolidation = New DevExpress.XtraBars.BarButtonItem()
        Me.bVendeurs = New DevExpress.XtraBars.BarButtonItem()
        Me.bAchat = New DevExpress.XtraBars.BarButtonItem()
        Me.bVente = New DevExpress.XtraBars.BarButtonItem()
        Me.BarSubItem1 = New DevExpress.XtraBars.BarSubItem()
        Me.BarButtonItem7 = New DevExpress.XtraBars.BarButtonItem()
        Me.bEtats = New DevExpress.XtraBars.BarSubItem()
        Me.bEtatDesComptes = New DevExpress.XtraBars.BarButtonItem()
        Me.bEtatDesVentes = New DevExpress.XtraBars.BarButtonItem()
        Me.bEtatDesAchats = New DevExpress.XtraBars.BarButtonItem()
        Me.bLivraisonsParRestaurant = New DevExpress.XtraBars.BarButtonItem()
        Me.bRapportTaxesLivreurs = New DevExpress.XtraBars.BarButtonItem()
        Me.btnRapportTaxeIDS = New DevExpress.XtraBars.BarButtonItem()
        Me.btnRapportTaxePayeLivreur = New DevExpress.XtraBars.BarButtonItem()
        Me.BarSubItem3 = New DevExpress.XtraBars.BarSubItem()
        Me.BarSubItem4 = New DevExpress.XtraBars.BarSubItem()
        Me.BarSubItem2 = New DevExpress.XtraBars.BarSubItem()
        Me.BarSubItem5 = New DevExpress.XtraBars.BarSubItem()
        Me.BarButtonItem8 = New DevExpress.XtraBars.BarButtonItem()
        Me.bVentes = New DevExpress.XtraBars.BarButtonItem()
        Me.bAchats = New DevExpress.XtraBars.BarButtonItem()
        Me.BarSubItem6 = New DevExpress.XtraBars.BarSubItem()
        Me.cbYears = New DevExpress.XtraBars.BarEditItem()
        Me.RepositoryItemLookUpEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit()
        Me.bAjouterUneCommande = New DevExpress.XtraBars.BarButtonItem()
        Me.bImprimerFactures = New DevExpress.XtraBars.BarButtonItem()
        Me.bImprimerCommission = New DevExpress.XtraBars.BarButtonItem()
        Me.bImprimerCommissionsVendeurs = New DevExpress.XtraBars.BarButtonItem()
        Me.btnGenererFacturation = New DevExpress.XtraBars.BarButtonItem()
        Me.bGenererPaye = New DevExpress.XtraBars.BarButtonItem()
        Me.btnGenererFacturationVendeurs2 = New DevExpress.XtraBars.BarButtonItem()
        Me.bLivraisonParRestaurant = New DevExpress.XtraBars.BarButtonItem()
        Me.bLivraisonsParLivreurs = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem17 = New DevExpress.XtraBars.BarButtonItem()
        Me.chkPRINT = New DevExpress.XtraBars.BarCheckItem()
        Me.bGenererLaPaye = New DevExpress.XtraBars.BarButtonItem()
        Me.bImprimerCommissions = New DevExpress.XtraBars.BarButtonItem()
        Me.bImprimerCommisssions = New DevExpress.XtraBars.BarButtonItem()
        Me.bGenererLesCommission = New DevExpress.XtraBars.BarButtonItem()
        Me.bLivraisonParLivreur = New DevExpress.XtraBars.BarButtonItem()
        Me.bCommisssion = New DevExpress.XtraBars.BarButtonItem()
        Me.bFournisseurs = New DevExpress.XtraBars.BarButtonItem()
        Me.bImprimerCommissionRapport = New DevExpress.XtraBars.BarButtonItem()
        Me.bPrêts = New DevExpress.XtraBars.BarButtonItem()
        Me.bEpargnes = New DevExpress.XtraBars.BarButtonItem()
        Me.bResidences = New DevExpress.XtraBars.BarButtonItem()
        Me.bRapportTaxeLivreur = New DevExpress.XtraBars.BarSubItem()
        Me.bCalendrier = New DevExpress.XtraBars.BarButtonItem()
        Me.bLivraisonParRestaurant2 = New DevExpress.XtraBars.BarButtonItem()
        Me.RibbonPage2 = New DevExpress.XtraBars.Ribbon.RibbonPage()
        Me.RibbonPageGroup2 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.RibbonPageGroup6 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.RibbonPageGroup7 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.RibbonPageGroup8 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.RibbonPageGroup9 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.RibbonPageGroup3 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.RibbonPage1 = New DevExpress.XtraBars.Ribbon.RibbonPage()
        Me.RibbonPageGroup1 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.RibbonPageGroup4 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.RibbonPageGroup5 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
        Me.RibbonStatusBar = New DevExpress.XtraBars.Ribbon.RibbonStatusBar()
        Me.rgbiSkins2 = New DevExpress.XtraBars.SkinRibbonGalleryBarItem()
        Me.LayoutControl1 = New DevExpress.XtraLayout.LayoutControl()
        Me.tabMain = New DevExpress.XtraTab.XtraTabControl()
        Me.XtraTabPage1 = New DevExpress.XtraTab.XtraTabPage()
        Me.LayoutControl3 = New DevExpress.XtraLayout.LayoutControl()
        Me.bResetTaxationLivreurPaye = New DevExpress.XtraEditors.SimpleButton()
        Me.bPrintTaxationLivreurPaye = New DevExpress.XtraEditors.SimpleButton()
        Me.bEmailTaxationLivreurPaye = New DevExpress.XtraEditors.SimpleButton()
        Me.cbRapportRevenuPaye = New DevExpress.XtraEditors.CheckedComboBoxEdit()
        Me.bResetTaxationLivreur = New DevExpress.XtraEditors.SimpleButton()
        Me.bPrintTaxationLivreur = New DevExpress.XtraEditors.SimpleButton()
        Me.bEmailTaxationLivreur = New DevExpress.XtraEditors.SimpleButton()
        Me.bAnnulerCommissions = New DevExpress.XtraEditors.SimpleButton()
        Me.bPrintCommissions = New DevExpress.XtraEditors.SimpleButton()
        Me.bEnvoyerCommissionVendeur = New DevExpress.XtraEditors.SimpleButton()
        Me.bEnvoyerLivraisonsRestaurants = New DevExpress.XtraEditors.SimpleButton()
        Me.bEnvoyerEtatCompte = New DevExpress.XtraEditors.SimpleButton()
        Me.bEnvoyerPaye = New DevExpress.XtraEditors.SimpleButton()
        Me.bEnvoyerFacture = New DevExpress.XtraEditors.SimpleButton()
        Me.bPrintLivraisonR = New DevExpress.XtraEditors.SimpleButton()
        Me.bPrintEtat = New DevExpress.XtraEditors.SimpleButton()
        Me.bPrintPaye = New DevExpress.XtraEditors.SimpleButton()
        Me.bPrintFacture = New DevExpress.XtraEditors.SimpleButton()
        Me.optTypePeriode = New DevExpress.XtraEditors.RadioGroup()
        Me.bResetLivraisonsRestaurant = New DevExpress.XtraEditors.SimpleButton()
        Me.bResetPeriode = New DevExpress.XtraEditors.SimpleButton()
        Me.bResetEtatdeCompte = New DevExpress.XtraEditors.SimpleButton()
        Me.cbAnnee = New DevExpress.XtraEditors.LookUpEdit()
        Me.bResetDate = New DevExpress.XtraEditors.SimpleButton()
        Me.bConsolidationDate = New DevExpress.XtraEditors.SimpleButton()
        Me.cbFactures = New DevExpress.XtraEditors.CheckedComboBoxEdit()
        Me.cbPayes = New DevExpress.XtraEditors.CheckedComboBoxEdit()
        Me.Calendar = New DevExpress.XtraEditors.Controls.CalendarControl()
        Me.ProgressBarControl1 = New DevExpress.XtraEditors.ProgressBarControl()
        Me.bFiltrerDate = New DevExpress.XtraEditors.SimpleButton()
        Me.txtMobilusXLS = New DevExpress.XtraEditors.TextEdit()
        Me.DateEdit1 = New DevExpress.XtraEditors.DateEdit()
        Me.bImportMobilus = New DevExpress.XtraEditors.SimpleButton()
        Me.navbarImageCollectionLarge3 = New DevExpress.Utils.ImageCollection(Me.components)
        Me.bSelectfile = New DevExpress.XtraEditors.SimpleButton()
        Me.bSupprimerI = New DevExpress.XtraEditors.SimpleButton()
        Me.cbFichierImport = New DevExpress.XtraEditors.LookUpEdit()
        Me.bAnnulerFichierImport = New DevExpress.XtraEditors.SimpleButton()
        Me.tbDateFrom = New DevExpress.XtraEditors.DateEdit()
        Me.tbDateTo = New DevExpress.XtraEditors.DateEdit()
        Me.cbPeriodes = New DevExpress.XtraEditors.LookUpEdit()
        Me.bAnnulerPayes = New DevExpress.XtraEditors.SimpleButton()
        Me.bAnnulerFactures = New DevExpress.XtraEditors.SimpleButton()
        Me.cbEtatsDeComptes = New DevExpress.XtraEditors.CheckedComboBoxEdit()
        Me.cbLivraisonRestaurants = New DevExpress.XtraEditors.CheckedComboBoxEdit()
        Me.cbCommissions = New DevExpress.XtraEditors.CheckedComboBoxEdit()
        Me.cbRapportTaxation = New DevExpress.XtraEditors.CheckedComboBoxEdit()
        Me.LayoutControlGroup6 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutBas = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem1 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem10 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem17 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem2 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem26 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem22 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem23 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem28 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem3 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlGroup1 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlGroup8 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem9 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem32 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem27 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlGroup3 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem5 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem6 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem48 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem14 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem13 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutDroite = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutEtat = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem42 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutEtatLivraisonRestaurant = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem69 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutFactures = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutPayes = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem24 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem25 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem19 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem21 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem35 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem47 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem64 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem72 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem74 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem75 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutCommissions = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem37 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem46 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem51 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutCommissions1 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem63 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem65 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.bResetTaxationLivreurs = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem20 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem68 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem71 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem73 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem40 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem6 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.EmptySpaceItem7 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.XtraTabPage2 = New DevExpress.XtraTab.XtraTabPage()
        Me.LayoutControl4 = New DevExpress.XtraLayout.LayoutControl()
        Me.bAffecterResidence = New DevExpress.XtraEditors.SimpleButton()
        Me.bSupprimerLivraisons = New DevExpress.XtraEditors.SimpleButton()
        Me.optFiltreGrille = New DevExpress.XtraEditors.RadioGroup()
        Me.bExportToHTML = New DevExpress.XtraEditors.SimpleButton()
        Me.bExportToExcel = New DevExpress.XtraEditors.SimpleButton()
        Me.bExportToPDF = New DevExpress.XtraEditors.SimpleButton()
        Me.bExportToDoc = New DevExpress.XtraEditors.SimpleButton()
        Me.grdImport = New DevExpress.XtraGrid.GridControl()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colImportID = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn2 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colReference = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colClient = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemMemoEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit()
        Me.colClientAddress = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colMomentDePassage = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colPreparePar = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colHeureAppel = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colHeurePrep = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colHeureDepart = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colHeureLivraison = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colMessagePassage = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemMemoEdit3 = New DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit()
        Me.colTotalTempService = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colLivreur = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colMontant = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colOrganisation = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemMemoEdit2 = New DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit()
        Me.colDebit = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCredit = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colGlacier = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colMontantGlacierLivreur = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colMontantGlacierOrganisation = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colMontantLivreur = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.Extras = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colExtraKM = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn3 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridIDPeriode = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.IDClientAddresse = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colIDResidence = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colResidence = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemCalcEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit()
        Me.RepositoryItemTextEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit()
        Me.RepositoryItemTextEdit2 = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit()
        Me.RepositoryItemCalcEdit2 = New DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit()
        Me.RepositoryItemSpinEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit()
        Me.RepositoryItemCheckEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit()
        Me.LayoutControlGroup7 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem4 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem7 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem11 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem12 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem18 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem29 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem1 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlItem30 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem39 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.XtraTabPage3 = New DevExpress.XtraTab.XtraTabPage()
        Me.LayoutControl5 = New DevExpress.XtraLayout.LayoutControl()
        Me.grdClientsAddresse = New DevExpress.XtraGrid.GridControl()
        Me.GridView3 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn4 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn5 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn6 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn7 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn8 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn9 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn10 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.bRemoveFilter = New DevExpress.XtraEditors.SimpleButton()
        Me.bImportMiseaJour = New DevExpress.XtraEditors.SimpleButton()
        Me.bSupprimerAddresse = New DevExpress.XtraEditors.SimpleButton()
        Me.bReturnToAction = New DevExpress.XtraEditors.SimpleButton()
        Me.bAddToImport = New DevExpress.XtraEditors.SimpleButton()
        Me.tbCustomerAddressID = New DevExpress.XtraEditors.TextEdit()
        Me.tbAdresseNo = New DevExpress.XtraEditors.TextEdit()
        Me.tbAdresse = New DevExpress.XtraEditors.TextEdit()
        Me.tbCodePostale = New DevExpress.XtraEditors.TextEdit()
        Me.tbAppartement = New DevExpress.XtraEditors.TextEdit()
        Me.tbCodeEntree = New DevExpress.XtraEditors.TextEdit()
        Me.tbTelephone = New DevExpress.XtraEditors.TextEdit()
        Me.tbNom = New DevExpress.XtraEditors.TextEdit()
        Me.cblivreur = New DevExpress.XtraEditors.LookUpEdit()
        Me.tbImportIDL = New DevExpress.XtraEditors.TextEdit()
        Me.tbMobile = New DevExpress.XtraEditors.TextEdit()
        Me.tbMontantOrganisation = New DevExpress.XtraEditors.TextEdit()
        Me.tbMontantLivreur = New DevExpress.XtraEditors.TextEdit()
        Me.tbDateLivraison = New DevExpress.XtraEditors.DateEdit()
        Me.tbDistanceR = New DevExpress.XtraEditors.TextEdit()
        Me.cbRestaurant = New DevExpress.XtraEditors.LookUpEdit()
        Me.cbVille = New DevExpress.XtraEditors.LookUpEdit()
        Me.LayoutControlGroup9 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlGroup10 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlGroup13 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem54 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem57 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlGroup15 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.lblPrixI = New DevExpress.XtraLayout.LayoutControlItem()
        Me.lblPrixI1 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.layoutAddressID = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem49 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem58 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem53 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem33 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem50 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem52 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem56 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem15 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem16 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem36 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem66 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem59 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem8 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem34 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem67 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem70 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem61 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem31 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.Root = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem38 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.navbarImageCollectionLarge4 = New DevExpress.Utils.ImageCollection(Me.components)
        Me.navbarImageCollectionLarge21 = New DevExpress.Utils.ImageCollection(Me.components)
        Me.navbarImageCollectionLarge = New DevExpress.Utils.ImageCollection(Me.components)
        Me.navbarImageCollectionLarge2 = New DevExpress.Utils.ImageCollection(Me.components)
        Me.navbarImageCollectionLarge1 = New DevExpress.Utils.ImageCollection(Me.components)
        Me.ribbonImageCollection = New DevExpress.Utils.ImageCollection(Me.components)
        Me.FolderBrowserDialog1 = New System.Windows.Forms.FolderBrowserDialog()
        Me.BarButtonItem2 = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem6 = New DevExpress.XtraBars.BarButtonItem()
        Me.DockManager1 = New DevExpress.XtraBars.Docking.DockManager(Me.components)
        Me.DockEdit = New DevExpress.XtraBars.Docking.DockPanel()
        Me.DockPanel1_Container = New DevExpress.XtraBars.Docking.ControlContainer()
        Me.LayoutControl2 = New DevExpress.XtraLayout.LayoutControl()
        Me.btnAnnuler = New DevExpress.XtraEditors.SimpleButton()
        Me.bSauvegarde = New DevExpress.XtraEditors.SimpleButton()
        Me.tbImportID = New DevExpress.XtraEditors.TextEdit()
        Me.tbDistance = New DevExpress.XtraEditors.TextEdit()
        Me.LayoutControlGroup4 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem43 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutParNombreKmCL = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem44 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem45 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.DockResidence = New DevExpress.XtraBars.Docking.DockPanel()
        Me.ControlContainer1 = New DevExpress.XtraBars.Docking.ControlContainer()
        Me.LayoutControl6 = New DevExpress.XtraLayout.LayoutControl()
        Me.bEffacerResidence = New DevExpress.XtraEditors.SimpleButton()
        Me.bAnnulerResidence = New DevExpress.XtraEditors.SimpleButton()
        Me.bAppliquerResidence = New DevExpress.XtraEditors.SimpleButton()
        Me.cbResidence = New DevExpress.XtraEditors.LookUpEdit()
        Me.LayoutControlGroup2 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.EmptySpaceItem2 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlItem41 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem55 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem60 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem62 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlGroup11 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.BarButtonItem9 = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem10 = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem11 = New DevExpress.XtraBars.BarButtonItem()
        CType(Me.RibbonControl, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemLookUpEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LayoutControl1.SuspendLayout()
        CType(Me.tabMain, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabMain.SuspendLayout()
        Me.XtraTabPage1.SuspendLayout()
        CType(Me.LayoutControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LayoutControl3.SuspendLayout()
        CType(Me.cbRapportRevenuPaye.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.optTypePeriode.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbAnnee.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbFactures.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbPayes.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Calendar.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ProgressBarControl1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtMobilusXLS.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEdit1.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.navbarImageCollectionLarge3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbFichierImport.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbDateFrom.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbDateFrom.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbDateTo.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbDateTo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbPeriodes.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbEtatsDeComptes.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbLivraisonRestaurants.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbCommissions.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbRapportTaxation.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutBas, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem17, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem26, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem22, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem23, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem28, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem32, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem27, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem48, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem14, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem13, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutDroite, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutEtat, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem42, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutEtatLivraisonRestaurant, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem69, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutFactures, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutPayes, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem24, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem25, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem19, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem21, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem35, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem47, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem64, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem72, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem74, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem75, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutCommissions, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem37, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem46, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem51, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutCommissions1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem63, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem65, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bResetTaxationLivreurs, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem20, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem68, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem71, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem73, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem40, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem7, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabPage2.SuspendLayout()
        CType(Me.LayoutControl4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LayoutControl4.SuspendLayout()
        CType(Me.optFiltreGrille.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grdImport, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemMemoEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemMemoEdit3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemMemoEdit2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemCalcEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemTextEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemTextEdit2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemCalcEdit2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemSpinEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemCheckEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem18, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem29, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem30, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem39, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabPage3.SuspendLayout()
        CType(Me.LayoutControl5, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LayoutControl5.SuspendLayout()
        CType(Me.grdClientsAddresse, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbCustomerAddressID.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbAdresseNo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbAdresse.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbCodePostale.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbAppartement.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbCodeEntree.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbTelephone.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbNom.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cblivreur.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbImportIDL.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbMobile.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbMontantOrganisation.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbMontantLivreur.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbDateLivraison.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbDateLivraison.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbDistanceR.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbRestaurant.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbVille.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup13, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem54, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem57, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup15, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblPrixI, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblPrixI1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.layoutAddressID, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem49, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem58, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem53, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem33, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem50, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem52, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem56, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem15, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem16, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem36, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem66, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem59, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem34, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem67, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem70, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem61, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem31, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Root, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem38, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.navbarImageCollectionLarge4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.navbarImageCollectionLarge21, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.navbarImageCollectionLarge, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.navbarImageCollectionLarge2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.navbarImageCollectionLarge1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ribbonImageCollection, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DockManager1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.DockEdit.SuspendLayout()
        Me.DockPanel1_Container.SuspendLayout()
        CType(Me.LayoutControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LayoutControl2.SuspendLayout()
        CType(Me.tbImportID.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbDistance.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem43, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutParNombreKmCL, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem44, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem45, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.DockResidence.SuspendLayout()
        Me.ControlContainer1.SuspendLayout()
        CType(Me.LayoutControl6, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LayoutControl6.SuspendLayout()
        CType(Me.cbResidence.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem41, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem55, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem60, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem62, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup11, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Distance
        '
        Me.Distance.AppearanceCell.Options.UseTextOptions = True
        Me.Distance.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center
        Me.Distance.Caption = "Distance"
        Me.Distance.FieldName = "Distance"
        Me.Distance.Name = "Distance"
        Me.Distance.OptionsColumn.AllowEdit = False
        Me.Distance.OptionsColumn.AllowFocus = False
        Me.Distance.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Distance", "SUM={0:0.##}")})
        Me.Distance.Visible = True
        Me.Distance.VisibleIndex = 15
        Me.Distance.Width = 92
        '
        'IsMobilus
        '
        Me.IsMobilus.Caption = "IsMobilus"
        Me.IsMobilus.FieldName = "IsMobilus"
        Me.IsMobilus.Name = "IsMobilus"
        '
        'RibbonControl
        '
        Me.RibbonControl.ExpandCollapseItem.Id = 0
        Me.RibbonControl.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.RibbonControl.ExpandCollapseItem, Me.RibbonControl.SearchEditItem, Me.BarButtonItem1, Me.bOrganisations, Me.BarButtonItem3, Me.bMiseaJour, Me.BarButtonItem5, Me.bFermer, Me.bLivreurs, Me.bFermer2, Me.SkinRibbonGalleryBarItem1, Me.bSettings, Me.bFactures, Me.bPayes, Me.bPrintAll, Me.BarButtonItem4, Me.bConsolidation, Me.bVendeurs, Me.bAchat, Me.bVente, Me.BarSubItem1, Me.BarButtonItem7, Me.bEtats, Me.BarSubItem3, Me.BarSubItem4, Me.BarSubItem2, Me.BarSubItem5, Me.BarButtonItem8, Me.bEtatDesComptes, Me.bEtatDesVentes, Me.bEtatDesAchats, Me.bVentes, Me.bAchats, Me.BarSubItem6, Me.cbYears, Me.bAjouterUneCommande, Me.bImprimerFactures, Me.bImprimerCommission, Me.bImprimerCommissionsVendeurs, Me.btnGenererFacturation, Me.bGenererPaye, Me.btnGenererFacturationVendeurs2, Me.bLivraisonParRestaurant, Me.bLivraisonsParLivreurs, Me.BarButtonItem17, Me.chkPRINT, Me.bGenererLaPaye, Me.bImprimerCommissions, Me.bImprimerCommisssions, Me.bGenererLesCommission, Me.bLivraisonParLivreur, Me.bCommisssion, Me.bFournisseurs, Me.bImprimerCommissionRapport, Me.bPrêts, Me.bEpargnes, Me.bResidences, Me.bRapportTaxeLivreur, Me.bRapportTaxesLivreurs, Me.bCalendrier, Me.btnRapportTaxeIDS, Me.btnRapportTaxePayeLivreur, Me.bLivraisonsParRestaurant, Me.bLivraisonParRestaurant2})
        Me.RibbonControl.Location = New System.Drawing.Point(0, 0)
        Me.RibbonControl.MaxItemId = 63
        Me.RibbonControl.Name = "RibbonControl"
        Me.RibbonControl.Pages.AddRange(New DevExpress.XtraBars.Ribbon.RibbonPage() {Me.RibbonPage2, Me.RibbonPage1})
        Me.RibbonControl.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemLookUpEdit1})
        Me.RibbonControl.Size = New System.Drawing.Size(1575, 147)
        Me.RibbonControl.StatusBar = Me.RibbonStatusBar
        '
        'BarButtonItem1
        '
        Me.BarButtonItem1.Caption = "BarButtonItem1"
        Me.BarButtonItem1.Id = 1
        Me.BarButtonItem1.ImageOptions.Image = CType(resources.GetObject("BarButtonItem1.ImageOptions.Image"), System.Drawing.Image)
        Me.BarButtonItem1.ImageOptions.LargeImage = CType(resources.GetObject("BarButtonItem1.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.BarButtonItem1.Name = "BarButtonItem1"
        '
        'bOrganisations
        '
        Me.bOrganisations.Caption = "Organisations"
        Me.bOrganisations.Enabled = False
        Me.bOrganisations.Id = 2
        Me.bOrganisations.ImageOptions.LargeImage = CType(resources.GetObject("bOrganisations.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.bOrganisations.Name = "bOrganisations"
        '
        'BarButtonItem3
        '
        Me.BarButtonItem3.Caption = "BarButtonItem3"
        Me.BarButtonItem3.Id = 3
        Me.BarButtonItem3.ImageOptions.LargeImage = CType(resources.GetObject("BarButtonItem3.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.BarButtonItem3.Name = "BarButtonItem3"
        '
        'bMiseaJour
        '
        Me.bMiseaJour.Caption = "Mise a jour"
        Me.bMiseaJour.Id = 4
        Me.bMiseaJour.ImageOptions.LargeImage = CType(resources.GetObject("bMiseaJour.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.bMiseaJour.Name = "bMiseaJour"
        '
        'BarButtonItem5
        '
        Me.BarButtonItem5.Caption = "Drivers"
        Me.BarButtonItem5.Id = 5
        Me.BarButtonItem5.ImageOptions.LargeImage = CType(resources.GetObject("BarButtonItem5.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.BarButtonItem5.Name = "BarButtonItem5"
        '
        'bFermer
        '
        Me.bFermer.Caption = "Fermer le programme"
        Me.bFermer.Id = 6
        Me.bFermer.ImageOptions.LargeImage = CType(resources.GetObject("bFermer.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.bFermer.Name = "bFermer"
        '
        'bLivreurs
        '
        Me.bLivreurs.Caption = "Livreurs / vendeurs"
        Me.bLivreurs.Id = 7
        Me.bLivreurs.ImageOptions.LargeImage = CType(resources.GetObject("bLivreurs.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.bLivreurs.Name = "bLivreurs"
        '
        'bFermer2
        '
        Me.bFermer2.Caption = "Fermer le programme"
        Me.bFermer2.Id = 8
        Me.bFermer2.ImageOptions.LargeImage = CType(resources.GetObject("bFermer2.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.bFermer2.Name = "bFermer2"
        '
        'SkinRibbonGalleryBarItem1
        '
        Me.SkinRibbonGalleryBarItem1.Caption = "SkinRibbonGalleryBarItem1"
        Me.SkinRibbonGalleryBarItem1.Id = 9
        Me.SkinRibbonGalleryBarItem1.Name = "SkinRibbonGalleryBarItem1"
        '
        'bSettings
        '
        Me.bSettings.Caption = "Configurations"
        Me.bSettings.Id = 10
        Me.bSettings.ImageOptions.Image = CType(resources.GetObject("bSettings.ImageOptions.Image"), System.Drawing.Image)
        Me.bSettings.ImageOptions.LargeImage = CType(resources.GetObject("bSettings.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.bSettings.Name = "bSettings"
        '
        'bFactures
        '
        Me.bFactures.Caption = "Factures"
        Me.bFactures.Enabled = False
        Me.bFactures.Id = 11
        Me.bFactures.ImageOptions.Image = CType(resources.GetObject("bFactures.ImageOptions.Image"), System.Drawing.Image)
        Me.bFactures.ImageOptions.LargeImage = CType(resources.GetObject("bFactures.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.bFactures.Name = "bFactures"
        '
        'bPayes
        '
        Me.bPayes.Caption = "Payes"
        Me.bPayes.Enabled = False
        Me.bPayes.Id = 12
        Me.bPayes.ImageOptions.Image = CType(resources.GetObject("bPayes.ImageOptions.Image"), System.Drawing.Image)
        Me.bPayes.ImageOptions.LargeImage = CType(resources.GetObject("bPayes.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.bPayes.Name = "bPayes"
        '
        'bPrintAll
        '
        Me.bPrintAll.Caption = "Imprimer les Factures et Paies"
        Me.bPrintAll.Id = 13
        Me.bPrintAll.ImageOptions.Image = CType(resources.GetObject("bPrintAll.ImageOptions.Image"), System.Drawing.Image)
        Me.bPrintAll.ImageOptions.LargeImage = CType(resources.GetObject("bPrintAll.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.bPrintAll.Name = "bPrintAll"
        '
        'BarButtonItem4
        '
        Me.BarButtonItem4.Caption = "BarButtonItem4"
        Me.BarButtonItem4.Id = 14
        Me.BarButtonItem4.Name = "BarButtonItem4"
        '
        'bConsolidation
        '
        Me.bConsolidation.Caption = "Etat de comptes"
        Me.bConsolidation.Id = 15
        Me.bConsolidation.ImageOptions.Image = CType(resources.GetObject("bConsolidation.ImageOptions.Image"), System.Drawing.Image)
        Me.bConsolidation.ImageOptions.LargeImage = CType(resources.GetObject("bConsolidation.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.bConsolidation.Name = "bConsolidation"
        '
        'bVendeurs
        '
        Me.bVendeurs.Caption = "Vendeurs"
        Me.bVendeurs.Id = 16
        Me.bVendeurs.ImageOptions.Image = CType(resources.GetObject("bVendeurs.ImageOptions.Image"), System.Drawing.Image)
        Me.bVendeurs.ImageOptions.LargeImage = CType(resources.GetObject("bVendeurs.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.bVendeurs.Name = "bVendeurs"
        '
        'bAchat
        '
        Me.bAchat.Caption = "Achats"
        Me.bAchat.Enabled = False
        Me.bAchat.Id = 17
        Me.bAchat.ImageOptions.Image = CType(resources.GetObject("bAchat.ImageOptions.Image"), System.Drawing.Image)
        Me.bAchat.ImageOptions.LargeImage = CType(resources.GetObject("bAchat.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.bAchat.Name = "bAchat"
        '
        'bVente
        '
        Me.bVente.Caption = "Ventes"
        Me.bVente.Enabled = False
        Me.bVente.Id = 18
        Me.bVente.ImageOptions.Image = CType(resources.GetObject("bVente.ImageOptions.Image"), System.Drawing.Image)
        Me.bVente.ImageOptions.LargeImage = CType(resources.GetObject("bVente.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.bVente.Name = "bVente"
        '
        'BarSubItem1
        '
        Me.BarSubItem1.Caption = "BarSubItem1"
        Me.BarSubItem1.Id = 19
        Me.BarSubItem1.Name = "BarSubItem1"
        '
        'BarButtonItem7
        '
        Me.BarButtonItem7.Caption = "BarButtonItem7"
        Me.BarButtonItem7.Id = 20
        Me.BarButtonItem7.Name = "BarButtonItem7"
        '
        'bEtats
        '
        Me.bEtats.Caption = "États"
        Me.bEtats.Id = 21
        Me.bEtats.ImageOptions.Image = CType(resources.GetObject("bEtats.ImageOptions.Image"), System.Drawing.Image)
        Me.bEtats.ImageOptions.LargeImage = CType(resources.GetObject("bEtats.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.bEtats.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.bEtatDesComptes), New DevExpress.XtraBars.LinkPersistInfo(Me.bEtatDesVentes), New DevExpress.XtraBars.LinkPersistInfo(Me.bEtatDesAchats), New DevExpress.XtraBars.LinkPersistInfo(Me.bRapportTaxesLivreurs), New DevExpress.XtraBars.LinkPersistInfo(Me.btnRapportTaxeIDS), New DevExpress.XtraBars.LinkPersistInfo(Me.btnRapportTaxePayeLivreur)})
        Me.bEtats.Name = "bEtats"
        Me.bEtats.RibbonStyle = CType(((DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText) _
            Or DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithoutText), DevExpress.XtraBars.Ribbon.RibbonItemStyles)
        '
        'bEtatDesComptes
        '
        Me.bEtatDesComptes.Caption = "État des comptes"
        Me.bEtatDesComptes.Id = 27
        Me.bEtatDesComptes.ImageOptions.Image = CType(resources.GetObject("bEtatDesComptes.ImageOptions.Image"), System.Drawing.Image)
        Me.bEtatDesComptes.Name = "bEtatDesComptes"
        '
        'bEtatDesVentes
        '
        Me.bEtatDesVentes.Caption = "État des ventes"
        Me.bEtatDesVentes.Id = 28
        Me.bEtatDesVentes.ImageOptions.Image = CType(resources.GetObject("bEtatDesVentes.ImageOptions.Image"), System.Drawing.Image)
        Me.bEtatDesVentes.Name = "bEtatDesVentes"
        '
        'bEtatDesAchats
        '
        Me.bEtatDesAchats.Caption = "État des achats"
        Me.bEtatDesAchats.Id = 29
        Me.bEtatDesAchats.ImageOptions.Image = CType(resources.GetObject("bEtatDesAchats.ImageOptions.Image"), System.Drawing.Image)
        Me.bEtatDesAchats.Name = "bEtatDesAchats"
        '
        'bLivraisonsParRestaurant
        '
        Me.bLivraisonsParRestaurant.Caption = "Livraisons par Restaurant"
        Me.bLivraisonsParRestaurant.Id = 61
        Me.bLivraisonsParRestaurant.ImageOptions.Image = CType(resources.GetObject("bLivraisonsParRestaurant.ImageOptions.Image"), System.Drawing.Image)
        Me.bLivraisonsParRestaurant.Name = "bLivraisonsParRestaurant"
        '
        'bRapportTaxesLivreurs
        '
        Me.bRapportTaxesLivreurs.Caption = "Rapport de Taxation Livreurs"
        Me.bRapportTaxesLivreurs.Id = 57
        Me.bRapportTaxesLivreurs.ImageOptions.Image = CType(resources.GetObject("bRapportTaxesLivreurs.ImageOptions.Image"), System.Drawing.Image)
        Me.bRapportTaxesLivreurs.Name = "bRapportTaxesLivreurs"
        '
        'btnRapportTaxeIDS
        '
        Me.btnRapportTaxeIDS.Caption = "Rapport de Taxation IDS"
        Me.btnRapportTaxeIDS.Id = 59
        Me.btnRapportTaxeIDS.ImageOptions.Image = CType(resources.GetObject("btnRapportTaxeIDS.ImageOptions.Image"), System.Drawing.Image)
        Me.btnRapportTaxeIDS.Name = "btnRapportTaxeIDS"
        '
        'btnRapportTaxePayeLivreur
        '
        Me.btnRapportTaxePayeLivreur.Caption = "Rapport de Revenu Livreurs"
        Me.btnRapportTaxePayeLivreur.Id = 60
        Me.btnRapportTaxePayeLivreur.ImageOptions.Image = CType(resources.GetObject("btnRapportTaxePayeLivreur.ImageOptions.Image"), System.Drawing.Image)
        Me.btnRapportTaxePayeLivreur.Name = "btnRapportTaxePayeLivreur"
        '
        'BarSubItem3
        '
        Me.BarSubItem3.Caption = "BarSubItem3"
        Me.BarSubItem3.Id = 22
        Me.BarSubItem3.Name = "BarSubItem3"
        '
        'BarSubItem4
        '
        Me.BarSubItem4.Caption = "BarSubItem4"
        Me.BarSubItem4.Id = 23
        Me.BarSubItem4.Name = "BarSubItem4"
        '
        'BarSubItem2
        '
        Me.BarSubItem2.Caption = "État des comptes"
        Me.BarSubItem2.Id = 24
        Me.BarSubItem2.Name = "BarSubItem2"
        '
        'BarSubItem5
        '
        Me.BarSubItem5.Caption = "État des ventes"
        Me.BarSubItem5.Id = 25
        Me.BarSubItem5.Name = "BarSubItem5"
        '
        'BarButtonItem8
        '
        Me.BarButtonItem8.Caption = "BarButtonItem8"
        Me.BarButtonItem8.Id = 26
        Me.BarButtonItem8.Name = "BarButtonItem8"
        '
        'bVentes
        '
        Me.bVentes.Caption = "Ventes"
        Me.bVentes.Enabled = False
        Me.bVentes.Id = 30
        Me.bVentes.ImageOptions.ImageUri.Uri = "icon%20builder/business_dollar"
        Me.bVentes.Name = "bVentes"
        '
        'bAchats
        '
        Me.bAchats.Caption = "Dépenses"
        Me.bAchats.Enabled = False
        Me.bAchats.Id = 31
        Me.bAchats.ImageOptions.ImageUri.Uri = "business%20objects/bo_invoice"
        Me.bAchats.Name = "bAchats"
        '
        'BarSubItem6
        '
        Me.BarSubItem6.Caption = "BarSubItem6"
        Me.BarSubItem6.Id = 32
        Me.BarSubItem6.Name = "BarSubItem6"
        '
        'cbYears
        '
        Me.cbYears.Caption = "Années"
        Me.cbYears.Edit = Me.RepositoryItemLookUpEdit1
        Me.cbYears.Id = 33
        Me.cbYears.Name = "cbYears"
        '
        'RepositoryItemLookUpEdit1
        '
        Me.RepositoryItemLookUpEdit1.AutoHeight = False
        Me.RepositoryItemLookUpEdit1.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemLookUpEdit1.Name = "RepositoryItemLookUpEdit1"
        '
        'bAjouterUneCommande
        '
        Me.bAjouterUneCommande.Caption = "Ajouter une commande"
        Me.bAjouterUneCommande.Enabled = False
        Me.bAjouterUneCommande.Id = 34
        Me.bAjouterUneCommande.ImageOptions.Image = CType(resources.GetObject("bAjouterUneCommande.ImageOptions.Image"), System.Drawing.Image)
        Me.bAjouterUneCommande.ImageOptions.LargeImage = CType(resources.GetObject("bAjouterUneCommande.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.bAjouterUneCommande.Name = "bAjouterUneCommande"
        '
        'bImprimerFactures
        '
        Me.bImprimerFactures.Caption = "Factures"
        Me.bImprimerFactures.Enabled = False
        Me.bImprimerFactures.Id = 35
        Me.bImprimerFactures.ImageOptions.Image = CType(resources.GetObject("bImprimerFactures.ImageOptions.Image"), System.Drawing.Image)
        Me.bImprimerFactures.Name = "bImprimerFactures"
        '
        'bImprimerCommission
        '
        Me.bImprimerCommission.Caption = "Payes"
        Me.bImprimerCommission.Enabled = False
        Me.bImprimerCommission.Id = 36
        Me.bImprimerCommission.ImageOptions.Image = CType(resources.GetObject("bImprimerCommission.ImageOptions.Image"), System.Drawing.Image)
        Me.bImprimerCommission.Name = "bImprimerCommission"
        '
        'bImprimerCommissionsVendeurs
        '
        Me.bImprimerCommissionsVendeurs.Caption = "6) Imprimer les commissions"
        Me.bImprimerCommissionsVendeurs.Enabled = False
        Me.bImprimerCommissionsVendeurs.Id = 37
        Me.bImprimerCommissionsVendeurs.ImageOptions.Image = CType(resources.GetObject("bImprimerCommissionsVendeurs.ImageOptions.Image"), System.Drawing.Image)
        Me.bImprimerCommissionsVendeurs.Name = "bImprimerCommissionsVendeurs"
        '
        'btnGenererFacturation
        '
        Me.btnGenererFacturation.Caption = "Facturation"
        Me.btnGenererFacturation.Enabled = False
        Me.btnGenererFacturation.Id = 38
        Me.btnGenererFacturation.ImageOptions.Image = CType(resources.GetObject("btnGenererFacturation.ImageOptions.Image"), System.Drawing.Image)
        Me.btnGenererFacturation.Name = "btnGenererFacturation"
        '
        'bGenererPaye
        '
        Me.bGenererPaye.Caption = "2) Générer la paie"
        Me.bGenererPaye.Enabled = False
        Me.bGenererPaye.Id = 39
        Me.bGenererPaye.ImageOptions.Image = CType(resources.GetObject("bGenererPaye.ImageOptions.Image"), System.Drawing.Image)
        Me.bGenererPaye.Name = "bGenererPaye"
        '
        'btnGenererFacturationVendeurs2
        '
        Me.btnGenererFacturationVendeurs2.Caption = "BarButtonItem14"
        Me.btnGenererFacturationVendeurs2.Id = 40
        Me.btnGenererFacturationVendeurs2.Name = "btnGenererFacturationVendeurs2"
        '
        'bLivraisonParRestaurant
        '
        Me.bLivraisonParRestaurant.Caption = "Livraisons par Restaurant"
        Me.bLivraisonParRestaurant.Enabled = False
        Me.bLivraisonParRestaurant.Id = 41
        Me.bLivraisonParRestaurant.ImageOptions.Image = CType(resources.GetObject("bLivraisonParRestaurant.ImageOptions.Image"), System.Drawing.Image)
        Me.bLivraisonParRestaurant.Name = "bLivraisonParRestaurant"
        '
        'bLivraisonsParLivreurs
        '
        Me.bLivraisonsParLivreurs.Caption = "Livraison par Livreur"
        Me.bLivraisonsParLivreurs.Enabled = False
        Me.bLivraisonsParLivreurs.Id = 42
        Me.bLivraisonsParLivreurs.ImageOptions.Image = CType(resources.GetObject("bLivraisonsParLivreurs.ImageOptions.Image"), System.Drawing.Image)
        Me.bLivraisonsParLivreurs.Name = "bLivraisonsParLivreurs"
        '
        'BarButtonItem17
        '
        Me.BarButtonItem17.Caption = "BarButtonItem17"
        Me.BarButtonItem17.Id = 43
        Me.BarButtonItem17.Name = "BarButtonItem17"
        '
        'chkPRINT
        '
        Me.chkPRINT.Caption = "Imprimer"
        Me.chkPRINT.Id = 44
        Me.chkPRINT.Name = "chkPRINT"
        '
        'bGenererLaPaye
        '
        Me.bGenererLaPaye.Caption = "Paye"
        Me.bGenererLaPaye.CausesValidation = True
        Me.bGenererLaPaye.Enabled = False
        Me.bGenererLaPaye.Id = 45
        Me.bGenererLaPaye.ImageOptions.Image = CType(resources.GetObject("bGenererLaPaye.ImageOptions.Image"), System.Drawing.Image)
        Me.bGenererLaPaye.Name = "bGenererLaPaye"
        '
        'bImprimerCommissions
        '
        Me.bImprimerCommissions.Caption = "6) Imprimer les commissions"
        Me.bImprimerCommissions.Enabled = False
        Me.bImprimerCommissions.Id = 46
        Me.bImprimerCommissions.ImageOptions.Image = CType(resources.GetObject("bImprimerCommissions.ImageOptions.Image"), System.Drawing.Image)
        Me.bImprimerCommissions.Name = "bImprimerCommissions"
        '
        'bImprimerCommisssions
        '
        Me.bImprimerCommisssions.Caption = "6) Imprimer les commissions"
        Me.bImprimerCommisssions.Enabled = False
        Me.bImprimerCommisssions.Id = 47
        Me.bImprimerCommisssions.ImageOptions.Image = CType(resources.GetObject("bImprimerCommisssions.ImageOptions.Image"), System.Drawing.Image)
        Me.bImprimerCommisssions.Name = "bImprimerCommisssions"
        '
        'bGenererLesCommission
        '
        Me.bGenererLesCommission.Caption = "Commissions"
        Me.bGenererLesCommission.Enabled = False
        Me.bGenererLesCommission.Id = 48
        Me.bGenererLesCommission.ImageOptions.Image = CType(resources.GetObject("bGenererLesCommission.ImageOptions.Image"), System.Drawing.Image)
        Me.bGenererLesCommission.Name = "bGenererLesCommission"
        '
        'bLivraisonParLivreur
        '
        Me.bLivraisonParLivreur.Caption = "Livraisons par Livreur"
        Me.bLivraisonParLivreur.Enabled = False
        Me.bLivraisonParLivreur.Id = 49
        Me.bLivraisonParLivreur.ImageOptions.Image = CType(resources.GetObject("bLivraisonParLivreur.ImageOptions.Image"), System.Drawing.Image)
        Me.bLivraisonParLivreur.Name = "bLivraisonParLivreur"
        '
        'bCommisssion
        '
        Me.bCommisssion.Caption = "Commissions"
        Me.bCommisssion.Enabled = False
        Me.bCommisssion.Id = 50
        Me.bCommisssion.ImageOptions.Image = CType(resources.GetObject("bCommisssion.ImageOptions.Image"), System.Drawing.Image)
        Me.bCommisssion.ImageOptions.LargeImage = CType(resources.GetObject("bCommisssion.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.bCommisssion.Name = "bCommisssion"
        '
        'bFournisseurs
        '
        Me.bFournisseurs.Caption = "Services"
        Me.bFournisseurs.Id = 51
        Me.bFournisseurs.ImageOptions.Image = CType(resources.GetObject("bFournisseurs.ImageOptions.Image"), System.Drawing.Image)
        Me.bFournisseurs.ImageOptions.LargeImage = CType(resources.GetObject("bFournisseurs.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.bFournisseurs.Name = "bFournisseurs"
        '
        'bImprimerCommissionRapport
        '
        Me.bImprimerCommissionRapport.Caption = "Commissions"
        Me.bImprimerCommissionRapport.Enabled = False
        Me.bImprimerCommissionRapport.Id = 52
        Me.bImprimerCommissionRapport.ImageOptions.Image = CType(resources.GetObject("bImprimerCommissionRapport.ImageOptions.Image"), System.Drawing.Image)
        Me.bImprimerCommissionRapport.Name = "bImprimerCommissionRapport"
        '
        'bPrêts
        '
        Me.bPrêts.Caption = "Prêts"
        Me.bPrêts.Id = 53
        Me.bPrêts.ImageOptions.LargeImage = CType(resources.GetObject("bPrêts.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.bPrêts.Name = "bPrêts"
        '
        'bEpargnes
        '
        Me.bEpargnes.Caption = "Epargnes"
        Me.bEpargnes.Enabled = False
        Me.bEpargnes.Id = 54
        Me.bEpargnes.ImageOptions.LargeImage = CType(resources.GetObject("bEpargnes.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.bEpargnes.Name = "bEpargnes"
        '
        'bResidences
        '
        Me.bResidences.Caption = "Résidences"
        Me.bResidences.Id = 55
        Me.bResidences.ImageOptions.ImageUri.Uri = "Home"
        Me.bResidences.Name = "bResidences"
        '
        'bRapportTaxeLivreur
        '
        Me.bRapportTaxeLivreur.Caption = "Rapport de taxe Livreur"
        Me.bRapportTaxeLivreur.Id = 56
        Me.bRapportTaxeLivreur.ImageOptions.Image = CType(resources.GetObject("bRapportTaxeLivreur.ImageOptions.Image"), System.Drawing.Image)
        Me.bRapportTaxeLivreur.Name = "bRapportTaxeLivreur"
        '
        'bCalendrier
        '
        Me.bCalendrier.Caption = "Calendrier"
        Me.bCalendrier.Id = 58
        Me.bCalendrier.ImageOptions.Image = CType(resources.GetObject("bCalendrier.ImageOptions.Image"), System.Drawing.Image)
        Me.bCalendrier.ImageOptions.LargeImage = CType(resources.GetObject("bCalendrier.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.bCalendrier.Name = "bCalendrier"
        '
        'bLivraisonParRestaurant2
        '
        Me.bLivraisonParRestaurant2.Caption = "Livraisons par Restaurant"
        Me.bLivraisonParRestaurant2.CausesValidation = True
        Me.bLivraisonParRestaurant2.Enabled = False
        Me.bLivraisonParRestaurant2.Id = 62
        Me.bLivraisonParRestaurant2.Name = "bLivraisonParRestaurant2"
        '
        'RibbonPage2
        '
        Me.RibbonPage2.Groups.AddRange(New DevExpress.XtraBars.Ribbon.RibbonPageGroup() {Me.RibbonPageGroup2, Me.RibbonPageGroup6, Me.RibbonPageGroup7, Me.RibbonPageGroup8, Me.RibbonPageGroup9, Me.RibbonPageGroup3})
        Me.RibbonPage2.Name = "RibbonPage2"
        Me.RibbonPage2.Text = "Accueil"
        '
        'RibbonPageGroup2
        '
        Me.RibbonPageGroup2.ItemLinks.Add(Me.bOrganisations)
        Me.RibbonPageGroup2.ItemLinks.Add(Me.bLivreurs)
        Me.RibbonPageGroup2.ItemLinks.Add(Me.bFactures)
        Me.RibbonPageGroup2.ItemLinks.Add(Me.bPayes)
        Me.RibbonPageGroup2.ItemLinks.Add(Me.bCommisssion)
        Me.RibbonPageGroup2.ItemLinks.Add(Me.bAchats)
        Me.RibbonPageGroup2.ItemLinks.Add(Me.bVentes)
        Me.RibbonPageGroup2.ItemLinks.Add(Me.bFournisseurs)
        Me.RibbonPageGroup2.ItemLinks.Add(Me.bPrêts)
        Me.RibbonPageGroup2.ItemLinks.Add(Me.bEpargnes)
        Me.RibbonPageGroup2.ItemLinks.Add(Me.bResidences)
        Me.RibbonPageGroup2.ItemLinks.Add(Me.bCalendrier)
        Me.RibbonPageGroup2.Name = "RibbonPageGroup2"
        Me.RibbonPageGroup2.Text = "Management"
        '
        'RibbonPageGroup6
        '
        Me.RibbonPageGroup6.ItemLinks.Add(Me.bAjouterUneCommande)
        Me.RibbonPageGroup6.Name = "RibbonPageGroup6"
        Me.RibbonPageGroup6.Text = "Commandes"
        '
        'RibbonPageGroup7
        '
        Me.RibbonPageGroup7.ItemLinks.Add(Me.btnGenererFacturation)
        Me.RibbonPageGroup7.ItemLinks.Add(Me.bGenererLaPaye)
        Me.RibbonPageGroup7.ItemLinks.Add(Me.bGenererLesCommission)
        Me.RibbonPageGroup7.Name = "RibbonPageGroup7"
        Me.RibbonPageGroup7.Text = "Générer"
        '
        'RibbonPageGroup8
        '
        Me.RibbonPageGroup8.ItemLinks.Add(Me.bImprimerFactures)
        Me.RibbonPageGroup8.ItemLinks.Add(Me.bImprimerCommission)
        Me.RibbonPageGroup8.ItemLinks.Add(Me.bImprimerCommissionRapport)
        Me.RibbonPageGroup8.ItemLinks.Add(Me.bLivraisonParRestaurant2, False, "", "", False, False)
        Me.RibbonPageGroup8.Name = "RibbonPageGroup8"
        Me.RibbonPageGroup8.Text = "Imprimer"
        '
        'RibbonPageGroup9
        '
        Me.RibbonPageGroup9.ItemLinks.Add(Me.bEtats, True)
        Me.RibbonPageGroup9.Name = "RibbonPageGroup9"
        Me.RibbonPageGroup9.Text = "Etats"
        '
        'RibbonPageGroup3
        '
        Me.RibbonPageGroup3.ItemLinks.Add(Me.bFermer)
        Me.RibbonPageGroup3.Name = "RibbonPageGroup3"
        Me.RibbonPageGroup3.Text = "System"
        '
        'RibbonPage1
        '
        Me.RibbonPage1.Groups.AddRange(New DevExpress.XtraBars.Ribbon.RibbonPageGroup() {Me.RibbonPageGroup1, Me.RibbonPageGroup4, Me.RibbonPageGroup5})
        Me.RibbonPage1.Name = "RibbonPage1"
        Me.RibbonPage1.Text = "Administrateur"
        '
        'RibbonPageGroup1
        '
        Me.RibbonPageGroup1.ItemLinks.Add(Me.bSettings)
        Me.RibbonPageGroup1.Name = "RibbonPageGroup1"
        Me.RibbonPageGroup1.Text = "Configurations"
        '
        'RibbonPageGroup4
        '
        Me.RibbonPageGroup4.ItemLinks.Add(Me.SkinRibbonGalleryBarItem1)
        Me.RibbonPageGroup4.Name = "RibbonPageGroup4"
        Me.RibbonPageGroup4.Text = "Skins"
        '
        'RibbonPageGroup5
        '
        Me.RibbonPageGroup5.ItemLinks.Add(Me.bFermer2)
        Me.RibbonPageGroup5.Name = "RibbonPageGroup5"
        Me.RibbonPageGroup5.Text = "Système"
        '
        'RibbonStatusBar
        '
        Me.RibbonStatusBar.Location = New System.Drawing.Point(0, 819)
        Me.RibbonStatusBar.Name = "RibbonStatusBar"
        Me.RibbonStatusBar.Ribbon = Me.RibbonControl
        Me.RibbonStatusBar.Size = New System.Drawing.Size(1575, 23)
        '
        'rgbiSkins2
        '
        Me.rgbiSkins2.Caption = "SkinRibbonGalleryBarItem1"
        Me.rgbiSkins2.Id = 145
        Me.rgbiSkins2.Name = "rgbiSkins2"
        '
        'LayoutControl1
        '
        Me.LayoutControl1.Controls.Add(Me.tabMain)
        Me.LayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LayoutControl1.Location = New System.Drawing.Point(0, 147)
        Me.LayoutControl1.Name = "LayoutControl1"
        Me.LayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = New System.Drawing.Rectangle(587, 333, 650, 400)
        Me.LayoutControl1.OptionsFocus.MoveFocusRightToLeft = False
        Me.LayoutControl1.Root = Me.Root
        Me.LayoutControl1.Size = New System.Drawing.Size(1575, 672)
        Me.LayoutControl1.TabIndex = 2
        Me.LayoutControl1.Text = "LayoutControl1"
        '
        'tabMain
        '
        Me.tabMain.Appearance.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.tabMain.Appearance.Options.UseFont = True
        Me.tabMain.AppearancePage.Header.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.tabMain.AppearancePage.Header.Options.UseFont = True
        Me.tabMain.Location = New System.Drawing.Point(12, 12)
        Me.tabMain.Name = "tabMain"
        Me.tabMain.SelectedTabPage = Me.XtraTabPage1
        Me.tabMain.Size = New System.Drawing.Size(1551, 648)
        Me.tabMain.TabIndex = 41
        Me.tabMain.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.XtraTabPage1, Me.XtraTabPage2, Me.XtraTabPage3})
        '
        'XtraTabPage1
        '
        Me.XtraTabPage1.Controls.Add(Me.LayoutControl3)
        Me.XtraTabPage1.Name = "XtraTabPage1"
        Me.XtraTabPage1.Size = New System.Drawing.Size(1546, 619)
        Me.XtraTabPage1.Text = "Actions"
        '
        'LayoutControl3
        '
        Me.LayoutControl3.Controls.Add(Me.bResetTaxationLivreurPaye)
        Me.LayoutControl3.Controls.Add(Me.bPrintTaxationLivreurPaye)
        Me.LayoutControl3.Controls.Add(Me.bEmailTaxationLivreurPaye)
        Me.LayoutControl3.Controls.Add(Me.cbRapportRevenuPaye)
        Me.LayoutControl3.Controls.Add(Me.bResetTaxationLivreur)
        Me.LayoutControl3.Controls.Add(Me.bPrintTaxationLivreur)
        Me.LayoutControl3.Controls.Add(Me.bEmailTaxationLivreur)
        Me.LayoutControl3.Controls.Add(Me.bAnnulerCommissions)
        Me.LayoutControl3.Controls.Add(Me.bPrintCommissions)
        Me.LayoutControl3.Controls.Add(Me.bEnvoyerCommissionVendeur)
        Me.LayoutControl3.Controls.Add(Me.bEnvoyerLivraisonsRestaurants)
        Me.LayoutControl3.Controls.Add(Me.bEnvoyerEtatCompte)
        Me.LayoutControl3.Controls.Add(Me.bEnvoyerPaye)
        Me.LayoutControl3.Controls.Add(Me.bEnvoyerFacture)
        Me.LayoutControl3.Controls.Add(Me.bPrintLivraisonR)
        Me.LayoutControl3.Controls.Add(Me.bPrintEtat)
        Me.LayoutControl3.Controls.Add(Me.bPrintPaye)
        Me.LayoutControl3.Controls.Add(Me.bPrintFacture)
        Me.LayoutControl3.Controls.Add(Me.optTypePeriode)
        Me.LayoutControl3.Controls.Add(Me.bResetLivraisonsRestaurant)
        Me.LayoutControl3.Controls.Add(Me.bResetPeriode)
        Me.LayoutControl3.Controls.Add(Me.bResetEtatdeCompte)
        Me.LayoutControl3.Controls.Add(Me.cbAnnee)
        Me.LayoutControl3.Controls.Add(Me.bResetDate)
        Me.LayoutControl3.Controls.Add(Me.bConsolidationDate)
        Me.LayoutControl3.Controls.Add(Me.cbFactures)
        Me.LayoutControl3.Controls.Add(Me.cbPayes)
        Me.LayoutControl3.Controls.Add(Me.Calendar)
        Me.LayoutControl3.Controls.Add(Me.ProgressBarControl1)
        Me.LayoutControl3.Controls.Add(Me.bFiltrerDate)
        Me.LayoutControl3.Controls.Add(Me.txtMobilusXLS)
        Me.LayoutControl3.Controls.Add(Me.DateEdit1)
        Me.LayoutControl3.Controls.Add(Me.bImportMobilus)
        Me.LayoutControl3.Controls.Add(Me.bSelectfile)
        Me.LayoutControl3.Controls.Add(Me.bSupprimerI)
        Me.LayoutControl3.Controls.Add(Me.cbFichierImport)
        Me.LayoutControl3.Controls.Add(Me.bAnnulerFichierImport)
        Me.LayoutControl3.Controls.Add(Me.tbDateFrom)
        Me.LayoutControl3.Controls.Add(Me.tbDateTo)
        Me.LayoutControl3.Controls.Add(Me.cbPeriodes)
        Me.LayoutControl3.Controls.Add(Me.bAnnulerPayes)
        Me.LayoutControl3.Controls.Add(Me.bAnnulerFactures)
        Me.LayoutControl3.Controls.Add(Me.cbEtatsDeComptes)
        Me.LayoutControl3.Controls.Add(Me.cbLivraisonRestaurants)
        Me.LayoutControl3.Controls.Add(Me.cbCommissions)
        Me.LayoutControl3.Controls.Add(Me.cbRapportTaxation)
        Me.LayoutControl3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LayoutControl3.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControl3.Name = "LayoutControl3"
        Me.LayoutControl3.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = New System.Drawing.Rectangle(507, 641, 650, 400)
        Me.LayoutControl3.Root = Me.LayoutControlGroup6
        Me.LayoutControl3.Size = New System.Drawing.Size(1546, 619)
        Me.LayoutControl3.TabIndex = 0
        Me.LayoutControl3.Text = "LayoutControl3"
        '
        'bResetTaxationLivreurPaye
        '
        Me.bResetTaxationLivreurPaye.Location = New System.Drawing.Point(1484, 264)
        Me.bResetTaxationLivreurPaye.Name = "bResetTaxationLivreurPaye"
        Me.bResetTaxationLivreurPaye.Size = New System.Drawing.Size(26, 22)
        Me.bResetTaxationLivreurPaye.StyleController = Me.LayoutControl3
        Me.bResetTaxationLivreurPaye.TabIndex = 85
        Me.bResetTaxationLivreurPaye.Text = "..."
        '
        'bPrintTaxationLivreurPaye
        '
        Me.bPrintTaxationLivreurPaye.ImageOptions.Image = CType(resources.GetObject("bPrintTaxationLivreurPaye.ImageOptions.Image"), System.Drawing.Image)
        Me.bPrintTaxationLivreurPaye.Location = New System.Drawing.Point(1450, 264)
        Me.bPrintTaxationLivreurPaye.Name = "bPrintTaxationLivreurPaye"
        Me.bPrintTaxationLivreurPaye.Size = New System.Drawing.Size(30, 22)
        Me.bPrintTaxationLivreurPaye.StyleController = Me.LayoutControl3
        Me.bPrintTaxationLivreurPaye.TabIndex = 84
        '
        'bEmailTaxationLivreurPaye
        '
        Me.bEmailTaxationLivreurPaye.ImageOptions.Image = CType(resources.GetObject("bEmailTaxationLivreurPaye.ImageOptions.Image"), System.Drawing.Image)
        Me.bEmailTaxationLivreurPaye.Location = New System.Drawing.Point(1416, 264)
        Me.bEmailTaxationLivreurPaye.Name = "bEmailTaxationLivreurPaye"
        Me.bEmailTaxationLivreurPaye.Size = New System.Drawing.Size(30, 22)
        Me.bEmailTaxationLivreurPaye.StyleController = Me.LayoutControl3
        Me.bEmailTaxationLivreurPaye.TabIndex = 83
        '
        'cbRapportRevenuPaye
        '
        Me.cbRapportRevenuPaye.Location = New System.Drawing.Point(923, 264)
        Me.cbRapportRevenuPaye.Name = "cbRapportRevenuPaye"
        Me.cbRapportRevenuPaye.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbRapportRevenuPaye.Properties.ForceUpdateEditValue = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbRapportRevenuPaye.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard
        Me.cbRapportRevenuPaye.Size = New System.Drawing.Size(489, 20)
        Me.cbRapportRevenuPaye.StyleController = Me.LayoutControl3
        Me.cbRapportRevenuPaye.TabIndex = 82
        '
        'bResetTaxationLivreur
        '
        Me.bResetTaxationLivreur.Location = New System.Drawing.Point(1484, 238)
        Me.bResetTaxationLivreur.Name = "bResetTaxationLivreur"
        Me.bResetTaxationLivreur.Size = New System.Drawing.Size(26, 22)
        Me.bResetTaxationLivreur.StyleController = Me.LayoutControl3
        Me.bResetTaxationLivreur.TabIndex = 81
        Me.bResetTaxationLivreur.Text = "..."
        '
        'bPrintTaxationLivreur
        '
        Me.bPrintTaxationLivreur.ImageOptions.Image = CType(resources.GetObject("bPrintTaxationLivreur.ImageOptions.Image"), System.Drawing.Image)
        Me.bPrintTaxationLivreur.Location = New System.Drawing.Point(1450, 238)
        Me.bPrintTaxationLivreur.Name = "bPrintTaxationLivreur"
        Me.bPrintTaxationLivreur.Size = New System.Drawing.Size(30, 22)
        Me.bPrintTaxationLivreur.StyleController = Me.LayoutControl3
        Me.bPrintTaxationLivreur.TabIndex = 80
        '
        'bEmailTaxationLivreur
        '
        Me.bEmailTaxationLivreur.ImageOptions.Image = CType(resources.GetObject("bEmailTaxationLivreur.ImageOptions.Image"), System.Drawing.Image)
        Me.bEmailTaxationLivreur.Location = New System.Drawing.Point(1416, 238)
        Me.bEmailTaxationLivreur.Name = "bEmailTaxationLivreur"
        Me.bEmailTaxationLivreur.Size = New System.Drawing.Size(30, 22)
        Me.bEmailTaxationLivreur.StyleController = Me.LayoutControl3
        Me.bEmailTaxationLivreur.TabIndex = 79
        '
        'bAnnulerCommissions
        '
        Me.bAnnulerCommissions.Location = New System.Drawing.Point(1484, 108)
        Me.bAnnulerCommissions.Name = "bAnnulerCommissions"
        Me.bAnnulerCommissions.Size = New System.Drawing.Size(26, 22)
        Me.bAnnulerCommissions.StyleController = Me.LayoutControl3
        Me.bAnnulerCommissions.TabIndex = 78
        Me.bAnnulerCommissions.Text = "..."
        '
        'bPrintCommissions
        '
        Me.bPrintCommissions.ImageOptions.Image = CType(resources.GetObject("bPrintCommissions.ImageOptions.Image"), System.Drawing.Image)
        Me.bPrintCommissions.Location = New System.Drawing.Point(1450, 108)
        Me.bPrintCommissions.Name = "bPrintCommissions"
        Me.bPrintCommissions.Size = New System.Drawing.Size(30, 22)
        Me.bPrintCommissions.StyleController = Me.LayoutControl3
        Me.bPrintCommissions.TabIndex = 77
        '
        'bEnvoyerCommissionVendeur
        '
        Me.bEnvoyerCommissionVendeur.ImageOptions.Image = CType(resources.GetObject("bEnvoyerCommissionVendeur.ImageOptions.Image"), System.Drawing.Image)
        Me.bEnvoyerCommissionVendeur.Location = New System.Drawing.Point(1416, 108)
        Me.bEnvoyerCommissionVendeur.Name = "bEnvoyerCommissionVendeur"
        Me.bEnvoyerCommissionVendeur.Size = New System.Drawing.Size(30, 22)
        Me.bEnvoyerCommissionVendeur.StyleController = Me.LayoutControl3
        Me.bEnvoyerCommissionVendeur.TabIndex = 76
        '
        'bEnvoyerLivraisonsRestaurants
        '
        Me.bEnvoyerLivraisonsRestaurants.ImageOptions.Image = CType(resources.GetObject("bEnvoyerLivraisonsRestaurants.ImageOptions.Image"), System.Drawing.Image)
        Me.bEnvoyerLivraisonsRestaurants.Location = New System.Drawing.Point(1416, 212)
        Me.bEnvoyerLivraisonsRestaurants.Name = "bEnvoyerLivraisonsRestaurants"
        Me.bEnvoyerLivraisonsRestaurants.Size = New System.Drawing.Size(30, 22)
        Me.bEnvoyerLivraisonsRestaurants.StyleController = Me.LayoutControl3
        Me.bEnvoyerLivraisonsRestaurants.TabIndex = 74
        '
        'bEnvoyerEtatCompte
        '
        Me.bEnvoyerEtatCompte.ImageOptions.Image = CType(resources.GetObject("bEnvoyerEtatCompte.ImageOptions.Image"), System.Drawing.Image)
        Me.bEnvoyerEtatCompte.Location = New System.Drawing.Point(1416, 186)
        Me.bEnvoyerEtatCompte.Name = "bEnvoyerEtatCompte"
        Me.bEnvoyerEtatCompte.Size = New System.Drawing.Size(30, 22)
        Me.bEnvoyerEtatCompte.StyleController = Me.LayoutControl3
        Me.bEnvoyerEtatCompte.TabIndex = 73
        '
        'bEnvoyerPaye
        '
        Me.bEnvoyerPaye.ImageOptions.Image = CType(resources.GetObject("bEnvoyerPaye.ImageOptions.Image"), System.Drawing.Image)
        Me.bEnvoyerPaye.Location = New System.Drawing.Point(1416, 160)
        Me.bEnvoyerPaye.Name = "bEnvoyerPaye"
        Me.bEnvoyerPaye.Size = New System.Drawing.Size(30, 22)
        Me.bEnvoyerPaye.StyleController = Me.LayoutControl3
        Me.bEnvoyerPaye.TabIndex = 72
        '
        'bEnvoyerFacture
        '
        Me.bEnvoyerFacture.ImageOptions.Image = CType(resources.GetObject("bEnvoyerFacture.ImageOptions.Image"), System.Drawing.Image)
        Me.bEnvoyerFacture.Location = New System.Drawing.Point(1416, 134)
        Me.bEnvoyerFacture.Name = "bEnvoyerFacture"
        Me.bEnvoyerFacture.Size = New System.Drawing.Size(30, 22)
        Me.bEnvoyerFacture.StyleController = Me.LayoutControl3
        Me.bEnvoyerFacture.TabIndex = 71
        '
        'bPrintLivraisonR
        '
        Me.bPrintLivraisonR.ImageOptions.Image = CType(resources.GetObject("bPrintLivraisonR.ImageOptions.Image"), System.Drawing.Image)
        Me.bPrintLivraisonR.Location = New System.Drawing.Point(1450, 212)
        Me.bPrintLivraisonR.Name = "bPrintLivraisonR"
        Me.bPrintLivraisonR.Size = New System.Drawing.Size(30, 22)
        Me.bPrintLivraisonR.StyleController = Me.LayoutControl3
        Me.bPrintLivraisonR.TabIndex = 69
        '
        'bPrintEtat
        '
        Me.bPrintEtat.ImageOptions.Image = CType(resources.GetObject("bPrintEtat.ImageOptions.Image"), System.Drawing.Image)
        Me.bPrintEtat.Location = New System.Drawing.Point(1450, 186)
        Me.bPrintEtat.Name = "bPrintEtat"
        Me.bPrintEtat.Size = New System.Drawing.Size(30, 22)
        Me.bPrintEtat.StyleController = Me.LayoutControl3
        Me.bPrintEtat.TabIndex = 68
        '
        'bPrintPaye
        '
        Me.bPrintPaye.ImageOptions.Image = CType(resources.GetObject("bPrintPaye.ImageOptions.Image"), System.Drawing.Image)
        Me.bPrintPaye.Location = New System.Drawing.Point(1450, 160)
        Me.bPrintPaye.Name = "bPrintPaye"
        Me.bPrintPaye.Size = New System.Drawing.Size(30, 22)
        Me.bPrintPaye.StyleController = Me.LayoutControl3
        Me.bPrintPaye.TabIndex = 67
        '
        'bPrintFacture
        '
        Me.bPrintFacture.ImageOptions.Image = CType(resources.GetObject("bPrintFacture.ImageOptions.Image"), System.Drawing.Image)
        Me.bPrintFacture.Location = New System.Drawing.Point(1450, 134)
        Me.bPrintFacture.Name = "bPrintFacture"
        Me.bPrintFacture.Size = New System.Drawing.Size(30, 22)
        Me.bPrintFacture.StyleController = Me.LayoutControl3
        Me.bPrintFacture.TabIndex = 66
        '
        'optTypePeriode
        '
        Me.optTypePeriode.Location = New System.Drawing.Point(196, 170)
        Me.optTypePeriode.MenuManager = Me.RibbonControl
        Me.optTypePeriode.Name = "optTypePeriode"
        Me.optTypePeriode.Properties.Columns = 2
        Me.optTypePeriode.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "Périodes"), New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "Etats")})
        Me.optTypePeriode.Size = New System.Drawing.Size(539, 42)
        Me.optTypePeriode.StyleController = Me.LayoutControl3
        Me.optTypePeriode.TabIndex = 62
        '
        'bResetLivraisonsRestaurant
        '
        Me.bResetLivraisonsRestaurant.Location = New System.Drawing.Point(1484, 212)
        Me.bResetLivraisonsRestaurant.Name = "bResetLivraisonsRestaurant"
        Me.bResetLivraisonsRestaurant.Size = New System.Drawing.Size(26, 22)
        Me.bResetLivraisonsRestaurant.StyleController = Me.LayoutControl3
        Me.bResetLivraisonsRestaurant.TabIndex = 60
        Me.bResetLivraisonsRestaurant.Text = "..."
        '
        'bResetPeriode
        '
        Me.bResetPeriode.Location = New System.Drawing.Point(721, 108)
        Me.bResetPeriode.Name = "bResetPeriode"
        Me.bResetPeriode.Size = New System.Drawing.Size(26, 22)
        Me.bResetPeriode.StyleController = Me.LayoutControl3
        Me.bResetPeriode.TabIndex = 57
        Me.bResetPeriode.Text = "..."
        '
        'bResetEtatdeCompte
        '
        Me.bResetEtatdeCompte.Location = New System.Drawing.Point(1484, 186)
        Me.bResetEtatdeCompte.Name = "bResetEtatdeCompte"
        Me.bResetEtatdeCompte.Size = New System.Drawing.Size(26, 22)
        Me.bResetEtatdeCompte.StyleController = Me.LayoutControl3
        Me.bResetEtatdeCompte.TabIndex = 54
        Me.bResetEtatdeCompte.Text = "..."
        '
        'cbAnnee
        '
        Me.cbAnnee.Location = New System.Drawing.Point(768, 48)
        Me.cbAnnee.MenuManager = Me.RibbonControl
        Me.cbAnnee.Name = "cbAnnee"
        Me.cbAnnee.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbAnnee.Size = New System.Drawing.Size(171, 20)
        Me.cbAnnee.StyleController = Me.LayoutControl3
        Me.cbAnnee.TabIndex = 52
        '
        'bResetDate
        '
        Me.bResetDate.Location = New System.Drawing.Point(397, 252)
        Me.bResetDate.Name = "bResetDate"
        Me.bResetDate.Size = New System.Drawing.Size(155, 34)
        Me.bResetDate.StyleController = Me.LayoutControl3
        Me.bResetDate.TabIndex = 46
        Me.bResetDate.Text = "Reset Période"
        '
        'bConsolidationDate
        '
        Me.bConsolidationDate.Location = New System.Drawing.Point(556, 252)
        Me.bConsolidationDate.Name = "bConsolidationDate"
        Me.bConsolidationDate.Size = New System.Drawing.Size(191, 34)
        Me.bConsolidationDate.StyleController = Me.LayoutControl3
        Me.bConsolidationDate.TabIndex = 45
        Me.bConsolidationDate.Text = "Date de Consolidation mensuel"
        '
        'cbFactures
        '
        Me.cbFactures.Location = New System.Drawing.Point(923, 134)
        Me.cbFactures.MenuManager = Me.RibbonControl
        Me.cbFactures.Name = "cbFactures"
        Me.cbFactures.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbFactures.Properties.ForceUpdateEditValue = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbFactures.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard
        Me.cbFactures.Size = New System.Drawing.Size(489, 20)
        Me.cbFactures.StyleController = Me.LayoutControl3
        Me.cbFactures.TabIndex = 44
        '
        'cbPayes
        '
        Me.cbPayes.Location = New System.Drawing.Point(923, 160)
        Me.cbPayes.MenuManager = Me.RibbonControl
        Me.cbPayes.Name = "cbPayes"
        Me.cbPayes.Properties.AllowMultiSelect = True
        Me.cbPayes.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbPayes.Properties.ForceUpdateEditValue = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbPayes.Properties.SelectAllItemVisible = False
        Me.cbPayes.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard
        Me.cbPayes.Size = New System.Drawing.Size(489, 20)
        Me.cbPayes.StyleController = Me.LayoutControl3
        Me.cbPayes.TabIndex = 43
        '
        'Calendar
        '
        Me.Calendar.AutoSize = False
        Me.Calendar.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.Calendar.DateTime = New Date(2020, 9, 30, 0, 0, 0, 0)
        Me.Calendar.EditValue = New Date(2020, 9, 30, 0, 0, 0, 0)
        Me.Calendar.Location = New System.Drawing.Point(782, 350)
        Me.Calendar.Name = "Calendar"
        Me.Calendar.Size = New System.Drawing.Size(740, 227)
        Me.Calendar.StyleController = Me.LayoutControl3
        Me.Calendar.TabIndex = 41
        '
        'ProgressBarControl1
        '
        Me.ProgressBarControl1.Location = New System.Drawing.Point(24, 581)
        Me.ProgressBarControl1.MenuManager = Me.RibbonControl
        Me.ProgressBarControl1.Name = "ProgressBarControl1"
        Me.ProgressBarControl1.Size = New System.Drawing.Size(1498, 14)
        Me.ProgressBarControl1.StyleController = Me.LayoutControl3
        Me.ProgressBarControl1.TabIndex = 38
        '
        'bFiltrerDate
        '
        Me.bFiltrerDate.Location = New System.Drawing.Point(36, 252)
        Me.bFiltrerDate.Name = "bFiltrerDate"
        Me.bFiltrerDate.Size = New System.Drawing.Size(357, 34)
        Me.bFiltrerDate.StyleController = Me.LayoutControl3
        Me.bFiltrerDate.TabIndex = 37
        Me.bFiltrerDate.Text = "Créer la période de facturation et paye"
        '
        'txtMobilusXLS
        '
        Me.txtMobilusXLS.Location = New System.Drawing.Point(172, 350)
        Me.txtMobilusXLS.Name = "txtMobilusXLS"
        Me.txtMobilusXLS.Size = New System.Drawing.Size(478, 20)
        Me.txtMobilusXLS.StyleController = Me.LayoutControl3
        Me.txtMobilusXLS.TabIndex = 0
        '
        'DateEdit1
        '
        Me.DateEdit1.EditValue = Nothing
        Me.DateEdit1.Location = New System.Drawing.Point(172, 402)
        Me.DateEdit1.Name = "DateEdit1"
        Me.DateEdit1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEdit1.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEdit1.Properties.DisplayFormat.FormatString = "g"
        Me.DateEdit1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.DateEdit1.Properties.EditFormat.FormatString = "g"
        Me.DateEdit1.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.DateEdit1.Properties.MaskSettings.Set("mask", "g")
        Me.DateEdit1.Size = New System.Drawing.Size(606, 20)
        Me.DateEdit1.StyleController = Me.LayoutControl3
        Me.DateEdit1.TabIndex = 7
        Me.DateEdit1.Visible = False
        '
        'bImportMobilus
        '
        Me.bImportMobilus.Enabled = False
        Me.bImportMobilus.ImageOptions.ImageIndex = 2
        Me.bImportMobilus.ImageOptions.ImageList = Me.navbarImageCollectionLarge3
        Me.bImportMobilus.Location = New System.Drawing.Point(24, 426)
        Me.bImportMobilus.Name = "bImportMobilus"
        Me.bImportMobilus.Size = New System.Drawing.Size(754, 151)
        Me.bImportMobilus.StyleController = Me.LayoutControl3
        Me.bImportMobilus.TabIndex = 8
        Me.bImportMobilus.Text = "Importer mobilus"
        '
        'navbarImageCollectionLarge3
        '
        Me.navbarImageCollectionLarge3.ImageSize = New System.Drawing.Size(32, 32)
        Me.navbarImageCollectionLarge3.ImageStream = CType(resources.GetObject("navbarImageCollectionLarge3.ImageStream"), DevExpress.Utils.ImageCollectionStreamer)
        Me.navbarImageCollectionLarge3.TransparentColor = System.Drawing.Color.Transparent
        Me.navbarImageCollectionLarge3.Images.SetKeyName(0, "Mail_32x32.png")
        Me.navbarImageCollectionLarge3.Images.SetKeyName(1, "Organizer_32x32.png")
        Me.navbarImageCollectionLarge3.Images.SetKeyName(2, "table-import-512.png")
        Me.navbarImageCollectionLarge3.Images.SetKeyName(3, "images.png")
        Me.navbarImageCollectionLarge3.Images.SetKeyName(4, "transparent-bill-icon-payment-icon-invoice-icon-5e23aca6b774e8.609381951579396262" &
        "7514.jpg")
        Me.navbarImageCollectionLarge3.Images.SetKeyName(5, "2244482.png")
        '
        'bSelectfile
        '
        Me.bSelectfile.Enabled = False
        Me.bSelectfile.Location = New System.Drawing.Point(654, 350)
        Me.bSelectfile.Name = "bSelectfile"
        Me.bSelectfile.Size = New System.Drawing.Size(124, 22)
        Me.bSelectfile.StyleController = Me.LayoutControl3
        Me.bSelectfile.TabIndex = 2
        Me.bSelectfile.Text = "Sélectionner fichier"
        '
        'bSupprimerI
        '
        Me.bSupprimerI.Location = New System.Drawing.Point(654, 376)
        Me.bSupprimerI.Name = "bSupprimerI"
        Me.bSupprimerI.Size = New System.Drawing.Size(124, 22)
        Me.bSupprimerI.StyleController = Me.LayoutControl3
        Me.bSupprimerI.TabIndex = 6
        Me.bSupprimerI.Text = "Supprimer"
        '
        'cbFichierImport
        '
        Me.cbFichierImport.Location = New System.Drawing.Point(172, 376)
        Me.cbFichierImport.Name = "cbFichierImport"
        Me.cbFichierImport.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbFichierImport.Properties.PopupFilterMode = DevExpress.XtraEditors.PopupFilterMode.Contains
        Me.cbFichierImport.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard
        Me.cbFichierImport.Size = New System.Drawing.Size(448, 20)
        Me.cbFichierImport.StyleController = Me.LayoutControl3
        Me.cbFichierImport.TabIndex = 31
        '
        'bAnnulerFichierImport
        '
        Me.bAnnulerFichierImport.Location = New System.Drawing.Point(624, 376)
        Me.bAnnulerFichierImport.Name = "bAnnulerFichierImport"
        Me.bAnnulerFichierImport.Size = New System.Drawing.Size(26, 22)
        Me.bAnnulerFichierImport.StyleController = Me.LayoutControl3
        Me.bAnnulerFichierImport.TabIndex = 33
        Me.bAnnulerFichierImport.Text = "..."
        '
        'tbDateFrom
        '
        Me.tbDateFrom.EditValue = Nothing
        Me.tbDateFrom.Location = New System.Drawing.Point(196, 216)
        Me.tbDateFrom.Name = "tbDateFrom"
        Me.tbDateFrom.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.tbDateFrom.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.tbDateFrom.Properties.DisplayFormat.FormatString = "MM-dd-yy H:mm"
        Me.tbDateFrom.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.tbDateFrom.Properties.EditFormat.FormatString = "MM-dd-yy H:mm"
        Me.tbDateFrom.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.tbDateFrom.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.tbDateFrom.Properties.MaskSettings.Set("mask", "MM-dd-yy H:mm")
        Me.tbDateFrom.Size = New System.Drawing.Size(189, 20)
        Me.tbDateFrom.StyleController = Me.LayoutControl3
        Me.tbDateFrom.TabIndex = 22
        '
        'tbDateTo
        '
        Me.tbDateTo.EditValue = Nothing
        Me.tbDateTo.Location = New System.Drawing.Point(537, 216)
        Me.tbDateTo.Name = "tbDateTo"
        Me.tbDateTo.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.tbDateTo.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.tbDateTo.Properties.DisplayFormat.FormatString = "MM-dd-yy H:mm"
        Me.tbDateTo.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.tbDateTo.Properties.EditFormat.FormatString = "MM-dd-yy H:mm"
        Me.tbDateTo.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.tbDateTo.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.tbDateTo.Properties.MaskSettings.Set("mask", "MM-dd-yy H:mm")
        Me.tbDateTo.Size = New System.Drawing.Size(198, 20)
        Me.tbDateTo.StyleController = Me.LayoutControl3
        Me.tbDateTo.TabIndex = 23
        '
        'cbPeriodes
        '
        Me.cbPeriodes.Location = New System.Drawing.Point(184, 108)
        Me.cbPeriodes.Name = "cbPeriodes"
        Me.cbPeriodes.Properties.AcceptEditorTextAsNewValue = DevExpress.Utils.DefaultBoolean.[False]
        Me.cbPeriodes.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold)
        Me.cbPeriodes.Properties.Appearance.Options.UseFont = True
        Me.cbPeriodes.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbPeriodes.Properties.PopupFilterMode = DevExpress.XtraEditors.PopupFilterMode.Contains
        Me.cbPeriodes.Size = New System.Drawing.Size(533, 22)
        Me.cbPeriodes.StyleController = Me.LayoutControl3
        Me.cbPeriodes.TabIndex = 28
        '
        'bAnnulerPayes
        '
        Me.bAnnulerPayes.Location = New System.Drawing.Point(1484, 160)
        Me.bAnnulerPayes.Name = "bAnnulerPayes"
        Me.bAnnulerPayes.Size = New System.Drawing.Size(26, 22)
        Me.bAnnulerPayes.StyleController = Me.LayoutControl3
        Me.bAnnulerPayes.TabIndex = 35
        Me.bAnnulerPayes.Text = "..."
        '
        'bAnnulerFactures
        '
        Me.bAnnulerFactures.Location = New System.Drawing.Point(1484, 134)
        Me.bAnnulerFactures.Name = "bAnnulerFactures"
        Me.bAnnulerFactures.Size = New System.Drawing.Size(26, 22)
        Me.bAnnulerFactures.StyleController = Me.LayoutControl3
        Me.bAnnulerFactures.TabIndex = 34
        Me.bAnnulerFactures.Text = "..."
        '
        'cbEtatsDeComptes
        '
        Me.cbEtatsDeComptes.Location = New System.Drawing.Point(923, 186)
        Me.cbEtatsDeComptes.Name = "cbEtatsDeComptes"
        Me.cbEtatsDeComptes.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbEtatsDeComptes.Properties.ForceUpdateEditValue = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbEtatsDeComptes.Size = New System.Drawing.Size(489, 20)
        Me.cbEtatsDeComptes.StyleController = Me.LayoutControl3
        Me.cbEtatsDeComptes.TabIndex = 44
        '
        'cbLivraisonRestaurants
        '
        Me.cbLivraisonRestaurants.Location = New System.Drawing.Point(923, 212)
        Me.cbLivraisonRestaurants.Name = "cbLivraisonRestaurants"
        Me.cbLivraisonRestaurants.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbLivraisonRestaurants.Properties.ForceUpdateEditValue = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbLivraisonRestaurants.Size = New System.Drawing.Size(489, 20)
        Me.cbLivraisonRestaurants.StyleController = Me.LayoutControl3
        Me.cbLivraisonRestaurants.TabIndex = 44
        '
        'cbCommissions
        '
        Me.cbCommissions.Location = New System.Drawing.Point(923, 108)
        Me.cbCommissions.Name = "cbCommissions"
        Me.cbCommissions.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbCommissions.Properties.ForceUpdateEditValue = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbCommissions.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard
        Me.cbCommissions.Size = New System.Drawing.Size(489, 20)
        Me.cbCommissions.StyleController = Me.LayoutControl3
        Me.cbCommissions.TabIndex = 44
        '
        'cbRapportTaxation
        '
        Me.cbRapportTaxation.Location = New System.Drawing.Point(923, 238)
        Me.cbRapportTaxation.Name = "cbRapportTaxation"
        Me.cbRapportTaxation.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbRapportTaxation.Properties.ForceUpdateEditValue = DevExpress.Utils.DefaultBoolean.[True]
        Me.cbRapportTaxation.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard
        Me.cbRapportTaxation.Size = New System.Drawing.Size(489, 20)
        Me.cbRapportTaxation.StyleController = Me.LayoutControl3
        Me.cbRapportTaxation.TabIndex = 44
        '
        'LayoutControlGroup6
        '
        Me.LayoutControlGroup6.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup6.GroupBordersVisible = False
        Me.LayoutControlGroup6.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutBas, Me.LayoutControlGroup1})
        Me.LayoutControlGroup6.Name = "Root"
        Me.LayoutControlGroup6.Size = New System.Drawing.Size(1546, 619)
        Me.LayoutControlGroup6.TextVisible = False
        '
        'LayoutBas
        '
        Me.LayoutBas.AppearanceGroup.BorderColor = System.Drawing.Color.YellowGreen
        Me.LayoutBas.AppearanceGroup.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.LayoutBas.AppearanceGroup.Options.UseBorderColor = True
        Me.LayoutBas.AppearanceGroup.Options.UseFont = True
        Me.LayoutBas.AppearanceGroup.Options.UseTextOptions = True
        Me.LayoutBas.AppearanceGroup.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.LayoutBas.CustomizationFormText = "Importer Fichier Mobilus"
        Me.LayoutBas.Enabled = False
        Me.LayoutBas.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem1, Me.LayoutControlItem10, Me.LayoutControlItem17, Me.LayoutControlItem2, Me.LayoutControlItem26, Me.LayoutControlItem22, Me.LayoutControlItem23, Me.LayoutControlItem28, Me.LayoutControlItem3})
        Me.LayoutBas.Location = New System.Drawing.Point(0, 302)
        Me.LayoutBas.Name = "LayoutBas"
        Me.LayoutBas.Size = New System.Drawing.Size(1526, 297)
        Me.LayoutBas.Text = "Importer Fichier Mobilus"
        '
        'LayoutControlItem1
        '
        Me.LayoutControlItem1.Control = Me.txtMobilusXLS
        Me.LayoutControlItem1.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem1.CustomizationFormText = "Fichier mobilus (xls)"
        Me.LayoutControlItem1.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem1.Name = "LayoutControlItem1"
        Me.LayoutControlItem1.Size = New System.Drawing.Size(630, 26)
        Me.LayoutControlItem1.Text = "Fichier mobilus (xls)"
        Me.LayoutControlItem1.TextSize = New System.Drawing.Size(144, 13)
        '
        'LayoutControlItem10
        '
        Me.LayoutControlItem10.Control = Me.DateEdit1
        Me.LayoutControlItem10.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem10.CustomizationFormText = "LayoutControlItem10"
        Me.LayoutControlItem10.Location = New System.Drawing.Point(0, 52)
        Me.LayoutControlItem10.Name = "LayoutControlItem10"
        Me.LayoutControlItem10.Size = New System.Drawing.Size(758, 24)
        Me.LayoutControlItem10.TextSize = New System.Drawing.Size(144, 13)
        Me.LayoutControlItem10.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never
        '
        'LayoutControlItem17
        '
        Me.LayoutControlItem17.Control = Me.bImportMobilus
        Me.LayoutControlItem17.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem17.CustomizationFormText = "LayoutControlItem17"
        Me.LayoutControlItem17.Location = New System.Drawing.Point(0, 76)
        Me.LayoutControlItem17.MinSize = New System.Drawing.Size(128, 42)
        Me.LayoutControlItem17.Name = "LayoutControlItem17"
        Me.LayoutControlItem17.Size = New System.Drawing.Size(758, 155)
        Me.LayoutControlItem17.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem17.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem17.TextVisible = False
        '
        'LayoutControlItem2
        '
        Me.LayoutControlItem2.Control = Me.bSelectfile
        Me.LayoutControlItem2.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem2.CustomizationFormText = "LayoutControlItem2"
        Me.LayoutControlItem2.Location = New System.Drawing.Point(630, 0)
        Me.LayoutControlItem2.Name = "LayoutControlItem2"
        Me.LayoutControlItem2.Size = New System.Drawing.Size(128, 26)
        Me.LayoutControlItem2.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem2.TextVisible = False
        '
        'LayoutControlItem26
        '
        Me.LayoutControlItem26.Control = Me.bSupprimerI
        Me.LayoutControlItem26.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem26.CustomizationFormText = "LayoutControlItem26"
        Me.LayoutControlItem26.Location = New System.Drawing.Point(630, 26)
        Me.LayoutControlItem26.Name = "LayoutControlItem26"
        Me.LayoutControlItem26.Size = New System.Drawing.Size(128, 26)
        Me.LayoutControlItem26.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem26.TextVisible = False
        '
        'LayoutControlItem22
        '
        Me.LayoutControlItem22.Control = Me.cbFichierImport
        Me.LayoutControlItem22.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem22.CustomizationFormText = "Liste des fichiers importés"
        Me.LayoutControlItem22.Location = New System.Drawing.Point(0, 26)
        Me.LayoutControlItem22.Name = "LayoutControlItem22"
        Me.LayoutControlItem22.Size = New System.Drawing.Size(600, 26)
        Me.LayoutControlItem22.Text = "Liste des fichiers importés"
        Me.LayoutControlItem22.TextSize = New System.Drawing.Size(144, 13)
        '
        'LayoutControlItem23
        '
        Me.LayoutControlItem23.Control = Me.bAnnulerFichierImport
        Me.LayoutControlItem23.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem23.CustomizationFormText = "LayoutControlItem23"
        Me.LayoutControlItem23.Location = New System.Drawing.Point(600, 26)
        Me.LayoutControlItem23.Name = "LayoutControlItem23"
        Me.LayoutControlItem23.Size = New System.Drawing.Size(30, 26)
        Me.LayoutControlItem23.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem23.TextVisible = False
        '
        'LayoutControlItem28
        '
        Me.LayoutControlItem28.Control = Me.Calendar
        Me.LayoutControlItem28.Location = New System.Drawing.Point(758, 0)
        Me.LayoutControlItem28.Name = "LayoutControlItem28"
        Me.LayoutControlItem28.Size = New System.Drawing.Size(744, 231)
        Me.LayoutControlItem28.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem28.TextVisible = False
        '
        'LayoutControlItem3
        '
        Me.LayoutControlItem3.Control = Me.ProgressBarControl1
        Me.LayoutControlItem3.Location = New System.Drawing.Point(0, 231)
        Me.LayoutControlItem3.Name = "LayoutControlItem3"
        Me.LayoutControlItem3.Size = New System.Drawing.Size(1502, 18)
        Me.LayoutControlItem3.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem3.TextVisible = False
        '
        'LayoutControlGroup1
        '
        Me.LayoutControlGroup1.AppearanceGroup.BackColor = System.Drawing.Color.White
        Me.LayoutControlGroup1.AppearanceGroup.BackColor2 = System.Drawing.Color.White
        Me.LayoutControlGroup1.AppearanceGroup.BorderColor = System.Drawing.Color.CornflowerBlue
        Me.LayoutControlGroup1.AppearanceGroup.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.LayoutControlGroup1.AppearanceGroup.Options.UseBackColor = True
        Me.LayoutControlGroup1.AppearanceGroup.Options.UseBorderColor = True
        Me.LayoutControlGroup1.AppearanceGroup.Options.UseFont = True
        Me.LayoutControlGroup1.AppearanceGroup.Options.UseTextOptions = True
        Me.LayoutControlGroup1.AppearanceGroup.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.LayoutControlGroup1.AppearanceTabPage.PageClient.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.LayoutControlGroup1.AppearanceTabPage.PageClient.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.LayoutControlGroup1.AppearanceTabPage.PageClient.Options.UseBackColor = True
        Me.LayoutControlGroup1.GroupStyle = DevExpress.Utils.GroupStyle.Card
        Me.LayoutControlGroup1.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlGroup8, Me.LayoutDroite, Me.LayoutControlItem40, Me.EmptySpaceItem6, Me.EmptySpaceItem7})
        Me.LayoutControlGroup1.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup1.Name = "LayoutControlGroup1"
        Me.LayoutControlGroup1.Size = New System.Drawing.Size(1526, 302)
        Me.LayoutControlGroup1.Text = "Filtrer"
        '
        'LayoutControlGroup8
        '
        Me.LayoutControlGroup8.AppearanceGroup.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.LayoutControlGroup8.AppearanceGroup.BackColor2 = System.Drawing.Color.Yellow
        Me.LayoutControlGroup8.AppearanceGroup.BorderColor = System.Drawing.Color.Yellow
        Me.LayoutControlGroup8.AppearanceGroup.Options.UseBackColor = True
        Me.LayoutControlGroup8.AppearanceGroup.Options.UseBorderColor = True
        Me.LayoutControlGroup8.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem9, Me.LayoutControlItem32, Me.LayoutControlItem27, Me.LayoutControlGroup3, Me.LayoutControlItem14, Me.LayoutControlItem13})
        Me.LayoutControlGroup8.Location = New System.Drawing.Point(0, 24)
        Me.LayoutControlGroup8.Name = "LayoutControlGroup8"
        Me.LayoutControlGroup8.Size = New System.Drawing.Size(739, 230)
        Me.LayoutControlGroup8.Text = " Périodes de facturation et payes"
        '
        'LayoutControlItem9
        '
        Me.LayoutControlItem9.Control = Me.bFiltrerDate
        Me.LayoutControlItem9.Location = New System.Drawing.Point(0, 144)
        Me.LayoutControlItem9.MinSize = New System.Drawing.Size(201, 26)
        Me.LayoutControlItem9.Name = "LayoutControlItem9"
        Me.LayoutControlItem9.Size = New System.Drawing.Size(361, 38)
        Me.LayoutControlItem9.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem9.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem9.TextVisible = False
        '
        'LayoutControlItem32
        '
        Me.LayoutControlItem32.Control = Me.bResetDate
        Me.LayoutControlItem32.Location = New System.Drawing.Point(361, 144)
        Me.LayoutControlItem32.MinSize = New System.Drawing.Size(80, 26)
        Me.LayoutControlItem32.Name = "LayoutControlItem32"
        Me.LayoutControlItem32.Size = New System.Drawing.Size(159, 38)
        Me.LayoutControlItem32.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem32.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem32.TextVisible = False
        '
        'LayoutControlItem27
        '
        Me.LayoutControlItem27.Control = Me.bConsolidationDate
        Me.LayoutControlItem27.Location = New System.Drawing.Point(520, 144)
        Me.LayoutControlItem27.MinSize = New System.Drawing.Size(160, 26)
        Me.LayoutControlItem27.Name = "LayoutControlItem27"
        Me.LayoutControlItem27.Size = New System.Drawing.Size(195, 38)
        Me.LayoutControlItem27.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem27.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem27.TextVisible = False
        '
        'LayoutControlGroup3
        '
        Me.LayoutControlGroup3.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem5, Me.LayoutControlItem6, Me.LayoutControlItem48})
        Me.LayoutControlGroup3.Location = New System.Drawing.Point(0, 26)
        Me.LayoutControlGroup3.Name = "LayoutControlGroup3"
        Me.LayoutControlGroup3.Size = New System.Drawing.Size(715, 118)
        Me.LayoutControlGroup3.Text = "Période courante"
        '
        'LayoutControlItem5
        '
        Me.LayoutControlItem5.Control = Me.tbDateFrom
        Me.LayoutControlItem5.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem5.CustomizationFormText = "Date du"
        Me.LayoutControlItem5.Location = New System.Drawing.Point(0, 46)
        Me.LayoutControlItem5.Name = "LayoutControlItem5"
        Me.LayoutControlItem5.Size = New System.Drawing.Size(341, 24)
        Me.LayoutControlItem5.Text = "Date du"
        Me.LayoutControlItem5.TextSize = New System.Drawing.Size(144, 13)
        '
        'LayoutControlItem6
        '
        Me.LayoutControlItem6.Control = Me.tbDateTo
        Me.LayoutControlItem6.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem6.CustomizationFormText = "Date Au"
        Me.LayoutControlItem6.Location = New System.Drawing.Point(341, 46)
        Me.LayoutControlItem6.Name = "LayoutControlItem6"
        Me.LayoutControlItem6.Size = New System.Drawing.Size(350, 24)
        Me.LayoutControlItem6.Text = "Date Au"
        Me.LayoutControlItem6.TextSize = New System.Drawing.Size(144, 13)
        '
        'LayoutControlItem48
        '
        Me.LayoutControlItem48.Control = Me.optTypePeriode
        Me.LayoutControlItem48.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem48.Name = "LayoutControlItem48"
        Me.LayoutControlItem48.Size = New System.Drawing.Size(691, 46)
        Me.LayoutControlItem48.Text = "Type de Périodes"
        Me.LayoutControlItem48.TextSize = New System.Drawing.Size(144, 13)
        '
        'LayoutControlItem14
        '
        Me.LayoutControlItem14.AppearanceItemCaption.BackColor = System.Drawing.Color.Yellow
        Me.LayoutControlItem14.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold)
        Me.LayoutControlItem14.AppearanceItemCaption.FontStyleDelta = System.Drawing.FontStyle.Bold
        Me.LayoutControlItem14.AppearanceItemCaption.ForeColor = System.Drawing.Color.Black
        Me.LayoutControlItem14.AppearanceItemCaption.Options.UseBackColor = True
        Me.LayoutControlItem14.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem14.AppearanceItemCaption.Options.UseForeColor = True
        Me.LayoutControlItem14.Control = Me.cbPeriodes
        Me.LayoutControlItem14.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem14.CustomizationFormText = "Périodes"
        Me.LayoutControlItem14.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem14.Name = "LayoutControlItem14"
        Me.LayoutControlItem14.Size = New System.Drawing.Size(685, 26)
        Me.LayoutControlItem14.Text = "Périodes"
        Me.LayoutControlItem14.TextSize = New System.Drawing.Size(144, 19)
        '
        'LayoutControlItem13
        '
        Me.LayoutControlItem13.Control = Me.bResetPeriode
        Me.LayoutControlItem13.Location = New System.Drawing.Point(685, 0)
        Me.LayoutControlItem13.Name = "LayoutControlItem13"
        Me.LayoutControlItem13.Size = New System.Drawing.Size(30, 26)
        Me.LayoutControlItem13.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem13.TextVisible = False
        '
        'LayoutDroite
        '
        Me.LayoutDroite.AppearanceGroup.BackColor = System.Drawing.Color.Lime
        Me.LayoutDroite.AppearanceGroup.BackColor2 = System.Drawing.Color.Aqua
        Me.LayoutDroite.AppearanceGroup.BorderColor = System.Drawing.Color.Lime
        Me.LayoutDroite.AppearanceGroup.Options.UseBackColor = True
        Me.LayoutDroite.AppearanceGroup.Options.UseBorderColor = True
        Me.LayoutDroite.AppearanceTabPage.PageClient.BackColor = System.Drawing.Color.Lime
        Me.LayoutDroite.AppearanceTabPage.PageClient.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.LayoutDroite.AppearanceTabPage.PageClient.Options.UseBackColor = True
        Me.LayoutDroite.CustomizationFormText = "Périodes d'Etats de comptes, achats et ventes"
        Me.LayoutDroite.Enabled = False
        Me.LayoutDroite.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutEtat, Me.LayoutControlItem42, Me.LayoutEtatLivraisonRestaurant, Me.LayoutControlItem69, Me.LayoutFactures, Me.LayoutPayes, Me.LayoutControlItem24, Me.LayoutControlItem25, Me.LayoutControlItem19, Me.LayoutControlItem21, Me.LayoutControlItem35, Me.LayoutControlItem47, Me.LayoutControlItem64, Me.LayoutControlItem72, Me.LayoutControlItem74, Me.LayoutControlItem75, Me.LayoutCommissions, Me.LayoutControlItem37, Me.LayoutControlItem46, Me.LayoutControlItem51, Me.LayoutCommissions1, Me.LayoutControlItem63, Me.LayoutControlItem65, Me.bResetTaxationLivreurs, Me.LayoutControlItem20, Me.LayoutControlItem68, Me.LayoutControlItem71, Me.LayoutControlItem73})
        Me.LayoutDroite.Location = New System.Drawing.Point(739, 24)
        Me.LayoutDroite.Name = "LayoutDroite"
        Me.LayoutDroite.Size = New System.Drawing.Size(763, 230)
        Me.LayoutDroite.Text = "Factures, payes, Etats de comptes, achats et ventes et rapports Livraisons"
        '
        'LayoutEtat
        '
        Me.LayoutEtat.Control = Me.cbEtatsDeComptes
        Me.LayoutEtat.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutEtat.CustomizationFormText = "Liste des factures"
        Me.LayoutEtat.Location = New System.Drawing.Point(0, 78)
        Me.LayoutEtat.Name = "LayoutEtat"
        Me.LayoutEtat.Size = New System.Drawing.Size(641, 26)
        Me.LayoutEtat.Text = "Liste des etats de compte"
        Me.LayoutEtat.TextSize = New System.Drawing.Size(144, 13)
        '
        'LayoutControlItem42
        '
        Me.LayoutControlItem42.Control = Me.bResetEtatdeCompte
        Me.LayoutControlItem42.Location = New System.Drawing.Point(709, 78)
        Me.LayoutControlItem42.Name = "LayoutControlItem42"
        Me.LayoutControlItem42.Size = New System.Drawing.Size(30, 26)
        Me.LayoutControlItem42.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem42.TextVisible = False
        '
        'LayoutEtatLivraisonRestaurant
        '
        Me.LayoutEtatLivraisonRestaurant.Control = Me.cbLivraisonRestaurants
        Me.LayoutEtatLivraisonRestaurant.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutEtatLivraisonRestaurant.CustomizationFormText = "Livraisons par Restaurant"
        Me.LayoutEtatLivraisonRestaurant.Location = New System.Drawing.Point(0, 104)
        Me.LayoutEtatLivraisonRestaurant.Name = "LayoutEtatLivraisonRestaurant"
        Me.LayoutEtatLivraisonRestaurant.Size = New System.Drawing.Size(641, 26)
        Me.LayoutEtatLivraisonRestaurant.Text = "Livraisons par Restaurant"
        Me.LayoutEtatLivraisonRestaurant.TextSize = New System.Drawing.Size(144, 13)
        '
        'LayoutControlItem69
        '
        Me.LayoutControlItem69.Control = Me.bResetLivraisonsRestaurant
        Me.LayoutControlItem69.Location = New System.Drawing.Point(709, 104)
        Me.LayoutControlItem69.Name = "LayoutControlItem69"
        Me.LayoutControlItem69.Size = New System.Drawing.Size(30, 26)
        Me.LayoutControlItem69.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem69.TextVisible = False
        '
        'LayoutFactures
        '
        Me.LayoutFactures.Control = Me.cbFactures
        Me.LayoutFactures.Location = New System.Drawing.Point(0, 26)
        Me.LayoutFactures.Name = "LayoutFactures"
        Me.LayoutFactures.Size = New System.Drawing.Size(641, 26)
        Me.LayoutFactures.Text = "Liste des factures"
        Me.LayoutFactures.TextSize = New System.Drawing.Size(144, 13)
        '
        'LayoutPayes
        '
        Me.LayoutPayes.Control = Me.cbPayes
        Me.LayoutPayes.Location = New System.Drawing.Point(0, 52)
        Me.LayoutPayes.Name = "LayoutPayes"
        Me.LayoutPayes.Size = New System.Drawing.Size(641, 26)
        Me.LayoutPayes.Text = "Liste des payes"
        Me.LayoutPayes.TextSize = New System.Drawing.Size(144, 13)
        '
        'LayoutControlItem24
        '
        Me.LayoutControlItem24.Control = Me.bAnnulerFactures
        Me.LayoutControlItem24.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem24.CustomizationFormText = "LayoutControlItem24"
        Me.LayoutControlItem24.Location = New System.Drawing.Point(709, 26)
        Me.LayoutControlItem24.Name = "LayoutControlItem24"
        Me.LayoutControlItem24.Size = New System.Drawing.Size(30, 26)
        Me.LayoutControlItem24.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem24.TextVisible = False
        '
        'LayoutControlItem25
        '
        Me.LayoutControlItem25.Control = Me.bAnnulerPayes
        Me.LayoutControlItem25.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem25.CustomizationFormText = "LayoutControlItem25"
        Me.LayoutControlItem25.Location = New System.Drawing.Point(709, 52)
        Me.LayoutControlItem25.Name = "LayoutControlItem25"
        Me.LayoutControlItem25.Size = New System.Drawing.Size(30, 26)
        Me.LayoutControlItem25.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem25.TextVisible = False
        '
        'LayoutControlItem19
        '
        Me.LayoutControlItem19.Control = Me.bPrintFacture
        Me.LayoutControlItem19.Location = New System.Drawing.Point(675, 26)
        Me.LayoutControlItem19.Name = "LayoutControlItem19"
        Me.LayoutControlItem19.Size = New System.Drawing.Size(34, 26)
        Me.LayoutControlItem19.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem19.TextVisible = False
        '
        'LayoutControlItem21
        '
        Me.LayoutControlItem21.Control = Me.bPrintPaye
        Me.LayoutControlItem21.Location = New System.Drawing.Point(675, 52)
        Me.LayoutControlItem21.Name = "LayoutControlItem21"
        Me.LayoutControlItem21.Size = New System.Drawing.Size(34, 26)
        Me.LayoutControlItem21.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem21.TextVisible = False
        '
        'LayoutControlItem35
        '
        Me.LayoutControlItem35.Control = Me.bPrintEtat
        Me.LayoutControlItem35.Location = New System.Drawing.Point(675, 78)
        Me.LayoutControlItem35.Name = "LayoutControlItem35"
        Me.LayoutControlItem35.Size = New System.Drawing.Size(34, 26)
        Me.LayoutControlItem35.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem35.TextVisible = False
        '
        'LayoutControlItem47
        '
        Me.LayoutControlItem47.Control = Me.bPrintLivraisonR
        Me.LayoutControlItem47.Location = New System.Drawing.Point(675, 104)
        Me.LayoutControlItem47.Name = "LayoutControlItem47"
        Me.LayoutControlItem47.Size = New System.Drawing.Size(34, 26)
        Me.LayoutControlItem47.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem47.TextVisible = False
        '
        'LayoutControlItem64
        '
        Me.LayoutControlItem64.Control = Me.bEnvoyerFacture
        Me.LayoutControlItem64.Location = New System.Drawing.Point(641, 26)
        Me.LayoutControlItem64.Name = "LayoutControlItem64"
        Me.LayoutControlItem64.Size = New System.Drawing.Size(34, 26)
        Me.LayoutControlItem64.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem64.TextVisible = False
        '
        'LayoutControlItem72
        '
        Me.LayoutControlItem72.Control = Me.bEnvoyerPaye
        Me.LayoutControlItem72.Location = New System.Drawing.Point(641, 52)
        Me.LayoutControlItem72.Name = "LayoutControlItem72"
        Me.LayoutControlItem72.Size = New System.Drawing.Size(34, 26)
        Me.LayoutControlItem72.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem72.TextVisible = False
        '
        'LayoutControlItem74
        '
        Me.LayoutControlItem74.Control = Me.bEnvoyerEtatCompte
        Me.LayoutControlItem74.Location = New System.Drawing.Point(641, 78)
        Me.LayoutControlItem74.Name = "LayoutControlItem74"
        Me.LayoutControlItem74.Size = New System.Drawing.Size(34, 26)
        Me.LayoutControlItem74.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem74.TextVisible = False
        '
        'LayoutControlItem75
        '
        Me.LayoutControlItem75.Control = Me.bEnvoyerLivraisonsRestaurants
        Me.LayoutControlItem75.Location = New System.Drawing.Point(641, 104)
        Me.LayoutControlItem75.Name = "LayoutControlItem75"
        Me.LayoutControlItem75.Size = New System.Drawing.Size(34, 26)
        Me.LayoutControlItem75.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem75.TextVisible = False
        '
        'LayoutCommissions
        '
        Me.LayoutCommissions.Control = Me.cbCommissions
        Me.LayoutCommissions.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutCommissions.CustomizationFormText = "Liste des factures"
        Me.LayoutCommissions.Location = New System.Drawing.Point(0, 0)
        Me.LayoutCommissions.Name = "LayoutCommissions"
        Me.LayoutCommissions.Size = New System.Drawing.Size(641, 26)
        Me.LayoutCommissions.Text = "Liste des commissions"
        Me.LayoutCommissions.TextSize = New System.Drawing.Size(144, 13)
        '
        'LayoutControlItem37
        '
        Me.LayoutControlItem37.Control = Me.bEnvoyerCommissionVendeur
        Me.LayoutControlItem37.Location = New System.Drawing.Point(641, 0)
        Me.LayoutControlItem37.Name = "LayoutControlItem37"
        Me.LayoutControlItem37.Size = New System.Drawing.Size(34, 26)
        Me.LayoutControlItem37.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem37.TextVisible = False
        '
        'LayoutControlItem46
        '
        Me.LayoutControlItem46.Control = Me.bPrintCommissions
        Me.LayoutControlItem46.Location = New System.Drawing.Point(675, 0)
        Me.LayoutControlItem46.Name = "LayoutControlItem46"
        Me.LayoutControlItem46.Size = New System.Drawing.Size(34, 26)
        Me.LayoutControlItem46.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem46.TextVisible = False
        '
        'LayoutControlItem51
        '
        Me.LayoutControlItem51.Control = Me.bAnnulerCommissions
        Me.LayoutControlItem51.Location = New System.Drawing.Point(709, 0)
        Me.LayoutControlItem51.Name = "LayoutControlItem51"
        Me.LayoutControlItem51.Size = New System.Drawing.Size(30, 26)
        Me.LayoutControlItem51.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem51.TextVisible = False
        '
        'LayoutCommissions1
        '
        Me.LayoutCommissions1.Control = Me.cbRapportTaxation
        Me.LayoutCommissions1.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutCommissions1.CustomizationFormText = "Liste des factures"
        Me.LayoutCommissions1.Location = New System.Drawing.Point(0, 130)
        Me.LayoutCommissions1.Name = "LayoutCommissions1"
        Me.LayoutCommissions1.Size = New System.Drawing.Size(641, 26)
        Me.LayoutCommissions1.Text = "Rrapports de taxation livreur"
        Me.LayoutCommissions1.TextSize = New System.Drawing.Size(144, 13)
        '
        'LayoutControlItem63
        '
        Me.LayoutControlItem63.Control = Me.bEmailTaxationLivreur
        Me.LayoutControlItem63.Location = New System.Drawing.Point(641, 130)
        Me.LayoutControlItem63.Name = "LayoutControlItem63"
        Me.LayoutControlItem63.Size = New System.Drawing.Size(34, 26)
        Me.LayoutControlItem63.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem63.TextVisible = False
        '
        'LayoutControlItem65
        '
        Me.LayoutControlItem65.Control = Me.bPrintTaxationLivreur
        Me.LayoutControlItem65.Location = New System.Drawing.Point(675, 130)
        Me.LayoutControlItem65.Name = "LayoutControlItem65"
        Me.LayoutControlItem65.Size = New System.Drawing.Size(34, 26)
        Me.LayoutControlItem65.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem65.TextVisible = False
        '
        'bResetTaxationLivreurs
        '
        Me.bResetTaxationLivreurs.Control = Me.bResetTaxationLivreur
        Me.bResetTaxationLivreurs.Location = New System.Drawing.Point(709, 130)
        Me.bResetTaxationLivreurs.Name = "bResetTaxationLivreurs"
        Me.bResetTaxationLivreurs.Size = New System.Drawing.Size(30, 26)
        Me.bResetTaxationLivreurs.TextSize = New System.Drawing.Size(0, 0)
        Me.bResetTaxationLivreurs.TextVisible = False
        '
        'LayoutControlItem20
        '
        Me.LayoutControlItem20.Control = Me.cbRapportRevenuPaye
        Me.LayoutControlItem20.Location = New System.Drawing.Point(0, 156)
        Me.LayoutControlItem20.Name = "LayoutControlItem20"
        Me.LayoutControlItem20.Size = New System.Drawing.Size(641, 26)
        Me.LayoutControlItem20.Text = "Rrapports de Revenu Livreurs"
        Me.LayoutControlItem20.TextSize = New System.Drawing.Size(144, 13)
        '
        'LayoutControlItem68
        '
        Me.LayoutControlItem68.Control = Me.bEmailTaxationLivreurPaye
        Me.LayoutControlItem68.Location = New System.Drawing.Point(641, 156)
        Me.LayoutControlItem68.Name = "LayoutControlItem68"
        Me.LayoutControlItem68.Size = New System.Drawing.Size(34, 26)
        Me.LayoutControlItem68.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem68.TextVisible = False
        '
        'LayoutControlItem71
        '
        Me.LayoutControlItem71.Control = Me.bPrintTaxationLivreurPaye
        Me.LayoutControlItem71.Location = New System.Drawing.Point(675, 156)
        Me.LayoutControlItem71.Name = "LayoutControlItem71"
        Me.LayoutControlItem71.Size = New System.Drawing.Size(34, 26)
        Me.LayoutControlItem71.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem71.TextVisible = False
        '
        'LayoutControlItem73
        '
        Me.LayoutControlItem73.Control = Me.bResetTaxationLivreurPaye
        Me.LayoutControlItem73.Location = New System.Drawing.Point(709, 156)
        Me.LayoutControlItem73.Name = "LayoutControlItem73"
        Me.LayoutControlItem73.Size = New System.Drawing.Size(30, 26)
        Me.LayoutControlItem73.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem73.TextVisible = False
        '
        'LayoutControlItem40
        '
        Me.LayoutControlItem40.Control = Me.cbAnnee
        Me.LayoutControlItem40.Location = New System.Drawing.Point(596, 0)
        Me.LayoutControlItem40.Name = "LayoutControlItem40"
        Me.LayoutControlItem40.Size = New System.Drawing.Size(323, 24)
        Me.LayoutControlItem40.Text = "Année"
        Me.LayoutControlItem40.TextSize = New System.Drawing.Size(144, 13)
        '
        'EmptySpaceItem6
        '
        Me.EmptySpaceItem6.AllowHotTrack = False
        Me.EmptySpaceItem6.Location = New System.Drawing.Point(0, 0)
        Me.EmptySpaceItem6.Name = "EmptySpaceItem6"
        Me.EmptySpaceItem6.Size = New System.Drawing.Size(596, 24)
        Me.EmptySpaceItem6.TextSize = New System.Drawing.Size(0, 0)
        '
        'EmptySpaceItem7
        '
        Me.EmptySpaceItem7.AllowHotTrack = False
        Me.EmptySpaceItem7.Location = New System.Drawing.Point(919, 0)
        Me.EmptySpaceItem7.Name = "EmptySpaceItem7"
        Me.EmptySpaceItem7.Size = New System.Drawing.Size(583, 24)
        Me.EmptySpaceItem7.TextSize = New System.Drawing.Size(0, 0)
        '
        'XtraTabPage2
        '
        Me.XtraTabPage2.Controls.Add(Me.LayoutControl4)
        Me.XtraTabPage2.Name = "XtraTabPage2"
        Me.XtraTabPage2.Size = New System.Drawing.Size(1546, 619)
        Me.XtraTabPage2.Text = "Grille résultats"
        '
        'LayoutControl4
        '
        Me.LayoutControl4.Controls.Add(Me.bAffecterResidence)
        Me.LayoutControl4.Controls.Add(Me.bSupprimerLivraisons)
        Me.LayoutControl4.Controls.Add(Me.optFiltreGrille)
        Me.LayoutControl4.Controls.Add(Me.bExportToHTML)
        Me.LayoutControl4.Controls.Add(Me.bExportToExcel)
        Me.LayoutControl4.Controls.Add(Me.bExportToPDF)
        Me.LayoutControl4.Controls.Add(Me.bExportToDoc)
        Me.LayoutControl4.Controls.Add(Me.grdImport)
        Me.LayoutControl4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LayoutControl4.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControl4.Name = "LayoutControl4"
        Me.LayoutControl4.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = New System.Drawing.Rectangle(976, 223, 771, 350)
        Me.LayoutControl4.OptionsCustomizationForm.ShowPropertyGrid = True
        Me.LayoutControl4.Root = Me.LayoutControlGroup7
        Me.LayoutControl4.Size = New System.Drawing.Size(1546, 619)
        Me.LayoutControl4.TabIndex = 0
        Me.LayoutControl4.Text = "LayoutControl1"
        '
        'bAffecterResidence
        '
        Me.bAffecterResidence.Location = New System.Drawing.Point(1133, 38)
        Me.bAffecterResidence.Name = "bAffecterResidence"
        Me.bAffecterResidence.Size = New System.Drawing.Size(198, 22)
        Me.bAffecterResidence.StyleController = Me.LayoutControl4
        Me.bAffecterResidence.TabIndex = 47
        Me.bAffecterResidence.Text = "Affecter une résidence"
        '
        'bSupprimerLivraisons
        '
        Me.bSupprimerLivraisons.Location = New System.Drawing.Point(774, 38)
        Me.bSupprimerLivraisons.Name = "bSupprimerLivraisons"
        Me.bSupprimerLivraisons.Size = New System.Drawing.Size(355, 22)
        Me.bSupprimerLivraisons.StyleController = Me.LayoutControl4
        Me.bSupprimerLivraisons.TabIndex = 46
        Me.bSupprimerLivraisons.Text = "Supprimer Livraisons"
        '
        'optFiltreGrille
        '
        Me.optFiltreGrille.Location = New System.Drawing.Point(40, 38)
        Me.optFiltreGrille.MenuManager = Me.RibbonControl
        Me.optFiltreGrille.Name = "optFiltreGrille"
        Me.optFiltreGrille.Properties.Columns = 4
        Me.optFiltreGrille.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "Voir toutes les livraisons"), New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "Voir mobilus"), New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "Voir restaurant"), New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "Voir Valeurs a fixer")})
        Me.optFiltreGrille.Size = New System.Drawing.Size(730, 25)
        Me.optFiltreGrille.StyleController = Me.LayoutControl4
        Me.optFiltreGrille.TabIndex = 45
        '
        'bExportToHTML
        '
        Me.bExportToHTML.Location = New System.Drawing.Point(1133, 12)
        Me.bExportToHTML.Name = "bExportToHTML"
        Me.bExportToHTML.Size = New System.Drawing.Size(401, 22)
        Me.bExportToHTML.StyleController = Me.LayoutControl4
        Me.bExportToHTML.TabIndex = 44
        Me.bExportToHTML.Text = "Exporter vers HTML"
        '
        'bExportToExcel
        '
        Me.bExportToExcel.Location = New System.Drawing.Point(12, 12)
        Me.bExportToExcel.Name = "bExportToExcel"
        Me.bExportToExcel.Size = New System.Drawing.Size(375, 22)
        Me.bExportToExcel.StyleController = Me.LayoutControl4
        Me.bExportToExcel.TabIndex = 43
        Me.bExportToExcel.Text = "Exporter vers Excel"
        '
        'bExportToPDF
        '
        Me.bExportToPDF.Location = New System.Drawing.Point(391, 12)
        Me.bExportToPDF.Name = "bExportToPDF"
        Me.bExportToPDF.Size = New System.Drawing.Size(379, 22)
        Me.bExportToPDF.StyleController = Me.LayoutControl4
        Me.bExportToPDF.TabIndex = 42
        Me.bExportToPDF.Text = "Exporter vers PDF"
        '
        'bExportToDoc
        '
        Me.bExportToDoc.Location = New System.Drawing.Point(774, 12)
        Me.bExportToDoc.Name = "bExportToDoc"
        Me.bExportToDoc.Size = New System.Drawing.Size(355, 22)
        Me.bExportToDoc.StyleController = Me.LayoutControl4
        Me.bExportToDoc.TabIndex = 41
        Me.bExportToDoc.Text = "Exporter vers Doc"
        '
        'grdImport
        '
        Me.grdImport.Location = New System.Drawing.Point(12, 67)
        Me.grdImport.MainView = Me.GridView1
        Me.grdImport.MenuManager = Me.RibbonControl
        Me.grdImport.Name = "grdImport"
        Me.grdImport.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemCalcEdit1, Me.RepositoryItemTextEdit1, Me.RepositoryItemTextEdit2, Me.RepositoryItemMemoEdit1, Me.RepositoryItemMemoEdit2, Me.RepositoryItemMemoEdit3, Me.RepositoryItemCalcEdit2, Me.RepositoryItemSpinEdit1, Me.RepositoryItemCheckEdit1})
        Me.grdImport.Size = New System.Drawing.Size(1522, 540)
        Me.grdImport.TabIndex = 30
        Me.grdImport.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'GridView1
        '
        Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colImportID, Me.GridColumn2, Me.colReference, Me.colClient, Me.colClientAddress, Me.colMomentDePassage, Me.colPreparePar, Me.colHeureAppel, Me.colHeurePrep, Me.colHeureDepart, Me.colHeureLivraison, Me.colMessagePassage, Me.colTotalTempService, Me.colLivreur, Me.Distance, Me.colMontant, Me.colOrganisation, Me.colDebit, Me.colCredit, Me.colGlacier, Me.colMontantGlacierLivreur, Me.colMontantGlacierOrganisation, Me.colMontantLivreur, Me.Extras, Me.colExtraKM, Me.GridColumn3, Me.GridIDPeriode, Me.IsMobilus, Me.IDClientAddresse, Me.colIDResidence, Me.colResidence})
        GridFormatRule1.ApplyToRow = True
        GridFormatRule1.Column = Me.Distance
        GridFormatRule1.ColumnApplyTo = Me.Distance
        GridFormatRule1.Name = "Format0"
        FormatConditionRuleValue1.Condition = DevExpress.XtraEditors.FormatCondition.Equal
        FormatConditionRuleValue1.PredefinedName = "Red Fill"
        FormatConditionRuleValue1.Value1 = 0
        GridFormatRule1.Rule = FormatConditionRuleValue1
        GridFormatRule2.ApplyToRow = True
        GridFormatRule2.Column = Me.Distance
        GridFormatRule2.ColumnApplyTo = Me.Distance
        GridFormatRule2.Name = "Format1"
        FormatConditionRuleValue2.Condition = DevExpress.XtraEditors.FormatCondition.Greater
        FormatConditionRuleValue2.PredefinedName = "Green Fill"
        FormatConditionRuleValue2.Value1 = 400
        GridFormatRule2.Rule = FormatConditionRuleValue2
        GridFormatRule3.ApplyToRow = True
        GridFormatRule3.Column = Me.IsMobilus
        GridFormatRule3.ColumnApplyTo = Me.IsMobilus
        GridFormatRule3.Name = "Format2"
        FormatConditionRuleValue3.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        FormatConditionRuleValue3.Appearance.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        FormatConditionRuleValue3.Appearance.Options.UseBackColor = True
        FormatConditionRuleValue3.Condition = DevExpress.XtraEditors.FormatCondition.Equal
        FormatConditionRuleValue3.Value1 = False
        GridFormatRule3.Rule = FormatConditionRuleValue3
        Me.GridView1.FormatRules.Add(GridFormatRule1)
        Me.GridView1.FormatRules.Add(GridFormatRule2)
        Me.GridView1.FormatRules.Add(GridFormatRule3)
        Me.GridView1.GridControl = Me.grdImport
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsBehavior.Editable = False
        Me.GridView1.OptionsSelection.MultiSelect = True
        Me.GridView1.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect
        Me.GridView1.OptionsView.RowAutoHeight = True
        Me.GridView1.OptionsView.ShowAutoFilterRow = True
        Me.GridView1.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.ShowAlways
        Me.GridView1.OptionsView.ShowFooter = True
        '
        'colImportID
        '
        Me.colImportID.AppearanceCell.Options.UseTextOptions = True
        Me.colImportID.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center
        Me.colImportID.Caption = "Import ID"
        Me.colImportID.FieldName = "ImportID"
        Me.colImportID.Name = "colImportID"
        Me.colImportID.OptionsColumn.AllowEdit = False
        Me.colImportID.OptionsColumn.AllowFocus = False
        Me.colImportID.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count, "ImportID", "{0}")})
        Me.colImportID.Visible = True
        Me.colImportID.VisibleIndex = 1
        Me.colImportID.Width = 36
        '
        'GridColumn2
        '
        Me.GridColumn2.Caption = "Code"
        Me.GridColumn2.FieldName = "Code"
        Me.GridColumn2.Name = "GridColumn2"
        Me.GridColumn2.OptionsColumn.AllowEdit = False
        Me.GridColumn2.OptionsColumn.AllowFocus = False
        Me.GridColumn2.Visible = True
        Me.GridColumn2.VisibleIndex = 2
        Me.GridColumn2.Width = 92
        '
        'colReference
        '
        Me.colReference.Caption = "Reference"
        Me.colReference.FieldName = "Reference"
        Me.colReference.Name = "colReference"
        Me.colReference.OptionsColumn.AllowFocus = False
        '
        'colClient
        '
        Me.colClient.AppearanceCell.Options.UseTextOptions = True
        Me.colClient.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center
        Me.colClient.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.colClient.Caption = "Client"
        Me.colClient.ColumnEdit = Me.RepositoryItemMemoEdit1
        Me.colClient.CustomizationCaption = "Client"
        Me.colClient.FieldName = "Client"
        Me.colClient.Name = "colClient"
        Me.colClient.OptionsColumn.AllowEdit = False
        Me.colClient.OptionsColumn.AllowFocus = False
        Me.colClient.Visible = True
        Me.colClient.VisibleIndex = 3
        Me.colClient.Width = 92
        '
        'RepositoryItemMemoEdit1
        '
        Me.RepositoryItemMemoEdit1.Name = "RepositoryItemMemoEdit1"
        '
        'colClientAddress
        '
        Me.colClientAddress.AppearanceCell.Options.UseTextOptions = True
        Me.colClientAddress.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center
        Me.colClientAddress.Caption = "ClientAddress"
        Me.colClientAddress.CustomizationCaption = "ClientAddress"
        Me.colClientAddress.FieldName = "ClientAddress"
        Me.colClientAddress.Name = "colClientAddress"
        Me.colClientAddress.OptionsColumn.AllowEdit = False
        Me.colClientAddress.OptionsColumn.AllowFocus = False
        '
        'colMomentDePassage
        '
        Me.colMomentDePassage.AppearanceCell.Options.UseTextOptions = True
        Me.colMomentDePassage.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center
        Me.colMomentDePassage.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.colMomentDePassage.Caption = "MomentDePassage"
        Me.colMomentDePassage.FieldName = "MomentDePassage"
        Me.colMomentDePassage.Name = "colMomentDePassage"
        Me.colMomentDePassage.OptionsColumn.AllowEdit = False
        Me.colMomentDePassage.OptionsColumn.AllowFocus = False
        '
        'colPreparePar
        '
        Me.colPreparePar.AppearanceCell.Options.UseTextOptions = True
        Me.colPreparePar.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center
        Me.colPreparePar.AppearanceHeader.Options.UseTextOptions = True
        Me.colPreparePar.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.colPreparePar.Caption = "Préparé par"
        Me.colPreparePar.CustomizationCaption = "Prepare Par"
        Me.colPreparePar.FieldName = "PreparePar"
        Me.colPreparePar.Name = "colPreparePar"
        Me.colPreparePar.OptionsColumn.AllowEdit = False
        Me.colPreparePar.OptionsColumn.AllowFocus = False
        '
        'colHeureAppel
        '
        Me.colHeureAppel.AppearanceCell.Options.UseTextOptions = True
        Me.colHeureAppel.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center
        Me.colHeureAppel.AppearanceHeader.Options.UseTextOptions = True
        Me.colHeureAppel.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.colHeureAppel.Caption = "HeureAppel"
        Me.colHeureAppel.DisplayFormat.FormatString = "g"
        Me.colHeureAppel.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.colHeureAppel.FieldName = "HeureAppel"
        Me.colHeureAppel.Name = "colHeureAppel"
        Me.colHeureAppel.OptionsColumn.AllowEdit = False
        Me.colHeureAppel.OptionsColumn.AllowFocus = False
        '
        'colHeurePrep
        '
        Me.colHeurePrep.AppearanceCell.Options.UseTextOptions = True
        Me.colHeurePrep.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center
        Me.colHeurePrep.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.colHeurePrep.AppearanceHeader.Options.UseTextOptions = True
        Me.colHeurePrep.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.colHeurePrep.Caption = "Préparation"
        Me.colHeurePrep.DisplayFormat.FormatString = "g"
        Me.colHeurePrep.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.colHeurePrep.FieldName = "HeurePrep"
        Me.colHeurePrep.Name = "colHeurePrep"
        Me.colHeurePrep.OptionsColumn.AllowEdit = False
        Me.colHeurePrep.OptionsColumn.AllowFocus = False
        Me.colHeurePrep.Visible = True
        Me.colHeurePrep.VisibleIndex = 4
        Me.colHeurePrep.Width = 92
        '
        'colHeureDepart
        '
        Me.colHeureDepart.AppearanceCell.Options.UseTextOptions = True
        Me.colHeureDepart.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center
        Me.colHeureDepart.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.colHeureDepart.AppearanceHeader.Options.UseTextOptions = True
        Me.colHeureDepart.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.colHeureDepart.Caption = "Départ"
        Me.colHeureDepart.DisplayFormat.FormatString = "g"
        Me.colHeureDepart.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.colHeureDepart.FieldName = "HeureDepart"
        Me.colHeureDepart.Name = "colHeureDepart"
        Me.colHeureDepart.OptionsColumn.AllowEdit = False
        Me.colHeureDepart.OptionsColumn.AllowFocus = False
        Me.colHeureDepart.Visible = True
        Me.colHeureDepart.VisibleIndex = 5
        Me.colHeureDepart.Width = 92
        '
        'colHeureLivraison
        '
        Me.colHeureLivraison.AppearanceCell.Options.UseTextOptions = True
        Me.colHeureLivraison.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center
        Me.colHeureLivraison.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.colHeureLivraison.AppearanceHeader.Options.UseTextOptions = True
        Me.colHeureLivraison.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.colHeureLivraison.Caption = "Livraison"
        Me.colHeureLivraison.DisplayFormat.FormatString = "g"
        Me.colHeureLivraison.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.colHeureLivraison.FieldName = "HeureLivraison"
        Me.colHeureLivraison.Name = "colHeureLivraison"
        Me.colHeureLivraison.OptionsColumn.AllowEdit = False
        Me.colHeureLivraison.OptionsColumn.AllowFocus = False
        Me.colHeureLivraison.Visible = True
        Me.colHeureLivraison.VisibleIndex = 6
        Me.colHeureLivraison.Width = 92
        '
        'colMessagePassage
        '
        Me.colMessagePassage.AppearanceCell.Options.UseTextOptions = True
        Me.colMessagePassage.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center
        Me.colMessagePassage.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.colMessagePassage.AppearanceHeader.Options.UseTextOptions = True
        Me.colMessagePassage.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.colMessagePassage.Caption = "Message"
        Me.colMessagePassage.ColumnEdit = Me.RepositoryItemMemoEdit3
        Me.colMessagePassage.FieldName = "MessagePassage"
        Me.colMessagePassage.Name = "colMessagePassage"
        Me.colMessagePassage.OptionsColumn.AllowEdit = False
        Me.colMessagePassage.OptionsColumn.AllowFocus = False
        Me.colMessagePassage.Visible = True
        Me.colMessagePassage.VisibleIndex = 7
        Me.colMessagePassage.Width = 92
        '
        'RepositoryItemMemoEdit3
        '
        Me.RepositoryItemMemoEdit3.Name = "RepositoryItemMemoEdit3"
        '
        'colTotalTempService
        '
        Me.colTotalTempService.AppearanceCell.Options.UseTextOptions = True
        Me.colTotalTempService.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center
        Me.colTotalTempService.Caption = "TotalTempService"
        Me.colTotalTempService.FieldName = "TotalTempService"
        Me.colTotalTempService.Name = "colTotalTempService"
        Me.colTotalTempService.OptionsColumn.AllowEdit = False
        Me.colTotalTempService.OptionsColumn.AllowFocus = False
        '
        'colLivreur
        '
        Me.colLivreur.AppearanceCell.Options.UseTextOptions = True
        Me.colLivreur.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center
        Me.colLivreur.Caption = "Livreur"
        Me.colLivreur.FieldName = "Livreur"
        Me.colLivreur.Name = "colLivreur"
        Me.colLivreur.OptionsColumn.AllowEdit = False
        Me.colLivreur.OptionsColumn.AllowFocus = False
        Me.colLivreur.Visible = True
        Me.colLivreur.VisibleIndex = 8
        Me.colLivreur.Width = 92
        '
        'colMontant
        '
        Me.colMontant.AppearanceCell.Options.UseTextOptions = True
        Me.colMontant.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center
        Me.colMontant.Caption = "Montant O."
        Me.colMontant.DisplayFormat.FormatString = "c"
        Me.colMontant.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.colMontant.FieldName = "Montant"
        Me.colMontant.Name = "colMontant"
        Me.colMontant.OptionsColumn.AllowEdit = False
        Me.colMontant.OptionsColumn.AllowFocus = False
        Me.colMontant.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Montant", "SUM={0:c2}")})
        Me.colMontant.Visible = True
        Me.colMontant.VisibleIndex = 10
        Me.colMontant.Width = 92
        '
        'colOrganisation
        '
        Me.colOrganisation.AppearanceCell.Options.UseTextOptions = True
        Me.colOrganisation.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center
        Me.colOrganisation.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.colOrganisation.Caption = "Organisation"
        Me.colOrganisation.ColumnEdit = Me.RepositoryItemMemoEdit2
        Me.colOrganisation.FieldName = "Organisation"
        Me.colOrganisation.Name = "colOrganisation"
        Me.colOrganisation.OptionsColumn.AllowEdit = False
        Me.colOrganisation.OptionsColumn.AllowFocus = False
        Me.colOrganisation.Visible = True
        Me.colOrganisation.VisibleIndex = 9
        Me.colOrganisation.Width = 92
        '
        'RepositoryItemMemoEdit2
        '
        Me.RepositoryItemMemoEdit2.Name = "RepositoryItemMemoEdit2"
        '
        'colDebit
        '
        Me.colDebit.AppearanceCell.Options.UseTextOptions = True
        Me.colDebit.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center
        Me.colDebit.Caption = "Débit"
        Me.colDebit.DisplayFormat.FormatString = "c"
        Me.colDebit.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.colDebit.FieldName = "Debit"
        Me.colDebit.Name = "colDebit"
        Me.colDebit.OptionsColumn.AllowEdit = False
        Me.colDebit.OptionsColumn.AllowFocus = False
        Me.colDebit.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Extra", "SUM={0:c2}")})
        Me.colDebit.Visible = True
        Me.colDebit.VisibleIndex = 18
        Me.colDebit.Width = 92
        '
        'colCredit
        '
        Me.colCredit.AppearanceCell.Options.UseTextOptions = True
        Me.colCredit.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center
        Me.colCredit.Caption = "Crédit"
        Me.colCredit.DisplayFormat.FormatString = "c"
        Me.colCredit.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.colCredit.FieldName = "Credit"
        Me.colCredit.Name = "colCredit"
        Me.colCredit.OptionsColumn.AllowEdit = False
        Me.colCredit.OptionsColumn.AllowFocus = False
        Me.colCredit.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Credit", "SUM={0:c2}")})
        Me.colCredit.Visible = True
        Me.colCredit.VisibleIndex = 19
        Me.colCredit.Width = 92
        '
        'colGlacier
        '
        Me.colGlacier.Caption = "Nombre Glacier"
        Me.colGlacier.FieldName = "NombreGlacier"
        Me.colGlacier.Name = "colGlacier"
        Me.colGlacier.OptionsColumn.AllowEdit = False
        Me.colGlacier.OptionsColumn.AllowFocus = False
        Me.colGlacier.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "NombreGlacier", "SUM={0:0.##}")})
        Me.colGlacier.Visible = True
        Me.colGlacier.VisibleIndex = 12
        Me.colGlacier.Width = 92
        '
        'colMontantGlacierLivreur
        '
        Me.colMontantGlacierLivreur.Caption = "Glacier L."
        Me.colMontantGlacierLivreur.DisplayFormat.FormatString = "c"
        Me.colMontantGlacierLivreur.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.colMontantGlacierLivreur.FieldName = "MontantGlacierLivreur"
        Me.colMontantGlacierLivreur.Name = "colMontantGlacierLivreur"
        Me.colMontantGlacierLivreur.OptionsColumn.AllowEdit = False
        Me.colMontantGlacierLivreur.OptionsColumn.AllowFocus = False
        Me.colMontantGlacierLivreur.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "MontantGlacierLivreur", "SUM={0:0.##}")})
        Me.colMontantGlacierLivreur.Visible = True
        Me.colMontantGlacierLivreur.VisibleIndex = 13
        Me.colMontantGlacierLivreur.Width = 92
        '
        'colMontantGlacierOrganisation
        '
        Me.colMontantGlacierOrganisation.Caption = "Glacier O."
        Me.colMontantGlacierOrganisation.DisplayFormat.FormatString = "c"
        Me.colMontantGlacierOrganisation.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.colMontantGlacierOrganisation.FieldName = "MontantGlacierOrganisation"
        Me.colMontantGlacierOrganisation.Name = "colMontantGlacierOrganisation"
        Me.colMontantGlacierOrganisation.OptionsColumn.AllowEdit = False
        Me.colMontantGlacierOrganisation.OptionsColumn.AllowFocus = False
        Me.colMontantGlacierOrganisation.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "MontantGlacierOrganisation", "SUM={0:0.##}")})
        Me.colMontantGlacierOrganisation.Visible = True
        Me.colMontantGlacierOrganisation.VisibleIndex = 14
        Me.colMontantGlacierOrganisation.Width = 92
        '
        'colMontantLivreur
        '
        Me.colMontantLivreur.Caption = "Montant L."
        Me.colMontantLivreur.DisplayFormat.FormatString = "c"
        Me.colMontantLivreur.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.colMontantLivreur.FieldName = "MontantLivreur"
        Me.colMontantLivreur.Name = "colMontantLivreur"
        Me.colMontantLivreur.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "MontantLivreur", "SUM={0:0.##}")})
        Me.colMontantLivreur.Visible = True
        Me.colMontantLivreur.VisibleIndex = 11
        Me.colMontantLivreur.Width = 104
        '
        'Extras
        '
        Me.Extras.Caption = "Extra"
        Me.Extras.DisplayFormat.FormatString = "c"
        Me.Extras.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.Extras.FieldName = "Extra"
        Me.Extras.Name = "Extras"
        Me.Extras.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Extra", "SUM={0:0.##}")})
        Me.Extras.Visible = True
        Me.Extras.VisibleIndex = 16
        '
        'colExtraKM
        '
        Me.colExtraKM.Caption = "Extra KM"
        Me.colExtraKM.FieldName = "ExtraKM"
        Me.colExtraKM.Name = "colExtraKM"
        Me.colExtraKM.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "ExtraKM", "SUM={0:0.##}")})
        Me.colExtraKM.Visible = True
        Me.colExtraKM.VisibleIndex = 17
        '
        'GridColumn3
        '
        Me.GridColumn3.Caption = "Date Import"
        Me.GridColumn3.FieldName = "DateImport"
        Me.GridColumn3.Name = "GridColumn3"
        Me.GridColumn3.Visible = True
        Me.GridColumn3.VisibleIndex = 20
        '
        'GridIDPeriode
        '
        Me.GridIDPeriode.Caption = "ID Periode"
        Me.GridIDPeriode.FieldName = "IDPeriode"
        Me.GridIDPeriode.Name = "GridIDPeriode"
        '
        'IDClientAddresse
        '
        Me.IDClientAddresse.Caption = "IDClientAddresse"
        Me.IDClientAddresse.FieldName = "IDClientAddresse"
        Me.IDClientAddresse.Name = "IDClientAddresse"
        '
        'colIDResidence
        '
        Me.colIDResidence.Caption = "ID Residence"
        Me.colIDResidence.FieldName = "IDResidence"
        Me.colIDResidence.Name = "colIDResidence"
        '
        'colResidence
        '
        Me.colResidence.Caption = "Residence"
        Me.colResidence.FieldName = "Residence"
        Me.colResidence.Name = "colResidence"
        Me.colResidence.Visible = True
        Me.colResidence.VisibleIndex = 21
        '
        'RepositoryItemCalcEdit1
        '
        Me.RepositoryItemCalcEdit1.AutoHeight = False
        Me.RepositoryItemCalcEdit1.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemCalcEdit1.Name = "RepositoryItemCalcEdit1"
        '
        'RepositoryItemTextEdit1
        '
        Me.RepositoryItemTextEdit1.AutoHeight = False
        Me.RepositoryItemTextEdit1.Name = "RepositoryItemTextEdit1"
        '
        'RepositoryItemTextEdit2
        '
        Me.RepositoryItemTextEdit2.AutoHeight = False
        Me.RepositoryItemTextEdit2.Name = "RepositoryItemTextEdit2"
        '
        'RepositoryItemCalcEdit2
        '
        Me.RepositoryItemCalcEdit2.AutoHeight = False
        Me.RepositoryItemCalcEdit2.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemCalcEdit2.Name = "RepositoryItemCalcEdit2"
        '
        'RepositoryItemSpinEdit1
        '
        Me.RepositoryItemSpinEdit1.AutoHeight = False
        Me.RepositoryItemSpinEdit1.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemSpinEdit1.Name = "RepositoryItemSpinEdit1"
        '
        'RepositoryItemCheckEdit1
        '
        Me.RepositoryItemCheckEdit1.AutoHeight = False
        Me.RepositoryItemCheckEdit1.Name = "RepositoryItemCheckEdit1"
        '
        'LayoutControlGroup7
        '
        Me.LayoutControlGroup7.CustomizationFormText = "LayoutControlGroup1"
        Me.LayoutControlGroup7.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup7.GroupBordersVisible = False
        Me.LayoutControlGroup7.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem4, Me.LayoutControlItem7, Me.LayoutControlItem11, Me.LayoutControlItem12, Me.LayoutControlItem18, Me.LayoutControlItem29, Me.EmptySpaceItem1, Me.LayoutControlItem30, Me.LayoutControlItem39})
        Me.LayoutControlGroup7.Name = "Root"
        Me.LayoutControlGroup7.Size = New System.Drawing.Size(1546, 619)
        Me.LayoutControlGroup7.TextVisible = False
        '
        'LayoutControlItem4
        '
        Me.LayoutControlItem4.Control = Me.grdImport
        Me.LayoutControlItem4.Location = New System.Drawing.Point(0, 55)
        Me.LayoutControlItem4.Name = "LayoutControlItem4"
        Me.LayoutControlItem4.Size = New System.Drawing.Size(1526, 544)
        Me.LayoutControlItem4.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem4.TextVisible = False
        '
        'LayoutControlItem7
        '
        Me.LayoutControlItem7.Control = Me.bExportToHTML
        Me.LayoutControlItem7.Location = New System.Drawing.Point(1121, 0)
        Me.LayoutControlItem7.Name = "LayoutControlItem7"
        Me.LayoutControlItem7.Size = New System.Drawing.Size(405, 26)
        Me.LayoutControlItem7.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem7.TextVisible = False
        '
        'LayoutControlItem11
        '
        Me.LayoutControlItem11.Control = Me.bExportToExcel
        Me.LayoutControlItem11.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem11.Name = "LayoutControlItem11"
        Me.LayoutControlItem11.Size = New System.Drawing.Size(379, 26)
        Me.LayoutControlItem11.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem11.TextVisible = False
        '
        'LayoutControlItem12
        '
        Me.LayoutControlItem12.Control = Me.bExportToPDF
        Me.LayoutControlItem12.Location = New System.Drawing.Point(379, 0)
        Me.LayoutControlItem12.Name = "LayoutControlItem12"
        Me.LayoutControlItem12.Size = New System.Drawing.Size(383, 26)
        Me.LayoutControlItem12.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem12.TextVisible = False
        '
        'LayoutControlItem18
        '
        Me.LayoutControlItem18.Control = Me.bExportToDoc
        Me.LayoutControlItem18.Location = New System.Drawing.Point(762, 0)
        Me.LayoutControlItem18.Name = "LayoutControlItem18"
        Me.LayoutControlItem18.Size = New System.Drawing.Size(359, 26)
        Me.LayoutControlItem18.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem18.TextVisible = False
        '
        'LayoutControlItem29
        '
        Me.LayoutControlItem29.Control = Me.optFiltreGrille
        Me.LayoutControlItem29.Location = New System.Drawing.Point(0, 26)
        Me.LayoutControlItem29.Name = "LayoutControlItem29"
        Me.LayoutControlItem29.Size = New System.Drawing.Size(762, 29)
        Me.LayoutControlItem29.Text = "Filtre"
        Me.LayoutControlItem29.TextSize = New System.Drawing.Size(24, 13)
        '
        'EmptySpaceItem1
        '
        Me.EmptySpaceItem1.AllowHotTrack = False
        Me.EmptySpaceItem1.Location = New System.Drawing.Point(1323, 26)
        Me.EmptySpaceItem1.Name = "EmptySpaceItem1"
        Me.EmptySpaceItem1.Size = New System.Drawing.Size(203, 29)
        Me.EmptySpaceItem1.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlItem30
        '
        Me.LayoutControlItem30.Control = Me.bSupprimerLivraisons
        Me.LayoutControlItem30.Location = New System.Drawing.Point(762, 26)
        Me.LayoutControlItem30.Name = "LayoutControlItem30"
        Me.LayoutControlItem30.Size = New System.Drawing.Size(359, 29)
        Me.LayoutControlItem30.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem30.TextVisible = False
        '
        'LayoutControlItem39
        '
        Me.LayoutControlItem39.Control = Me.bAffecterResidence
        Me.LayoutControlItem39.Location = New System.Drawing.Point(1121, 26)
        Me.LayoutControlItem39.Name = "LayoutControlItem39"
        Me.LayoutControlItem39.Size = New System.Drawing.Size(202, 29)
        Me.LayoutControlItem39.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem39.TextVisible = False
        '
        'XtraTabPage3
        '
        Me.XtraTabPage3.Controls.Add(Me.LayoutControl5)
        Me.XtraTabPage3.Name = "XtraTabPage3"
        Me.XtraTabPage3.PageVisible = False
        Me.XtraTabPage3.Size = New System.Drawing.Size(1546, 619)
        Me.XtraTabPage3.Text = "Ajouter une livraison"
        '
        'LayoutControl5
        '
        Me.LayoutControl5.Controls.Add(Me.grdClientsAddresse)
        Me.LayoutControl5.Controls.Add(Me.bRemoveFilter)
        Me.LayoutControl5.Controls.Add(Me.bImportMiseaJour)
        Me.LayoutControl5.Controls.Add(Me.bSupprimerAddresse)
        Me.LayoutControl5.Controls.Add(Me.bReturnToAction)
        Me.LayoutControl5.Controls.Add(Me.bAddToImport)
        Me.LayoutControl5.Controls.Add(Me.tbCustomerAddressID)
        Me.LayoutControl5.Controls.Add(Me.tbAdresseNo)
        Me.LayoutControl5.Controls.Add(Me.tbAdresse)
        Me.LayoutControl5.Controls.Add(Me.tbCodePostale)
        Me.LayoutControl5.Controls.Add(Me.tbAppartement)
        Me.LayoutControl5.Controls.Add(Me.tbCodeEntree)
        Me.LayoutControl5.Controls.Add(Me.tbTelephone)
        Me.LayoutControl5.Controls.Add(Me.tbNom)
        Me.LayoutControl5.Controls.Add(Me.cblivreur)
        Me.LayoutControl5.Controls.Add(Me.tbImportIDL)
        Me.LayoutControl5.Controls.Add(Me.tbMobile)
        Me.LayoutControl5.Controls.Add(Me.tbMontantOrganisation)
        Me.LayoutControl5.Controls.Add(Me.tbMontantLivreur)
        Me.LayoutControl5.Controls.Add(Me.tbDateLivraison)
        Me.LayoutControl5.Controls.Add(Me.tbDistanceR)
        Me.LayoutControl5.Controls.Add(Me.cbRestaurant)
        Me.LayoutControl5.Controls.Add(Me.cbVille)
        Me.LayoutControl5.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LayoutControl5.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControl5.Name = "LayoutControl5"
        Me.LayoutControl5.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = New System.Drawing.Rectangle(975, 554, 650, 400)
        Me.LayoutControl5.Root = Me.LayoutControlGroup9
        Me.LayoutControl5.Size = New System.Drawing.Size(1546, 619)
        Me.LayoutControl5.TabIndex = 0
        Me.LayoutControl5.Text = "LayoutControl5"
        '
        'grdClientsAddresse
        '
        Me.grdClientsAddresse.Location = New System.Drawing.Point(726, 12)
        Me.grdClientsAddresse.MainView = Me.GridView3
        Me.grdClientsAddresse.MenuManager = Me.RibbonControl
        Me.grdClientsAddresse.Name = "grdClientsAddresse"
        Me.grdClientsAddresse.Size = New System.Drawing.Size(808, 595)
        Me.grdClientsAddresse.TabIndex = 143
        Me.grdClientsAddresse.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView3})
        '
        'GridView3
        '
        Me.GridView3.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn4, Me.GridColumn5, Me.GridColumn6, Me.GridColumn7, Me.GridColumn8, Me.GridColumn9, Me.GridColumn10})
        Me.GridView3.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFullFocus
        Me.GridView3.GridControl = Me.grdClientsAddresse
        Me.GridView3.Name = "GridView3"
        Me.GridView3.OptionsBehavior.Editable = False
        Me.GridView3.OptionsCustomization.AllowGroup = False
        Me.GridView3.OptionsView.ShowGroupPanel = False
        '
        'GridColumn4
        '
        Me.GridColumn4.Caption = "IDClientAddress"
        Me.GridColumn4.FieldName = "IDClientAddress"
        Me.GridColumn4.Name = "GridColumn4"
        Me.GridColumn4.Visible = True
        Me.GridColumn4.VisibleIndex = 0
        '
        'GridColumn5
        '
        Me.GridColumn5.Caption = "AdresseNo"
        Me.GridColumn5.FieldName = "AdresseNo"
        Me.GridColumn5.Name = "GridColumn5"
        Me.GridColumn5.Visible = True
        Me.GridColumn5.VisibleIndex = 1
        '
        'GridColumn6
        '
        Me.GridColumn6.Caption = "Adresse"
        Me.GridColumn6.FieldName = "Adresse"
        Me.GridColumn6.FieldNameSortGroup = "Adresse"
        Me.GridColumn6.Name = "GridColumn6"
        Me.GridColumn6.Visible = True
        Me.GridColumn6.VisibleIndex = 2
        '
        'GridColumn7
        '
        Me.GridColumn7.Caption = "Appartement"
        Me.GridColumn7.FieldName = "Appartement"
        Me.GridColumn7.Name = "GridColumn7"
        Me.GridColumn7.Visible = True
        Me.GridColumn7.VisibleIndex = 3
        '
        'GridColumn8
        '
        Me.GridColumn8.Caption = "CodeEntree"
        Me.GridColumn8.FieldName = "CodeEntree"
        Me.GridColumn8.Name = "GridColumn8"
        Me.GridColumn8.Visible = True
        Me.GridColumn8.VisibleIndex = 4
        '
        'GridColumn9
        '
        Me.GridColumn9.Caption = "Telephone"
        Me.GridColumn9.FieldName = "Telephone"
        Me.GridColumn9.Name = "GridColumn9"
        Me.GridColumn9.Visible = True
        Me.GridColumn9.VisibleIndex = 5
        '
        'GridColumn10
        '
        Me.GridColumn10.Caption = "Mobile"
        Me.GridColumn10.FieldName = "Mobile"
        Me.GridColumn10.Name = "GridColumn10"
        Me.GridColumn10.Visible = True
        Me.GridColumn10.VisibleIndex = 6
        '
        'bRemoveFilter
        '
        Me.bRemoveFilter.Location = New System.Drawing.Point(352, 210)
        Me.bRemoveFilter.Name = "bRemoveFilter"
        Me.bRemoveFilter.Size = New System.Drawing.Size(346, 22)
        Me.bRemoveFilter.StyleController = Me.LayoutControl5
        Me.bRemoveFilter.TabIndex = 142
        Me.bRemoveFilter.Text = "Enlever filtre"
        '
        'bImportMiseaJour
        '
        Me.bImportMiseaJour.Enabled = False
        Me.bImportMiseaJour.Location = New System.Drawing.Point(225, 12)
        Me.bImportMiseaJour.Name = "bImportMiseaJour"
        Me.bImportMiseaJour.Size = New System.Drawing.Size(225, 22)
        Me.bImportMiseaJour.StyleController = Me.LayoutControl5
        Me.bImportMiseaJour.TabIndex = 140
        Me.bImportMiseaJour.Text = "Mise a jour"
        '
        'bSupprimerAddresse
        '
        Me.bSupprimerAddresse.Enabled = False
        Me.bSupprimerAddresse.Location = New System.Drawing.Point(36, 210)
        Me.bSupprimerAddresse.Name = "bSupprimerAddresse"
        Me.bSupprimerAddresse.Size = New System.Drawing.Size(312, 22)
        Me.bSupprimerAddresse.StyleController = Me.LayoutControl5
        Me.bSupprimerAddresse.TabIndex = 134
        Me.bSupprimerAddresse.Text = "Supprimer"
        '
        'bReturnToAction
        '
        Me.bReturnToAction.Location = New System.Drawing.Point(454, 12)
        Me.bReturnToAction.Name = "bReturnToAction"
        Me.bReturnToAction.Size = New System.Drawing.Size(268, 22)
        Me.bReturnToAction.StyleController = Me.LayoutControl5
        Me.bReturnToAction.TabIndex = 131
        Me.bReturnToAction.Text = "Retourner a la grille"
        '
        'bAddToImport
        '
        Me.bAddToImport.Location = New System.Drawing.Point(12, 12)
        Me.bAddToImport.Name = "bAddToImport"
        Me.bAddToImport.Size = New System.Drawing.Size(209, 22)
        Me.bAddToImport.StyleController = Me.LayoutControl5
        Me.bAddToImport.TabIndex = 128
        Me.bAddToImport.Text = "Sauvegarder"
        '
        'tbCustomerAddressID
        '
        Me.tbCustomerAddressID.EditValue = "0"
        Me.tbCustomerAddressID.Enabled = False
        Me.tbCustomerAddressID.Location = New System.Drawing.Point(162, 236)
        Me.tbCustomerAddressID.Name = "tbCustomerAddressID"
        Me.tbCustomerAddressID.Properties.Appearance.BackColor = System.Drawing.Color.Azure
        Me.tbCustomerAddressID.Properties.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!)
        Me.tbCustomerAddressID.Properties.Appearance.Options.UseBackColor = True
        Me.tbCustomerAddressID.Properties.Appearance.Options.UseFont = True
        Me.tbCustomerAddressID.Properties.MaskSettings.Set("MaskManagerType", GetType(DevExpress.Data.Mask.NumericMaskManager))
        Me.tbCustomerAddressID.Properties.MaskSettings.Set("mask", "n0")
        Me.tbCustomerAddressID.Size = New System.Drawing.Size(536, 24)
        Me.tbCustomerAddressID.StyleController = Me.LayoutControl5
        Me.tbCustomerAddressID.TabIndex = 126
        '
        'tbAdresseNo
        '
        Me.tbAdresseNo.Location = New System.Drawing.Point(162, 292)
        Me.tbAdresseNo.Name = "tbAdresseNo"
        Me.tbAdresseNo.Properties.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!)
        Me.tbAdresseNo.Properties.Appearance.Options.UseFont = True
        Me.tbAdresseNo.Size = New System.Drawing.Size(204, 24)
        Me.tbAdresseNo.StyleController = Me.LayoutControl5
        Me.tbAdresseNo.TabIndex = 7
        '
        'tbAdresse
        '
        Me.tbAdresse.Location = New System.Drawing.Point(496, 292)
        Me.tbAdresse.Margin = New System.Windows.Forms.Padding(2)
        Me.tbAdresse.Name = "tbAdresse"
        Me.tbAdresse.Properties.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!)
        Me.tbAdresse.Properties.Appearance.Options.UseFont = True
        Me.tbAdresse.Size = New System.Drawing.Size(202, 24)
        Me.tbAdresse.StyleController = Me.LayoutControl5
        Me.tbAdresse.TabIndex = 68
        '
        'tbCodePostale
        '
        Me.tbCodePostale.Location = New System.Drawing.Point(496, 320)
        Me.tbCodePostale.Name = "tbCodePostale"
        Me.tbCodePostale.Properties.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!)
        Me.tbCodePostale.Properties.Appearance.Options.UseFont = True
        Me.tbCodePostale.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.tbCodePostale.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.tbCodePostale.Properties.MaskSettings.Set("MaskManagerType", GetType(DevExpress.Data.Mask.RegExpMaskManager))
        Me.tbCodePostale.Properties.MaskSettings.Set("mask", "([a-zA-Z]\d[a-zA-z]-( )?\d[a-zA-Z]\d)")
        Me.tbCodePostale.Size = New System.Drawing.Size(202, 24)
        Me.tbCodePostale.StyleController = Me.LayoutControl5
        Me.tbCodePostale.TabIndex = 4
        '
        'tbAppartement
        '
        Me.tbAppartement.Location = New System.Drawing.Point(162, 348)
        Me.tbAppartement.Name = "tbAppartement"
        Me.tbAppartement.Properties.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!)
        Me.tbAppartement.Properties.Appearance.Options.UseFont = True
        Me.tbAppartement.Size = New System.Drawing.Size(204, 24)
        Me.tbAppartement.StyleController = Me.LayoutControl5
        Me.tbAppartement.TabIndex = 12
        '
        'tbCodeEntree
        '
        Me.tbCodeEntree.Location = New System.Drawing.Point(496, 348)
        Me.tbCodeEntree.Name = "tbCodeEntree"
        Me.tbCodeEntree.Properties.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!)
        Me.tbCodeEntree.Properties.Appearance.Options.UseFont = True
        Me.tbCodeEntree.Size = New System.Drawing.Size(202, 24)
        Me.tbCodeEntree.StyleController = Me.LayoutControl5
        Me.tbCodeEntree.TabIndex = 13
        '
        'tbTelephone
        '
        Me.tbTelephone.Location = New System.Drawing.Point(162, 376)
        Me.tbTelephone.Name = "tbTelephone"
        Me.tbTelephone.Properties.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!)
        Me.tbTelephone.Properties.Appearance.Options.UseFont = True
        Me.tbTelephone.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.tbTelephone.Properties.MaskSettings.Set("MaskManagerType", GetType(DevExpress.Data.Mask.RegularMaskManager))
        Me.tbTelephone.Properties.MaskSettings.Set("mask", "(\d?\d?\d?) \d\d\d-\d\d\d\d")
        Me.tbTelephone.Size = New System.Drawing.Size(204, 24)
        Me.tbTelephone.StyleController = Me.LayoutControl5
        Me.tbTelephone.TabIndex = 14
        '
        'tbNom
        '
        Me.tbNom.Location = New System.Drawing.Point(162, 264)
        Me.tbNom.Name = "tbNom"
        Me.tbNom.Properties.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!)
        Me.tbNom.Properties.Appearance.Options.UseFont = True
        Me.tbNom.Size = New System.Drawing.Size(536, 24)
        Me.tbNom.StyleController = Me.LayoutControl5
        Me.tbNom.TabIndex = 11
        '
        'cblivreur
        '
        Me.cblivreur.Location = New System.Drawing.Point(150, 150)
        Me.cblivreur.Name = "cblivreur"
        Me.cblivreur.Properties.AcceptEditorTextAsNewValue = DevExpress.Utils.DefaultBoolean.[False]
        Me.cblivreur.Properties.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!)
        Me.cblivreur.Properties.Appearance.Options.UseFont = True
        Me.cblivreur.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cblivreur.Properties.PopupFilterMode = DevExpress.XtraEditors.PopupFilterMode.Contains
        Me.cblivreur.Size = New System.Drawing.Size(560, 24)
        Me.cblivreur.StyleController = Me.LayoutControl5
        Me.cblivreur.TabIndex = 28
        '
        'tbImportIDL
        '
        Me.tbImportIDL.EditValue = "0"
        Me.tbImportIDL.Enabled = False
        Me.tbImportIDL.Location = New System.Drawing.Point(150, 70)
        Me.tbImportIDL.Name = "tbImportIDL"
        Me.tbImportIDL.Properties.Appearance.BackColor = System.Drawing.Color.Azure
        Me.tbImportIDL.Properties.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!)
        Me.tbImportIDL.Properties.Appearance.Options.UseBackColor = True
        Me.tbImportIDL.Properties.Appearance.Options.UseFont = True
        Me.tbImportIDL.Size = New System.Drawing.Size(560, 24)
        Me.tbImportIDL.StyleController = Me.LayoutControl5
        Me.tbImportIDL.TabIndex = 73
        '
        'tbMobile
        '
        Me.tbMobile.Location = New System.Drawing.Point(496, 376)
        Me.tbMobile.Name = "tbMobile"
        Me.tbMobile.Properties.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!)
        Me.tbMobile.Properties.Appearance.Options.UseFont = True
        Me.tbMobile.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.tbMobile.Properties.MaskSettings.Set("MaskManagerType", GetType(DevExpress.Data.Mask.RegularMaskManager))
        Me.tbMobile.Properties.MaskSettings.Set("mask", "(\d?\d?\d?) \d\d\d-\d\d\d\d")
        Me.tbMobile.Size = New System.Drawing.Size(202, 24)
        Me.tbMobile.StyleController = Me.LayoutControl5
        Me.tbMobile.TabIndex = 14
        '
        'tbMontantOrganisation
        '
        Me.tbMontantOrganisation.EditValue = "$0.00"
        Me.tbMontantOrganisation.Location = New System.Drawing.Point(174, 460)
        Me.tbMontantOrganisation.Name = "tbMontantOrganisation"
        Me.tbMontantOrganisation.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.tbMontantOrganisation.Properties.Appearance.Options.UseFont = True
        Me.tbMontantOrganisation.Properties.DisplayFormat.FormatString = "c"
        Me.tbMontantOrganisation.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.tbMontantOrganisation.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.tbMontantOrganisation.Properties.MaskSettings.Set("MaskManagerType", GetType(DevExpress.Data.Mask.NumericMaskManager))
        Me.tbMontantOrganisation.Properties.MaskSettings.Set("mask", "c")
        Me.tbMontantOrganisation.Size = New System.Drawing.Size(512, 22)
        Me.tbMontantOrganisation.StyleController = Me.LayoutControl5
        Me.tbMontantOrganisation.TabIndex = 139
        '
        'tbMontantLivreur
        '
        Me.tbMontantLivreur.EditValue = "$0.00"
        Me.tbMontantLivreur.Location = New System.Drawing.Point(174, 486)
        Me.tbMontantLivreur.Name = "tbMontantLivreur"
        Me.tbMontantLivreur.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.tbMontantLivreur.Properties.Appearance.Options.UseFont = True
        Me.tbMontantLivreur.Properties.DisplayFormat.FormatString = "c"
        Me.tbMontantLivreur.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.tbMontantLivreur.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.tbMontantLivreur.Properties.MaskSettings.Set("MaskManagerType", GetType(DevExpress.Data.Mask.NumericMaskManager))
        Me.tbMontantLivreur.Properties.MaskSettings.Set("mask", "c")
        Me.tbMontantLivreur.Size = New System.Drawing.Size(512, 22)
        Me.tbMontantLivreur.StyleController = Me.LayoutControl5
        Me.tbMontantLivreur.TabIndex = 139
        '
        'tbDateLivraison
        '
        Me.tbDateLivraison.EditValue = Nothing
        Me.tbDateLivraison.Location = New System.Drawing.Point(150, 98)
        Me.tbDateLivraison.Name = "tbDateLivraison"
        Me.tbDateLivraison.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.tbDateLivraison.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.tbDateLivraison.Properties.DisplayFormat.FormatString = "MM-dd-yy H:mm"
        Me.tbDateLivraison.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.tbDateLivraison.Properties.EditFormat.FormatString = "MM-dd-yy H:mm"
        Me.tbDateLivraison.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.tbDateLivraison.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.tbDateLivraison.Properties.MaskSettings.Set("mask", "MM-dd-yy H:mm")
        Me.tbDateLivraison.Size = New System.Drawing.Size(560, 20)
        Me.tbDateLivraison.StyleController = Me.LayoutControl5
        Me.tbDateLivraison.TabIndex = 22
        '
        'tbDistanceR
        '
        Me.tbDistanceR.EditValue = "0"
        Me.tbDistanceR.Location = New System.Drawing.Point(162, 404)
        Me.tbDistanceR.Name = "tbDistanceR"
        Me.tbDistanceR.Properties.DisplayFormat.FormatString = "n"
        Me.tbDistanceR.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.tbDistanceR.Properties.EditFormat.FormatString = "n"
        Me.tbDistanceR.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.tbDistanceR.Properties.MaskSettings.Set("MaskManagerType", GetType(DevExpress.Data.Mask.NumericMaskManager))
        Me.tbDistanceR.Properties.MaskSettings.Set("mask", "n")
        Me.tbDistanceR.Size = New System.Drawing.Size(536, 20)
        Me.tbDistanceR.StyleController = Me.LayoutControl5
        Me.tbDistanceR.TabIndex = 126
        '
        'cbRestaurant
        '
        Me.cbRestaurant.Location = New System.Drawing.Point(150, 122)
        Me.cbRestaurant.Name = "cbRestaurant"
        Me.cbRestaurant.Properties.AcceptEditorTextAsNewValue = DevExpress.Utils.DefaultBoolean.[False]
        Me.cbRestaurant.Properties.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!)
        Me.cbRestaurant.Properties.Appearance.Options.UseFont = True
        Me.cbRestaurant.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbRestaurant.Properties.PopupFilterMode = DevExpress.XtraEditors.PopupFilterMode.Contains
        Me.cbRestaurant.Size = New System.Drawing.Size(560, 24)
        Me.cbRestaurant.StyleController = Me.LayoutControl5
        Me.cbRestaurant.TabIndex = 28
        '
        'cbVille
        '
        Me.cbVille.Location = New System.Drawing.Point(162, 320)
        Me.cbVille.Name = "cbVille"
        Me.cbVille.Properties.AcceptEditorTextAsNewValue = DevExpress.Utils.DefaultBoolean.[False]
        Me.cbVille.Properties.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!)
        Me.cbVille.Properties.Appearance.Options.UseFont = True
        Me.cbVille.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbVille.Properties.PopupFilterMode = DevExpress.XtraEditors.PopupFilterMode.Contains
        Me.cbVille.Size = New System.Drawing.Size(204, 24)
        Me.cbVille.StyleController = Me.LayoutControl5
        Me.cbVille.TabIndex = 28
        '
        'LayoutControlGroup9
        '
        Me.LayoutControlGroup9.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup9.GroupBordersVisible = False
        Me.LayoutControlGroup9.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlGroup10, Me.LayoutControlItem67, Me.LayoutControlItem70, Me.LayoutControlItem61, Me.LayoutControlItem31})
        Me.LayoutControlGroup9.Name = "Root"
        Me.LayoutControlGroup9.Size = New System.Drawing.Size(1546, 619)
        Me.LayoutControlGroup9.TextVisible = False
        '
        'LayoutControlGroup10
        '
        Me.LayoutControlGroup10.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlGroup13, Me.LayoutControlItem66, Me.LayoutControlItem59, Me.LayoutControlItem8, Me.LayoutControlItem34})
        Me.LayoutControlGroup10.Location = New System.Drawing.Point(0, 26)
        Me.LayoutControlGroup10.Name = "LayoutControlGroup10"
        Me.LayoutControlGroup10.Size = New System.Drawing.Size(714, 573)
        Me.LayoutControlGroup10.Text = "Nouvelle Livraison"
        '
        'LayoutControlGroup13
        '
        Me.LayoutControlGroup13.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem54, Me.LayoutControlItem57, Me.LayoutControlGroup15, Me.layoutAddressID, Me.LayoutControlItem49, Me.LayoutControlItem58, Me.LayoutControlItem53, Me.LayoutControlItem33, Me.LayoutControlItem50, Me.LayoutControlItem52, Me.LayoutControlItem56, Me.LayoutControlItem15, Me.LayoutControlItem16, Me.LayoutControlItem36})
        Me.LayoutControlGroup13.Location = New System.Drawing.Point(0, 108)
        Me.LayoutControlGroup13.Name = "LayoutControlGroup13"
        Me.LayoutControlGroup13.Size = New System.Drawing.Size(690, 421)
        Me.LayoutControlGroup13.Text = "Addresse Livraison détail"
        '
        'LayoutControlItem54
        '
        Me.LayoutControlItem54.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.LayoutControlItem54.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem54.Control = Me.tbAppartement
        Me.LayoutControlItem54.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem54.CustomizationFormText = "Appartement"
        Me.LayoutControlItem54.Location = New System.Drawing.Point(0, 138)
        Me.LayoutControlItem54.Name = "LayoutControlItem54"
        Me.LayoutControlItem54.Size = New System.Drawing.Size(334, 28)
        Me.LayoutControlItem54.Text = "Appartement"
        Me.LayoutControlItem54.TextSize = New System.Drawing.Size(122, 16)
        '
        'LayoutControlItem57
        '
        Me.LayoutControlItem57.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.LayoutControlItem57.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem57.Control = Me.tbTelephone
        Me.LayoutControlItem57.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem57.CustomizationFormText = "Téléphone"
        Me.LayoutControlItem57.Location = New System.Drawing.Point(0, 166)
        Me.LayoutControlItem57.Name = "LayoutControlItem57"
        Me.LayoutControlItem57.Size = New System.Drawing.Size(334, 28)
        Me.LayoutControlItem57.Text = "Téléphone"
        Me.LayoutControlItem57.TextSize = New System.Drawing.Size(122, 16)
        '
        'LayoutControlGroup15
        '
        Me.LayoutControlGroup15.CustomizationFormText = "Frais systeme"
        Me.LayoutControlGroup15.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.lblPrixI, Me.lblPrixI1})
        Me.LayoutControlGroup15.Location = New System.Drawing.Point(0, 218)
        Me.LayoutControlGroup15.Name = "LayoutControlGroup15"
        Me.LayoutControlGroup15.Size = New System.Drawing.Size(666, 159)
        Me.LayoutControlGroup15.Text = "Frais systeme"
        '
        'lblPrixI
        '
        Me.lblPrixI.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.lblPrixI.AppearanceItemCaption.Options.UseFont = True
        Me.lblPrixI.Control = Me.tbMontantOrganisation
        Me.lblPrixI.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.lblPrixI.CustomizationFormText = "Prix"
        Me.lblPrixI.Location = New System.Drawing.Point(0, 0)
        Me.lblPrixI.Name = "lblPrixI"
        Me.lblPrixI.Size = New System.Drawing.Size(642, 26)
        Me.lblPrixI.Text = "Montant Organisation"
        Me.lblPrixI.TextSize = New System.Drawing.Size(122, 16)
        '
        'lblPrixI1
        '
        Me.lblPrixI1.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.lblPrixI1.AppearanceItemCaption.Options.UseFont = True
        Me.lblPrixI1.Control = Me.tbMontantLivreur
        Me.lblPrixI1.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.lblPrixI1.CustomizationFormText = "Prix"
        Me.lblPrixI1.Location = New System.Drawing.Point(0, 26)
        Me.lblPrixI1.Name = "lblPrixI1"
        Me.lblPrixI1.Size = New System.Drawing.Size(642, 89)
        Me.lblPrixI1.Text = "Montant Livreur"
        Me.lblPrixI1.TextSize = New System.Drawing.Size(122, 16)
        '
        'layoutAddressID
        '
        Me.layoutAddressID.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.layoutAddressID.AppearanceItemCaption.Options.UseFont = True
        Me.layoutAddressID.Control = Me.tbCustomerAddressID
        Me.layoutAddressID.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.layoutAddressID.CustomizationFormText = "Bonus par kilométrage démarre a"
        Me.layoutAddressID.Location = New System.Drawing.Point(0, 26)
        Me.layoutAddressID.Name = "layoutAddressID"
        Me.layoutAddressID.Size = New System.Drawing.Size(666, 28)
        Me.layoutAddressID.Text = "ID"
        Me.layoutAddressID.TextSize = New System.Drawing.Size(122, 16)
        '
        'LayoutControlItem49
        '
        Me.LayoutControlItem49.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.LayoutControlItem49.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem49.Control = Me.tbAdresseNo
        Me.LayoutControlItem49.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem49.CustomizationFormText = "Contact #1"
        Me.LayoutControlItem49.Location = New System.Drawing.Point(0, 82)
        Me.LayoutControlItem49.Name = "LayoutControlItem49"
        Me.LayoutControlItem49.Size = New System.Drawing.Size(334, 28)
        Me.LayoutControlItem49.Text = "Numéro"
        Me.LayoutControlItem49.TextSize = New System.Drawing.Size(122, 16)
        '
        'LayoutControlItem58
        '
        Me.LayoutControlItem58.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.LayoutControlItem58.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem58.Control = Me.tbNom
        Me.LayoutControlItem58.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem58.CustomizationFormText = "Nom"
        Me.LayoutControlItem58.Location = New System.Drawing.Point(0, 54)
        Me.LayoutControlItem58.Name = "LayoutControlItem58"
        Me.LayoutControlItem58.Size = New System.Drawing.Size(666, 28)
        Me.LayoutControlItem58.Text = "Nom"
        Me.LayoutControlItem58.TextSize = New System.Drawing.Size(122, 16)
        '
        'LayoutControlItem53
        '
        Me.LayoutControlItem53.Control = Me.bSupprimerAddresse
        Me.LayoutControlItem53.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem53.Name = "LayoutControlItem53"
        Me.LayoutControlItem53.Size = New System.Drawing.Size(316, 26)
        Me.LayoutControlItem53.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem53.TextVisible = False
        '
        'LayoutControlItem33
        '
        Me.LayoutControlItem33.Control = Me.bRemoveFilter
        Me.LayoutControlItem33.Location = New System.Drawing.Point(316, 0)
        Me.LayoutControlItem33.Name = "LayoutControlItem33"
        Me.LayoutControlItem33.Size = New System.Drawing.Size(350, 26)
        Me.LayoutControlItem33.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem33.TextVisible = False
        '
        'LayoutControlItem50
        '
        Me.LayoutControlItem50.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.LayoutControlItem50.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem50.Control = Me.tbAdresse
        Me.LayoutControlItem50.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem50.CustomizationFormText = "Adresse"
        Me.LayoutControlItem50.Location = New System.Drawing.Point(334, 82)
        Me.LayoutControlItem50.Name = "LayoutControlItem50"
        Me.LayoutControlItem50.Size = New System.Drawing.Size(332, 28)
        Me.LayoutControlItem50.Text = "Adresse"
        Me.LayoutControlItem50.TextSize = New System.Drawing.Size(122, 16)
        '
        'LayoutControlItem52
        '
        Me.LayoutControlItem52.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.LayoutControlItem52.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem52.Control = Me.tbCodePostale
        Me.LayoutControlItem52.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem52.CustomizationFormText = "Code Postale"
        Me.LayoutControlItem52.Location = New System.Drawing.Point(334, 110)
        Me.LayoutControlItem52.Name = "LayoutControlItem52"
        Me.LayoutControlItem52.Size = New System.Drawing.Size(332, 28)
        Me.LayoutControlItem52.Text = "Code Postale"
        Me.LayoutControlItem52.TextSize = New System.Drawing.Size(122, 16)
        '
        'LayoutControlItem56
        '
        Me.LayoutControlItem56.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.LayoutControlItem56.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem56.Control = Me.tbCodeEntree
        Me.LayoutControlItem56.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem56.CustomizationFormText = "Code entrée"
        Me.LayoutControlItem56.Location = New System.Drawing.Point(334, 138)
        Me.LayoutControlItem56.Name = "LayoutControlItem56"
        Me.LayoutControlItem56.Size = New System.Drawing.Size(332, 28)
        Me.LayoutControlItem56.Text = "Code entrée"
        Me.LayoutControlItem56.TextSize = New System.Drawing.Size(122, 16)
        '
        'LayoutControlItem15
        '
        Me.LayoutControlItem15.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.LayoutControlItem15.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem15.Control = Me.tbMobile
        Me.LayoutControlItem15.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem15.CustomizationFormText = "Téléphone"
        Me.LayoutControlItem15.Location = New System.Drawing.Point(334, 166)
        Me.LayoutControlItem15.Name = "LayoutControlItem15"
        Me.LayoutControlItem15.Size = New System.Drawing.Size(332, 28)
        Me.LayoutControlItem15.Text = "Mobile"
        Me.LayoutControlItem15.TextSize = New System.Drawing.Size(122, 16)
        '
        'LayoutControlItem16
        '
        Me.LayoutControlItem16.Control = Me.tbDistanceR
        Me.LayoutControlItem16.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem16.CustomizationFormText = "Bonus par kilométrage démarre a"
        Me.LayoutControlItem16.Location = New System.Drawing.Point(0, 194)
        Me.LayoutControlItem16.Name = "LayoutControlItem16"
        Me.LayoutControlItem16.Size = New System.Drawing.Size(666, 24)
        Me.LayoutControlItem16.Text = "Distance"
        Me.LayoutControlItem16.TextSize = New System.Drawing.Size(122, 13)
        '
        'LayoutControlItem36
        '
        Me.LayoutControlItem36.Control = Me.cbVille
        Me.LayoutControlItem36.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem36.CustomizationFormText = "Périodes"
        Me.LayoutControlItem36.Location = New System.Drawing.Point(0, 110)
        Me.LayoutControlItem36.Name = "LayoutControlItem36"
        Me.LayoutControlItem36.Size = New System.Drawing.Size(334, 28)
        Me.LayoutControlItem36.Text = "Ville"
        Me.LayoutControlItem36.TextSize = New System.Drawing.Size(122, 13)
        '
        'LayoutControlItem66
        '
        Me.LayoutControlItem66.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.LayoutControlItem66.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem66.Control = Me.tbImportIDL
        Me.LayoutControlItem66.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem66.CustomizationFormText = "ID"
        Me.LayoutControlItem66.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem66.Name = "LayoutControlItem66"
        Me.LayoutControlItem66.Size = New System.Drawing.Size(690, 28)
        Me.LayoutControlItem66.Text = "ID Livraison"
        Me.LayoutControlItem66.TextSize = New System.Drawing.Size(122, 16)
        '
        'LayoutControlItem59
        '
        Me.LayoutControlItem59.AppearanceItemCaption.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.LayoutControlItem59.AppearanceItemCaption.Options.UseFont = True
        Me.LayoutControlItem59.Control = Me.cblivreur
        Me.LayoutControlItem59.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem59.CustomizationFormText = "Périodes"
        Me.LayoutControlItem59.Location = New System.Drawing.Point(0, 80)
        Me.LayoutControlItem59.Name = "LayoutControlItem59"
        Me.LayoutControlItem59.Size = New System.Drawing.Size(690, 28)
        Me.LayoutControlItem59.Text = "Livreur"
        Me.LayoutControlItem59.TextSize = New System.Drawing.Size(122, 16)
        '
        'LayoutControlItem8
        '
        Me.LayoutControlItem8.Control = Me.tbDateLivraison
        Me.LayoutControlItem8.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem8.CustomizationFormText = "Date du"
        Me.LayoutControlItem8.Location = New System.Drawing.Point(0, 28)
        Me.LayoutControlItem8.Name = "LayoutControlItem8"
        Me.LayoutControlItem8.Size = New System.Drawing.Size(690, 24)
        Me.LayoutControlItem8.Text = "Date de livraison"
        Me.LayoutControlItem8.TextSize = New System.Drawing.Size(122, 13)
        '
        'LayoutControlItem34
        '
        Me.LayoutControlItem34.Control = Me.cbRestaurant
        Me.LayoutControlItem34.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem34.CustomizationFormText = "Périodes"
        Me.LayoutControlItem34.Location = New System.Drawing.Point(0, 52)
        Me.LayoutControlItem34.Name = "LayoutControlItem34"
        Me.LayoutControlItem34.Size = New System.Drawing.Size(690, 28)
        Me.LayoutControlItem34.Text = "Commerce"
        Me.LayoutControlItem34.TextSize = New System.Drawing.Size(122, 13)
        '
        'LayoutControlItem67
        '
        Me.LayoutControlItem67.Control = Me.bAddToImport
        Me.LayoutControlItem67.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem67.Name = "LayoutControlItem67"
        Me.LayoutControlItem67.Size = New System.Drawing.Size(213, 26)
        Me.LayoutControlItem67.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem67.TextVisible = False
        '
        'LayoutControlItem70
        '
        Me.LayoutControlItem70.Control = Me.bReturnToAction
        Me.LayoutControlItem70.Location = New System.Drawing.Point(442, 0)
        Me.LayoutControlItem70.Name = "LayoutControlItem70"
        Me.LayoutControlItem70.Size = New System.Drawing.Size(272, 26)
        Me.LayoutControlItem70.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem70.TextVisible = False
        '
        'LayoutControlItem61
        '
        Me.LayoutControlItem61.Control = Me.bImportMiseaJour
        Me.LayoutControlItem61.Location = New System.Drawing.Point(213, 0)
        Me.LayoutControlItem61.Name = "LayoutControlItem61"
        Me.LayoutControlItem61.Size = New System.Drawing.Size(229, 26)
        Me.LayoutControlItem61.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem61.TextVisible = False
        '
        'LayoutControlItem31
        '
        Me.LayoutControlItem31.Control = Me.grdClientsAddresse
        Me.LayoutControlItem31.Location = New System.Drawing.Point(714, 0)
        Me.LayoutControlItem31.Name = "LayoutControlItem31"
        Me.LayoutControlItem31.Size = New System.Drawing.Size(812, 599)
        Me.LayoutControlItem31.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem31.TextVisible = False
        '
        'Root
        '
        Me.Root.CustomizationFormText = "Root"
        Me.Root.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.Root.GroupBordersVisible = False
        Me.Root.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem38})
        Me.Root.Name = "Root"
        Me.Root.Size = New System.Drawing.Size(1575, 672)
        Me.Root.TextVisible = False
        '
        'LayoutControlItem38
        '
        Me.LayoutControlItem38.Control = Me.tabMain
        Me.LayoutControlItem38.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem38.CustomizationFormText = "LayoutControlItem38"
        Me.LayoutControlItem38.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem38.Name = "LayoutControlItem38"
        Me.LayoutControlItem38.Size = New System.Drawing.Size(1555, 652)
        Me.LayoutControlItem38.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem38.TextVisible = False
        '
        'navbarImageCollectionLarge4
        '
        Me.navbarImageCollectionLarge4.ImageSize = New System.Drawing.Size(32, 32)
        Me.navbarImageCollectionLarge4.ImageStream = CType(resources.GetObject("navbarImageCollectionLarge4.ImageStream"), DevExpress.Utils.ImageCollectionStreamer)
        Me.navbarImageCollectionLarge4.TransparentColor = System.Drawing.Color.Transparent
        Me.navbarImageCollectionLarge4.Images.SetKeyName(0, "Mail_32x32.png")
        Me.navbarImageCollectionLarge4.Images.SetKeyName(1, "Organizer_32x32.png")
        Me.navbarImageCollectionLarge4.Images.SetKeyName(2, "table-import-512.png")
        Me.navbarImageCollectionLarge4.Images.SetKeyName(3, "images.png")
        Me.navbarImageCollectionLarge4.Images.SetKeyName(4, "transparent-bill-icon-payment-icon-invoice-icon-5e23aca6b774e8.609381951579396262" &
        "7514.jpg")
        Me.navbarImageCollectionLarge4.Images.SetKeyName(5, "2244482.png")
        '
        'navbarImageCollectionLarge21
        '
        Me.navbarImageCollectionLarge21.ImageSize = New System.Drawing.Size(32, 32)
        Me.navbarImageCollectionLarge21.ImageStream = CType(resources.GetObject("navbarImageCollectionLarge21.ImageStream"), DevExpress.Utils.ImageCollectionStreamer)
        Me.navbarImageCollectionLarge21.TransparentColor = System.Drawing.Color.Transparent
        Me.navbarImageCollectionLarge21.Images.SetKeyName(0, "Mail_32x32.png")
        Me.navbarImageCollectionLarge21.Images.SetKeyName(1, "Organizer_32x32.png")
        Me.navbarImageCollectionLarge21.Images.SetKeyName(2, "table-import-512.png")
        Me.navbarImageCollectionLarge21.Images.SetKeyName(3, "images.png")
        Me.navbarImageCollectionLarge21.Images.SetKeyName(4, "transparent-bill-icon-payment-icon-invoice-icon-5e23aca6b774e8.609381951579396262" &
        "7514.jpg")
        Me.navbarImageCollectionLarge21.Images.SetKeyName(5, "2244482.png")
        '
        'navbarImageCollectionLarge
        '
        Me.navbarImageCollectionLarge.ImageSize = New System.Drawing.Size(32, 32)
        Me.navbarImageCollectionLarge.ImageStream = CType(resources.GetObject("navbarImageCollectionLarge.ImageStream"), DevExpress.Utils.ImageCollectionStreamer)
        Me.navbarImageCollectionLarge.TransparentColor = System.Drawing.Color.Transparent
        Me.navbarImageCollectionLarge.Images.SetKeyName(0, "Mail_32x32.png")
        Me.navbarImageCollectionLarge.Images.SetKeyName(1, "Organizer_32x32.png")
        Me.navbarImageCollectionLarge.Images.SetKeyName(2, "table-import-512.png")
        Me.navbarImageCollectionLarge.Images.SetKeyName(3, "images.png")
        Me.navbarImageCollectionLarge.Images.SetKeyName(4, "transparent-bill-icon-payment-icon-invoice-icon-5e23aca6b774e8.609381951579396262" &
        "7514.jpg")
        Me.navbarImageCollectionLarge.Images.SetKeyName(5, "2244482.png")
        '
        'navbarImageCollectionLarge2
        '
        Me.navbarImageCollectionLarge2.ImageSize = New System.Drawing.Size(32, 32)
        Me.navbarImageCollectionLarge2.ImageStream = CType(resources.GetObject("navbarImageCollectionLarge2.ImageStream"), DevExpress.Utils.ImageCollectionStreamer)
        Me.navbarImageCollectionLarge2.TransparentColor = System.Drawing.Color.Transparent
        Me.navbarImageCollectionLarge2.Images.SetKeyName(0, "Mail_32x32.png")
        Me.navbarImageCollectionLarge2.Images.SetKeyName(1, "Organizer_32x32.png")
        Me.navbarImageCollectionLarge2.Images.SetKeyName(2, "table-import-512.png")
        Me.navbarImageCollectionLarge2.Images.SetKeyName(3, "images.png")
        Me.navbarImageCollectionLarge2.Images.SetKeyName(4, "transparent-bill-icon-payment-icon-invoice-icon-5e23aca6b774e8.609381951579396262" &
        "7514.jpg")
        Me.navbarImageCollectionLarge2.Images.SetKeyName(5, "2244482.png")
        '
        'navbarImageCollectionLarge1
        '
        Me.navbarImageCollectionLarge1.ImageSize = New System.Drawing.Size(32, 32)
        Me.navbarImageCollectionLarge1.ImageStream = CType(resources.GetObject("navbarImageCollectionLarge1.ImageStream"), DevExpress.Utils.ImageCollectionStreamer)
        Me.navbarImageCollectionLarge1.TransparentColor = System.Drawing.Color.Transparent
        Me.navbarImageCollectionLarge1.Images.SetKeyName(0, "Mail_32x32.png")
        Me.navbarImageCollectionLarge1.Images.SetKeyName(1, "Organizer_32x32.png")
        Me.navbarImageCollectionLarge1.Images.SetKeyName(2, "table-import-512.png")
        Me.navbarImageCollectionLarge1.Images.SetKeyName(3, "images.png")
        Me.navbarImageCollectionLarge1.Images.SetKeyName(4, "transparent-bill-icon-payment-icon-invoice-icon-5e23aca6b774e8.609381951579396262" &
        "7514.jpg")
        Me.navbarImageCollectionLarge1.Images.SetKeyName(5, "2244482.png")
        '
        'ribbonImageCollection
        '
        Me.ribbonImageCollection.ImageStream = CType(resources.GetObject("ribbonImageCollection.ImageStream"), DevExpress.Utils.ImageCollectionStreamer)
        Me.ribbonImageCollection.Images.SetKeyName(0, "Ribbon_New_16x16.png")
        Me.ribbonImageCollection.Images.SetKeyName(1, "Ribbon_Open_16x16.png")
        Me.ribbonImageCollection.Images.SetKeyName(2, "Ribbon_Close_16x16.png")
        Me.ribbonImageCollection.Images.SetKeyName(3, "Ribbon_Find_16x16.png")
        Me.ribbonImageCollection.Images.SetKeyName(4, "Ribbon_Save_16x16.png")
        Me.ribbonImageCollection.Images.SetKeyName(5, "Ribbon_SaveAs_16x16.png")
        Me.ribbonImageCollection.Images.SetKeyName(6, "Ribbon_Exit_16x16.png")
        Me.ribbonImageCollection.Images.SetKeyName(7, "Ribbon_Content_16x16.png")
        Me.ribbonImageCollection.Images.SetKeyName(8, "Ribbon_Info_16x16.png")
        Me.ribbonImageCollection.Images.SetKeyName(9, "Ribbon_Bold_16x16.png")
        Me.ribbonImageCollection.Images.SetKeyName(10, "Ribbon_Italic_16x16.png")
        Me.ribbonImageCollection.Images.SetKeyName(11, "Ribbon_Underline_16x16.png")
        Me.ribbonImageCollection.Images.SetKeyName(12, "Ribbon_AlignLeft_16x16.png")
        Me.ribbonImageCollection.Images.SetKeyName(13, "Ribbon_AlignCenter_16x16.png")
        Me.ribbonImageCollection.Images.SetKeyName(14, "Ribbon_AlignRight_16x16.png")
        '
        'BarButtonItem2
        '
        Me.BarButtonItem2.Caption = "Payes"
        Me.BarButtonItem2.Enabled = False
        Me.BarButtonItem2.Id = 12
        Me.BarButtonItem2.ImageOptions.Image = CType(resources.GetObject("BarButtonItem2.ImageOptions.Image"), System.Drawing.Image)
        Me.BarButtonItem2.ImageOptions.LargeImage = CType(resources.GetObject("BarButtonItem2.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.BarButtonItem2.Name = "BarButtonItem2"
        '
        'BarButtonItem6
        '
        Me.BarButtonItem6.Caption = "Payes"
        Me.BarButtonItem6.Enabled = False
        Me.BarButtonItem6.Id = 12
        Me.BarButtonItem6.ImageOptions.Image = CType(resources.GetObject("BarButtonItem6.ImageOptions.Image"), System.Drawing.Image)
        Me.BarButtonItem6.ImageOptions.LargeImage = CType(resources.GetObject("BarButtonItem6.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.BarButtonItem6.Name = "BarButtonItem6"
        '
        'DockManager1
        '
        Me.DockManager1.Form = Me
        Me.DockManager1.HiddenPanels.AddRange(New DevExpress.XtraBars.Docking.DockPanel() {Me.DockEdit, Me.DockResidence})
        Me.DockManager1.TopZIndexControls.AddRange(New String() {"DevExpress.XtraBars.BarDockControl", "DevExpress.XtraBars.StandaloneBarDockControl", "System.Windows.Forms.MenuStrip", "System.Windows.Forms.StatusStrip", "System.Windows.Forms.StatusBar", "DevExpress.XtraBars.Ribbon.RibbonStatusBar", "DevExpress.XtraBars.Ribbon.RibbonControl", "DevExpress.XtraBars.Navigation.OfficeNavigationBar", "DevExpress.XtraBars.Navigation.TileNavPane", "DevExpress.XtraBars.TabFormControl", "DevExpress.XtraBars.FluentDesignSystem.FluentDesignFormControl", "DevExpress.XtraBars.ToolbarForm.ToolbarFormControl"})
        '
        'DockEdit
        '
        Me.DockEdit.Controls.Add(Me.DockPanel1_Container)
        Me.DockEdit.Dock = DevExpress.XtraBars.Docking.DockingStyle.Float
        Me.DockEdit.FloatLocation = New System.Drawing.Point(598, 546)
        Me.DockEdit.FloatSize = New System.Drawing.Size(375, 235)
        Me.DockEdit.ID = New System.Guid("f4a8ca56-e374-4452-b352-250e20e430b7")
        Me.DockEdit.Location = New System.Drawing.Point(-32768, -32768)
        Me.DockEdit.Name = "DockEdit"
        Me.DockEdit.OriginalSize = New System.Drawing.Size(419, 200)
        Me.DockEdit.SavedIndex = 0
        Me.DockEdit.Size = New System.Drawing.Size(375, 235)
        Me.DockEdit.Text = "Modifier la distance"
        Me.DockEdit.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Hidden
        '
        'DockPanel1_Container
        '
        Me.DockPanel1_Container.Controls.Add(Me.LayoutControl2)
        Me.DockPanel1_Container.Location = New System.Drawing.Point(4, 30)
        Me.DockPanel1_Container.Name = "DockPanel1_Container"
        Me.DockPanel1_Container.Size = New System.Drawing.Size(367, 201)
        Me.DockPanel1_Container.TabIndex = 0
        '
        'LayoutControl2
        '
        Me.LayoutControl2.Controls.Add(Me.btnAnnuler)
        Me.LayoutControl2.Controls.Add(Me.bSauvegarde)
        Me.LayoutControl2.Controls.Add(Me.tbImportID)
        Me.LayoutControl2.Controls.Add(Me.tbDistance)
        Me.LayoutControl2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LayoutControl2.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControl2.Name = "LayoutControl2"
        Me.LayoutControl2.Root = Me.LayoutControlGroup4
        Me.LayoutControl2.Size = New System.Drawing.Size(367, 201)
        Me.LayoutControl2.TabIndex = 0
        Me.LayoutControl2.Text = "LayoutControl2"
        '
        'btnAnnuler
        '
        Me.btnAnnuler.Location = New System.Drawing.Point(185, 60)
        Me.btnAnnuler.Name = "btnAnnuler"
        Me.btnAnnuler.Size = New System.Drawing.Size(170, 22)
        Me.btnAnnuler.StyleController = Me.LayoutControl2
        Me.btnAnnuler.TabIndex = 137
        Me.btnAnnuler.Text = "Annuler"
        '
        'bSauvegarde
        '
        Me.bSauvegarde.Location = New System.Drawing.Point(12, 60)
        Me.bSauvegarde.Name = "bSauvegarde"
        Me.bSauvegarde.Size = New System.Drawing.Size(169, 22)
        Me.bSauvegarde.StyleController = Me.LayoutControl2
        Me.bSauvegarde.TabIndex = 136
        Me.bSauvegarde.Text = "Sauvegarder"
        '
        'tbImportID
        '
        Me.tbImportID.EditValue = "0"
        Me.tbImportID.Enabled = False
        Me.tbImportID.Location = New System.Drawing.Point(62, 12)
        Me.tbImportID.Name = "tbImportID"
        Me.tbImportID.Properties.Appearance.BackColor = System.Drawing.Color.Linen
        Me.tbImportID.Properties.Appearance.Options.UseBackColor = True
        Me.tbImportID.Properties.MaskSettings.Set("MaskManagerType", GetType(DevExpress.Data.Mask.NumericMaskManager))
        Me.tbImportID.Properties.MaskSettings.Set("mask", "n0")
        Me.tbImportID.Size = New System.Drawing.Size(293, 20)
        Me.tbImportID.StyleController = Me.LayoutControl2
        Me.tbImportID.TabIndex = 132
        '
        'tbDistance
        '
        Me.tbDistance.EditValue = "0"
        Me.tbDistance.Location = New System.Drawing.Point(62, 36)
        Me.tbDistance.Name = "tbDistance"
        Me.tbDistance.Properties.DisplayFormat.FormatString = "n"
        Me.tbDistance.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.tbDistance.Properties.EditFormat.FormatString = "n"
        Me.tbDistance.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.tbDistance.Properties.MaskSettings.Set("MaskManagerType", GetType(DevExpress.Data.Mask.NumericMaskManager))
        Me.tbDistance.Properties.MaskSettings.Set("mask", "n")
        Me.tbDistance.Size = New System.Drawing.Size(293, 20)
        Me.tbDistance.StyleController = Me.LayoutControl2
        Me.tbDistance.TabIndex = 126
        '
        'LayoutControlGroup4
        '
        Me.LayoutControlGroup4.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup4.GroupBordersVisible = False
        Me.LayoutControlGroup4.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem43, Me.LayoutParNombreKmCL, Me.LayoutControlItem44, Me.LayoutControlItem45})
        Me.LayoutControlGroup4.Name = "Root"
        Me.LayoutControlGroup4.Size = New System.Drawing.Size(367, 201)
        Me.LayoutControlGroup4.TextVisible = False
        '
        'LayoutControlItem43
        '
        Me.LayoutControlItem43.Control = Me.tbImportID
        Me.LayoutControlItem43.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem43.CustomizationFormText = "Nombre maximum free echange (305 et 35)"
        Me.LayoutControlItem43.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem43.Name = "LayoutControlItem43"
        Me.LayoutControlItem43.Size = New System.Drawing.Size(347, 24)
        Me.LayoutControlItem43.Text = "ID Import"
        Me.LayoutControlItem43.TextSize = New System.Drawing.Size(46, 13)
        '
        'LayoutParNombreKmCL
        '
        Me.LayoutParNombreKmCL.Control = Me.tbDistance
        Me.LayoutParNombreKmCL.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutParNombreKmCL.CustomizationFormText = "Bonus par kilométrage démarre a"
        Me.LayoutParNombreKmCL.Location = New System.Drawing.Point(0, 24)
        Me.LayoutParNombreKmCL.Name = "LayoutParNombreKmCL"
        Me.LayoutParNombreKmCL.Size = New System.Drawing.Size(347, 24)
        Me.LayoutParNombreKmCL.Text = "Distance"
        Me.LayoutParNombreKmCL.TextSize = New System.Drawing.Size(46, 13)
        '
        'LayoutControlItem44
        '
        Me.LayoutControlItem44.Control = Me.bSauvegarde
        Me.LayoutControlItem44.Location = New System.Drawing.Point(0, 48)
        Me.LayoutControlItem44.Name = "LayoutControlItem44"
        Me.LayoutControlItem44.Size = New System.Drawing.Size(173, 133)
        Me.LayoutControlItem44.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem44.TextVisible = False
        '
        'LayoutControlItem45
        '
        Me.LayoutControlItem45.Control = Me.btnAnnuler
        Me.LayoutControlItem45.Location = New System.Drawing.Point(173, 48)
        Me.LayoutControlItem45.Name = "LayoutControlItem45"
        Me.LayoutControlItem45.Size = New System.Drawing.Size(174, 133)
        Me.LayoutControlItem45.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem45.TextVisible = False
        '
        'DockResidence
        '
        Me.DockResidence.Controls.Add(Me.ControlContainer1)
        Me.DockResidence.Dock = DevExpress.XtraBars.Docking.DockingStyle.Left
        Me.DockResidence.FloatSize = New System.Drawing.Size(300, 144)
        Me.DockResidence.ID = New System.Guid("5f979c89-8069-4ad2-8d13-f57e6df649c1")
        Me.DockResidence.Location = New System.Drawing.Point(0, 147)
        Me.DockResidence.Name = "DockResidence"
        Me.DockResidence.OriginalSize = New System.Drawing.Size(258, 200)
        Me.DockResidence.SavedDock = DevExpress.XtraBars.Docking.DockingStyle.Left
        Me.DockResidence.SavedIndex = 0
        Me.DockResidence.Size = New System.Drawing.Size(258, 672)
        Me.DockResidence.Text = "DockResidence"
        Me.DockResidence.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Hidden
        '
        'ControlContainer1
        '
        Me.ControlContainer1.Controls.Add(Me.LayoutControl6)
        Me.ControlContainer1.Location = New System.Drawing.Point(3, 29)
        Me.ControlContainer1.Name = "ControlContainer1"
        Me.ControlContainer1.Size = New System.Drawing.Size(251, 640)
        Me.ControlContainer1.TabIndex = 0
        '
        'LayoutControl6
        '
        Me.LayoutControl6.Controls.Add(Me.bEffacerResidence)
        Me.LayoutControl6.Controls.Add(Me.bAnnulerResidence)
        Me.LayoutControl6.Controls.Add(Me.bAppliquerResidence)
        Me.LayoutControl6.Controls.Add(Me.cbResidence)
        Me.LayoutControl6.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LayoutControl6.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControl6.Name = "LayoutControl6"
        Me.LayoutControl6.Root = Me.LayoutControlGroup2
        Me.LayoutControl6.Size = New System.Drawing.Size(251, 640)
        Me.LayoutControl6.TabIndex = 0
        Me.LayoutControl6.Text = "LayoutControl6"
        '
        'bEffacerResidence
        '
        Me.bEffacerResidence.Location = New System.Drawing.Point(12, 62)
        Me.bEffacerResidence.Name = "bEffacerResidence"
        Me.bEffacerResidence.Size = New System.Drawing.Size(227, 22)
        Me.bEffacerResidence.StyleController = Me.LayoutControl6
        Me.bEffacerResidence.TabIndex = 7
        Me.bEffacerResidence.Text = "Effacer l'affectation de résidence"
        '
        'bAnnulerResidence
        '
        Me.bAnnulerResidence.Location = New System.Drawing.Point(127, 36)
        Me.bAnnulerResidence.Name = "bAnnulerResidence"
        Me.bAnnulerResidence.Size = New System.Drawing.Size(112, 22)
        Me.bAnnulerResidence.StyleController = Me.LayoutControl6
        Me.bAnnulerResidence.TabIndex = 6
        Me.bAnnulerResidence.Text = "Annuler"
        '
        'bAppliquerResidence
        '
        Me.bAppliquerResidence.Location = New System.Drawing.Point(12, 36)
        Me.bAppliquerResidence.Name = "bAppliquerResidence"
        Me.bAppliquerResidence.Size = New System.Drawing.Size(111, 22)
        Me.bAppliquerResidence.StyleController = Me.LayoutControl6
        Me.bAppliquerResidence.TabIndex = 5
        Me.bAppliquerResidence.Text = "Appliquer"
        '
        'cbResidence
        '
        Me.cbResidence.Location = New System.Drawing.Point(65, 12)
        Me.cbResidence.MenuManager = Me.RibbonControl
        Me.cbResidence.Name = "cbResidence"
        Me.cbResidence.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbResidence.Size = New System.Drawing.Size(174, 20)
        Me.cbResidence.StyleController = Me.LayoutControl6
        Me.cbResidence.TabIndex = 4
        '
        'LayoutControlGroup2
        '
        Me.LayoutControlGroup2.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup2.GroupBordersVisible = False
        Me.LayoutControlGroup2.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.EmptySpaceItem2, Me.LayoutControlItem41, Me.LayoutControlItem55, Me.LayoutControlItem60, Me.LayoutControlItem62})
        Me.LayoutControlGroup2.Name = "LayoutControlGroup2"
        Me.LayoutControlGroup2.Size = New System.Drawing.Size(251, 640)
        Me.LayoutControlGroup2.TextVisible = False
        '
        'EmptySpaceItem2
        '
        Me.EmptySpaceItem2.AllowHotTrack = False
        Me.EmptySpaceItem2.Location = New System.Drawing.Point(0, 76)
        Me.EmptySpaceItem2.Name = "EmptySpaceItem2"
        Me.EmptySpaceItem2.Size = New System.Drawing.Size(231, 544)
        Me.EmptySpaceItem2.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlItem41
        '
        Me.LayoutControlItem41.Control = Me.cbResidence
        Me.LayoutControlItem41.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem41.Name = "LayoutControlItem41"
        Me.LayoutControlItem41.Size = New System.Drawing.Size(231, 24)
        Me.LayoutControlItem41.Text = "Résidence"
        Me.LayoutControlItem41.TextSize = New System.Drawing.Size(49, 13)
        '
        'LayoutControlItem55
        '
        Me.LayoutControlItem55.Control = Me.bAnnulerResidence
        Me.LayoutControlItem55.Location = New System.Drawing.Point(115, 24)
        Me.LayoutControlItem55.Name = "LayoutControlItem55"
        Me.LayoutControlItem55.Size = New System.Drawing.Size(116, 26)
        Me.LayoutControlItem55.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem55.TextVisible = False
        '
        'LayoutControlItem60
        '
        Me.LayoutControlItem60.Control = Me.bAppliquerResidence
        Me.LayoutControlItem60.Location = New System.Drawing.Point(0, 24)
        Me.LayoutControlItem60.Name = "LayoutControlItem60"
        Me.LayoutControlItem60.Size = New System.Drawing.Size(115, 26)
        Me.LayoutControlItem60.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem60.TextVisible = False
        '
        'LayoutControlItem62
        '
        Me.LayoutControlItem62.Control = Me.bEffacerResidence
        Me.LayoutControlItem62.Location = New System.Drawing.Point(0, 50)
        Me.LayoutControlItem62.Name = "LayoutControlItem62"
        Me.LayoutControlItem62.Size = New System.Drawing.Size(231, 26)
        Me.LayoutControlItem62.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem62.TextVisible = False
        '
        'LayoutControlGroup11
        '
        Me.LayoutControlGroup11.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup11.Name = "LayoutControlGroup11"
        Me.LayoutControlGroup11.Size = New System.Drawing.Size(1517, 665)
        '
        'BarButtonItem9
        '
        Me.BarButtonItem9.Caption = "1) Générer la facturation"
        Me.BarButtonItem9.Enabled = False
        Me.BarButtonItem9.Id = 38
        Me.BarButtonItem9.ImageOptions.Image = CType(resources.GetObject("BarButtonItem9.ImageOptions.Image"), System.Drawing.Image)
        Me.BarButtonItem9.Name = "BarButtonItem9"
        '
        'BarButtonItem10
        '
        Me.BarButtonItem10.Caption = "5) Imprimer les payes"
        Me.BarButtonItem10.Enabled = False
        Me.BarButtonItem10.Id = 36
        Me.BarButtonItem10.ImageOptions.Image = CType(resources.GetObject("BarButtonItem10.ImageOptions.Image"), System.Drawing.Image)
        Me.BarButtonItem10.Name = "BarButtonItem10"
        '
        'BarButtonItem11
        '
        Me.BarButtonItem11.Caption = "État des achats"
        Me.BarButtonItem11.Id = 29
        Me.BarButtonItem11.ImageOptions.Image = CType(resources.GetObject("BarButtonItem11.ImageOptions.Image"), System.Drawing.Image)
        Me.BarButtonItem11.Name = "BarButtonItem11"
        '
        'frmMain
        '
        Me.AllowFormGlass = DevExpress.Utils.DefaultBoolean.[True]
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1575, 842)
        Me.Controls.Add(Me.LayoutControl1)
        Me.Controls.Add(Me.RibbonStatusBar)
        Me.Controls.Add(Me.RibbonControl)
        Me.IconOptions.Icon = CType(resources.GetObject("frmMain.IconOptions.Icon"), System.Drawing.Icon)
        Me.IconOptions.Image = CType(resources.GetObject("frmMain.IconOptions.Image"), System.Drawing.Image)
        Me.Name = "frmMain"
        Me.Ribbon = Me.RibbonControl
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.StatusBar = Me.RibbonStatusBar
        Me.Text = "IDS Comptabilité"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.RibbonControl, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemLookUpEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LayoutControl1.ResumeLayout(False)
        CType(Me.tabMain, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabMain.ResumeLayout(False)
        Me.XtraTabPage1.ResumeLayout(False)
        CType(Me.LayoutControl3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LayoutControl3.ResumeLayout(False)
        CType(Me.cbRapportRevenuPaye.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.optTypePeriode.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbAnnee.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbFactures.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbPayes.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Calendar.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ProgressBarControl1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtMobilusXLS.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEdit1.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.navbarImageCollectionLarge3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbFichierImport.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbDateFrom.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbDateFrom.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbDateTo.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbDateTo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbPeriodes.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbEtatsDeComptes.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbLivraisonRestaurants.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbCommissions.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbRapportTaxation.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutBas, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem17, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem26, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem22, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem23, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem28, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem32, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem27, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem48, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem14, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem13, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutDroite, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutEtat, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem42, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutEtatLivraisonRestaurant, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem69, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutFactures, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutPayes, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem24, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem25, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem19, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem21, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem35, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem47, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem64, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem72, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem74, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem75, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutCommissions, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem37, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem46, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem51, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutCommissions1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem63, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem65, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bResetTaxationLivreurs, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem20, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem68, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem71, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem73, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem40, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem7, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabPage2.ResumeLayout(False)
        CType(Me.LayoutControl4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LayoutControl4.ResumeLayout(False)
        CType(Me.optFiltreGrille.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grdImport, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemMemoEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemMemoEdit3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemMemoEdit2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemCalcEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemTextEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemTextEdit2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemCalcEdit2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemSpinEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemCheckEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem18, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem29, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem30, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem39, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabPage3.ResumeLayout(False)
        CType(Me.LayoutControl5, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LayoutControl5.ResumeLayout(False)
        CType(Me.grdClientsAddresse, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbCustomerAddressID.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbAdresseNo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbAdresse.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbCodePostale.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbAppartement.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbCodeEntree.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbTelephone.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbNom.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cblivreur.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbImportIDL.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbMobile.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbMontantOrganisation.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbMontantLivreur.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbDateLivraison.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbDateLivraison.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbDistanceR.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbRestaurant.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbVille.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup13, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem54, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem57, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup15, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblPrixI, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblPrixI1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.layoutAddressID, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem49, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem58, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem53, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem33, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem50, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem52, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem56, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem15, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem16, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem36, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem66, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem59, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem34, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem67, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem70, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem61, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem31, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Root, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem38, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.navbarImageCollectionLarge4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.navbarImageCollectionLarge21, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.navbarImageCollectionLarge, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.navbarImageCollectionLarge2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.navbarImageCollectionLarge1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ribbonImageCollection, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DockManager1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.DockEdit.ResumeLayout(False)
        Me.DockPanel1_Container.ResumeLayout(False)
        CType(Me.LayoutControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LayoutControl2.ResumeLayout(False)
        CType(Me.tbImportID.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbDistance.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem43, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutParNombreKmCL, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem44, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem45, System.ComponentModel.ISupportInitialize).EndInit()
        Me.DockResidence.ResumeLayout(False)
        Me.ControlContainer1.ResumeLayout(False)
        CType(Me.LayoutControl6, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LayoutControl6.ResumeLayout(False)
        CType(Me.cbResidence.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem41, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem55, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem60, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem62, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup11, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents RibbonControl As DevExpress.XtraBars.Ribbon.RibbonControl
    Friend WithEvents RibbonPage1 As DevExpress.XtraBars.Ribbon.RibbonPage
    Friend WithEvents RibbonPageGroup1 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents RibbonStatusBar As DevExpress.XtraBars.Ribbon.RibbonStatusBar
    Friend WithEvents RibbonPage2 As DevExpress.XtraBars.Ribbon.RibbonPage
    Friend WithEvents RibbonPageGroup2 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents RibbonPageGroup3 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents BarButtonItem1 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents bOrganisations As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem3 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents bMiseaJour As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem5 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents bFermer As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents bLivreurs As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents RibbonPageGroup4 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents RibbonPageGroup5 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents bFermer2 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents SkinRibbonGalleryBarItem1 As DevExpress.XtraBars.SkinRibbonGalleryBarItem
    Friend WithEvents rgbiSkins2 As DevExpress.XtraBars.SkinRibbonGalleryBarItem
    Friend WithEvents LayoutControl1 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents Root As DevExpress.XtraLayout.LayoutControlGroup
    Private WithEvents navbarImageCollectionLarge As DevExpress.Utils.ImageCollection
    Private WithEvents ribbonImageCollection As DevExpress.Utils.ImageCollection
    Friend WithEvents bSettings As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents navbarImageCollectionLarge1 As DevExpress.Utils.ImageCollection
    Friend WithEvents navbarImageCollectionLarge2 As DevExpress.Utils.ImageCollection
    Friend WithEvents FolderBrowserDialog1 As FolderBrowserDialog
    Friend WithEvents bFactures As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents bPayes As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents tabMain As DevExpress.XtraTab.XtraTabControl
    Friend WithEvents XtraTabPage1 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents XtraTabPage2 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents LayoutControl4 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents grdImport As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colImportID As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colReference As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colClient As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemMemoEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit
    Friend WithEvents colClientAddress As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colMomentDePassage As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colPreparePar As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colHeureAppel As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colHeurePrep As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colHeureDepart As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colHeureLivraison As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colMessagePassage As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemMemoEdit3 As DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit
    Friend WithEvents colTotalTempService As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colLivreur As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents Distance As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colMontant As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colOrganisation As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemMemoEdit2 As DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit
    Friend WithEvents colDebit As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCredit As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colGlacier As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colMontantGlacierLivreur As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colMontantGlacierOrganisation As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colMontantLivreur As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents Extras As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colExtraKM As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemCalcEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit
    Friend WithEvents RepositoryItemTextEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents RepositoryItemTextEdit2 As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents RepositoryItemCalcEdit2 As DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit
    Friend WithEvents RepositoryItemSpinEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit
    Friend WithEvents RepositoryItemCheckEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
    Friend WithEvents LayoutControlGroup7 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem4 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControl3 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents LayoutControlGroup6 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem38 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents txtMobilusXLS As DevExpress.XtraEditors.TextEdit
    Friend WithEvents DateEdit1 As DevExpress.XtraEditors.DateEdit
    Friend WithEvents bImportMobilus As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents navbarImageCollectionLarge3 As DevExpress.Utils.ImageCollection
    Friend WithEvents bSelectfile As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents bSupprimerI As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cbFichierImport As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents bAnnulerFichierImport As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LayoutBas As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem1 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem10 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem17 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem2 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem26 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem22 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem23 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents tbDateFrom As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LayoutControlItem5 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents tbDateTo As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LayoutControlItem6 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents bFiltrerDate As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents navbarImageCollectionLarge21 As DevExpress.Utils.ImageCollection
    Friend WithEvents navbarImageCollectionLarge4 As DevExpress.Utils.ImageCollection
    Friend WithEvents cbPeriodes As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents bAnnulerPayes As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents bAnnulerFactures As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LayoutControlItem14 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem25 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem24 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlGroup1 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem9 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents bExportToHTML As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents bExportToExcel As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents bExportToPDF As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents bExportToDoc As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LayoutControlItem7 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem11 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem12 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem18 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents ProgressBarControl1 As DevExpress.XtraEditors.ProgressBarControl
    Friend WithEvents LayoutControlItem3 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents Calendar As DevExpress.XtraEditors.Controls.CalendarControl
    Friend WithEvents LayoutControlItem28 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents bPrintAll As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem4 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents cbPayes As DevExpress.XtraEditors.CheckedComboBoxEdit
    Friend WithEvents LayoutPayes As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents cbFactures As DevExpress.XtraEditors.CheckedComboBoxEdit
    Friend WithEvents LayoutFactures As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents bConsolidation As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem2 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem6 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents GridColumn3 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridIDPeriode As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents bVendeurs As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents bConsolidationDate As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LayoutControlItem27 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents bResetDate As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LayoutControlItem32 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents bAchat As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents bVente As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarSubItem1 As DevExpress.XtraBars.BarSubItem
    Friend WithEvents BarButtonItem7 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents bEtats As DevExpress.XtraBars.BarSubItem
    Friend WithEvents bEtatDesComptes As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents bEtatDesVentes As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents bEtatDesAchats As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarSubItem3 As DevExpress.XtraBars.BarSubItem
    Friend WithEvents BarSubItem4 As DevExpress.XtraBars.BarSubItem
    Friend WithEvents BarSubItem2 As DevExpress.XtraBars.BarSubItem
    Friend WithEvents BarSubItem5 As DevExpress.XtraBars.BarSubItem
    Friend WithEvents BarButtonItem8 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents bVentes As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents bAchats As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarSubItem6 As DevExpress.XtraBars.BarSubItem
    Friend WithEvents cbEtatsDeComptes As DevExpress.XtraEditors.CheckedComboBoxEdit
    Friend WithEvents LayoutEtat As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents cbYears As DevExpress.XtraBars.BarEditItem
    Friend WithEvents RepositoryItemLookUpEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
    Friend WithEvents cbAnnee As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LayoutControlItem40 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents bResetEtatdeCompte As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LayoutControlItem42 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents DockManager1 As DevExpress.XtraBars.Docking.DockManager
    Friend WithEvents DockEdit As DevExpress.XtraBars.Docking.DockPanel
    Friend WithEvents DockPanel1_Container As DevExpress.XtraBars.Docking.ControlContainer
    Friend WithEvents LayoutControl2 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents LayoutControlGroup4 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents tbImportID As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlItem43 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents tbDistance As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutParNombreKmCL As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents bSauvegarde As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LayoutControlItem44 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents btnAnnuler As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LayoutControlItem45 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutDroite As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlGroup8 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents EmptySpaceItem6 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents EmptySpaceItem7 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents bResetPeriode As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LayoutControlItem13 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents XtraTabPage3 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents LayoutControl5 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents LayoutControlGroup9 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlGroup10 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlGroup11 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents tbCustomerAddressID As DevExpress.XtraEditors.TextEdit
    Friend WithEvents layoutAddressID As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents tbAdresseNo As DevExpress.XtraEditors.TextEdit
    Friend WithEvents tbAdresse As DevExpress.XtraEditors.TextEdit
    Friend WithEvents tbCodePostale As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlItem49 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem50 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem52 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents tbAppartement As DevExpress.XtraEditors.TextEdit
    Friend WithEvents tbCodeEntree As DevExpress.XtraEditors.TextEdit
    Friend WithEvents tbTelephone As DevExpress.XtraEditors.TextEdit
    Friend WithEvents tbNom As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlItem54 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem56 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem57 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem58 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlGroup13 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents cblivreur As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents tbImportIDL As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlItem66 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlGroup15 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem59 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents bReturnToAction As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents bAddToImport As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LayoutControlItem67 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem70 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents bAjouterUneCommande As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents RibbonPageGroup6 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents tbMobile As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlItem15 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents bSupprimerAddresse As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LayoutControlItem53 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents tbMontantOrganisation As DevExpress.XtraEditors.TextEdit
    Friend WithEvents tbMontantLivreur As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblPrixI As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents lblPrixI1 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents IsMobilus As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents IDClientAddresse As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents bImportMiseaJour As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LayoutControlItem61 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents bImprimerFactures As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents bImprimerCommission As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents bImprimerCommissionsVendeurs As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnGenererFacturation As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents bGenererPaye As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnGenererFacturationVendeurs2 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents bLivraisonParRestaurant As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents bLivraisonsParLivreurs As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem17 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents RibbonPageGroup7 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents RibbonPageGroup8 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents chkPRINT As DevExpress.XtraBars.BarCheckItem
    Friend WithEvents tbDateLivraison As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LayoutControlItem8 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents tbDistanceR As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlItem16 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents RibbonPageGroup9 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
    Friend WithEvents optFiltreGrille As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents LayoutControlItem29 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents bRemoveFilter As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LayoutControlItem33 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents bGenererLaPaye As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem9 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents bImprimerCommissions As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem10 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents bImprimerCommisssions As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents bGenererLesCommission As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents cbLivraisonRestaurants As DevExpress.XtraEditors.CheckedComboBoxEdit
    Friend WithEvents LayoutEtatLivraisonRestaurant As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents bResetLivraisonsRestaurant As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LayoutControlItem69 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents optTypePeriode As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents LayoutControlItem48 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlGroup3 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents bPrintLivraisonR As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents bPrintEtat As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents bPrintPaye As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents bPrintFacture As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LayoutControlItem19 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem21 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem35 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem47 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents bEnvoyerLivraisonsRestaurants As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents bEnvoyerEtatCompte As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents bEnvoyerPaye As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents bEnvoyerFacture As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LayoutControlItem64 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem72 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem74 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem75 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents cbRestaurant As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LayoutControlItem34 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents cbVille As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LayoutControlItem36 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem1 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents bLivraisonParLivreur As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents cbCommissions As DevExpress.XtraEditors.CheckedComboBoxEdit
    Friend WithEvents LayoutCommissions As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents bAnnulerCommissions As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents bPrintCommissions As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents bEnvoyerCommissionVendeur As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LayoutControlItem37 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem46 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem51 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents bCommisssion As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents bFournisseurs As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents bImprimerCommissionRapport As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents bSupprimerLivraisons As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LayoutControlItem30 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents bPrêts As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents grdClientsAddresse As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView3 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn4 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn5 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn6 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn7 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn8 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn9 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents LayoutControlItem31 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents GridColumn10 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents bEpargnes As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents bResidences As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents colIDResidence As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colResidence As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents DockResidence As DevExpress.XtraBars.Docking.DockPanel
    Friend WithEvents ControlContainer1 As DevExpress.XtraBars.Docking.ControlContainer
    Friend WithEvents LayoutControl6 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents LayoutControlGroup2 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents bAnnulerResidence As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents bAppliquerResidence As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cbResidence As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents EmptySpaceItem2 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents LayoutControlItem41 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem55 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem60 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents bAffecterResidence As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LayoutControlItem39 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents bEffacerResidence As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LayoutControlItem62 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents bRapportTaxesLivreurs As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents bRapportTaxeLivreur As DevExpress.XtraBars.BarSubItem
    Friend WithEvents BarButtonItem11 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents bResetTaxationLivreur As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents bPrintTaxationLivreur As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents bEmailTaxationLivreur As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cbRapportTaxation As DevExpress.XtraEditors.CheckedComboBoxEdit
    Friend WithEvents LayoutCommissions1 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem63 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem65 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents bResetTaxationLivreurs As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents bCalendrier As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnRapportTaxeIDS As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents bResetTaxationLivreurPaye As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents bPrintTaxationLivreurPaye As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents bEmailTaxationLivreurPaye As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cbRapportRevenuPaye As DevExpress.XtraEditors.CheckedComboBoxEdit
    Friend WithEvents LayoutControlItem20 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem68 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem71 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem73 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents btnRapportTaxePayeLivreur As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents bLivraisonsParRestaurant As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents bLivraisonParRestaurant2 As DevExpress.XtraBars.BarButtonItem
End Class
