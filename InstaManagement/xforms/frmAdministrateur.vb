﻿Imports System.Data.OleDb
Imports System.Threading
Imports System.Globalization
Imports System.Data.SqlClient
Public Class frmAdministrateur


    Private oAdminData As New AdminData
    Private oAdmin As New Admin

    Private Sub frmAdministrateur_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load



        Dim dt As DataTable
        Dim dr As DataRow



        dt = oAdminData.SelectAll
        If dt.Rows.Count = 1 Then
            For Each dr In dt.Rows

                If Not IsDBNull(dr("LocationFacture")) Then
                    tbLocationFacture.Text = dr("LocationFacture")
                Else
                    tbLocationFacture.Text = Nothing
                End If

                If Not IsDBNull(dr("LocationPaie")) Then
                    tbLocationPaie.Text = dr("LocationPaie")
                Else
                    tbLocationPaie.Text = Nothing
                End If


                If Not IsDBNull(dr("LocationConsolidation")) Then
                    tbLocationConsolidation.Text = dr("LocationConsolidation")
                Else
                    tbLocationConsolidation.Text = Nothing
                End If

                If Not IsDBNull(dr("LocationVentes")) Then
                    tbLocationVentes.Text = dr("LocationVentes")
                Else
                    tbLocationVentes.Text = Nothing
                End If

                If Not IsDBNull(dr("LocationAchats")) Then
                    tbLocationAchats.Text = dr("LocationAchats")
                Else
                    tbLocationAchats.Text = Nothing
                End If
                If Not IsDBNull(dr("LocationCommissions")) Then
                    tbLocationCommission.Text = dr("LocationCommissions")
                Else
                    tbLocationCommission.Text = Nothing
                End If


                If Not IsDBNull(dr("LocationRapportRestaurant")) Then
                    tbLivraisonRestaurants.Text = dr("LocationRapportRestaurant")
                Else
                    tbLivraisonRestaurants.Text = Nothing
                End If

                If Not IsDBNull(dr("LocationRapportDeTaxation")) Then
                    tbRapportDeTaxation.Text = dr("LocationRapportDeTaxation")
                Else
                    tbRapportDeTaxation.Text = Nothing
                End If

                If Not IsDBNull(dr("LocationRapportDeTaxationIDS")) Then
                    tbRapportDeTaxationIDS.Text = dr("LocationRapportDeTaxationIDS")
                Else
                    tbRapportDeTaxationIDS.Text = Nothing
                End If

                If Not IsDBNull(dr("LocationRapportDeTaxationIDS")) Then
                    tbRapportTaxationLivreurPaye.Text = dr("RapportTaxationLivreurPaye")
                Else
                    tbRapportTaxationLivreurPaye.Text = Nothing
                End If




                If Not IsDBNull(dr("Server")) Then
                    tbServer.Text = dr("Server")
                Else
                    tbServer.Text = Nothing
                End If
                If Not IsDBNull(dr("Username")) Then
                    tbUsername.Text = dr("Username")
                Else
                    tbUsername.Text = Nothing
                End If

                If Not IsDBNull(dr("Password")) Then
                    tbPassword.Text = dr("Password")
                Else
                    tbPassword.Text = Nothing
                End If

                If Not IsDBNull(dr("Port")) Then
                    tbPort.Text = dr("Port")
                Else
                    tbPort.Text = Nothing
                End If


                If Not IsDBNull(dr("UseSSL")) Then
                    chkSSL.Checked = dr("UseSSL")
                Else
                    chkSSL.Checked = False
                End If

                If Not IsDBNull(dr("CompanyContactTelephone")) Then
                    tbCieTEl.Text = dr("CompanyContactTelephone")
                Else
                    tbCieTEl.Text = Nothing
                End If

                If Not IsDBNull(dr("CompanyAdresse")) Then
                    tbCieAdress.Text = dr("CompanyAdresse")
                Else
                    tbCieAdress.Text = Nothing
                End If


                If Not IsDBNull(dr("CompanyName")) Then
                    tbNomCompagnie.Text = dr("CompanyName")
                Else
                    tbNomCompagnie.Text = Nothing
                End If



                If Not IsDBNull(dr("TPS")) Then
                    tbTPS.Text = dr("TPS")
                Else
                    tbTPS.Text = Nothing
                End If

                If Not IsDBNull(dr("TVQ")) Then
                    tbTVQ.Text = dr("TVQ")
                Else
                    tbTVQ.Text = Nothing
                End If

                If Not IsDBNull(dr("TPSNumero")) Then
                    tbNoTPS.Text = dr("TPSNumero")
                Else
                    tbNoTPS.Text = Nothing
                End If


                If Not IsDBNull(dr("TVQNumero")) Then
                    tbNoTVQ.Text = dr("TVQNumero")
                Else
                    tbNoTVQ.Text = Nothing
                End If
            Next
        End If



    End Sub


    Private Sub txtTPS_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        Dim allowedChars As String = "1234567890."
        If allowedChars.IndexOf(e.KeyChar) = -1 AndAlso
                Not e.KeyChar = ChrW(8) Then
            ' Invalid Character
            e.Handled = True
        End If
    End Sub




    Private Sub txtTVQ_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        Dim allowedChars As String = "1234567890."
        If allowedChars.IndexOf(e.KeyChar) = -1 AndAlso
                Not e.KeyChar = ChrW(8) Then
            ' Invalid Character
            e.Handled = True
        End If
    End Sub



    Private Sub cmdExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.Close()
    End Sub



    Private Sub bLocationFacture_Click(sender As Object, e As EventArgs) Handles bLocationFacture.Click



        If (FolderBrowserDialog1.ShowDialog() = DialogResult.OK) Then
            tbLocationFacture.Text = FolderBrowserDialog1.SelectedPath

            'Dim dt As New DataTable
            'Dim dr As DataRow
            'Dim strSQL As String = ""
            'dt = oAdminData.SelectAll
            'If dt.Rows.Count = 1 Then
            '    For Each dr In dt.Rows
            '        strSQL = "UPDATE RapportFacturePaye  SET CheminRapport = REPLACE(CheminRapport,'" & dr("LocationFacture").ToString.Trim & "','" & tbLocationFacture.Text.ToString.Trim & "')"
            '        SysTemGlobals.ExecuteSQL(strSQL)
            '    Next


            'End If



        Else
            tbLocationFacture.Text = vbNullString
        End If

    End Sub


    Private Sub bAnnuler_Click(sender As Object, e As EventArgs) Handles bAnnuler.Click
        Me.Close()
    End Sub

    Private Sub bSauvegarde_Click(sender As Object, e As EventArgs) Handles bSauvegarde.Click
        If MsgBox("Sauvegarder la fiche?", MsgBoxStyle.YesNo + MsgBoxStyle.Information, "Exit") = MsgBoxResult.Yes Then


            If tbLocationFacture.Text.ToString.Trim <> "" Then
                oAdmin.LocationFacture = tbLocationFacture.Text
                go_Globals.LocationFacture = tbLocationFacture.Text

            End If

            If tbLocationPaie.Text.ToString.Trim <> "" Then
                oAdmin.LocationPaie = tbLocationPaie.Text
                go_Globals.LocationPaie = tbLocationPaie.Text
            End If


            If tbLocationConsolidation.Text.ToString.Trim <> "" Then
                oAdmin.LocationConsolidation = tbLocationConsolidation.Text
                go_Globals.LocationConsolidation = tbLocationConsolidation.Text
            End If

            If tbLocationVentes.Text.ToString.Trim <> "" Then
                oAdmin.LocationVentes = tbLocationVentes.Text
                go_Globals.LocationVentes = tbLocationVentes.Text
            End If

            If tbLocationAchats.Text.ToString.Trim <> "" Then
                oAdmin.LocationAchats = tbLocationAchats.Text
                go_Globals.LocationAchats = tbLocationAchats.Text
            End If
            If tbLocationCommission.Text.ToString.Trim <> "" Then
                oAdmin.LocationCommissions = tbLocationCommission.Text
                go_Globals.LocationCommissions = tbLocationCommission.Text
            End If

            If tbRapportDeTaxation.Text.ToString.Trim <> "" Then
                oAdmin.LocationRapportDeTaxation = tbRapportDeTaxation.Text
                go_Globals.LocationRapportDeTaxation = tbRapportDeTaxation.Text
            End If

            If tbRapportDeTaxationIDS.Text.ToString.Trim <> "" Then
                oAdmin.LocationRapportDeTaxationIDS = tbRapportDeTaxationIDS.Text
                go_Globals.LocationRapportDeTaxationIDS = tbRapportDeTaxationIDS.Text
            End If

            If tbRapportTaxationLivreurPaye.Text.ToString.Trim <> "" Then
                oAdmin.RapportTaxationLivreurPaye = tbRapportTaxationLivreurPaye.Text
                go_Globals.RapportTaxationLivreurPaye = tbRapportTaxationLivreurPaye.Text
            End If


            If tbLivraisonRestaurants.Text.ToString.Trim <> "" Then
                oAdmin.LocationRapportRestaurant = tbLivraisonRestaurants.Text
                go_Globals.LocationRapportRestaurant = tbLivraisonRestaurants.Text
            End If


            If tbServer.Text.ToString.Trim <> "" Then
                oAdmin.Server = tbServer.Text
                go_Globals.Server = tbServer.Text
            End If

            If tbUsername.Text.ToString.Trim <> "" Then
                oAdmin.Username = tbUsername.Text
                go_Globals.Username = tbUsername.Text
            End If

            If tbPassword.Text.ToString.Trim <> "" Then
                oAdmin.Password = tbPassword.Text
                go_Globals.Password = tbPassword.Text
            End If

            If tbPort.Text.ToString.Trim <> "" Then
                oAdmin.Port = tbPort.Text
                go_Globals.Port = tbPort.Text
            End If



            oAdmin.UseSSL = chkSSL.Checked


            If tbNomCompagnie.Text.ToString.Trim <> "" Then
                oAdmin.CompanyName = tbNomCompagnie.Text

            End If

            If tbCieTEl.Text.ToString.Trim <> "" Then
                oAdmin.CompanyContactTelephone = tbCieTEl.Text

            End If



            If tbCieAdress.Text.ToString.Trim <> "" Then
                oAdmin.CompanyAdresse = tbCieAdress.Text

            End If


            If tbTPS.Text.ToString.Trim <> "" Then
                oAdmin.TPS = tbTPS.Text

            End If

            If tbTVQ.Text.ToString.Trim <> "" Then
                oAdmin.TVQ = tbTVQ.Text

            End If

            If tbNoTPS.Text.ToString.Trim <> "" Then
                oAdmin.TPSNumero = tbNoTPS.Text

            End If


            If tbNoTVQ.Text.ToString.Trim <> "" Then
                oAdmin.TVQNumero = tbNoTVQ.Text

            End If

            If oAdminData.Update(oAdmin, oAdmin) = True Then
                MessageBox.Show("Fiche sauvegardée")
            Else
                MessageBox.Show("Un problême est survenu lors de la sauvegarde")

            End If
            Me.Close()



        Else
            Exit Sub
        End If
    End Sub

    Private Sub bLocationPaye_Click(sender As Object, e As EventArgs) Handles bLocationPaye.Click
        If (FolderBrowserDialog1.ShowDialog() = DialogResult.OK) Then
            tbLocationPaie.Text = FolderBrowserDialog1.SelectedPath

            'Dim dt As New DataTable
            'Dim dr As DataRow
            'Dim strSQL As String = ""
            'dt = oAdminData.SelectAll
            'If dt.Rows.Count = 1 Then
            '    For Each dr In dt.Rows
            '        strSQL = "UPDATE RapportFacturePaye  SET CheminRapport = REPLACE(CheminRapport,'" & dr("LocationPaie").ToString.Trim & "','" & tbLocationPaie.Text.ToString.Trim & "')"
            '        SysTemGlobals.ExecuteSQL(strSQL)
            '    Next


            'End If


        Else
            tbLocationPaie.Text = vbNullString
        End If
    End Sub

    Private Sub tbNoTVQ_EditValueChanged(sender As Object, e As EventArgs) Handles tbNoTVQ.EditValueChanged

    End Sub

    Private Sub tbTPS_KeyPress(sender As Object, e As KeyPressEventArgs) Handles tbTPS.KeyPress
        If e.KeyChar = "." Or e.KeyChar = "," Or e.KeyChar = Convert.ToChar(Keys.Back) Then
            e.Handled = e.KeyChar = ","
        Else
            e.Handled = Not (Char.IsDigit(e.KeyChar))
        End If
    End Sub

    Private Sub tbTVQ_KeyPress(sender As Object, e As KeyPressEventArgs) Handles tbTVQ.KeyPress
        If e.KeyChar = "." Or e.KeyChar = "," Or e.KeyChar = Convert.ToChar(Keys.Back) Then
            e.Handled = e.KeyChar = ","
        Else
            e.Handled = Not (Char.IsDigit(e.KeyChar))
        End If
    End Sub

    Private Sub bLocationConsolidation_Click(sender As Object, e As EventArgs) Handles bLocationConsolidation.Click
        If (FolderBrowserDialog1.ShowDialog() = DialogResult.OK) Then
            tbLocationConsolidation.Text = FolderBrowserDialog1.SelectedPath

            'Dim dt As New DataTable
            'Dim dr As DataRow
            'Dim strSQL As String = ""
            'dt = oAdminData.SelectAll
            'If dt.Rows.Count = 1 Then
            '    For Each dr In dt.Rows
            '        strSQL = "UPDATE Etats  SET CheminRapport = REPLACE(CheminRapport,'" & dr("LocationConsolidation").ToString.Trim & "','" & tbLocationConsolidation.Text.ToString.Trim & "')"
            '        SysTemGlobals.ExecuteSQL(strSQL)
            '    Next
            'End If



        Else
            tbLocationConsolidation.Text = vbNullString
        End If
    End Sub

    Private Sub bLocationVentes_Click(sender As Object, e As EventArgs) Handles bLocationVentes.Click
        If (FolderBrowserDialog1.ShowDialog() = DialogResult.OK) Then
            tbLocationVentes.Text = FolderBrowserDialog1.SelectedPath

            'Dim dt As New DataTable
            'Dim dr As DataRow
            'Dim strSQL As String = ""
            'dt = oAdminData.SelectAll
            'If dt.Rows.Count = 1 Then
            '    For Each dr In dt.Rows
            '        strSQL = "UPDATE EtatsVentes  SET CheminRapport = REPLACE(CheminRapport,'" & dr("LocationVentes").ToString.Trim & "','" & tbLocationVentes.Text.ToString.Trim & "')"
            '        SysTemGlobals.ExecuteSQL(strSQL)
            '    Next
            'End If


        Else
            tbLocationVentes.Text = vbNullString
        End If
    End Sub

    Private Sub bLocationAchats_Click(sender As Object, e As EventArgs) Handles bLocationAchats.Click
        If (FolderBrowserDialog1.ShowDialog() = DialogResult.OK) Then
            tbLocationAchats.Text = FolderBrowserDialog1.SelectedPath

            'Dim dt As New DataTable
            'Dim dr As DataRow
            'Dim strSQL As String = ""
            'dt = oAdminData.SelectAll
            'If dt.Rows.Count = 1 Then
            '    For Each dr In dt.Rows
            '        strSQL = "UPDATE EtatsAchats  SET CheminRapport = REPLACE(CheminRapport,'" & dr("LocationAchats").ToString.Trim & "','" & tbLocationAchats.Text.ToString.Trim & "')"
            '        SysTemGlobals.ExecuteSQL(strSQL)
            '    Next
            'End If


        Else
            tbLocationAchats.Text = vbNullString
        End If
    End Sub

    Private Sub bLocationCommissions_Click(sender As Object, e As EventArgs) Handles bLocationCommissions.Click
        If (FolderBrowserDialog1.ShowDialog() = DialogResult.OK) Then
            tbLocationCommission.Text = FolderBrowserDialog1.SelectedPath

            'Dim dt As New DataTable
            'Dim dr As DataRow
            'Dim strSQL As String = ""
            'dt = oAdminData.SelectAll
            'If dt.Rows.Count = 1 Then
            '    For Each dr In dt.Rows
            '        strSQL = "UPDATE RapportCommission  SET CheminRapport = REPLACE(CheminRapport,'" & dr("LocationCommissions").ToString.Trim & "','" & tbLocationCommission.Text.ToString.Trim & "')"
            '        SysTemGlobals.ExecuteSQL(strSQL)
            '    Next
            'End If


        Else
            tbLocationCommission.Text = vbNullString
        End If
    End Sub


    Private Sub bLivraisonRestaurants_Click(sender As Object, e As EventArgs) Handles bLivraisonRestaurants.Click
        If (FolderBrowserDialog1.ShowDialog() = DialogResult.OK) Then
            tbLivraisonRestaurants.Text = FolderBrowserDialog1.SelectedPath

            'Dim dt As New DataTable
            'Dim dr As DataRow
            'Dim strSQL As String = ""
            'dt = oAdminData.SelectAll
            'If dt.Rows.Count = 1 Then
            '    For Each dr In dt.Rows
            '        strSQL = "UPDATE RapportLivraison  SET CheminRapport = REPLACE(CheminRapport,'" & dr("LocationRapportRestaurant").ToString.Trim & "','" & tbLivraisonRestaurants.Text.ToString.Trim & "')"
            '        SysTemGlobals.ExecuteSQL(strSQL)
            '    Next
            'End If


        Else
            tbLivraisonRestaurants.Text = vbNullString
        End If
    End Sub

    Private Sub tbRapportDeTaxation_EditValueChanged(sender As Object, e As EventArgs) Handles tbRapportDeTaxation.EditValueChanged

    End Sub

    Private Sub tbRapportTaxation_Click(sender As Object, e As EventArgs) Handles tbRapportTaxation.Click
        If (FolderBrowserDialog1.ShowDialog() = DialogResult.OK) Then

            tbRapportDeTaxation.Text = FolderBrowserDialog1.SelectedPath

            'Dim dt As New DataTable
            'Dim dr As DataRow
            'Dim strSQL As String = ""
            'dt = oAdminData.SelectAll
            'If dt.Rows.Count = 1 Then
            '    For Each dr In dt.Rows
            '        strSQL = "UPDATE RapportPayeTaxe SET CheminRapport = REPLACE(CheminRapport,'" & dr("LocationRapportRestaurant").ToString.Trim & "','" & tbRapportDeTaxation.Text.ToString.Trim & "')"
            '        SysTemGlobals.ExecuteSQL(strSQL)
            '    Next
            'End If


        Else
            tbLivraisonRestaurants.Text = vbNullString
        End If
    End Sub

    Private Sub tbRapportTaxationLivreurPaye_Click(sender As Object, e As EventArgs) Handles btRapportTaxationLivreurPaye.Click
        If (FolderBrowserDialog1.ShowDialog() = DialogResult.OK) Then
            tbRapportTaxationLivreurPaye.Text = FolderBrowserDialog1.SelectedPath

            Dim dt As New DataTable
            Dim dr As DataRow
            Dim strSQL As String = ""
            dt = oAdminData.SelectAll
            If dt.Rows.Count = 1 Then
                For Each dr In dt.Rows
                    strSQL = "UPDATE RapportFacturePaye  SET CheminRapport = REPLACE(CheminRapport,'" & dr("LocationFacture").ToString.Trim & "','" & tbLocationFacture.Text.ToString.Trim & "')"
                    SysTemGlobals.ExecuteSQL(strSQL)
                Next


            End If



        Else
            tbRapportTaxationLivreurPaye.Text = vbNullString
        End If
    End Sub

    Private Sub btRapportdetaxationIDS_Click(sender As Object, e As EventArgs) Handles btRapportdetaxationIDS.Click
        If (FolderBrowserDialog1.ShowDialog() = DialogResult.OK) Then
            tbRapportDeTaxationIDS.Text = FolderBrowserDialog1.SelectedPath

            'Dim dt As New DataTable
            'Dim dr As DataRow
            'Dim strSQL As String = ""
            'dt = oAdminData.SelectAll
            'If dt.Rows.Count = 1 Then
            '    For Each dr In dt.Rows
            '        strSQL = "UPDATE RapportFacturePaye  SET CheminRapport = REPLACE(CheminRapport,'" & dr("LocationFacture").ToString.Trim & "','" & tbLocationFacture.Text.ToString.Trim & "')"
            '        SysTemGlobals.ExecuteSQL(strSQL)
            '    Next


            'End If



        Else
            tbRapportDeTaxationIDS.Text = vbNullString
        End If
    End Sub
End Class