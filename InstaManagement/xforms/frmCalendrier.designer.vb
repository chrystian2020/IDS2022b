﻿Namespace dxSampleScheduler
    Partial Class frmCalendrier
        ''' <summary>
        ''' Required designer variable.
        ''' </summary>
        Private components As ComponentModel.IContainer = Nothing

        ''' <summary>
        ''' Clean up any resources being used.
        ''' </summary>
        ''' <paramname="disposing">true if managed resources should be disposed; otherwise, false.</param>
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If

            MyBase.Dispose(disposing)
        End Sub

#Region "Windows Form Designer generated code"

        ''' <summary>
        ''' Required method for Designer support - do not modify
        ''' the contents of this method with the code editor.
        ''' </summary>
        Private Sub InitializeComponent()
            Me.components = New System.ComponentModel.Container()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmCalendrier))
            Dim TimeRuler1 As DevExpress.XtraScheduler.TimeRuler = New DevExpress.XtraScheduler.TimeRuler()
            Dim TimeRuler2 As DevExpress.XtraScheduler.TimeRuler = New DevExpress.XtraScheduler.TimeRuler()
            Dim TimeRuler3 As DevExpress.XtraScheduler.TimeRuler = New DevExpress.XtraScheduler.TimeRuler()
            Me.schedulerStorage1 = New DevExpress.XtraScheduler.SchedulerStorage(Me.components)
            Me.AppointmentsBindingSource = New System.Windows.Forms.BindingSource(Me.components)
            Me.IDScomptabiliteDataSet = New IDScomptabilite.IDScomptabiliteDataSet()
            Me.IDScomptabiliteDataSetBindingSource = New System.Windows.Forms.BindingSource(Me.components)
            Me.layoutControl1 = New DevExpress.XtraLayout.LayoutControl()
            Me.dateNavigator1 = New DevExpress.XtraScheduler.DateNavigator()
            Me.schedulerControl1 = New DevExpress.XtraScheduler.SchedulerControl()
            Me.ribbonControl1 = New DevExpress.XtraBars.Ribbon.RibbonControl()
            Me.openScheduleItem1 = New DevExpress.XtraScheduler.UI.OpenScheduleItem()
            Me.saveScheduleItem1 = New DevExpress.XtraScheduler.UI.SaveScheduleItem()
            Me.printPreviewItem1 = New DevExpress.XtraScheduler.UI.PrintPreviewItem()
            Me.printItem1 = New DevExpress.XtraScheduler.UI.PrintItem()
            Me.printPageSetupItem1 = New DevExpress.XtraScheduler.UI.PrintPageSetupItem()
            Me.newAppointmentItem1 = New DevExpress.XtraScheduler.UI.NewAppointmentItem()
            Me.newRecurringAppointmentItem1 = New DevExpress.XtraScheduler.UI.NewRecurringAppointmentItem()
            Me.navigateViewBackwardItem1 = New DevExpress.XtraScheduler.UI.NavigateViewBackwardItem()
            Me.navigateViewForwardItem1 = New DevExpress.XtraScheduler.UI.NavigateViewForwardItem()
            Me.gotoTodayItem1 = New DevExpress.XtraScheduler.UI.GotoTodayItem()
            Me.viewZoomInItem1 = New DevExpress.XtraScheduler.UI.ViewZoomInItem()
            Me.viewZoomOutItem1 = New DevExpress.XtraScheduler.UI.ViewZoomOutItem()
            Me.switchToDayViewItem1 = New DevExpress.XtraScheduler.UI.SwitchToDayViewItem()
            Me.switchToWorkWeekViewItem1 = New DevExpress.XtraScheduler.UI.SwitchToWorkWeekViewItem()
            Me.switchToWeekViewItem1 = New DevExpress.XtraScheduler.UI.SwitchToWeekViewItem()
            Me.switchToFullWeekViewItem1 = New DevExpress.XtraScheduler.UI.SwitchToFullWeekViewItem()
            Me.switchToMonthViewItem1 = New DevExpress.XtraScheduler.UI.SwitchToMonthViewItem()
            Me.switchToTimelineViewItem1 = New DevExpress.XtraScheduler.UI.SwitchToTimelineViewItem()
            Me.switchToGanttViewItem1 = New DevExpress.XtraScheduler.UI.SwitchToGanttViewItem()
            Me.switchToAgendaViewItem1 = New DevExpress.XtraScheduler.UI.SwitchToAgendaViewItem()
            Me.groupByNoneItem1 = New DevExpress.XtraScheduler.UI.GroupByNoneItem()
            Me.groupByDateItem1 = New DevExpress.XtraScheduler.UI.GroupByDateItem()
            Me.groupByResourceItem1 = New DevExpress.XtraScheduler.UI.GroupByResourceItem()
            Me.switchTimeScalesItem1 = New DevExpress.XtraScheduler.UI.SwitchTimeScalesItem()
            Me.changeScaleWidthItem1 = New DevExpress.XtraScheduler.UI.ChangeScaleWidthItem()
            Me.repositoryItemSpinEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit()
            Me.switchTimeScalesCaptionItem1 = New DevExpress.XtraScheduler.UI.SwitchTimeScalesCaptionItem()
            Me.switchCompressWeekendItem1 = New DevExpress.XtraScheduler.UI.SwitchCompressWeekendItem()
            Me.switchShowWorkTimeOnlyItem1 = New DevExpress.XtraScheduler.UI.SwitchShowWorkTimeOnlyItem()
            Me.switchCellsAutoHeightItem1 = New DevExpress.XtraScheduler.UI.SwitchCellsAutoHeightItem()
            Me.changeSnapToCellsUIItem1 = New DevExpress.XtraScheduler.UI.ChangeSnapToCellsUIItem()
            Me.editAppointmentQueryItem1 = New DevExpress.XtraScheduler.UI.EditAppointmentQueryItem()
            Me.editOccurrenceUICommandItem1 = New DevExpress.XtraScheduler.UI.EditOccurrenceUICommandItem()
            Me.editSeriesUICommandItem1 = New DevExpress.XtraScheduler.UI.EditSeriesUICommandItem()
            Me.deleteAppointmentsItem1 = New DevExpress.XtraScheduler.UI.DeleteAppointmentsItem()
            Me.deleteOccurrenceItem1 = New DevExpress.XtraScheduler.UI.DeleteOccurrenceItem()
            Me.deleteSeriesItem1 = New DevExpress.XtraScheduler.UI.DeleteSeriesItem()
            Me.splitAppointmentItem1 = New DevExpress.XtraScheduler.UI.SplitAppointmentItem()
            Me.changeAppointmentStatusItem1 = New DevExpress.XtraScheduler.UI.ChangeAppointmentStatusItem()
            Me.changeAppointmentLabelItem1 = New DevExpress.XtraScheduler.UI.ChangeAppointmentLabelItem()
            Me.toggleRecurrenceItem1 = New DevExpress.XtraScheduler.UI.ToggleRecurrenceItem()
            Me.changeAppointmentReminderItem1 = New DevExpress.XtraScheduler.UI.ChangeAppointmentReminderItem()
            Me.repositoryItemDuration1 = New DevExpress.XtraScheduler.UI.RepositoryItemDuration()
            Me.bExit = New DevExpress.XtraBars.BarButtonItem()
            Me.calendarToolsRibbonPageCategory1 = New DevExpress.XtraScheduler.UI.CalendarToolsRibbonPageCategory()
            Me.appointmentRibbonPage1 = New DevExpress.XtraScheduler.UI.AppointmentRibbonPage()
            Me.actionsRibbonPageGroup1 = New DevExpress.XtraScheduler.UI.ActionsRibbonPageGroup()
            Me.optionsRibbonPageGroup1 = New DevExpress.XtraScheduler.UI.OptionsRibbonPageGroup()
            Me.fileRibbonPage1 = New DevExpress.XtraScheduler.UI.FileRibbonPage()
            Me.commonRibbonPageGroup1 = New DevExpress.XtraScheduler.UI.CommonRibbonPageGroup()
            Me.printRibbonPageGroup1 = New DevExpress.XtraScheduler.UI.PrintRibbonPageGroup()
            Me.homeRibbonPage1 = New DevExpress.XtraScheduler.UI.HomeRibbonPage()
            Me.appointmentRibbonPageGroup1 = New DevExpress.XtraScheduler.UI.AppointmentRibbonPageGroup()
            Me.navigatorRibbonPageGroup1 = New DevExpress.XtraScheduler.UI.NavigatorRibbonPageGroup()
            Me.arrangeRibbonPageGroup1 = New DevExpress.XtraScheduler.UI.ArrangeRibbonPageGroup()
            Me.groupByRibbonPageGroup1 = New DevExpress.XtraScheduler.UI.GroupByRibbonPageGroup()
            Me.viewRibbonPage1 = New DevExpress.XtraScheduler.UI.ViewRibbonPage()
            Me.activeViewRibbonPageGroup1 = New DevExpress.XtraScheduler.UI.ActiveViewRibbonPageGroup()
            Me.timeScaleRibbonPageGroup1 = New DevExpress.XtraScheduler.UI.TimeScaleRibbonPageGroup()
            Me.layoutRibbonPageGroup1 = New DevExpress.XtraScheduler.UI.LayoutRibbonPageGroup()
            Me.layoutControlGroup1 = New DevExpress.XtraLayout.LayoutControlGroup()
            Me.layoutControlItem1 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.layoutControlItem2 = New DevExpress.XtraLayout.LayoutControlItem()
            Me.splitterItem1 = New DevExpress.XtraLayout.SplitterItem()
            Me.schedulerBarController1 = New DevExpress.XtraScheduler.UI.SchedulerBarController(Me.components)
            Me.AppointmentsTableAdapter = New IDScomptabilite.IDScomptabiliteDataSetTableAdapters.AppointmentsTableAdapter()
            Me.ResourcesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
            Me.ResourcesTableAdapter = New IDScomptabilite.IDScomptabiliteDataSetTableAdapters.ResourcesTableAdapter()
            CType(Me.schedulerStorage1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.AppointmentsBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.IDScomptabiliteDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.IDScomptabiliteDataSetBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.layoutControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.layoutControl1.SuspendLayout()
            CType(Me.dateNavigator1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.dateNavigator1.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.schedulerControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.ribbonControl1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.repositoryItemSpinEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.repositoryItemDuration1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.layoutControlGroup1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.layoutControlItem1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.layoutControlItem2, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.splitterItem1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.schedulerBarController1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.ResourcesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'schedulerStorage1
            '
            Me.schedulerStorage1.Appointments.CustomFieldMappings.Add(New DevExpress.XtraScheduler.AppointmentCustomFieldMapping("CustomField1", "CustomField1"))
            Me.schedulerStorage1.Appointments.DataSource = Me.AppointmentsBindingSource
            Me.schedulerStorage1.Appointments.Mappings.AllDay = "AllDay"
            Me.schedulerStorage1.Appointments.Mappings.Description = "Description"
            Me.schedulerStorage1.Appointments.Mappings.End = "EndDate"
            Me.schedulerStorage1.Appointments.Mappings.Label = "Label"
            Me.schedulerStorage1.Appointments.Mappings.Location = "Location"
            Me.schedulerStorage1.Appointments.Mappings.RecurrenceInfo = "RecurrenceInfo"
            Me.schedulerStorage1.Appointments.Mappings.ReminderInfo = "ReminderInfo"
            Me.schedulerStorage1.Appointments.Mappings.ResourceId = "ResourceID"
            Me.schedulerStorage1.Appointments.Mappings.Start = "StartDate"
            Me.schedulerStorage1.Appointments.Mappings.Status = "Status"
            Me.schedulerStorage1.Appointments.Mappings.Subject = "Subject"
            Me.schedulerStorage1.Appointments.Mappings.Type = "Type"
            Me.schedulerStorage1.Resources.CustomFieldMappings.Add(New DevExpress.XtraScheduler.ResourceCustomFieldMapping("CustomField1", "CustomField1"))
            Me.schedulerStorage1.Resources.DataSource = Me.IDScomptabiliteDataSetBindingSource
            Me.schedulerStorage1.Resources.Mappings.Caption = "ResourceName"
            Me.schedulerStorage1.Resources.Mappings.Color = "Color"
            Me.schedulerStorage1.Resources.Mappings.Id = "ResourceID"
            Me.schedulerStorage1.Resources.Mappings.Image = "Image"
            Me.schedulerStorage1.Resources.Mappings.ParentId = "UniqueID"
            '
            'AppointmentsBindingSource
            '
            Me.AppointmentsBindingSource.DataMember = "Appointments"
            Me.AppointmentsBindingSource.DataSource = Me.IDScomptabiliteDataSet
            '
            'IDScomptabiliteDataSet
            '
            Me.IDScomptabiliteDataSet.DataSetName = "IDScomptabiliteDataSet"
            Me.IDScomptabiliteDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
            '
            'IDScomptabiliteDataSetBindingSource
            '
            Me.IDScomptabiliteDataSetBindingSource.DataSource = Me.IDScomptabiliteDataSet
            Me.IDScomptabiliteDataSetBindingSource.Position = 0
            '
            'layoutControl1
            '
            resources.ApplyResources(Me.layoutControl1, "layoutControl1")
            Me.layoutControl1.Controls.Add(Me.dateNavigator1)
            Me.layoutControl1.Controls.Add(Me.schedulerControl1)
            Me.layoutControl1.Name = "layoutControl1"
            Me.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = New System.Drawing.Rectangle(2692, 501, 450, 400)
            Me.layoutControl1.Root = Me.layoutControlGroup1
            '
            'dateNavigator1
            '
            resources.ApplyResources(Me.dateNavigator1, "dateNavigator1")
            Me.dateNavigator1.CalendarAppearance.DayCellSpecial.FontStyleDelta = CType(resources.GetObject("dateNavigator1.CalendarAppearance.DayCellSpecial.FontStyleDelta"), System.Drawing.FontStyle)
            Me.dateNavigator1.CalendarAppearance.DayCellSpecial.Options.UseFont = True
            Me.dateNavigator1.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(CType(resources.GetObject("dateNavigator1.CalendarTimeProperties.Buttons"), DevExpress.XtraEditors.Controls.ButtonPredefines))})
            Me.dateNavigator1.Cursor = System.Windows.Forms.Cursors.Default
            Me.dateNavigator1.DateTime = New Date(2020, 7, 5, 0, 0, 0, 0)
            Me.dateNavigator1.FirstDayOfWeek = System.DayOfWeek.Sunday
            Me.dateNavigator1.Name = "dateNavigator1"
            Me.dateNavigator1.SchedulerControl = Me.schedulerControl1
            Me.dateNavigator1.StyleController = Me.layoutControl1
            '
            'schedulerControl1
            '
            resources.ApplyResources(Me.schedulerControl1, "schedulerControl1")
            Me.schedulerControl1.ActiveViewType = DevExpress.XtraScheduler.SchedulerViewType.Month
            Me.schedulerControl1.DataStorage = Me.schedulerStorage1
            Me.schedulerControl1.MenuManager = Me.ribbonControl1
            Me.schedulerControl1.Name = "schedulerControl1"
            Me.schedulerControl1.Start = New Date(2020, 7, 5, 0, 0, 0, 0)
            resources.ApplyResources(TimeRuler1, "TimeRuler1")
            Me.schedulerControl1.Views.DayView.TimeRulers.Add(TimeRuler1)
            Me.schedulerControl1.Views.FullWeekView.Enabled = True
            resources.ApplyResources(TimeRuler2, "TimeRuler2")
            Me.schedulerControl1.Views.FullWeekView.TimeRulers.Add(TimeRuler2)
            Me.schedulerControl1.Views.WeekView.Enabled = False
            resources.ApplyResources(TimeRuler3, "TimeRuler3")
            Me.schedulerControl1.Views.WorkWeekView.TimeRulers.Add(TimeRuler3)
            Me.schedulerControl1.Views.YearView.UseOptimizedScrolling = False
            '
            'ribbonControl1
            '
            resources.ApplyResources(Me.ribbonControl1, "ribbonControl1")
            Me.ribbonControl1.ExpandCollapseItem.Id = 0
            Me.ribbonControl1.ExpandCollapseItem.ImageOptions.ImageIndex = CType(resources.GetObject("ribbonControl1.ExpandCollapseItem.ImageOptions.ImageIndex"), Integer)
            Me.ribbonControl1.ExpandCollapseItem.ImageOptions.LargeImageIndex = CType(resources.GetObject("ribbonControl1.ExpandCollapseItem.ImageOptions.LargeImageIndex"), Integer)
            Me.ribbonControl1.ExpandCollapseItem.ImageOptions.SvgImage = CType(resources.GetObject("ribbonControl1.ExpandCollapseItem.ImageOptions.SvgImage"), DevExpress.Utils.Svg.SvgImage)
            Me.ribbonControl1.ExpandCollapseItem.SearchTags = resources.GetString("ribbonControl1.ExpandCollapseItem.SearchTags")
            Me.ribbonControl1.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.ribbonControl1.ExpandCollapseItem, Me.ribbonControl1.SearchEditItem, Me.openScheduleItem1, Me.saveScheduleItem1, Me.printPreviewItem1, Me.printItem1, Me.printPageSetupItem1, Me.newAppointmentItem1, Me.newRecurringAppointmentItem1, Me.navigateViewBackwardItem1, Me.navigateViewForwardItem1, Me.gotoTodayItem1, Me.viewZoomInItem1, Me.viewZoomOutItem1, Me.switchToDayViewItem1, Me.switchToWorkWeekViewItem1, Me.switchToWeekViewItem1, Me.switchToFullWeekViewItem1, Me.switchToMonthViewItem1, Me.switchToTimelineViewItem1, Me.switchToGanttViewItem1, Me.switchToAgendaViewItem1, Me.groupByNoneItem1, Me.groupByDateItem1, Me.groupByResourceItem1, Me.switchTimeScalesItem1, Me.changeScaleWidthItem1, Me.switchTimeScalesCaptionItem1, Me.switchCompressWeekendItem1, Me.switchShowWorkTimeOnlyItem1, Me.switchCellsAutoHeightItem1, Me.changeSnapToCellsUIItem1, Me.editAppointmentQueryItem1, Me.editOccurrenceUICommandItem1, Me.editSeriesUICommandItem1, Me.deleteAppointmentsItem1, Me.deleteOccurrenceItem1, Me.deleteSeriesItem1, Me.splitAppointmentItem1, Me.changeAppointmentStatusItem1, Me.changeAppointmentLabelItem1, Me.toggleRecurrenceItem1, Me.changeAppointmentReminderItem1, Me.bExit})
            Me.ribbonControl1.MaxItemId = 43
            Me.ribbonControl1.Name = "ribbonControl1"
            Me.ribbonControl1.PageCategories.AddRange(New DevExpress.XtraBars.Ribbon.RibbonPageCategory() {Me.calendarToolsRibbonPageCategory1})
            Me.ribbonControl1.Pages.AddRange(New DevExpress.XtraBars.Ribbon.RibbonPage() {Me.fileRibbonPage1, Me.homeRibbonPage1, Me.viewRibbonPage1})
            Me.ribbonControl1.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.repositoryItemSpinEdit1, Me.repositoryItemDuration1})
            '
            'openScheduleItem1
            '
            resources.ApplyResources(Me.openScheduleItem1, "openScheduleItem1")
            Me.openScheduleItem1.Id = 1
            Me.openScheduleItem1.ImageOptions.ImageIndex = CType(resources.GetObject("openScheduleItem1.ImageOptions.ImageIndex"), Integer)
            Me.openScheduleItem1.ImageOptions.LargeImageIndex = CType(resources.GetObject("openScheduleItem1.ImageOptions.LargeImageIndex"), Integer)
            Me.openScheduleItem1.ImageOptions.SvgImage = CType(resources.GetObject("openScheduleItem1.ImageOptions.SvgImage"), DevExpress.Utils.Svg.SvgImage)
            Me.openScheduleItem1.Name = "openScheduleItem1"
            '
            'saveScheduleItem1
            '
            resources.ApplyResources(Me.saveScheduleItem1, "saveScheduleItem1")
            Me.saveScheduleItem1.Id = 2
            Me.saveScheduleItem1.ImageOptions.ImageIndex = CType(resources.GetObject("saveScheduleItem1.ImageOptions.ImageIndex"), Integer)
            Me.saveScheduleItem1.ImageOptions.LargeImageIndex = CType(resources.GetObject("saveScheduleItem1.ImageOptions.LargeImageIndex"), Integer)
            Me.saveScheduleItem1.ImageOptions.SvgImage = CType(resources.GetObject("saveScheduleItem1.ImageOptions.SvgImage"), DevExpress.Utils.Svg.SvgImage)
            Me.saveScheduleItem1.Name = "saveScheduleItem1"
            '
            'printPreviewItem1
            '
            resources.ApplyResources(Me.printPreviewItem1, "printPreviewItem1")
            Me.printPreviewItem1.Id = 3
            Me.printPreviewItem1.ImageOptions.ImageIndex = CType(resources.GetObject("printPreviewItem1.ImageOptions.ImageIndex"), Integer)
            Me.printPreviewItem1.ImageOptions.LargeImageIndex = CType(resources.GetObject("printPreviewItem1.ImageOptions.LargeImageIndex"), Integer)
            Me.printPreviewItem1.ImageOptions.SvgImage = CType(resources.GetObject("printPreviewItem1.ImageOptions.SvgImage"), DevExpress.Utils.Svg.SvgImage)
            Me.printPreviewItem1.Name = "printPreviewItem1"
            '
            'printItem1
            '
            resources.ApplyResources(Me.printItem1, "printItem1")
            Me.printItem1.Id = 4
            Me.printItem1.ImageOptions.ImageIndex = CType(resources.GetObject("printItem1.ImageOptions.ImageIndex"), Integer)
            Me.printItem1.ImageOptions.LargeImageIndex = CType(resources.GetObject("printItem1.ImageOptions.LargeImageIndex"), Integer)
            Me.printItem1.ImageOptions.SvgImage = CType(resources.GetObject("printItem1.ImageOptions.SvgImage"), DevExpress.Utils.Svg.SvgImage)
            Me.printItem1.Name = "printItem1"
            '
            'printPageSetupItem1
            '
            resources.ApplyResources(Me.printPageSetupItem1, "printPageSetupItem1")
            Me.printPageSetupItem1.Id = 5
            Me.printPageSetupItem1.ImageOptions.ImageIndex = CType(resources.GetObject("printPageSetupItem1.ImageOptions.ImageIndex"), Integer)
            Me.printPageSetupItem1.ImageOptions.LargeImageIndex = CType(resources.GetObject("printPageSetupItem1.ImageOptions.LargeImageIndex"), Integer)
            Me.printPageSetupItem1.ImageOptions.SvgImage = CType(resources.GetObject("printPageSetupItem1.ImageOptions.SvgImage"), DevExpress.Utils.Svg.SvgImage)
            Me.printPageSetupItem1.Name = "printPageSetupItem1"
            '
            'newAppointmentItem1
            '
            resources.ApplyResources(Me.newAppointmentItem1, "newAppointmentItem1")
            Me.newAppointmentItem1.Id = 6
            Me.newAppointmentItem1.ImageOptions.ImageIndex = CType(resources.GetObject("newAppointmentItem1.ImageOptions.ImageIndex"), Integer)
            Me.newAppointmentItem1.ImageOptions.LargeImageIndex = CType(resources.GetObject("newAppointmentItem1.ImageOptions.LargeImageIndex"), Integer)
            Me.newAppointmentItem1.ImageOptions.SvgImage = CType(resources.GetObject("newAppointmentItem1.ImageOptions.SvgImage"), DevExpress.Utils.Svg.SvgImage)
            Me.newAppointmentItem1.Name = "newAppointmentItem1"
            '
            'newRecurringAppointmentItem1
            '
            resources.ApplyResources(Me.newRecurringAppointmentItem1, "newRecurringAppointmentItem1")
            Me.newRecurringAppointmentItem1.Id = 7
            Me.newRecurringAppointmentItem1.ImageOptions.ImageIndex = CType(resources.GetObject("newRecurringAppointmentItem1.ImageOptions.ImageIndex"), Integer)
            Me.newRecurringAppointmentItem1.ImageOptions.LargeImageIndex = CType(resources.GetObject("newRecurringAppointmentItem1.ImageOptions.LargeImageIndex"), Integer)
            Me.newRecurringAppointmentItem1.ImageOptions.SvgImage = CType(resources.GetObject("newRecurringAppointmentItem1.ImageOptions.SvgImage"), DevExpress.Utils.Svg.SvgImage)
            Me.newRecurringAppointmentItem1.Name = "newRecurringAppointmentItem1"
            '
            'navigateViewBackwardItem1
            '
            resources.ApplyResources(Me.navigateViewBackwardItem1, "navigateViewBackwardItem1")
            Me.navigateViewBackwardItem1.Id = 8
            Me.navigateViewBackwardItem1.ImageOptions.ImageIndex = CType(resources.GetObject("navigateViewBackwardItem1.ImageOptions.ImageIndex"), Integer)
            Me.navigateViewBackwardItem1.ImageOptions.LargeImageIndex = CType(resources.GetObject("navigateViewBackwardItem1.ImageOptions.LargeImageIndex"), Integer)
            Me.navigateViewBackwardItem1.ImageOptions.SvgImage = CType(resources.GetObject("navigateViewBackwardItem1.ImageOptions.SvgImage"), DevExpress.Utils.Svg.SvgImage)
            Me.navigateViewBackwardItem1.Name = "navigateViewBackwardItem1"
            '
            'navigateViewForwardItem1
            '
            resources.ApplyResources(Me.navigateViewForwardItem1, "navigateViewForwardItem1")
            Me.navigateViewForwardItem1.Id = 9
            Me.navigateViewForwardItem1.ImageOptions.ImageIndex = CType(resources.GetObject("navigateViewForwardItem1.ImageOptions.ImageIndex"), Integer)
            Me.navigateViewForwardItem1.ImageOptions.LargeImageIndex = CType(resources.GetObject("navigateViewForwardItem1.ImageOptions.LargeImageIndex"), Integer)
            Me.navigateViewForwardItem1.ImageOptions.SvgImage = CType(resources.GetObject("navigateViewForwardItem1.ImageOptions.SvgImage"), DevExpress.Utils.Svg.SvgImage)
            Me.navigateViewForwardItem1.Name = "navigateViewForwardItem1"
            '
            'gotoTodayItem1
            '
            resources.ApplyResources(Me.gotoTodayItem1, "gotoTodayItem1")
            Me.gotoTodayItem1.Id = 10
            Me.gotoTodayItem1.ImageOptions.ImageIndex = CType(resources.GetObject("gotoTodayItem1.ImageOptions.ImageIndex"), Integer)
            Me.gotoTodayItem1.ImageOptions.LargeImageIndex = CType(resources.GetObject("gotoTodayItem1.ImageOptions.LargeImageIndex"), Integer)
            Me.gotoTodayItem1.ImageOptions.SvgImage = CType(resources.GetObject("gotoTodayItem1.ImageOptions.SvgImage"), DevExpress.Utils.Svg.SvgImage)
            Me.gotoTodayItem1.Name = "gotoTodayItem1"
            '
            'viewZoomInItem1
            '
            resources.ApplyResources(Me.viewZoomInItem1, "viewZoomInItem1")
            Me.viewZoomInItem1.Id = 11
            Me.viewZoomInItem1.ImageOptions.ImageIndex = CType(resources.GetObject("viewZoomInItem1.ImageOptions.ImageIndex"), Integer)
            Me.viewZoomInItem1.ImageOptions.LargeImageIndex = CType(resources.GetObject("viewZoomInItem1.ImageOptions.LargeImageIndex"), Integer)
            Me.viewZoomInItem1.ImageOptions.SvgImage = CType(resources.GetObject("viewZoomInItem1.ImageOptions.SvgImage"), DevExpress.Utils.Svg.SvgImage)
            Me.viewZoomInItem1.Name = "viewZoomInItem1"
            '
            'viewZoomOutItem1
            '
            resources.ApplyResources(Me.viewZoomOutItem1, "viewZoomOutItem1")
            Me.viewZoomOutItem1.Id = 12
            Me.viewZoomOutItem1.ImageOptions.ImageIndex = CType(resources.GetObject("viewZoomOutItem1.ImageOptions.ImageIndex"), Integer)
            Me.viewZoomOutItem1.ImageOptions.LargeImageIndex = CType(resources.GetObject("viewZoomOutItem1.ImageOptions.LargeImageIndex"), Integer)
            Me.viewZoomOutItem1.ImageOptions.SvgImage = CType(resources.GetObject("viewZoomOutItem1.ImageOptions.SvgImage"), DevExpress.Utils.Svg.SvgImage)
            Me.viewZoomOutItem1.Name = "viewZoomOutItem1"
            '
            'switchToDayViewItem1
            '
            resources.ApplyResources(Me.switchToDayViewItem1, "switchToDayViewItem1")
            Me.switchToDayViewItem1.Id = 13
            Me.switchToDayViewItem1.ImageOptions.ImageIndex = CType(resources.GetObject("switchToDayViewItem1.ImageOptions.ImageIndex"), Integer)
            Me.switchToDayViewItem1.ImageOptions.LargeImageIndex = CType(resources.GetObject("switchToDayViewItem1.ImageOptions.LargeImageIndex"), Integer)
            Me.switchToDayViewItem1.ImageOptions.SvgImage = CType(resources.GetObject("switchToDayViewItem1.ImageOptions.SvgImage"), DevExpress.Utils.Svg.SvgImage)
            Me.switchToDayViewItem1.Name = "switchToDayViewItem1"
            '
            'switchToWorkWeekViewItem1
            '
            resources.ApplyResources(Me.switchToWorkWeekViewItem1, "switchToWorkWeekViewItem1")
            Me.switchToWorkWeekViewItem1.Id = 14
            Me.switchToWorkWeekViewItem1.ImageOptions.ImageIndex = CType(resources.GetObject("switchToWorkWeekViewItem1.ImageOptions.ImageIndex"), Integer)
            Me.switchToWorkWeekViewItem1.ImageOptions.LargeImageIndex = CType(resources.GetObject("switchToWorkWeekViewItem1.ImageOptions.LargeImageIndex"), Integer)
            Me.switchToWorkWeekViewItem1.ImageOptions.SvgImage = CType(resources.GetObject("switchToWorkWeekViewItem1.ImageOptions.SvgImage"), DevExpress.Utils.Svg.SvgImage)
            Me.switchToWorkWeekViewItem1.Name = "switchToWorkWeekViewItem1"
            '
            'switchToWeekViewItem1
            '
            resources.ApplyResources(Me.switchToWeekViewItem1, "switchToWeekViewItem1")
            Me.switchToWeekViewItem1.Id = 15
            Me.switchToWeekViewItem1.ImageOptions.ImageIndex = CType(resources.GetObject("switchToWeekViewItem1.ImageOptions.ImageIndex"), Integer)
            Me.switchToWeekViewItem1.ImageOptions.LargeImageIndex = CType(resources.GetObject("switchToWeekViewItem1.ImageOptions.LargeImageIndex"), Integer)
            Me.switchToWeekViewItem1.ImageOptions.SvgImage = CType(resources.GetObject("switchToWeekViewItem1.ImageOptions.SvgImage"), DevExpress.Utils.Svg.SvgImage)
            Me.switchToWeekViewItem1.Name = "switchToWeekViewItem1"
            '
            'switchToFullWeekViewItem1
            '
            resources.ApplyResources(Me.switchToFullWeekViewItem1, "switchToFullWeekViewItem1")
            Me.switchToFullWeekViewItem1.Id = 16
            Me.switchToFullWeekViewItem1.ImageOptions.ImageIndex = CType(resources.GetObject("switchToFullWeekViewItem1.ImageOptions.ImageIndex"), Integer)
            Me.switchToFullWeekViewItem1.ImageOptions.LargeImageIndex = CType(resources.GetObject("switchToFullWeekViewItem1.ImageOptions.LargeImageIndex"), Integer)
            Me.switchToFullWeekViewItem1.ImageOptions.SvgImage = CType(resources.GetObject("switchToFullWeekViewItem1.ImageOptions.SvgImage"), DevExpress.Utils.Svg.SvgImage)
            Me.switchToFullWeekViewItem1.Name = "switchToFullWeekViewItem1"
            '
            'switchToMonthViewItem1
            '
            resources.ApplyResources(Me.switchToMonthViewItem1, "switchToMonthViewItem1")
            Me.switchToMonthViewItem1.Id = 17
            Me.switchToMonthViewItem1.ImageOptions.ImageIndex = CType(resources.GetObject("switchToMonthViewItem1.ImageOptions.ImageIndex"), Integer)
            Me.switchToMonthViewItem1.ImageOptions.LargeImageIndex = CType(resources.GetObject("switchToMonthViewItem1.ImageOptions.LargeImageIndex"), Integer)
            Me.switchToMonthViewItem1.ImageOptions.SvgImage = CType(resources.GetObject("switchToMonthViewItem1.ImageOptions.SvgImage"), DevExpress.Utils.Svg.SvgImage)
            Me.switchToMonthViewItem1.Name = "switchToMonthViewItem1"
            '
            'switchToTimelineViewItem1
            '
            resources.ApplyResources(Me.switchToTimelineViewItem1, "switchToTimelineViewItem1")
            Me.switchToTimelineViewItem1.Id = 18
            Me.switchToTimelineViewItem1.ImageOptions.ImageIndex = CType(resources.GetObject("switchToTimelineViewItem1.ImageOptions.ImageIndex"), Integer)
            Me.switchToTimelineViewItem1.ImageOptions.LargeImageIndex = CType(resources.GetObject("switchToTimelineViewItem1.ImageOptions.LargeImageIndex"), Integer)
            Me.switchToTimelineViewItem1.ImageOptions.SvgImage = CType(resources.GetObject("switchToTimelineViewItem1.ImageOptions.SvgImage"), DevExpress.Utils.Svg.SvgImage)
            Me.switchToTimelineViewItem1.Name = "switchToTimelineViewItem1"
            '
            'switchToGanttViewItem1
            '
            resources.ApplyResources(Me.switchToGanttViewItem1, "switchToGanttViewItem1")
            Me.switchToGanttViewItem1.Id = 19
            Me.switchToGanttViewItem1.ImageOptions.ImageIndex = CType(resources.GetObject("switchToGanttViewItem1.ImageOptions.ImageIndex"), Integer)
            Me.switchToGanttViewItem1.ImageOptions.LargeImageIndex = CType(resources.GetObject("switchToGanttViewItem1.ImageOptions.LargeImageIndex"), Integer)
            Me.switchToGanttViewItem1.ImageOptions.SvgImage = CType(resources.GetObject("switchToGanttViewItem1.ImageOptions.SvgImage"), DevExpress.Utils.Svg.SvgImage)
            Me.switchToGanttViewItem1.Name = "switchToGanttViewItem1"
            '
            'switchToAgendaViewItem1
            '
            resources.ApplyResources(Me.switchToAgendaViewItem1, "switchToAgendaViewItem1")
            Me.switchToAgendaViewItem1.Id = 20
            Me.switchToAgendaViewItem1.ImageOptions.ImageIndex = CType(resources.GetObject("switchToAgendaViewItem1.ImageOptions.ImageIndex"), Integer)
            Me.switchToAgendaViewItem1.ImageOptions.LargeImageIndex = CType(resources.GetObject("switchToAgendaViewItem1.ImageOptions.LargeImageIndex"), Integer)
            Me.switchToAgendaViewItem1.ImageOptions.SvgImage = CType(resources.GetObject("switchToAgendaViewItem1.ImageOptions.SvgImage"), DevExpress.Utils.Svg.SvgImage)
            Me.switchToAgendaViewItem1.Name = "switchToAgendaViewItem1"
            '
            'groupByNoneItem1
            '
            resources.ApplyResources(Me.groupByNoneItem1, "groupByNoneItem1")
            Me.groupByNoneItem1.Id = 21
            Me.groupByNoneItem1.ImageOptions.ImageIndex = CType(resources.GetObject("groupByNoneItem1.ImageOptions.ImageIndex"), Integer)
            Me.groupByNoneItem1.ImageOptions.LargeImageIndex = CType(resources.GetObject("groupByNoneItem1.ImageOptions.LargeImageIndex"), Integer)
            Me.groupByNoneItem1.ImageOptions.SvgImage = CType(resources.GetObject("groupByNoneItem1.ImageOptions.SvgImage"), DevExpress.Utils.Svg.SvgImage)
            Me.groupByNoneItem1.Name = "groupByNoneItem1"
            '
            'groupByDateItem1
            '
            resources.ApplyResources(Me.groupByDateItem1, "groupByDateItem1")
            Me.groupByDateItem1.Id = 22
            Me.groupByDateItem1.ImageOptions.ImageIndex = CType(resources.GetObject("groupByDateItem1.ImageOptions.ImageIndex"), Integer)
            Me.groupByDateItem1.ImageOptions.LargeImageIndex = CType(resources.GetObject("groupByDateItem1.ImageOptions.LargeImageIndex"), Integer)
            Me.groupByDateItem1.ImageOptions.SvgImage = CType(resources.GetObject("groupByDateItem1.ImageOptions.SvgImage"), DevExpress.Utils.Svg.SvgImage)
            Me.groupByDateItem1.Name = "groupByDateItem1"
            '
            'groupByResourceItem1
            '
            resources.ApplyResources(Me.groupByResourceItem1, "groupByResourceItem1")
            Me.groupByResourceItem1.Id = 23
            Me.groupByResourceItem1.ImageOptions.ImageIndex = CType(resources.GetObject("groupByResourceItem1.ImageOptions.ImageIndex"), Integer)
            Me.groupByResourceItem1.ImageOptions.LargeImageIndex = CType(resources.GetObject("groupByResourceItem1.ImageOptions.LargeImageIndex"), Integer)
            Me.groupByResourceItem1.ImageOptions.SvgImage = CType(resources.GetObject("groupByResourceItem1.ImageOptions.SvgImage"), DevExpress.Utils.Svg.SvgImage)
            Me.groupByResourceItem1.Name = "groupByResourceItem1"
            '
            'switchTimeScalesItem1
            '
            resources.ApplyResources(Me.switchTimeScalesItem1, "switchTimeScalesItem1")
            Me.switchTimeScalesItem1.Id = 24
            Me.switchTimeScalesItem1.ImageOptions.ImageIndex = CType(resources.GetObject("switchTimeScalesItem1.ImageOptions.ImageIndex"), Integer)
            Me.switchTimeScalesItem1.ImageOptions.LargeImageIndex = CType(resources.GetObject("switchTimeScalesItem1.ImageOptions.LargeImageIndex"), Integer)
            Me.switchTimeScalesItem1.ImageOptions.SvgImage = CType(resources.GetObject("switchTimeScalesItem1.ImageOptions.SvgImage"), DevExpress.Utils.Svg.SvgImage)
            Me.switchTimeScalesItem1.Name = "switchTimeScalesItem1"
            '
            'changeScaleWidthItem1
            '
            resources.ApplyResources(Me.changeScaleWidthItem1, "changeScaleWidthItem1")
            Me.changeScaleWidthItem1.Edit = Me.repositoryItemSpinEdit1
            Me.changeScaleWidthItem1.Id = 25
            Me.changeScaleWidthItem1.ImageOptions.ImageIndex = CType(resources.GetObject("changeScaleWidthItem1.ImageOptions.ImageIndex"), Integer)
            Me.changeScaleWidthItem1.ImageOptions.LargeImageIndex = CType(resources.GetObject("changeScaleWidthItem1.ImageOptions.LargeImageIndex"), Integer)
            Me.changeScaleWidthItem1.ImageOptions.SvgImage = CType(resources.GetObject("changeScaleWidthItem1.ImageOptions.SvgImage"), DevExpress.Utils.Svg.SvgImage)
            Me.changeScaleWidthItem1.Name = "changeScaleWidthItem1"
            Me.changeScaleWidthItem1.UseCommandCaption = True
            '
            'repositoryItemSpinEdit1
            '
            resources.ApplyResources(Me.repositoryItemSpinEdit1, "repositoryItemSpinEdit1")
            Me.repositoryItemSpinEdit1.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(CType(resources.GetObject("repositoryItemSpinEdit1.Buttons"), DevExpress.XtraEditors.Controls.ButtonPredefines))})
            Me.repositoryItemSpinEdit1.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.[Default]
            Me.repositoryItemSpinEdit1.MaxValue = New Decimal(New Integer() {200, 0, 0, 0})
            Me.repositoryItemSpinEdit1.MinValue = New Decimal(New Integer() {10, 0, 0, 0})
            Me.repositoryItemSpinEdit1.Name = "repositoryItemSpinEdit1"
            '
            'switchTimeScalesCaptionItem1
            '
            resources.ApplyResources(Me.switchTimeScalesCaptionItem1, "switchTimeScalesCaptionItem1")
            Me.switchTimeScalesCaptionItem1.Id = 26
            Me.switchTimeScalesCaptionItem1.ImageOptions.ImageIndex = CType(resources.GetObject("switchTimeScalesCaptionItem1.ImageOptions.ImageIndex"), Integer)
            Me.switchTimeScalesCaptionItem1.ImageOptions.LargeImageIndex = CType(resources.GetObject("switchTimeScalesCaptionItem1.ImageOptions.LargeImageIndex"), Integer)
            Me.switchTimeScalesCaptionItem1.ImageOptions.SvgImage = CType(resources.GetObject("switchTimeScalesCaptionItem1.ImageOptions.SvgImage"), DevExpress.Utils.Svg.SvgImage)
            Me.switchTimeScalesCaptionItem1.Name = "switchTimeScalesCaptionItem1"
            '
            'switchCompressWeekendItem1
            '
            resources.ApplyResources(Me.switchCompressWeekendItem1, "switchCompressWeekendItem1")
            Me.switchCompressWeekendItem1.Id = 27
            Me.switchCompressWeekendItem1.ImageOptions.ImageIndex = CType(resources.GetObject("switchCompressWeekendItem1.ImageOptions.ImageIndex"), Integer)
            Me.switchCompressWeekendItem1.ImageOptions.LargeImageIndex = CType(resources.GetObject("switchCompressWeekendItem1.ImageOptions.LargeImageIndex"), Integer)
            Me.switchCompressWeekendItem1.ImageOptions.SvgImage = CType(resources.GetObject("switchCompressWeekendItem1.ImageOptions.SvgImage"), DevExpress.Utils.Svg.SvgImage)
            Me.switchCompressWeekendItem1.Name = "switchCompressWeekendItem1"
            '
            'switchShowWorkTimeOnlyItem1
            '
            resources.ApplyResources(Me.switchShowWorkTimeOnlyItem1, "switchShowWorkTimeOnlyItem1")
            Me.switchShowWorkTimeOnlyItem1.Id = 28
            Me.switchShowWorkTimeOnlyItem1.ImageOptions.ImageIndex = CType(resources.GetObject("switchShowWorkTimeOnlyItem1.ImageOptions.ImageIndex"), Integer)
            Me.switchShowWorkTimeOnlyItem1.ImageOptions.LargeImageIndex = CType(resources.GetObject("switchShowWorkTimeOnlyItem1.ImageOptions.LargeImageIndex"), Integer)
            Me.switchShowWorkTimeOnlyItem1.ImageOptions.SvgImage = CType(resources.GetObject("switchShowWorkTimeOnlyItem1.ImageOptions.SvgImage"), DevExpress.Utils.Svg.SvgImage)
            Me.switchShowWorkTimeOnlyItem1.Name = "switchShowWorkTimeOnlyItem1"
            '
            'switchCellsAutoHeightItem1
            '
            resources.ApplyResources(Me.switchCellsAutoHeightItem1, "switchCellsAutoHeightItem1")
            Me.switchCellsAutoHeightItem1.Id = 29
            Me.switchCellsAutoHeightItem1.ImageOptions.ImageIndex = CType(resources.GetObject("switchCellsAutoHeightItem1.ImageOptions.ImageIndex"), Integer)
            Me.switchCellsAutoHeightItem1.ImageOptions.LargeImageIndex = CType(resources.GetObject("switchCellsAutoHeightItem1.ImageOptions.LargeImageIndex"), Integer)
            Me.switchCellsAutoHeightItem1.ImageOptions.SvgImage = CType(resources.GetObject("switchCellsAutoHeightItem1.ImageOptions.SvgImage"), DevExpress.Utils.Svg.SvgImage)
            Me.switchCellsAutoHeightItem1.Name = "switchCellsAutoHeightItem1"
            '
            'changeSnapToCellsUIItem1
            '
            resources.ApplyResources(Me.changeSnapToCellsUIItem1, "changeSnapToCellsUIItem1")
            Me.changeSnapToCellsUIItem1.Id = 30
            Me.changeSnapToCellsUIItem1.ImageOptions.ImageIndex = CType(resources.GetObject("changeSnapToCellsUIItem1.ImageOptions.ImageIndex"), Integer)
            Me.changeSnapToCellsUIItem1.ImageOptions.LargeImageIndex = CType(resources.GetObject("changeSnapToCellsUIItem1.ImageOptions.LargeImageIndex"), Integer)
            Me.changeSnapToCellsUIItem1.ImageOptions.SvgImage = CType(resources.GetObject("changeSnapToCellsUIItem1.ImageOptions.SvgImage"), DevExpress.Utils.Svg.SvgImage)
            Me.changeSnapToCellsUIItem1.Name = "changeSnapToCellsUIItem1"
            '
            'editAppointmentQueryItem1
            '
            resources.ApplyResources(Me.editAppointmentQueryItem1, "editAppointmentQueryItem1")
            Me.editAppointmentQueryItem1.Id = 31
            Me.editAppointmentQueryItem1.ImageOptions.ImageIndex = CType(resources.GetObject("editAppointmentQueryItem1.ImageOptions.ImageIndex"), Integer)
            Me.editAppointmentQueryItem1.ImageOptions.LargeImageIndex = CType(resources.GetObject("editAppointmentQueryItem1.ImageOptions.LargeImageIndex"), Integer)
            Me.editAppointmentQueryItem1.ImageOptions.SvgImage = CType(resources.GetObject("editAppointmentQueryItem1.ImageOptions.SvgImage"), DevExpress.Utils.Svg.SvgImage)
            Me.editAppointmentQueryItem1.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.editOccurrenceUICommandItem1), New DevExpress.XtraBars.LinkPersistInfo(Me.editSeriesUICommandItem1)})
            Me.editAppointmentQueryItem1.Name = "editAppointmentQueryItem1"
            Me.editAppointmentQueryItem1.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph
            '
            'editOccurrenceUICommandItem1
            '
            resources.ApplyResources(Me.editOccurrenceUICommandItem1, "editOccurrenceUICommandItem1")
            Me.editOccurrenceUICommandItem1.Id = 32
            Me.editOccurrenceUICommandItem1.ImageOptions.ImageIndex = CType(resources.GetObject("editOccurrenceUICommandItem1.ImageOptions.ImageIndex"), Integer)
            Me.editOccurrenceUICommandItem1.ImageOptions.LargeImageIndex = CType(resources.GetObject("editOccurrenceUICommandItem1.ImageOptions.LargeImageIndex"), Integer)
            Me.editOccurrenceUICommandItem1.ImageOptions.SvgImage = CType(resources.GetObject("editOccurrenceUICommandItem1.ImageOptions.SvgImage"), DevExpress.Utils.Svg.SvgImage)
            Me.editOccurrenceUICommandItem1.Name = "editOccurrenceUICommandItem1"
            '
            'editSeriesUICommandItem1
            '
            resources.ApplyResources(Me.editSeriesUICommandItem1, "editSeriesUICommandItem1")
            Me.editSeriesUICommandItem1.Id = 33
            Me.editSeriesUICommandItem1.ImageOptions.ImageIndex = CType(resources.GetObject("editSeriesUICommandItem1.ImageOptions.ImageIndex"), Integer)
            Me.editSeriesUICommandItem1.ImageOptions.LargeImageIndex = CType(resources.GetObject("editSeriesUICommandItem1.ImageOptions.LargeImageIndex"), Integer)
            Me.editSeriesUICommandItem1.ImageOptions.SvgImage = CType(resources.GetObject("editSeriesUICommandItem1.ImageOptions.SvgImage"), DevExpress.Utils.Svg.SvgImage)
            Me.editSeriesUICommandItem1.Name = "editSeriesUICommandItem1"
            '
            'deleteAppointmentsItem1
            '
            resources.ApplyResources(Me.deleteAppointmentsItem1, "deleteAppointmentsItem1")
            Me.deleteAppointmentsItem1.Id = 34
            Me.deleteAppointmentsItem1.ImageOptions.ImageIndex = CType(resources.GetObject("deleteAppointmentsItem1.ImageOptions.ImageIndex"), Integer)
            Me.deleteAppointmentsItem1.ImageOptions.LargeImageIndex = CType(resources.GetObject("deleteAppointmentsItem1.ImageOptions.LargeImageIndex"), Integer)
            Me.deleteAppointmentsItem1.ImageOptions.SvgImage = CType(resources.GetObject("deleteAppointmentsItem1.ImageOptions.SvgImage"), DevExpress.Utils.Svg.SvgImage)
            Me.deleteAppointmentsItem1.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.deleteOccurrenceItem1), New DevExpress.XtraBars.LinkPersistInfo(Me.deleteSeriesItem1)})
            Me.deleteAppointmentsItem1.Name = "deleteAppointmentsItem1"
            Me.deleteAppointmentsItem1.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph
            '
            'deleteOccurrenceItem1
            '
            resources.ApplyResources(Me.deleteOccurrenceItem1, "deleteOccurrenceItem1")
            Me.deleteOccurrenceItem1.Id = 35
            Me.deleteOccurrenceItem1.ImageOptions.ImageIndex = CType(resources.GetObject("deleteOccurrenceItem1.ImageOptions.ImageIndex"), Integer)
            Me.deleteOccurrenceItem1.ImageOptions.LargeImageIndex = CType(resources.GetObject("deleteOccurrenceItem1.ImageOptions.LargeImageIndex"), Integer)
            Me.deleteOccurrenceItem1.ImageOptions.SvgImage = CType(resources.GetObject("deleteOccurrenceItem1.ImageOptions.SvgImage"), DevExpress.Utils.Svg.SvgImage)
            Me.deleteOccurrenceItem1.Name = "deleteOccurrenceItem1"
            '
            'deleteSeriesItem1
            '
            resources.ApplyResources(Me.deleteSeriesItem1, "deleteSeriesItem1")
            Me.deleteSeriesItem1.Id = 36
            Me.deleteSeriesItem1.ImageOptions.ImageIndex = CType(resources.GetObject("deleteSeriesItem1.ImageOptions.ImageIndex"), Integer)
            Me.deleteSeriesItem1.ImageOptions.LargeImageIndex = CType(resources.GetObject("deleteSeriesItem1.ImageOptions.LargeImageIndex"), Integer)
            Me.deleteSeriesItem1.ImageOptions.SvgImage = CType(resources.GetObject("deleteSeriesItem1.ImageOptions.SvgImage"), DevExpress.Utils.Svg.SvgImage)
            Me.deleteSeriesItem1.Name = "deleteSeriesItem1"
            '
            'splitAppointmentItem1
            '
            resources.ApplyResources(Me.splitAppointmentItem1, "splitAppointmentItem1")
            Me.splitAppointmentItem1.Id = 37
            Me.splitAppointmentItem1.ImageOptions.ImageIndex = CType(resources.GetObject("splitAppointmentItem1.ImageOptions.ImageIndex"), Integer)
            Me.splitAppointmentItem1.ImageOptions.LargeImageIndex = CType(resources.GetObject("splitAppointmentItem1.ImageOptions.LargeImageIndex"), Integer)
            Me.splitAppointmentItem1.ImageOptions.SvgImage = CType(resources.GetObject("splitAppointmentItem1.ImageOptions.SvgImage"), DevExpress.Utils.Svg.SvgImage)
            Me.splitAppointmentItem1.Name = "splitAppointmentItem1"
            '
            'changeAppointmentStatusItem1
            '
            resources.ApplyResources(Me.changeAppointmentStatusItem1, "changeAppointmentStatusItem1")
            Me.changeAppointmentStatusItem1.Id = 38
            Me.changeAppointmentStatusItem1.ImageOptions.ImageIndex = CType(resources.GetObject("changeAppointmentStatusItem1.ImageOptions.ImageIndex"), Integer)
            Me.changeAppointmentStatusItem1.ImageOptions.LargeImageIndex = CType(resources.GetObject("changeAppointmentStatusItem1.ImageOptions.LargeImageIndex"), Integer)
            Me.changeAppointmentStatusItem1.ImageOptions.SvgImage = CType(resources.GetObject("changeAppointmentStatusItem1.ImageOptions.SvgImage"), DevExpress.Utils.Svg.SvgImage)
            Me.changeAppointmentStatusItem1.Name = "changeAppointmentStatusItem1"
            '
            'changeAppointmentLabelItem1
            '
            resources.ApplyResources(Me.changeAppointmentLabelItem1, "changeAppointmentLabelItem1")
            Me.changeAppointmentLabelItem1.Id = 39
            Me.changeAppointmentLabelItem1.ImageOptions.ImageIndex = CType(resources.GetObject("changeAppointmentLabelItem1.ImageOptions.ImageIndex"), Integer)
            Me.changeAppointmentLabelItem1.ImageOptions.LargeImageIndex = CType(resources.GetObject("changeAppointmentLabelItem1.ImageOptions.LargeImageIndex"), Integer)
            Me.changeAppointmentLabelItem1.ImageOptions.SvgImage = CType(resources.GetObject("changeAppointmentLabelItem1.ImageOptions.SvgImage"), DevExpress.Utils.Svg.SvgImage)
            Me.changeAppointmentLabelItem1.Name = "changeAppointmentLabelItem1"
            '
            'toggleRecurrenceItem1
            '
            resources.ApplyResources(Me.toggleRecurrenceItem1, "toggleRecurrenceItem1")
            Me.toggleRecurrenceItem1.Id = 40
            Me.toggleRecurrenceItem1.ImageOptions.ImageIndex = CType(resources.GetObject("toggleRecurrenceItem1.ImageOptions.ImageIndex"), Integer)
            Me.toggleRecurrenceItem1.ImageOptions.LargeImageIndex = CType(resources.GetObject("toggleRecurrenceItem1.ImageOptions.LargeImageIndex"), Integer)
            Me.toggleRecurrenceItem1.ImageOptions.SvgImage = CType(resources.GetObject("toggleRecurrenceItem1.ImageOptions.SvgImage"), DevExpress.Utils.Svg.SvgImage)
            Me.toggleRecurrenceItem1.Name = "toggleRecurrenceItem1"
            '
            'changeAppointmentReminderItem1
            '
            resources.ApplyResources(Me.changeAppointmentReminderItem1, "changeAppointmentReminderItem1")
            Me.changeAppointmentReminderItem1.Edit = Me.repositoryItemDuration1
            Me.changeAppointmentReminderItem1.Id = 41
            Me.changeAppointmentReminderItem1.ImageOptions.ImageIndex = CType(resources.GetObject("changeAppointmentReminderItem1.ImageOptions.ImageIndex"), Integer)
            Me.changeAppointmentReminderItem1.ImageOptions.LargeImageIndex = CType(resources.GetObject("changeAppointmentReminderItem1.ImageOptions.LargeImageIndex"), Integer)
            Me.changeAppointmentReminderItem1.ImageOptions.SvgImage = CType(resources.GetObject("changeAppointmentReminderItem1.ImageOptions.SvgImage"), DevExpress.Utils.Svg.SvgImage)
            Me.changeAppointmentReminderItem1.Name = "changeAppointmentReminderItem1"
            '
            'repositoryItemDuration1
            '
            Me.repositoryItemDuration1.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
            resources.ApplyResources(Me.repositoryItemDuration1, "repositoryItemDuration1")
            Me.repositoryItemDuration1.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(CType(resources.GetObject("repositoryItemDuration1.Buttons"), DevExpress.XtraEditors.Controls.ButtonPredefines))})
            Me.repositoryItemDuration1.DisabledStateText = Nothing
            Me.repositoryItemDuration1.Name = "repositoryItemDuration1"
            Me.repositoryItemDuration1.ShowEmptyItem = True
            Me.repositoryItemDuration1.ValidateOnEnterKey = True
            '
            'bExit
            '
            resources.ApplyResources(Me.bExit, "bExit")
            Me.bExit.Id = 42
            Me.bExit.ImageOptions.Image = CType(resources.GetObject("bExit.ImageOptions.Image"), System.Drawing.Image)
            Me.bExit.ImageOptions.ImageIndex = CType(resources.GetObject("bExit.ImageOptions.ImageIndex"), Integer)
            Me.bExit.ImageOptions.LargeImage = CType(resources.GetObject("bExit.ImageOptions.LargeImage"), System.Drawing.Image)
            Me.bExit.ImageOptions.LargeImageIndex = CType(resources.GetObject("bExit.ImageOptions.LargeImageIndex"), Integer)
            Me.bExit.ImageOptions.SvgImage = CType(resources.GetObject("bExit.ImageOptions.SvgImage"), DevExpress.Utils.Svg.SvgImage)
            Me.bExit.Name = "bExit"
            '
            'calendarToolsRibbonPageCategory1
            '
            Me.calendarToolsRibbonPageCategory1.Control = Me.schedulerControl1
            Me.calendarToolsRibbonPageCategory1.Name = "calendarToolsRibbonPageCategory1"
            Me.calendarToolsRibbonPageCategory1.Pages.AddRange(New DevExpress.XtraBars.Ribbon.RibbonPage() {Me.appointmentRibbonPage1})
            resources.ApplyResources(Me.calendarToolsRibbonPageCategory1, "calendarToolsRibbonPageCategory1")
            '
            'appointmentRibbonPage1
            '
            Me.appointmentRibbonPage1.Groups.AddRange(New DevExpress.XtraBars.Ribbon.RibbonPageGroup() {Me.actionsRibbonPageGroup1, Me.optionsRibbonPageGroup1})
            Me.appointmentRibbonPage1.Name = "appointmentRibbonPage1"
            Me.appointmentRibbonPage1.Visible = False
            '
            'actionsRibbonPageGroup1
            '
            Me.actionsRibbonPageGroup1.CaptionButtonVisible = DevExpress.Utils.DefaultBoolean.[False]
            Me.actionsRibbonPageGroup1.ItemLinks.Add(Me.editAppointmentQueryItem1)
            Me.actionsRibbonPageGroup1.ItemLinks.Add(Me.deleteAppointmentsItem1)
            Me.actionsRibbonPageGroup1.ItemLinks.Add(Me.splitAppointmentItem1)
            Me.actionsRibbonPageGroup1.Name = "actionsRibbonPageGroup1"
            '
            'optionsRibbonPageGroup1
            '
            Me.optionsRibbonPageGroup1.CaptionButtonVisible = DevExpress.Utils.DefaultBoolean.[False]
            Me.optionsRibbonPageGroup1.ItemLinks.Add(Me.changeAppointmentStatusItem1)
            Me.optionsRibbonPageGroup1.ItemLinks.Add(Me.changeAppointmentLabelItem1)
            Me.optionsRibbonPageGroup1.ItemLinks.Add(Me.toggleRecurrenceItem1)
            Me.optionsRibbonPageGroup1.ItemLinks.Add(Me.changeAppointmentReminderItem1)
            Me.optionsRibbonPageGroup1.Name = "optionsRibbonPageGroup1"
            '
            'fileRibbonPage1
            '
            Me.fileRibbonPage1.Groups.AddRange(New DevExpress.XtraBars.Ribbon.RibbonPageGroup() {Me.commonRibbonPageGroup1, Me.printRibbonPageGroup1})
            Me.fileRibbonPage1.Name = "fileRibbonPage1"
            '
            'commonRibbonPageGroup1
            '
            Me.commonRibbonPageGroup1.CaptionButtonVisible = DevExpress.Utils.DefaultBoolean.[False]
            Me.commonRibbonPageGroup1.ItemLinks.Add(Me.openScheduleItem1)
            Me.commonRibbonPageGroup1.ItemLinks.Add(Me.saveScheduleItem1)
            Me.commonRibbonPageGroup1.Name = "commonRibbonPageGroup1"
            '
            'printRibbonPageGroup1
            '
            Me.printRibbonPageGroup1.CaptionButtonVisible = DevExpress.Utils.DefaultBoolean.[False]
            Me.printRibbonPageGroup1.ItemLinks.Add(Me.printPreviewItem1)
            Me.printRibbonPageGroup1.ItemLinks.Add(Me.printItem1)
            Me.printRibbonPageGroup1.ItemLinks.Add(Me.printPageSetupItem1)
            Me.printRibbonPageGroup1.ItemLinks.Add(Me.bExit)
            Me.printRibbonPageGroup1.Name = "printRibbonPageGroup1"
            '
            'homeRibbonPage1
            '
            Me.homeRibbonPage1.Groups.AddRange(New DevExpress.XtraBars.Ribbon.RibbonPageGroup() {Me.appointmentRibbonPageGroup1, Me.navigatorRibbonPageGroup1, Me.arrangeRibbonPageGroup1, Me.groupByRibbonPageGroup1})
            Me.homeRibbonPage1.Name = "homeRibbonPage1"
            '
            'appointmentRibbonPageGroup1
            '
            Me.appointmentRibbonPageGroup1.CaptionButtonVisible = DevExpress.Utils.DefaultBoolean.[False]
            Me.appointmentRibbonPageGroup1.ItemLinks.Add(Me.newAppointmentItem1)
            Me.appointmentRibbonPageGroup1.ItemLinks.Add(Me.newRecurringAppointmentItem1)
            Me.appointmentRibbonPageGroup1.Name = "appointmentRibbonPageGroup1"
            '
            'navigatorRibbonPageGroup1
            '
            Me.navigatorRibbonPageGroup1.CaptionButtonVisible = DevExpress.Utils.DefaultBoolean.[False]
            Me.navigatorRibbonPageGroup1.ItemLinks.Add(Me.navigateViewBackwardItem1)
            Me.navigatorRibbonPageGroup1.ItemLinks.Add(Me.navigateViewForwardItem1)
            Me.navigatorRibbonPageGroup1.ItemLinks.Add(Me.gotoTodayItem1)
            Me.navigatorRibbonPageGroup1.ItemLinks.Add(Me.viewZoomInItem1)
            Me.navigatorRibbonPageGroup1.ItemLinks.Add(Me.viewZoomOutItem1)
            Me.navigatorRibbonPageGroup1.Name = "navigatorRibbonPageGroup1"
            '
            'arrangeRibbonPageGroup1
            '
            Me.arrangeRibbonPageGroup1.CaptionButtonVisible = DevExpress.Utils.DefaultBoolean.[False]
            Me.arrangeRibbonPageGroup1.ItemLinks.Add(Me.switchToDayViewItem1)
            Me.arrangeRibbonPageGroup1.ItemLinks.Add(Me.switchToWorkWeekViewItem1)
            Me.arrangeRibbonPageGroup1.ItemLinks.Add(Me.switchToWeekViewItem1)
            Me.arrangeRibbonPageGroup1.ItemLinks.Add(Me.switchToFullWeekViewItem1)
            Me.arrangeRibbonPageGroup1.ItemLinks.Add(Me.switchToMonthViewItem1)
            Me.arrangeRibbonPageGroup1.ItemLinks.Add(Me.switchToTimelineViewItem1)
            Me.arrangeRibbonPageGroup1.ItemLinks.Add(Me.switchToGanttViewItem1)
            Me.arrangeRibbonPageGroup1.ItemLinks.Add(Me.switchToAgendaViewItem1)
            Me.arrangeRibbonPageGroup1.Name = "arrangeRibbonPageGroup1"
            '
            'groupByRibbonPageGroup1
            '
            Me.groupByRibbonPageGroup1.CaptionButtonVisible = DevExpress.Utils.DefaultBoolean.[False]
            Me.groupByRibbonPageGroup1.ItemLinks.Add(Me.groupByNoneItem1)
            Me.groupByRibbonPageGroup1.ItemLinks.Add(Me.groupByDateItem1)
            Me.groupByRibbonPageGroup1.ItemLinks.Add(Me.groupByResourceItem1)
            Me.groupByRibbonPageGroup1.Name = "groupByRibbonPageGroup1"
            '
            'viewRibbonPage1
            '
            Me.viewRibbonPage1.Groups.AddRange(New DevExpress.XtraBars.Ribbon.RibbonPageGroup() {Me.activeViewRibbonPageGroup1, Me.timeScaleRibbonPageGroup1, Me.layoutRibbonPageGroup1})
            Me.viewRibbonPage1.Name = "viewRibbonPage1"
            '
            'activeViewRibbonPageGroup1
            '
            Me.activeViewRibbonPageGroup1.CaptionButtonVisible = DevExpress.Utils.DefaultBoolean.[False]
            Me.activeViewRibbonPageGroup1.ItemLinks.Add(Me.switchToDayViewItem1)
            Me.activeViewRibbonPageGroup1.ItemLinks.Add(Me.switchToWorkWeekViewItem1)
            Me.activeViewRibbonPageGroup1.ItemLinks.Add(Me.switchToWeekViewItem1)
            Me.activeViewRibbonPageGroup1.ItemLinks.Add(Me.switchToFullWeekViewItem1)
            Me.activeViewRibbonPageGroup1.ItemLinks.Add(Me.switchToMonthViewItem1)
            Me.activeViewRibbonPageGroup1.ItemLinks.Add(Me.switchToTimelineViewItem1)
            Me.activeViewRibbonPageGroup1.ItemLinks.Add(Me.switchToGanttViewItem1)
            Me.activeViewRibbonPageGroup1.ItemLinks.Add(Me.switchToAgendaViewItem1)
            Me.activeViewRibbonPageGroup1.Name = "activeViewRibbonPageGroup1"
            '
            'timeScaleRibbonPageGroup1
            '
            Me.timeScaleRibbonPageGroup1.CaptionButtonVisible = DevExpress.Utils.DefaultBoolean.[False]
            Me.timeScaleRibbonPageGroup1.ItemLinks.Add(Me.switchTimeScalesItem1)
            Me.timeScaleRibbonPageGroup1.ItemLinks.Add(Me.changeScaleWidthItem1)
            Me.timeScaleRibbonPageGroup1.ItemLinks.Add(Me.switchTimeScalesCaptionItem1)
            Me.timeScaleRibbonPageGroup1.Name = "timeScaleRibbonPageGroup1"
            '
            'layoutRibbonPageGroup1
            '
            Me.layoutRibbonPageGroup1.CaptionButtonVisible = DevExpress.Utils.DefaultBoolean.[False]
            Me.layoutRibbonPageGroup1.ItemLinks.Add(Me.switchCompressWeekendItem1)
            Me.layoutRibbonPageGroup1.ItemLinks.Add(Me.switchShowWorkTimeOnlyItem1)
            Me.layoutRibbonPageGroup1.ItemLinks.Add(Me.switchCellsAutoHeightItem1)
            Me.layoutRibbonPageGroup1.ItemLinks.Add(Me.changeSnapToCellsUIItem1)
            Me.layoutRibbonPageGroup1.Name = "layoutRibbonPageGroup1"
            '
            'layoutControlGroup1
            '
            resources.ApplyResources(Me.layoutControlGroup1, "layoutControlGroup1")
            Me.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
            Me.layoutControlGroup1.GroupBordersVisible = False
            Me.layoutControlGroup1.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.layoutControlItem1, Me.layoutControlItem2, Me.splitterItem1})
            Me.layoutControlGroup1.Name = "Root"
            Me.layoutControlGroup1.Size = New System.Drawing.Size(986, 607)
            Me.layoutControlGroup1.TextVisible = False
            '
            'layoutControlItem1
            '
            resources.ApplyResources(Me.layoutControlItem1, "layoutControlItem1")
            Me.layoutControlItem1.Control = Me.schedulerControl1
            Me.layoutControlItem1.Location = New System.Drawing.Point(0, 0)
            Me.layoutControlItem1.Name = "layoutControlItem1"
            Me.layoutControlItem1.Size = New System.Drawing.Size(700, 587)
            Me.layoutControlItem1.TextSize = New System.Drawing.Size(0, 0)
            Me.layoutControlItem1.TextVisible = False
            '
            'layoutControlItem2
            '
            resources.ApplyResources(Me.layoutControlItem2, "layoutControlItem2")
            Me.layoutControlItem2.Control = Me.dateNavigator1
            Me.layoutControlItem2.Location = New System.Drawing.Point(706, 0)
            Me.layoutControlItem2.Name = "layoutControlItem2"
            Me.layoutControlItem2.Size = New System.Drawing.Size(260, 587)
            Me.layoutControlItem2.TextSize = New System.Drawing.Size(0, 0)
            Me.layoutControlItem2.TextVisible = False
            '
            'splitterItem1
            '
            resources.ApplyResources(Me.splitterItem1, "splitterItem1")
            Me.splitterItem1.AllowHotTrack = True
            Me.splitterItem1.Location = New System.Drawing.Point(700, 0)
            Me.splitterItem1.Name = "splitterItem1"
            Me.splitterItem1.Size = New System.Drawing.Size(6, 587)
            '
            'schedulerBarController1
            '
            Me.schedulerBarController1.BarItems.Add(Me.openScheduleItem1)
            Me.schedulerBarController1.BarItems.Add(Me.saveScheduleItem1)
            Me.schedulerBarController1.BarItems.Add(Me.printPreviewItem1)
            Me.schedulerBarController1.BarItems.Add(Me.printItem1)
            Me.schedulerBarController1.BarItems.Add(Me.printPageSetupItem1)
            Me.schedulerBarController1.BarItems.Add(Me.newAppointmentItem1)
            Me.schedulerBarController1.BarItems.Add(Me.newRecurringAppointmentItem1)
            Me.schedulerBarController1.BarItems.Add(Me.navigateViewBackwardItem1)
            Me.schedulerBarController1.BarItems.Add(Me.navigateViewForwardItem1)
            Me.schedulerBarController1.BarItems.Add(Me.gotoTodayItem1)
            Me.schedulerBarController1.BarItems.Add(Me.viewZoomInItem1)
            Me.schedulerBarController1.BarItems.Add(Me.viewZoomOutItem1)
            Me.schedulerBarController1.BarItems.Add(Me.switchToDayViewItem1)
            Me.schedulerBarController1.BarItems.Add(Me.switchToWorkWeekViewItem1)
            Me.schedulerBarController1.BarItems.Add(Me.switchToWeekViewItem1)
            Me.schedulerBarController1.BarItems.Add(Me.switchToFullWeekViewItem1)
            Me.schedulerBarController1.BarItems.Add(Me.switchToMonthViewItem1)
            Me.schedulerBarController1.BarItems.Add(Me.switchToTimelineViewItem1)
            Me.schedulerBarController1.BarItems.Add(Me.switchToGanttViewItem1)
            Me.schedulerBarController1.BarItems.Add(Me.switchToAgendaViewItem1)
            Me.schedulerBarController1.BarItems.Add(Me.groupByNoneItem1)
            Me.schedulerBarController1.BarItems.Add(Me.groupByDateItem1)
            Me.schedulerBarController1.BarItems.Add(Me.groupByResourceItem1)
            Me.schedulerBarController1.BarItems.Add(Me.switchTimeScalesItem1)
            Me.schedulerBarController1.BarItems.Add(Me.changeScaleWidthItem1)
            Me.schedulerBarController1.BarItems.Add(Me.switchTimeScalesCaptionItem1)
            Me.schedulerBarController1.BarItems.Add(Me.switchCompressWeekendItem1)
            Me.schedulerBarController1.BarItems.Add(Me.switchShowWorkTimeOnlyItem1)
            Me.schedulerBarController1.BarItems.Add(Me.switchCellsAutoHeightItem1)
            Me.schedulerBarController1.BarItems.Add(Me.changeSnapToCellsUIItem1)
            Me.schedulerBarController1.BarItems.Add(Me.editOccurrenceUICommandItem1)
            Me.schedulerBarController1.BarItems.Add(Me.editSeriesUICommandItem1)
            Me.schedulerBarController1.BarItems.Add(Me.editAppointmentQueryItem1)
            Me.schedulerBarController1.BarItems.Add(Me.deleteOccurrenceItem1)
            Me.schedulerBarController1.BarItems.Add(Me.deleteSeriesItem1)
            Me.schedulerBarController1.BarItems.Add(Me.deleteAppointmentsItem1)
            Me.schedulerBarController1.BarItems.Add(Me.splitAppointmentItem1)
            Me.schedulerBarController1.BarItems.Add(Me.changeAppointmentStatusItem1)
            Me.schedulerBarController1.BarItems.Add(Me.changeAppointmentLabelItem1)
            Me.schedulerBarController1.BarItems.Add(Me.toggleRecurrenceItem1)
            Me.schedulerBarController1.BarItems.Add(Me.changeAppointmentReminderItem1)
            Me.schedulerBarController1.Control = Me.schedulerControl1
            '
            'AppointmentsTableAdapter
            '
            Me.AppointmentsTableAdapter.ClearBeforeFill = True
            '
            'ResourcesBindingSource
            '
            Me.ResourcesBindingSource.DataMember = "Resources"
            Me.ResourcesBindingSource.DataSource = Me.IDScomptabiliteDataSet
            '
            'ResourcesTableAdapter
            '
            Me.ResourcesTableAdapter.ClearBeforeFill = True
            '
            'frmCalendrier
            '
            resources.ApplyResources(Me, "$this")
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.Controls.Add(Me.layoutControl1)
            Me.Controls.Add(Me.ribbonControl1)
            Me.Name = "frmCalendrier"
            Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
            CType(Me.schedulerStorage1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.AppointmentsBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.IDScomptabiliteDataSet, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.IDScomptabiliteDataSetBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.layoutControl1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.layoutControl1.ResumeLayout(False)
            CType(Me.dateNavigator1.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.dateNavigator1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.schedulerControl1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.ribbonControl1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.repositoryItemSpinEdit1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.repositoryItemDuration1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.layoutControlGroup1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.layoutControlItem1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.layoutControlItem2, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.splitterItem1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.schedulerBarController1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.ResourcesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)
            Me.PerformLayout()

        End Sub

#End Region

        Private schedulerStorage1 As DevExpress.XtraScheduler.SchedulerStorage
        Private layoutControl1 As DevExpress.XtraLayout.LayoutControl
        Private dateNavigator1 As DevExpress.XtraScheduler.DateNavigator
        Private schedulerControl1 As DevExpress.XtraScheduler.SchedulerControl
        Private layoutControlGroup1 As DevExpress.XtraLayout.LayoutControlGroup
        Private layoutControlItem1 As DevExpress.XtraLayout.LayoutControlItem
        Private layoutControlItem2 As DevExpress.XtraLayout.LayoutControlItem
        Private splitterItem1 As DevExpress.XtraLayout.SplitterItem
        Private ribbonControl1 As DevExpress.XtraBars.Ribbon.RibbonControl
        Private openScheduleItem1 As DevExpress.XtraScheduler.UI.OpenScheduleItem
        Private saveScheduleItem1 As DevExpress.XtraScheduler.UI.SaveScheduleItem
        Private printPreviewItem1 As DevExpress.XtraScheduler.UI.PrintPreviewItem
        Private printItem1 As DevExpress.XtraScheduler.UI.PrintItem
        Private printPageSetupItem1 As DevExpress.XtraScheduler.UI.PrintPageSetupItem
        Private newAppointmentItem1 As DevExpress.XtraScheduler.UI.NewAppointmentItem
        Private newRecurringAppointmentItem1 As DevExpress.XtraScheduler.UI.NewRecurringAppointmentItem
        Private navigateViewBackwardItem1 As DevExpress.XtraScheduler.UI.NavigateViewBackwardItem
        Private navigateViewForwardItem1 As DevExpress.XtraScheduler.UI.NavigateViewForwardItem
        Private gotoTodayItem1 As DevExpress.XtraScheduler.UI.GotoTodayItem
        Private viewZoomInItem1 As DevExpress.XtraScheduler.UI.ViewZoomInItem
        Private viewZoomOutItem1 As DevExpress.XtraScheduler.UI.ViewZoomOutItem
        Private switchToDayViewItem1 As DevExpress.XtraScheduler.UI.SwitchToDayViewItem
        Private switchToWorkWeekViewItem1 As DevExpress.XtraScheduler.UI.SwitchToWorkWeekViewItem
        Private switchToWeekViewItem1 As DevExpress.XtraScheduler.UI.SwitchToWeekViewItem
        Private switchToFullWeekViewItem1 As DevExpress.XtraScheduler.UI.SwitchToFullWeekViewItem
        Private switchToMonthViewItem1 As DevExpress.XtraScheduler.UI.SwitchToMonthViewItem
        Private switchToTimelineViewItem1 As DevExpress.XtraScheduler.UI.SwitchToTimelineViewItem
        Private switchToGanttViewItem1 As DevExpress.XtraScheduler.UI.SwitchToGanttViewItem
        Private switchToAgendaViewItem1 As DevExpress.XtraScheduler.UI.SwitchToAgendaViewItem
        Private groupByNoneItem1 As DevExpress.XtraScheduler.UI.GroupByNoneItem
        Private groupByDateItem1 As DevExpress.XtraScheduler.UI.GroupByDateItem
        Private groupByResourceItem1 As DevExpress.XtraScheduler.UI.GroupByResourceItem
        Private switchTimeScalesItem1 As DevExpress.XtraScheduler.UI.SwitchTimeScalesItem
        Private changeScaleWidthItem1 As DevExpress.XtraScheduler.UI.ChangeScaleWidthItem
        Private repositoryItemSpinEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit
        Private switchTimeScalesCaptionItem1 As DevExpress.XtraScheduler.UI.SwitchTimeScalesCaptionItem
        Private switchCompressWeekendItem1 As DevExpress.XtraScheduler.UI.SwitchCompressWeekendItem
        Private switchShowWorkTimeOnlyItem1 As DevExpress.XtraScheduler.UI.SwitchShowWorkTimeOnlyItem
        Private switchCellsAutoHeightItem1 As DevExpress.XtraScheduler.UI.SwitchCellsAutoHeightItem
        Private changeSnapToCellsUIItem1 As DevExpress.XtraScheduler.UI.ChangeSnapToCellsUIItem
        Private editAppointmentQueryItem1 As DevExpress.XtraScheduler.UI.EditAppointmentQueryItem
        Private editOccurrenceUICommandItem1 As DevExpress.XtraScheduler.UI.EditOccurrenceUICommandItem
        Private editSeriesUICommandItem1 As DevExpress.XtraScheduler.UI.EditSeriesUICommandItem
        Private deleteAppointmentsItem1 As DevExpress.XtraScheduler.UI.DeleteAppointmentsItem
        Private deleteOccurrenceItem1 As DevExpress.XtraScheduler.UI.DeleteOccurrenceItem
        Private deleteSeriesItem1 As DevExpress.XtraScheduler.UI.DeleteSeriesItem
        Private splitAppointmentItem1 As DevExpress.XtraScheduler.UI.SplitAppointmentItem
        Private changeAppointmentStatusItem1 As DevExpress.XtraScheduler.UI.ChangeAppointmentStatusItem
        Private changeAppointmentLabelItem1 As DevExpress.XtraScheduler.UI.ChangeAppointmentLabelItem
        Private toggleRecurrenceItem1 As DevExpress.XtraScheduler.UI.ToggleRecurrenceItem
        Private changeAppointmentReminderItem1 As DevExpress.XtraScheduler.UI.ChangeAppointmentReminderItem
        Private repositoryItemDuration1 As DevExpress.XtraScheduler.UI.RepositoryItemDuration
        Private calendarToolsRibbonPageCategory1 As DevExpress.XtraScheduler.UI.CalendarToolsRibbonPageCategory
        Private appointmentRibbonPage1 As DevExpress.XtraScheduler.UI.AppointmentRibbonPage
        Private actionsRibbonPageGroup1 As DevExpress.XtraScheduler.UI.ActionsRibbonPageGroup
        Private optionsRibbonPageGroup1 As DevExpress.XtraScheduler.UI.OptionsRibbonPageGroup
        Private fileRibbonPage1 As DevExpress.XtraScheduler.UI.FileRibbonPage
        Private commonRibbonPageGroup1 As DevExpress.XtraScheduler.UI.CommonRibbonPageGroup
        Private printRibbonPageGroup1 As DevExpress.XtraScheduler.UI.PrintRibbonPageGroup
        Private homeRibbonPage1 As DevExpress.XtraScheduler.UI.HomeRibbonPage
        Private appointmentRibbonPageGroup1 As DevExpress.XtraScheduler.UI.AppointmentRibbonPageGroup
        Private navigatorRibbonPageGroup1 As DevExpress.XtraScheduler.UI.NavigatorRibbonPageGroup
        Private arrangeRibbonPageGroup1 As DevExpress.XtraScheduler.UI.ArrangeRibbonPageGroup
        Private groupByRibbonPageGroup1 As DevExpress.XtraScheduler.UI.GroupByRibbonPageGroup
        Private viewRibbonPage1 As DevExpress.XtraScheduler.UI.ViewRibbonPage
        Private activeViewRibbonPageGroup1 As DevExpress.XtraScheduler.UI.ActiveViewRibbonPageGroup
        Private timeScaleRibbonPageGroup1 As DevExpress.XtraScheduler.UI.TimeScaleRibbonPageGroup
        Private layoutRibbonPageGroup1 As DevExpress.XtraScheduler.UI.LayoutRibbonPageGroup
        Private schedulerBarController1 As DevExpress.XtraScheduler.UI.SchedulerBarController
        Friend WithEvents IDScomptabiliteDataSet As IDScomptabiliteDataSet
        Friend WithEvents AppointmentsBindingSource As BindingSource
        Friend WithEvents AppointmentsTableAdapter As IDScomptabiliteDataSetTableAdapters.AppointmentsTableAdapter
        Friend WithEvents ResourcesBindingSource As BindingSource
        Friend WithEvents ResourcesTableAdapter As IDScomptabiliteDataSetTableAdapters.ResourcesTableAdapter
        Friend WithEvents IDScomptabiliteDataSetBindingSource As BindingSource
        Friend WithEvents bExit As DevExpress.XtraBars.BarButtonItem
    End Class
End Namespace
