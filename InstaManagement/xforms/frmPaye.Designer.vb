﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPaye
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmPaye))
        Dim GridFormatRule1 As DevExpress.XtraGrid.GridFormatRule = New DevExpress.XtraGrid.GridFormatRule()
        Dim FormatConditionRuleValue1 As DevExpress.XtraEditors.FormatConditionRuleValue = New DevExpress.XtraEditors.FormatConditionRuleValue()
        Me.colDistance = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.TS = New System.Windows.Forms.ToolStrip()
        Me.Head = New System.Windows.Forms.ToolStripLabel()
        Me.sp1 = New System.Windows.Forms.ToolStripSeparator()
        Me.bDelete = New System.Windows.Forms.ToolStripButton()
        Me.bNouvellePaye = New System.Windows.Forms.ToolStripButton()
        Me.sp3 = New System.Windows.Forms.ToolStripSeparator()
        Me.bAjouterLaPaye = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.bCancel = New System.Windows.Forms.ToolStripButton()
        Me.sp4 = New System.Windows.Forms.ToolStripSeparator()
        Me.ToolStripButton6 = New System.Windows.Forms.ToolStripButton()
        Me.LayoutControl1 = New DevExpress.XtraLayout.LayoutControl()
        Me.tabData = New DevExpress.XtraTab.XtraTabControl()
        Me.XtraTabPage1 = New DevExpress.XtraTab.XtraTabPage()
        Me.LayoutControl2 = New DevExpress.XtraLayout.LayoutControl()
        Me.grdPaye = New DevExpress.XtraGrid.GridControl()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colID = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colPayeNumber = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colPayeDate = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colIDLivreur = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colLivreur = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colDateFrom = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colDateTo = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.LayoutControlGroup1 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem2 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.XtraTabPage2 = New DevExpress.XtraTab.XtraTabPage()
        Me.LayoutControl3 = New DevExpress.XtraLayout.LayoutControl()
        Me.bExportToHTML = New DevExpress.XtraEditors.SimpleButton()
        Me.bExportToPDF = New DevExpress.XtraEditors.SimpleButton()
        Me.bExportToDoc = New DevExpress.XtraEditors.SimpleButton()
        Me.bExportToExcel = New DevExpress.XtraEditors.SimpleButton()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.grdImport = New DevExpress.XtraGrid.GridControl()
        Me.GridView3 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colImportID = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn2 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colReference = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colClient = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemMemoEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit()
        Me.colClientAddress = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colMomentDePassage = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colPreparePar = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colHeureAppel = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colHeurePrep = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colHeureDepart = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colHeureLivraison = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colMessagePassage = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemMemoEdit3 = New DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit()
        Me.colTotalTempService = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn3 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn4 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemMemoEdit2 = New DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit()
        Me.GridColumn5 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn6 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn7 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colMontantGlacierLivreur = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colMontantGlacierOrganisation = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colMontantLivreur = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.Extras = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colExtraKM = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemCalcEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit()
        Me.RepositoryItemTextEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit()
        Me.RepositoryItemTextEdit2 = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit()
        Me.RepositoryItemCalcEdit2 = New DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit()
        Me.RepositoryItemSpinEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit()
        Me.RepositoryItemCheckEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit()
        Me.grdPayeLine = New DevExpress.XtraGrid.GridControl()
        Me.GridView2 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colIDInvoiceLine = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colIDPaye = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colPayeNumber2 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colDateFrom2 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colDateTo2 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colIDLivreur2 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colLivreur2 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colOrganisation = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCredit = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colNombreGlacier = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colGlacier = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colKm = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colDescription = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colUnite = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colPrix = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colMontant = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colNombreGlacier2 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colTotalGlacier = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.LayoutControlGroup2 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem1 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem4 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem5 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem9 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem7 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem8 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem6 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.Root = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem3 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.DockManager1 = New DevExpress.XtraBars.Docking.DockManager(Me.components)
        Me.DockNouvellePaye = New DevExpress.XtraBars.Docking.DockPanel()
        Me.ControlContainer1 = New DevExpress.XtraBars.Docking.ControlContainer()
        Me.LayoutControl5 = New DevExpress.XtraLayout.LayoutControl()
        Me.bAnnulerNouvellePaye = New DevExpress.XtraEditors.SimpleButton()
        Me.bSauvegarderNouvellePaye = New DevExpress.XtraEditors.SimpleButton()
        Me.tbPayeNumberNP = New DevExpress.XtraEditors.TextEdit()
        Me.cbLivreurNP = New DevExpress.XtraEditors.LookUpEdit()
        Me.tbDateFromNP = New DevExpress.XtraEditors.DateEdit()
        Me.tbDateAuNP = New DevExpress.XtraEditors.DateEdit()
        Me.cbPeriodesNP = New DevExpress.XtraEditors.LookUpEdit()
        Me.tbIDPayeNP = New DevExpress.XtraEditors.TextEdit()
        Me.tbPayeDateNP = New DevExpress.XtraEditors.DateEdit()
        Me.LayoutControlGroup4 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem23 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem24 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem25 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem26 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem28 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem29 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem30 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem31 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem11 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.DockPaye = New DevExpress.XtraBars.Docking.DockPanel()
        Me.DockPanel1_Container = New DevExpress.XtraBars.Docking.ControlContainer()
        Me.LayoutControl4 = New DevExpress.XtraLayout.LayoutControl()
        Me.chkAjout = New DevExpress.XtraEditors.CheckEdit()
        Me.radChoice = New DevExpress.XtraEditors.RadioGroup()
        Me.btnAnnuler = New DevExpress.XtraEditors.SimpleButton()
        Me.bSauvegarde = New DevExpress.XtraEditors.SimpleButton()
        Me.tbPayeNumber = New DevExpress.XtraEditors.TextEdit()
        Me.tbDescription = New DevExpress.XtraEditors.TextEdit()
        Me.tbPrix = New DevExpress.XtraEditors.TextEdit()
        Me.tbTotal = New DevExpress.XtraEditors.TextEdit()
        Me.tbQuantite = New DevExpress.XtraEditors.TextEdit()
        Me.cbOrganisation = New DevExpress.XtraEditors.LookUpEdit()
        Me.cbLivreur = New DevExpress.XtraEditors.LookUpEdit()
        Me.tbDateFrom = New DevExpress.XtraEditors.DateEdit()
        Me.tbDateAu = New DevExpress.XtraEditors.DateEdit()
        Me.cbPeriodes = New DevExpress.XtraEditors.LookUpEdit()
        Me.tbIDPaye = New DevExpress.XtraEditors.TextEdit()
        Me.tbPayeDate = New DevExpress.XtraEditors.DateEdit()
        Me.tbIDPayeLine = New DevExpress.XtraEditors.TextEdit()
        Me.LayoutControlItem19 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlGroup3 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.EmptySpaceItem1 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutSave = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem16 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutNombreFree2 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutNombreFree1 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem10 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem18 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem15 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem13 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem14 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem20 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem27 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem12 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.lblQuantite = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutPayeAuLivreur = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutPayeAuLivreur1 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem21 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem32 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.btnSauvegarde = New DevExpress.XtraEditors.SimpleButton()
        Me.TS.SuspendLayout()
        CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LayoutControl1.SuspendLayout()
        CType(Me.tabData, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabData.SuspendLayout()
        Me.XtraTabPage1.SuspendLayout()
        CType(Me.LayoutControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LayoutControl2.SuspendLayout()
        CType(Me.grdPaye, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabPage2.SuspendLayout()
        CType(Me.LayoutControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LayoutControl3.SuspendLayout()
        CType(Me.grdImport, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemMemoEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemMemoEdit3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemMemoEdit2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemCalcEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemTextEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemTextEdit2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemCalcEdit2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemSpinEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemCheckEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grdPayeLine, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Root, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DockManager1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.DockNouvellePaye.SuspendLayout()
        Me.ControlContainer1.SuspendLayout()
        CType(Me.LayoutControl5, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LayoutControl5.SuspendLayout()
        CType(Me.tbPayeNumberNP.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbLivreurNP.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbDateFromNP.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbDateFromNP.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbDateAuNP.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbDateAuNP.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbPeriodesNP.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbIDPayeNP.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbPayeDateNP.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbPayeDateNP.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem23, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem24, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem25, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem26, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem28, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem29, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem30, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem31, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem11, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.DockPaye.SuspendLayout()
        Me.DockPanel1_Container.SuspendLayout()
        CType(Me.LayoutControl4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LayoutControl4.SuspendLayout()
        CType(Me.chkAjout.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radChoice.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbPayeNumber.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbDescription.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbPrix.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbTotal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbQuantite.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbOrganisation.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbLivreur.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbDateFrom.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbDateFrom.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbDateAu.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbDateAu.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbPeriodes.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbIDPaye.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbPayeDate.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbPayeDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbIDPayeLine.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem19, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutSave, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem16, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutNombreFree2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutNombreFree1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem18, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem15, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem13, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem14, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem20, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem27, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblQuantite, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutPayeAuLivreur, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutPayeAuLivreur1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem21, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem32, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'colDistance
        '
        Me.colDistance.AppearanceCell.Options.UseTextOptions = True
        Me.colDistance.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center
        Me.colDistance.Caption = "Distance"
        Me.colDistance.FieldName = "Distance"
        Me.colDistance.Name = "colDistance"
        Me.colDistance.OptionsColumn.AllowEdit = False
        Me.colDistance.OptionsColumn.AllowFocus = False
        Me.colDistance.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Distance", "SUM={0:0.##}")})
        Me.colDistance.Visible = True
        Me.colDistance.VisibleIndex = 14
        Me.colDistance.Width = 92
        '
        'TS
        '
        Me.TS.AutoSize = False
        Me.TS.Font = New System.Drawing.Font("Verdana", 9.75!)
        Me.TS.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Head, Me.sp1, Me.bDelete, Me.bNouvellePaye, Me.sp3, Me.bAjouterLaPaye, Me.ToolStripSeparator2, Me.bCancel, Me.sp4, Me.ToolStripButton6})
        Me.TS.Location = New System.Drawing.Point(0, 0)
        Me.TS.Name = "TS"
        Me.TS.Size = New System.Drawing.Size(1379, 39)
        Me.TS.TabIndex = 8
        '
        'Head
        '
        Me.Head.Name = "Head"
        Me.Head.Size = New System.Drawing.Size(0, 36)
        '
        'sp1
        '
        Me.sp1.Name = "sp1"
        Me.sp1.Size = New System.Drawing.Size(6, 39)
        '
        'bDelete
        '
        Me.bDelete.Image = CType(resources.GetObject("bDelete.Image"), System.Drawing.Image)
        Me.bDelete.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.bDelete.Name = "bDelete"
        Me.bDelete.Size = New System.Drawing.Size(93, 36)
        Me.bDelete.Text = "Supprimer"
        Me.bDelete.Visible = False
        '
        'bNouvellePaye
        '
        Me.bNouvellePaye.Image = CType(resources.GetObject("bNouvellePaye.Image"), System.Drawing.Image)
        Me.bNouvellePaye.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.bNouvellePaye.Name = "bNouvellePaye"
        Me.bNouvellePaye.Size = New System.Drawing.Size(120, 36)
        Me.bNouvellePaye.Text = "Nouvelle paye"
        '
        'sp3
        '
        Me.sp3.Name = "sp3"
        Me.sp3.Size = New System.Drawing.Size(6, 39)
        '
        'bAjouterLaPaye
        '
        Me.bAjouterLaPaye.Image = CType(resources.GetObject("bAjouterLaPaye.Image"), System.Drawing.Image)
        Me.bAjouterLaPaye.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.bAjouterLaPaye.Name = "bAjouterLaPaye"
        Me.bAjouterLaPaye.Size = New System.Drawing.Size(193, 36)
        Me.bAjouterLaPaye.Text = "Ajouter une ligne de paie"
        Me.bAjouterLaPaye.Visible = False
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        Me.ToolStripSeparator2.Size = New System.Drawing.Size(6, 39)
        '
        'bCancel
        '
        Me.bCancel.Image = CType(resources.GetObject("bCancel.Image"), System.Drawing.Image)
        Me.bCancel.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.bCancel.Name = "bCancel"
        Me.bCancel.Size = New System.Drawing.Size(204, 36)
        Me.bCancel.Text = "Retour a la liste des payes"
        Me.bCancel.Visible = False
        '
        'sp4
        '
        Me.sp4.Name = "sp4"
        Me.sp4.Size = New System.Drawing.Size(6, 39)
        '
        'ToolStripButton6
        '
        Me.ToolStripButton6.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.ToolStripButton6.Image = CType(resources.GetObject("ToolStripButton6.Image"), System.Drawing.Image)
        Me.ToolStripButton6.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton6.Name = "ToolStripButton6"
        Me.ToolStripButton6.Size = New System.Drawing.Size(73, 36)
        Me.ToolStripButton6.Text = "Fermer"
        '
        'LayoutControl1
        '
        Me.LayoutControl1.Controls.Add(Me.tabData)
        Me.LayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LayoutControl1.Location = New System.Drawing.Point(0, 39)
        Me.LayoutControl1.Name = "LayoutControl1"
        Me.LayoutControl1.Root = Me.Root
        Me.LayoutControl1.Size = New System.Drawing.Size(1379, 551)
        Me.LayoutControl1.TabIndex = 9
        Me.LayoutControl1.Text = "LayoutControl1"
        '
        'tabData
        '
        Me.tabData.Location = New System.Drawing.Point(12, 12)
        Me.tabData.Name = "tabData"
        Me.tabData.SelectedTabPage = Me.XtraTabPage1
        Me.tabData.Size = New System.Drawing.Size(1355, 527)
        Me.tabData.TabIndex = 9
        Me.tabData.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.XtraTabPage1, Me.XtraTabPage2})
        '
        'XtraTabPage1
        '
        Me.XtraTabPage1.Controls.Add(Me.LayoutControl2)
        Me.XtraTabPage1.Name = "XtraTabPage1"
        Me.XtraTabPage1.Size = New System.Drawing.Size(1350, 501)
        Me.XtraTabPage1.Text = "Liste des payes"
        '
        'LayoutControl2
        '
        Me.LayoutControl2.Controls.Add(Me.grdPaye)
        Me.LayoutControl2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LayoutControl2.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControl2.Name = "LayoutControl2"
        Me.LayoutControl2.Root = Me.LayoutControlGroup1
        Me.LayoutControl2.Size = New System.Drawing.Size(1350, 501)
        Me.LayoutControl2.TabIndex = 0
        Me.LayoutControl2.Text = "LayoutControl2"
        '
        'grdPaye
        '
        Me.grdPaye.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grdPaye.Location = New System.Drawing.Point(12, 12)
        Me.grdPaye.MainView = Me.GridView1
        Me.grdPaye.Name = "grdPaye"
        Me.grdPaye.Size = New System.Drawing.Size(1326, 477)
        Me.grdPaye.TabIndex = 5
        Me.grdPaye.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'GridView1
        '
        Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colID, Me.colPayeNumber, Me.colPayeDate, Me.colIDLivreur, Me.colLivreur, Me.colDateFrom, Me.colDateTo})
        Me.GridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.GridView1.GridControl = Me.grdPaye
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsBehavior.Editable = False
        Me.GridView1.OptionsCustomization.AllowGroup = False
        Me.GridView1.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridView1.OptionsSelection.UseIndicatorForSelection = False
        Me.GridView1.OptionsView.ShowAutoFilterRow = True
        Me.GridView1.OptionsView.ShowGroupPanel = False
        Me.GridView1.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.colPayeNumber, DevExpress.Data.ColumnSortOrder.Ascending)})
        '
        'colID
        '
        Me.colID.Caption = "ID"
        Me.colID.FieldName = "IDPaye"
        Me.colID.Name = "colID"
        Me.colID.Visible = True
        Me.colID.VisibleIndex = 0
        Me.colID.Width = 94
        '
        'colPayeNumber
        '
        Me.colPayeNumber.Caption = "Numéro de Paie"
        Me.colPayeNumber.FieldName = "PayeNumber"
        Me.colPayeNumber.Name = "colPayeNumber"
        Me.colPayeNumber.Visible = True
        Me.colPayeNumber.VisibleIndex = 2
        Me.colPayeNumber.Width = 94
        '
        'colPayeDate
        '
        Me.colPayeDate.Caption = "Date de paie"
        Me.colPayeDate.FieldName = "PayeDate"
        Me.colPayeDate.Name = "colPayeDate"
        Me.colPayeDate.Visible = True
        Me.colPayeDate.VisibleIndex = 3
        Me.colPayeDate.Width = 94
        '
        'colIDLivreur
        '
        Me.colIDLivreur.Caption = "ID livreur"
        Me.colIDLivreur.FieldName = "IDLivreur"
        Me.colIDLivreur.Name = "colIDLivreur"
        '
        'colLivreur
        '
        Me.colLivreur.Caption = "Livreur"
        Me.colLivreur.FieldName = "Livreur"
        Me.colLivreur.Name = "colLivreur"
        Me.colLivreur.Visible = True
        Me.colLivreur.VisibleIndex = 1
        '
        'colDateFrom
        '
        Me.colDateFrom.Caption = "Période du"
        Me.colDateFrom.FieldName = "DateFrom"
        Me.colDateFrom.Name = "colDateFrom"
        Me.colDateFrom.Visible = True
        Me.colDateFrom.VisibleIndex = 4
        '
        'colDateTo
        '
        Me.colDateTo.Caption = "Période au"
        Me.colDateTo.FieldName = "DateTo"
        Me.colDateTo.Name = "colDateTo"
        Me.colDateTo.Visible = True
        Me.colDateTo.VisibleIndex = 5
        '
        'LayoutControlGroup1
        '
        Me.LayoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup1.GroupBordersVisible = False
        Me.LayoutControlGroup1.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem2})
        Me.LayoutControlGroup1.Name = "Root"
        Me.LayoutControlGroup1.Size = New System.Drawing.Size(1350, 501)
        Me.LayoutControlGroup1.TextVisible = False
        '
        'LayoutControlItem2
        '
        Me.LayoutControlItem2.Control = Me.grdPaye
        Me.LayoutControlItem2.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem2.Name = "LayoutControlItem2"
        Me.LayoutControlItem2.Size = New System.Drawing.Size(1330, 481)
        Me.LayoutControlItem2.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem2.TextVisible = False
        '
        'XtraTabPage2
        '
        Me.XtraTabPage2.Controls.Add(Me.LayoutControl3)
        Me.XtraTabPage2.Name = "XtraTabPage2"
        Me.XtraTabPage2.PageVisible = False
        Me.XtraTabPage2.Size = New System.Drawing.Size(1350, 501)
        Me.XtraTabPage2.Text = "Détail sur une Paye"
        '
        'LayoutControl3
        '
        Me.LayoutControl3.Controls.Add(Me.bExportToHTML)
        Me.LayoutControl3.Controls.Add(Me.bExportToPDF)
        Me.LayoutControl3.Controls.Add(Me.bExportToDoc)
        Me.LayoutControl3.Controls.Add(Me.bExportToExcel)
        Me.LayoutControl3.Controls.Add(Me.LabelControl1)
        Me.LayoutControl3.Controls.Add(Me.grdImport)
        Me.LayoutControl3.Controls.Add(Me.grdPayeLine)
        Me.LayoutControl3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LayoutControl3.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControl3.Name = "LayoutControl3"
        Me.LayoutControl3.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = New System.Drawing.Rectangle(976, 223, 771, 350)
        Me.LayoutControl3.OptionsCustomizationForm.ShowPropertyGrid = True
        Me.LayoutControl3.Root = Me.LayoutControlGroup2
        Me.LayoutControl3.Size = New System.Drawing.Size(1350, 501)
        Me.LayoutControl3.TabIndex = 0
        Me.LayoutControl3.Text = "LayoutControl1"
        '
        'bExportToHTML
        '
        Me.bExportToHTML.Location = New System.Drawing.Point(1054, 185)
        Me.bExportToHTML.Name = "bExportToHTML"
        Me.bExportToHTML.Size = New System.Drawing.Size(284, 22)
        Me.bExportToHTML.StyleController = Me.LayoutControl3
        Me.bExportToHTML.TabIndex = 47
        Me.bExportToHTML.Text = "Exporter vers HTML"
        '
        'bExportToPDF
        '
        Me.bExportToPDF.Location = New System.Drawing.Point(368, 185)
        Me.bExportToPDF.Name = "bExportToPDF"
        Me.bExportToPDF.Size = New System.Drawing.Size(361, 22)
        Me.bExportToPDF.StyleController = Me.LayoutControl3
        Me.bExportToPDF.TabIndex = 46
        Me.bExportToPDF.Text = "Exporter vers PDF"
        '
        'bExportToDoc
        '
        Me.bExportToDoc.Location = New System.Drawing.Point(733, 185)
        Me.bExportToDoc.Name = "bExportToDoc"
        Me.bExportToDoc.Size = New System.Drawing.Size(317, 22)
        Me.bExportToDoc.StyleController = Me.LayoutControl3
        Me.bExportToDoc.TabIndex = 45
        Me.bExportToDoc.Text = "Exporter vers Doc"
        '
        'bExportToExcel
        '
        Me.bExportToExcel.Location = New System.Drawing.Point(12, 185)
        Me.bExportToExcel.Name = "bExportToExcel"
        Me.bExportToExcel.Size = New System.Drawing.Size(352, 22)
        Me.bExportToExcel.StyleController = Me.LayoutControl3
        Me.bExportToExcel.TabIndex = 44
        Me.bExportToExcel.Text = "Exporter vers Excel"
        '
        'LabelControl1
        '
        Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Tahoma", 15.0!)
        Me.LabelControl1.Appearance.Options.UseFont = True
        Me.LabelControl1.Appearance.Options.UseTextOptions = True
        Me.LabelControl1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.LabelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl1.Location = New System.Drawing.Point(12, 157)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(1326, 24)
        Me.LabelControl1.StyleController = Me.LayoutControl3
        Me.LabelControl1.TabIndex = 29
        Me.LabelControl1.Text = "Détails de la paye"
        '
        'grdImport
        '
        Me.grdImport.Location = New System.Drawing.Point(12, 211)
        Me.grdImport.MainView = Me.GridView3
        Me.grdImport.Name = "grdImport"
        Me.grdImport.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemCalcEdit1, Me.RepositoryItemTextEdit1, Me.RepositoryItemTextEdit2, Me.RepositoryItemMemoEdit1, Me.RepositoryItemMemoEdit2, Me.RepositoryItemMemoEdit3, Me.RepositoryItemCalcEdit2, Me.RepositoryItemSpinEdit1, Me.RepositoryItemCheckEdit1})
        Me.grdImport.Size = New System.Drawing.Size(1326, 278)
        Me.grdImport.TabIndex = 28
        Me.grdImport.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView3})
        '
        'GridView3
        '
        Me.GridView3.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colImportID, Me.GridColumn2, Me.colReference, Me.colClient, Me.colClientAddress, Me.colMomentDePassage, Me.colPreparePar, Me.colHeureAppel, Me.colHeurePrep, Me.colHeureDepart, Me.colHeureLivraison, Me.colMessagePassage, Me.colTotalTempService, Me.GridColumn1, Me.colDistance, Me.GridColumn3, Me.GridColumn4, Me.GridColumn5, Me.GridColumn6, Me.GridColumn7, Me.colMontantGlacierLivreur, Me.colMontantGlacierOrganisation, Me.colMontantLivreur, Me.Extras, Me.colExtraKM})
        GridFormatRule1.ApplyToRow = True
        GridFormatRule1.Column = Me.colDistance
        GridFormatRule1.ColumnApplyTo = Me.colDistance
        GridFormatRule1.Name = "Format0"
        FormatConditionRuleValue1.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer))
        FormatConditionRuleValue1.Appearance.Options.UseBackColor = True
        FormatConditionRuleValue1.Condition = DevExpress.XtraEditors.FormatCondition.Equal
        FormatConditionRuleValue1.PredefinedName = "Red Fill"
        FormatConditionRuleValue1.Value1 = "0"
        GridFormatRule1.Rule = FormatConditionRuleValue1
        Me.GridView3.FormatRules.Add(GridFormatRule1)
        Me.GridView3.GridControl = Me.grdImport
        Me.GridView3.Name = "GridView3"
        Me.GridView3.OptionsCustomization.AllowGroup = False
        Me.GridView3.OptionsView.RowAutoHeight = True
        Me.GridView3.OptionsView.ShowAutoFilterRow = True
        Me.GridView3.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.ShowAlways
        Me.GridView3.OptionsView.ShowFooter = True
        Me.GridView3.OptionsView.ShowGroupPanel = False
        '
        'colImportID
        '
        Me.colImportID.AppearanceCell.Options.UseTextOptions = True
        Me.colImportID.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center
        Me.colImportID.Caption = "Import ID"
        Me.colImportID.FieldName = "ImportID"
        Me.colImportID.Name = "colImportID"
        Me.colImportID.OptionsColumn.AllowEdit = False
        Me.colImportID.OptionsColumn.AllowFocus = False
        Me.colImportID.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count, "ImportID", "{0}")})
        Me.colImportID.Visible = True
        Me.colImportID.VisibleIndex = 0
        Me.colImportID.Width = 36
        '
        'GridColumn2
        '
        Me.GridColumn2.Caption = "Code"
        Me.GridColumn2.FieldName = "Code"
        Me.GridColumn2.Name = "GridColumn2"
        Me.GridColumn2.OptionsColumn.AllowEdit = False
        Me.GridColumn2.OptionsColumn.AllowFocus = False
        Me.GridColumn2.Visible = True
        Me.GridColumn2.VisibleIndex = 1
        Me.GridColumn2.Width = 92
        '
        'colReference
        '
        Me.colReference.Caption = "Reference"
        Me.colReference.FieldName = "Reference"
        Me.colReference.Name = "colReference"
        Me.colReference.OptionsColumn.AllowFocus = False
        '
        'colClient
        '
        Me.colClient.AppearanceCell.Options.UseTextOptions = True
        Me.colClient.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center
        Me.colClient.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.colClient.Caption = "Client"
        Me.colClient.ColumnEdit = Me.RepositoryItemMemoEdit1
        Me.colClient.CustomizationCaption = "Client"
        Me.colClient.FieldName = "Client"
        Me.colClient.Name = "colClient"
        Me.colClient.OptionsColumn.AllowEdit = False
        Me.colClient.OptionsColumn.AllowFocus = False
        Me.colClient.Visible = True
        Me.colClient.VisibleIndex = 2
        Me.colClient.Width = 92
        '
        'RepositoryItemMemoEdit1
        '
        Me.RepositoryItemMemoEdit1.Name = "RepositoryItemMemoEdit1"
        '
        'colClientAddress
        '
        Me.colClientAddress.AppearanceCell.Options.UseTextOptions = True
        Me.colClientAddress.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center
        Me.colClientAddress.Caption = "ClientAddress"
        Me.colClientAddress.CustomizationCaption = "ClientAddress"
        Me.colClientAddress.FieldName = "ClientAddress"
        Me.colClientAddress.Name = "colClientAddress"
        Me.colClientAddress.OptionsColumn.AllowEdit = False
        Me.colClientAddress.OptionsColumn.AllowFocus = False
        '
        'colMomentDePassage
        '
        Me.colMomentDePassage.AppearanceCell.Options.UseTextOptions = True
        Me.colMomentDePassage.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center
        Me.colMomentDePassage.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.colMomentDePassage.Caption = "MomentDePassage"
        Me.colMomentDePassage.FieldName = "MomentDePassage"
        Me.colMomentDePassage.Name = "colMomentDePassage"
        Me.colMomentDePassage.OptionsColumn.AllowEdit = False
        Me.colMomentDePassage.OptionsColumn.AllowFocus = False
        '
        'colPreparePar
        '
        Me.colPreparePar.AppearanceCell.Options.UseTextOptions = True
        Me.colPreparePar.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center
        Me.colPreparePar.AppearanceHeader.Options.UseTextOptions = True
        Me.colPreparePar.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.colPreparePar.Caption = "Préparé par"
        Me.colPreparePar.CustomizationCaption = "Prepare Par"
        Me.colPreparePar.FieldName = "PreparePar"
        Me.colPreparePar.Name = "colPreparePar"
        Me.colPreparePar.OptionsColumn.AllowEdit = False
        Me.colPreparePar.OptionsColumn.AllowFocus = False
        '
        'colHeureAppel
        '
        Me.colHeureAppel.AppearanceCell.Options.UseTextOptions = True
        Me.colHeureAppel.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center
        Me.colHeureAppel.AppearanceHeader.Options.UseTextOptions = True
        Me.colHeureAppel.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.colHeureAppel.Caption = "HeureAppel"
        Me.colHeureAppel.DisplayFormat.FormatString = "g"
        Me.colHeureAppel.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.colHeureAppel.FieldName = "HeureAppel"
        Me.colHeureAppel.Name = "colHeureAppel"
        Me.colHeureAppel.OptionsColumn.AllowEdit = False
        Me.colHeureAppel.OptionsColumn.AllowFocus = False
        '
        'colHeurePrep
        '
        Me.colHeurePrep.AppearanceCell.Options.UseTextOptions = True
        Me.colHeurePrep.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center
        Me.colHeurePrep.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.colHeurePrep.AppearanceHeader.Options.UseTextOptions = True
        Me.colHeurePrep.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.colHeurePrep.Caption = "Préparation"
        Me.colHeurePrep.DisplayFormat.FormatString = "g"
        Me.colHeurePrep.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.colHeurePrep.FieldName = "HeurePrep"
        Me.colHeurePrep.Name = "colHeurePrep"
        Me.colHeurePrep.OptionsColumn.AllowEdit = False
        Me.colHeurePrep.OptionsColumn.AllowFocus = False
        Me.colHeurePrep.Visible = True
        Me.colHeurePrep.VisibleIndex = 3
        Me.colHeurePrep.Width = 92
        '
        'colHeureDepart
        '
        Me.colHeureDepart.AppearanceCell.Options.UseTextOptions = True
        Me.colHeureDepart.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center
        Me.colHeureDepart.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.colHeureDepart.AppearanceHeader.Options.UseTextOptions = True
        Me.colHeureDepart.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.colHeureDepart.Caption = "Départ"
        Me.colHeureDepart.DisplayFormat.FormatString = "g"
        Me.colHeureDepart.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.colHeureDepart.FieldName = "HeureDepart"
        Me.colHeureDepart.Name = "colHeureDepart"
        Me.colHeureDepart.OptionsColumn.AllowEdit = False
        Me.colHeureDepart.OptionsColumn.AllowFocus = False
        Me.colHeureDepart.Visible = True
        Me.colHeureDepart.VisibleIndex = 4
        Me.colHeureDepart.Width = 92
        '
        'colHeureLivraison
        '
        Me.colHeureLivraison.AppearanceCell.Options.UseTextOptions = True
        Me.colHeureLivraison.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center
        Me.colHeureLivraison.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.colHeureLivraison.AppearanceHeader.Options.UseTextOptions = True
        Me.colHeureLivraison.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.colHeureLivraison.Caption = "Livraison"
        Me.colHeureLivraison.DisplayFormat.FormatString = "g"
        Me.colHeureLivraison.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.colHeureLivraison.FieldName = "HeureLivraison"
        Me.colHeureLivraison.Name = "colHeureLivraison"
        Me.colHeureLivraison.OptionsColumn.AllowEdit = False
        Me.colHeureLivraison.OptionsColumn.AllowFocus = False
        Me.colHeureLivraison.Visible = True
        Me.colHeureLivraison.VisibleIndex = 5
        Me.colHeureLivraison.Width = 92
        '
        'colMessagePassage
        '
        Me.colMessagePassage.AppearanceCell.Options.UseTextOptions = True
        Me.colMessagePassage.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center
        Me.colMessagePassage.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.colMessagePassage.AppearanceHeader.Options.UseTextOptions = True
        Me.colMessagePassage.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.colMessagePassage.Caption = "Message"
        Me.colMessagePassage.ColumnEdit = Me.RepositoryItemMemoEdit3
        Me.colMessagePassage.FieldName = "MessagePassage"
        Me.colMessagePassage.Name = "colMessagePassage"
        Me.colMessagePassage.OptionsColumn.AllowEdit = False
        Me.colMessagePassage.OptionsColumn.AllowFocus = False
        Me.colMessagePassage.Visible = True
        Me.colMessagePassage.VisibleIndex = 6
        Me.colMessagePassage.Width = 92
        '
        'RepositoryItemMemoEdit3
        '
        Me.RepositoryItemMemoEdit3.Name = "RepositoryItemMemoEdit3"
        '
        'colTotalTempService
        '
        Me.colTotalTempService.AppearanceCell.Options.UseTextOptions = True
        Me.colTotalTempService.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center
        Me.colTotalTempService.Caption = "TotalTempService"
        Me.colTotalTempService.FieldName = "TotalTempService"
        Me.colTotalTempService.Name = "colTotalTempService"
        Me.colTotalTempService.OptionsColumn.AllowEdit = False
        Me.colTotalTempService.OptionsColumn.AllowFocus = False
        '
        'GridColumn1
        '
        Me.GridColumn1.AppearanceCell.Options.UseTextOptions = True
        Me.GridColumn1.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center
        Me.GridColumn1.Caption = "Livreur"
        Me.GridColumn1.FieldName = "Livreur"
        Me.GridColumn1.Name = "GridColumn1"
        Me.GridColumn1.OptionsColumn.AllowEdit = False
        Me.GridColumn1.OptionsColumn.AllowFocus = False
        Me.GridColumn1.Visible = True
        Me.GridColumn1.VisibleIndex = 7
        Me.GridColumn1.Width = 92
        '
        'GridColumn3
        '
        Me.GridColumn3.AppearanceCell.Options.UseTextOptions = True
        Me.GridColumn3.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center
        Me.GridColumn3.Caption = "Montant O."
        Me.GridColumn3.DisplayFormat.FormatString = "c"
        Me.GridColumn3.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.GridColumn3.FieldName = "Montant"
        Me.GridColumn3.Name = "GridColumn3"
        Me.GridColumn3.OptionsColumn.AllowEdit = False
        Me.GridColumn3.OptionsColumn.AllowFocus = False
        Me.GridColumn3.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Montant", "SUM={0:c2}")})
        Me.GridColumn3.Visible = True
        Me.GridColumn3.VisibleIndex = 9
        Me.GridColumn3.Width = 92
        '
        'GridColumn4
        '
        Me.GridColumn4.AppearanceCell.Options.UseTextOptions = True
        Me.GridColumn4.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center
        Me.GridColumn4.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.GridColumn4.Caption = "Organisation"
        Me.GridColumn4.ColumnEdit = Me.RepositoryItemMemoEdit2
        Me.GridColumn4.FieldName = "Organisation"
        Me.GridColumn4.Name = "GridColumn4"
        Me.GridColumn4.OptionsColumn.AllowEdit = False
        Me.GridColumn4.OptionsColumn.AllowFocus = False
        Me.GridColumn4.Visible = True
        Me.GridColumn4.VisibleIndex = 8
        Me.GridColumn4.Width = 92
        '
        'RepositoryItemMemoEdit2
        '
        Me.RepositoryItemMemoEdit2.Name = "RepositoryItemMemoEdit2"
        '
        'GridColumn5
        '
        Me.GridColumn5.AppearanceCell.Options.UseTextOptions = True
        Me.GridColumn5.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center
        Me.GridColumn5.Caption = "Débit"
        Me.GridColumn5.DisplayFormat.FormatString = "c"
        Me.GridColumn5.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.GridColumn5.FieldName = "Debit"
        Me.GridColumn5.Name = "GridColumn5"
        Me.GridColumn5.OptionsColumn.AllowEdit = False
        Me.GridColumn5.OptionsColumn.AllowFocus = False
        Me.GridColumn5.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Extra", "SUM={0:c2}")})
        Me.GridColumn5.Visible = True
        Me.GridColumn5.VisibleIndex = 17
        Me.GridColumn5.Width = 92
        '
        'GridColumn6
        '
        Me.GridColumn6.AppearanceCell.Options.UseTextOptions = True
        Me.GridColumn6.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center
        Me.GridColumn6.Caption = "Crédit"
        Me.GridColumn6.DisplayFormat.FormatString = "c"
        Me.GridColumn6.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.GridColumn6.FieldName = "Credit"
        Me.GridColumn6.Name = "GridColumn6"
        Me.GridColumn6.OptionsColumn.AllowEdit = False
        Me.GridColumn6.OptionsColumn.AllowFocus = False
        Me.GridColumn6.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Credit", "SUM={0:c2}")})
        Me.GridColumn6.Visible = True
        Me.GridColumn6.VisibleIndex = 18
        Me.GridColumn6.Width = 92
        '
        'GridColumn7
        '
        Me.GridColumn7.Caption = "# Glacier"
        Me.GridColumn7.FieldName = "NombreGlacier"
        Me.GridColumn7.Name = "GridColumn7"
        Me.GridColumn7.OptionsColumn.AllowEdit = False
        Me.GridColumn7.OptionsColumn.AllowFocus = False
        Me.GridColumn7.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "NombreGlacier", "SUM={0:0.##}")})
        Me.GridColumn7.Visible = True
        Me.GridColumn7.VisibleIndex = 11
        Me.GridColumn7.Width = 92
        '
        'colMontantGlacierLivreur
        '
        Me.colMontantGlacierLivreur.Caption = "Glacier L."
        Me.colMontantGlacierLivreur.DisplayFormat.FormatString = "c"
        Me.colMontantGlacierLivreur.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.colMontantGlacierLivreur.FieldName = "MontantGlacierLivreur"
        Me.colMontantGlacierLivreur.Name = "colMontantGlacierLivreur"
        Me.colMontantGlacierLivreur.OptionsColumn.AllowEdit = False
        Me.colMontantGlacierLivreur.OptionsColumn.AllowFocus = False
        Me.colMontantGlacierLivreur.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "MontantGlacierLivreur", "SUM={0:0.##}")})
        Me.colMontantGlacierLivreur.Visible = True
        Me.colMontantGlacierLivreur.VisibleIndex = 12
        Me.colMontantGlacierLivreur.Width = 92
        '
        'colMontantGlacierOrganisation
        '
        Me.colMontantGlacierOrganisation.Caption = "Glacier O."
        Me.colMontantGlacierOrganisation.DisplayFormat.FormatString = "c"
        Me.colMontantGlacierOrganisation.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.colMontantGlacierOrganisation.FieldName = "MontantGlacierOrganisation"
        Me.colMontantGlacierOrganisation.Name = "colMontantGlacierOrganisation"
        Me.colMontantGlacierOrganisation.OptionsColumn.AllowEdit = False
        Me.colMontantGlacierOrganisation.OptionsColumn.AllowFocus = False
        Me.colMontantGlacierOrganisation.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "MontantGlacierOrganisation", "SUM={0:0.##}")})
        Me.colMontantGlacierOrganisation.Visible = True
        Me.colMontantGlacierOrganisation.VisibleIndex = 13
        Me.colMontantGlacierOrganisation.Width = 92
        '
        'colMontantLivreur
        '
        Me.colMontantLivreur.Caption = "Montant L."
        Me.colMontantLivreur.DisplayFormat.FormatString = "c"
        Me.colMontantLivreur.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.colMontantLivreur.FieldName = "MontantLivreur"
        Me.colMontantLivreur.Name = "colMontantLivreur"
        Me.colMontantLivreur.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "MontantLivreur", "SUM={0:0.##}")})
        Me.colMontantLivreur.Visible = True
        Me.colMontantLivreur.VisibleIndex = 10
        Me.colMontantLivreur.Width = 104
        '
        'Extras
        '
        Me.Extras.Caption = "Extra"
        Me.Extras.DisplayFormat.FormatString = "c"
        Me.Extras.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.Extras.FieldName = "Extra"
        Me.Extras.Name = "Extras"
        Me.Extras.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Extra", "SUM={0:0.##}")})
        Me.Extras.Visible = True
        Me.Extras.VisibleIndex = 15
        '
        'colExtraKM
        '
        Me.colExtraKM.Caption = "Extra KM"
        Me.colExtraKM.FieldName = "ExtraKM"
        Me.colExtraKM.Name = "colExtraKM"
        Me.colExtraKM.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "ExtraKM", "SUM={0:0.##}")})
        Me.colExtraKM.Visible = True
        Me.colExtraKM.VisibleIndex = 16
        '
        'RepositoryItemCalcEdit1
        '
        Me.RepositoryItemCalcEdit1.AutoHeight = False
        Me.RepositoryItemCalcEdit1.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemCalcEdit1.Name = "RepositoryItemCalcEdit1"
        '
        'RepositoryItemTextEdit1
        '
        Me.RepositoryItemTextEdit1.AutoHeight = False
        Me.RepositoryItemTextEdit1.Name = "RepositoryItemTextEdit1"
        '
        'RepositoryItemTextEdit2
        '
        Me.RepositoryItemTextEdit2.AutoHeight = False
        Me.RepositoryItemTextEdit2.Name = "RepositoryItemTextEdit2"
        '
        'RepositoryItemCalcEdit2
        '
        Me.RepositoryItemCalcEdit2.AutoHeight = False
        Me.RepositoryItemCalcEdit2.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemCalcEdit2.Name = "RepositoryItemCalcEdit2"
        '
        'RepositoryItemSpinEdit1
        '
        Me.RepositoryItemSpinEdit1.AutoHeight = False
        Me.RepositoryItemSpinEdit1.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemSpinEdit1.Name = "RepositoryItemSpinEdit1"
        '
        'RepositoryItemCheckEdit1
        '
        Me.RepositoryItemCheckEdit1.AutoHeight = False
        Me.RepositoryItemCheckEdit1.Name = "RepositoryItemCheckEdit1"
        '
        'grdPayeLine
        '
        Me.grdPayeLine.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grdPayeLine.Location = New System.Drawing.Point(12, 12)
        Me.grdPayeLine.MainView = Me.GridView2
        Me.grdPayeLine.Name = "grdPayeLine"
        Me.grdPayeLine.Size = New System.Drawing.Size(1326, 141)
        Me.grdPayeLine.TabIndex = 4
        Me.grdPayeLine.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView2})
        '
        'GridView2
        '
        Me.GridView2.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colIDInvoiceLine, Me.colIDPaye, Me.colPayeNumber2, Me.colDateFrom2, Me.colDateTo2, Me.colIDLivreur2, Me.colLivreur2, Me.colOrganisation, Me.colCredit, Me.colNombreGlacier, Me.colGlacier, Me.colKm, Me.colDescription, Me.colUnite, Me.colPrix, Me.colMontant, Me.colNombreGlacier2, Me.colTotalGlacier})
        Me.GridView2.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.GridView2.GridControl = Me.grdPayeLine
        Me.GridView2.Name = "GridView2"
        Me.GridView2.OptionsBehavior.Editable = False
        Me.GridView2.OptionsCustomization.AllowGroup = False
        Me.GridView2.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridView2.OptionsSelection.UseIndicatorForSelection = False
        Me.GridView2.OptionsView.ShowAutoFilterRow = True
        Me.GridView2.OptionsView.ShowGroupPanel = False
        Me.GridView2.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.colPayeNumber2, DevExpress.Data.ColumnSortOrder.Ascending)})
        '
        'colIDInvoiceLine
        '
        Me.colIDInvoiceLine.Caption = "ID paie détail"
        Me.colIDInvoiceLine.FieldName = "IDPayeLine"
        Me.colIDInvoiceLine.Name = "colIDInvoiceLine"
        Me.colIDInvoiceLine.Visible = True
        Me.colIDInvoiceLine.VisibleIndex = 15
        Me.colIDInvoiceLine.Width = 94
        '
        'colIDPaye
        '
        Me.colIDPaye.Caption = "ID PAIE"
        Me.colIDPaye.FieldName = "IDPaye"
        Me.colIDPaye.Name = "colIDPaye"
        '
        'colPayeNumber2
        '
        Me.colPayeNumber2.Caption = "Numéro de paie"
        Me.colPayeNumber2.FieldName = "PayeNumber"
        Me.colPayeNumber2.Name = "colPayeNumber2"
        Me.colPayeNumber2.Visible = True
        Me.colPayeNumber2.VisibleIndex = 0
        Me.colPayeNumber2.Width = 105
        '
        'colDateFrom2
        '
        Me.colDateFrom2.Caption = "Période du"
        Me.colDateFrom2.FieldName = "DateFrom"
        Me.colDateFrom2.Name = "colDateFrom2"
        Me.colDateFrom2.Visible = True
        Me.colDateFrom2.VisibleIndex = 1
        Me.colDateFrom2.Width = 105
        '
        'colDateTo2
        '
        Me.colDateTo2.Caption = "Période au"
        Me.colDateTo2.FieldName = "DateTo"
        Me.colDateTo2.Name = "colDateTo2"
        Me.colDateTo2.Visible = True
        Me.colDateTo2.VisibleIndex = 2
        Me.colDateTo2.Width = 84
        '
        'colIDLivreur2
        '
        Me.colIDLivreur2.Caption = "ID Livreur"
        Me.colIDLivreur2.FieldName = "IDLivreur"
        Me.colIDLivreur2.Name = "colIDLivreur2"
        '
        'colLivreur2
        '
        Me.colLivreur2.Caption = "Livreur"
        Me.colLivreur2.FieldName = "Livreur"
        Me.colLivreur2.Name = "colLivreur2"
        Me.colLivreur2.Visible = True
        Me.colLivreur2.VisibleIndex = 3
        Me.colLivreur2.Width = 84
        '
        'colOrganisation
        '
        Me.colOrganisation.Caption = "Organisation"
        Me.colOrganisation.FieldName = "Organisation"
        Me.colOrganisation.Name = "colOrganisation"
        Me.colOrganisation.Visible = True
        Me.colOrganisation.VisibleIndex = 5
        Me.colOrganisation.Width = 78
        '
        'colCredit
        '
        Me.colCredit.Caption = "Crédits"
        Me.colCredit.DisplayFormat.FormatString = "c"
        Me.colCredit.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.colCredit.FieldName = "Credit"
        Me.colCredit.Name = "colCredit"
        Me.colCredit.Visible = True
        Me.colCredit.VisibleIndex = 9
        Me.colCredit.Width = 78
        '
        'colNombreGlacier
        '
        Me.colNombreGlacier.Caption = "Nombre Glacier"
        Me.colNombreGlacier.FieldName = "NombreGlacier"
        Me.colNombreGlacier.Name = "colNombreGlacier"
        Me.colNombreGlacier.Visible = True
        Me.colNombreGlacier.VisibleIndex = 10
        Me.colNombreGlacier.Width = 78
        '
        'colGlacier
        '
        Me.colGlacier.Caption = "Total glaciers"
        Me.colGlacier.DisplayFormat.FormatString = "c"
        Me.colGlacier.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.colGlacier.FieldName = "Glacier"
        Me.colGlacier.Name = "colGlacier"
        Me.colGlacier.Visible = True
        Me.colGlacier.VisibleIndex = 11
        Me.colGlacier.Width = 78
        '
        'colKm
        '
        Me.colKm.Caption = "Km"
        Me.colKm.FieldName = "Km"
        Me.colKm.Name = "colKm"
        Me.colKm.Visible = True
        Me.colKm.VisibleIndex = 12
        Me.colKm.Width = 78
        '
        'colDescription
        '
        Me.colDescription.Caption = "Description"
        Me.colDescription.FieldName = "Description"
        Me.colDescription.Name = "colDescription"
        Me.colDescription.Visible = True
        Me.colDescription.VisibleIndex = 4
        Me.colDescription.Width = 137
        '
        'colUnite
        '
        Me.colUnite.Caption = "Unité"
        Me.colUnite.FieldName = "Unite"
        Me.colUnite.Name = "colUnite"
        Me.colUnite.Visible = True
        Me.colUnite.VisibleIndex = 6
        Me.colUnite.Width = 78
        '
        'colPrix
        '
        Me.colPrix.Caption = "Prix"
        Me.colPrix.DisplayFormat.FormatString = "c"
        Me.colPrix.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.colPrix.FieldName = "Prix"
        Me.colPrix.Name = "colPrix"
        Me.colPrix.Visible = True
        Me.colPrix.VisibleIndex = 7
        Me.colPrix.Width = 78
        '
        'colMontant
        '
        Me.colMontant.Caption = "Montant"
        Me.colMontant.DisplayFormat.FormatString = "c"
        Me.colMontant.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.colMontant.FieldName = "Montant"
        Me.colMontant.Name = "colMontant"
        Me.colMontant.Visible = True
        Me.colMontant.VisibleIndex = 8
        Me.colMontant.Width = 78
        '
        'colNombreGlacier2
        '
        Me.colNombreGlacier2.Caption = "Nombre Glacier"
        Me.colNombreGlacier2.FieldName = "NombreGlacier"
        Me.colNombreGlacier2.Name = "colNombreGlacier2"
        Me.colNombreGlacier2.Visible = True
        Me.colNombreGlacier2.VisibleIndex = 13
        Me.colNombreGlacier2.Width = 78
        '
        'colTotalGlacier
        '
        Me.colTotalGlacier.Caption = "Total Glacier"
        Me.colTotalGlacier.DisplayFormat.FormatString = "c"
        Me.colTotalGlacier.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.colTotalGlacier.FieldName = "Glacier"
        Me.colTotalGlacier.Name = "colTotalGlacier"
        Me.colTotalGlacier.Visible = True
        Me.colTotalGlacier.VisibleIndex = 14
        Me.colTotalGlacier.Width = 90
        '
        'LayoutControlGroup2
        '
        Me.LayoutControlGroup2.CustomizationFormText = "LayoutControlGroup1"
        Me.LayoutControlGroup2.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup2.GroupBordersVisible = False
        Me.LayoutControlGroup2.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem1, Me.LayoutControlItem4, Me.LayoutControlItem5, Me.LayoutControlItem9, Me.LayoutControlItem7, Me.LayoutControlItem8, Me.LayoutControlItem6})
        Me.LayoutControlGroup2.Name = "Root"
        Me.LayoutControlGroup2.Size = New System.Drawing.Size(1350, 501)
        Me.LayoutControlGroup2.TextVisible = False
        '
        'LayoutControlItem1
        '
        Me.LayoutControlItem1.Control = Me.grdPayeLine
        Me.LayoutControlItem1.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem1.Name = "LayoutControlItem1"
        Me.LayoutControlItem1.Size = New System.Drawing.Size(1330, 145)
        Me.LayoutControlItem1.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem1.TextVisible = False
        '
        'LayoutControlItem4
        '
        Me.LayoutControlItem4.Control = Me.LabelControl1
        Me.LayoutControlItem4.Location = New System.Drawing.Point(0, 145)
        Me.LayoutControlItem4.Name = "LayoutControlItem4"
        Me.LayoutControlItem4.Size = New System.Drawing.Size(1330, 28)
        Me.LayoutControlItem4.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem4.TextVisible = False
        '
        'LayoutControlItem5
        '
        Me.LayoutControlItem5.Control = Me.grdImport
        Me.LayoutControlItem5.Location = New System.Drawing.Point(0, 199)
        Me.LayoutControlItem5.Name = "LayoutControlItem5"
        Me.LayoutControlItem5.Size = New System.Drawing.Size(1330, 282)
        Me.LayoutControlItem5.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem5.TextVisible = False
        '
        'LayoutControlItem9
        '
        Me.LayoutControlItem9.Control = Me.bExportToExcel
        Me.LayoutControlItem9.Location = New System.Drawing.Point(0, 173)
        Me.LayoutControlItem9.Name = "LayoutControlItem9"
        Me.LayoutControlItem9.Size = New System.Drawing.Size(356, 26)
        Me.LayoutControlItem9.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem9.TextVisible = False
        '
        'LayoutControlItem7
        '
        Me.LayoutControlItem7.Control = Me.bExportToPDF
        Me.LayoutControlItem7.Location = New System.Drawing.Point(356, 173)
        Me.LayoutControlItem7.Name = "LayoutControlItem7"
        Me.LayoutControlItem7.Size = New System.Drawing.Size(365, 26)
        Me.LayoutControlItem7.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem7.TextVisible = False
        '
        'LayoutControlItem8
        '
        Me.LayoutControlItem8.Control = Me.bExportToDoc
        Me.LayoutControlItem8.Location = New System.Drawing.Point(721, 173)
        Me.LayoutControlItem8.Name = "LayoutControlItem8"
        Me.LayoutControlItem8.Size = New System.Drawing.Size(321, 26)
        Me.LayoutControlItem8.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem8.TextVisible = False
        '
        'LayoutControlItem6
        '
        Me.LayoutControlItem6.Control = Me.bExportToHTML
        Me.LayoutControlItem6.Location = New System.Drawing.Point(1042, 173)
        Me.LayoutControlItem6.Name = "LayoutControlItem6"
        Me.LayoutControlItem6.Size = New System.Drawing.Size(288, 26)
        Me.LayoutControlItem6.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem6.TextVisible = False
        '
        'Root
        '
        Me.Root.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.Root.GroupBordersVisible = False
        Me.Root.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem3})
        Me.Root.Name = "Root"
        Me.Root.Size = New System.Drawing.Size(1379, 551)
        Me.Root.TextVisible = False
        '
        'LayoutControlItem3
        '
        Me.LayoutControlItem3.Control = Me.tabData
        Me.LayoutControlItem3.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem3.Name = "LayoutControlItem3"
        Me.LayoutControlItem3.Size = New System.Drawing.Size(1359, 531)
        Me.LayoutControlItem3.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem3.TextVisible = False
        '
        'DockManager1
        '
        Me.DockManager1.Form = Me
        Me.DockManager1.RootPanels.AddRange(New DevExpress.XtraBars.Docking.DockPanel() {Me.DockNouvellePaye, Me.DockPaye})
        Me.DockManager1.TopZIndexControls.AddRange(New String() {"DevExpress.XtraBars.BarDockControl", "DevExpress.XtraBars.StandaloneBarDockControl", "System.Windows.Forms.MenuStrip", "System.Windows.Forms.StatusStrip", "System.Windows.Forms.StatusBar", "DevExpress.XtraBars.Ribbon.RibbonStatusBar", "DevExpress.XtraBars.Ribbon.RibbonControl", "DevExpress.XtraBars.Navigation.OfficeNavigationBar", "DevExpress.XtraBars.Navigation.TileNavPane", "DevExpress.XtraBars.TabFormControl", "DevExpress.XtraBars.FluentDesignSystem.FluentDesignFormControl", "DevExpress.XtraBars.ToolbarForm.ToolbarFormControl"})
        '
        'DockNouvellePaye
        '
        Me.DockNouvellePaye.Controls.Add(Me.ControlContainer1)
        Me.DockNouvellePaye.Dock = DevExpress.XtraBars.Docking.DockingStyle.Float
        Me.DockNouvellePaye.FloatLocation = New System.Drawing.Point(121, 221)
        Me.DockNouvellePaye.FloatSize = New System.Drawing.Size(401, 268)
        Me.DockNouvellePaye.ID = New System.Guid("d61c8dc6-43b7-42fa-821d-97c0010c407d")
        Me.DockNouvellePaye.Location = New System.Drawing.Point(0, 0)
        Me.DockNouvellePaye.Name = "DockNouvellePaye"
        Me.DockNouvellePaye.OriginalSize = New System.Drawing.Size(356, 200)
        Me.DockNouvellePaye.Size = New System.Drawing.Size(401, 268)
        Me.DockNouvellePaye.Text = "Nouvelle paye"
        '
        'ControlContainer1
        '
        Me.ControlContainer1.Controls.Add(Me.LayoutControl5)
        Me.ControlContainer1.Location = New System.Drawing.Point(4, 30)
        Me.ControlContainer1.Name = "ControlContainer1"
        Me.ControlContainer1.Size = New System.Drawing.Size(393, 234)
        Me.ControlContainer1.TabIndex = 0
        '
        'LayoutControl5
        '
        Me.LayoutControl5.Controls.Add(Me.bAnnulerNouvellePaye)
        Me.LayoutControl5.Controls.Add(Me.bSauvegarderNouvellePaye)
        Me.LayoutControl5.Controls.Add(Me.tbPayeNumberNP)
        Me.LayoutControl5.Controls.Add(Me.cbLivreurNP)
        Me.LayoutControl5.Controls.Add(Me.tbDateFromNP)
        Me.LayoutControl5.Controls.Add(Me.tbDateAuNP)
        Me.LayoutControl5.Controls.Add(Me.cbPeriodesNP)
        Me.LayoutControl5.Controls.Add(Me.tbIDPayeNP)
        Me.LayoutControl5.Controls.Add(Me.tbPayeDateNP)
        Me.LayoutControl5.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LayoutControl5.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControl5.Name = "LayoutControl5"
        Me.LayoutControl5.Root = Me.LayoutControlGroup4
        Me.LayoutControl5.Size = New System.Drawing.Size(393, 234)
        Me.LayoutControl5.TabIndex = 0
        Me.LayoutControl5.Text = "LayoutControl5"
        '
        'bAnnulerNouvellePaye
        '
        Me.bAnnulerNouvellePaye.Location = New System.Drawing.Point(171, 182)
        Me.bAnnulerNouvellePaye.Name = "bAnnulerNouvellePaye"
        Me.bAnnulerNouvellePaye.Size = New System.Drawing.Size(210, 22)
        Me.bAnnulerNouvellePaye.StyleController = Me.LayoutControl5
        Me.bAnnulerNouvellePaye.TabIndex = 137
        Me.bAnnulerNouvellePaye.Text = "Annuler"
        '
        'bSauvegarderNouvellePaye
        '
        Me.bSauvegarderNouvellePaye.Location = New System.Drawing.Point(12, 182)
        Me.bSauvegarderNouvellePaye.Name = "bSauvegarderNouvellePaye"
        Me.bSauvegarderNouvellePaye.Size = New System.Drawing.Size(155, 22)
        Me.bSauvegarderNouvellePaye.StyleController = Me.LayoutControl5
        Me.bSauvegarderNouvellePaye.TabIndex = 136
        Me.bSauvegarderNouvellePaye.Text = "Sauvegarder"
        '
        'tbPayeNumberNP
        '
        Me.tbPayeNumberNP.Location = New System.Drawing.Point(94, 134)
        Me.tbPayeNumberNP.Name = "tbPayeNumberNP"
        Me.tbPayeNumberNP.Size = New System.Drawing.Size(287, 20)
        Me.tbPayeNumberNP.StyleController = Me.LayoutControl5
        Me.tbPayeNumberNP.TabIndex = 4
        '
        'cbLivreurNP
        '
        Me.cbLivreurNP.Location = New System.Drawing.Point(94, 158)
        Me.cbLivreurNP.Name = "cbLivreurNP"
        Me.cbLivreurNP.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbLivreurNP.Size = New System.Drawing.Size(287, 20)
        Me.cbLivreurNP.StyleController = Me.LayoutControl5
        Me.cbLivreurNP.TabIndex = 30
        '
        'tbDateFromNP
        '
        Me.tbDateFromNP.EditValue = Nothing
        Me.tbDateFromNP.Location = New System.Drawing.Point(94, 86)
        Me.tbDateFromNP.Name = "tbDateFromNP"
        Me.tbDateFromNP.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.tbDateFromNP.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.tbDateFromNP.Properties.DisplayFormat.FormatString = "MM-dd-yy H:mm"
        Me.tbDateFromNP.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.tbDateFromNP.Properties.EditFormat.FormatString = "MM-dd-yy H:mm"
        Me.tbDateFromNP.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.tbDateFromNP.Properties.Mask.EditMask = "MM-dd-yy H:mm"
        Me.tbDateFromNP.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.tbDateFromNP.Size = New System.Drawing.Size(287, 20)
        Me.tbDateFromNP.StyleController = Me.LayoutControl5
        Me.tbDateFromNP.TabIndex = 22
        '
        'tbDateAuNP
        '
        Me.tbDateAuNP.EditValue = Nothing
        Me.tbDateAuNP.Location = New System.Drawing.Point(94, 110)
        Me.tbDateAuNP.Name = "tbDateAuNP"
        Me.tbDateAuNP.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.tbDateAuNP.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.tbDateAuNP.Properties.DisplayFormat.FormatString = "MM-dd-yy H:mm"
        Me.tbDateAuNP.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.tbDateAuNP.Properties.EditFormat.FormatString = "MM-dd-yy H:mm"
        Me.tbDateAuNP.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.tbDateAuNP.Properties.Mask.EditMask = "MM-dd-yy H:mm"
        Me.tbDateAuNP.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.tbDateAuNP.Size = New System.Drawing.Size(287, 20)
        Me.tbDateAuNP.StyleController = Me.LayoutControl5
        Me.tbDateAuNP.TabIndex = 22
        '
        'cbPeriodesNP
        '
        Me.cbPeriodesNP.Location = New System.Drawing.Point(94, 60)
        Me.cbPeriodesNP.Name = "cbPeriodesNP"
        Me.cbPeriodesNP.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbPeriodesNP.Properties.Appearance.Options.UseFont = True
        Me.cbPeriodesNP.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbPeriodesNP.Size = New System.Drawing.Size(287, 22)
        Me.cbPeriodesNP.StyleController = Me.LayoutControl5
        Me.cbPeriodesNP.TabIndex = 28
        '
        'tbIDPayeNP
        '
        Me.tbIDPayeNP.EditValue = "0"
        Me.tbIDPayeNP.Enabled = False
        Me.tbIDPayeNP.Location = New System.Drawing.Point(94, 12)
        Me.tbIDPayeNP.Name = "tbIDPayeNP"
        Me.tbIDPayeNP.Properties.Appearance.BackColor = System.Drawing.Color.Linen
        Me.tbIDPayeNP.Properties.Appearance.Options.UseBackColor = True
        Me.tbIDPayeNP.Properties.Mask.EditMask = "n0"
        Me.tbIDPayeNP.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.tbIDPayeNP.Size = New System.Drawing.Size(287, 20)
        Me.tbIDPayeNP.StyleController = Me.LayoutControl5
        Me.tbIDPayeNP.TabIndex = 132
        '
        'tbPayeDateNP
        '
        Me.tbPayeDateNP.EditValue = Nothing
        Me.tbPayeDateNP.Location = New System.Drawing.Point(94, 36)
        Me.tbPayeDateNP.Name = "tbPayeDateNP"
        Me.tbPayeDateNP.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.tbPayeDateNP.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.tbPayeDateNP.Properties.DisplayFormat.FormatString = "MM-dd-yy H:mm"
        Me.tbPayeDateNP.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.tbPayeDateNP.Properties.EditFormat.FormatString = "MM-dd-yy H:mm"
        Me.tbPayeDateNP.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.tbPayeDateNP.Properties.Mask.EditMask = "MM-dd-yy H:mm"
        Me.tbPayeDateNP.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.tbPayeDateNP.Size = New System.Drawing.Size(287, 20)
        Me.tbPayeDateNP.StyleController = Me.LayoutControl5
        Me.tbPayeDateNP.TabIndex = 22
        '
        'LayoutControlGroup4
        '
        Me.LayoutControlGroup4.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup4.GroupBordersVisible = False
        Me.LayoutControlGroup4.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem23, Me.LayoutControlItem24, Me.LayoutControlItem25, Me.LayoutControlItem26, Me.LayoutControlItem28, Me.LayoutControlItem29, Me.LayoutControlItem30, Me.LayoutControlItem31, Me.LayoutControlItem11})
        Me.LayoutControlGroup4.Name = "Root"
        Me.LayoutControlGroup4.Size = New System.Drawing.Size(393, 234)
        Me.LayoutControlGroup4.TextVisible = False
        '
        'LayoutControlItem23
        '
        Me.LayoutControlItem23.Control = Me.tbPayeNumberNP
        Me.LayoutControlItem23.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem23.CustomizationFormText = "Numéro de paye"
        Me.LayoutControlItem23.Location = New System.Drawing.Point(0, 122)
        Me.LayoutControlItem23.Name = "LayoutControlItem23"
        Me.LayoutControlItem23.Size = New System.Drawing.Size(373, 24)
        Me.LayoutControlItem23.Text = "Numéro de paye"
        Me.LayoutControlItem23.TextSize = New System.Drawing.Size(79, 13)
        '
        'LayoutControlItem24
        '
        Me.LayoutControlItem24.Control = Me.cbLivreurNP
        Me.LayoutControlItem24.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem24.CustomizationFormText = "Liste des payes"
        Me.LayoutControlItem24.Location = New System.Drawing.Point(0, 146)
        Me.LayoutControlItem24.Name = "LayoutControlItem24"
        Me.LayoutControlItem24.Size = New System.Drawing.Size(373, 24)
        Me.LayoutControlItem24.Text = "Livreur"
        Me.LayoutControlItem24.TextSize = New System.Drawing.Size(79, 13)
        '
        'LayoutControlItem25
        '
        Me.LayoutControlItem25.Control = Me.tbDateFromNP
        Me.LayoutControlItem25.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem25.CustomizationFormText = "Date du"
        Me.LayoutControlItem25.Location = New System.Drawing.Point(0, 74)
        Me.LayoutControlItem25.Name = "LayoutControlItem25"
        Me.LayoutControlItem25.Size = New System.Drawing.Size(373, 24)
        Me.LayoutControlItem25.Text = "Date du"
        Me.LayoutControlItem25.TextSize = New System.Drawing.Size(79, 13)
        '
        'LayoutControlItem26
        '
        Me.LayoutControlItem26.Control = Me.tbDateAuNP
        Me.LayoutControlItem26.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem26.CustomizationFormText = "Date du"
        Me.LayoutControlItem26.Location = New System.Drawing.Point(0, 98)
        Me.LayoutControlItem26.Name = "LayoutControlItem26"
        Me.LayoutControlItem26.Size = New System.Drawing.Size(373, 24)
        Me.LayoutControlItem26.Text = "Date au"
        Me.LayoutControlItem26.TextSize = New System.Drawing.Size(79, 13)
        '
        'LayoutControlItem28
        '
        Me.LayoutControlItem28.Control = Me.cbPeriodesNP
        Me.LayoutControlItem28.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem28.CustomizationFormText = "Périodes"
        Me.LayoutControlItem28.Location = New System.Drawing.Point(0, 48)
        Me.LayoutControlItem28.Name = "LayoutControlItem28"
        Me.LayoutControlItem28.Size = New System.Drawing.Size(373, 26)
        Me.LayoutControlItem28.Text = "Période"
        Me.LayoutControlItem28.TextSize = New System.Drawing.Size(79, 13)
        '
        'LayoutControlItem29
        '
        Me.LayoutControlItem29.Control = Me.tbIDPayeNP
        Me.LayoutControlItem29.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem29.CustomizationFormText = "Nombre maximum free echange (305 et 35)"
        Me.LayoutControlItem29.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem29.Name = "LayoutControlItem29"
        Me.LayoutControlItem29.Size = New System.Drawing.Size(373, 24)
        Me.LayoutControlItem29.Text = "ID Paie"
        Me.LayoutControlItem29.TextSize = New System.Drawing.Size(79, 13)
        '
        'LayoutControlItem30
        '
        Me.LayoutControlItem30.Control = Me.bSauvegarderNouvellePaye
        Me.LayoutControlItem30.Location = New System.Drawing.Point(0, 170)
        Me.LayoutControlItem30.Name = "LayoutControlItem30"
        Me.LayoutControlItem30.Size = New System.Drawing.Size(159, 44)
        Me.LayoutControlItem30.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem30.TextVisible = False
        '
        'LayoutControlItem31
        '
        Me.LayoutControlItem31.Control = Me.bAnnulerNouvellePaye
        Me.LayoutControlItem31.Location = New System.Drawing.Point(159, 170)
        Me.LayoutControlItem31.Name = "LayoutControlItem31"
        Me.LayoutControlItem31.Size = New System.Drawing.Size(214, 44)
        Me.LayoutControlItem31.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem31.TextVisible = False
        '
        'LayoutControlItem11
        '
        Me.LayoutControlItem11.Control = Me.tbPayeDateNP
        Me.LayoutControlItem11.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem11.CustomizationFormText = "Date du"
        Me.LayoutControlItem11.Location = New System.Drawing.Point(0, 24)
        Me.LayoutControlItem11.Name = "LayoutControlItem11"
        Me.LayoutControlItem11.Size = New System.Drawing.Size(373, 24)
        Me.LayoutControlItem11.Text = "Date paie"
        Me.LayoutControlItem11.TextSize = New System.Drawing.Size(79, 13)
        '
        'DockPaye
        '
        Me.DockPaye.Controls.Add(Me.DockPanel1_Container)
        Me.DockPaye.Dock = DevExpress.XtraBars.Docking.DockingStyle.Float
        Me.DockPaye.FloatLocation = New System.Drawing.Point(670, 285)
        Me.DockPaye.FloatSize = New System.Drawing.Size(639, 442)
        Me.DockPaye.ID = New System.Guid("edfca4ae-53f8-4060-ac10-801ce49e5fb7")
        Me.DockPaye.Location = New System.Drawing.Point(0, 0)
        Me.DockPaye.Name = "DockPaye"
        Me.DockPaye.Options.ShowAutoHideButton = False
        Me.DockPaye.Options.ShowCloseButton = False
        Me.DockPaye.Options.ShowMaximizeButton = False
        Me.DockPaye.OriginalSize = New System.Drawing.Size(522, 200)
        Me.DockPaye.SavedDock = DevExpress.XtraBars.Docking.DockingStyle.Left
        Me.DockPaye.SavedIndex = 1
        Me.DockPaye.Size = New System.Drawing.Size(639, 442)
        Me.DockPaye.Text = "Ajouter une ligne a la paie"
        '
        'DockPanel1_Container
        '
        Me.DockPanel1_Container.Controls.Add(Me.LayoutControl4)
        Me.DockPanel1_Container.Location = New System.Drawing.Point(4, 30)
        Me.DockPanel1_Container.Name = "DockPanel1_Container"
        Me.DockPanel1_Container.Size = New System.Drawing.Size(631, 408)
        Me.DockPanel1_Container.TabIndex = 0
        '
        'LayoutControl4
        '
        Me.LayoutControl4.Controls.Add(Me.chkAjout)
        Me.LayoutControl4.Controls.Add(Me.radChoice)
        Me.LayoutControl4.Controls.Add(Me.btnAnnuler)
        Me.LayoutControl4.Controls.Add(Me.bSauvegarde)
        Me.LayoutControl4.Controls.Add(Me.tbPayeNumber)
        Me.LayoutControl4.Controls.Add(Me.tbDescription)
        Me.LayoutControl4.Controls.Add(Me.tbPrix)
        Me.LayoutControl4.Controls.Add(Me.tbTotal)
        Me.LayoutControl4.Controls.Add(Me.tbQuantite)
        Me.LayoutControl4.Controls.Add(Me.cbOrganisation)
        Me.LayoutControl4.Controls.Add(Me.cbLivreur)
        Me.LayoutControl4.Controls.Add(Me.tbDateFrom)
        Me.LayoutControl4.Controls.Add(Me.tbDateAu)
        Me.LayoutControl4.Controls.Add(Me.cbPeriodes)
        Me.LayoutControl4.Controls.Add(Me.tbIDPaye)
        Me.LayoutControl4.Controls.Add(Me.tbPayeDate)
        Me.LayoutControl4.Controls.Add(Me.tbIDPayeLine)
        Me.LayoutControl4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LayoutControl4.HiddenItems.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem19})
        Me.LayoutControl4.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControl4.Name = "LayoutControl4"
        Me.LayoutControl4.Root = Me.LayoutControlGroup3
        Me.LayoutControl4.Size = New System.Drawing.Size(631, 408)
        Me.LayoutControl4.TabIndex = 0
        Me.LayoutControl4.Text = "LayoutControl4"
        '
        'chkAjout
        '
        Me.chkAjout.Location = New System.Drawing.Point(12, 326)
        Me.chkAjout.Name = "chkAjout"
        Me.chkAjout.Properties.Caption = "Ajout?"
        Me.chkAjout.Size = New System.Drawing.Size(303, 19)
        Me.chkAjout.StyleController = Me.LayoutControl4
        Me.chkAjout.TabIndex = 141
        '
        'radChoice
        '
        Me.radChoice.Location = New System.Drawing.Point(319, 28)
        Me.radChoice.Name = "radChoice"
        Me.radChoice.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "Ajouter ligne de paie"), New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "Ajouter Glaciers"), New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "Ajouter crédit"), New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "Ajouter débit"), New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "Ajouter un extra")})
        Me.radChoice.Size = New System.Drawing.Size(300, 317)
        Me.radChoice.StyleController = Me.LayoutControl4
        Me.radChoice.TabIndex = 139
        '
        'btnAnnuler
        '
        Me.btnAnnuler.Location = New System.Drawing.Point(308, 374)
        Me.btnAnnuler.Name = "btnAnnuler"
        Me.btnAnnuler.Size = New System.Drawing.Size(311, 22)
        Me.btnAnnuler.StyleController = Me.LayoutControl4
        Me.btnAnnuler.TabIndex = 135
        Me.btnAnnuler.Text = "Fermer"
        '
        'bSauvegarde
        '
        Me.bSauvegarde.Location = New System.Drawing.Point(12, 374)
        Me.bSauvegarde.Name = "bSauvegarde"
        Me.bSauvegarde.Size = New System.Drawing.Size(292, 22)
        Me.bSauvegarde.StyleController = Me.LayoutControl4
        Me.bSauvegarde.TabIndex = 134
        Me.bSauvegarde.Text = "Sauvegarder"
        '
        'tbPayeNumber
        '
        Me.tbPayeNumber.Location = New System.Drawing.Point(94, 60)
        Me.tbPayeNumber.Name = "tbPayeNumber"
        Me.tbPayeNumber.Size = New System.Drawing.Size(221, 20)
        Me.tbPayeNumber.StyleController = Me.LayoutControl4
        Me.tbPayeNumber.TabIndex = 4
        '
        'tbDescription
        '
        Me.tbDescription.Location = New System.Drawing.Point(94, 230)
        Me.tbDescription.Name = "tbDescription"
        Me.tbDescription.Size = New System.Drawing.Size(221, 20)
        Me.tbDescription.StyleController = Me.LayoutControl4
        Me.tbDescription.TabIndex = 4
        '
        'tbPrix
        '
        Me.tbPrix.EditValue = "0"
        Me.tbPrix.Location = New System.Drawing.Point(94, 278)
        Me.tbPrix.Name = "tbPrix"
        Me.tbPrix.Properties.DisplayFormat.FormatString = "c"
        Me.tbPrix.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.tbPrix.Properties.EditFormat.FormatString = "c"
        Me.tbPrix.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.tbPrix.Properties.Mask.EditMask = "c"
        Me.tbPrix.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.tbPrix.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.tbPrix.Size = New System.Drawing.Size(221, 20)
        Me.tbPrix.StyleController = Me.LayoutControl4
        Me.tbPrix.TabIndex = 128
        '
        'tbTotal
        '
        Me.tbTotal.EditValue = "0"
        Me.tbTotal.Location = New System.Drawing.Point(94, 302)
        Me.tbTotal.Name = "tbTotal"
        Me.tbTotal.Properties.DisplayFormat.FormatString = "c"
        Me.tbTotal.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.tbTotal.Properties.EditFormat.FormatString = "c"
        Me.tbTotal.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.tbTotal.Properties.Mask.EditMask = "c"
        Me.tbTotal.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.tbTotal.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.tbTotal.Size = New System.Drawing.Size(221, 20)
        Me.tbTotal.StyleController = Me.LayoutControl4
        Me.tbTotal.TabIndex = 128
        '
        'tbQuantite
        '
        Me.tbQuantite.EditValue = "1"
        Me.tbQuantite.Location = New System.Drawing.Point(94, 254)
        Me.tbQuantite.Name = "tbQuantite"
        Me.tbQuantite.Properties.Mask.EditMask = "n0"
        Me.tbQuantite.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.tbQuantite.Size = New System.Drawing.Size(221, 20)
        Me.tbQuantite.StyleController = Me.LayoutControl4
        Me.tbQuantite.TabIndex = 132
        '
        'cbOrganisation
        '
        Me.cbOrganisation.Location = New System.Drawing.Point(94, 206)
        Me.cbOrganisation.Name = "cbOrganisation"
        Me.cbOrganisation.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbOrganisation.Size = New System.Drawing.Size(221, 20)
        Me.cbOrganisation.StyleController = Me.LayoutControl4
        Me.cbOrganisation.TabIndex = 36
        '
        'cbLivreur
        '
        Me.cbLivreur.Location = New System.Drawing.Point(94, 182)
        Me.cbLivreur.Name = "cbLivreur"
        Me.cbLivreur.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbLivreur.Size = New System.Drawing.Size(221, 20)
        Me.cbLivreur.StyleController = Me.LayoutControl4
        Me.cbLivreur.TabIndex = 30
        '
        'tbDateFrom
        '
        Me.tbDateFrom.EditValue = Nothing
        Me.tbDateFrom.Location = New System.Drawing.Point(94, 134)
        Me.tbDateFrom.Name = "tbDateFrom"
        Me.tbDateFrom.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.tbDateFrom.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.tbDateFrom.Properties.DisplayFormat.FormatString = "MM-dd-yy H:mm"
        Me.tbDateFrom.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.tbDateFrom.Properties.EditFormat.FormatString = "MM-dd-yy H:mm"
        Me.tbDateFrom.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.tbDateFrom.Properties.Mask.EditMask = "MM-dd-yy H:mm"
        Me.tbDateFrom.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.tbDateFrom.Size = New System.Drawing.Size(221, 20)
        Me.tbDateFrom.StyleController = Me.LayoutControl4
        Me.tbDateFrom.TabIndex = 22
        '
        'tbDateAu
        '
        Me.tbDateAu.EditValue = Nothing
        Me.tbDateAu.Location = New System.Drawing.Point(94, 158)
        Me.tbDateAu.Name = "tbDateAu"
        Me.tbDateAu.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.tbDateAu.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.tbDateAu.Properties.DisplayFormat.FormatString = "MM-dd-yy H:mm"
        Me.tbDateAu.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.tbDateAu.Properties.EditFormat.FormatString = "MM-dd-yy H:mm"
        Me.tbDateAu.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.tbDateAu.Properties.Mask.EditMask = "MM-dd-yy H:mm"
        Me.tbDateAu.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.tbDateAu.Size = New System.Drawing.Size(221, 20)
        Me.tbDateAu.StyleController = Me.LayoutControl4
        Me.tbDateAu.TabIndex = 22
        '
        'cbPeriodes
        '
        Me.cbPeriodes.Location = New System.Drawing.Point(94, 108)
        Me.cbPeriodes.Name = "cbPeriodes"
        Me.cbPeriodes.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbPeriodes.Properties.Appearance.Options.UseFont = True
        Me.cbPeriodes.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbPeriodes.Size = New System.Drawing.Size(221, 22)
        Me.cbPeriodes.StyleController = Me.LayoutControl4
        Me.cbPeriodes.TabIndex = 28
        '
        'tbIDPaye
        '
        Me.tbIDPaye.EditValue = "0"
        Me.tbIDPaye.Enabled = False
        Me.tbIDPaye.Location = New System.Drawing.Point(94, 36)
        Me.tbIDPaye.Name = "tbIDPaye"
        Me.tbIDPaye.Properties.Appearance.BackColor = System.Drawing.Color.Linen
        Me.tbIDPaye.Properties.Appearance.Options.UseBackColor = True
        Me.tbIDPaye.Properties.Mask.EditMask = "n0"
        Me.tbIDPaye.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.tbIDPaye.Size = New System.Drawing.Size(221, 20)
        Me.tbIDPaye.StyleController = Me.LayoutControl4
        Me.tbIDPaye.TabIndex = 132
        '
        'tbPayeDate
        '
        Me.tbPayeDate.EditValue = Nothing
        Me.tbPayeDate.Location = New System.Drawing.Point(94, 84)
        Me.tbPayeDate.Name = "tbPayeDate"
        Me.tbPayeDate.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.tbPayeDate.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.tbPayeDate.Properties.DisplayFormat.FormatString = "MM-dd-yy H:mm"
        Me.tbPayeDate.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.tbPayeDate.Properties.EditFormat.FormatString = "MM-dd-yy H:mm"
        Me.tbPayeDate.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.tbPayeDate.Properties.Mask.EditMask = "MM-dd-yy H:mm"
        Me.tbPayeDate.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.tbPayeDate.Size = New System.Drawing.Size(221, 20)
        Me.tbPayeDate.StyleController = Me.LayoutControl4
        Me.tbPayeDate.TabIndex = 22
        '
        'tbIDPayeLine
        '
        Me.tbIDPayeLine.EditValue = "0"
        Me.tbIDPayeLine.Enabled = False
        Me.tbIDPayeLine.Location = New System.Drawing.Point(94, 12)
        Me.tbIDPayeLine.Name = "tbIDPayeLine"
        Me.tbIDPayeLine.Properties.Appearance.BackColor = System.Drawing.Color.Linen
        Me.tbIDPayeLine.Properties.Appearance.Options.UseBackColor = True
        Me.tbIDPayeLine.Properties.Mask.EditMask = "n0"
        Me.tbIDPayeLine.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.tbIDPayeLine.Size = New System.Drawing.Size(221, 20)
        Me.tbIDPayeLine.StyleController = Me.LayoutControl4
        Me.tbIDPayeLine.TabIndex = 132
        '
        'LayoutControlItem19
        '
        Me.LayoutControlItem19.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem19.Name = "LayoutControlItem19"
        Me.LayoutControlItem19.Size = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem19.TextSize = New System.Drawing.Size(50, 20)
        '
        'LayoutControlGroup3
        '
        Me.LayoutControlGroup3.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup3.GroupBordersVisible = False
        Me.LayoutControlGroup3.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.EmptySpaceItem1, Me.LayoutSave, Me.LayoutControlItem16, Me.LayoutNombreFree2, Me.LayoutNombreFree1, Me.LayoutControlItem10, Me.LayoutControlItem18, Me.LayoutControlItem15, Me.LayoutControlItem13, Me.LayoutControlItem14, Me.LayoutControlItem20, Me.LayoutControlItem27, Me.LayoutControlItem12, Me.lblQuantite, Me.LayoutPayeAuLivreur, Me.LayoutPayeAuLivreur1, Me.LayoutControlItem21, Me.LayoutControlItem32})
        Me.LayoutControlGroup3.Name = "Root"
        Me.LayoutControlGroup3.Size = New System.Drawing.Size(631, 408)
        Me.LayoutControlGroup3.TextVisible = False
        '
        'EmptySpaceItem1
        '
        Me.EmptySpaceItem1.AllowHotTrack = False
        Me.EmptySpaceItem1.Location = New System.Drawing.Point(0, 337)
        Me.EmptySpaceItem1.Name = "EmptySpaceItem1"
        Me.EmptySpaceItem1.Size = New System.Drawing.Size(611, 25)
        Me.EmptySpaceItem1.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutSave
        '
        Me.LayoutSave.Control = Me.bSauvegarde
        Me.LayoutSave.Location = New System.Drawing.Point(0, 362)
        Me.LayoutSave.Name = "LayoutSave"
        Me.LayoutSave.Size = New System.Drawing.Size(296, 26)
        Me.LayoutSave.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutSave.TextVisible = False
        '
        'LayoutControlItem16
        '
        Me.LayoutControlItem16.Control = Me.btnAnnuler
        Me.LayoutControlItem16.Location = New System.Drawing.Point(296, 362)
        Me.LayoutControlItem16.Name = "LayoutControlItem16"
        Me.LayoutControlItem16.Size = New System.Drawing.Size(315, 26)
        Me.LayoutControlItem16.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem16.TextVisible = False
        '
        'LayoutNombreFree2
        '
        Me.LayoutNombreFree2.Control = Me.tbIDPayeLine
        Me.LayoutNombreFree2.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutNombreFree2.CustomizationFormText = "Nombre maximum free echange (305 et 35)"
        Me.LayoutNombreFree2.Location = New System.Drawing.Point(0, 0)
        Me.LayoutNombreFree2.Name = "LayoutNombreFree2"
        Me.LayoutNombreFree2.Size = New System.Drawing.Size(307, 24)
        Me.LayoutNombreFree2.Text = "ID Paye Ligne"
        Me.LayoutNombreFree2.TextSize = New System.Drawing.Size(79, 13)
        '
        'LayoutNombreFree1
        '
        Me.LayoutNombreFree1.Control = Me.tbIDPaye
        Me.LayoutNombreFree1.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutNombreFree1.CustomizationFormText = "Nombre maximum free echange (305 et 35)"
        Me.LayoutNombreFree1.Location = New System.Drawing.Point(0, 24)
        Me.LayoutNombreFree1.Name = "LayoutNombreFree1"
        Me.LayoutNombreFree1.Size = New System.Drawing.Size(307, 24)
        Me.LayoutNombreFree1.Text = "ID Paie"
        Me.LayoutNombreFree1.TextSize = New System.Drawing.Size(79, 13)
        '
        'LayoutControlItem10
        '
        Me.LayoutControlItem10.Control = Me.tbPayeNumber
        Me.LayoutControlItem10.Location = New System.Drawing.Point(0, 48)
        Me.LayoutControlItem10.Name = "LayoutControlItem10"
        Me.LayoutControlItem10.Size = New System.Drawing.Size(307, 24)
        Me.LayoutControlItem10.Text = "Numéro de paye"
        Me.LayoutControlItem10.TextSize = New System.Drawing.Size(79, 13)
        '
        'LayoutControlItem18
        '
        Me.LayoutControlItem18.Control = Me.tbPayeDate
        Me.LayoutControlItem18.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem18.CustomizationFormText = "Date du"
        Me.LayoutControlItem18.Location = New System.Drawing.Point(0, 72)
        Me.LayoutControlItem18.Name = "LayoutControlItem18"
        Me.LayoutControlItem18.Size = New System.Drawing.Size(307, 24)
        Me.LayoutControlItem18.Text = "Date paie"
        Me.LayoutControlItem18.TextSize = New System.Drawing.Size(79, 13)
        '
        'LayoutControlItem15
        '
        Me.LayoutControlItem15.Control = Me.cbPeriodes
        Me.LayoutControlItem15.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem15.CustomizationFormText = "Périodes"
        Me.LayoutControlItem15.Location = New System.Drawing.Point(0, 96)
        Me.LayoutControlItem15.Name = "LayoutControlItem15"
        Me.LayoutControlItem15.Size = New System.Drawing.Size(307, 26)
        Me.LayoutControlItem15.Text = "Période"
        Me.LayoutControlItem15.TextSize = New System.Drawing.Size(79, 13)
        '
        'LayoutControlItem13
        '
        Me.LayoutControlItem13.Control = Me.tbDateFrom
        Me.LayoutControlItem13.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem13.CustomizationFormText = "Date du"
        Me.LayoutControlItem13.Location = New System.Drawing.Point(0, 122)
        Me.LayoutControlItem13.Name = "LayoutControlItem13"
        Me.LayoutControlItem13.Size = New System.Drawing.Size(307, 24)
        Me.LayoutControlItem13.Text = "Date du"
        Me.LayoutControlItem13.TextSize = New System.Drawing.Size(79, 13)
        '
        'LayoutControlItem14
        '
        Me.LayoutControlItem14.Control = Me.tbDateAu
        Me.LayoutControlItem14.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem14.CustomizationFormText = "Date du"
        Me.LayoutControlItem14.Location = New System.Drawing.Point(0, 146)
        Me.LayoutControlItem14.Name = "LayoutControlItem14"
        Me.LayoutControlItem14.Size = New System.Drawing.Size(307, 24)
        Me.LayoutControlItem14.Text = "Date au"
        Me.LayoutControlItem14.TextSize = New System.Drawing.Size(79, 13)
        '
        'LayoutControlItem20
        '
        Me.LayoutControlItem20.Control = Me.cbLivreur
        Me.LayoutControlItem20.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem20.CustomizationFormText = "Liste des payes"
        Me.LayoutControlItem20.Location = New System.Drawing.Point(0, 170)
        Me.LayoutControlItem20.Name = "LayoutControlItem20"
        Me.LayoutControlItem20.Size = New System.Drawing.Size(307, 24)
        Me.LayoutControlItem20.Text = "Livreur"
        Me.LayoutControlItem20.TextSize = New System.Drawing.Size(79, 13)
        '
        'LayoutControlItem27
        '
        Me.LayoutControlItem27.Control = Me.cbOrganisation
        Me.LayoutControlItem27.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem27.CustomizationFormText = "Liste des factures"
        Me.LayoutControlItem27.Location = New System.Drawing.Point(0, 194)
        Me.LayoutControlItem27.Name = "LayoutControlItem27"
        Me.LayoutControlItem27.Size = New System.Drawing.Size(307, 24)
        Me.LayoutControlItem27.Text = "Organisation"
        Me.LayoutControlItem27.TextSize = New System.Drawing.Size(79, 13)
        '
        'LayoutControlItem12
        '
        Me.LayoutControlItem12.Control = Me.tbDescription
        Me.LayoutControlItem12.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem12.CustomizationFormText = "LayoutControlItem10"
        Me.LayoutControlItem12.Location = New System.Drawing.Point(0, 218)
        Me.LayoutControlItem12.Name = "LayoutControlItem12"
        Me.LayoutControlItem12.Size = New System.Drawing.Size(307, 24)
        Me.LayoutControlItem12.Text = "Description"
        Me.LayoutControlItem12.TextSize = New System.Drawing.Size(79, 13)
        '
        'lblQuantite
        '
        Me.lblQuantite.Control = Me.tbQuantite
        Me.lblQuantite.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.lblQuantite.CustomizationFormText = "Nombre maximum free echange (305 et 35)"
        Me.lblQuantite.Location = New System.Drawing.Point(0, 242)
        Me.lblQuantite.Name = "lblQuantite"
        Me.lblQuantite.Size = New System.Drawing.Size(307, 24)
        Me.lblQuantite.Text = "Quantité"
        Me.lblQuantite.TextSize = New System.Drawing.Size(79, 13)
        '
        'LayoutPayeAuLivreur
        '
        Me.LayoutPayeAuLivreur.Control = Me.tbPrix
        Me.LayoutPayeAuLivreur.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutPayeAuLivreur.CustomizationFormText = "Payé au livreur"
        Me.LayoutPayeAuLivreur.Location = New System.Drawing.Point(0, 266)
        Me.LayoutPayeAuLivreur.Name = "LayoutPayeAuLivreur"
        Me.LayoutPayeAuLivreur.Size = New System.Drawing.Size(307, 24)
        Me.LayoutPayeAuLivreur.Text = "Prix"
        Me.LayoutPayeAuLivreur.TextSize = New System.Drawing.Size(79, 13)
        '
        'LayoutPayeAuLivreur1
        '
        Me.LayoutPayeAuLivreur1.Control = Me.tbTotal
        Me.LayoutPayeAuLivreur1.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutPayeAuLivreur1.CustomizationFormText = "Payé au livreur"
        Me.LayoutPayeAuLivreur1.Location = New System.Drawing.Point(0, 290)
        Me.LayoutPayeAuLivreur1.Name = "LayoutPayeAuLivreur1"
        Me.LayoutPayeAuLivreur1.Size = New System.Drawing.Size(307, 24)
        Me.LayoutPayeAuLivreur1.Text = "Total"
        Me.LayoutPayeAuLivreur1.TextSize = New System.Drawing.Size(79, 13)
        '
        'LayoutControlItem21
        '
        Me.LayoutControlItem21.Control = Me.radChoice
        Me.LayoutControlItem21.Location = New System.Drawing.Point(307, 0)
        Me.LayoutControlItem21.Name = "LayoutControlItem21"
        Me.LayoutControlItem21.Size = New System.Drawing.Size(304, 337)
        Me.LayoutControlItem21.Text = "Type d'ajout"
        Me.LayoutControlItem21.TextLocation = DevExpress.Utils.Locations.Top
        Me.LayoutControlItem21.TextSize = New System.Drawing.Size(79, 13)
        '
        'LayoutControlItem32
        '
        Me.LayoutControlItem32.Control = Me.chkAjout
        Me.LayoutControlItem32.Location = New System.Drawing.Point(0, 314)
        Me.LayoutControlItem32.Name = "LayoutControlItem32"
        Me.LayoutControlItem32.Size = New System.Drawing.Size(307, 23)
        Me.LayoutControlItem32.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem32.TextVisible = False
        '
        'btnSauvegarde
        '
        Me.btnSauvegarde.Location = New System.Drawing.Point(12, 171)
        Me.btnSauvegarde.Name = "btnSauvegarde"
        Me.btnSauvegarde.Size = New System.Drawing.Size(276, 22)
        Me.btnSauvegarde.StyleController = Me.LayoutControl2
        Me.btnSauvegarde.TabIndex = 129
        Me.btnSauvegarde.Text = "Sauvegarder"
        '
        'frmPaye
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1379, 590)
        Me.Controls.Add(Me.LayoutControl1)
        Me.Controls.Add(Me.TS)
        Me.Name = "frmPaye"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Gestion de la paie"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.TS.ResumeLayout(False)
        Me.TS.PerformLayout()
        CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LayoutControl1.ResumeLayout(False)
        CType(Me.tabData, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabData.ResumeLayout(False)
        Me.XtraTabPage1.ResumeLayout(False)
        CType(Me.LayoutControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LayoutControl2.ResumeLayout(False)
        CType(Me.grdPaye, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabPage2.ResumeLayout(False)
        CType(Me.LayoutControl3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LayoutControl3.ResumeLayout(False)
        CType(Me.grdImport, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemMemoEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemMemoEdit3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemMemoEdit2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemCalcEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemTextEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemTextEdit2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemCalcEdit2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemSpinEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemCheckEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grdPayeLine, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Root, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DockManager1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.DockNouvellePaye.ResumeLayout(False)
        Me.ControlContainer1.ResumeLayout(False)
        CType(Me.LayoutControl5, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LayoutControl5.ResumeLayout(False)
        CType(Me.tbPayeNumberNP.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbLivreurNP.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbDateFromNP.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbDateFromNP.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbDateAuNP.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbDateAuNP.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbPeriodesNP.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbIDPayeNP.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbPayeDateNP.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbPayeDateNP.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem23, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem24, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem25, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem26, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem28, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem29, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem30, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem31, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem11, System.ComponentModel.ISupportInitialize).EndInit()
        Me.DockPaye.ResumeLayout(False)
        Me.DockPanel1_Container.ResumeLayout(False)
        CType(Me.LayoutControl4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LayoutControl4.ResumeLayout(False)
        CType(Me.chkAjout.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radChoice.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbPayeNumber.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbDescription.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbPrix.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbTotal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbQuantite.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbOrganisation.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbLivreur.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbDateFrom.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbDateFrom.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbDateAu.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbDateAu.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbPeriodes.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbIDPaye.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbPayeDate.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbPayeDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbIDPayeLine.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem19, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutSave, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem16, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutNombreFree2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutNombreFree1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem18, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem15, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem13, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem14, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem20, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem27, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblQuantite, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutPayeAuLivreur, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutPayeAuLivreur1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem21, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem32, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents TS As ToolStrip
    Friend WithEvents Head As ToolStripLabel
    Friend WithEvents sp1 As ToolStripSeparator
    Friend WithEvents bDelete As ToolStripButton
    Friend WithEvents sp3 As ToolStripSeparator
    Friend WithEvents bCancel As ToolStripButton
    Friend WithEvents sp4 As ToolStripSeparator
    Friend WithEvents ToolStripButton6 As ToolStripButton
    Friend WithEvents LayoutControl1 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents Root As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents tabData As DevExpress.XtraTab.XtraTabControl
    Friend WithEvents XtraTabPage1 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents LayoutControl2 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents grdPaye As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colID As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colPayeNumber As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colPayeDate As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colIDLivreur As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colLivreur As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colDateFrom As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colDateTo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents LayoutControlGroup1 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem2 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents XtraTabPage2 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents LayoutControl3 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents grdPayeLine As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView2 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colIDInvoiceLine As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colPayeNumber2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colDateFrom2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colDateTo2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colIDLivreur2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colLivreur2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colOrganisation As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colUnite As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colPrix As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colMontant As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCredit As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colNombreGlacier As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colGlacier As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents LayoutControlGroup2 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem1 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem3 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents colKm As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colDescription As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colNombreGlacier2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colTotalGlacier As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents grdImport As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView3 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colImportID As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colReference As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colClient As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemMemoEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit
    Friend WithEvents colClientAddress As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colMomentDePassage As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colPreparePar As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colHeureAppel As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colHeurePrep As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colHeureDepart As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colHeureLivraison As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colMessagePassage As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemMemoEdit3 As DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit
    Friend WithEvents colTotalTempService As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colDistance As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn3 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn4 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemMemoEdit2 As DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit
    Friend WithEvents GridColumn5 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn6 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn7 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colMontantGlacierLivreur As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colMontantGlacierOrganisation As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colMontantLivreur As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents Extras As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colExtraKM As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemCalcEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit
    Friend WithEvents RepositoryItemTextEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents RepositoryItemTextEdit2 As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents RepositoryItemCalcEdit2 As DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit
    Friend WithEvents RepositoryItemSpinEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit
    Friend WithEvents RepositoryItemCheckEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
    Friend WithEvents LayoutControlItem4 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem5 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents bExportToHTML As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents bExportToPDF As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents bExportToDoc As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents bExportToExcel As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LayoutControlItem9 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem7 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem8 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem6 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents DockManager1 As DevExpress.XtraBars.Docking.DockManager
    Friend WithEvents DockPaye As DevExpress.XtraBars.Docking.DockPanel
    Friend WithEvents DockPanel1_Container As DevExpress.XtraBars.Docking.ControlContainer
    Friend WithEvents LayoutControl4 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents tbPayeNumber As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlGroup3 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem10 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem1 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents tbDescription As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlItem12 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents tbPrix As DevExpress.XtraEditors.TextEdit
    Friend WithEvents tbTotal As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutPayeAuLivreur As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutPayeAuLivreur1 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents tbQuantite As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblQuantite As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents cbOrganisation As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LayoutControlItem27 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents bAjouterLaPaye As ToolStripButton
    Friend WithEvents cbLivreur As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LayoutControlItem20 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents tbDateFrom As DevExpress.XtraEditors.DateEdit
    Friend WithEvents tbDateAu As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LayoutControlItem13 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem14 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents cbPeriodes As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LayoutControlItem15 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents bSauvegarde As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LayoutSave As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents btnSauvegarde As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnAnnuler As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LayoutControlItem16 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents tbIDPaye As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutNombreFree1 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents colIDPaye As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents bNouvellePaye As ToolStripButton
    Friend WithEvents DockNouvellePaye As DevExpress.XtraBars.Docking.DockPanel
    Friend WithEvents ControlContainer1 As DevExpress.XtraBars.Docking.ControlContainer
    Friend WithEvents LayoutControl5 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents LayoutControlGroup4 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents bAnnulerNouvellePaye As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents bSauvegarderNouvellePaye As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents tbPayeNumberNP As DevExpress.XtraEditors.TextEdit
    Friend WithEvents cbLivreurNP As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents tbDateFromNP As DevExpress.XtraEditors.DateEdit
    Friend WithEvents tbDateAuNP As DevExpress.XtraEditors.DateEdit
    Friend WithEvents cbPeriodesNP As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents tbIDPayeNP As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlItem23 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem24 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem25 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem26 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem28 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem29 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem30 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem31 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents ToolStripSeparator2 As ToolStripSeparator
    Friend WithEvents tbPayeDate As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LayoutControlItem18 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents tbPayeDateNP As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LayoutControlItem11 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents tbIDPayeLine As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutNombreFree2 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents radChoice As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents radChoix As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents LayoutControlItem19 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem21 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents chkAjout As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LayoutControlItem32 As DevExpress.XtraLayout.LayoutControlItem
End Class
