﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmVentes
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmVentes))
        Me.TS = New System.Windows.Forms.ToolStrip()
        Me.Head = New System.Windows.Forms.ToolStripLabel()
        Me.bAdd = New System.Windows.Forms.ToolStripButton()
        Me.sp1 = New System.Windows.Forms.ToolStripSeparator()
        Me.bEdit = New System.Windows.Forms.ToolStripButton()
        Me.sp2 = New System.Windows.Forms.ToolStripSeparator()
        Me.bDelete = New System.Windows.Forms.ToolStripButton()
        Me.sp3 = New System.Windows.Forms.ToolStripSeparator()
        Me.bCancel = New System.Windows.Forms.ToolStripButton()
        Me.sp4 = New System.Windows.Forms.ToolStripSeparator()
        Me.bRefresh = New System.Windows.Forms.ToolStripButton()
        Me.sp5 = New System.Windows.Forms.ToolStripSeparator()
        Me.ToolStripButton6 = New System.Windows.Forms.ToolStripButton()
        Me.tabData = New DevExpress.XtraTab.XtraTabControl()
        Me.XtraTabPage1 = New DevExpress.XtraTab.XtraTabPage()
        Me.grdVentes = New DevExpress.XtraGrid.GridControl()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colIDVente = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colDateFrom = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colDateTo = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colOrganisation = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colDescription = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colMontant = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.XtraTabPage2 = New DevExpress.XtraTab.XtraTabPage()
        Me.LayoutControl1 = New DevExpress.XtraLayout.LayoutControl()
        Me.TLPBut = New System.Windows.Forms.TableLayoutPanel()
        Me.butApply = New System.Windows.Forms.Button()
        Me.butClose = New System.Windows.Forms.Button()
        Me.butCancel = New System.Windows.Forms.Button()
        Me.nudVenteID = New DevExpress.XtraEditors.TextEdit()
        Me.cbOrganisation = New DevExpress.XtraEditors.LookUpEdit()
        Me.tbDateFrom = New DevExpress.XtraEditors.DateEdit()
        Me.tbDateTo = New DevExpress.XtraEditors.DateEdit()
        Me.cbPeriodes = New DevExpress.XtraEditors.LookUpEdit()
        Me.tbQuantite = New DevExpress.XtraEditors.TextEdit()
        Me.tbDescription = New DevExpress.XtraEditors.TextEdit()
        Me.tbTotal = New DevExpress.XtraEditors.TextEdit()
        Me.tbDate = New DevExpress.XtraEditors.DateEdit()
        Me.LayoutControlGroup1 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem20 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem8 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.lblOrganisation = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem19 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem17 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem18 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.lblDescription = New DevExpress.XtraLayout.LayoutControlItem()
        Me.lblQuantite = New DevExpress.XtraLayout.LayoutControlItem()
        Me.lblTotalI = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem1 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlItem1 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.DockManager1 = New DevExpress.XtraBars.Docking.DockManager(Me.components)
        Me.TS.SuspendLayout()
        CType(Me.tabData, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabData.SuspendLayout()
        Me.XtraTabPage1.SuspendLayout()
        CType(Me.grdVentes, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabPage2.SuspendLayout()
        CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LayoutControl1.SuspendLayout()
        Me.TLPBut.SuspendLayout()
        CType(Me.nudVenteID.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbOrganisation.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbDateFrom.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbDateFrom.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbDateTo.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbDateTo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbPeriodes.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbQuantite.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbDescription.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbTotal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbDate.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem20, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblOrganisation, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem19, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem17, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem18, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblDescription, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblQuantite, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblTotalI, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DockManager1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'TS
        '
        Me.TS.AutoSize = False
        Me.TS.Font = New System.Drawing.Font("Verdana", 9.75!)
        Me.TS.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Head, Me.bAdd, Me.sp1, Me.bEdit, Me.sp2, Me.bDelete, Me.sp3, Me.bCancel, Me.sp4, Me.bRefresh, Me.sp5, Me.ToolStripButton6})
        Me.TS.Location = New System.Drawing.Point(0, 0)
        Me.TS.Name = "TS"
        Me.TS.Size = New System.Drawing.Size(955, 39)
        Me.TS.TabIndex = 7
        '
        'Head
        '
        Me.Head.Name = "Head"
        Me.Head.Size = New System.Drawing.Size(0, 36)
        '
        'bAdd
        '
        Me.bAdd.Image = CType(resources.GetObject("bAdd.Image"), System.Drawing.Image)
        Me.bAdd.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.bAdd.Name = "bAdd"
        Me.bAdd.Size = New System.Drawing.Size(76, 36)
        Me.bAdd.Text = "Ajouter"
        '
        'sp1
        '
        Me.sp1.Name = "sp1"
        Me.sp1.Size = New System.Drawing.Size(6, 39)
        '
        'bEdit
        '
        Me.bEdit.Image = CType(resources.GetObject("bEdit.Image"), System.Drawing.Image)
        Me.bEdit.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.bEdit.Name = "bEdit"
        Me.bEdit.Size = New System.Drawing.Size(72, 36)
        Me.bEdit.Text = "Edition"
        '
        'sp2
        '
        Me.sp2.Name = "sp2"
        Me.sp2.Size = New System.Drawing.Size(6, 39)
        '
        'bDelete
        '
        Me.bDelete.Image = CType(resources.GetObject("bDelete.Image"), System.Drawing.Image)
        Me.bDelete.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.bDelete.Name = "bDelete"
        Me.bDelete.Size = New System.Drawing.Size(93, 36)
        Me.bDelete.Text = "Supprimer"
        '
        'sp3
        '
        Me.sp3.Name = "sp3"
        Me.sp3.Size = New System.Drawing.Size(6, 39)
        '
        'bCancel
        '
        Me.bCancel.Image = CType(resources.GetObject("bCancel.Image"), System.Drawing.Image)
        Me.bCancel.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.bCancel.Name = "bCancel"
        Me.bCancel.Size = New System.Drawing.Size(77, 36)
        Me.bCancel.Text = "Annuler"
        '
        'sp4
        '
        Me.sp4.Name = "sp4"
        Me.sp4.Size = New System.Drawing.Size(6, 39)
        '
        'bRefresh
        '
        Me.bRefresh.Image = CType(resources.GetObject("bRefresh.Image"), System.Drawing.Image)
        Me.bRefresh.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.bRefresh.Name = "bRefresh"
        Me.bRefresh.Size = New System.Drawing.Size(89, 36)
        Me.bRefresh.Text = "Rafraîchir"
        '
        'sp5
        '
        Me.sp5.Name = "sp5"
        Me.sp5.Size = New System.Drawing.Size(6, 39)
        '
        'ToolStripButton6
        '
        Me.ToolStripButton6.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.ToolStripButton6.Image = CType(resources.GetObject("ToolStripButton6.Image"), System.Drawing.Image)
        Me.ToolStripButton6.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton6.Name = "ToolStripButton6"
        Me.ToolStripButton6.Size = New System.Drawing.Size(73, 36)
        Me.ToolStripButton6.Text = "Fermer"
        '
        'tabData
        '
        Me.tabData.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tabData.Location = New System.Drawing.Point(0, 39)
        Me.tabData.Name = "tabData"
        Me.tabData.SelectedTabPage = Me.XtraTabPage1
        Me.tabData.Size = New System.Drawing.Size(955, 477)
        Me.tabData.TabIndex = 8
        Me.tabData.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.XtraTabPage1, Me.XtraTabPage2})
        '
        'XtraTabPage1
        '
        Me.XtraTabPage1.Controls.Add(Me.grdVentes)
        Me.XtraTabPage1.Name = "XtraTabPage1"
        Me.XtraTabPage1.Size = New System.Drawing.Size(950, 451)
        Me.XtraTabPage1.Text = "Liste des Ventes"
        '
        'grdVentes
        '
        Me.grdVentes.AllowRestoreSelectionAndFocusedRow = DevExpress.Utils.DefaultBoolean.[True]
        Me.grdVentes.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grdVentes.Location = New System.Drawing.Point(0, 0)
        Me.grdVentes.MainView = Me.GridView1
        Me.grdVentes.Name = "grdVentes"
        Me.grdVentes.Size = New System.Drawing.Size(947, 433)
        Me.grdVentes.TabIndex = 0
        Me.grdVentes.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'GridView1
        '
        Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colIDVente, Me.colDateFrom, Me.colDateTo, Me.colOrganisation, Me.colDescription, Me.colMontant})
        Me.GridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.GridView1.GridControl = Me.grdVentes
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsBehavior.Editable = False
        Me.GridView1.OptionsCustomization.AllowGroup = False
        Me.GridView1.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridView1.OptionsSelection.UseIndicatorForSelection = False
        Me.GridView1.OptionsView.ShowAutoFilterRow = True
        Me.GridView1.OptionsView.ShowGroupPanel = False
        Me.GridView1.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.colDateFrom, DevExpress.Data.ColumnSortOrder.Ascending)})
        '
        'colIDVente
        '
        Me.colIDVente.Caption = "ID Vente"
        Me.colIDVente.FieldName = "IDVente"
        Me.colIDVente.Name = "colIDVente"
        Me.colIDVente.Visible = True
        Me.colIDVente.VisibleIndex = 0
        Me.colIDVente.Width = 94
        '
        'colDateFrom
        '
        Me.colDateFrom.Caption = "Date du"
        Me.colDateFrom.FieldName = "DateFrom"
        Me.colDateFrom.Name = "colDateFrom"
        Me.colDateFrom.Visible = True
        Me.colDateFrom.VisibleIndex = 1
        Me.colDateFrom.Width = 94
        '
        'colDateTo
        '
        Me.colDateTo.Caption = "Date au"
        Me.colDateTo.FieldName = "DateTo"
        Me.colDateTo.Name = "colDateTo"
        Me.colDateTo.Visible = True
        Me.colDateTo.VisibleIndex = 2
        Me.colDateTo.Width = 94
        '
        'colOrganisation
        '
        Me.colOrganisation.Caption = "Organisation"
        Me.colOrganisation.FieldName = "Organisation"
        Me.colOrganisation.Name = "colOrganisation"
        Me.colOrganisation.Visible = True
        Me.colOrganisation.VisibleIndex = 3
        '
        'colDescription
        '
        Me.colDescription.Caption = "Description"
        Me.colDescription.FieldName = "Description"
        Me.colDescription.Name = "colDescription"
        Me.colDescription.Visible = True
        Me.colDescription.VisibleIndex = 4
        '
        'colMontant
        '
        Me.colMontant.Caption = "Montant"
        Me.colMontant.FieldName = "Montant"
        Me.colMontant.Name = "colMontant"
        Me.colMontant.Visible = True
        Me.colMontant.VisibleIndex = 5
        '
        'XtraTabPage2
        '
        Me.XtraTabPage2.Controls.Add(Me.LayoutControl1)
        Me.XtraTabPage2.Name = "XtraTabPage2"
        Me.XtraTabPage2.PageVisible = False
        Me.XtraTabPage2.Size = New System.Drawing.Size(950, 451)
        Me.XtraTabPage2.Text = "Détail sur une vente"
        '
        'LayoutControl1
        '
        Me.LayoutControl1.Controls.Add(Me.TLPBut)
        Me.LayoutControl1.Controls.Add(Me.nudVenteID)
        Me.LayoutControl1.Controls.Add(Me.cbOrganisation)
        Me.LayoutControl1.Controls.Add(Me.tbDateFrom)
        Me.LayoutControl1.Controls.Add(Me.tbDateTo)
        Me.LayoutControl1.Controls.Add(Me.cbPeriodes)
        Me.LayoutControl1.Controls.Add(Me.tbQuantite)
        Me.LayoutControl1.Controls.Add(Me.tbDescription)
        Me.LayoutControl1.Controls.Add(Me.tbTotal)
        Me.LayoutControl1.Controls.Add(Me.tbDate)
        Me.LayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LayoutControl1.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControl1.Name = "LayoutControl1"
        Me.LayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = New System.Drawing.Rectangle(1125, 201, 771, 350)
        Me.LayoutControl1.OptionsCustomizationForm.ShowPropertyGrid = True
        Me.LayoutControl1.Root = Me.LayoutControlGroup1
        Me.LayoutControl1.Size = New System.Drawing.Size(950, 451)
        Me.LayoutControl1.TabIndex = 0
        Me.LayoutControl1.Text = "LayoutControl1"
        '
        'TLPBut
        '
        Me.TLPBut.ColumnCount = 7
        Me.TLPBut.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TLPBut.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100.0!))
        Me.TLPBut.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TLPBut.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100.0!))
        Me.TLPBut.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TLPBut.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100.0!))
        Me.TLPBut.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TLPBut.Controls.Add(Me.butApply, 1, 1)
        Me.TLPBut.Controls.Add(Me.butClose, 5, 1)
        Me.TLPBut.Controls.Add(Me.butCancel, 3, 1)
        Me.TLPBut.Location = New System.Drawing.Point(12, 230)
        Me.TLPBut.Name = "TLPBut"
        Me.TLPBut.RowCount = 2
        Me.TLPBut.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26.0!))
        Me.TLPBut.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 34.0!))
        Me.TLPBut.Size = New System.Drawing.Size(926, 209)
        Me.TLPBut.TabIndex = 8
        '
        'butApply
        '
        Me.butApply.Dock = System.Windows.Forms.DockStyle.Top
        Me.butApply.Location = New System.Drawing.Point(296, 29)
        Me.butApply.Name = "butApply"
        Me.butApply.Size = New System.Drawing.Size(94, 28)
        Me.butApply.TabIndex = 20
        Me.butApply.Text = "Appliquer"
        '
        'butClose
        '
        Me.butClose.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.butClose.Dock = System.Windows.Forms.DockStyle.Top
        Me.butClose.Location = New System.Drawing.Point(536, 29)
        Me.butClose.Name = "butClose"
        Me.butClose.Size = New System.Drawing.Size(94, 28)
        Me.butClose.TabIndex = 22
        Me.butClose.Text = "Fermer"
        '
        'butCancel
        '
        Me.butCancel.Dock = System.Windows.Forms.DockStyle.Top
        Me.butCancel.Location = New System.Drawing.Point(416, 29)
        Me.butCancel.Name = "butCancel"
        Me.butCancel.Size = New System.Drawing.Size(94, 28)
        Me.butCancel.TabIndex = 21
        Me.butCancel.Text = "Retour a la grille"
        '
        'nudVenteID
        '
        Me.nudVenteID.Enabled = False
        Me.nudVenteID.Location = New System.Drawing.Point(76, 12)
        Me.nudVenteID.Name = "nudVenteID"
        Me.nudVenteID.Properties.Appearance.BackColor = System.Drawing.Color.Azure
        Me.nudVenteID.Properties.Appearance.Options.UseBackColor = True
        Me.nudVenteID.Size = New System.Drawing.Size(286, 20)
        Me.nudVenteID.StyleController = Me.LayoutControl1
        Me.nudVenteID.TabIndex = 5
        '
        'cbOrganisation
        '
        Me.cbOrganisation.Location = New System.Drawing.Point(76, 134)
        Me.cbOrganisation.Name = "cbOrganisation"
        Me.cbOrganisation.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbOrganisation.Size = New System.Drawing.Size(286, 20)
        Me.cbOrganisation.StyleController = Me.LayoutControl1
        Me.cbOrganisation.TabIndex = 36
        '
        'tbDateFrom
        '
        Me.tbDateFrom.EditValue = Nothing
        Me.tbDateFrom.Location = New System.Drawing.Point(76, 86)
        Me.tbDateFrom.Name = "tbDateFrom"
        Me.tbDateFrom.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.tbDateFrom.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.tbDateFrom.Properties.DisplayFormat.FormatString = "MM-dd-yy H:mm"
        Me.tbDateFrom.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.tbDateFrom.Properties.EditFormat.FormatString = "MM-dd-yy H:mm"
        Me.tbDateFrom.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.tbDateFrom.Properties.Mask.EditMask = "MM-dd-yy H:mm"
        Me.tbDateFrom.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.tbDateFrom.Size = New System.Drawing.Size(286, 20)
        Me.tbDateFrom.StyleController = Me.LayoutControl1
        Me.tbDateFrom.TabIndex = 22
        '
        'tbDateTo
        '
        Me.tbDateTo.EditValue = Nothing
        Me.tbDateTo.Location = New System.Drawing.Point(76, 110)
        Me.tbDateTo.Name = "tbDateTo"
        Me.tbDateTo.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.tbDateTo.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.tbDateTo.Properties.DisplayFormat.FormatString = "MM-dd-yy H:mm"
        Me.tbDateTo.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.tbDateTo.Properties.EditFormat.FormatString = "MM-dd-yy H:mm"
        Me.tbDateTo.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.tbDateTo.Properties.Mask.EditMask = "MM-dd-yy H:mm"
        Me.tbDateTo.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.tbDateTo.Size = New System.Drawing.Size(286, 20)
        Me.tbDateTo.StyleController = Me.LayoutControl1
        Me.tbDateTo.TabIndex = 22
        '
        'cbPeriodes
        '
        Me.cbPeriodes.Location = New System.Drawing.Point(76, 60)
        Me.cbPeriodes.Name = "cbPeriodes"
        Me.cbPeriodes.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbPeriodes.Properties.Appearance.Options.UseFont = True
        Me.cbPeriodes.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbPeriodes.Size = New System.Drawing.Size(286, 22)
        Me.cbPeriodes.StyleController = Me.LayoutControl1
        Me.cbPeriodes.TabIndex = 28
        '
        'tbQuantite
        '
        Me.tbQuantite.EditValue = "1"
        Me.tbQuantite.Location = New System.Drawing.Point(76, 182)
        Me.tbQuantite.Name = "tbQuantite"
        Me.tbQuantite.Properties.Mask.EditMask = "n0"
        Me.tbQuantite.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.tbQuantite.Size = New System.Drawing.Size(286, 20)
        Me.tbQuantite.StyleController = Me.LayoutControl1
        Me.tbQuantite.TabIndex = 132
        '
        'tbDescription
        '
        Me.tbDescription.Location = New System.Drawing.Point(76, 158)
        Me.tbDescription.Name = "tbDescription"
        Me.tbDescription.Size = New System.Drawing.Size(286, 20)
        Me.tbDescription.StyleController = Me.LayoutControl1
        Me.tbDescription.TabIndex = 4
        '
        'tbTotal
        '
        Me.tbTotal.EditValue = "$0.00"
        Me.tbTotal.Location = New System.Drawing.Point(76, 206)
        Me.tbTotal.Name = "tbTotal"
        Me.tbTotal.Properties.DisplayFormat.FormatString = "c"
        Me.tbTotal.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.tbTotal.Properties.EditFormat.FormatString = "c"
        Me.tbTotal.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.tbTotal.Properties.Mask.EditMask = "c"
        Me.tbTotal.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.tbTotal.Size = New System.Drawing.Size(286, 20)
        Me.tbTotal.StyleController = Me.LayoutControl1
        Me.tbTotal.TabIndex = 139
        '
        'tbDate
        '
        Me.tbDate.EditValue = Nothing
        Me.tbDate.Location = New System.Drawing.Point(76, 36)
        Me.tbDate.Name = "tbDate"
        Me.tbDate.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.tbDate.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.tbDate.Properties.DisplayFormat.FormatString = "MM-dd-yy"
        Me.tbDate.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.tbDate.Properties.EditFormat.FormatString = "MM-dd-yy"
        Me.tbDate.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.tbDate.Properties.Mask.EditMask = "MM-dd-yy"
        Me.tbDate.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.tbDate.Size = New System.Drawing.Size(286, 20)
        Me.tbDate.StyleController = Me.LayoutControl1
        Me.tbDate.TabIndex = 22
        '
        'LayoutControlGroup1
        '
        Me.LayoutControlGroup1.CustomizationFormText = "LayoutControlGroup1"
        Me.LayoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup1.GroupBordersVisible = False
        Me.LayoutControlGroup1.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem20, Me.LayoutControlItem8, Me.lblOrganisation, Me.LayoutControlItem19, Me.LayoutControlItem17, Me.LayoutControlItem18, Me.lblDescription, Me.lblQuantite, Me.lblTotalI, Me.EmptySpaceItem1, Me.LayoutControlItem1})
        Me.LayoutControlGroup1.Name = "Root"
        Me.LayoutControlGroup1.Size = New System.Drawing.Size(950, 451)
        Me.LayoutControlGroup1.TextVisible = False
        '
        'LayoutControlItem20
        '
        Me.LayoutControlItem20.Control = Me.TLPBut
        Me.LayoutControlItem20.CustomizationFormText = "LayoutControlItem20"
        Me.LayoutControlItem20.Location = New System.Drawing.Point(0, 218)
        Me.LayoutControlItem20.Name = "LayoutControlItem20"
        Me.LayoutControlItem20.Size = New System.Drawing.Size(930, 213)
        Me.LayoutControlItem20.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem20.TextVisible = False
        '
        'LayoutControlItem8
        '
        Me.LayoutControlItem8.Control = Me.nudVenteID
        Me.LayoutControlItem8.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem8.Name = "LayoutControlItem8"
        Me.LayoutControlItem8.Size = New System.Drawing.Size(354, 24)
        Me.LayoutControlItem8.Text = "IDVente"
        Me.LayoutControlItem8.TextSize = New System.Drawing.Size(61, 13)
        '
        'lblOrganisation
        '
        Me.lblOrganisation.Control = Me.cbOrganisation
        Me.lblOrganisation.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.lblOrganisation.CustomizationFormText = "Liste des factures"
        Me.lblOrganisation.Location = New System.Drawing.Point(0, 122)
        Me.lblOrganisation.Name = "lblOrganisation"
        Me.lblOrganisation.Size = New System.Drawing.Size(354, 24)
        Me.lblOrganisation.Text = "Organisation"
        Me.lblOrganisation.TextSize = New System.Drawing.Size(61, 13)
        '
        'LayoutControlItem19
        '
        Me.LayoutControlItem19.Control = Me.cbPeriodes
        Me.LayoutControlItem19.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem19.CustomizationFormText = "Périodes"
        Me.LayoutControlItem19.Location = New System.Drawing.Point(0, 48)
        Me.LayoutControlItem19.Name = "LayoutControlItem19"
        Me.LayoutControlItem19.Size = New System.Drawing.Size(354, 26)
        Me.LayoutControlItem19.Text = "Période"
        Me.LayoutControlItem19.TextSize = New System.Drawing.Size(61, 13)
        '
        'LayoutControlItem17
        '
        Me.LayoutControlItem17.Control = Me.tbDateFrom
        Me.LayoutControlItem17.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem17.CustomizationFormText = "Date du"
        Me.LayoutControlItem17.Location = New System.Drawing.Point(0, 74)
        Me.LayoutControlItem17.Name = "LayoutControlItem17"
        Me.LayoutControlItem17.Size = New System.Drawing.Size(354, 24)
        Me.LayoutControlItem17.Text = "Date du"
        Me.LayoutControlItem17.TextSize = New System.Drawing.Size(61, 13)
        '
        'LayoutControlItem18
        '
        Me.LayoutControlItem18.Control = Me.tbDateTo
        Me.LayoutControlItem18.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem18.CustomizationFormText = "Date du"
        Me.LayoutControlItem18.Location = New System.Drawing.Point(0, 98)
        Me.LayoutControlItem18.Name = "LayoutControlItem18"
        Me.LayoutControlItem18.Size = New System.Drawing.Size(354, 24)
        Me.LayoutControlItem18.Text = "Date au"
        Me.LayoutControlItem18.TextSize = New System.Drawing.Size(61, 13)
        '
        'lblDescription
        '
        Me.lblDescription.Control = Me.tbDescription
        Me.lblDescription.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.lblDescription.CustomizationFormText = "LayoutControlItem10"
        Me.lblDescription.Location = New System.Drawing.Point(0, 146)
        Me.lblDescription.Name = "lblDescription"
        Me.lblDescription.Size = New System.Drawing.Size(354, 24)
        Me.lblDescription.Text = "Description"
        Me.lblDescription.TextSize = New System.Drawing.Size(61, 13)
        '
        'lblQuantite
        '
        Me.lblQuantite.Control = Me.tbQuantite
        Me.lblQuantite.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.lblQuantite.CustomizationFormText = "Nombre maximum free echange (305 et 35)"
        Me.lblQuantite.Location = New System.Drawing.Point(0, 170)
        Me.lblQuantite.Name = "lblQuantite"
        Me.lblQuantite.Size = New System.Drawing.Size(354, 24)
        Me.lblQuantite.Text = "Quantité"
        Me.lblQuantite.TextSize = New System.Drawing.Size(61, 13)
        '
        'lblTotalI
        '
        Me.lblTotalI.Control = Me.tbTotal
        Me.lblTotalI.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.lblTotalI.CustomizationFormText = "lblPrixI"
        Me.lblTotalI.Location = New System.Drawing.Point(0, 194)
        Me.lblTotalI.Name = "lblTotalI"
        Me.lblTotalI.Size = New System.Drawing.Size(354, 24)
        Me.lblTotalI.Text = "Total"
        Me.lblTotalI.TextSize = New System.Drawing.Size(61, 13)
        '
        'EmptySpaceItem1
        '
        Me.EmptySpaceItem1.AllowHotTrack = False
        Me.EmptySpaceItem1.Location = New System.Drawing.Point(354, 0)
        Me.EmptySpaceItem1.Name = "EmptySpaceItem1"
        Me.EmptySpaceItem1.Size = New System.Drawing.Size(576, 218)
        Me.EmptySpaceItem1.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlItem1
        '
        Me.LayoutControlItem1.Control = Me.tbDate
        Me.LayoutControlItem1.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem1.CustomizationFormText = "Date du"
        Me.LayoutControlItem1.Location = New System.Drawing.Point(0, 24)
        Me.LayoutControlItem1.Name = "LayoutControlItem1"
        Me.LayoutControlItem1.Size = New System.Drawing.Size(354, 24)
        Me.LayoutControlItem1.Text = "Date"
        Me.LayoutControlItem1.TextSize = New System.Drawing.Size(61, 13)
        '
        'DockManager1
        '
        Me.DockManager1.Form = Me
        Me.DockManager1.TopZIndexControls.AddRange(New String() {"DevExpress.XtraBars.BarDockControl", "DevExpress.XtraBars.StandaloneBarDockControl", "System.Windows.Forms.MenuStrip", "System.Windows.Forms.StatusStrip", "System.Windows.Forms.StatusBar", "DevExpress.XtraBars.Ribbon.RibbonStatusBar", "DevExpress.XtraBars.Ribbon.RibbonControl", "DevExpress.XtraBars.Navigation.OfficeNavigationBar", "DevExpress.XtraBars.Navigation.TileNavPane", "DevExpress.XtraBars.TabFormControl", "DevExpress.XtraBars.FluentDesignSystem.FluentDesignFormControl", "DevExpress.XtraBars.ToolbarForm.ToolbarFormControl"})
        '
        'frmVentes
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(955, 516)
        Me.Controls.Add(Me.tabData)
        Me.Controls.Add(Me.TS)
        Me.Name = "frmVentes"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Gestion des ventes"
        Me.TS.ResumeLayout(False)
        Me.TS.PerformLayout()
        CType(Me.tabData, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabData.ResumeLayout(False)
        Me.XtraTabPage1.ResumeLayout(False)
        CType(Me.grdVentes, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabPage2.ResumeLayout(False)
        CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LayoutControl1.ResumeLayout(False)
        Me.TLPBut.ResumeLayout(False)
        CType(Me.nudVenteID.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbOrganisation.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbDateFrom.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbDateFrom.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbDateTo.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbDateTo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbPeriodes.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbQuantite.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbDescription.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbTotal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbDate.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem20, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblOrganisation, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem19, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem17, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem18, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblDescription, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblQuantite, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblTotalI, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DockManager1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents TS As ToolStrip
    Friend WithEvents Head As ToolStripLabel
    Friend WithEvents bAdd As ToolStripButton
    Friend WithEvents sp1 As ToolStripSeparator
    Friend WithEvents bEdit As ToolStripButton
    Friend WithEvents sp2 As ToolStripSeparator
    Friend WithEvents bDelete As ToolStripButton
    Friend WithEvents sp3 As ToolStripSeparator
    Friend WithEvents bCancel As ToolStripButton
    Friend WithEvents sp4 As ToolStripSeparator
    Friend WithEvents bRefresh As ToolStripButton
    Friend WithEvents sp5 As ToolStripSeparator
    Friend WithEvents ToolStripButton6 As ToolStripButton
    Friend WithEvents tabData As DevExpress.XtraTab.XtraTabControl
    Friend WithEvents XtraTabPage1 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents grdVentes As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colIDVente As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colDateFrom As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colDateTo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents XtraTabPage2 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents LayoutControl1 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents TLPBut As TableLayoutPanel
    Friend WithEvents butApply As Button
    Friend WithEvents butClose As Button
    Friend WithEvents butCancel As Button
    Friend WithEvents nudVenteID As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlGroup1 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem20 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem8 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents cbOrganisation As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents lblOrganisation As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents tbDateFrom As DevExpress.XtraEditors.DateEdit
    Friend WithEvents tbDateTo As DevExpress.XtraEditors.DateEdit
    Friend WithEvents cbPeriodes As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LayoutControlItem19 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem17 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem18 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents tbQuantite As DevExpress.XtraEditors.TextEdit
    Friend WithEvents tbDescription As DevExpress.XtraEditors.TextEdit
    Friend WithEvents tbTotal As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblDescription As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents lblQuantite As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents lblTotalI As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem1 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents colOrganisation As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colDescription As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colMontant As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents tbDate As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LayoutControlItem1 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents DockManager1 As DevExpress.XtraBars.Docking.DockManager
End Class
