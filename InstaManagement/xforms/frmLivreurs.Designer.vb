﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmLivreurs
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmLivreurs))
        Me.TS = New System.Windows.Forms.ToolStrip()
        Me.Head = New System.Windows.Forms.ToolStripLabel()
        Me.bAdd = New System.Windows.Forms.ToolStripButton()
        Me.sp1 = New System.Windows.Forms.ToolStripSeparator()
        Me.bEdit = New System.Windows.Forms.ToolStripButton()
        Me.sp2 = New System.Windows.Forms.ToolStripSeparator()
        Me.bCancel = New System.Windows.Forms.ToolStripButton()
        Me.sp4 = New System.Windows.Forms.ToolStripSeparator()
        Me.bRefresh = New System.Windows.Forms.ToolStripButton()
        Me.sp5 = New System.Windows.Forms.ToolStripSeparator()
        Me.ToolStripButton6 = New System.Windows.Forms.ToolStripButton()
        Me.tabData = New DevExpress.XtraTab.XtraTabControl()
        Me.XtraTabPage1 = New DevExpress.XtraTab.XtraTabPage()
        Me.LayoutControl2 = New DevExpress.XtraLayout.LayoutControl()
        Me.tbIDLivreurChangerDestination = New DevExpress.XtraEditors.LookUpEdit()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.tbIDLivreurDepart = New DevExpress.XtraEditors.TextEdit()
        Me.ProgressBarControl1 = New DevExpress.XtraEditors.ProgressBarControl()
        Me.grdLivreurs = New DevExpress.XtraGrid.GridControl()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colLivreurID = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colLivreur = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colTelephone = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colIsVendeur = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.Actif = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.CalculerTaxes = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.Email = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.optFiltreGrille = New DevExpress.XtraEditors.RadioGroup()
        Me.Root = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem7 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem29 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem12 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlGroup2 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem16 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem18 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem19 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.XtraTabPage2 = New DevExpress.XtraTab.XtraTabPage()
        Me.LayoutControl1 = New DevExpress.XtraLayout.LayoutControl()
        Me.ckActif = New DevExpress.XtraEditors.CheckEdit()
        Me.ckVendeur = New DevExpress.XtraEditors.CheckEdit()
        Me.ckCalculerTaxes = New DevExpress.XtraEditors.CheckEdit()
        Me.tbTelephone = New DevExpress.XtraEditors.TextEdit()
        Me.tbAdresse = New DevExpress.XtraEditors.MemoEdit()
        Me.TLPBut = New System.Windows.Forms.TableLayoutPanel()
        Me.butApply = New System.Windows.Forms.Button()
        Me.butClose = New System.Windows.Forms.Button()
        Me.butCancel = New System.Windows.Forms.Button()
        Me.tbLivreur = New DevExpress.XtraEditors.TextEdit()
        Me.nudLivreurID = New DevExpress.XtraEditors.TextEdit()
        Me.tbEmail = New DevExpress.XtraEditors.TextEdit()
        Me.tbLivreurCache = New DevExpress.XtraEditors.TextEdit()
        Me.tbCommissionVendeur = New DevExpress.XtraEditors.TextEdit()
        Me.cbVendeur = New DevExpress.XtraEditors.LookUpEdit()
        Me.bAnnulerVendeur = New DevExpress.XtraEditors.SimpleButton()
        Me.tbNomCompagnie = New DevExpress.XtraEditors.TextEdit()
        Me.tbNoTPS = New DevExpress.XtraEditors.TextEdit()
        Me.tbNoTVQ = New DevExpress.XtraEditors.TextEdit()
        Me.LayoutControlGroup1 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem4 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem20 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem5 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem8 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem1 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem2 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem3 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlGroup3 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem10 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem6 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem9 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlGroup15 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem79 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem11 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem73 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem13 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem14 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem15 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.tbEmail1 = New DevExpress.XtraEditors.TextEdit()
        Me.LayoutControlItem17 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.tbEmail2 = New DevExpress.XtraEditors.TextEdit()
        Me.LayoutControlItem21 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.TS.SuspendLayout()
        CType(Me.tabData, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabData.SuspendLayout()
        Me.XtraTabPage1.SuspendLayout()
        CType(Me.LayoutControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LayoutControl2.SuspendLayout()
        CType(Me.tbIDLivreurChangerDestination.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbIDLivreurDepart.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ProgressBarControl1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grdLivreurs, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.optFiltreGrille.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Root, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem29, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem16, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem18, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem19, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabPage2.SuspendLayout()
        CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LayoutControl1.SuspendLayout()
        CType(Me.ckActif.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ckVendeur.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ckCalculerTaxes.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbTelephone.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbAdresse.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TLPBut.SuspendLayout()
        CType(Me.tbLivreur.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudLivreurID.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbEmail.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbLivreurCache.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbCommissionVendeur.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbVendeur.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbNomCompagnie.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbNoTPS.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbNoTVQ.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem20, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup15, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem79, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem73, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem13, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem14, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem15, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbEmail1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem17, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbEmail2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem21, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'TS
        '
        Me.TS.AutoSize = False
        Me.TS.Font = New System.Drawing.Font("Verdana", 9.75!)
        Me.TS.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Head, Me.bAdd, Me.sp1, Me.bEdit, Me.sp2, Me.bCancel, Me.sp4, Me.bRefresh, Me.sp5, Me.ToolStripButton6})
        Me.TS.Location = New System.Drawing.Point(0, 0)
        Me.TS.Name = "TS"
        Me.TS.Size = New System.Drawing.Size(1100, 39)
        Me.TS.TabIndex = 6
        '
        'Head
        '
        Me.Head.Name = "Head"
        Me.Head.Size = New System.Drawing.Size(0, 36)
        '
        'bAdd
        '
        Me.bAdd.Image = CType(resources.GetObject("bAdd.Image"), System.Drawing.Image)
        Me.bAdd.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.bAdd.Name = "bAdd"
        Me.bAdd.Size = New System.Drawing.Size(76, 36)
        Me.bAdd.Text = "Ajouter"
        '
        'sp1
        '
        Me.sp1.Name = "sp1"
        Me.sp1.Size = New System.Drawing.Size(6, 39)
        '
        'bEdit
        '
        Me.bEdit.Image = CType(resources.GetObject("bEdit.Image"), System.Drawing.Image)
        Me.bEdit.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.bEdit.Name = "bEdit"
        Me.bEdit.Size = New System.Drawing.Size(72, 36)
        Me.bEdit.Text = "Edition"
        '
        'sp2
        '
        Me.sp2.Name = "sp2"
        Me.sp2.Size = New System.Drawing.Size(6, 39)
        '
        'bCancel
        '
        Me.bCancel.Image = CType(resources.GetObject("bCancel.Image"), System.Drawing.Image)
        Me.bCancel.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.bCancel.Name = "bCancel"
        Me.bCancel.Size = New System.Drawing.Size(77, 36)
        Me.bCancel.Text = "Annuler"
        '
        'sp4
        '
        Me.sp4.Name = "sp4"
        Me.sp4.Size = New System.Drawing.Size(6, 39)
        '
        'bRefresh
        '
        Me.bRefresh.Image = CType(resources.GetObject("bRefresh.Image"), System.Drawing.Image)
        Me.bRefresh.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.bRefresh.Name = "bRefresh"
        Me.bRefresh.Size = New System.Drawing.Size(89, 36)
        Me.bRefresh.Text = "Rafraîchir"
        '
        'sp5
        '
        Me.sp5.Name = "sp5"
        Me.sp5.Size = New System.Drawing.Size(6, 39)
        '
        'ToolStripButton6
        '
        Me.ToolStripButton6.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.ToolStripButton6.Image = CType(resources.GetObject("ToolStripButton6.Image"), System.Drawing.Image)
        Me.ToolStripButton6.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton6.Name = "ToolStripButton6"
        Me.ToolStripButton6.Size = New System.Drawing.Size(73, 36)
        Me.ToolStripButton6.Text = "Fermer"
        '
        'tabData
        '
        Me.tabData.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tabData.Location = New System.Drawing.Point(0, 39)
        Me.tabData.Name = "tabData"
        Me.tabData.SelectedTabPage = Me.XtraTabPage1
        Me.tabData.Size = New System.Drawing.Size(1100, 529)
        Me.tabData.TabIndex = 7
        Me.tabData.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.XtraTabPage1, Me.XtraTabPage2})
        '
        'XtraTabPage1
        '
        Me.XtraTabPage1.Controls.Add(Me.LayoutControl2)
        Me.XtraTabPage1.Name = "XtraTabPage1"
        Me.XtraTabPage1.Size = New System.Drawing.Size(1095, 459)
        Me.XtraTabPage1.Text = "Liste des Livreurs / vendeurs"
        '
        'LayoutControl2
        '
        Me.LayoutControl2.Controls.Add(Me.tbIDLivreurChangerDestination)
        Me.LayoutControl2.Controls.Add(Me.Button1)
        Me.LayoutControl2.Controls.Add(Me.tbIDLivreurDepart)
        Me.LayoutControl2.Controls.Add(Me.ProgressBarControl1)
        Me.LayoutControl2.Controls.Add(Me.grdLivreurs)
        Me.LayoutControl2.Controls.Add(Me.optFiltreGrille)
        Me.LayoutControl2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LayoutControl2.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControl2.Name = "LayoutControl2"
        Me.LayoutControl2.Root = Me.Root
        Me.LayoutControl2.Size = New System.Drawing.Size(1095, 459)
        Me.LayoutControl2.TabIndex = 1
        Me.LayoutControl2.Text = "LayoutControl2"
        '
        'tbIDLivreurChangerDestination
        '
        Me.tbIDLivreurChangerDestination.Location = New System.Drawing.Point(494, 391)
        Me.tbIDLivreurChangerDestination.Name = "tbIDLivreurChangerDestination"
        Me.tbIDLivreurChangerDestination.Properties.AcceptEditorTextAsNewValue = DevExpress.Utils.DefaultBoolean.[False]
        Me.tbIDLivreurChangerDestination.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold)
        Me.tbIDLivreurChangerDestination.Properties.Appearance.Options.UseFont = True
        Me.tbIDLivreurChangerDestination.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.tbIDLivreurChangerDestination.Properties.PopupFilterMode = DevExpress.XtraEditors.PopupFilterMode.Contains
        Me.tbIDLivreurChangerDestination.Size = New System.Drawing.Size(415, 22)
        Me.tbIDLivreurChangerDestination.StyleController = Me.LayoutControl2
        Me.tbIDLivreurChangerDestination.TabIndex = 49
        '
        'Button1
        '
        Me.Button1.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Button1.Location = New System.Drawing.Point(913, 391)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(158, 22)
        Me.Button1.TabIndex = 48
        Me.Button1.Text = "Changer le ID dans les tables"
        '
        'tbIDLivreurDepart
        '
        Me.tbIDLivreurDepart.Location = New System.Drawing.Point(91, 391)
        Me.tbIDLivreurDepart.Name = "tbIDLivreurDepart"
        Me.tbIDLivreurDepart.Size = New System.Drawing.Size(332, 20)
        Me.tbIDLivreurDepart.StyleController = Me.LayoutControl2
        Me.tbIDLivreurDepart.TabIndex = 46
        '
        'ProgressBarControl1
        '
        Me.ProgressBarControl1.Location = New System.Drawing.Point(12, 429)
        Me.ProgressBarControl1.Name = "ProgressBarControl1"
        Me.ProgressBarControl1.Size = New System.Drawing.Size(1071, 18)
        Me.ProgressBarControl1.StyleController = Me.LayoutControl2
        Me.ProgressBarControl1.TabIndex = 39
        '
        'grdLivreurs
        '
        Me.grdLivreurs.Location = New System.Drawing.Point(12, 41)
        Me.grdLivreurs.MainView = Me.GridView1
        Me.grdLivreurs.Name = "grdLivreurs"
        Me.grdLivreurs.Size = New System.Drawing.Size(1071, 314)
        Me.grdLivreurs.TabIndex = 0
        Me.grdLivreurs.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'GridView1
        '
        Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colLivreurID, Me.colLivreur, Me.colTelephone, Me.colIsVendeur, Me.Actif, Me.CalculerTaxes, Me.Email})
        Me.GridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.GridView1.GridControl = Me.grdLivreurs
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsBehavior.Editable = False
        Me.GridView1.OptionsCustomization.AllowGroup = False
        Me.GridView1.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridView1.OptionsSelection.UseIndicatorForSelection = False
        Me.GridView1.OptionsView.ShowAutoFilterRow = True
        Me.GridView1.OptionsView.ShowGroupPanel = False
        Me.GridView1.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.colLivreur, DevExpress.Data.ColumnSortOrder.Ascending)})
        '
        'colLivreurID
        '
        Me.colLivreurID.Caption = "Livreur ID"
        Me.colLivreurID.FieldName = "LivreurID"
        Me.colLivreurID.Name = "colLivreurID"
        Me.colLivreurID.Visible = True
        Me.colLivreurID.VisibleIndex = 0
        Me.colLivreurID.Width = 94
        '
        'colLivreur
        '
        Me.colLivreur.Caption = "Livreur"
        Me.colLivreur.FieldName = "Livreur"
        Me.colLivreur.Name = "colLivreur"
        Me.colLivreur.Visible = True
        Me.colLivreur.VisibleIndex = 1
        Me.colLivreur.Width = 94
        '
        'colTelephone
        '
        Me.colTelephone.Caption = "Telephone"
        Me.colTelephone.FieldName = "Telephone"
        Me.colTelephone.Name = "colTelephone"
        Me.colTelephone.Visible = True
        Me.colTelephone.VisibleIndex = 2
        Me.colTelephone.Width = 94
        '
        'colIsVendeur
        '
        Me.colIsVendeur.Caption = "Vendeur?"
        Me.colIsVendeur.FieldName = "IsVendeur"
        Me.colIsVendeur.Name = "colIsVendeur"
        Me.colIsVendeur.Visible = True
        Me.colIsVendeur.VisibleIndex = 3
        '
        'Actif
        '
        Me.Actif.Caption = "Actif"
        Me.Actif.FieldName = "Actif"
        Me.Actif.Name = "Actif"
        Me.Actif.Visible = True
        Me.Actif.VisibleIndex = 4
        '
        'CalculerTaxes
        '
        Me.CalculerTaxes.Caption = "Calculer Taxes"
        Me.CalculerTaxes.FieldName = "CalculerTaxes"
        Me.CalculerTaxes.Name = "CalculerTaxes"
        Me.CalculerTaxes.Visible = True
        Me.CalculerTaxes.VisibleIndex = 5
        '
        'Email
        '
        Me.Email.Caption = "Email"
        Me.Email.FieldName = "Email"
        Me.Email.Name = "Email"
        Me.Email.Visible = True
        Me.Email.VisibleIndex = 6
        '
        'optFiltreGrille
        '
        Me.optFiltreGrille.Location = New System.Drawing.Point(79, 12)
        Me.optFiltreGrille.Name = "optFiltreGrille"
        Me.optFiltreGrille.Properties.Columns = 4
        Me.optFiltreGrille.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "Inactif"), New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "Actif")})
        Me.optFiltreGrille.Size = New System.Drawing.Size(1004, 25)
        Me.optFiltreGrille.StyleController = Me.LayoutControl2
        Me.optFiltreGrille.TabIndex = 45
        '
        'Root
        '
        Me.Root.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.Root.GroupBordersVisible = False
        Me.Root.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem7, Me.LayoutControlItem29, Me.LayoutControlItem12, Me.LayoutControlGroup2})
        Me.Root.Name = "Root"
        Me.Root.Size = New System.Drawing.Size(1095, 459)
        Me.Root.TextVisible = False
        '
        'LayoutControlItem7
        '
        Me.LayoutControlItem7.Control = Me.grdLivreurs
        Me.LayoutControlItem7.Location = New System.Drawing.Point(0, 29)
        Me.LayoutControlItem7.Name = "LayoutControlItem7"
        Me.LayoutControlItem7.Size = New System.Drawing.Size(1075, 318)
        Me.LayoutControlItem7.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem7.TextVisible = False
        '
        'LayoutControlItem29
        '
        Me.LayoutControlItem29.Control = Me.optFiltreGrille
        Me.LayoutControlItem29.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem29.CustomizationFormText = "Filtre"
        Me.LayoutControlItem29.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem29.Name = "LayoutControlItem29"
        Me.LayoutControlItem29.Size = New System.Drawing.Size(1075, 29)
        Me.LayoutControlItem29.Text = "Filtre"
        Me.LayoutControlItem29.TextSize = New System.Drawing.Size(63, 13)
        '
        'LayoutControlItem12
        '
        Me.LayoutControlItem12.Control = Me.ProgressBarControl1
        Me.LayoutControlItem12.Location = New System.Drawing.Point(0, 417)
        Me.LayoutControlItem12.Name = "LayoutControlItem12"
        Me.LayoutControlItem12.Size = New System.Drawing.Size(1075, 22)
        Me.LayoutControlItem12.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem12.TextVisible = False
        '
        'LayoutControlGroup2
        '
        Me.LayoutControlGroup2.CustomizationFormText = "Remplacer un Livreur ID par un autre dans les tables (Attention, cette fonction e" &
    "st dangeureuse)"
        Me.LayoutControlGroup2.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem16, Me.LayoutControlItem18, Me.LayoutControlItem19})
        Me.LayoutControlGroup2.Location = New System.Drawing.Point(0, 347)
        Me.LayoutControlGroup2.Name = "LayoutControlGroup2"
        Me.LayoutControlGroup2.Size = New System.Drawing.Size(1075, 70)
        Me.LayoutControlGroup2.Text = "Remplacer un Livreur ID par un autre dans les tables (Attention, cette fonction e" &
    "st dangeureuse)"
        '
        'LayoutControlItem16
        '
        Me.LayoutControlItem16.Control = Me.tbIDLivreurDepart
        Me.LayoutControlItem16.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem16.Name = "LayoutControlItem16"
        Me.LayoutControlItem16.Size = New System.Drawing.Size(403, 26)
        Me.LayoutControlItem16.Text = "Ce Livreur ID"
        Me.LayoutControlItem16.TextSize = New System.Drawing.Size(63, 13)
        '
        'LayoutControlItem18
        '
        Me.LayoutControlItem18.Control = Me.Button1
        Me.LayoutControlItem18.Location = New System.Drawing.Point(889, 0)
        Me.LayoutControlItem18.Name = "LayoutControlItem18"
        Me.LayoutControlItem18.Size = New System.Drawing.Size(162, 26)
        Me.LayoutControlItem18.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem18.TextVisible = False
        '
        'LayoutControlItem19
        '
        Me.LayoutControlItem19.Control = Me.tbIDLivreurChangerDestination
        Me.LayoutControlItem19.Location = New System.Drawing.Point(403, 0)
        Me.LayoutControlItem19.Name = "LayoutControlItem19"
        Me.LayoutControlItem19.Size = New System.Drawing.Size(486, 26)
        Me.LayoutControlItem19.Text = "Va devenir"
        Me.LayoutControlItem19.TextSize = New System.Drawing.Size(63, 13)
        '
        'XtraTabPage2
        '
        Me.XtraTabPage2.Controls.Add(Me.LayoutControl1)
        Me.XtraTabPage2.Name = "XtraTabPage2"
        Me.XtraTabPage2.PageVisible = False
        Me.XtraTabPage2.Size = New System.Drawing.Size(1095, 503)
        Me.XtraTabPage2.Text = "Détail sur un Livreur / vendeur"
        '
        'LayoutControl1
        '
        Me.LayoutControl1.Controls.Add(Me.ckActif)
        Me.LayoutControl1.Controls.Add(Me.ckVendeur)
        Me.LayoutControl1.Controls.Add(Me.ckCalculerTaxes)
        Me.LayoutControl1.Controls.Add(Me.tbTelephone)
        Me.LayoutControl1.Controls.Add(Me.tbAdresse)
        Me.LayoutControl1.Controls.Add(Me.TLPBut)
        Me.LayoutControl1.Controls.Add(Me.tbLivreur)
        Me.LayoutControl1.Controls.Add(Me.nudLivreurID)
        Me.LayoutControl1.Controls.Add(Me.tbEmail)
        Me.LayoutControl1.Controls.Add(Me.tbLivreurCache)
        Me.LayoutControl1.Controls.Add(Me.tbCommissionVendeur)
        Me.LayoutControl1.Controls.Add(Me.cbVendeur)
        Me.LayoutControl1.Controls.Add(Me.bAnnulerVendeur)
        Me.LayoutControl1.Controls.Add(Me.tbNomCompagnie)
        Me.LayoutControl1.Controls.Add(Me.tbNoTPS)
        Me.LayoutControl1.Controls.Add(Me.tbNoTVQ)
        Me.LayoutControl1.Controls.Add(Me.tbEmail1)
        Me.LayoutControl1.Controls.Add(Me.tbEmail2)
        Me.LayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LayoutControl1.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControl1.Name = "LayoutControl1"
        Me.LayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = New System.Drawing.Rectangle(976, 223, 771, 350)
        Me.LayoutControl1.OptionsCustomizationForm.ShowPropertyGrid = True
        Me.LayoutControl1.Root = Me.LayoutControlGroup1
        Me.LayoutControl1.Size = New System.Drawing.Size(1095, 503)
        Me.LayoutControl1.TabIndex = 0
        Me.LayoutControl1.Text = "LayoutControl1"
        '
        'ckActif
        '
        Me.ckActif.Location = New System.Drawing.Point(12, 35)
        Me.ckActif.Name = "ckActif"
        Me.ckActif.Properties.Caption = "Actif"
        Me.ckActif.Size = New System.Drawing.Size(474, 19)
        Me.ckActif.StyleController = Me.LayoutControl1
        Me.ckActif.TabIndex = 34
        '
        'ckVendeur
        '
        Me.ckVendeur.Location = New System.Drawing.Point(12, 12)
        Me.ckVendeur.Name = "ckVendeur"
        Me.ckVendeur.Properties.Caption = "Vendeur?"
        Me.ckVendeur.Size = New System.Drawing.Size(474, 19)
        Me.ckVendeur.StyleController = Me.LayoutControl1
        Me.ckVendeur.TabIndex = 33
        '
        'ckCalculerTaxes
        '
        Me.ckCalculerTaxes.Location = New System.Drawing.Point(12, 226)
        Me.ckCalculerTaxes.Name = "ckCalculerTaxes"
        Me.ckCalculerTaxes.Properties.Caption = "Calculer Taxes"
        Me.ckCalculerTaxes.Size = New System.Drawing.Size(474, 19)
        Me.ckCalculerTaxes.StyleController = Me.LayoutControl1
        Me.ckCalculerTaxes.TabIndex = 32
        '
        'tbTelephone
        '
        Me.tbTelephone.Location = New System.Drawing.Point(98, 130)
        Me.tbTelephone.Name = "tbTelephone"
        Me.tbTelephone.Properties.Mask.EditMask = "(\d?\d?\d?) \d\d\d-\d\d\d\d"
        Me.tbTelephone.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Regular
        Me.tbTelephone.Properties.Mask.SaveLiteral = False
        Me.tbTelephone.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.tbTelephone.Size = New System.Drawing.Size(388, 20)
        Me.tbTelephone.StyleController = Me.LayoutControl1
        Me.tbTelephone.TabIndex = 9
        '
        'tbAdresse
        '
        Me.tbAdresse.Location = New System.Drawing.Point(588, 44)
        Me.tbAdresse.Name = "tbAdresse"
        Me.tbAdresse.Size = New System.Drawing.Size(483, 355)
        Me.tbAdresse.StyleController = Me.LayoutControl1
        Me.tbAdresse.TabIndex = 31
        '
        'TLPBut
        '
        Me.TLPBut.ColumnCount = 7
        Me.TLPBut.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TLPBut.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100.0!))
        Me.TLPBut.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 8.0!))
        Me.TLPBut.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 124.0!))
        Me.TLPBut.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 8.0!))
        Me.TLPBut.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 95.0!))
        Me.TLPBut.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TLPBut.Controls.Add(Me.butApply, 1, 1)
        Me.TLPBut.Controls.Add(Me.butClose, 5, 1)
        Me.TLPBut.Controls.Add(Me.butCancel, 3, 1)
        Me.TLPBut.Location = New System.Drawing.Point(12, 415)
        Me.TLPBut.Name = "TLPBut"
        Me.TLPBut.RowCount = 2
        Me.TLPBut.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26.0!))
        Me.TLPBut.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 34.0!))
        Me.TLPBut.Size = New System.Drawing.Size(1071, 76)
        Me.TLPBut.TabIndex = 8
        '
        'butApply
        '
        Me.butApply.Dock = System.Windows.Forms.DockStyle.Top
        Me.butApply.Location = New System.Drawing.Point(371, 29)
        Me.butApply.Name = "butApply"
        Me.butApply.Size = New System.Drawing.Size(94, 28)
        Me.butApply.TabIndex = 20
        Me.butApply.Text = "Appliquer"
        '
        'butClose
        '
        Me.butClose.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.butClose.Dock = System.Windows.Forms.DockStyle.Top
        Me.butClose.Location = New System.Drawing.Point(611, 29)
        Me.butClose.Name = "butClose"
        Me.butClose.Size = New System.Drawing.Size(89, 28)
        Me.butClose.TabIndex = 22
        Me.butClose.Text = "Fermer"
        '
        'butCancel
        '
        Me.butCancel.Dock = System.Windows.Forms.DockStyle.Top
        Me.butCancel.Location = New System.Drawing.Point(479, 29)
        Me.butCancel.Name = "butCancel"
        Me.butCancel.Size = New System.Drawing.Size(118, 28)
        Me.butCancel.TabIndex = 21
        Me.butCancel.Text = "Retour a la grille"
        '
        'tbLivreur
        '
        Me.tbLivreur.Location = New System.Drawing.Point(98, 82)
        Me.tbLivreur.Name = "tbLivreur"
        Me.tbLivreur.Size = New System.Drawing.Size(388, 20)
        Me.tbLivreur.StyleController = Me.LayoutControl1
        Me.tbLivreur.TabIndex = 7
        '
        'nudLivreurID
        '
        Me.nudLivreurID.Enabled = False
        Me.nudLivreurID.Location = New System.Drawing.Point(98, 58)
        Me.nudLivreurID.Name = "nudLivreurID"
        Me.nudLivreurID.Properties.Appearance.BackColor = System.Drawing.Color.Azure
        Me.nudLivreurID.Properties.Appearance.Options.UseBackColor = True
        Me.nudLivreurID.Size = New System.Drawing.Size(388, 20)
        Me.nudLivreurID.StyleController = Me.LayoutControl1
        Me.nudLivreurID.TabIndex = 5
        '
        'tbEmail
        '
        Me.tbEmail.Location = New System.Drawing.Point(98, 154)
        Me.tbEmail.Name = "tbEmail"
        Me.tbEmail.Size = New System.Drawing.Size(388, 20)
        Me.tbEmail.StyleController = Me.LayoutControl1
        Me.tbEmail.TabIndex = 7
        '
        'tbLivreurCache
        '
        Me.tbLivreurCache.Enabled = False
        Me.tbLivreurCache.Location = New System.Drawing.Point(98, 106)
        Me.tbLivreurCache.Name = "tbLivreurCache"
        Me.tbLivreurCache.Properties.Appearance.BackColor = System.Drawing.Color.RosyBrown
        Me.tbLivreurCache.Properties.Appearance.Options.UseBackColor = True
        Me.tbLivreurCache.Size = New System.Drawing.Size(388, 20)
        Me.tbLivreurCache.StyleController = Me.LayoutControl1
        Me.tbLivreurCache.TabIndex = 7
        '
        'tbCommissionVendeur
        '
        Me.tbCommissionVendeur.Location = New System.Drawing.Point(110, 379)
        Me.tbCommissionVendeur.Name = "tbCommissionVendeur"
        Me.tbCommissionVendeur.Properties.Mask.EditMask = "p"
        Me.tbCommissionVendeur.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.tbCommissionVendeur.Size = New System.Drawing.Size(364, 20)
        Me.tbCommissionVendeur.StyleController = Me.LayoutControl1
        Me.tbCommissionVendeur.TabIndex = 139
        '
        'cbVendeur
        '
        Me.cbVendeur.Location = New System.Drawing.Point(110, 353)
        Me.cbVendeur.Name = "cbVendeur"
        Me.cbVendeur.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbVendeur.Size = New System.Drawing.Size(50, 20)
        Me.cbVendeur.StyleController = Me.LayoutControl1
        Me.cbVendeur.TabIndex = 145
        '
        'bAnnulerVendeur
        '
        Me.bAnnulerVendeur.Location = New System.Drawing.Point(164, 353)
        Me.bAnnulerVendeur.Name = "bAnnulerVendeur"
        Me.bAnnulerVendeur.Size = New System.Drawing.Size(310, 22)
        Me.bAnnulerVendeur.StyleController = Me.LayoutControl1
        Me.bAnnulerVendeur.TabIndex = 137
        Me.bAnnulerVendeur.Text = "..."
        '
        'tbNomCompagnie
        '
        Me.tbNomCompagnie.Location = New System.Drawing.Point(98, 249)
        Me.tbNomCompagnie.Name = "tbNomCompagnie"
        Me.tbNomCompagnie.Size = New System.Drawing.Size(388, 20)
        Me.tbNomCompagnie.StyleController = Me.LayoutControl1
        Me.tbNomCompagnie.TabIndex = 7
        '
        'tbNoTPS
        '
        Me.tbNoTPS.Location = New System.Drawing.Point(98, 273)
        Me.tbNoTPS.Name = "tbNoTPS"
        Me.tbNoTPS.Size = New System.Drawing.Size(388, 20)
        Me.tbNoTPS.StyleController = Me.LayoutControl1
        Me.tbNoTPS.TabIndex = 7
        '
        'tbNoTVQ
        '
        Me.tbNoTVQ.Location = New System.Drawing.Point(98, 297)
        Me.tbNoTVQ.Name = "tbNoTVQ"
        Me.tbNoTVQ.Size = New System.Drawing.Size(388, 20)
        Me.tbNoTVQ.StyleController = Me.LayoutControl1
        Me.tbNoTVQ.TabIndex = 7
        '
        'LayoutControlGroup1
        '
        Me.LayoutControlGroup1.CustomizationFormText = "LayoutControlGroup1"
        Me.LayoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup1.GroupBordersVisible = False
        Me.LayoutControlGroup1.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem4, Me.LayoutControlItem20, Me.LayoutControlItem5, Me.LayoutControlItem8, Me.LayoutControlItem1, Me.LayoutControlItem2, Me.LayoutControlItem3, Me.LayoutControlGroup3, Me.LayoutControlItem6, Me.LayoutControlItem9, Me.LayoutControlGroup15, Me.LayoutControlItem13, Me.LayoutControlItem14, Me.LayoutControlItem15, Me.LayoutControlItem17, Me.LayoutControlItem21})
        Me.LayoutControlGroup1.Name = "Root"
        Me.LayoutControlGroup1.Size = New System.Drawing.Size(1095, 503)
        Me.LayoutControlGroup1.TextVisible = False
        '
        'LayoutControlItem4
        '
        Me.LayoutControlItem4.Control = Me.tbLivreur
        Me.LayoutControlItem4.CustomizationFormText = "Contact #1"
        Me.LayoutControlItem4.Location = New System.Drawing.Point(0, 70)
        Me.LayoutControlItem4.Name = "LayoutControlItem4"
        Me.LayoutControlItem4.Size = New System.Drawing.Size(478, 24)
        Me.LayoutControlItem4.Text = "Livreur"
        Me.LayoutControlItem4.TextSize = New System.Drawing.Size(82, 13)
        '
        'LayoutControlItem20
        '
        Me.LayoutControlItem20.Control = Me.TLPBut
        Me.LayoutControlItem20.CustomizationFormText = "LayoutControlItem20"
        Me.LayoutControlItem20.Location = New System.Drawing.Point(0, 403)
        Me.LayoutControlItem20.Name = "LayoutControlItem20"
        Me.LayoutControlItem20.Size = New System.Drawing.Size(1075, 80)
        Me.LayoutControlItem20.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem20.TextVisible = False
        '
        'LayoutControlItem5
        '
        Me.LayoutControlItem5.Control = Me.tbTelephone
        Me.LayoutControlItem5.CustomizationFormText = "LayoutControlItem5"
        Me.LayoutControlItem5.Location = New System.Drawing.Point(0, 118)
        Me.LayoutControlItem5.Name = "LayoutControlItem5"
        Me.LayoutControlItem5.Size = New System.Drawing.Size(478, 24)
        Me.LayoutControlItem5.Text = "Téléphone"
        Me.LayoutControlItem5.TextSize = New System.Drawing.Size(82, 13)
        '
        'LayoutControlItem8
        '
        Me.LayoutControlItem8.Control = Me.nudLivreurID
        Me.LayoutControlItem8.Location = New System.Drawing.Point(0, 46)
        Me.LayoutControlItem8.Name = "LayoutControlItem8"
        Me.LayoutControlItem8.Size = New System.Drawing.Size(478, 24)
        Me.LayoutControlItem8.Text = "Livreur ID"
        Me.LayoutControlItem8.TextSize = New System.Drawing.Size(82, 13)
        '
        'LayoutControlItem1
        '
        Me.LayoutControlItem1.Control = Me.ckCalculerTaxes
        Me.LayoutControlItem1.Location = New System.Drawing.Point(0, 214)
        Me.LayoutControlItem1.Name = "LayoutControlItem1"
        Me.LayoutControlItem1.Size = New System.Drawing.Size(478, 23)
        Me.LayoutControlItem1.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem1.TextVisible = False
        '
        'LayoutControlItem2
        '
        Me.LayoutControlItem2.Control = Me.tbEmail
        Me.LayoutControlItem2.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem2.CustomizationFormText = "Contact #1"
        Me.LayoutControlItem2.Location = New System.Drawing.Point(0, 142)
        Me.LayoutControlItem2.Name = "LayoutControlItem2"
        Me.LayoutControlItem2.Size = New System.Drawing.Size(478, 24)
        Me.LayoutControlItem2.Text = "Email"
        Me.LayoutControlItem2.TextSize = New System.Drawing.Size(82, 13)
        '
        'LayoutControlItem3
        '
        Me.LayoutControlItem3.Control = Me.tbLivreurCache
        Me.LayoutControlItem3.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem3.CustomizationFormText = "Contact #1"
        Me.LayoutControlItem3.Location = New System.Drawing.Point(0, 94)
        Me.LayoutControlItem3.Name = "LayoutControlItem3"
        Me.LayoutControlItem3.Size = New System.Drawing.Size(478, 24)
        Me.LayoutControlItem3.StartNewLine = True
        Me.LayoutControlItem3.Text = "Livreur caché"
        Me.LayoutControlItem3.TextSize = New System.Drawing.Size(82, 13)
        '
        'LayoutControlGroup3
        '
        Me.LayoutControlGroup3.CustomizationFormText = "Adresse"
        Me.LayoutControlGroup3.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem10})
        Me.LayoutControlGroup3.Location = New System.Drawing.Point(478, 0)
        Me.LayoutControlGroup3.Name = "LayoutControlGroup3"
        Me.LayoutControlGroup3.Size = New System.Drawing.Size(597, 403)
        Me.LayoutControlGroup3.Text = "Adresse"
        '
        'LayoutControlItem10
        '
        Me.LayoutControlItem10.Control = Me.tbAdresse
        Me.LayoutControlItem10.CustomizationFormText = "Adresse"
        Me.LayoutControlItem10.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem10.Name = "LayoutControlItem10"
        Me.LayoutControlItem10.Size = New System.Drawing.Size(573, 359)
        Me.LayoutControlItem10.Text = "Adresse"
        Me.LayoutControlItem10.TextSize = New System.Drawing.Size(82, 13)
        '
        'LayoutControlItem6
        '
        Me.LayoutControlItem6.Control = Me.ckVendeur
        Me.LayoutControlItem6.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem6.Name = "LayoutControlItem6"
        Me.LayoutControlItem6.Size = New System.Drawing.Size(478, 23)
        Me.LayoutControlItem6.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem6.TextVisible = False
        '
        'LayoutControlItem9
        '
        Me.LayoutControlItem9.Control = Me.ckActif
        Me.LayoutControlItem9.Location = New System.Drawing.Point(0, 23)
        Me.LayoutControlItem9.Name = "LayoutControlItem9"
        Me.LayoutControlItem9.Size = New System.Drawing.Size(478, 23)
        Me.LayoutControlItem9.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem9.TextVisible = False
        '
        'LayoutControlGroup15
        '
        Me.LayoutControlGroup15.CustomizationFormText = "Vendeurs"
        Me.LayoutControlGroup15.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem79, Me.LayoutControlItem11, Me.LayoutControlItem73})
        Me.LayoutControlGroup15.Location = New System.Drawing.Point(0, 309)
        Me.LayoutControlGroup15.Name = "LayoutControlGroup15"
        Me.LayoutControlGroup15.Size = New System.Drawing.Size(478, 94)
        Me.LayoutControlGroup15.Text = "Vendeurs"
        '
        'LayoutControlItem79
        '
        Me.LayoutControlItem79.Control = Me.tbCommissionVendeur
        Me.LayoutControlItem79.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem79.CustomizationFormText = "% de commission"
        Me.LayoutControlItem79.Location = New System.Drawing.Point(0, 26)
        Me.LayoutControlItem79.Name = "LayoutControlItem79"
        Me.LayoutControlItem79.Size = New System.Drawing.Size(454, 24)
        Me.LayoutControlItem79.Text = "% de commission"
        Me.LayoutControlItem79.TextSize = New System.Drawing.Size(82, 13)
        '
        'LayoutControlItem11
        '
        Me.LayoutControlItem11.Control = Me.cbVendeur
        Me.LayoutControlItem11.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem11.CustomizationFormText = "Vendeurs"
        Me.LayoutControlItem11.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem11.Name = "LayoutControlItem11"
        Me.LayoutControlItem11.Size = New System.Drawing.Size(140, 26)
        Me.LayoutControlItem11.Text = "Vendeurs"
        Me.LayoutControlItem11.TextSize = New System.Drawing.Size(82, 13)
        '
        'LayoutControlItem73
        '
        Me.LayoutControlItem73.Control = Me.bAnnulerVendeur
        Me.LayoutControlItem73.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem73.CustomizationFormText = "Vendeur"
        Me.LayoutControlItem73.Location = New System.Drawing.Point(140, 0)
        Me.LayoutControlItem73.Name = "LayoutControlItem73"
        Me.LayoutControlItem73.Size = New System.Drawing.Size(314, 26)
        Me.LayoutControlItem73.Text = "Vendeur"
        Me.LayoutControlItem73.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem73.TextVisible = False
        '
        'LayoutControlItem13
        '
        Me.LayoutControlItem13.Control = Me.tbNomCompagnie
        Me.LayoutControlItem13.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem13.CustomizationFormText = "Contact #1"
        Me.LayoutControlItem13.Location = New System.Drawing.Point(0, 237)
        Me.LayoutControlItem13.Name = "LayoutControlItem13"
        Me.LayoutControlItem13.Size = New System.Drawing.Size(478, 24)
        Me.LayoutControlItem13.Text = "Nom Compagnie"
        Me.LayoutControlItem13.TextSize = New System.Drawing.Size(82, 13)
        '
        'LayoutControlItem14
        '
        Me.LayoutControlItem14.Control = Me.tbNoTPS
        Me.LayoutControlItem14.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem14.CustomizationFormText = "Contact #1"
        Me.LayoutControlItem14.Location = New System.Drawing.Point(0, 261)
        Me.LayoutControlItem14.Name = "LayoutControlItem14"
        Me.LayoutControlItem14.Size = New System.Drawing.Size(478, 24)
        Me.LayoutControlItem14.Text = "No TPS"
        Me.LayoutControlItem14.TextSize = New System.Drawing.Size(82, 13)
        '
        'LayoutControlItem15
        '
        Me.LayoutControlItem15.Control = Me.tbNoTVQ
        Me.LayoutControlItem15.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem15.CustomizationFormText = "Contact #1"
        Me.LayoutControlItem15.Location = New System.Drawing.Point(0, 285)
        Me.LayoutControlItem15.Name = "LayoutControlItem15"
        Me.LayoutControlItem15.Size = New System.Drawing.Size(478, 24)
        Me.LayoutControlItem15.Text = "No TVQ"
        Me.LayoutControlItem15.TextSize = New System.Drawing.Size(82, 13)
        '
        'tbEmail1
        '
        Me.tbEmail1.Location = New System.Drawing.Point(98, 178)
        Me.tbEmail1.Name = "tbEmail1"
        Me.tbEmail1.Size = New System.Drawing.Size(388, 20)
        Me.tbEmail1.StyleController = Me.LayoutControl1
        Me.tbEmail1.TabIndex = 7
        '
        'LayoutControlItem17
        '
        Me.LayoutControlItem17.Control = Me.tbEmail1
        Me.LayoutControlItem17.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem17.CustomizationFormText = "Contact #1"
        Me.LayoutControlItem17.Location = New System.Drawing.Point(0, 166)
        Me.LayoutControlItem17.Name = "LayoutControlItem17"
        Me.LayoutControlItem17.Size = New System.Drawing.Size(478, 24)
        Me.LayoutControlItem17.Text = "Email 2"
        Me.LayoutControlItem17.TextSize = New System.Drawing.Size(82, 13)
        '
        'tbEmail2
        '
        Me.tbEmail2.Location = New System.Drawing.Point(98, 202)
        Me.tbEmail2.Name = "tbEmail2"
        Me.tbEmail2.Size = New System.Drawing.Size(388, 20)
        Me.tbEmail2.StyleController = Me.LayoutControl1
        Me.tbEmail2.TabIndex = 7
        '
        'LayoutControlItem21
        '
        Me.LayoutControlItem21.Control = Me.tbEmail2
        Me.LayoutControlItem21.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem21.CustomizationFormText = "Contact #1"
        Me.LayoutControlItem21.Location = New System.Drawing.Point(0, 190)
        Me.LayoutControlItem21.Name = "LayoutControlItem21"
        Me.LayoutControlItem21.Size = New System.Drawing.Size(478, 24)
        Me.LayoutControlItem21.Text = "Email 3"
        Me.LayoutControlItem21.TextSize = New System.Drawing.Size(82, 13)
        '
        'frmLivreurs
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1100, 568)
        Me.Controls.Add(Me.tabData)
        Me.Controls.Add(Me.TS)
        Me.IconOptions.SvgImage = CType(resources.GetObject("frmLivreurs.IconOptions.SvgImage"), DevExpress.Utils.Svg.SvgImage)
        Me.Name = "frmLivreurs"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Livreurs  / Vendeurs"
        Me.TS.ResumeLayout(False)
        Me.TS.PerformLayout()
        CType(Me.tabData, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabData.ResumeLayout(False)
        Me.XtraTabPage1.ResumeLayout(False)
        CType(Me.LayoutControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LayoutControl2.ResumeLayout(False)
        CType(Me.tbIDLivreurChangerDestination.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbIDLivreurDepart.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ProgressBarControl1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grdLivreurs, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.optFiltreGrille.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Root, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem29, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem16, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem18, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem19, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabPage2.ResumeLayout(False)
        CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LayoutControl1.ResumeLayout(False)
        CType(Me.ckActif.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ckVendeur.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ckCalculerTaxes.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbTelephone.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbAdresse.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TLPBut.ResumeLayout(False)
        CType(Me.tbLivreur.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudLivreurID.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbEmail.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbLivreurCache.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbCommissionVendeur.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbVendeur.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbNomCompagnie.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbNoTPS.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbNoTVQ.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem20, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup15, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem79, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem73, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem13, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem14, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem15, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbEmail1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem17, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbEmail2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem21, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents TS As System.Windows.Forms.ToolStrip
    Friend WithEvents Head As System.Windows.Forms.ToolStripLabel
    Friend WithEvents bAdd As System.Windows.Forms.ToolStripButton
    Friend WithEvents sp1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents bEdit As System.Windows.Forms.ToolStripButton
    Friend WithEvents sp2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents bCancel As System.Windows.Forms.ToolStripButton
    Friend WithEvents sp4 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents bRefresh As System.Windows.Forms.ToolStripButton
    Friend WithEvents sp5 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripButton6 As System.Windows.Forms.ToolStripButton
    Friend WithEvents tabData As DevExpress.XtraTab.XtraTabControl
    Friend WithEvents XtraTabPage1 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents grdLivreurs As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colLivreurID As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colLivreur As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colTelephone As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents XtraTabPage2 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents LayoutControl1 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents tbTelephone As DevExpress.XtraEditors.TextEdit
    Friend WithEvents tbAdresse As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents TLPBut As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents butApply As System.Windows.Forms.Button
    Friend WithEvents butClose As System.Windows.Forms.Button
    Friend WithEvents butCancel As System.Windows.Forms.Button
    Friend WithEvents tbLivreur As DevExpress.XtraEditors.TextEdit
    Friend WithEvents nudLivreurID As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlGroup1 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem4 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem20 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlGroup3 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem10 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem5 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem8 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents ckCalculerTaxes As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LayoutControlItem1 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents tbEmail As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlItem2 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents tbLivreurCache As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlItem3 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents ckVendeur As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LayoutControlItem6 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents colIsVendeur As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents LayoutControl2 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents Root As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem7 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents optFiltreGrille As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents LayoutControlItem29 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents ckActif As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LayoutControlItem9 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents Actif As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents tbCommissionVendeur As DevExpress.XtraEditors.TextEdit
    Friend WithEvents cbVendeur As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents bAnnulerVendeur As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LayoutControlGroup15 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem79 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem11 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem73 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents ProgressBarControl1 As DevExpress.XtraEditors.ProgressBarControl
    Friend WithEvents LayoutControlItem12 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents tbNomCompagnie As DevExpress.XtraEditors.TextEdit
    Friend WithEvents tbNoTPS As DevExpress.XtraEditors.TextEdit
    Friend WithEvents tbNoTVQ As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlItem13 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem14 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem15 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents CalculerTaxes As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents Button1 As Button
    Friend WithEvents tbIDLivreurDepart As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlGroup2 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem16 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem18 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents tbIDLivreurChangerDestination As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LayoutControlItem19 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents Email As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents tbEmail1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents tbEmail2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlItem17 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem21 As DevExpress.XtraLayout.LayoutControlItem
End Class
