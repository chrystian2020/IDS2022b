﻿Imports System.Globalization
Imports System.Threading

Public Class frmEpargne
    Private oEpargnesData As New EpargnesData
    Private oEpargnesPaiementsData As New EpargnesPaiementsData
    Private oLivreursData As New LivreursData
    Dim oPeriodeData As New PeriodesData

    Private bload As Boolean = False
    Private bAddMode As Boolean = False
    Private bEditMode As Boolean = False
    Private bDeleteMode As Boolean = False
    Public blnGridClick As Boolean = False
    Private IDFournisseur As Integer
    Private iRowGrid As Int32 = 0
    Dim ssql As String

    Private Sub FillAllLivreurs()


        Dim dtLivreur As DataTable
        Dim drLivreur As DataRow
        Dim oLivreurData As New LivreursData

        Dim table As New DataTable
        table.Columns.Add("value", Type.GetType("System.Int32"))
        table.Columns.Add("Livreur", Type.GetType("System.String"))
        dtLivreur = oLivreurData.SelectAll



        For Each drLivreur In dtLivreur.Rows
            Dim teller As Integer = 0

            teller = drLivreur("LivreurID")
            Dim toto As String = drLivreur("Livreur")
            table.Rows.Add(New Object() {teller, toto})
        Next


        cbLivreur.Properties.DataSource = table
        cbLivreur.Properties.NullText = "Selectionner un livreur"
        cbLivreur.Properties.DisplayMember = "Livreur"
        cbLivreur.Properties.ValueMember = "value"
        cbLivreur.Properties.PopupFormMinSize = New Size(50, 200)
        cbLivreur.Properties.PopulateColumns()


    End Sub

    'Get the first day of the month
    Public Function FirstDayOfMonth(ByVal sourceDate As DateTime) As DateTime
        Return New DateTime(sourceDate.Year, sourceDate.Month, 1)
    End Function

    'Get the last day of the month
    Public Function LastDayOfMonth(ByVal sourceDate As DateTime) As DateTime
        Dim lastDay As DateTime = New DateTime(sourceDate.Year, sourceDate.Month, 1)
        Return lastDay.AddMonths(1).AddDays(-1)
    End Function

    Private Sub frmEpargne_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        bload = True
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

        Thread.CurrentThread.CurrentCulture = New CultureInfo("en-US", True)

        FillAllLivreurs()


        LoadGridCode()

        bload = False

        Me.Cursor = System.Windows.Forms.Cursors.Default
    End Sub


    Private Sub LoadGridCode()
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        Try

            grdEpargne.DataSource = oEpargnesData.SelectAll()
            GridView1.ClearSorting()
            GridView1.Columns("IDEpargne").SortOrder = DevExpress.Data.ColumnSortOrder.Ascending

        Catch
        End Try
        Me.Cursor = System.Windows.Forms.Cursors.Default
    End Sub

    Private Sub Delete()
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        Me.AddMode = False
        Me.EditMode = False
        Me.DeleteMode = True
        GetData()

        EnableRecord(False)
        tabData.SelectedTabPage = tabData.TabPages(1)
        tabData.TabPages(1).PageVisible = True
        tabData.TabPages(0).PageVisible = False
        ShowToolStripItems("Delete")
        Me.Cursor = System.Windows.Forms.Cursors.Default
    End Sub




    Private Sub Add()


        Dim oEpargnes As New Epargnes
        Dim oEpargnesData As New EpargnesData
        Dim oEpargnesRetraits As New EpargnesRetraits
        Dim oEpargnesRetraitsData As New EpargnesRetraitsData
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        Me.AddMode = True
        Me.EditMode = False
        Me.DeleteMode = False

        ClearRecord()
        EnableRecord(True)

        chkActif.Checked = True
        bRetirer.Enabled = False
        tbDatePret.Text = DateTime.Now.ToShortDateString
        grdEpargnes.DataSource = oEpargnesPaiementsData.SelectAllByID(0)
        GridView2.ClearSorting()
        GridView2.Columns("IDEpargnePaiment").SortOrder = DevExpress.Data.ColumnSortOrder.Ascending

        grdRetraits.DataSource = oEpargnesRetraitsData.SelectAllByID(0)
        GridView3.ClearSorting()
        GridView3.Columns("IDRetrait").SortOrder = DevExpress.Data.ColumnSortOrder.Ascending

        tabData.SelectedTabPage = tabData.TabPages(1)
        tabData.TabPages(1).PageVisible = True
        tabData.TabPages(0).PageVisible = False
        ShowToolStripItems("Add")


        Me.Cursor = System.Windows.Forms.Cursors.Default
    End Sub
    Private Sub Edit()
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        Me.AddMode = False
        Me.EditMode = True
        Me.DeleteMode = False
        ClearRecord()
        GetData()

        EnableRecord(True)


        Me.Cursor = System.Windows.Forms.Cursors.Default
    End Sub

    Private Sub GoBack_To_Grid()
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        Dim gridOK As Boolean = False
        Try
            ShowToolStripItems("Cancel")
            LoadGridCode()

            gridOK = True
        Catch
            'Hide Erreur message.
        Finally
            If gridOK = False Then
                ''''
                ShowToolStripItems("Cancel")
                ''''
            End If
        End Try
        Me.Cursor = System.Windows.Forms.Cursors.Default
    End Sub

    Private Sub ShowToolStripItems(ByVal Item As String)
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        bAdd.Enabled = False
        bEdit.Enabled = False
        bDelete.Enabled = False
        bCancel.Enabled = False
        bRefresh.Enabled = False
        Select Case Item
            Case "Add"
                bCancel.Enabled = True
            Case "Edit"
                bCancel.Enabled = True
            Case "Delete"
                bCancel.Enabled = True
            Case "Cancel"
                bAdd.Enabled = True
                bEdit.Enabled = True
                bDelete.Enabled = True
                bRefresh.Enabled = True
                tabData.SelectedTabPage = tabData.TabPages(0)
                tabData.TabPages(1).PageVisible = False
                tabData.TabPages(0).PageVisible = True
            Case "Refresh"
                bAdd.Enabled = True
                bEdit.Enabled = True
                bDelete.Enabled = True
                bRefresh.Enabled = True
                LoadGridCode()
            Case "No Record"
                bAdd.Enabled = True
                bRefresh.Enabled = True
        End Select

        Me.Cursor = System.Windows.Forms.Cursors.Default
    End Sub

    Public Property AddMode() As Boolean
        Get
            Return bAddMode
        End Get
        Set(ByVal value As Boolean)
            bAddMode = value
        End Set
    End Property

    Public Property EditMode() As Boolean
        Get
            Return bEditMode
        End Get
        Set(ByVal value As Boolean)
            bEditMode = value
        End Set
    End Property

    Public Property DeleteMode() As Boolean
        Get
            Return bDeleteMode
        End Get
        Set(ByVal value As Boolean)
            bDeleteMode = value
        End Set
    End Property


    Private Sub ClearRecord()
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor


        Me.nudIDEpargne.Text = Nothing
        Me.tbDatePret.Text = Nothing
        Me.cbLivreur.EditValue = Nothing
        chkActif.Checked = False
        tbPaiementParPeriode.Text = 0
        tbBalance.Text = 0


        Me.Cursor = System.Windows.Forms.Cursors.Default
    End Sub

    Private Sub EnableRecord(ByVal YesNo As Boolean)
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor


        Me.tbDatePret.Enabled = YesNo
        Me.cbLivreur.Enabled = YesNo
        chkActif.Enabled = YesNo

        Me.Cursor = System.Windows.Forms.Cursors.Default
    End Sub

    Public Property SelectedRowFournisseur() As Int32
        Get
            Return iRowGrid
        End Get
        Set(ByVal value As Int32)
            iRowGrid = value
        End Set
    End Property

    Public Property SelectedRow() As Int32
        Get
            Return iRowGrid
        End Get
        Set(ByVal value As Int32)
            iRowGrid = value
        End Set
    End Property

    Private Sub bAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        tabData.SelectedTabPage = tabData.TabPages(1)
        tabData.TabPages(1).PageVisible = True
        tabData.TabPages(0).PageVisible = False
    End Sub

    Private Sub bEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        tabData.SelectedTabPage = tabData.TabPages(1)
        tabData.TabPages(1).PageVisible = True
        tabData.TabPages(0).PageVisible = False
    End Sub

    Private Sub bCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        tabData.SelectedTabPage = tabData.TabPages(0)
        tabData.TabPages(1).PageVisible = False
        tabData.TabPages(0).PageVisible = True
    End Sub

    Private Sub bDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        tabData.SelectedTabPage = tabData.TabPages(0)
        tabData.TabPages(1).PageVisible = False
        tabData.TabPages(0).PageVisible = True
    End Sub

    Private Sub butCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butCancel.Click
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        ShowToolStripItems("Cancel")
        tabData.SelectedTabPage = tabData.TabPages(0)
        tabData.TabPages(1).PageVisible = False
        LoadGridCode()
        tabData.TabPages(0).PageVisible = True


        Me.Cursor = System.Windows.Forms.Cursors.Default

    End Sub


    Private Sub butClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butClose.Click, ToolStripButton6.Click
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        Me.Close()
        Me.Cursor = System.Windows.Forms.Cursors.Default
    End Sub

    Private Sub GetData()
        Dim oEpargnes As New Epargnes
        Dim oEpargnesData As New EpargnesData
        Dim oEpargnesRetraits As New EpargnesRetraits
        Dim oEpargnesRetraitsData As New EpargnesRetraitsData
        ClearRecord()


        Dim row As System.Data.DataRow = GridView1.GetDataRow(GridView1.FocusedRowHandle)

        Dim clsEpargne As New Epargnes
        clsEpargne.IDEpargne = System.Convert.ToInt32(row("IDEpargne").ToString)
        clsEpargne = oEpargnesData.Select_Record(clsEpargne)





        If Not clsEpargne Is Nothing Then
            Try
                nudIDEpargne.Text = System.Convert.ToInt32(clsEpargne.IDEpargne)
                tbDatePret.Text = If(IsNothing(clsEpargne.DateEpargne), Nothing, clsEpargne.DateEpargne.ToString.Trim)
                cbLivreur.EditValue = If(IsNothing(clsEpargne.IDLivreur), Nothing, clsEpargne.IDLivreur)


                tbPaiementParPeriode.Text = If(IsNothing(clsEpargne.EpargneParPeriode), 0, clsEpargne.EpargneParPeriode)
                tbBalance.Text = If(IsNothing(clsEpargne.Balance), 0, clsEpargne.Balance)
                chkActif.Checked = If(IsNothing(clsEpargne.EpargneActif), 0, clsEpargne.EpargneActif)

                If clsEpargne.EpargneActif = True Then
                    chkActif.Checked = True
                Else
                    chkActif.Checked = False
                End If




                'Fill payment Grid

                Dim dtEpargne As DataTable
                Dim drEpargne As DataRow
                Dim dblTotal As Double = 0

                dtEpargne = oEpargnesPaiementsData.SelectAllByID(clsEpargne.IDEpargne)
                If dtEpargne.Rows.Count > 0 Then
                    For Each drEpargne In dtEpargne.Rows
                        dblTotal = dblTotal + drEpargne("Epargne")

                    Next
                End If


                Dim dtRetrait As DataTable
                Dim drRetrait As DataRow
                dtRetrait = oEpargnesRetraitsData.SelectAllByID(clsEpargne.IDEpargne)
                If dtRetrait.Rows.Count > 0 Then
                    For Each drRetrait In dtRetrait.Rows
                        dblTotal = dblTotal - drRetrait("Retrait")
                    Next
                End If
                tbBalance.EditValue = dblTotal

                grdEpargnes.DataSource = oEpargnesPaiementsData.SelectAllByID(clsEpargne.IDEpargne)


                GridView2.ClearSorting()
                GridView2.Columns("IDEpargnePaiment").SortOrder = DevExpress.Data.ColumnSortOrder.Ascending

                grdRetraits.DataSource = oEpargnesRetraitsData.SelectAllByID(clsEpargne.IDEpargne)
                GridView3.ClearSorting()
                GridView3.Columns("IDRetrait").SortOrder = DevExpress.Data.ColumnSortOrder.Ascending



                bRetirer.Enabled = True
            Catch
            End Try
        End If


    End Sub

    Private Sub SetData(ByVal clsEpargne As Epargnes)
        With clsEpargne

            .IDEpargne = If(String.IsNullOrEmpty(nudIDEpargne.Text), Nothing, nudIDEpargne.Text)
            .DateEpargne = If(String.IsNullOrEmpty(tbDatePret.Text), Nothing, tbDatePret.Text)

            If IsNothing(cbLivreur.EditValue) Then
                .IDLivreur = Nothing
                .Livreur = Nothing
            Else
                .IDLivreur = If(String.IsNullOrEmpty(cbLivreur.EditValue), Nothing, cbLivreur.EditValue)
                .Livreur = If(String.IsNullOrEmpty(cbLivreur.Text), Nothing, cbLivreur.Text)
            End If


            .EpargneParPeriode = If(String.IsNullOrEmpty(tbPaiementParPeriode.EditValue), 0, Convert.ToDouble(tbPaiementParPeriode.EditValue))
            .Balance = If(String.IsNullOrEmpty(tbBalance.EditValue), 0, Convert.ToDouble(tbBalance.EditValue))

            If chkActif.Checked = True Then
                .EpargneActif = True
            Else
                .EpargneActif = False
            End If


        End With
    End Sub
    Private Function VerifyData() As Boolean

        If cbLivreur.Text.ToString.Trim = "" Then
            MessageBox.Show("Vous devez sélectionner un livreur")
            cbLivreur.Focus()
            Return False
        End If

        'If tbMontantPret.EditValue = 0 Then
        '    MessageBox.Show("Vous devez sélectionner un total")
        '    tbMontantPret.Focus()
        '    Return False
        'End If

        Return True
    End Function



    Private Sub UpdateRecord()
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor



        Dim row As System.Data.DataRow = GridView1.GetDataRow(GridView1.FocusedRowHandle)
        Dim clsEpargne As New Epargnes
        clsEpargne.IDEpargne = System.Convert.ToInt32(nudIDEpargne.EditValue)
        clsEpargne = oEpargnesData.Select_Record(clsEpargne)

        If VerifyData() = True Then
            SetData(clsEpargne)
            Dim bSucess As Boolean
            bSucess = oEpargnesData.Update(clsEpargne, clsEpargne)
            If bSucess = True Then
                GoBack_To_Grid()
            Else
                MsgBox("Update failed.", MsgBoxStyle.OkOnly, "Erreur")
            End If
        End If
        Me.Cursor = System.Windows.Forms.Cursors.Default
    End Sub

    Private Sub DeleteRecord()
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

        Dim row As System.Data.DataRow = GridView1.GetDataRow(GridView1.FocusedRowHandle)

        Dim clsEpargne As New Epargnes
        clsEpargne.IDEpargne = System.Convert.ToInt32(nudIDEpargne.EditValue)
        clsEpargne = oEpargnesData.Select_Record(clsEpargne)


        clsEpargne.IDEpargne = System.Convert.ToInt32(nudIDEpargne.EditValue)
        Dim result As DialogResult = MessageBox.Show("Êtes-vous sur? Supprimer cet enregistrement?", "Supprimer", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
        If result = DialogResult.Yes Then
            SetData(clsEpargne)
            Dim bSucess As Boolean
            bSucess = oEpargnesData.Delete(clsEpargne)
            oEpargnesPaiementsData.DeleteByID(nudIDEpargne.EditValue)

            If bSucess = True Then
                GoBack_To_Grid()
            Else
                MsgBox("La supression a echouée.", MsgBoxStyle.OkOnly, "Erreur")
            End If
        End If
        Me.Cursor = System.Windows.Forms.Cursors.Default
    End Sub

    Private Sub InsertRecord()
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        Dim clsEpargne As New Epargnes
        If VerifyData() = True Then
            SetData(clsEpargne)
            Dim bSucess As Boolean
            bSucess = oEpargnesData.Add(clsEpargne)
            If bSucess = True Then
                GoBack_To_Grid()

            Else
                MsgBox("L'insertion a échouée.", MsgBoxStyle.OkOnly, "Erreur")
            End If
        End If
        Me.Cursor = System.Windows.Forms.Cursors.Default
    End Sub
    Private Sub butApply_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butApply.Click
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        If Me.AddMode = True Then
            Me.InsertRecord()
        ElseIf Me.EditMode = True Then
            Me.UpdateRecord()
        ElseIf Me.DeleteMode = True Then
            Me.DeleteRecord()
        End If
        Me.Cursor = System.Windows.Forms.Cursors.Default


    End Sub

    Private Sub TS_ItemClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolStripItemClickedEventArgs) Handles TS.ItemClicked
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

        Select Case e.ClickedItem.Text
            Case "Ajouter"

                tabData.SelectedTabPage = tabData.TabPages(1)
                tabData.TabPages(1).PageVisible = True
                tabData.TabPages(0).PageVisible = False
                Add()


            Case "Edition"
                tabData.SelectedTabPage = tabData.TabPages(1)
                tabData.TabPages(1).PageVisible = True
                tabData.TabPages(0).PageVisible = False
                Edit()

            Case "Supprimer"
                tabData.SelectedTabPage = tabData.TabPages(0)
                tabData.TabPages(0).PageVisible = True
                tabData.TabPages(1).PageVisible = False
                Delete()
            Case "Annuler"
                ShowToolStripItems("Cancel")
            Case "Rafraîchir"
                GoBack_To_Grid()
        End Select
        Me.Cursor = System.Windows.Forms.Cursors.Default
    End Sub

    Private Sub grdEpargne_Click(sender As Object, e As EventArgs) Handles grdEpargne.Click
        If GridView1.FocusedRowHandle < 0 Then Exit Sub

        Dim row As System.Data.DataRow = GridView1.GetDataRow(GridView1.FocusedRowHandle)


        SelectedRow = GridView1.FocusedRowHandle
    End Sub

    Private Sub grdEpargne_DoubleClick(sender As Object, e As EventArgs) Handles grdEpargne.DoubleClick
        Dim row As System.Data.DataRow = GridView1.GetDataRow(GridView1.FocusedRowHandle)

        SelectedRow = GridView1.FocusedRowHandle
        Edit()

        tabData.SelectedTabPage = tabData.TabPages(1)
        tabData.TabPages(1).PageVisible = True
        tabData.TabPages(0).PageVisible = False
    End Sub

    Private Sub bRetirer_Click(sender As Object, e As EventArgs) Handles bRetirer.Click

        Dim dblBalance As Double = tbBalance.EditValue
        Dim dblRetrait As Double = tbRetrait.EditValue


        If (dblBalance >= dblRetrait) And (dblBalance - dblRetrait >= 0) Then



            Dim oEpargnesRetraits As New EpargnesRetraits
            Dim oEpargnesRetraitsData As New EpargnesRetraitsData

            Dim oEpargnes As New Epargnes
            Dim oEpargnesData As New EpargnesData

            oEpargnesRetraits.IDEpargne = nudIDEpargne.Text
            oEpargnesRetraits.IDLivreur = cbLivreur.EditValue
            oEpargnesRetraits.Livreur = cbLivreur.Text.ToString.Trim
            oEpargnesRetraits.Retrait = dblRetrait
            oEpargnesRetraits.DateRetrait = DateTime.Now.ToShortDateString
            oEpargnesRetraits.Periode = go_Globals.IDPeriode
            oEpargnesRetraitsData.Add(oEpargnesRetraits)


            tbBalance.Text = dblBalance - dblRetrait


            ' Update existing balance with new balance after this paiement.
            oEpargnes.IDEpargne = nudIDEpargne.Text
            oEpargnes.Balance = dblBalance - dblRetrait
            oEpargnesData.UpdateBalance(oEpargnes, oEpargnes)

            grdRetraits.DataSource = oEpargnesRetraitsData.SelectAllByID(nudIDEpargne.Text)
            GridView3.ClearSorting()
            GridView3.Columns("IDRetrait").SortOrder = DevExpress.Data.ColumnSortOrder.Ascending


        Else
            MessageBox.Show("Le montant de retrait est invalide ou plus grand que la balance!")

        End If
    End Sub

    Private Sub bSupprimerRetrait_Click(sender As Object, e As EventArgs) Handles bSupprimerRetrait.Click


        Dim result As DialogResult = MessageBox.Show("Etes-vous sur de vouloir supprimer ce retrait?", "supprimer retrait", MessageBoxButtons.YesNo)
        If result = DialogResult.No Then
            Exit Sub

        ElseIf result = DialogResult.Yes Then


            Dim clsRetrait As New EpargnesRetraits
            Dim clsEpargne As New Epargnes
            Dim oEpargnesRetraits As New EpargnesRetraits
            Dim oEpargnesRetraitsData As New EpargnesRetraitsData

            If GridView3.FocusedRowHandle < 0 Then Exit Sub
            Dim row As System.Data.DataRow = GridView3.GetDataRow(GridView3.FocusedRowHandle)

            clsEpargne.IDEpargne = nudIDEpargne.Text
            clsRetrait.IDRetrait = row("IDRetrait")
            clsRetrait.Retrait = row("Retrait")
            oEpargnesRetraitsData.Delete(clsRetrait)

            Dim oEpargnesPaiementsData As New EpargnesPaiementsData
            Dim dtEpargne As DataTable
            Dim drEpargne As DataRow
            Dim dblTotal As Double = 0

            dtEpargne = oEpargnesPaiementsData.SelectAllByID(clsEpargne.IDEpargne)
            If dtEpargne.Rows.Count > 0 Then
                For Each drEpargne In dtEpargne.Rows
                    dblTotal = dblTotal + drEpargne("Epargne")

                Next
            End If

            Dim dtRetrait As DataTable
            Dim drRetrait As DataRow
            dtRetrait = oEpargnesRetraitsData.SelectAllByID(clsEpargne.IDEpargne)
            If dtRetrait.Rows.Count > 0 Then
                For Each drRetrait In dtRetrait.Rows
                    dblTotal = dblTotal - drRetrait("Retrait")
                Next
            End If
            tbBalance.EditValue = dblTotal


            grdRetraits.DataSource = oEpargnesRetraitsData.SelectAllByID(clsEpargne.IDEpargne)
            GridView3.ClearSorting()
            GridView3.Columns("IDRetrait").SortOrder = DevExpress.Data.ColumnSortOrder.Ascending
        End If

    End Sub

    Private Sub bSupprimer_Click(sender As Object, e As EventArgs) Handles bSupprimer.Click

        Dim result As DialogResult = MessageBox.Show("Etes-vous sur de vouloir supprimer cette épargne?", "supprimer épargne", MessageBoxButtons.YesNo)
        If result = DialogResult.No Then
            Exit Sub

        ElseIf result = DialogResult.Yes Then




            Dim oEpargnes As New Epargnes
            Dim oEpargnesData As New EpargnesData
            Dim clsEpargne As New Epargnes
            Dim clsEpargnePaiement As New EpargnesPaiements
            Dim oEpargnePaiement As New EpargnesPaiementsData

            If GridView2.FocusedRowHandle < 0 Then Exit Sub
            Dim row As System.Data.DataRow = GridView2.GetDataRow(GridView2.FocusedRowHandle)
            clsEpargne.IDEpargne = nudIDEpargne.Text
            clsEpargnePaiement.IDEpargnePaiment = row("IDEpargnePaiment")
            oEpargnePaiement.Delete(clsEpargnePaiement)

            Dim oEpargnesPaiementsData As New EpargnesPaiementsData
            Dim dtEpargne As DataTable
            Dim drEpargne As DataRow
            Dim dblTotal As Double = 0

            dtEpargne = oEpargnesPaiementsData.SelectAllByID(clsEpargne.IDEpargne)
            If dtEpargne.Rows.Count > 0 Then
                For Each drEpargne In dtEpargne.Rows
                    dblTotal = dblTotal + drEpargne("Epargne")

                Next
            End If

            Dim oEpargnesRetraitsData As New EpargnesRetraitsData
            Dim dtRetrait As DataTable
            Dim drRetrait As DataRow
            dtRetrait = oEpargnesRetraitsData.SelectAllByID(clsEpargne.IDEpargne)
            If dtRetrait.Rows.Count > 0 Then
                For Each drRetrait In dtRetrait.Rows
                    dblTotal = dblTotal - drRetrait("Retrait")
                Next
            End If
            tbBalance.EditValue = dblTotal


            grdEpargnes.DataSource = oEpargnesPaiementsData.SelectAllByID(clsEpargne.IDEpargne)
            GridView2.ClearSorting()
            GridView2.Columns("IDEpargnePaiment").SortOrder = DevExpress.Data.ColumnSortOrder.Ascending

        End If



    End Sub

    Private Sub grdEpargnes_Click(sender As Object, e As EventArgs) Handles grdEpargnes.Click
        bSupprimer.Enabled = True

    End Sub

    Private Sub grdRetraits_Click(sender As Object, e As EventArgs) Handles grdRetraits.Click

        bSupprimerRetrait.Enabled = True

    End Sub
End Class