﻿Imports System.ComponentModel
Imports System.Data.Odbc
Imports System.Data.SqlClient
Imports System.Globalization
Imports DevExpress.XtraScheduler
Imports DevExpress.XtraScheduler.Localization
Imports System.IO
Imports System.Linq
Imports System.Net
Imports System.Reflection
Imports System.Text
Imports System.Threading
Imports DevExpress
Imports DevExpress.LookAndFeel
Imports DevExpress.Skins
Imports DevExpress.UserSkins
Imports DevExpress.Utils.Menu
Imports DevExpress.XtraBars
Imports DevExpress.XtraBars.Helpers
Imports DevExpress.XtraBars.Ribbon
Imports DevExpress.XtraGrid.Views.Grid.ViewInfo
Imports DevExpress.XtraScheduler.Commands
Imports DevExpress.XtraScheduler.Services
Imports GemBox.Pdf
Imports GemBox.Spreadsheet
Imports DevExpress.XtraGrid.Columns







Public Class frmMain
    Private oEpargneData As New EpargnesData
    Private oEpargnesPaiementsData As New EpargnesPaiementsData

    Private oPretsData As New PretsData
    Private oPretsPaiementsData As New PretsPaiementsData
    Dim oRapportCommissionData As New RapportCommissionData
    Private oFournisseursData As New FournisseursData
    Dim oClientData As New ClientData
    Dim oClient As New Client
    Dim oLivreur As New Client
    Dim oImportData As New ImportData
    Dim oRapportLivraisonData As New RapportLivraisonData
    Dim oComissionKMData As New CommissionKMData
    Private oClientAddresse As New ClientAddresse
    Private oClientAddresseData As New ClientAddresseData
    Dim oLivreurs As New livreurs
    Dim oLivreurData As New LivreursData
    Dim oOrganisationData As New OrganisationsData
    Dim oAdminData As New AdminData
    Dim oAdministrateur As New Admin
    Dim oMaxPayableData As New MaxPayableData
    Dim oInvoiceData As New InvoiceData
    Dim oInvoiceLineData As New InvoiceLineData
    Dim oPayeData As New PayeData
    Dim oPayeLineData As New PayeLineData
    Dim oPeriodeData As New PeriodesData
    Dim oEtatsData As New EtatsData
    Dim oAchatsData As New AchatsData
    Dim oVentesData As New VentesData
    Dim oEtatsVentesData As New EtatsVentesData
    Dim oEtatsAchatsData As New EtatsAchatsData
    Dim oYearsData As New YearsData
    Dim oRapportFacturePayeData As New RapportFacturePayeData
    Dim oNomFichierImportData As New NomFichierImportData
    Dim oCadeauLivreurData As New CadeauLivreurData
    Dim oCommissionData As New CommissionData
    Dim oCommissionLineData As New CommissionLineData


    Private m_IsLoading As Boolean
    Private m_StatusID As Integer
    Private m_ImportID As Integer
    Private m_SelectedRow As Integer
    Private m_MaxPayable As Double
    Private m_Filename As String
    Private m_IDPeriode As Integer
    Dim TotalEchange As Integer = 0
    Private m_IDSelectedFacture As Integer
    Private m_IDSelectedPaye As Integer
    Private bPeriodeClick As Boolean = False
    Private IsGridClick As Boolean = False
    Private SelectedImportRow As Integer
    Private m_CustomerAddressID As Integer

    Public Property CustomerAddressID() As Int32
        Get
            Return m_CustomerAddressID
        End Get
        Set(ByVal value As Int32)
            m_CustomerAddressID = value
        End Set
    End Property

    Public Property IDSelectedFacture() As Int32
        Get
            Return m_IDSelectedFacture
        End Get
        Set(ByVal value As Int32)
            m_IDSelectedFacture = value
        End Set
    End Property

    Public Property IDSelectedPaye() As Int32
        Get
            Return m_IDSelectedPaye
        End Get
        Set(ByVal value As Int32)
            m_IDSelectedPaye = value
        End Set
    End Property
    Public Property IsLoading() As Boolean
        Get
            Return m_IsLoading
        End Get
        Set(ByVal value As Boolean)
            m_IsLoading = value
        End Set
    End Property
    Public Property IDPeriode() As Int32
        Get
            Return m_IDPeriode
        End Get
        Set(ByVal value As Int32)
            m_IDPeriode = value
        End Set
    End Property
    Public Property Filename() As String
        Get
            Return m_Filename
        End Get
        Set(ByVal value As String)
            m_Filename = value
        End Set
    End Property

    Public Property MaxPayable() As Double
        Get
            Return m_MaxPayable
        End Get
        Set(ByVal value As Double)
            m_MaxPayable = value
        End Set
    End Property

    Public Property SelectedRow() As Int32
        Get
            Return m_SelectedRow
        End Get
        Set(ByVal value As Int32)
            m_SelectedRow = value
        End Set
    End Property
    Public Property ImportID() As Int32
        Get
            Return m_ImportID
        End Get
        Set(ByVal value As Int32)
            m_ImportID = value
        End Set
    End Property

    Public Property cbStatusID() As Int32
        Get
            Return m_StatusID
        End Get
        Set(ByVal value As Int32)
            m_StatusID = value
        End Set
    End Property

    Private Sub FillAllResidence()

        Dim oResidenceData As New ResidenceData


        Dim dtResidence As DataTable
        Dim drResidence As DataRow
        Dim oVillesData As New VillesData
        Dim table As New DataTable
        table.Columns.Add("value", Type.GetType("System.Int32"))
        table.Columns.Add("displayText", Type.GetType("System.String"))
        dtResidence = oResidenceData.SelectAll()



        For Each drResidence In dtResidence.Rows
            Dim teller As Integer = 0

            teller = drResidence("IDResidence")
            Dim toto As String = drResidence("Residence")

            table.Rows.Add(New Object() {teller, toto})
        Next



        With cbResidence.Properties
            .DataSource = table
            .NullText = "Sélectionner une résidence"
            .DisplayMember = "displayText"
            .ValueMember = "value"
            .PopulateColumns()
            .PopupFormMinSize = New Size(50, 200)

        End With



    End Sub

    Private Sub FillAllVilles()

        Dim dtperiode As DataTable
        Dim drPeriode As DataRow
        Dim oVillesData As New VillesData
        Dim table As New DataTable
        table.Columns.Add("value", Type.GetType("System.Int32"))
        table.Columns.Add("displayText", Type.GetType("System.String"))
        dtperiode = oVillesData.SelectAll()



        For Each drPeriode In dtperiode.Rows
            Dim teller As Integer = 0

            teller = drPeriode("VilleID")
            Dim toto As String = drPeriode("Ville")

            table.Rows.Add(New Object() {teller, toto})
        Next



        With cbVille.Properties
            .DataSource = table
            .NullText = "Sélectionner une ville"
            .DisplayMember = "displayText"
            .ValueMember = "value"
            .PopulateColumns()
            .PopupFormMinSize = New Size(50, 200)

        End With



    End Sub


    Private Sub FillAllYears()

        Dim dtperiode As DataTable
        Dim drPeriode As DataRow


        Dim table As New DataTable
        table.Columns.Add("value", Type.GetType("System.Int32"))
        table.Columns.Add("displayText", Type.GetType("System.String"))
        dtperiode = oYearsData.SelectAll()



        For Each drPeriode In dtperiode.Rows
            Dim teller As Integer = 0

            teller = drPeriode("IDYear")
            Dim toto As String = drPeriode("Year")

            table.Rows.Add(New Object() {teller, toto})
        Next



        With cbAnnee.Properties
            .DataSource = table
            .NullText = "Années"
            .DisplayMember = "displayText"
            .ValueMember = "value"
            .PopulateColumns()
            .PopupFormMinSize = New Size(50, 200)
            .Columns("value").Visible = True
            .Columns("displayText").Caption = "Périodes"
        End With



    End Sub

    Private Sub FillAllPeriodes()

        Dim dtperiode As DataTable
        Dim drPeriode As DataRow


        Dim table As New DataTable
        table.Columns.Add("value", Type.GetType("System.Int32"))
        table.Columns.Add("displayText", Type.GetType("System.String"))
        dtperiode = oPeriodeData.SelectAll(optTypePeriode.SelectedIndex)



        For Each drPeriode In dtperiode.Rows
            Dim teller As Integer = 0

            teller = drPeriode("IDPeriode")
            Dim toto As String = drPeriode("Periode")

            table.Rows.Add(New Object() {teller, toto})
        Next



        With cbPeriodes.Properties
            .DataSource = table
            .NullText = "Selectionner une période"
            .DisplayMember = "displayText"
            .ValueMember = "value"
            .PopulateColumns()
            .PopupFormMinSize = New Size(50, 200)
            .Columns("value").Visible = True
            .Columns("displayText").Caption = "Périodes"
        End With



    End Sub


    Private Sub FillAllLivreur()

        Dim dtLivreur As DataTable
        Dim drLivreur As DataRow


        Dim table As New DataTable
        table.Columns.Add("value", Type.GetType("System.Int32"))
        table.Columns.Add("displayText", Type.GetType("System.String"))
        dtLivreur = oLivreurData.SelectAll()



        For Each drLivreur In dtLivreur.Rows
            Dim teller As Integer = 0

            teller = drLivreur("LivreurID")
            Dim toto As String = drLivreur("Livreur")

            table.Rows.Add(New Object() {teller, toto})
        Next



        With cblivreur.Properties
            .DataSource = table
            .NullText = "Selectionner un livreur"
            .DisplayMember = "displayText"
            .ValueMember = "value"
            .PopulateColumns()
            .PopupFormMinSize = New Size(50, 200)
        End With



    End Sub

    Private Sub FillAllNomFichierImport()
        Dim drFichierImport As DataRow
        Dim dtFichierImport As DataTable

        Dim table As New DataTable
        table.Columns.Add("value", Type.GetType("System.Int32"))
        table.Columns.Add("displayText", Type.GetType("System.String"))
        dtFichierImport = oNomFichierImportData.SelectAll()


        For Each drFichierImport In dtFichierImport.Rows
            Dim teller As Integer = 0

            teller = drFichierImport("IDNomFichierImport")
            Dim toto As String = drFichierImport("NomFichierImport").ToString.Trim

            table.Rows.Add(New Object() {teller, toto})
        Next

        With cbFichierImport.Properties
            .DataSource = table
            .NullText = "Selectionner une fichier d'import"
            .DisplayMember = "displayText"
            .ValueMember = "value"
            .PopulateColumns()
            .PopupFormMinSize = New Size(50, 200)
            .Columns("value").Visible = True
            .Columns("displayText").Caption = "fichier d'import"

        End With

    End Sub
    Private Sub FillNomFichierImport()
        Dim drFichierImport As DataRow
        Dim dtFichierImport As DataTable

        Dim table As New DataTable
        table.Columns.Add("value", Type.GetType("System.Int32"))
        table.Columns.Add("displayText", Type.GetType("System.String"))
        dtFichierImport = oNomFichierImportData.SelectAllByDate(go_Globals.PeriodeDateFrom, go_Globals.PeriodeDateTo)


        For Each drFichierImport In dtFichierImport.Rows
            Dim teller As Integer = 0

            teller = drFichierImport("IDNomFichierImport")
            Dim toto As String = drFichierImport("NomFichierImport").ToString.Trim

            table.Rows.Add(New Object() {teller, toto})
        Next

        With cbFichierImport.Properties
            .DataSource = table
            .NullText = "Selectionner une fichier d'import"
            .DisplayMember = "displayText"
            .ValueMember = "value"
            .PopulateColumns()
            .PopupFormMinSize = New Size(50, 200)
            .Columns("value").Visible = True
            .Columns("displayText").Caption = "fichier d'import"
        End With

    End Sub
    Private Sub FillAllInvoiceNumber()

        Dim drInvoice As DataRow
        Dim dtInvoice As DataTable

        Dim table As New DataTable
        table.Columns.Add("value", Type.GetType("System.Int32"))
        table.Columns.Add("displayText", Type.GetType("System.String"))


        If IsNothing(cbPeriodes.EditValue) Then Exit Sub

        dtInvoice = oRapportFacturePayeData.SelectAllF()



        For Each drInvoice In dtInvoice.Rows
            Dim teller As Integer = 0
            teller = drInvoice("IDRapport")
            Dim toto As String = drInvoice("NomFichier")

            table.Rows.Add(New Object() {teller, toto})
        Next

        With cbFactures.Properties
            .DataSource = table
            .NullText = "Selectionner une facture"
            .DisplayMember = "displayText"
            .ValueMember = "value"
            '.PopulateColumns()
            '.Columns("value").Visible = True
            .PopupFormMinSize = New Size(50, 200)

            '.Columns("displayText").Caption = "Liste des factures"
        End With




    End Sub
    Private Sub FillInvoiceNumber()

        Dim drInvoice As DataRow
        Dim dtInvoice As DataTable

        Dim table As New DataTable
        table.Columns.Add("value", Type.GetType("System.Int32"))
        table.Columns.Add("displayText", Type.GetType("System.String"))

        If IsNothing(cbPeriodes.EditValue) Then Exit Sub

        dtInvoice = oRapportFacturePayeData.SelectAllDataByPeriodeFNormal(IDPeriode, 0)


        For Each drInvoice In dtInvoice.Rows
            Dim teller As Integer = 0
            teller = drInvoice("IDRapport")
            Dim toto As String = drInvoice("NomFichier")

            table.Rows.Add(New Object() {teller, toto})
        Next

        With cbFactures.Properties
            .DataSource = table

            .DisplayMember = "displayText"
            .ValueMember = "value"
            '.PopulateColumns()
            .PopupFormMinSize = New Size(50, 200)
            '.Columns("value").Visible = True
            '.Columns("displayText").Caption = "Liste des factures"
        End With




    End Sub


    Private Sub FillAllRapportLivraisonRestaurant()

        Dim drLiv As DataRow
        Dim dtLiv As DataTable

        Dim table As New DataTable
        table.Columns.Add("value", Type.GetType("System.Int32"))
        table.Columns.Add("displayText", Type.GetType("System.String"))


        If IsNothing(cbPeriodes.EditValue) Then Exit Sub

        dtLiv = oRapportLivraisonData.SelectAllByPeriodeF(IDPeriode)


        For Each drLiv In dtLiv.Rows
            Dim teller As Integer = 0
            teller = drLiv("IDRapportLivraison")
            Dim toto As String = drLiv("NomFichier")

            table.Rows.Add(New Object() {teller, toto})
        Next

        With cbLivraisonRestaurants.Properties
            .DataSource = table
            .NullText = "Selectionner un rapport de livraison"
            .DisplayMember = "displayText"
            .ValueMember = "value"
            .PopupFormMinSize = New Size(50, 200)
            '.PopulateColumns()
            '.Columns("value").Visible = True
            '.Columns("displayText").Caption = "Liste des payes"
        End With



    End Sub
    Private Sub FillAllEtatsDeCompte()

        Dim drInvoice As DataRow
        Dim dtInvoice As DataTable

        Dim table As New DataTable
        table.Columns.Add("value", Type.GetType("System.Int32"))
        table.Columns.Add("displayText", Type.GetType("System.String"))


        If IsNothing(cbPeriodes.EditValue) Then Exit Sub

        dtInvoice = oEtatsData.SelectAllByPeriode(IDPeriode)


        For Each drInvoice In dtInvoice.Rows
            Dim teller As Integer = 0
            teller = drInvoice("IDEtats")
            Dim toto As String = drInvoice("NomFichier")

            table.Rows.Add(New Object() {teller, toto})
        Next

        With cbEtatsDeComptes.Properties
            .DataSource = table
            .NullText = "Selectionner un état de compte"
            .DisplayMember = "displayText"
            .ValueMember = "value"
            .PopupFormMinSize = New Size(50, 200)
            '.PopulateColumns()
            '.Columns("value").Visible = True
            '.Columns("displayText").Caption = "Liste des payes"
        End With



    End Sub

    Private Sub FillAllRestaurants()

        Dim drR As DataRow
        Dim dtR As DataTable

        Dim table As New DataTable
        table.Columns.Add("value", Type.GetType("System.Int32"))
        table.Columns.Add("displayText", Type.GetType("System.String"))


        If IsNothing(cbPeriodes.EditValue) Then Exit Sub

        dtR = oOrganisationData.SelectAllRestaurant


        For Each drR In dtR.Rows
            Dim teller As Integer = 0
            teller = drR("OrganisationID")
            Dim toto As String = drR("Organisation").ToString.Trim

            table.Rows.Add(New Object() {teller, toto})
        Next

        With cbRestaurant.Properties
            .DataSource = table
            .NullText = "Selectionner un restaurant"
            .DisplayMember = "displayText"
            .ValueMember = "value"
            .PopupFormMinSize = New Size(50, 200)
            '.PopulateColumns()
            '.Columns("value").Visible = True
            '.Columns("displayText").Caption = "Liste des payes"
        End With



    End Sub


    Private Sub FillRapportRevenu()


        Dim drTaxes As DataRow
        Dim dtTaxes As DataTable

        Dim oRapportTaxation As New RapportTaxationPayeLivreur
        Dim oRapportTaxationData As New RapportTaxationPayeLivreurData

        Dim table As New DataTable
        table.Columns.Add("value", Type.GetType("System.Int32"))
        table.Columns.Add("displayText", Type.GetType("System.String"))
        dtTaxes = oRapportTaxationData.SelectAllByDate(go_Globals.PeriodeDateFrom, go_Globals.PeriodeDateTo)


        For Each drTaxes In dtTaxes.Rows
            Dim teller As Integer = 0
            teller = drTaxes("IDRapportTaxation")
            Dim toto As String = drTaxes("NomFichier")

            table.Rows.Add(New Object() {teller, toto})
        Next

        With cbRapportRevenuPaye.Properties
            .DataSource = table
            .DisplayMember = "displayText"
            .ValueMember = "value"
            '.PopulateColumns()
            .PopupFormMinSize = New Size(50, 200)
            '.Columns("value").Visible = True
            '.Columns("displayText").Caption = "Liste des pays"
        End With



    End Sub


    Private Sub FillRapportTaxation()

        Dim drTaxes As DataRow
        Dim dtTaxes As DataTable

        Dim oRapportTaxation As New RapportTaxation

        Dim oRapportTaxationData As New RapportTaxationData

        Dim table As New DataTable
        table.Columns.Add("value", Type.GetType("System.Int32"))
        table.Columns.Add("displayText", Type.GetType("System.String"))
        dtTaxes = oRapportTaxationData.SelectAllByDate(go_Globals.PeriodeDateFrom, go_Globals.PeriodeDateTo)


        For Each drTaxes In dtTaxes.Rows
            Dim teller As Integer = 0
            teller = drTaxes("IDRapportTaxation")
            Dim toto As String = drTaxes("NomFichier")

            table.Rows.Add(New Object() {teller, toto})
        Next

        With cbRapportTaxation.Properties
            .DataSource = table
            .DisplayMember = "displayText"
            .ValueMember = "value"
            '.PopulateColumns()
            .PopupFormMinSize = New Size(50, 200)
            '.Columns("value").Visible = True
            '.Columns("displayText").Caption = "Liste des pays"
        End With



    End Sub




    Private Sub FillCommission()

        Dim drInvoice As DataRow
        Dim dtInvoice As DataTable

        Dim table As New DataTable
        table.Columns.Add("value", Type.GetType("System.Int32"))
        table.Columns.Add("displayText", Type.GetType("System.String"))
        dtInvoice = oRapportCommissionData.SelectAllPByDate(go_Globals.PeriodeDateFrom, go_Globals.PeriodeDateTo)


        For Each drInvoice In dtInvoice.Rows
            Dim teller As Integer = 0
            teller = drInvoice("IDRapportCommission")
            Dim toto As String = drInvoice("NomFichier")

            table.Rows.Add(New Object() {teller, toto})
        Next

        With cbCommissions.Properties
            .DataSource = table
            .DisplayMember = "displayText"
            .ValueMember = "value"
            '.PopulateColumns()
            .PopupFormMinSize = New Size(50, 200)
            '.Columns("value").Visible = True
            '.Columns("displayText").Caption = "Liste des pays"
        End With



    End Sub

    Private Sub FillPaieNumber()

        Dim drInvoice As DataRow
        Dim dtInvoice As DataTable

        Dim table As New DataTable
        table.Columns.Add("value", Type.GetType("System.Int32"))
        table.Columns.Add("displayText", Type.GetType("System.String"))
        dtInvoice = oRapportFacturePayeData.SelectAllPByDate(go_Globals.PeriodeDateFrom, go_Globals.PeriodeDateTo)


        For Each drInvoice In dtInvoice.Rows
            Dim teller As Integer = 0
            teller = drInvoice("IDRapport")
            Dim toto As String = drInvoice("NomFichier")

            table.Rows.Add(New Object() {teller, toto})
        Next

        With cbPayes.Properties
            .DataSource = table
            .DisplayMember = "displayText"
            .ValueMember = "value"
            '.PopulateColumns()
            .PopupFormMinSize = New Size(50, 200)
            '.Columns("value").Visible = True
            '.Columns("displayText").Caption = "Liste des pays"
        End With



    End Sub
    Sub New()


        InitSkins()
        InitializeComponent()

        Me.InitSkinGallery()

        DevExpress.Skins.SkinManager.EnableFormSkins()


    End Sub
    Sub InitSkins()
        DevExpress.Skins.SkinManager.EnableFormSkins()
        DevExpress.UserSkins.BonusSkins.Register()
        'UserLookAndFeel.Default.SetSkinStyle("Pumpkin")



    End Sub
    Private Sub InitSkinGallery()
        SkinHelper.InitSkinGallery(SkinRibbonGalleryBarItem1, True)
    End Sub
    Private Sub frmMain_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Try
            IsLoading = True

            checkServer()


            My.Settings.Item("IDScomptabiliteConnectionString") = cnString
            Process.GetCurrentProcess().PriorityClass = ProcessPriorityClass.AboveNormal


            DockEdit.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Hidden
            DockResidence.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Hidden

            Dim oPeriode As New Periodes
            Dim dtPeriode As New DataTable
            'Thread.CurrentThread.CurrentCulture = New CultureInfo("en-US", False)

            Microsoft.Win32.Registry.SetValue("HKEY_CURRENT_USER\Control Panel\International", "sShortDate", "M/d/yyyy")

            My.Application.ChangeCulture("en-US")

            Threading.Thread.CurrentThread.CurrentCulture = New CultureInfo("en-US")
            Threading.Thread.CurrentThread.CurrentUICulture = New CultureInfo("en-US")




            Call Application.EnableVisualStyles()



            Dim dtAdministrateur As DataTable
            Dim drAdministrateur As DataRow
            dtAdministrateur = oAdminData.SelectAll

            For Each drAdministrateur In dtAdministrateur.Rows
                go_Globals.CompanyAdresse = If(IsDBNull(drAdministrateur("CompanyAdresse")), Nothing, drAdministrateur("CompanyAdresse").ToString)
                go_Globals.CompanyContactTelephone = If(IsDBNull(drAdministrateur("CompanyContactTelephone")), Nothing, drAdministrateur("CompanyContactTelephone").ToString)
                go_Globals.CompanyName = If(IsDBNull(drAdministrateur("CompanyName")), Nothing, drAdministrateur("CompanyName").ToString)
                go_Globals.TPS = If(IsDBNull(drAdministrateur("TPS")), Nothing, drAdministrateur("TPS").ToString)
                go_Globals.TVQ = If(IsDBNull(drAdministrateur("TVQ")), Nothing, drAdministrateur("TVQ").ToString)
                go_Globals.TPSNumero = If(IsDBNull(drAdministrateur("TPSNumero")), Nothing, drAdministrateur("TPSNumero").ToString)
                go_Globals.TVQNumero = If(IsDBNull(drAdministrateur("TVQNumero")), Nothing, drAdministrateur("TVQNumero").ToString)
                go_Globals.LocationPaie = If(IsDBNull(drAdministrateur("LocationPaie")), Nothing, drAdministrateur("LocationPaie").ToString)
                go_Globals.LocationFacture = If(IsDBNull(drAdministrateur("LocationFacture")), Nothing, drAdministrateur("LocationFacture").ToString)
                go_Globals.LocationConsolidation = If(IsDBNull(drAdministrateur("LocationConsolidation")), Nothing, drAdministrateur("LocationConsolidation").ToString)
                go_Globals.LocationVentes = If(IsDBNull(drAdministrateur("LocationVentes")), Nothing, drAdministrateur("LocationVentes").ToString)
                go_Globals.LocationAchats = If(IsDBNull(drAdministrateur("LocationAchats")), Nothing, drAdministrateur("LocationAchats").ToString)
                go_Globals.LocationCommissions = If(IsDBNull(drAdministrateur("LocationCommissions")), Nothing, drAdministrateur("LocationCommissions").ToString)
                go_Globals.LocationRapportLivreur = If(IsDBNull(drAdministrateur("LocationRapportLivreur")), Nothing, drAdministrateur("LocationRapportLivreur").ToString)
                go_Globals.LocationRapportRestaurant = If(IsDBNull(drAdministrateur("LocationRapportRestaurant")), Nothing, drAdministrateur("LocationRapportRestaurant").ToString)
                go_Globals.LocationRapportDeTaxation = If(IsDBNull(drAdministrateur("LocationRapportDeTaxation")), Nothing, drAdministrateur("LocationRapportDeTaxation").ToString)
                go_Globals.LocationRapportDeTaxationIDS = If(IsDBNull(drAdministrateur("LocationRapportDeTaxationIDS")), Nothing, drAdministrateur("LocationRapportDeTaxationIDS").ToString)
                go_Globals.RapportTaxationLivreurPaye = If(IsDBNull(drAdministrateur("RapportTaxationLivreurPaye")), Nothing, drAdministrateur("RapportTaxationLivreurPaye").ToString)

                go_Globals.Server = If(IsDBNull(drAdministrateur("Server")), Nothing, drAdministrateur("Server").ToString)
                go_Globals.Username = If(IsDBNull(drAdministrateur("Username")), Nothing, drAdministrateur("Username").ToString)
                go_Globals.Password = If(IsDBNull(drAdministrateur("Password")), Nothing, drAdministrateur("Password").ToString)

                If Not IsDBNull(drAdministrateur("UseSSL")) Then
                    go_Globals.UseSSL = drAdministrateur("UseSSL")
                Else
                    go_Globals.UseSSL = False
                End If
                go_Globals.Port = If(IsDBNull(drAdministrateur("Port")), Nothing, drAdministrateur("Port").ToString)

            Next

            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

            'xVersion = myFileVersionInfo.FileVersion

            Me.Text = "IDS Compatibilité"


            My.Application.ChangeCulture("en-US")

            System.Threading.Thread.CurrentThread.CurrentCulture = New System.Globalization.CultureInfo("fr-FR")


            Dim dDatefrom As DateTime = GetLastSunday()
            Dim dDateTo As DateTime = GetNextSaturday(dDatefrom)

            tbDateFrom.EditValue = New DateTime(dDatefrom.Year, dDatefrom.Month, dDatefrom.Day, 0, 0, 0, 0)
            tbDateTo.EditValue = New DateTime(dDateTo.Year, dDateTo.Month, dDateTo.Day, 23, 59, 0, 0)

            go_Globals.Year = Now.Year

            FillAllLivreur()

            FillAllVilles()
            FillAllRestaurants()

            FillAllPeriodes()

            Dim ddDateFrom As DateTime
            Dim ddDateTo As DateTime
            My.Application.ChangeCulture("en-US")


            ddDateFrom = tbDateFrom.EditValue
            ddDateTo = tbDateTo.EditValue

            go_Globals.PeriodeDateFrom = ddDateFrom
            go_Globals.PeriodeDateTo = ddDateTo





            FillAllYears()

            cbAnnee.EditValue = cbAnnee.Properties.GetKeyValueByDisplayText(Now.Year.ToString)



            FillAllNomFichierImport()

            'FillAllPeriodes()
            'FillAllPeriodesEtat()

            'bGenererPaye.Enabled = False
            bImprimerFactures.Enabled = False
            btnGenererFacturation.Enabled = False
            'bGenererPaye.Enabled = False
            bImportMobilus.Enabled = False
            bEnvoyerPaye.Enabled = False
            bEnvoyerFacture.Enabled = False
            bSelectfile.Enabled = False

            'bImprimerFacturation.Enabled = False




            go_Globals.PeriodeDateFrom = tbDateFrom.Text
            go_Globals.PeriodeDateTo = tbDateTo.Text

            Dim dtClientAddress As New DataTable
            'Dim drClientAddress As DataRow



            grdClientsAddresse.DataSource = oClientAddresseData.SelectAll()

            Calendar.StartDate = DateTime.Now



            Me.WindowState = FormWindowState.Maximized

            Me.BringToFront()


            IsLoading = False

            Me.Cursor = System.Windows.Forms.Cursors.Default

            Exit Sub

        Catch ex As Exception
            Me.Cursor = System.Windows.Forms.Cursors.Default
            MessageBox.Show("Form1_Load()    " & ex.Message, "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try

    End Sub
    Private Sub bFermer_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles bFermer.ItemClick
        If MsgBox("Fermer le programme?", MsgBoxStyle.YesNo + MsgBoxStyle.Information, "Exit") = MsgBoxResult.Yes Then
            Application.Exit()
        Else
            Exit Sub
        End If
    End Sub

    Private Sub bLivreurs_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles bLivreurs.ItemClick
        If frmLivreurs.Visible = True Then
            frmLivreurs.WindowState = FormWindowState.Normal
            frmLivreurs.BringToFront()
        End If

        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        frmLivreurs.Show()
        Me.Cursor = System.Windows.Forms.Cursors.Default
    End Sub

    Private Sub bOrganisations_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles bOrganisations.ItemClick
        If IsNothing(cbPeriodes.EditValue) Then
            MessageBox.Show("Vous devez selectionner une periode")
            Exit Sub
        End If
        go_Globals.PeriodeDateFrom = tbDateFrom.Text
        go_Globals.PeriodeDateTo = tbDateTo.Text

        If frmOrganisations.Visible = True Then
            frmOrganisations.WindowState = FormWindowState.Normal
            frmOrganisations.BringToFront()
        End If

        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        frmOrganisations.Show()
        Me.Cursor = System.Windows.Forms.Cursors.Default
    End Sub

    Private Sub LoadGridCode()
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        Try

            'GridView1.BestFitColumns()
            GridView1.ClearSorting()
            GridView1.Columns("ImportID").SortOrder = DevExpress.Data.ColumnSortOrder.Ascending

            GridView1.Columns("HeurePrep").Width = 200
            GridView1.Columns("HeureDepart").Width = 200
            GridView1.Columns("HeureLivraison").Width = 200
            GridView1.Columns("Extra").Width = 200
            GridView1.Columns("Credit").Width = 200


            GridView1.OptionsView.ColumnAutoWidth = False
            'GridView1.BestFitColumns()
            Dim viewInfo As DevExpress.XtraGrid.Views.Grid.ViewInfo.GridViewInfo = TryCast(GridView1.GetViewInfo(), GridViewInfo)
            If viewInfo.ViewRects.ColumnTotalWidth < viewInfo.ViewRects.ColumnPanelWidth Then
                GridView1.OptionsView.ColumnAutoWidth = True
            End If


        Catch
        End Try
        Me.Cursor = System.Windows.Forms.Cursors.Default
    End Sub


    Private Sub bSettings_ItemClick(sender As Object, e As ItemClickEventArgs) Handles bSettings.ItemClick
        If frmAdministrateur.Visible = True Then
            frmAdministrateur.WindowState = FormWindowState.Normal
            frmAdministrateur.BringToFront()
        End If

        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        frmAdministrateur.Show()
        Me.Cursor = System.Windows.Forms.Cursors.Default
    End Sub

    Private Sub bFermer2_ItemClick(sender As Object, e As ItemClickEventArgs) Handles bFermer2.ItemClick
        If MsgBox("Fermer le programme?", MsgBoxStyle.YesNo + MsgBoxStyle.Information, "Exit") = MsgBoxResult.Yes Then
            Application.Exit()
        Else
            Exit Sub
        End If
    End Sub


    Private Function GetLastSunday() As DateTime

        Dim dLastSunday As Date = Now.AddDays(-(Now.DayOfWeek + 7))

        Return dLastSunday



    End Function

    Private Function GetNextSaturday(dDatefrom As DateTime) As DateTime

        Dim NextSaturday = dDatefrom.AddDays(6)

        Return NextSaturday

    End Function

    Private Sub bFiltrerDate_Click(sender As Object, e As EventArgs) Handles bFiltrerDate.Click


        Dim dDateFrom As DateTime = tbDateFrom.Text
        Dim dDateTo As DateTime = tbDateTo.Text
        Dim frenchCultureInfo = New Globalization.CultureInfo("fr-CA")
        Dim oPeriode As New Periodes
        Dim dtPeriode As New DataTable
        Dim drPeriode As DataRow

        IsLoading = True


        If dDateFrom.Month <> dDateTo.Month Then
            oPeriode.Periode = dDateFrom.Day & " " & GetMonthName(dDateFrom, frenchCultureInfo) & "-" & dDateTo.Day & " " & GetMonthName(dDateTo, frenchCultureInfo) & " " & dDateTo.Year.ToString
        Else
            oPeriode.Periode = dDateFrom.Day & "-" & dDateTo.Day & " " & GetMonthName(dDateTo, frenchCultureInfo) & " " & dDateTo.Year.ToString
        End If

        oPeriode.DateFrom = dDateFrom
        oPeriode.DateTo = dDateTo
        oPeriode.YearFrom = dDateFrom.Year
        oPeriode.YearTo = dDateTo.Year
        oPeriode.IDTypePeriode = optTypePeriode.SelectedIndex
        go_Globals.PeriodeDateFrom = dDateFrom
        go_Globals.PeriodeDateTo = dDateTo

        dtPeriode = oPeriodeData.SelectAllByPeriodes(oPeriode.Periode)
        If dtPeriode.Rows.Count > 0 Then
            For Each drPeriode In dtPeriode.Rows
                IDPeriode = drPeriode("IDPeriode")
                go_Globals.IDPeriode = IDPeriode
            Next

        Else
            Dim result As DialogResult = MessageBox.Show("Cette période n'existe pas dans le système, désirez-vous la créer?", "Periode", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
            If result = DialogResult.No Then

            ElseIf result = DialogResult.Yes Then
                IDPeriode = oPeriodeData.Add(oPeriode)
                go_Globals.IDPeriode = IDPeriode
            End If
        End If

        Try

            Me.Cursor = Cursors.WaitCursor
            My.Application.ChangeCulture("en-US")

            go_Globals.PeriodeDateFrom = dDateFrom
            go_Globals.PeriodeDateTo = dDateTo

            grdImport.DataSource = oImportData.SelectAllFromDate(dDateFrom, dDateTo)

            GridView1.OptionsView.ColumnAutoWidth = False
            'GridView1.BestFitColumns()
            Dim viewInfo As DevExpress.XtraGrid.Views.Grid.ViewInfo.GridViewInfo = TryCast(GridView1.GetViewInfo(), GridViewInfo)
            If viewInfo.ViewRects.ColumnTotalWidth < viewInfo.ViewRects.ColumnPanelWidth Then
                GridView1.OptionsView.ColumnAutoWidth = True
            End If


            If IDPeriode > 0 Then
                FillPaieNumber()
                FillInvoiceNumber()
                FillNomFichierImport()
                FillAllPeriodes()
                cbPeriodes.EditValue = IDPeriode
                cbPeriodes.EditValue = IDPeriode


                'bGenererPaye.Enabled = True
                bImprimerFactures.Enabled = True
                btnGenererFacturation.Enabled = True
                'bGenererPaye.Enabled = True
                bImportMobilus.Enabled = True
                bEnvoyerPaye.Enabled = True
                bEnvoyerFacture.Enabled = True
                bSelectfile.Enabled = True
                'bImprimerFacturation.Enabled = True

            Else

                FillPaieNumber()
                FillInvoiceNumber()
                FillNomFichierImport()
                FillAllPeriodes()

                'bGenererPaye.Enabled = False
                bImprimerFactures.Enabled = False
                btnGenererFacturation.Enabled = False
                'bGenererPaye.Enabled = False
                bImportMobilus.Enabled = False
                bEnvoyerPaye.Enabled = False
                bEnvoyerFacture.Enabled = False
                bSelectfile.Enabled = False
                'bImprimerFacturation.Enabled = False

            End If


            IsLoading = False
            Me.Cursor = Cursors.Default

        Catch ex As Exception
            Me.Cursor = Cursors.Default
            MessageBox.Show(ex.Message, "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try

    End Sub

    Public Function GetPositionOfFirstNumericCharacter(ByVal s As String) As Integer
        For i = 1 To Len(s)
            Dim currentCharacter As String
            currentCharacter = Mid(s, i, 1)
            If IsNumeric(currentCharacter) = True Then
                GetPositionOfFirstNumericCharacter = i
                Exit Function
            End If
        Next i
    End Function

    Public Function checkServer() As Boolean
        Try


            Dim fileReader As String
            fileReader = My.Computer.FileSystem.ReadAllText(Application.StartupPath & "\Config.ini")

            If fileReader.ToString.Trim = "" Then

                Return False
            Else
                cnString = fileReader.ToString.Trim
                Return True

            End If

            Exit Function

        Catch ex As Exception
            MessageBox.Show(ex.Message, "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            checkServer = False
        End Try
    End Function


    Public Function GenererCommissionVendeurslivreurs() As Boolean


        Try
            Dim oCommission As New Commission
            Dim oGenerationCommission As New GenerationCommissions
            Dim oLivreur As New livreurs
            Dim dtLivreur As DataTable
            Dim drLivreur As DataRow
            Dim oLivreurP As New livreurs
            Dim dtLivreurP As DataTable
            Dim drLivreurP As DataRow
            Dim dDateFrom As Date = tbDateFrom.Text
            Dim dDateTo As Date = tbDateTo.Text
            Dim frenchCultureInfo = New Globalization.CultureInfo("fr-CA")
            Dim iLivreurID As Integer
            Dim sOrganisation As String = ""
            Dim sLivreur As String = ""
            Dim oCommissionLine As New CommissionLine
            Dim dtRapportCommission As New DataTable
            Dim drRapportCommission As DataRow
            Dim dtCommission As New DataTable
            Dim drCommission As DataRow
            Dim dblAmount As Double = 0
            Dim dblPourcentageCommission As Double = 0
            Dim dblTotalCommission As Double = 0
            Dim iIDLivreur As Integer = 0
            Dim iIDLivreurString As String = ""
            Dim iIDRapport As Integer = 0
            Dim dtPaye As New DataTable
            Dim strSelectedValue As String = ""
            Dim iSelectedValue As Integer = 0


            Me.Cursor = Cursors.WaitCursor

            Dim oPeriode As New Periodes
            Dim dtPeriode As New DataTable
            Dim drPeriode As DataRow

            If dDateFrom.Month <> dDateTo.Month Then
                oPeriode.Periode = dDateFrom.Day & " " & GetMonthName(dDateFrom, frenchCultureInfo) & "-" & dDateTo.Day & " " & GetMonthName(dDateTo, frenchCultureInfo) & " " & dDateTo.Year.ToString
            Else
                oPeriode.Periode = dDateFrom.Day & "-" & dDateTo.Day & " " & GetMonthName(dDateTo, frenchCultureInfo) & " " & dDateTo.Year.ToString
            End If

            dtPeriode = oPeriodeData.SelectAllByPeriode(go_Globals.IDPeriode)
            If dtPeriode.Rows.Count > 0 Then
                For Each drPeriode In dtPeriode.Rows
                    IDPeriode = drPeriode("IDPeriode")
                Next
            Else
                Me.Cursor = Cursors.Default
                MessageBox.Show("La période n'existe pas!, Veuillez la créer en appuisant sur <Filtrer>", "Période manquante", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1)
                Return False
                Exit Function
            End If


            For Each item As DevExpress.XtraEditors.Controls.CheckedListBoxItem In cbCommissions.Properties.Items
                If item.CheckState = CheckState.Checked Then
                    strSelectedValue = strSelectedValue & item.Value & ","
                    iSelectedValue = iSelectedValue + 1
                End If
            Next

            If strSelectedValue.ToString.Trim <> "" Then
                strSelectedValue = strSelectedValue.Remove(strSelectedValue.Length - 1)
            End If



            Dim oCommissionLineB As New CommissionLine
            oCommissionLineB.IDPeriode = IDPeriode
            oCommissionLineData.DeleteByPeriodCommissionLivreur(oCommissionLineB)

            If iSelectedValue > 0 Then
                dtRapportCommission = oRapportCommissionData.SelectAllByIDsCommission(strSelectedValue)
                If dtRapportCommission.Rows.Count > 0 Then
                    For Each drRapportCommission In dtRapportCommission.Rows
                        iIDLivreur = If(IsDBNull(drRapportCommission("IDLivreur")), 0, CType(drRapportCommission("IDLivreur"), Int32?))

                        iIDLivreurString = iIDLivreurString & iIDLivreur.ToString & ","

                        dDateFrom = drRapportCommission("DateFrom")
                        dDateTo = drRapportCommission("DateTo")

                        Dim oCommissionLine2 As New CommissionLine
                        oCommissionLine2.IDLivreur = iIDLivreur
                        oCommissionLine2.IDPeriode = IDPeriode
                        oCommissionLineData.DeleteByPeriodLivreurCommissionLivreur(oCommissionLineB)

                    Next

                    If iIDLivreurString.ToString.Trim <> "" Then
                        iIDLivreurString = iIDLivreurString.Remove(iIDLivreurString.Length - 1)
                    End If

                End If
            End If

            dtLivreurP = oLivreurData.SelectAllVendeurs()

            If dtLivreurP.Rows.Count > 0 Then

                ProgressBarControl1.Properties.Step = 1
                ProgressBarControl1.Properties.PercentView = True
                ProgressBarControl1.Properties.Maximum = dtLivreurP.Rows.Count
                ProgressBarControl1.Properties.Minimum = 0

                For Each drLivreurP In dtLivreurP.Rows

                    iLivreurID = drLivreurP("LivreurID")


                    '--------------------------------------------
                    ' Cree l'entree du fichier des commissions
                    '--------------------------------------------
                    ' check if paye exist
                    Dim dtCommissionL As DataTable
                    Dim drCommissionL As DataRow

                    dtCommissionL = oCommissionData.SelectAllByPeriodIDLivreur(IDPeriode, drLivreurP("LivreurID"))

                    If dtCommissionL.Rows.Count > 0 Then
                        For Each drCommissionL In dtCommissionL.Rows
                            oCommission.CommissionNumber = drCommissionL("CommissionNumber")
                            oCommission.CommissionDate = drCommissionL("CommissionDate")
                            oCommission.DateFrom = drCommissionL("DateFrom")
                            oCommission.DateTo = drCommissionL("DateTo")
                            oCommission.IDPeriode = drCommissionL("IDPeriode")
                            oCommission.IDCommission = drCommissionL("IDCommission")
                        Next



                    Else
                        ' aucunes payes existe alors on la cree 
                        oCommission.CommissionNumber = oGenerationCommission.generate_CommissionNumero
                        oCommission.CommissionDate = dDateTo.ToShortDateString
                        oCommission.IDLivreur = drLivreurP("LivreurID")
                        oCommission.Livreur = drLivreurP("Livreur").ToString.Trim
                        oCommission.DateFrom = tbDateFrom.Text
                        oCommission.DateTo = tbDateTo.Text
                        oCommission.IDPeriode = IDPeriode
                        oCommission.Amount = 0
                        oCommission.TVQ = 0
                        oCommission.TPS = 0
                        oCommission.IDCommission = oCommissionData.Add(oCommission)

                    End If



                    '----------------------------------------------------------------------------------------
                    ' Selectionne tous les livreurs affectes a ce vendeur
                    '----------------------------------------------------------------------------------------
                    dtLivreur = oLivreurData.SelectAllByVendeur(iLivreurID)
                    If dtLivreur.Rows.Count > 0 Then



                        For Each drLivreur In dtLivreur.Rows


                            ' selectionne toutes les payes pour ce livreur
                            dtCommission = oPayeData.SelectAllByPeriodLivreurID(IDPeriode, drLivreur("LivreurID"))

                            If dtCommission.Rows.Count > 0 Then

                                For Each drCommission In dtCommission.Rows

                                    dblAmount = drCommission("Amount")
                                    dblPourcentageCommission = drLivreur("CommissionVendeur")

                                    dblTotalCommission = dblAmount * dblPourcentageCommission

                                    oCommissionLine.IDCommission = oCommission.IDCommission
                                    oCommissionLine.CommissionNumber = oCommission.CommissionNumber
                                    oCommissionLine.CommissionDate = oCommission.CommissionDate
                                    oCommissionLine.IDLivreur = drLivreur("LivreurID")
                                    oCommissionLine.Livreur = drLivreur("Livreur").ToString.Trim
                                    oCommissionLine.OrganisationID = Nothing
                                    oCommissionLine.Organisation = Nothing
                                    oCommissionLine.Code = Nothing
                                    oCommissionLine.Km = 0
                                    oCommissionLine.Description = drLivreur("Livreur").ToString.Trim & "/ Commissions"
                                    oCommissionLine.Unite = 1
                                    oCommissionLine.Prix = Math.Round(dblTotalCommission, 2)
                                    oCommissionLine.Montant = Math.Round(dblTotalCommission, 2)
                                    oCommissionLine.Credit = 0
                                    oCommissionLine.DateFrom = dDateFrom
                                    oCommissionLine.DateTo = dDateTo
                                    oCommissionLine.NombreGlacier = 0
                                    oCommissionLine.Glacier = 0
                                    oCommissionLine.IDPeriode = go_Globals.IDPeriode
                                    oCommissionLine.TypeAjout = 1
                                    oCommissionLine.Ajout = False
                                    oCommissionLine.IsRestaurant = False
                                    oCommissionLineData.Add(oCommissionLine)

                                Next

                            End If


                            '    Next


                            'End If

                        Next

                    End If


                    ProgressBarControl1.PerformStep()
                    ProgressBarControl1.Update()

                Next ' NEXT LIVREUR Periode
            End If



            ProgressBarControl1.EditValue = 0

            Return True

        Catch ex As Exception
            Me.Cursor = Cursors.Default
            MessageBox.Show(ex.Message, "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Return False
        End Try



    End Function

    Public Function GenererCommissionVendeursRestaurants() As Boolean


        Try
            Dim oCommission As New Commission
            Dim oGenerationCommission As New GenerationCommissions
            Dim oLivreur As New livreurs
            Dim dtorganisation As DataTable
            Dim drOrganisation As DataRow
            Dim oLivreurP As New livreurs
            Dim dtLivreurP As DataTable
            Dim drLivreurP As DataRow
            Dim dDateFrom As Date = tbDateFrom.Text
            Dim dDateTo As Date = tbDateTo.Text
            Dim frenchCultureInfo = New Globalization.CultureInfo("fr-CA")
            Dim iLivreurID As Integer
            Dim sOrganisation As String = ""
            Dim sLivreur As String = ""
            Dim oCommissionLine As New CommissionLine
            Dim dtRapportCommission As New DataTable
            Dim drRapportCommission As DataRow
            Dim dtCommission As New DataTable
            Dim drCommission As DataRow
            Dim dblAmount As Double = 0
            Dim dblPourcentageCommission As Double = 0
            Dim dblTotalCommission As Double = 0
            Dim iIDLivreur As Integer = 0
            Dim iIDLivreurString As String = ""
            Dim iIDRapport As Integer = 0
            Dim dtPaye As New DataTable
            Dim strSelectedValue As String = ""
            Dim iSelectedValue As Integer = 0


            Me.Cursor = Cursors.WaitCursor

            Dim oPeriode As New Periodes
            Dim dtPeriode As New DataTable
            Dim drPeriode As DataRow

            If dDateFrom.Month <> dDateTo.Month Then
                oPeriode.Periode = dDateFrom.Day & " " & GetMonthName(dDateFrom, frenchCultureInfo) & "-" & dDateTo.Day & " " & GetMonthName(dDateTo, frenchCultureInfo) & " " & dDateTo.Year.ToString
            Else
                oPeriode.Periode = dDateFrom.Day & "-" & dDateTo.Day & " " & GetMonthName(dDateTo, frenchCultureInfo) & " " & dDateTo.Year.ToString
            End If

            dtPeriode = oPeriodeData.SelectAllByPeriode(go_Globals.IDPeriode)
            If dtPeriode.Rows.Count > 0 Then
                For Each drPeriode In dtPeriode.Rows
                    IDPeriode = drPeriode("IDPeriode")
                Next
            Else
                Me.Cursor = Cursors.Default
                MessageBox.Show("La période n'existe pas!, Veuillez la créer en appuisant sur <Filtrer>", "Période manquante", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1)
                Return False
                Exit Function
            End If


            For Each item As DevExpress.XtraEditors.Controls.CheckedListBoxItem In cbCommissions.Properties.Items
                If item.CheckState = CheckState.Checked Then
                    strSelectedValue = strSelectedValue & item.Value & ","
                    iSelectedValue = iSelectedValue + 1
                End If
            Next

            If strSelectedValue.ToString.Trim <> "" Then
                strSelectedValue = strSelectedValue.Remove(strSelectedValue.Length - 1)
            End If

            If iSelectedValue = 0 Then
                dtPaye = oCommissionData.SelectAllByPeriodCommission(IDPeriode)
                If dtPaye.Rows.Count > 0 Then
                    Dim result As DialogResult = MessageBox.Show("Les fiches de commissions ont déjà été  générées pour cette periode, Voulez-vous les regénerer?", "Facturation", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
                    If result = DialogResult.No Then
                        Return True
                        Exit Function
                        Me.Cursor = Cursors.Default
                    ElseIf result = DialogResult.Yes Then

                        Dim oCommissionLineB As New CommissionLine
                        oCommissionLineB.IDPeriode = IDPeriode
                        oCommissionLineData.DeleteByPeriodCommission(oCommissionLineB)


                    End If
                End If
            End If


            If iSelectedValue > 0 Then
                dtRapportCommission = oRapportCommissionData.SelectAllByIDsCommission(strSelectedValue)
                If dtRapportCommission.Rows.Count > 0 Then
                    For Each drRapportCommission In dtRapportCommission.Rows
                        iIDLivreur = If(IsDBNull(drRapportCommission("IDLivreur")), 0, CType(drRapportCommission("IDLivreur"), Int32?))

                        iIDLivreurString = iIDLivreurString & iIDLivreur.ToString & ","

                        dDateFrom = drRapportCommission("DateFrom")
                        dDateTo = drRapportCommission("DateTo")

                        Dim oCommissionLineB As New CommissionLine
                        oCommissionLineB.IDLivreur = iIDLivreur
                        oCommissionLineB.IDPeriode = IDPeriode
                        oCommissionLineData.DeleteByPeriodLivreurCommission(oCommissionLineB)

                    Next

                    If iIDLivreurString.ToString.Trim <> "" Then
                        iIDLivreurString = iIDLivreurString.Remove(iIDLivreurString.Length - 1)
                    End If

                End If
            End If

            dtLivreurP = oLivreurData.SelectAllVendeurs()

            If dtLivreurP.Rows.Count > 0 Then

                ProgressBarControl1.Properties.Step = 1
                ProgressBarControl1.Properties.PercentView = True
                ProgressBarControl1.Properties.Maximum = dtLivreurP.Rows.Count
                ProgressBarControl1.Properties.Minimum = 0

                For Each drLivreurP In dtLivreurP.Rows

                    iLivreurID = drLivreurP("LivreurID")


                    '--------------------------------------------
                    ' Cree l'entree de la Commission
                    '--------------------------------------------
                    ' check if Commission exist
                    Dim dtCommissionL As DataTable
                    Dim drCommissionL As DataRow

                    dtCommissionL = oCommissionData.SelectAllByPeriodIDLivreur(IDPeriode, iLivreurID)

                    If dtCommissionL.Rows.Count > 0 Then

                        For Each drCommissionL In dtCommissionL.Rows
                            oCommission.CommissionNumber = drCommissionL("CommissionNumber")
                            oCommission.CommissionDate = drCommissionL("CommissionDate")
                            oCommission.DateFrom = drCommissionL("DateFrom")
                            oCommission.DateTo = drCommissionL("DateTo")
                            oCommission.IDPeriode = drCommissionL("IDPeriode")
                            oCommission.IDCommission = drCommissionL("IDCommission")

                        Next

                    Else
                        ' aucunes Commission existe alors on la cree 
                        oCommission.CommissionNumber = oGenerationCommission.generate_CommissionNumero
                        oCommission.CommissionDate = dDateTo.ToShortDateString
                        oCommission.IDLivreur = iLivreurID
                        oCommission.Livreur = drLivreurP("Livreur")
                        oCommission.DateFrom = tbDateFrom.Text
                        oCommission.DateTo = tbDateTo.Text
                        oCommission.IDPeriode = IDPeriode
                        oCommission.Amount = 0
                        oCommission.TVQ = 0
                        oCommission.TPS = 0
                        oCommission.IsRestaurant = True
                        oCommission.IDCommission = oCommissionData.Add(oCommission)
                    End If


                    ' selectionne tous les organisation affectees a ce vendeur
                    dtorganisation = oOrganisationData.SelectByIDVendeur(iLivreurID)

                    If dtorganisation.Rows.Count > 0 Then


                        ProgressBarControl1.Properties.Step = 1
                        ProgressBarControl1.Properties.PercentView = True
                        ProgressBarControl1.Properties.Maximum = dtorganisation.Rows.Count
                        ProgressBarControl1.Properties.Minimum = 0

                        For Each drOrganisation In dtorganisation.Rows


                            If drOrganisation("OrganisationID") = 46 Then

                                MessageBox.Show("")
                            End If

                            ' selectionne le total de facture pour cette periode pour cette organisation
                            dtCommission = oInvoiceData.SelectAllByPeriodOrganisationID(IDPeriode, drOrganisation("OrganisationID"))
                            If dtCommission.Rows.Count > 0 Then
                                For Each drCommission In dtCommission.Rows

                                    'dblAmount = drCommission("Amount")
                                    'dblPourcentageCommission = drOrganisation("CommissionVendeur")

                                    'dblTotalCommission = dblAmount * dblPourcentageCommission


                                    oCommissionLine.IDCommission = oCommission.IDCommission
                                    oCommissionLine.CommissionNumber = oCommission.CommissionNumber
                                    oCommissionLine.CommissionDate = oCommission.CommissionDate
                                    oCommissionLine.IDLivreur = Nothing
                                    oCommissionLine.Livreur = Nothing
                                    oCommissionLine.OrganisationID = drOrganisation("OrganisationID")
                                    oCommissionLine.Organisation = drOrganisation("Organisation").ToString.Trim
                                    oCommissionLine.Code = Nothing
                                    oCommissionLine.Km = 0
                                    oCommissionLine.Description = drOrganisation("Organisation").ToString.Trim
                                    oCommissionLine.Unite = oImportData.SelectQteeLivraison(go_Globals.PeriodeDateFrom, go_Globals.PeriodeDateTo, drOrganisation("OrganisationID"))

                                    dblTotalCommission = If(IsDBNull(drOrganisation("CommissionVendeur")), 0, drOrganisation("CommissionVendeur"))
                                    oCommissionLine.Prix = dblTotalCommission
                                    'oCommissionLine.Montant = Math.Round(dblTotalCommission, 2)
                                    oCommissionLine.Montant = oCommissionLine.Unite * dblTotalCommission
                                    oCommissionLine.Credit = 0
                                    oCommissionLine.DateFrom = dDateFrom
                                    oCommissionLine.DateTo = dDateTo
                                    oCommissionLine.NombreGlacier = 0
                                    oCommissionLine.Glacier = 0
                                    oCommissionLine.IDPeriode = go_Globals.IDPeriode
                                    oCommissionLine.TypeAjout = 1
                                    oCommissionLine.Ajout = False
                                    oCommissionLine.IsRestaurant = True
                                    oCommissionLineData.Add(oCommissionLine)

                                Next

                            End If
                        Next
                    End If


                    ProgressBarControl1.PerformStep()
                    ProgressBarControl1.Update()

                Next ' NEXT LIVREUR Periode
            End If



            ProgressBarControl1.EditValue = 0

            MessageBox.Show("Génération des commssions Vendeurs complétés avec succes")

            Return True

        Catch ex As Exception
            Me.Cursor = Cursors.Default
            MessageBox.Show(ex.Message, "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Return False
        End Try



    End Function
    Public Function GenererTousLesPaye() As Boolean


        Try


            Dim oPaye As New Paye
            Dim oGenerationPaye As New GenerationPaye
            Dim oLivreur As New livreurs
            Dim dtLivreur As DataTable
            Dim drLivreur As DataRow
            Dim dtLivreurImport As DataTable
            Dim drLivreurImport As DataRow
            Dim dtorganisation As DataTable
            Dim drOrganisation As DataRow
            Dim oLivreurP As New livreurs
            Dim dtLivreurP As DataTable
            Dim drLivreurP As DataRow
            Dim dDateFrom As Date = tbDateFrom.Text
            Dim dDateTo As Date = tbDateTo.Text
            Dim frenchCultureInfo = New Globalization.CultureInfo("fr-CA")
            Dim iOrganisationID As Integer
            Dim iLivreurID As Integer
            Dim sOrganisation As String = ""
            Dim sLivreur As String = ""
            Dim oPaieLigne As New PayeLine
            Dim oPaieLigneGlacier As New PayeLine
            Dim dblMontantGlacierLivreur As Double = 0
            Dim dtRapportFacturePaye As New DataTable
            Dim drRapportFacturePaye As DataRow
            Dim iIDLivreur As Integer = 0
            Dim iIDLivreurString As String = ""
            Dim iIDRapport As Integer = 0
            Dim dtPaye As New DataTable
            Dim strSelectedValue As String = ""
            Dim iSelectedValue As Integer = 0


            Me.Cursor = Cursors.WaitCursor

            Dim oPeriode As New Periodes
            Dim dtPeriode As New DataTable

            If dDateFrom.Month <> dDateTo.Month Then
                oPeriode.Periode = dDateFrom.Day & " " & GetMonthName(dDateFrom, frenchCultureInfo) & "-" & dDateTo.Day & " " & GetMonthName(dDateTo, frenchCultureInfo) & " " & dDateTo.Year.ToString
            Else
                oPeriode.Periode = dDateFrom.Day & "-" & dDateTo.Day & " " & GetMonthName(dDateTo, frenchCultureInfo) & " " & dDateTo.Year.ToString
            End If

            IDPeriode = go_Globals.IDPeriode



            For Each item As DevExpress.XtraEditors.Controls.CheckedListBoxItem In cbPayes.Properties.Items
                If item.CheckState = CheckState.Checked Then
                    strSelectedValue = strSelectedValue & item.Value & ","
                    iSelectedValue = iSelectedValue + 1
                End If
            Next

            If strSelectedValue.ToString.Trim <> "" Then
                strSelectedValue = strSelectedValue.Remove(strSelectedValue.Length - 1)
            End If

            If iSelectedValue = 0 Then
                dtPaye = oPayeData.SelectAllByPeriod(IDPeriode)
                If dtPaye.Rows.Count > 0 Then
                    Dim result As DialogResult = MessageBox.Show("Cette paye a été déjà générée pour cette periode, Voulez-vous la regénerer?", "Facturation", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
                    If result = DialogResult.No Then
                        Return True
                        Exit Function
                        Me.Cursor = Cursors.Default
                    ElseIf result = DialogResult.Yes Then
                        ' supprime les lignes entrees
                        'oPaye.IDPeriode = IDPeriode
                        'oPayeData.DeleteByPeriod(oPaye)

                        Dim oPayeLine As New PayeLine
                        oPayeLine.IDPeriode = IDPeriode
                        oPayeLineData.DeleteByPeriod(oPayeLine)

                        Dim oEpargnePaiementData As New EpargnesPaiementsData
                        Dim clsEpargnePaiement As New EpargnesPaiements
                        clsEpargnePaiement.Periode = IDPeriode
                        oEpargnePaiementData.DeleteParPeriode(clsEpargnePaiement)

                        'Dim oRapportFacture As New RapportFacturePaye
                        'oRapportFacture.IDPeriode = IDPeriode
                        'oRapportFacturePayeData.DeletePeriodL(oRapportFacture)

                    End If
                End If
            End If


            If iSelectedValue > 0 Then
                'iIDRapport = cbPayes.EditValue
                dtRapportFacturePaye = oRapportFacturePayeData.SelectAllByIDs(strSelectedValue)
                If dtRapportFacturePaye.Rows.Count > 0 Then
                    For Each drRapportFacturePaye In dtRapportFacturePaye.Rows

                        iIDLivreur = If(IsDBNull(drRapportFacturePaye("IDLivreur")), 0, CType(drRapportFacturePaye("IDLivreur"), Int32?))
                        iIDLivreurString = iIDLivreurString & iIDLivreur.ToString & ","

                        dDateFrom = drRapportFacturePaye("DateFrom")
                        dDateTo = drRapportFacturePaye("DateTo")

                        Dim oPayeLine As New PayeLine
                        oPayeLine.IDLivreur = iIDLivreur
                        oPayeLine.IDPeriode = IDPeriode
                        oPayeLineData.DeleteByPeriodLivreur(oPayeLine)


                    Next

                    If iIDLivreurString.ToString.Trim <> "" Then
                        iIDLivreurString = iIDLivreurString.Remove(iIDLivreurString.Length - 1)
                    End If

                End If
            End If



            dtLivreurP = oImportData.SelectLivreurAvecPayeString(dDateFrom, dDateTo, iIDLivreurString)
            If dtLivreurP.Rows.Count > 0 Then

                ProgressBarControl1.Properties.Step = 1
                ProgressBarControl1.Properties.PercentView = True
                ProgressBarControl1.Properties.Maximum = dtLivreurP.Rows.Count
                ProgressBarControl1.Properties.Minimum = 0

                For Each drLivreurP In dtLivreurP.Rows

                    iLivreurID = drLivreurP("LivreurID")


                    '----------------------------------------------------------------------------------------
                    ' Selectionne les infos du livreur
                    '----------------------------------------------------------------------------------------
                    dtLivreur = oLivreurData.SelectAllByID(iLivreurID)
                    If dtLivreur.Rows.Count > 0 Then

                        For Each drLivreur In dtLivreur.Rows

                            '--------------------------------------------
                            ' Cree l'entree de la PAIE
                            '--------------------------------------------
                            ' check if paye exist
                            Dim dtPayeL As DataTable
                            Dim drPayeL As DataRow

                            dtPayeL = oPayeData.SelectAllByPeriodIDLivreur(IDPeriode, iLivreurID)

                            If dtPayeL.Rows.Count > 0 Then
                                For Each drPayeL In dtPayeL.Rows
                                    oPaye.PayeNumber = drPayeL("PayeNumber")
                                    oPaye.PayeDate = drPayeL("PayeDate")
                                    oPaye.DateFrom = drPayeL("DateFrom")
                                    oPaye.DateTo = drPayeL("DateTo")
                                    oPaye.IDPeriode = drPayeL("IDPeriode")
                                    oPaye.IDPaye = drPayeL("IDPaye")

                                    'oPayeData.UpdatePayeDate(oPaye, oPaye)
                                Next



                            Else
                                ' aucunes payes existe alors on la cree 
                                oPaye.PayeNumber = oGenerationPaye.generate_PayeNumero
                                oPaye.PayeDate = dDateTo.ToShortDateString
                                oPaye.IDLivreur = iLivreurID
                                oPaye.Livreur = drLivreur("Livreur")
                                oPaye.DateFrom = tbDateFrom.Text
                                oPaye.DateTo = tbDateTo.Text
                                oPaye.IDPeriode = IDPeriode
                                oPaye.Amount = 0
                                oPaye.TVQ = 0
                                oPaye.TPS = 0
                                oPaye.IsCommissionVendeur = False
                                oPaye.IDPaye = oPayeData.Add(oPaye)

                            End If



                            '----------------------------------------------------------------------------------------
                            ' Selectionne tous les livraisons pour la periode groupe par organisation
                            ' SELECT count(importID), OrganisationID,Organisation
                            '----------------------------------------------------------------------------------------
                            'If drLivreur("LivreurID") = 9 Then MessageBox.Show("")
                            dtLivreurImport = oImportData.SelectAllImportbyOrganisationLivreur(dDateFrom, dDateTo, drLivreur("LivreurID"))
                            If dtLivreurImport.Rows.Count > 0 Then

                                For Each drLivreurImport In dtLivreurImport.Rows

                                    dtorganisation = oOrganisationData.SelectByID(drLivreurImport("OrganisationID"))
                                    If dtorganisation.Rows.Count > 0 Then


                                        For Each drOrganisation In dtorganisation.Rows

                                            iOrganisationID = drOrganisation("OrganisationID")
                                            sOrganisation = drOrganisation("Organisation")
                                            iLivreurID = drLivreur("LivreurID")
                                            sLivreur = drLivreur("Livreur")


                                            Select Case drOrganisation("MetodeCalculationLivreur")
                                                Case 0 ' Pharmaplus / Massicote.
                                                    If oGenerationPaye.CalculerPharmaplusMassicote(drOrganisation, dDateFrom, dDateTo, drLivreur, oPaye) = False Then
                                                        Me.Cursor = Cursors.Default
                                                        MessageBox.Show("Un problème est survenu pendant la génération de la paye Pharmaplus/Massicote, Veuillez contacter votre administrateur", "Arrêt de Facturation", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1)
                                                        Return False
                                                        Exit Function
                                                    End If

                                                Case 1 ' Prix fixe.
                                                    If oGenerationPaye.PrixFixe(drOrganisation, dDateFrom, dDateTo, drLivreur, oPaye) = False Then
                                                        Me.Cursor = Cursors.Default
                                                        MessageBox.Show("Un problème est survenu pendant la génération de la paye a prix fixe, Veuillez contacter votre administrateur", "Arrêt de Facturation", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1)
                                                        Return False
                                                        Exit Function

                                                    End If

                                                Case 2 ' Par Kilométrage.
                                                    If oGenerationPaye.ParKilometrage(drOrganisation, dDateFrom, dDateTo, drLivreur, oPaye) = False Then
                                                        Me.Cursor = Cursors.Default
                                                        MessageBox.Show("Un problème est survenu pendant la génération de la paye par Kilométrage, Veuillez contacter votre administrateur", "Arrêt de Facturation", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1)
                                                        Return False
                                                        Exit Function

                                                    End If

                                                Case 3 ' Par Kilométrage + Xtra
                                                    If oGenerationPaye.ParKilometrageXtra(drOrganisation, dDateFrom, dDateTo, drLivreur, oPaye) = False Then
                                                        Me.Cursor = Cursors.Default
                                                        MessageBox.Show("Un problème est survenu pendant la génération de la paye par Kilométrage + Xtra, Veuillez contacter votre administrateur", "Arrêt de Facturation", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1)
                                                        Return False
                                                        Exit Function
                                                    End If

                                                Case 4 ' Par Kilométrage + Xtra(Heure).
                                                    If oGenerationPaye.ParKilometrageXtraHeure(drOrganisation, dDateFrom, dDateTo, drLivreur, oPaye) = False Then
                                                        Me.Cursor = Cursors.Default
                                                        MessageBox.Show("Un problème est survenu pendant la génération de la paye par Kilométrage + Xtra(Heure), Veuillez contacter votre administrateur", "Arrêt de Facturation", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1)
                                                        Return False
                                                        Exit Function
                                                    End If

                                                Case 5 ' Par Nombre KM fixe + extra.
                                                    If oGenerationPaye.ParNombreKMFixeExtra(drOrganisation, dDateFrom, dDateTo, drLivreur, oPaye) = False Then
                                                        Me.Cursor = Cursors.Default
                                                        MessageBox.Show("Un problème est survenu pendant la génération de la paye par KM fixe + extra, Veuillez contacter votre administrateur", "Arrêt de Facturation", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1)
                                                        Return False
                                                        Exit Function

                                                    End If

                                                Case 6 ' Par Jour.
                                                    If oGenerationPaye.ParJour(drOrganisation, dDateFrom, dDateTo, drLivreur, oPaye) = False Then
                                                        Me.Cursor = Cursors.Default
                                                        MessageBox.Show("Un problème est survenu pendant la génération de la paye par jour, Veuillez contacter votre administrateur", "Arrêt de Facturation", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1)
                                                        Return False
                                                        Exit Function

                                                    End If

                                                Case 7 ' Par heure.
                                                    If oGenerationPaye.ParHeure(drOrganisation, dDateFrom, dDateTo, drLivreur, oPaye) = False Then
                                                        Me.Cursor = Cursors.Default
                                                        MessageBox.Show("Un problème est survenu pendant la génération de la paye par jour, Veuillez contacter votre administrateur", "Arrêt de Facturation", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1)
                                                        Return False
                                                        Exit Function

                                                    End If

                                                Case 8 ' Par residence.
                                                    If oGenerationPaye.ParResidence(drOrganisation, dDateFrom, dDateTo, drLivreur, oPaye) = False Then
                                                        Me.Cursor = Cursors.Default
                                                        MessageBox.Show("Un problème est survenu pendant la génération de la paye par jour, Veuillez contacter votre administrateur", "Arrêt de Facturation", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1)
                                                        Return False
                                                        Exit Function

                                                    End If



                                            End Select
                                        Next
                                    End If
                                Next
                            End If







                            '----------------------------------------------------------------------------------------
                            ' Ajouter Extras ici pour toutes les organisations
                            '----------------------------------------------------------------------------------------
                            Dim dtExtra As DataTable
                            Dim drExtra As DataRow
                            Dim dblTotalExtraKM As Double = 0
                            Dim dblTotalExtra As Double = 0
                            Dim dblTotalExtraCount As Integer = 0


                            dtExtra = oImportData.SelectExtraByDateLivreur(dDateFrom, dDateTo, iOrganisationID, iLivreurID)
                            If dtExtra.Rows.Count > 0 Then
                                For Each drExtra In dtExtra.Rows
                                    dblTotalExtra = dblTotalExtra + If(IsDBNull(drExtra("TotalExtra")), 0, CType(drExtra("TotalExtra"), Decimal?))
                                    dblTotalExtraKM = dblTotalExtraKM + If(IsDBNull(drExtra("TotalExtraKM")), 0, drExtra("TotalExtraKM"))
                                    dblTotalExtraCount = dblTotalExtraCount + If(IsDBNull(drExtra("TotalExtraCount")), 0, drExtra("TotalExtraCount"))
                                Next

                                If dblTotalExtra > 0 Then
                                    oPaieLigne.IDPaye = oPaye.IDPaye
                                    oPaieLigne.PayeNumber = oPaye.PayeNumber
                                    oPaieLigne.PayeDate = oPaye.PayeDate
                                    oPaieLigne.IDLivreur = iLivreurID
                                    oPaieLigne.Livreur = sLivreur.ToString.Trim
                                    oPaieLigne.OrganisationID = Nothing
                                    oPaieLigne.Organisation = Nothing
                                    oPaieLigne.Code = Nothing
                                    oPaieLigne.Km = Math.Round(dblTotalExtraKM, 1)
                                    oPaieLigne.Description = "Extras"
                                    oPaieLigne.Unite = 1
                                    oPaieLigne.Prix = dblTotalExtra
                                    oPaieLigne.Montant = dblTotalExtra
                                    oPaieLigne.Credit = 0
                                    oPaieLigne.DateFrom = dDateFrom
                                    oPaieLigne.DateTo = dDateTo
                                    oPaieLigne.NombreGlacier = 0
                                    oPaieLigne.Glacier = 0
                                    oPaieLigne.IDPeriode = go_Globals.IDPeriode
                                    oPaieLigne.TypeAjout = 4
                                    oPaieLigne.Ajout = False
                                    oPaieLigne.IsCommissionVendeur = False
                                    oPayeLineData.Add(oPaieLigne)
                                End If
                            End If



                            '----------------------------------------------------------------------------------------
                            ' Cadeau IDS
                            '----------------------------------------------------------------------------------------
                            Dim dblTotalCadeau As Double = 0

                            dblTotalCadeau = oCadeauLivreurData.SelectTotalAmount(dDateFrom, dDateTo, iLivreurID)
                            If dblTotalCadeau > 0 Then

                                oPaieLigne.IDPaye = oPaye.IDPaye
                                oPaieLigne.PayeNumber = oPaye.PayeNumber
                                oPaieLigne.PayeDate = oPaye.PayeDate
                                oPaieLigne.IDLivreur = iLivreurID
                                oPaieLigne.Livreur = sLivreur.ToString.Trim
                                oPaieLigne.OrganisationID = Nothing
                                oPaieLigne.Organisation = Nothing
                                oPaieLigne.Code = Nothing
                                oPaieLigne.Km = 0
                                oPaieLigne.Description = "Cadeau IDS"
                                oPaieLigne.Unite = 1
                                oPaieLigne.Prix = dblTotalCadeau
                                oPaieLigne.Montant = dblTotalCadeau
                                oPaieLigne.Credit = 0
                                oPaieLigne.DateFrom = dDateFrom
                                oPaieLigne.DateTo = dDateTo
                                oPaieLigne.NombreGlacier = 0
                                oPaieLigne.Glacier = 0
                                oPaieLigne.IDPeriode = go_Globals.IDPeriode
                                oPaieLigne.TypeAjout = 3
                                oPaieLigne.Ajout = False
                                oPaieLigne.IsCommissionVendeur = False
                                oPayeLineData.Add(oPaieLigne)
                            End If




                            '----------------------------------------------------------------------------------------
                            ' Ajouter  fixe de fin de semaine
                            '----------------------------------------------------------------------------------------
                            Dim dtPrixFixe As DataTable
                            Dim drPrixFixe As DataRow
                            Dim oPrixFixe As New PrixFixe
                            Dim oPrixFixeData As New PrixFixeData
                            Dim dblPrixFixe As Double = 0

                            dtPrixFixe = oPrixFixeData.SelectByIDLivreur(iLivreurID)
                            If dtPrixFixe.Rows.Count > 0 Then

                                For Each drPrixFixe In dtPrixFixe.Rows

                                    Dim oOrganisation As New Organisations
                                    Dim oOrganisationSel As New Organisations
                                    oOrganisation.OrganisationID = drPrixFixe("IDOrganisation")
                                    oOrganisation.Organisation = drPrixFixe("Organisation").ToString.Trim
                                    oOrganisationSel = oOrganisationData.Select_Record(oOrganisation)

                                    dblPrixFixe = If(IsDBNull(drPrixFixe("MontantLivreur")), 0, CType(drPrixFixe("MontantLivreur"), Decimal?))

                                    oPaieLigne.IDPaye = oPaye.IDPaye
                                    oPaieLigne.PayeNumber = oPaye.PayeNumber
                                    oPaieLigne.PayeDate = oPaye.PayeDate
                                    oPaieLigne.IDLivreur = iLivreurID
                                    oPaieLigne.Livreur = sLivreur.ToString.Trim
                                    oPaieLigne.OrganisationID = oOrganisation.OrganisationID
                                    oPaieLigne.Organisation = oOrganisation.Organisation.ToString.Trim
                                    oPaieLigne.Code = Nothing
                                    oPaieLigne.Km = 0
                                    oPaieLigne.Description = oOrganisation.Organisation.ToString.Trim & "/ Fin de semaine"
                                    oPaieLigne.Unite = 1
                                    oPaieLigne.Prix = dblPrixFixe
                                    oPaieLigne.Montant = dblPrixFixe
                                    oPaieLigne.Credit = 0
                                    oPaieLigne.DateFrom = dDateFrom
                                    oPaieLigne.DateTo = dDateTo
                                    oPaieLigne.NombreGlacier = 0
                                    oPaieLigne.Glacier = 0
                                    oPaieLigne.IDPeriode = go_Globals.IDPeriode
                                    oPaieLigne.TypeAjout = 4
                                    oPaieLigne.Ajout = False
                                    oPaieLigne.IsCommissionVendeur = False
                                    oPayeLineData.Add(oPaieLigne)
                                Next


                            End If


                            '----------------------------------------------------------------------------------------
                            ' Enlever le montant de pret si un pret existe avec une balance
                            '----------------------------------------------------------------------------------------

                            Dim dtPret As DataTable
                            Dim drPret As DataRow
                            Dim dtPretPayment As DataTable
                            Dim oPret As New Prets
                            Dim oPretPayment As New PretsPaiements
                            Dim iTotalPaiementCount As Integer = 0

                            ' check if livreur has a pret going on
                            dtPret = oPretsData.SelectAllByLivreur(iLivreurID)

                            If dtPret.Rows.Count > 0 Then
                                For Each drPret In dtPret.Rows


                                    ' check if payment has been done on this pret for this periode
                                    dtPretPayment = oPretsPaiementsData.SelectAllPaymentsForPeriode(drPret("IDPret"), iLivreurID, go_Globals.IDPeriode)
                                    iTotalPaiementCount = oPretsPaiementsData.SelectAllPaymentsCount(drPret("IDPret"), iLivreurID)

                                    If Not dtPretPayment.Rows.Count > 0 Then
                                        If (iTotalPaiementCount < drPret("NombreSemaine")) And (drPret("DatePret") <= DateTime.Now.ToShortDateString) Then

                                            'add a payment to Payment table
                                            oPretPayment.IDPret = drPret("IDPret")
                                            oPretPayment.IDLivreur = iLivreurID
                                            oPretPayment.Livreur = sLivreur.ToString.Trim
                                            oPretPayment.Periode = go_Globals.IDPeriode
                                            oPretPayment.DatePaiement = DateTime.Now
                                            oPretPayment.Paiement = drPret("PaiementParPeriode")
                                            oPretPayment.Periode = go_Globals.IDPeriode
                                            oPretsPaiementsData.Add(oPretPayment)

                                            ' add amount as credit on the paye (remove amount from the paye
                                            oPaieLigne.IDPaye = oPaye.IDPaye
                                            oPaieLigne.PayeNumber = oPaye.PayeNumber
                                            oPaieLigne.PayeDate = oPaye.PayeDate
                                            oPaieLigne.IDLivreur = iLivreurID
                                            oPaieLigne.Livreur = sLivreur.ToString.Trim
                                            oPaieLigne.OrganisationID = Nothing
                                            oPaieLigne.Organisation = Nothing
                                            oPaieLigne.Code = Nothing
                                            oPaieLigne.Km = 0
                                            oPaieLigne.Description = "Paiement sur le pret # " & drPret("IDPret").ToString
                                            oPaieLigne.Unite = 1
                                            oPaieLigne.Prix = drPret("PaiementParPeriode")
                                            oPaieLigne.Montant = drPret("PaiementParPeriode")
                                            oPaieLigne.Credit = 0
                                            oPaieLigne.DateFrom = dDateFrom
                                            oPaieLigne.DateTo = dDateTo
                                            oPaieLigne.NombreGlacier = 0
                                            oPaieLigne.Glacier = 0
                                            oPaieLigne.IDPeriode = go_Globals.IDPeriode
                                            oPaieLigne.TypeAjout = 2
                                            oPaieLigne.Ajout = False
                                            oPaieLigne.IsCommissionVendeur = False
                                            oPayeLineData.Add(oPaieLigne)

                                            ' Update existing balance with new balance after this paiement.
                                            oPret.IDPret = drPret("IDPret")
                                            oPret.BalancePret = drPret("BalancePret") - drPret("PaiementParPeriode")
                                            oPretsData.UpdateBalance(oPret, oPret)


                                        End If

                                    Else

                                        ' add amount as credit on the paye (remove amount from the paye
                                        oPaieLigne.IDPaye = oPaye.IDPaye
                                        oPaieLigne.PayeNumber = oPaye.PayeNumber
                                        oPaieLigne.PayeDate = oPaye.PayeDate
                                        oPaieLigne.IDLivreur = iLivreurID
                                        oPaieLigne.Livreur = sLivreur.ToString.Trim
                                        oPaieLigne.OrganisationID = Nothing
                                        oPaieLigne.Organisation = Nothing
                                        oPaieLigne.Code = Nothing
                                        oPaieLigne.Km = 0
                                        oPaieLigne.Description = "Paiement sur le pret # " & drPret("IDPret").ToString
                                        oPaieLigne.Unite = 1
                                        oPaieLigne.Prix = drPret("PaiementParPeriode")
                                        oPaieLigne.Montant = drPret("PaiementParPeriode")
                                        oPaieLigne.Credit = 0
                                        oPaieLigne.DateFrom = dDateFrom
                                        oPaieLigne.DateTo = dDateTo
                                        oPaieLigne.NombreGlacier = 0
                                        oPaieLigne.Glacier = 0
                                        oPaieLigne.IDPeriode = go_Globals.IDPeriode
                                        oPaieLigne.TypeAjout = 2
                                        oPaieLigne.Ajout = False
                                        oPaieLigne.IsCommissionVendeur = False
                                        oPayeLineData.Add(oPaieLigne)


                                    End If

                                Next
                            End If





                            '----------------------------------------------------------------------------------------
                            ' Enlever le montant de epargne si un pret epargne avec une balance
                            '----------------------------------------------------------------------------------------

                            Dim dtEpargne As DataTable
                            Dim drEpargne As DataRow
                            Dim dtEpargnePayment As DataTable
                            Dim oEpargnes As New Epargnes
                            Dim oEpargnesData As New EpargnesData
                            Dim oEpargnesPaiements As New EpargnesPaiements
                            Dim oEpargnesPaiementsData As New EpargnesPaiementsData
                            Dim iTotalEpargneCount As Integer = 0

                            ' check if livreur has a pret going on
                            dtEpargne = oEpargneData.SelectAllByLivreur(iLivreurID)

                            If dtEpargne.Rows.Count > 0 Then
                                For Each drEpargne In dtEpargne.Rows

                                    If drEpargne("EpargneActif") = True And drEpargne("EpargneParPeriode") > 0 Then
                                        ' check if payment has been done on this pret for this periode
                                        dtEpargnePayment = oEpargnesPaiementsData.SelectAllEpargneForPeriode(drEpargne("IDEpargne"), iLivreurID, go_Globals.IDPeriode)
                                        If Not dtEpargnePayment.Rows.Count > 0 Then
                                            If drEpargne("DateEpargne") <= DateTime.Now.ToShortDateString Then

                                                'add a payment to Payment table
                                                oEpargnesPaiements.IDEpargne = drEpargne("IDEpargne")
                                                oEpargnesPaiements.IDLivreur = iLivreurID
                                                oEpargnesPaiements.Livreur = sLivreur.ToString.Trim
                                                oEpargnesPaiements.Periode = go_Globals.IDPeriode
                                                oEpargnesPaiements.DateEpargne = DateTime.Now
                                                oEpargnesPaiements.Epargne = drEpargne("EpargneParPeriode")
                                                oEpargnesPaiements.Periode = go_Globals.IDPeriode
                                                oEpargnesPaiementsData.Add(oEpargnesPaiements)

                                                Dim dblBalance As Double = 0
                                                If Not IsNothing(drEpargne("Balance")) Then
                                                    dblBalance = drEpargne("Balance")
                                                Else
                                                    dblBalance = 0
                                                End If

                                                ' Update existing balance with new balance after this paiement.
                                                oEpargnes.IDEpargne = drEpargne("IDEpargne")
                                                oEpargnes.Balance = dblBalance + oEpargnesPaiements.Epargne
                                                oEpargnesData.UpdateBalance(oEpargnes, oEpargnes)

                                                ' add amount as credit on the paye (remove amount from the paye
                                                oPaieLigne.IDPaye = oPaye.IDPaye
                                                oPaieLigne.PayeNumber = oPaye.PayeNumber
                                                oPaieLigne.PayeDate = oPaye.PayeDate
                                                oPaieLigne.IDLivreur = iLivreurID
                                                oPaieLigne.Livreur = sLivreur.ToString.Trim
                                                oPaieLigne.OrganisationID = Nothing
                                                oPaieLigne.Organisation = Nothing
                                                oPaieLigne.Code = Nothing
                                                oPaieLigne.Km = 0
                                                oPaieLigne.Description = "Epargne sur l'epargne # " & drEpargne("IDEpargne").ToString
                                                oPaieLigne.Unite = 1
                                                oPaieLigne.Prix = drEpargne("EpargneParPeriode")
                                                oPaieLigne.Montant = drEpargne("EpargneParPeriode")
                                                oPaieLigne.Credit = 0
                                                oPaieLigne.DateFrom = dDateFrom
                                                oPaieLigne.DateTo = dDateTo
                                                oPaieLigne.NombreGlacier = 0
                                                oPaieLigne.Glacier = 0
                                                oPaieLigne.IDPeriode = go_Globals.IDPeriode
                                                oPaieLigne.TypeAjout = 2
                                                oPaieLigne.Ajout = False
                                                oPaieLigne.IsCommissionVendeur = False
                                                oPayeLineData.Add(oPaieLigne)



                                            End If



                                        Else

                                            ' add amount as credit on the paye (remove amount from the paye
                                            oPaieLigne.IDPaye = oPaye.IDPaye
                                            oPaieLigne.PayeNumber = oPaye.PayeNumber
                                            oPaieLigne.PayeDate = oPaye.PayeDate
                                            oPaieLigne.IDLivreur = iLivreurID
                                            oPaieLigne.Livreur = sLivreur.ToString.Trim
                                            oPaieLigne.OrganisationID = Nothing
                                            oPaieLigne.Organisation = Nothing
                                            oPaieLigne.Code = Nothing
                                            oPaieLigne.Km = 0
                                            oPaieLigne.Description = "Epargne sur l'epargne # " & drEpargne("IDEpargne").ToString
                                            oPaieLigne.Unite = 1
                                            oPaieLigne.Prix = drEpargne("EpargneParPeriode")
                                            oPaieLigne.Montant = drEpargne("EpargneParPeriode")
                                            oPaieLigne.Credit = 0
                                            oPaieLigne.DateFrom = dDateFrom
                                            oPaieLigne.DateTo = dDateTo
                                            oPaieLigne.NombreGlacier = 0
                                            oPaieLigne.Glacier = 0
                                            oPaieLigne.IDPeriode = go_Globals.IDPeriode
                                            oPaieLigne.TypeAjout = 2
                                            oPaieLigne.Ajout = False
                                            oPaieLigne.IsCommissionVendeur = False
                                            oPayeLineData.Add(oPaieLigne)


                                        End If

                                    End If

                                Next
                            End If



                            '----------------------------------------------------------------------------------------
                            ' Ajoute le retrait si y en a un
                            '----------------------------------------------------------------------------------------

                            Dim dtEpargneR As DataTable
                            Dim drEpargneR As DataRow
                            Dim dtEpargneRetraitsR As DataTable
                            Dim oEpargnesR As New Epargnes
                            Dim oEpargnesDataR As New EpargnesData
                            Dim oEpargnesRetraitsR As New EpargnesRetraits
                            Dim oEpargnesRetraitsDataR As New EpargnesRetraitsData
                            Dim iTotalEpargneCountR As Integer = 0

                            ' check if livreur has a pret going on
                            dtEpargneRetraitsR = oEpargnesRetraitsDataR.SelectAllByLivreurIDPeriode(iLivreurID, go_Globals.IDPeriode)

                            If dtEpargneRetraitsR.Rows.Count > 0 Then
                                For Each drEpargneR In dtEpargneRetraitsR.Rows


                                    ' add amount as credit on the paye (remove amount from the paye
                                    oPaieLigne.IDPaye = oPaye.IDPaye
                                    oPaieLigne.PayeNumber = oPaye.PayeNumber
                                    oPaieLigne.PayeDate = oPaye.PayeDate
                                    oPaieLigne.IDLivreur = iLivreurID
                                    oPaieLigne.Livreur = sLivreur.ToString.Trim
                                    oPaieLigne.OrganisationID = Nothing
                                    oPaieLigne.Organisation = Nothing
                                    oPaieLigne.Code = Nothing
                                    oPaieLigne.Km = 0
                                    oPaieLigne.Description = "Paiment sur l'epargne"
                                    oPaieLigne.Unite = 1
                                    oPaieLigne.Prix = drEpargneR("Retrait")
                                    oPaieLigne.Montant = drEpargneR("Retrait")
                                    oPaieLigne.Credit = 0
                                    oPaieLigne.DateFrom = dDateFrom
                                    oPaieLigne.DateTo = dDateTo
                                    oPaieLigne.NombreGlacier = 0
                                    oPaieLigne.Glacier = 0
                                    oPaieLigne.IDPeriode = go_Globals.IDPeriode
                                    oPaieLigne.TypeAjout = 1
                                    oPaieLigne.Ajout = False
                                    oPaieLigne.IsCommissionVendeur = False
                                    oPayeLineData.Add(oPaieLigne)
                                Next
                            End If





                        Next ' NEXT LIVREUR Detail
                    End If

                    ProgressBarControl1.PerformStep()
                    ProgressBarControl1.Update()

                Next ' NEXT LIVREUR Periode
            End If



            ProgressBarControl1.EditValue = 0
            grdImport.DataSource = oImportData.SelectAllFromDate(tbDateFrom.Text, tbDateTo.Text)

            GridView1.OptionsView.ColumnAutoWidth = False

            Dim viewInfo As DevExpress.XtraGrid.Views.Grid.ViewInfo.GridViewInfo = TryCast(GridView1.GetViewInfo(), GridViewInfo)
            If viewInfo.ViewRects.ColumnTotalWidth < viewInfo.ViewRects.ColumnPanelWidth Then
                GridView1.OptionsView.ColumnAutoWidth = True
            End If




            Return True

        Catch ex As Exception
            Me.Cursor = Cursors.Default
            MessageBox.Show(ex.Message, "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Return False
        End Try



    End Function

    Private Sub bGenererPaye_Click(sender As Object, e As EventArgs)

        Dim iTotalCalcul As Integer = 0

        '----------------------------------------------------------------------------------------
        ' Verifie que tous les methodes de calculs ont ete referencer dans le systeme
        '----------------------------------------------------------------------------------------

        iTotalCalcul = oOrganisationData.SelectAllMetodeCalculationOrganisation()

        If iTotalCalcul > 0 Then
            Me.Cursor = Cursors.Default
            MessageBox.Show("il y a " & (iTotalCalcul) & " organisations qui n'ont pas de méthode de calcul, Veuillez affecter une méthode de calcul a toutes les organisations")
            Exit Sub
        End If


        iTotalCalcul = oOrganisationData.SelectAllMetodeCalculationLivreur()

        If iTotalCalcul > 0 Then
            Me.Cursor = Cursors.Default
            MessageBox.Show("il y a " & (iTotalCalcul) & " Livreur(s) qui n'ont pas de méthode de calcul, Veuillez affecter une méthode de calcul a toutes les Livreurs")
            Exit Sub
        End If


        '----------------------------------------------------------------------------------------
        ' Facturation et paye generation
        '----------------------------------------------------------------------------------------

        Dim result As DialogResult = MessageBox.Show("Générer la facturation et la paie?", "Facturation/Paie", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
        If result = DialogResult.No Then
            Exit Sub
        ElseIf result = DialogResult.Yes Then

            If GenererToutesLesFactures() = False Then
                Exit Sub
            End If


            If GenererTousLesPaye() = False Then
                Exit Sub
            End If


            Me.Cursor = Cursors.Default
            Dim sMessage As String = "Génération complété avec succès?" & vbCrLf & "Désirez-vous générer les factures et les payes maintenant?"

            Dim result2 As DialogResult = MessageBox.Show(sMessage, "Facturation/Paie", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
            If result2 = DialogResult.No Then
                Exit Sub
            ElseIf result2 = DialogResult.Yes Then
                If ImprimerLesFactures() = False Then
                    Exit Sub
                End If

                If ImprimerLaPaie() = False Then
                    Exit Sub
                End If
            End If
        End If



    End Sub



    Public Function GenererToutesLesFactures() As Boolean


        Dim dtorganisation As DataTable
        Dim drOrganisation As DataRow
        Dim dDateFrom As Date = tbDateFrom.Text
        Dim dDateTo As Date = tbDateTo.Text
        Dim oFacturation As New Facturation
        Dim dtOrganisationCheck As New DataTable
        Dim iTotalCalcul As Integer = 0
        Dim oInvoiceData As New InvoiceData
        Dim dtorganisationO As DataTable
        Dim drOrganisationO As DataRow
        Dim oPeriode As New Periodes
        Dim dtPeriode As New DataTable
        Dim frenchCultureInfo = New Globalization.CultureInfo("fr-CA")
        Dim iIDRapport As Integer = 0
        Dim dtRapportFacturePaye As New DataTable
        Dim drRapportFacturePaye As DataRow
        Dim iIDOrganisation As Integer = 0
        Dim iIDOrganisationString As String = ""

        Dim dtInvoice As New DataTable
        Dim strSelectedValue As String = ""
        Dim iSelectedValue As Integer = 0
        Try


            If dDateFrom.Month <> dDateTo.Month Then
                oPeriode.Periode = dDateFrom.Day & " " & GetMonthName(dDateFrom, frenchCultureInfo) & "-" & dDateTo.Day & " " & GetMonthName(dDateTo, frenchCultureInfo) & " " & dDateTo.Year.ToString
            Else
                oPeriode.Periode = dDateFrom.Day & "-" & dDateTo.Day & " " & GetMonthName(dDateTo, frenchCultureInfo) & " " & dDateTo.Year.ToString
            End If

            My.Computer.FileSystem.CreateDirectory(go_Globals.LocationFacture)

            go_Globals.TotalEchange305 = 0
            go_Globals.TotalEchange35 = 0


            For Each item As DevExpress.XtraEditors.Controls.CheckedListBoxItem In cbFactures.Properties.Items
                If item.CheckState = CheckState.Checked Then
                    strSelectedValue = strSelectedValue & item.Value & ","
                    iSelectedValue = iSelectedValue + 1
                End If
            Next

            If strSelectedValue.ToString.Trim <> "" Then
                strSelectedValue = strSelectedValue.Remove(strSelectedValue.Length - 1)
            End If



            If iSelectedValue = 0 Then
                'check if this period has already been generated
                dtInvoice = oInvoiceData.SelectAllByPeriod(IDPeriode)
                If dtInvoice.Rows.Count > 0 Then
                    Dim result As DialogResult = MessageBox.Show("La facturation a déjà été générée pour cette periode, Voulez-vous la regénerer?", "Facturation", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
                    If result = DialogResult.No Then
                        Return True
                        Exit Function
                        Me.Cursor = Cursors.Default
                    ElseIf result = DialogResult.Yes Then

                        Dim oInvoiceLine As New InvoiceLine
                        oInvoiceLine.IDPeriode = IDPeriode
                        oInvoiceLineData.DeleteByPeriodO(oInvoiceLine)

                        'Dim oRapportFacture As New RapportFacturePaye
                        'oRapportFacture.IDPeriode = IDPeriode
                        'oRapportFacturePayeData.DeletePeriodO(oRapportFacture)

                    End If
                End If
            End If




            Me.Cursor = Cursors.WaitCursor

            If iSelectedValue > 0 Then
                'iIDRapport = cbFactures.EditValue
                dtRapportFacturePaye = oRapportFacturePayeData.SelectAllByID(strSelectedValue)
                If dtRapportFacturePaye.Rows.Count > 0 Then
                    For Each drRapportFacturePaye In dtRapportFacturePaye.Rows
                        iIDOrganisation = If(IsDBNull(drRapportFacturePaye("IDOrganisation")), 0, CType(drRapportFacturePaye("IDOrganisation"), Int32?))


                        iIDOrganisationString = iIDOrganisationString & iIDOrganisation.ToString & ","

                        dDateFrom = drRapportFacturePaye("DateFrom")
                        dDateTo = drRapportFacturePaye("DateTo")

                        ' supprime les lignes entrees pour cette organisations

                        Dim oInvoiceLine As New InvoiceLine
                        oInvoiceLine.OrganisationID = iIDOrganisation
                        oInvoiceLine.IDPeriode = IDPeriode
                        oInvoiceLineData.DeleteByPeriodOrganisation(oInvoiceLine)

                    Next


                    If iIDOrganisationString.ToString.Trim <> "" Then
                        iIDOrganisationString = iIDOrganisationString.Remove(iIDOrganisationString.Length - 1)
                    End If


                End If
            End If



            dtorganisationO = oImportData.SelectOrganisationsAvecFactureString(dDateFrom, dDateTo, iIDOrganisationString)
            If dtorganisationO.Rows.Count > 0 Then

                ProgressBarControl1.Properties.Step = 1
                ProgressBarControl1.Properties.PercentView = True
                ProgressBarControl1.Properties.Maximum = dtorganisationO.Rows.Count
                ProgressBarControl1.Properties.Minimum = 0



                For Each drOrganisationO In dtorganisationO.Rows


                    '--------------------------------------------
                    ' Calcul totaux de paye par Organisations (Type de calculs)
                    '--------------------------------------------
                    dtorganisation = oOrganisationData.SelectByID(drOrganisationO("OrganisationID"))


                    For Each drOrganisation In dtorganisation.Rows

                        Application.DoEvents()

                        Select Case drOrganisation("MetodeCalculationOrganisation")
                            Case 0 ' Pharmaplus / Massicote.
                                If oFacturation.CalculerPharmaplusMassicote(drOrganisation, dDateFrom, dDateTo) = False Then
                                    Me.Cursor = Cursors.Default
                                    MessageBox.Show("Un problème est survenu pendant la génération de la facturation Pharmaplus / Massicote, Veuillez contacter votre administrateur", "Arrêt de Facturation", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1)
                                    Exit Function
                                End If

                            Case 1 ' Prix fixe.
                                If oFacturation.PrixFixe(drOrganisation, dDateFrom, dDateTo) = False Then
                                    Me.Cursor = Cursors.Default
                                    MessageBox.Show("Un problème est survenu pendant la génération de la facturation a prix fixe, Veuillez contacter votre administrateur", "Arrêt de Facturation", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1)
                                    Exit Function
                                End If

                            Case 2 ' Par Kilométrage.
                                If oFacturation.ParKilometrage(drOrganisation, dDateFrom, dDateTo) = False Then
                                    Me.Cursor = Cursors.Default
                                    MessageBox.Show("Un problème est survenu pendant la génération de la facturation par Kilométrage, Veuillez contacter votre administrateur", "Arrêt de Facturation", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1)
                                    Exit Function
                                End If

                            Case 3 ' Par Kilométrage + Xtra.
                                If oFacturation.ParKilometrageXtra(drOrganisation, dDateFrom, dDateTo) = False Then
                                    Me.Cursor = Cursors.Default
                                    MessageBox.Show("Un problème est survenu pendant la génération de la facturation par Kilométrage + Xtra, Veuillez contacter votre administrateur", "Arrêt de Facturation", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1)
                                    Exit Function
                                End If


                            Case 4 ' Par Kilométrage + Xtra(Heure).
                                If oFacturation.ParKilometrageXtraHeure(drOrganisation, dDateFrom, dDateTo) = False Then
                                    Me.Cursor = Cursors.Default
                                    MessageBox.Show("Un problème est survenu pendant la génération de la facturation par Kilométrage + Xtra(Heure), Veuillez contacter votre administrateur", "Arrêt de Facturation", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1)
                                    Exit Function
                                End If

                            Case 5 ' Par Nombre KM fixe + extra.
                                If oFacturation.ParNombreKMFixeExtra(drOrganisation, dDateFrom, dDateTo) = False Then
                                    Me.Cursor = Cursors.Default
                                    MessageBox.Show("Un problème est survenu pendant la génération de la facturation par Nombre KM fixe + extra, Veuillez contacter votre administrateur", "Arrêt de Facturation", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1)
                                    Exit Function
                                End If

                            Case 6 ' Par Jour.
                                If oFacturation.ParJour(drOrganisation, dDateFrom, dDateTo) = False Then
                                    Me.Cursor = Cursors.Default
                                    MessageBox.Show("Un problème est survenu pendant la génération de la facturation par jour, Veuillez contacter votre administrateur", "Arrêt de Facturation", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1)
                                    Exit Function
                                End If



                            Case 6 ' Par Jour.
                                If oFacturation.ParJour(drOrganisation, dDateFrom, dDateTo) = False Then
                                    Me.Cursor = Cursors.Default
                                    MessageBox.Show("Un problème est survenu pendant la génération de la facturation par jour, Veuillez contacter votre administrateur", "Arrêt de Facturation", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1)
                                    Exit Function
                                End If


                            Case 7 ' Par heure.
                                If oFacturation.ParHeure(drOrganisation, dDateFrom, dDateTo) = False Then
                                    Me.Cursor = Cursors.Default
                                    MessageBox.Show("Un problème est survenu pendant la génération de la facturation par heure, Veuillez contacter votre administrateur", "Arrêt de Facturation", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1)
                                    Exit Function
                                End If


                            Case 8 ' Par heure.
                                If oFacturation.PrixFixeAvecExtras(drOrganisation, dDateFrom, dDateTo) = False Then
                                    Me.Cursor = Cursors.Default
                                    MessageBox.Show("Un problème est survenu pendant la génération de la facturation Prix Fixe + Extras, Veuillez contacter votre administrateur", "Arrêt de Facturation", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1)
                                    Exit Function
                                End If


                            Case 9 ' Par residence.
                                If oFacturation.ParResidence(drOrganisation, dDateFrom, dDateTo) = False Then
                                    Me.Cursor = Cursors.Default
                                    MessageBox.Show("Un problème est survenu pendant la génération de la facturation Prix Fixe + Extras, Veuillez contacter votre administrateur", "Arrêt de Facturation", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1)
                                    Exit Function
                                End If


                        End Select

                    Next

                    ProgressBarControl1.PerformStep()
                    ProgressBarControl1.Update()

                Next


            End If


            ProgressBarControl1.EditValue = 0

            Return True


        Catch ex As Exception
            Me.Cursor = Cursors.Default
            MessageBox.Show(ex.Message, "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Return False
        End Try




    End Function
    Private Function GetMonthNameNumber(d As Integer) As String

        Dim sMonth As String = ""


        Select Case d
            Case 1
                sMonth = "Janv"
            Case 2
                sMonth = "Fév"
            Case 3
                sMonth = "Mars"
            Case 4
                sMonth = "Avr"
            Case 5
                sMonth = "Mai"
            Case 6
                sMonth = "Juin"
            Case 7
                sMonth = "Juil"
            Case 8
                sMonth = "Août"
            Case 9
                sMonth = "Sept"
            Case 10
                sMonth = "Oct"
            Case 11
                sMonth = "Nov"
            Case 12
                sMonth = "Déc"

        End Select

        Return sMonth
    End Function

    Private Function GetMonthName(d As DateTime, ci As Globalization.CultureInfo) As String

        Dim sMonth As String = ""


        sMonth = ci.DateTimeFormat.GetMonthName(d.Month)

        Select Case sMonth
            Case "janvier"
                sMonth = "Janv"
            Case "février"
                sMonth = "Fév"
            Case "mars"
                sMonth = "Mars"
            Case "avril"
                sMonth = "Avr"
            Case "mai"
                sMonth = "Mai"
            Case "juin"
                sMonth = "Juin"
            Case "juillet"
                sMonth = "Juil"
            Case "août"
                sMonth = "Août"
            Case "septembre"
                sMonth = "Sept"
            Case "octobre"
                sMonth = "Oct"
            Case "novembre"
                sMonth = "Nov"
            Case "décembre"
                sMonth = "Déc"

        End Select

        Return sMonth
    End Function


    Public Function ImprimerLesFactures() As Boolean

        Dim dDateFrom As Date = tbDateFrom.Text
        Dim dDateTo As Date = tbDateTo.Text
        Dim oPeriode As New Periodes
        Dim dtPeriode As New DataTable
        Dim drPeriode As DataRow
        Dim frenchCultureInfo = New Globalization.CultureInfo("fr-CA")


        Try

            If dDateFrom.Month <> dDateTo.Month Then
                oPeriode.Periode = dDateFrom.Day & " " & GetMonthName(dDateFrom, frenchCultureInfo) & "-" & dDateTo.Day & " " & GetMonthName(dDateTo, frenchCultureInfo) & " " & dDateTo.Year.ToString
            Else
                oPeriode.Periode = dDateFrom.Day & "-" & dDateTo.Day & " " & GetMonthName(dDateTo, frenchCultureInfo) & " " & dDateTo.Year.ToString
            End If

            dtPeriode = oPeriodeData.SelectAllByPeriode(go_Globals.IDPeriode)
            If dtPeriode.Rows.Count > 0 Then
                For Each drPeriode In dtPeriode.Rows
                    IDPeriode = drPeriode("IDPeriode")
                Next
            Else
                MessageBox.Show("La période n'existe pas!, Veuillez la créer en appuisant sur <Filtrer>", "Période manquante", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1)
                Return False
                Exit Function
            End If

            ImprimerFacturation()

            FillInvoiceNumber()

            Return True


        Catch ex As Exception
            Me.Cursor = Cursors.Default
            MessageBox.Show(ex.Message, "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try


    End Function
    Private Sub bImprimerFacturation_Click(sender As Object, e As EventArgs)

    End Sub


    Private Sub CalculerFactureTotaux()

        Dim oCadeauOrganisationData As New CadeauOrganisationData
        Dim frenchCultureInfo = New Globalization.CultureInfo("fr-CA")
        Dim dtorganisation As DataTable
        Dim drOrganisation As DataRow
        Dim dtInvoice As DataTable
        Dim drInvoice As DataRow
        Dim dtInvoiceLine As DataTable
        Dim drInvoiceLine As DataRow
        Dim strPeriode As String = vbNullString
        Dim dDateFrom As Date = tbDateFrom.Text
        Dim dDateTo As Date = tbDateTo.Text
        Dim oFacturation As New Facturation
        Dim irow As Integer = 0
        Dim dtInvoiceDate As Date
        Dim dblCredits As Double = 0
        Dim dblDebits As Double = 0
        Dim dblTotalAmount As Double = 0
        Dim dblCadeauOrganisation As Double = 0
        Dim dblGrandTotal As Double = 0
        Dim dblGrandTotalTPS As Double = 0
        Dim dblGrandTotalTVQ As Double = 0
        Dim iQteeLivraisons As Integer = 0
        Dim dblTotalParQteeLivraisons As Double = 0
        Dim oinv As New Facturation
        Dim dtorganisationO As DataTable
        Dim drOrganisationO As DataRow
        Dim iIDRapport As Integer = 0
        Dim dtRapportFacturePaye As New DataTable
        Dim drRapportFacturePaye As DataRow
        Dim iIDOrganisation As String = ""
        Dim iIDOrganisationString As String = ""
        Dim strSelectedValue As String = ""
        Dim iSelectedValue As Integer = 0
        Dim IDInvoice As Integer = 0




        Me.Cursor = Cursors.WaitCursor


        '--------------------------------------------
        ' Genere la periode
        '--------------------------------------------
        If dDateFrom.Month <> dDateTo.Month Then
            strPeriode = dDateFrom.Day & " " & GetMonthName(dDateFrom, frenchCultureInfo) & "-" & dDateTo.Day & " " & GetMonthName(dDateTo, frenchCultureInfo) & " " & dDateTo.Year.ToString
        Else
            strPeriode = dDateFrom.Day & "-" & dDateTo.Day & " " & GetMonthName(dDateTo, frenchCultureInfo) & " " & dDateTo.Year.ToString
        End If



        For Each item As DevExpress.XtraEditors.Controls.CheckedListBoxItem In cbFactures.Properties.Items
            If item.CheckState = CheckState.Checked Then
                strSelectedValue = strSelectedValue & item.Value & ","
                iSelectedValue = iSelectedValue + 1
            End If
        Next

        If strSelectedValue.ToString.Trim <> "" Then
            strSelectedValue = strSelectedValue.Remove(strSelectedValue.Length - 1)
        End If



        If iSelectedValue > 0 Then
            'iIDRapport = cbFactures.EditValue
            dtRapportFacturePaye = oRapportFacturePayeData.SelectAllByIDs(strSelectedValue)
            If dtRapportFacturePaye.Rows.Count > 0 Then
                For Each drRapportFacturePaye In dtRapportFacturePaye.Rows
                    iIDOrganisation = If(IsDBNull(drRapportFacturePaye("IDOrganisation")), 0, CType(drRapportFacturePaye("IDOrganisation"), Int32?))
                    iIDOrganisationString = iIDOrganisationString & iIDOrganisation.ToString & ","
                    dDateFrom = drRapportFacturePaye("DateFrom")
                    dDateTo = drRapportFacturePaye("DateTo")
                Next



                If iIDOrganisationString.ToString.Trim <> "" Then
                    iIDOrganisationString = iIDOrganisationString.Remove(iIDOrganisationString.Length - 1)
                End If

            End If


        Else

        End If


        dtorganisationO = oImportData.SelectOrganisationsAvecFacture(dDateFrom, dDateTo, iIDOrganisationString)
        If dtorganisationO.Rows.Count > 0 Then
            For Each drOrganisationO In dtorganisationO.Rows
                ProgressBarControl1.Properties.Step = 1
                ProgressBarControl1.Properties.PercentView = True
                ProgressBarControl1.Properties.Maximum = dtorganisationO.Rows.Count
                ProgressBarControl1.Properties.Minimum = 0
                dtorganisation = oOrganisationData.SelectByID(drOrganisationO("OrganisationID"))
                For Each drOrganisation In dtorganisation.Rows

                    '--------------------------------------------
                    ' Selectionne les info de la facture
                    '--------------------------------------------
                    dtInvoice = oInvoiceData.SelectInvoiceByDate(dDateFrom, dDateTo, drOrganisation("OrganisationID"))
                    If dtInvoice.Rows.Count > 0 Then

                        For Each drInvoice In dtInvoice.Rows

                            IDInvoice = drInvoice("IDInvoice")
                            dtInvoiceDate = drInvoice("InvoiceDate")
                            Dim sMonth As String = GetMonthName(Now, frenchCultureInfo).ToString

                            Select Case sMonth
                                Case "janvier"
                                    sMonth = "Janv"
                                Case "février"
                                    sMonth = "Fév"
                                Case "mars"
                                    sMonth = "Mars"
                                Case "avril"
                                    sMonth = "Avr"
                                Case "mai"
                                    sMonth = "Mai"
                                Case "juin"
                                    sMonth = "Juin"
                                Case "juillet"
                                    sMonth = "Juil"
                                Case "août"
                                    sMonth = "Août"
                                Case "septembre"
                                    sMonth = "Sept"
                                Case "octobre"
                                    sMonth = "Oct"
                                Case "novembre"
                                    sMonth = "Nov"
                                Case "décembre"
                                    sMonth = "Déc"

                            End Select


                            irow = 10

                            '--------------------------------------------
                            ' Selectionne le detail de la facture
                            '--------------------------------------------
                            dtInvoiceLine = oInvoiceLineData.SelectAllByDate(dDateFrom, dDateTo, drOrganisation("OrganisationID"))

                            Dim dtInvDate As Date
                            If dtInvoiceLine.Rows.Count > 0 Then
                                For Each drInvoiceLine In dtInvoiceLine.Rows

                                    dtInvDate = drInvoiceLine("DisplayDate")


                                    Select Case drInvoiceLine("TypeAjout")
                                        Case 0 ' Ajout
                                            dblTotalAmount = dblTotalAmount + If(IsDBNull(drInvoiceLine("Montant")), 0, CType(drInvoiceLine("Montant"), Decimal?))
                                        Case 1 ' Glacier
                                            dblTotalAmount = dblTotalAmount + If(IsDBNull(drInvoiceLine("Montant")), 0, CType(drInvoiceLine("Montant"), Decimal?))
                                        Case 2 ' Credit
                                            dblTotalAmount = dblTotalAmount - If(IsDBNull(drInvoiceLine("Montant")), 0, CType(drInvoiceLine("Montant"), Decimal?))

                                        Case 3 ' Debit
                                            dblTotalAmount = dblTotalAmount + If(IsDBNull(drInvoiceLine("Montant")), 0, CType(drInvoiceLine("Montant"), Decimal?))
                                        Case Else
                                            dblTotalAmount = dblTotalAmount + If(IsDBNull(drInvoiceLine("Montant")), 0, CType(drInvoiceLine("Montant"), Decimal?))
                                    End Select



                                    irow = irow + 1
                                Next
                            End If


                        Next

                    End If


                    Dim iQuantiteLivraison As Integer = If(IsDBNull(drOrganisation("QuantiteLivraison")), 0, CType(drOrganisation("QuantiteLivraison"), Int32?))
                    Dim dblCreditParLivraison As Double = If(IsDBNull(drOrganisation("CreditParLivraison")), 0, CType(drOrganisation("CreditParLivraison"), Decimal?))
                    iQteeLivraisons = oImportData.SelectQteeLivraison(dDateFrom, dDateTo, drOrganisation("OrganisationID"))
                    If iQuantiteLivraison > 0 Then
                        If (iQteeLivraisons >= iQuantiteLivraison) And iQteeLivraisons > 0 Then
                            dblTotalParQteeLivraisons = iQteeLivraisons * dblCreditParLivraison
                        End If
                    End If


                    '--------------------------------------------
                    ' Enleve et ajoute les extras
                    '--------------------------------------------
                    dblGrandTotal = dblTotalAmount
                    dblGrandTotal = dblGrandTotal - dblCadeauOrganisation
                    dblGrandTotal = dblGrandTotal - dblTotalParQteeLivraisons

                    Dim oInvoice As New Invoice

                    oInvoice.IDInvoice = IDInvoice
                    oInvoice.Amount = dblGrandTotal


                    dblGrandTotalTPS = dblGrandTotal * go_Globals.TPS
                    oInvoice.TPS = dblGrandTotalTPS

                    dblGrandTotalTVQ = dblGrandTotal * go_Globals.TVQ
                    oInvoice.TVQ = dblGrandTotalTVQ
                    oInvoice.Total = dblGrandTotal + dblGrandTotalTPS + dblGrandTotalTVQ
                    oInvoiceData.UpdateAmount(oInvoice, oInvoice)

                    dblCredits = 0
                    dblDebits = 0
                    dblTotalAmount = 0
                    dblCadeauOrganisation = 0
                    dblGrandTotalTPS = 0
                    dblGrandTotalTVQ = 0
                    dblGrandTotal = 0
                    iQuantiteLivraison = 0
                    dblCreditParLivraison = 0
                    dblTotalParQteeLivraisons = 0


                Next

                ProgressBarControl1.PerformStep()
                ProgressBarControl1.Update()
            Next

        End If

        ProgressBarControl1.EditValue = 0

        MessageBox.Show("Calculation des factures complete!")

        Me.Cursor = Cursors.Default



    End Sub
    Private Function ImprimerRapportDesLivraisonParRestaurant() As Boolean

        Dim oCadeauOrganisationData As New CadeauOrganisationData
        Dim frenchCultureInfo = New Globalization.CultureInfo("fr-CA")
        Dim dtorganisation As DataTable
        Dim drOrganisation As DataRow
        Dim dtImport As DataTable
        Dim drImport As DataRow
        Dim strPeriode As String = vbNullString
        Dim dDateFrom As Date = tbDateFrom.Text
        Dim dDateTo As Date = tbDateTo.Text
        Dim oFacturation As New Facturation
        Dim irow As Integer = 0
        Dim dtImportDate As Date
        Dim oinv As New Facturation
        Dim dtorganisationO As DataTable
        Dim drOrganisationO As DataRow
        Dim iIDRapportLiv As Integer = 0
        Dim dtLiv As New DataTable
        Dim drLiv As DataRow
        Dim iIDOrganisation As String = ""
        Dim iIDOrganisationString As String = ""
        Dim strSelectedValue As String = ""
        Dim iSelectedValue As Integer = 0
        Dim IDImport As Integer = 0
        Dim sFirstCell As String = ""
        Dim sLastCell As String = ""

        Try



            Me.Cursor = Cursors.WaitCursor

            SpreadsheetInfo.SetLicense("ERDC-FN4O-YKYN-4DBM")

            '--------------------------------------------
            ' Genere la periode
            '--------------------------------------------
            If dDateFrom.Month <> dDateTo.Month Then
                strPeriode = dDateFrom.Day & " " & GetMonthName(dDateFrom, frenchCultureInfo) & "-" & dDateTo.Day & " " & GetMonthName(dDateTo, frenchCultureInfo) & " " & dDateTo.Year.ToString
            Else
                strPeriode = dDateFrom.Day & "-" & dDateTo.Day & " " & GetMonthName(dDateTo, frenchCultureInfo) & " " & dDateTo.Year.ToString
            End If

            My.Computer.FileSystem.CreateDirectory(go_Globals.LocationRapportRestaurant & "\" & strPeriode)

            For Each item As DevExpress.XtraEditors.Controls.CheckedListBoxItem In cbLivraisonRestaurants.Properties.Items
                If item.CheckState = CheckState.Checked Then
                    strSelectedValue = strSelectedValue & item.Value & ","
                    iSelectedValue = iSelectedValue + 1
                End If
            Next

            If strSelectedValue.ToString.Trim <> "" Then
                strSelectedValue = strSelectedValue.Remove(strSelectedValue.Length - 1)
            End If



            If iSelectedValue > 0 Then

                dtLiv = oRapportLivraisonData.SelectAllByIDs(strSelectedValue)
                If dtLiv.Rows.Count > 0 Then
                    For Each drLiv In dtLiv.Rows

                        iIDRapportLiv = drLiv("IDRapportLivraison")

                        iIDOrganisation = If(IsDBNull(drLiv("IDOrganisation")), 0, CType(drLiv("IDOrganisation"), Int32?))
                        iIDOrganisationString = iIDOrganisationString & iIDOrganisation.ToString & ","

                        dDateFrom = drLiv("DateFrom")
                        dDateTo = drLiv("DateTo")

                        If My.Computer.FileSystem.FileExists(drLiv("CheminRapport")) Then
                            My.Computer.FileSystem.DeleteFile(drLiv("CheminRapport"))
                        End If


                        Dim oRapportLivraison As New RapportLivraison
                        oRapportLivraison.IDRapportLivraison = iIDRapportLiv
                        oRapportLivraisonData.Delete(oRapportLivraison)
                    Next

                    If iIDOrganisationString.ToString.Trim <> "" Then
                        iIDOrganisationString = iIDOrganisationString.Remove(iIDOrganisationString.Length - 1)
                    End If

                End If


            Else
                Dim directoryName As String = go_Globals.LocationRapportRestaurant & "\" & strPeriode & "\"
                For Each deleteFile In Directory.GetFiles(directoryName, "*.*", SearchOption.TopDirectoryOnly)
                    File.Delete(deleteFile)
                Next


                Dim oRapportLivraison As New RapportLivraison
                oRapportLivraison.IDPeriode = IDPeriode
                oRapportLivraisonData.DeleteByPeriodF(oRapportLivraison)


            End If


            dtorganisationO = oImportData.SelectOrganisationsAvecFactureRestaurant(dDateFrom, dDateTo, iIDOrganisationString)
            If dtorganisationO.Rows.Count > 0 Then
                For Each drOrganisationO In dtorganisationO.Rows
                    ProgressBarControl1.Properties.Step = 1
                    ProgressBarControl1.Properties.PercentView = True
                    ProgressBarControl1.Properties.Maximum = dtorganisationO.Rows.Count
                    ProgressBarControl1.Properties.Minimum = 0

                    dtorganisation = oOrganisationData.SelectByID(drOrganisationO("OrganisationID"))
                    For Each drOrganisation In dtorganisation.Rows


                        Dim sExcelPath As String = Application.StartupPath() & "\LivraisonsParCommerce.xlsx"
                        Dim ef As ExcelFile = ExcelFile.Load(sExcelPath)

                        '--------------------------------------------
                        ' Genere le header de la facture
                        '--------------------------------------------
                        ef.Worksheets(0).Cells(1, 2).Value = IIf(drOrganisation.IsNull("Organisation"), "", drOrganisation("Organisation").ToString.Trim)

                        Dim sAddress As String
                        sAddress = IIf(drOrganisation.IsNull("Adresse"), "", drOrganisation("Adresse").ToString.Trim)
                        sAddress = sAddress.Replace(vbCrLf, "")
                        ef.Worksheets(0).Cells(2, 2).Value = sAddress
                        ef.Worksheets(0).Cells(3, 2).Value = IIf(drOrganisation.IsNull("Telephone"), "", drOrganisation("Telephone").ToString.Trim)

                        Dim stremailString As String = ""

                        'Dim strEmail As String = IIf(drOrganisation.IsNull("Email"), "", drOrganisation("Email").ToString.Trim)
                        'Dim strEmail2 As String = IIf(drOrganisation.IsNull("Email1"), "", drOrganisation("Email1").ToString.Trim)
                        'Dim strEmail3 As String = IIf(drOrganisation.IsNull("Email2"), "", drOrganisation("Email2").ToString.Trim)
                        'Dim strEmail4 As String = IIf(drOrganisation.IsNull("Email3"), "", drOrganisation("Email3").ToString.Trim)

                        'If strEmail.ToString.Trim <> "" And drOrganisation("CacheEmail1") = False Then
                        '    stremailString = strEmail.ToString.Trim
                        'End If

                        'If strEmail2.ToString.Trim <> "" And drOrganisation("CacheEmail2") = False Then
                        '    stremailString = stremailString & "," & strEmail2.ToString.Trim
                        'End If

                        'If strEmail3.ToString.Trim <> "" And drOrganisation("CacheEmail3") = False Then
                        '    stremailString = stremailString & "," & strEmail3.ToString.Trim
                        'End If

                        'If strEmail4.ToString.Trim <> "" And drOrganisation("CacheEmail4") = False Then
                        '    stremailString = stremailString & "," & strEmail3.ToString.Trim
                        'End If


                        'ef.Worksheets(0).Cells(5, 2).Value = stremailString
                        Dim sMonth As String = GetMonthName(Now, frenchCultureInfo).ToString

                        Select Case sMonth
                            Case "janvier"
                                sMonth = "Janv"
                            Case "février"
                                sMonth = "Fév"
                            Case "mars"
                                sMonth = "Mars"
                            Case "avril"
                                sMonth = "Avr"
                            Case "mai"
                                sMonth = "Mai"
                            Case "juin"
                                sMonth = "Juin"
                            Case "juillet"
                                sMonth = "Juil"
                            Case "août"
                                sMonth = "Août"
                            Case "septembre"
                                sMonth = "Sept"
                            Case "octobre"
                                sMonth = "Oct"
                            Case "novembre"
                                sMonth = "Nov"
                            Case "décembre"
                                sMonth = "Déc"

                        End Select

                        ef.Worksheets(0).Cells(6, 9).Value = dDateFrom.ToShortDateString
                        ef.Worksheets(0).Cells(7, 9).Value = dDateTo.ToShortDateString

                        irow = 12

                        Dim style2 As New CellStyle
                        style2.HorizontalAlignment = HorizontalAlignmentStyle.Left
                        style2.VerticalAlignment = VerticalAlignmentStyle.Center
                        style2.WrapText = True



                        '--------------------------------------------
                        ' Detail du rapport de livraison
                        '--------------------------------------------                        
                        dtImport = oImportData.RapportdeLivraisonParRestaurant(dDateFrom, dDateTo, drOrganisation("OrganisationID"))
                        If dtImport.Rows.Count > 0 Then

                            For Each drImport In dtImport.Rows

                                IDImport = drImport("ImportID")
                                dtImportDate = drImport("DateImport")


                                sFirstCell = "A" & irow.ToString
                                sLastCell = "B" & irow.ToString

                                ef.Worksheets(0).Cells.GetSubrange(sFirstCell, sLastCell).Merged = True
                                ef.Worksheets(0).Cells.GetSubrange(sFirstCell, sLastCell).Style = style2

                                sFirstCell = "C" & irow.ToString
                                sLastCell = "E" & irow.ToString
                                ef.Worksheets(0).Cells.GetSubrange(sFirstCell, sLastCell).Merged = True
                                ef.Worksheets(0).Cells.GetSubrange(sFirstCell, sLastCell).Style = style2

                                sFirstCell = "F" & irow.ToString
                                sLastCell = "H" & irow.ToString
                                ef.Worksheets(0).Cells.GetSubrange(sFirstCell, sLastCell).Merged = True
                                ef.Worksheets(0).Cells.GetSubrange(sFirstCell, sLastCell).Style = style2


                                sFirstCell = "J" & irow.ToString
                                sLastCell = "K" & irow.ToString
                                ef.Worksheets(0).Cells.GetSubrange(sFirstCell, sLastCell).Merged = True
                                ef.Worksheets(0).Cells.GetSubrange(sFirstCell, sLastCell).Style = style2

                                Dim dDate As Date = If(IsDBNull(drImport("DateImport")), 0, drImport("DateImport"))

                                ef.Worksheets(0).Cells(irow, 1).Value = dDate.ToShortDateString
                                ef.Worksheets(0).Cells(irow, 1).Row.AutoFit()
                                ef.Worksheets(0).Cells(irow, 3).Value = IIf(drImport.IsNull("clientaddress"), 0, drImport("clientaddress"))
                                ef.Worksheets(0).Cells(irow, 3).Row.AutoFit()

                                Dim oClientAddresse As New ClientAddresse
                                Dim oClientAddresseSelected As New ClientAddresse
                                Dim oClientAddresseData As New ClientAddresseData

                                oClientAddresse.IDClientAddress = If(IsDBNull(drImport("IDClientAddresse")), 0, drImport("IDClientAddresse"))

                                If oClientAddresse.IDClientAddress > 0 Then

                                    oClientAddresseSelected = oClientAddresseData.Select_Record(oClientAddresse)

                                    If Not IsNothing(oClientAddresseSelected) Then


                                        If Not IsNothing(oClientAddresseSelected.CodeEntree) Then
                                            ef.Worksheets(0).Cells(irow, 5).Value = oClientAddresseSelected.CodeEntree
                                            ef.Worksheets(0).Cells(irow, 5).Row.AutoFit()
                                        Else
                                            ef.Worksheets(0).Cells(irow, 5).Value = "-"
                                        End If


                                        If Not IsNothing(oClientAddresseSelected.Appartement) Then
                                            ef.Worksheets(0).Cells(irow, 8).Value = oClientAddresseSelected.Appartement
                                            ef.Worksheets(0).Cells(irow, 8).Row.AutoFit()
                                        Else
                                            ef.Worksheets(0).Cells(irow, 8).Value = "-"
                                        End If

                                        If Not IsNothing(oClientAddresseSelected.Telephone) Then


                                            ef.Worksheets(0).Cells(irow, 9).Value = oClientAddresseSelected.Telephone.ToString.Trim

                                            ef.Worksheets(0).Cells(irow, 9).Row.AutoFit()
                                        Else
                                            ef.Worksheets(0).Cells(irow, 9).Value = "-"
                                        End If


                                    Else
                                        ef.Worksheets(0).Cells(irow, 8).Value = "-"
                                        ef.Worksheets(0).Cells(irow, 9).Value = "-"
                                    End If
                                End If

                                irow = irow + 1

                            Next



                            sFirstCell = "A" & irow.ToString
                            sLastCell = "B" & irow.ToString
                            ef.Worksheets(0).Cells.GetSubrange(sFirstCell, sLastCell).Merged = True
                            ef.Worksheets(0).Cells.GetSubrange(sFirstCell, sLastCell).Style = style2

                            sFirstCell = "C" & irow.ToString
                            sLastCell = "E" & irow.ToString
                            ef.Worksheets(0).Cells.GetSubrange(sFirstCell, sLastCell).Merged = True
                            ef.Worksheets(0).Cells.GetSubrange(sFirstCell, sLastCell).Style = style2

                            sFirstCell = "F" & irow.ToString
                            sLastCell = "H" & irow.ToString
                            ef.Worksheets(0).Cells.GetSubrange(sFirstCell, sLastCell).Merged = True
                            ef.Worksheets(0).Cells.GetSubrange(sFirstCell, sLastCell).Style = style2


                            sFirstCell = "J" & irow.ToString
                            sLastCell = "K" & irow.ToString
                            ef.Worksheets(0).Cells.GetSubrange(sFirstCell, sLastCell).Merged = True
                            ef.Worksheets(0).Cells.GetSubrange(sFirstCell, sLastCell).Style = style2


                        End If

                        Dim style As New CellStyle
                        style.HorizontalAlignment = HorizontalAlignmentStyle.Right
                        style.VerticalAlignment = VerticalAlignmentStyle.Center
                        style.FillPattern.SetSolid(Color.Yellow)
                        style.Font.Weight = ExcelFont.BoldWeight

                        irow = irow + 3

                        Dim styleFooter As New CellStyle
                        styleFooter.HorizontalAlignment = HorizontalAlignmentStyle.Center
                        styleFooter.VerticalAlignment = VerticalAlignmentStyle.Center
                        styleFooter.WrapText = True


                        sFirstCell = "A" & irow.ToString
                        sLastCell = "K" & irow + 9
                        ef.Worksheets(0).Cells.GetSubrange(sFirstCell, sLastCell).Merged = True
                        ef.Worksheets(0).Cells.GetSubrange(sFirstCell, sLastCell).Style = styleFooter

                        Dim strFooter As String = "TPS 803457597 RT 0001" & " - " & "TVQ 1222669355 TQ 0001" & vbCrLf
                        strFooter = strFooter & "1105 Cherbourg  Saint-Hubert  J3Y 6G5 QC" & vbCrLf
                        strFooter = strFooter & "idslivraisonexpress.com" & " - " & "idslivraisonexpress@gmail.com" & vbCrLf

                        ef.Worksheets(0).Cells(irow, 2).Value = strFooter

                        Dim sFile As String
                        Dim Icount As Integer = 0
                        'ef.Worksheets(0).PrintOptions.FitWorksheetWidthToPages = 1


                        sFile = drOrganisation("Organisation").ToString.Trim & "-" & strPeriode & ".pdf"
                        sFile = sFile.Replace("'", "")

CHECKFILE:
                        ' if the file name already exist, create a copy
                        If System.IO.File.Exists(go_Globals.LocationRapportRestaurant & "\" & strPeriode & "\" & sFile) Then
                            Icount = Icount + 1
                            sFile = drOrganisation("Organisation").ToString.Trim & "-" & strPeriode & "_COPY" & Icount.ToString & ".pdf"
                            GoTo CHECKFILE
                        End If


                        ef.Save(go_Globals.LocationRapportRestaurant & "\" & strPeriode & "\" & sFile)
                        ef = Nothing


                        If chkPRINT.Checked = True Then
                            GemBox.Pdf.ComponentInfo.SetLicense("AD0G-Y35C-Z376-E74D")

                            Using document As PdfDocument = PdfDocument.Load(go_Globals.LocationRapportRestaurant & "\" & strPeriode & "\" & sFile)
                                ' Print PDF document to default printer (e.g. 'Microsoft Print to Pdf').
                                Dim printerName As String = Nothing
                                document.Print(printerName)
                            End Using
                        End If



                        Dim oRapportLivraison As New RapportLivraison
                        oRapportLivraison.IDPeriode = IDPeriode
                        oRapportLivraison.IDOrganisation = drOrganisation("OrganisationID")
                        oRapportLivraison.Organisation = If(IsNothing(drOrganisation("Organisation")), Nothing, drOrganisation("Organisation").ToString.Trim)
                        oRapportLivraison.IDLivreur = Nothing
                        oRapportLivraison.Livreur = Nothing
                        oRapportLivraison.Email = If(IsNothing(drOrganisation("Email")), Nothing, drOrganisation("Email").ToString.Trim)
                        oRapportLivraison.Email1 = If(IsNothing(drOrganisation("Email1")), Nothing, drOrganisation("Email1").ToString.Trim)
                        oRapportLivraison.Email2 = If(IsNothing(drOrganisation("Email2")), Nothing, drOrganisation("Email2").ToString.Trim)
                        oRapportLivraison.CheminRapport = go_Globals.LocationRapportRestaurant & "\" & strPeriode & "\" & sFile
                        oRapportLivraison.NomFichier = sFile
                        oRapportLivraison.DateFrom = dDateFrom
                        oRapportLivraison.DateTo = dDateTo
                        oRapportLivraison.IsRapportRestaurant = True
                        oRapportLivraisonData.Add(oRapportLivraison)


                    Next

                    ProgressBarControl1.PerformStep()
                    ProgressBarControl1.Update()
                Next

            End If
            ImprimerRapportDesLivraisonParRestaurant = True
            ProgressBarControl1.EditValue = 0
            FillAllRapportLivraisonRestaurant()

            Process.Start(go_Globals.LocationRapportRestaurant & "\" & strPeriode)
            Me.Cursor = Cursors.Default

        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
            ImprimerRapportDesLivraisonParRestaurant = False
        End Try


    End Function
    Private Function ImprimerFacturation() As Boolean

        Dim oCadeauOrganisationData As New CadeauOrganisationData
        Dim frenchCultureInfo = New Globalization.CultureInfo("fr-CA")
        Dim dtorganisation As DataTable
        Dim drOrganisation As DataRow
        Dim dtInvoice As DataTable
        Dim drInvoice As DataRow
        Dim dtInvoiceLine As DataTable
        Dim drInvoiceLine As DataRow
        Dim strPeriode As String = vbNullString
        Dim dDateFrom As Date = tbDateFrom.Text
        Dim dDateTo As Date = tbDateTo.Text
        Dim oFacturation As New Facturation
        Dim irow As Integer = 0
        Dim dtInvoiceDate As Date
        Dim dblCredits As Double = 0
        Dim dblDebits As Double = 0
        Dim dblTotalAmount As Double = 0
        Dim dblCadeauOrganisation As Double = 0
        Dim dblGrandTotal As Double = 0
        Dim dblGrandTotalTPS As Double = 0
        Dim dblGrandTotalTVQ As Double = 0
        Dim iQteeLivraisons As Integer = 0
        Dim dblTotalParQteeLivraisons As Double = 0
        Dim oinv As New Facturation
        Dim dtorganisationO As DataTable
        Dim drOrganisationO As DataRow
        Dim iIDRapport As Integer = 0
        Dim dtRapportFacturePaye As New DataTable
        Dim drRapportFacturePaye As DataRow
        Dim iIDOrganisation As String = ""
        Dim iIDOrganisationString As String = ""
        Dim strSelectedValue As String = ""
        Dim iSelectedValue As Integer = 0
        Dim IDInvoice As Integer = 0
        Dim sFirstCell As String = ""
        Dim sLastCell As String = ""

        Try



            Me.Cursor = Cursors.WaitCursor

            SpreadsheetInfo.SetLicense("ERDC-FN4O-YKYN-4DBM")

            '--------------------------------------------
            ' Genere la periode
            '--------------------------------------------
            If dDateFrom.Month <> dDateTo.Month Then
                strPeriode = dDateFrom.Day & " " & GetMonthName(dDateFrom, frenchCultureInfo) & "-" & dDateTo.Day & " " & GetMonthName(dDateTo, frenchCultureInfo) & " " & dDateTo.Year.ToString
            Else
                strPeriode = dDateFrom.Day & "-" & dDateTo.Day & " " & GetMonthName(dDateTo, frenchCultureInfo) & " " & dDateTo.Year.ToString
            End If

            My.Computer.FileSystem.CreateDirectory(go_Globals.LocationFacture & "\" & strPeriode)



            For Each item As DevExpress.XtraEditors.Controls.CheckedListBoxItem In cbFactures.Properties.Items
                If item.CheckState = CheckState.Checked Then
                    strSelectedValue = strSelectedValue & item.Value & ","
                    iSelectedValue = iSelectedValue + 1
                End If
            Next

            If strSelectedValue.ToString.Trim <> "" Then
                strSelectedValue = strSelectedValue.Remove(strSelectedValue.Length - 1)
            End If


            Dim style2 As New CellStyle
            style2.HorizontalAlignment = HorizontalAlignmentStyle.Center
            style2.VerticalAlignment = VerticalAlignmentStyle.Center

            If iSelectedValue > 0 Then

                dtRapportFacturePaye = oRapportFacturePayeData.SelectAllByIDs(strSelectedValue)
                If dtRapportFacturePaye.Rows.Count > 0 Then
                    For Each drRapportFacturePaye In dtRapportFacturePaye.Rows


                        iIDOrganisation = If(IsDBNull(drRapportFacturePaye("IDOrganisation")), 0, CType(drRapportFacturePaye("IDOrganisation"), Int32?))
                        iIDOrganisationString = iIDOrganisationString & iIDOrganisation.ToString & ","

                        iIDRapport = drRapportFacturePaye("IDRapport")
                        dDateFrom = drRapportFacturePaye("DateFrom")
                        dDateTo = drRapportFacturePaye("DateTo")

                        If My.Computer.FileSystem.FileExists(drRapportFacturePaye("CheminRapport")) Then
                            My.Computer.FileSystem.DeleteFile(drRapportFacturePaye("CheminRapport"))
                        End If


                        Dim oRapportFacture As New RapportFacturePaye
                        oRapportFacture.IDRapport = iIDRapport
                        oRapportFacturePayeData.Delete(oRapportFacture)
                    Next



                    If iIDOrganisationString.ToString.Trim <> "" Then
                        iIDOrganisationString = iIDOrganisationString.Remove(iIDOrganisationString.Length - 1)
                    End If

                End If


            Else
                Dim directoryName As String = go_Globals.LocationFacture & "\" & strPeriode & "\"
                For Each deleteFile In Directory.GetFiles(directoryName, "*.*", SearchOption.TopDirectoryOnly)
                    File.Delete(deleteFile)
                Next

                Dim oRapportFacture As New RapportFacturePaye
                oRapportFacture.IDPeriode = IDPeriode
                oRapportFacturePayeData.DeletePeriodO(oRapportFacture)

            End If


            dtorganisationO = oInvoiceData.SelectAllByPeriod(go_Globals.IDPeriode)
            If dtorganisationO.Rows.Count > 0 Then
                For Each drOrganisationO In dtorganisationO.Rows
                    ProgressBarControl1.Properties.Step = 1
                    ProgressBarControl1.Properties.PercentView = True
                    ProgressBarControl1.Properties.Maximum = dtorganisationO.Rows.Count
                    ProgressBarControl1.Properties.Minimum = 0
                    dtorganisation = oOrganisationData.SelectByID(drOrganisationO("OrganisationID"))
                    For Each drOrganisation In dtorganisation.Rows


                        Dim sExcelPath As String = Application.StartupPath() & "\Facture.xlsx"
                        Dim ef As ExcelFile = ExcelFile.Load(sExcelPath)




                        '--------------------------------------------
                        ' Genere le header de la facture
                        '--------------------------------------------
                        ef.Worksheets(0).Cells(2, 2).Value = IIf(drOrganisation.IsNull("Organisation"), "", drOrganisation("Organisation").ToString.Trim)
                        ef.Worksheets(0).Cells(3, 2).Value = IIf(drOrganisation.IsNull("Adresse"), "", drOrganisation("Adresse").ToString.Trim)
                        ef.Worksheets(0).Cells(4, 2).Value = IIf(drOrganisation.IsNull("Telephone"), "", drOrganisation("Telephone").ToString.Trim)

                        Dim stremailString As String = ""

                        Dim strEmail As String = IIf(drOrganisation.IsNull("Email"), "", drOrganisation("Email").ToString.Trim)
                        Dim strEmail2 As String = IIf(drOrganisation.IsNull("Email1"), "", drOrganisation("Email1").ToString.Trim)
                        Dim strEmail3 As String = IIf(drOrganisation.IsNull("Email2"), "", drOrganisation("Email2").ToString.Trim)
                        Dim strEmail4 As String = IIf(drOrganisation.IsNull("Email3"), "", drOrganisation("Email3").ToString.Trim)

                        If strEmail.ToString.Trim <> "" And drOrganisation("CacheEmail1") = False Then
                            stremailString = strEmail.ToString.Trim
                        End If

                        If strEmail2.ToString.Trim <> "" And drOrganisation("CacheEmail2") = False Then
                            stremailString = stremailString & "," & strEmail2.ToString.Trim
                        End If

                        If strEmail3.ToString.Trim <> "" And drOrganisation("CacheEmail3") = False Then
                            stremailString = stremailString & "," & strEmail3.ToString.Trim
                        End If

                        If strEmail4.ToString.Trim <> "" And drOrganisation("CacheEmail4") = False Then
                            stremailString = stremailString & "," & strEmail3.ToString.Trim
                        End If


                        ef.Worksheets(0).Cells(5, 2).Value = stremailString

                        '--------------------------------------------
                        ' Selectionne les info de la facture
                        '--------------------------------------------
                        dtInvoice = oInvoiceData.SelectInvoiceByDateINFO(dDateFrom, dDateTo, drOrganisation("OrganisationID"))
                        If dtInvoice.Rows.Count > 0 Then

                            For Each drInvoice In dtInvoice.Rows

                                IDInvoice = drInvoice("IDInvoice")
                                dtInvoiceDate = drInvoice("InvoiceDate")
                                Dim sMonth As String = GetMonthName(Now, frenchCultureInfo).ToString

                                Select Case sMonth
                                    Case "janvier"
                                        sMonth = "Janv"
                                    Case "février"
                                        sMonth = "Fév"
                                    Case "mars"
                                        sMonth = "Mars"
                                    Case "avril"
                                        sMonth = "Avr"
                                    Case "mai"
                                        sMonth = "Mai"
                                    Case "juin"
                                        sMonth = "Juin"
                                    Case "juillet"
                                        sMonth = "Juil"
                                    Case "août"
                                        sMonth = "Août"
                                    Case "septembre"
                                        sMonth = "Sept"
                                    Case "octobre"
                                        sMonth = "Oct"
                                    Case "novembre"
                                        sMonth = "Nov"
                                    Case "décembre"
                                        sMonth = "Déc"

                                End Select


                                irow = 10
                                ef.Worksheets(0).Cells(4, 6).Value = Now.Day & " " & sMonth & " " & Now.Year
                                ef.Worksheets(0).Cells(5, 6).Value = drInvoice("InvoiceNumber").ToString.Trim
                                ef.Worksheets(0).Cells(7, 1).Value = strPeriode.ToString


                                '--------------------------------------------
                                ' Selectionne le detail de la facture
                                '--------------------------------------------


                                dtInvoiceLine = oInvoiceLineData.SelectAllByDate(dDateFrom, dDateTo, drOrganisation("OrganisationID"))

                                Dim dtInvDate As Date
                                If dtInvoiceLine.Rows.Count > 0 Then
                                    For Each drInvoiceLine In dtInvoiceLine.Rows




                                        dtInvDate = drInvoiceLine("DisplayDate")
                                        sFirstCell = "C" & irow.ToString
                                        sLastCell = "D" & irow.ToString
                                        ef.Worksheets(0).Cells.GetSubrange(sFirstCell, sLastCell).Merged = True
                                        ef.Worksheets(0).Cells.GetSubrange(sFirstCell, sLastCell).Style = style2

                                        If drInvoiceLine("Description").ToString.Trim <> "Extras" Then

                                            If drOrganisation("MetodeCalculationOrganisation") = 7 Then
                                                ef.Worksheets(0).Cells(irow, 1).Value = strPeriode
                                            Else
                                                ef.Worksheets(0).Cells(irow, 1).Value = dtInvDate.Day.ToString & " " & GetMonthName(dtInvDate, frenchCultureInfo)
                                            End If


                                            ef.Worksheets(0).Cells(irow, 5).Value = If(IsDBNull(drInvoiceLine("Prix")), 0, CType(drInvoiceLine("Prix"), Decimal?))

                                            ef.Worksheets(0).Cells(irow, 2).Value = drInvoiceLine("Description").ToString.Trim
                                            ef.Worksheets(0).Cells(irow, 2).Style = style2
                                        Else
                                            ef.Worksheets(0).Cells(irow, 1).Value = strPeriode
                                            ef.Worksheets(0).Cells(irow, 2).Value = "Extra"
                                        End If

                                        'ef.Worksheets(0).Cells(irow, 3).Value = If(IsDBNull(drInvoiceLine("Km")), 0, drInvoiceLine("Km"))
                                        ef.Worksheets(0).Cells(irow, 4).Value = IIf(drInvoiceLine.IsNull("Unite"), 0, drInvoiceLine("Unite"))

                                        ef.Worksheets(0).Cells(irow, 5).Style.NumberFormat = "$0.00"
                                        ef.Worksheets(0).Cells(irow, 5).Style.HorizontalAlignment = HorizontalAlignmentStyle.Right


                                        ef.Worksheets(0).Cells(irow, 6).Value = If(IsDBNull(drInvoiceLine("Montant")), 0, CType(drInvoiceLine("Montant"), Decimal?))
                                        ef.Worksheets(0).Cells(irow, 6).Style.NumberFormat = "$0.00"
                                        ef.Worksheets(0).Cells(irow, 6).Style.HorizontalAlignment = HorizontalAlignmentStyle.Right
                                        Select Case drInvoiceLine("TypeAjout")
                                            Case 0 ' Ajout
                                                ef.Worksheets(0).Cells(irow, 2).Value = drInvoiceLine("Description").ToString.Trim
                                                dblTotalAmount = dblTotalAmount + If(IsDBNull(drInvoiceLine("Montant")), 0, CType(drInvoiceLine("Montant"), Decimal?))
                                            Case 1 ' Glacier
                                                ef.Worksheets(0).Cells(irow, 2).Value = drInvoiceLine("Description").ToString.Trim
                                                dblTotalAmount = dblTotalAmount + If(IsDBNull(drInvoiceLine("Montant")), 0, CType(drInvoiceLine("Montant"), Decimal?))
                                            Case 2 ' Credit
                                                ef.Worksheets(0).Cells(irow, 2).Value = drInvoiceLine("Description").ToString.Trim
                                                ef.Worksheets(0).Cells(irow, 5).Style.Font.Color = SpreadsheetColor.FromName(ColorName.Red)
                                                ef.Worksheets(0).Cells(irow, 5).Style.NumberFormat = "$0.00"
                                                ef.Worksheets(0).Cells(irow, 5).Value = If(IsDBNull(drInvoiceLine("Prix")), 0, CType(drInvoiceLine("Prix"), Decimal?))

                                                ef.Worksheets(0).Cells(irow, 6).Style.Font.Color = SpreadsheetColor.FromName(ColorName.Red)
                                                ef.Worksheets(0).Cells(irow, 6).Style.NumberFormat = "$0.00"
                                                ef.Worksheets(0).Cells(irow, 6).Value = If(IsDBNull(drInvoiceLine("Montant")), 0, CType(drInvoiceLine("Montant"), Decimal?))

                                                dblTotalAmount = dblTotalAmount - If(IsDBNull(drInvoiceLine("Montant")), 0, CType(drInvoiceLine("Montant"), Decimal?))

                                            Case 3 ' Debit
                                                ef.Worksheets(0).Cells(irow, 2).Value = drInvoiceLine("Description").ToString.Trim
                                                dblTotalAmount = dblTotalAmount + If(IsDBNull(drInvoiceLine("Montant")), 0, CType(drInvoiceLine("Montant"), Decimal?))
                                            Case Else
                                                ef.Worksheets(0).Cells(irow, 2).Value = drInvoiceLine("Description").ToString.Trim
                                                dblTotalAmount = dblTotalAmount + If(IsDBNull(drInvoiceLine("Montant")), 0, CType(drInvoiceLine("Montant"), Decimal?))
                                        End Select



                                        irow = irow + 1


                                    Next



                                    sFirstCell = "C" & irow.ToString
                                    sLastCell = "D" & irow.ToString
                                    ef.Worksheets(0).Cells.GetSubrange(sFirstCell, sLastCell).Merged = True
                                    ef.Worksheets(0).Cells.GetSubrange(sFirstCell, sLastCell).Style = style2

                                End If


                            Next

                        End If

                        irow = irow + 1
                        Dim iQuantiteLivraison As Integer = If(IsDBNull(drOrganisation("QuantiteLivraison")), 0, CType(drOrganisation("QuantiteLivraison"), Int32?))
                        Dim dblCreditParLivraison As Double = If(IsDBNull(drOrganisation("CreditParLivraison")), 0, CType(drOrganisation("CreditParLivraison"), Decimal?))
                        iQteeLivraisons = oImportData.SelectQteeLivraison(dDateFrom, dDateTo, drOrganisation("OrganisationID"))
                        If iQuantiteLivraison > 0 Then
                            If (iQteeLivraisons >= iQuantiteLivraison) And iQteeLivraisons > 0 Then
                                dblTotalParQteeLivraisons = iQteeLivraisons * dblCreditParLivraison
                                ef.Worksheets(0).Cells(29, 2).Value = "Escompte plus de " & iQuantiteLivraison.ToString & " livraisons par semaines."
                            End If
                        End If


                        '--------------------------------------------
                        ' Enleve et ajoute les extras
                        '--------------------------------------------

                        Dim style As New CellStyle
                        style.HorizontalAlignment = HorizontalAlignmentStyle.Right
                        style.VerticalAlignment = VerticalAlignmentStyle.Center
                        style.FillPattern.SetSolid(Color.Yellow)
                        style.Font.Weight = ExcelFont.BoldWeight



                        dblGrandTotal = dblTotalAmount
                        dblGrandTotal = dblGrandTotal - dblCadeauOrganisation
                        dblGrandTotal = dblGrandTotal - dblTotalParQteeLivraisons

                        Dim oInvoice As New Invoice

                        oInvoice.IDInvoice = IDInvoice
                        oInvoice.Amount = dblGrandTotal

                        ef.Worksheets(0).Cells(irow, 5).Value = "Sous total"
                        ef.Worksheets(0).Cells(irow, 5).Style = style
                        ef.Worksheets(0).Cells(irow, 6).Style = style
                        ef.Worksheets(0).Cells(irow, 6).Value = If(IsDBNull(dblTotalAmount), 0, CType(dblTotalAmount, Decimal?))
                        ef.Worksheets(0).Cells(irow, 6).Style.NumberFormat = "$0.00"

                        irow = irow + 1

                        dblGrandTotalTPS = dblGrandTotal * go_Globals.TPS
                        oInvoice.TPS = dblGrandTotalTPS

                        dblGrandTotalTVQ = dblGrandTotal * go_Globals.TVQ
                        oInvoice.TVQ = dblGrandTotalTVQ
                        oInvoice.Total = dblGrandTotal + dblGrandTotalTPS + dblGrandTotalTVQ
                        oInvoiceData.UpdateAmount(oInvoice, oInvoice)
                        ef.Worksheets(0).Cells(irow, 5).Value = "TPS"
                        ef.Worksheets(0).Cells(irow, 5).Style = style
                        ef.Worksheets(0).Cells(irow, 6).Style = style
                        ef.Worksheets(0).Cells(irow, 6).Value = Math.Round(dblGrandTotalTPS, 2)
                        ef.Worksheets(0).Cells(irow, 6).Style.NumberFormat = "$0.00"

                        irow = irow + 1
                        ef.Worksheets(0).Cells(irow, 5).Value = "TVQ"
                        ef.Worksheets(0).Cells(irow, 5).Style = style
                        ef.Worksheets(0).Cells(irow, 6).Value = Math.Round(dblGrandTotalTVQ, 2)
                        ef.Worksheets(0).Cells(irow, 6).Style = style
                        ef.Worksheets(0).Cells(irow, 6).Style.NumberFormat = "$0.00"
                        irow = irow + 1
                        ef.Worksheets(0).Cells(irow, 5).Value = "Total"
                        ef.Worksheets(0).Cells(irow, 5).Style = style
                        ef.Worksheets(0).Cells(irow, 6).Value = Math.Round(dblGrandTotal + dblGrandTotalTPS + dblGrandTotalTVQ, 2)
                        ef.Worksheets(0).Cells(irow, 6).Style = style
                        ef.Worksheets(0).Cells(irow, 6).Style.NumberFormat = "$0.00"

                        irow = irow + 3


                        Dim styleFooterMessage As New CellStyle
                        styleFooterMessage.HorizontalAlignment = HorizontalAlignmentStyle.Center
                        styleFooterMessage.VerticalAlignment = VerticalAlignmentStyle.Center
                        styleFooterMessage.WrapText = True
                        styleFooterMessage.FillPattern.SetSolid(Color.Yellow)


                        sFirstCell = "B" & irow.ToString
                        sLastCell = "G" & irow + 2
                        ef.Worksheets(0).Cells.GetSubrange(sFirstCell, sLastCell).Merged = True
                        ef.Worksheets(0).Cells.GetSubrange(sFirstCell, sLastCell).Style = styleFooterMessage

                        Dim strFooterMessage As String = IIf(drOrganisation.IsNull("MessageFacture"), "", drOrganisation("MessageFacture").ToString.Trim)

                        ef.Worksheets(0).Cells(irow, 2).Value = strFooterMessage


                        irow = irow + 3



                        Dim styleFooter As New CellStyle
                        styleFooter.HorizontalAlignment = HorizontalAlignmentStyle.Center
                        styleFooter.VerticalAlignment = VerticalAlignmentStyle.Center
                        styleFooter.WrapText = True


                        sFirstCell = "B" & irow.ToString
                        sLastCell = "G" & irow + 9
                        ef.Worksheets(0).Cells.GetSubrange(sFirstCell, sLastCell).Merged = True
                        ef.Worksheets(0).Cells.GetSubrange(sFirstCell, sLastCell).Style = styleFooter

                        Dim strFooter As String = "TPS 803457597 RT 0001" & " - " & "TVQ 1222669355 TQ 0001" & vbCrLf
                        strFooter = strFooter & "1105 Cherbourg  Saint-Hubert  J3Y 6G5 QC" & vbCrLf
                        strFooter = strFooter & "idslivraisonexpress.com" & " - " & "idslivraisonexpress@gmail.com" & vbCrLf



                        ef.Worksheets(0).Cells(irow, 2).Value = strFooter



                        Dim sFile As String
                        Dim Icount As Integer = 0
                        ef.Worksheets(0).PrintOptions.FitWorksheetWidthToPages = 1





                        sFile = drOrganisation("Organisation").ToString.Trim & "-" & strPeriode & ".pdf"
                        sFile = sFile.Replace("'", "")

CHECKFILE:
                        ' if the file name already exist, create a copy
                        If System.IO.File.Exists(go_Globals.LocationFacture & "\" & strPeriode & "\" & sFile) Then
                            Icount = Icount + 1
                            sFile = drOrganisation("Organisation").ToString.Trim & "-" & strPeriode & "_COPY" & Icount.ToString & ".pdf"
                            GoTo CHECKFILE
                        End If


                        ef.Save(go_Globals.LocationFacture & "\" & strPeriode & "\" & sFile)
                        ef = Nothing


                        If chkPRINT.Checked = True Then
                            GemBox.Pdf.ComponentInfo.SetLicense("AD0G-Y35C-Z376-E74D")

                            Using document As PdfDocument = PdfDocument.Load(go_Globals.LocationFacture & "\" & strPeriode & "\" & sFile)
                                ' Print PDF document to default printer (e.g. 'Microsoft Print to Pdf').
                                Dim printerName As String = Nothing
                                document.Print(printerName)
                            End Using
                        End If



                        Dim oRapportFacturePaye As New RapportFacturePaye
                        oRapportFacturePaye.IDPeriode = IDPeriode
                        oRapportFacturePaye.IDOrganisation = drOrganisation("OrganisationID")
                        oRapportFacturePaye.Organisation = If(IsNothing(drOrganisation("Organisation")), Nothing, drOrganisation("Organisation").ToString.Trim)
                        oRapportFacturePaye.IDLivreur = Nothing
                        oRapportFacturePaye.Livreur = Nothing
                        oRapportFacturePaye.Email = If(IsNothing(drOrganisation("Email")), Nothing, drOrganisation("Email").ToString.Trim)
                        oRapportFacturePaye.Email1 = If(IsNothing(drOrganisation("Email1")), Nothing, drOrganisation("Email1").ToString.Trim)
                        oRapportFacturePaye.Email2 = If(IsNothing(drOrganisation("Email2")), Nothing, drOrganisation("Email2").ToString.Trim)
                        oRapportFacturePaye.CheminRapport = go_Globals.LocationFacture & "\" & strPeriode & "\" & sFile
                        oRapportFacturePaye.NomFichier = sFile
                        oRapportFacturePaye.DateFrom = dDateFrom
                        oRapportFacturePaye.DateTo = dDateTo
                        oRapportFacturePaye.IsCommissionVendeur = False
                        oRapportFacturePayeData.Add(oRapportFacturePaye)


                        dblCredits = 0
                        dblDebits = 0
                        dblTotalAmount = 0
                        dblCadeauOrganisation = 0
                        dblGrandTotalTPS = 0
                        dblGrandTotalTVQ = 0
                        dblGrandTotal = 0
                        iQuantiteLivraison = 0
                        dblCreditParLivraison = 0
                        dblTotalParQteeLivraisons = 0


                    Next

                    ProgressBarControl1.PerformStep()
                    ProgressBarControl1.Update()
                Next

            End If
            ImprimerFacturation = True
            ProgressBarControl1.EditValue = 0
            Process.Start(go_Globals.LocationFacture & "\" & strPeriode)
            Me.Cursor = Cursors.Default

        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
            ImprimerFacturation = False
        End Try


    End Function
    Private Sub CalculerCommissionsTotauxPaye()


        On Error GoTo ERR


        Dim oCadeauLivreurData As New CadeauOrganisationData
        Dim frenchCultureInfo = New Globalization.CultureInfo("fr-CA")
        Dim dtPayeLine As DataTable
        Dim drInvoiceLine As DataRow
        Dim strPeriode As String = vbNullString
        Dim dDateFrom As Date = tbDateFrom.Text
        Dim dDateTo As Date = tbDateTo.Text
        Dim irow As Integer = 0
        Dim dtPayeDate As Date
        Dim dblTotalAmount As Double = 0
        Dim dblCadeauLivreur As Double = 0
        Dim dblGrandTotal As Double = 0
        Dim dblGrandTotalTPS As Double = 0
        Dim dblGrandTotalTVQ As Double = 0
        Dim dtLivreur As DataTable
        Dim drlivreur As DataRow
        Dim dtPaye As DataTable
        Dim drPaye As DataRow
        Dim dtLivreurAvecPaye As DataTable
        Dim drLivreurAvecPaye As DataRow
        Dim dtRapportFacturePaye As New DataTable
        Dim drRapportFacturePaye As DataRow
        Dim iIDLivreur As String = ""
        Dim iIDLivreurString As String = ""
        Dim iIDRapport As Integer = 0
        Dim strSelectedValue As String = ""
        Dim iSelectedValue As Integer = 0
        Dim IDPaye As Integer = 0




        If dDateFrom.Month <> dDateTo.Month Then
            strPeriode = dDateFrom.Day & " " & GetMonthName(dDateFrom, frenchCultureInfo) & "-" & dDateTo.Day & " " & GetMonthName(dDateTo, frenchCultureInfo) & " " & dDateTo.Year.ToString
        Else
            strPeriode = dDateFrom.Day & "-" & dDateTo.Day & " " & GetMonthName(dDateTo, frenchCultureInfo) & " " & dDateTo.Year.ToString
        End If

        For Each item As DevExpress.XtraEditors.Controls.CheckedListBoxItem In cbPayes.Properties.Items
            If item.CheckState = CheckState.Checked Then
                strSelectedValue = strSelectedValue & item.Value & ","
                iSelectedValue = iSelectedValue + 1
            End If
        Next

        If strSelectedValue.ToString.Trim <> "" Then
            strSelectedValue = strSelectedValue.Remove(strSelectedValue.Length - 1)
        End If




        If iSelectedValue > 0 Then
            'iIDRapport = cbPayes.EditValue
            dtRapportFacturePaye = oRapportFacturePayeData.SelectAllByID(strSelectedValue)
            If dtRapportFacturePaye.Rows.Count > 0 Then
                For Each drRapportFacturePaye In dtRapportFacturePaye.Rows
                    iIDLivreur = If(IsDBNull(drRapportFacturePaye("IDLivreur")), 0, CType(drRapportFacturePaye("IDLivreur"), Int32?))
                    iIDLivreurString = iIDLivreurString & iIDLivreur & ","

                    dDateFrom = drRapportFacturePaye("DateFrom")
                    dDateTo = drRapportFacturePaye("DateTo")

                Next

                iIDLivreurString = iIDLivreurString.Remove(iIDLivreurString.Length - 1)

            End If

        Else
        End If


        dtLivreurAvecPaye = oPayeData.SelectLivreurAvecPayeStringCommission(dDateFrom, dDateTo, iIDLivreurString)
        If dtLivreurAvecPaye.Rows.Count > 0 Then
            ProgressBarControl1.Properties.Step = 1
            ProgressBarControl1.Properties.PercentView = True
            ProgressBarControl1.Properties.Maximum = dtLivreurAvecPaye.Rows.Count
            ProgressBarControl1.Properties.Minimum = 0

            For Each drLivreurAvecPaye In dtLivreurAvecPaye.Rows



                dtLivreur = oLivreurData.SelectAllByID(drLivreurAvecPaye("IDLivreur"))
                For Each drlivreur In dtLivreur.Rows

                    dtPaye = oPayeData.SelectPayeByDateCommission(drlivreur("LivreurID"))
                    If dtPaye.Rows.Count > 0 Then

                        For Each drPaye In dtPaye.Rows

                            IDPaye = drPaye("IDPaye")
                            dtPayeDate = drPaye("PayeDate")

                            irow = 8
                            Dim sMonth As String = GetMonthName(Now, frenchCultureInfo).ToString

                            Select Case sMonth
                                Case "janvier"
                                    sMonth = "Janv"
                                Case "février"
                                    sMonth = "Fév"
                                Case "mars"
                                    sMonth = "Mars"
                                Case "avril"
                                    sMonth = "Avr"
                                Case "mai"
                                    sMonth = "Mai"
                                Case "juin"
                                    sMonth = "Juin"
                                Case "juillet"
                                    sMonth = "Juil"
                                Case "août"
                                    sMonth = "Août"
                                Case "septembre"
                                    sMonth = "Sept"
                                Case "octobre"
                                    sMonth = "Oct"
                                Case "novembre"
                                    sMonth = "Nov"
                                Case "décembre"
                                    sMonth = "Déc"

                            End Select


                            '--------------------------------------------
                            ' Genere le detail de la facture
                            '--------------------------------------------
                            dtPayeLine = oPayeLineData.SelectAllByDateCommission(drlivreur("LivreurID"))



                            Dim dtInvDate As Date
                            If dtPayeLine.Rows.Count > 0 Then
                                For Each drInvoiceLine In dtPayeLine.Rows


                                    dtInvDate = drInvoiceLine("PayeDate")
                                    Select Case drInvoiceLine("TypeAjout")
                                        Case 0 ' Ajout
                                            dblTotalAmount = dblTotalAmount + If(IsDBNull(drInvoiceLine("Montant")), 0, CType(drInvoiceLine("Montant"), Decimal?))
                                        Case 1 ' Glacier
                                            dblTotalAmount = dblTotalAmount + If(IsDBNull(drInvoiceLine("Montant")), 0, CType(drInvoiceLine("Montant"), Decimal?))
                                        Case 2 ' Credit

                                            dblTotalAmount = dblTotalAmount - If(IsDBNull(drInvoiceLine("Montant")), 0, CType(drInvoiceLine("Montant"), Decimal?))
                                        Case 3 ' Debit
                                            dblTotalAmount = dblTotalAmount + If(IsDBNull(drInvoiceLine("Montant")), 0, CType(drInvoiceLine("Montant"), Decimal?))

                                        Case 4 ' Extra
                                            dblTotalAmount = dblTotalAmount + If(IsDBNull(drInvoiceLine("Montant")), 0, CType(drInvoiceLine("Montant"), Decimal?))

                                        Case Else
                                            dblTotalAmount = dblTotalAmount + If(IsDBNull(drInvoiceLine("Montant")), 0, CType(drInvoiceLine("Montant"), Decimal?))
                                    End Select

                                    irow = irow + 1



                                Next
                            End If


                            irow = irow + 1
                        Next
                    End If



                    Dim oPaye As New Paye
                    oPaye.IDPaye = IDPaye

                    dblGrandTotal = dblGrandTotal + dblTotalAmount
                    oPaye.Amount = dblGrandTotal
                    '--------------------------------------------
                    ' Calcule le total
                    '--------------------------------------------
                    oPaye.TVQ = 0
                    oPaye.TPS = 0
                    oPayeData.UpdateAmount(oPaye, oPaye)

                    dblTotalAmount = 0
                    dblCadeauLivreur = 0
                    dblGrandTotalTPS = 0
                    dblGrandTotalTVQ = 0
                    dblGrandTotal = 0


                Next

                ProgressBarControl1.PerformStep()
                ProgressBarControl1.Update()


            Next
        End If

        ProgressBarControl1.EditValue = 0

        MessageBox.Show("Calculation des commissions vendeurs complète!")
        Exit Sub


ERR:
        MessageBox.Show(Err.Description)




    End Sub
    Private Sub CalculerCommissionsTotaux()


        On Error GoTo ERR


        Dim oCadeauLivreurData As New CadeauOrganisationData
        Dim frenchCultureInfo = New Globalization.CultureInfo("fr-CA")
        Dim dtPayeLine As DataTable
        Dim drInvoiceLine As DataRow
        Dim strPeriode As String = vbNullString
        Dim dDateFrom As Date = tbDateFrom.Text
        Dim dDateTo As Date = tbDateTo.Text
        Dim irow As Integer = 0
        Dim dtPayeDate As Date
        Dim dblTotalAmount As Double = 0
        Dim dblCadeauLivreur As Double = 0
        Dim dblGrandTotal As Double = 0
        Dim dblGrandTotalTPS As Double = 0
        Dim dblGrandTotalTVQ As Double = 0
        Dim dtLivreur As DataTable
        Dim drlivreur As DataRow
        Dim dtPaye As DataTable
        Dim drPaye As DataRow
        Dim dtLivreurAvecPaye As DataTable
        Dim drLivreurAvecPaye As DataRow
        Dim dtRapportFacturePaye As New DataTable
        Dim drRapportFacturePaye As DataRow
        Dim iIDLivreur As String = ""
        Dim iIDLivreurString As String = ""
        Dim iIDRapport As Integer = 0
        Dim strSelectedValue As String = ""
        Dim iSelectedValue As Integer = 0
        Dim IDPaye As Integer = 0




        If dDateFrom.Month <> dDateTo.Month Then
            strPeriode = dDateFrom.Day & " " & GetMonthName(dDateFrom, frenchCultureInfo) & "-" & dDateTo.Day & " " & GetMonthName(dDateTo, frenchCultureInfo) & " " & dDateTo.Year.ToString
        Else
            strPeriode = dDateFrom.Day & "-" & dDateTo.Day & " " & GetMonthName(dDateTo, frenchCultureInfo) & " " & dDateTo.Year.ToString
        End If

        For Each item As DevExpress.XtraEditors.Controls.CheckedListBoxItem In cbPayes.Properties.Items
            If item.CheckState = CheckState.Checked Then
                strSelectedValue = strSelectedValue & item.Value & ","
                iSelectedValue = iSelectedValue + 1
            End If
        Next

        If strSelectedValue.ToString.Trim <> "" Then
            strSelectedValue = strSelectedValue.Remove(strSelectedValue.Length - 1)
        End If




        If iSelectedValue > 0 Then
            'iIDRapport = cbPayes.EditValue
            dtRapportFacturePaye = oRapportFacturePayeData.SelectAllByID(strSelectedValue)
            If dtRapportFacturePaye.Rows.Count > 0 Then
                For Each drRapportFacturePaye In dtRapportFacturePaye.Rows
                    iIDLivreur = If(IsDBNull(drRapportFacturePaye("IDLivreur")), 0, CType(drRapportFacturePaye("IDLivreur"), Int32?))
                    iIDLivreurString = iIDLivreurString & iIDLivreur & ","

                    dDateFrom = drRapportFacturePaye("DateFrom")
                    dDateTo = drRapportFacturePaye("DateTo")

                Next

                iIDLivreurString = iIDLivreurString.Remove(iIDLivreurString.Length - 1)

            End If

        Else
        End If


        dtLivreurAvecPaye = oPayeData.SelectLivreurAvecPayeStringCommission(dDateFrom, dDateTo, iIDLivreurString)
        If dtLivreurAvecPaye.Rows.Count > 0 Then
            ProgressBarControl1.Properties.Step = 1
            ProgressBarControl1.Properties.PercentView = True
            ProgressBarControl1.Properties.Maximum = dtLivreurAvecPaye.Rows.Count
            ProgressBarControl1.Properties.Minimum = 0

            For Each drLivreurAvecPaye In dtLivreurAvecPaye.Rows



                dtLivreur = oLivreurData.SelectAllByID(drLivreurAvecPaye("IDLivreur"))
                For Each drlivreur In dtLivreur.Rows

                    dtPaye = oPayeData.SelectPayeByDateCommission(drlivreur("LivreurID"))
                    If dtPaye.Rows.Count > 0 Then

                        For Each drPaye In dtPaye.Rows

                            IDPaye = drPaye("IDPaye")
                            dtPayeDate = drPaye("PayeDate")

                            irow = 8
                            Dim sMonth As String = GetMonthName(Now, frenchCultureInfo).ToString

                            Select Case sMonth
                                Case "janvier"
                                    sMonth = "Janv"
                                Case "février"
                                    sMonth = "Fév"
                                Case "mars"
                                    sMonth = "Mars"
                                Case "avril"
                                    sMonth = "Avr"
                                Case "mai"
                                    sMonth = "Mai"
                                Case "juin"
                                    sMonth = "Juin"
                                Case "juillet"
                                    sMonth = "Juil"
                                Case "août"
                                    sMonth = "Août"
                                Case "septembre"
                                    sMonth = "Sept"
                                Case "octobre"
                                    sMonth = "Oct"
                                Case "novembre"
                                    sMonth = "Nov"
                                Case "décembre"
                                    sMonth = "Déc"

                            End Select


                            '--------------------------------------------
                            ' Genere le detail de la facture
                            '--------------------------------------------
                            dtPayeLine = oPayeLineData.SelectAllByDateCommission(drlivreur("LivreurID"))



                            Dim dtInvDate As Date
                            If dtPayeLine.Rows.Count > 0 Then
                                For Each drInvoiceLine In dtPayeLine.Rows


                                    dtInvDate = drInvoiceLine("PayeDate")
                                    Select Case drInvoiceLine("TypeAjout")
                                        Case 0 ' Ajout
                                            dblTotalAmount = dblTotalAmount + If(IsDBNull(drInvoiceLine("Montant")), 0, CType(drInvoiceLine("Montant"), Decimal?))
                                        Case 1 ' Glacier
                                            dblTotalAmount = dblTotalAmount + If(IsDBNull(drInvoiceLine("Montant")), 0, CType(drInvoiceLine("Montant"), Decimal?))
                                        Case 2 ' Credit

                                            dblTotalAmount = dblTotalAmount - If(IsDBNull(drInvoiceLine("Montant")), 0, CType(drInvoiceLine("Montant"), Decimal?))
                                        Case 3 ' Debit
                                            dblTotalAmount = dblTotalAmount + If(IsDBNull(drInvoiceLine("Montant")), 0, CType(drInvoiceLine("Montant"), Decimal?))

                                        Case 4 ' Extra
                                            dblTotalAmount = dblTotalAmount + If(IsDBNull(drInvoiceLine("Montant")), 0, CType(drInvoiceLine("Montant"), Decimal?))

                                        Case Else
                                            dblTotalAmount = dblTotalAmount + If(IsDBNull(drInvoiceLine("Montant")), 0, CType(drInvoiceLine("Montant"), Decimal?))
                                    End Select

                                    irow = irow + 1



                                Next
                            End If


                            irow = irow + 1
                        Next
                    End If



                    Dim oPaye As New Paye
                    oPaye.IDPaye = IDPaye

                    dblGrandTotal = dblGrandTotal + dblTotalAmount
                    oPaye.Amount = dblGrandTotal
                    '--------------------------------------------
                    ' Calcule le total
                    '--------------------------------------------
                    oPaye.TVQ = 0
                    oPaye.TPS = 0
                    oPayeData.UpdateAmount(oPaye, oPaye)

                    dblTotalAmount = 0
                    dblCadeauLivreur = 0
                    dblGrandTotalTPS = 0
                    dblGrandTotalTVQ = 0
                    dblGrandTotal = 0


                Next

                ProgressBarControl1.PerformStep()
                ProgressBarControl1.Update()


            Next
        End If

        ProgressBarControl1.EditValue = 0

        MessageBox.Show("Calculation des commissions vendeurs complète!")
        Exit Sub


ERR:
        MessageBox.Show(Err.Description)




    End Sub
    Private Sub CalculerPayeTotaux()


        On Error GoTo ERR


        Dim oCadeauLivreurData As New CadeauOrganisationData
        Dim frenchCultureInfo = New Globalization.CultureInfo("fr-CA")
        Dim dtPayeLine As DataTable
        Dim drInvoiceLine As DataRow
        Dim strPeriode As String = vbNullString
        Dim dDateFrom As Date = tbDateFrom.Text
        Dim dDateTo As Date = tbDateTo.Text
        Dim irow As Integer = 0
        Dim dtPayeDate As Date
        Dim dblTotalAmount As Double = 0
        Dim dblCadeauLivreur As Double = 0
        Dim dblGrandTotal As Double = 0
        Dim dblGrandTotalTPS As Double = 0
        Dim dblGrandTotalTVQ As Double = 0
        Dim dtLivreur As DataTable
        Dim drlivreur As DataRow
        Dim dtPaye As DataTable
        Dim drPaye As DataRow
        Dim dtLivreurAvecPaye As DataTable
        Dim drLivreurAvecPaye As DataRow
        Dim dtRapportFacturePaye As New DataTable
        Dim drRapportFacturePaye As DataRow
        Dim iIDLivreur As String = ""
        Dim iIDLivreurString As String = ""
        Dim iIDRapport As Integer = 0
        Dim strSelectedValue As String = ""
        Dim iSelectedValue As Integer = 0
        Dim IDPaye As Integer = 0




        If dDateFrom.Month <> dDateTo.Month Then
            strPeriode = dDateFrom.Day & " " & GetMonthName(dDateFrom, frenchCultureInfo) & "-" & dDateTo.Day & " " & GetMonthName(dDateTo, frenchCultureInfo) & " " & dDateTo.Year.ToString
        Else
            strPeriode = dDateFrom.Day & "-" & dDateTo.Day & " " & GetMonthName(dDateTo, frenchCultureInfo) & " " & dDateTo.Year.ToString
        End If

        For Each item As DevExpress.XtraEditors.Controls.CheckedListBoxItem In cbPayes.Properties.Items
            If item.CheckState = CheckState.Checked Then
                strSelectedValue = strSelectedValue & item.Value & ","
                iSelectedValue = iSelectedValue + 1
            End If
        Next

        If strSelectedValue.ToString.Trim <> "" Then
            strSelectedValue = strSelectedValue.Remove(strSelectedValue.Length - 1)
        End If




        If iSelectedValue > 0 Then
            'iIDRapport = cbPayes.EditValue
            dtRapportFacturePaye = oRapportFacturePayeData.SelectAllByID(strSelectedValue)
            If dtRapportFacturePaye.Rows.Count > 0 Then
                For Each drRapportFacturePaye In dtRapportFacturePaye.Rows
                    iIDLivreur = If(IsDBNull(drRapportFacturePaye("IDLivreur")), 0, CType(drRapportFacturePaye("IDLivreur"), Int32?))
                    iIDLivreurString = iIDLivreurString & iIDLivreur & ","

                    dDateFrom = drRapportFacturePaye("DateFrom")
                    dDateTo = drRapportFacturePaye("DateTo")

                Next

                iIDLivreurString = iIDLivreurString.Remove(iIDLivreurString.Length - 1)

            End If

        Else
        End If


        dtLivreurAvecPaye = oPayeData.SelectLivreurAvecPayeString(dDateFrom, dDateTo, iIDLivreurString)
        If dtLivreurAvecPaye.Rows.Count > 0 Then
            ProgressBarControl1.Properties.Step = 1
            ProgressBarControl1.Properties.PercentView = True
            ProgressBarControl1.Properties.Maximum = dtLivreurAvecPaye.Rows.Count
            ProgressBarControl1.Properties.Minimum = 0

            For Each drLivreurAvecPaye In dtLivreurAvecPaye.Rows



                dtLivreur = oLivreurData.SelectAllByID(drLivreurAvecPaye("IDLivreur"))
                For Each drlivreur In dtLivreur.Rows

                    dtPaye = oPayeData.SelectPayeByDate(drlivreur("LivreurID"))
                    If dtPaye.Rows.Count > 0 Then

                        For Each drPaye In dtPaye.Rows

                            IDPaye = drPaye("IDPaye")
                            dtPayeDate = drPaye("PayeDate")

                            irow = 8
                            Dim sMonth As String = GetMonthName(Now, frenchCultureInfo).ToString

                            Select Case sMonth
                                Case "janvier"
                                    sMonth = "Janv"
                                Case "février"
                                    sMonth = "Fév"
                                Case "mars"
                                    sMonth = "Mars"
                                Case "avril"
                                    sMonth = "Avr"
                                Case "mai"
                                    sMonth = "Mai"
                                Case "juin"
                                    sMonth = "Juin"
                                Case "juillet"
                                    sMonth = "Juil"
                                Case "août"
                                    sMonth = "Août"
                                Case "septembre"
                                    sMonth = "Sept"
                                Case "octobre"
                                    sMonth = "Oct"
                                Case "novembre"
                                    sMonth = "Nov"
                                Case "décembre"
                                    sMonth = "Déc"

                            End Select


                            '--------------------------------------------
                            ' Genere le detail de la facture
                            '--------------------------------------------
                            dtPayeLine = oPayeLineData.SelectAllByDate(drlivreur("LivreurID"))



                            Dim dtInvDate As Date
                            If dtPayeLine.Rows.Count > 0 Then
                                For Each drInvoiceLine In dtPayeLine.Rows


                                    dtInvDate = drInvoiceLine("PayeDate")
                                    Select Case drInvoiceLine("TypeAjout")
                                        Case 0 ' Ajout
                                            dblTotalAmount = dblTotalAmount + If(IsDBNull(drInvoiceLine("Montant")), 0, CType(drInvoiceLine("Montant"), Decimal?))
                                        Case 1 ' Glacier
                                            dblTotalAmount = dblTotalAmount + If(IsDBNull(drInvoiceLine("Montant")), 0, CType(drInvoiceLine("Montant"), Decimal?))
                                        Case 2 ' Credit

                                            dblTotalAmount = dblTotalAmount - If(IsDBNull(drInvoiceLine("Montant")), 0, CType(drInvoiceLine("Montant"), Decimal?))
                                        Case 3 ' Debit
                                            dblTotalAmount = dblTotalAmount + If(IsDBNull(drInvoiceLine("Montant")), 0, CType(drInvoiceLine("Montant"), Decimal?))

                                        Case 4 ' Extra
                                            dblTotalAmount = dblTotalAmount + If(IsDBNull(drInvoiceLine("Montant")), 0, CType(drInvoiceLine("Montant"), Decimal?))

                                        Case Else
                                            dblTotalAmount = dblTotalAmount + If(IsDBNull(drInvoiceLine("Montant")), 0, CType(drInvoiceLine("Montant"), Decimal?))
                                    End Select

                                    irow = irow + 1



                                Next
                            End If


                            irow = irow + 1
                        Next
                    End If



                    Dim oPaye As New Paye
                    oPaye.IDPaye = IDPaye

                    dblGrandTotal = dblGrandTotal + dblTotalAmount
                    oPaye.Amount = dblGrandTotal
                    '--------------------------------------------
                    ' Calcule le total
                    '--------------------------------------------

                    If drlivreur("CalculerTaxes") = True Then
                        dblGrandTotalTPS = dblGrandTotal * go_Globals.TPS
                        dblGrandTotalTVQ = dblGrandTotal * go_Globals.TVQ
                        oPaye.TPS = Math.Round(dblGrandTotalTPS, 2)
                        oPaye.TVQ = Math.Round(dblGrandTotalTVQ, 2)
                        oPayeData.UpdateAmount(oPaye, oPaye)

                    Else
                        oPaye.TVQ = 0
                        oPaye.TPS = 0
                        oPayeData.UpdateAmount(oPaye, oPaye)
                    End If





                    dblTotalAmount = 0
                    dblCadeauLivreur = 0
                    dblGrandTotalTPS = 0
                    dblGrandTotalTVQ = 0
                    dblGrandTotal = 0


                Next

                ProgressBarControl1.PerformStep()
                ProgressBarControl1.Update()


            Next
        End If

        ProgressBarControl1.EditValue = 0

        MessageBox.Show("Calculation des payes complete!")
        Exit Sub


ERR:
        MessageBox.Show(Err.Description)




    End Sub

    Private Function ImprimerCommissions() As Boolean


        On Error GoTo ERR


        Dim oCadeauLivreurData As New CadeauOrganisationData
        Dim frenchCultureInfo = New Globalization.CultureInfo("fr-CA")
        Dim dtCommissionLine As DataTable
        Dim drInvoiceLine As DataRow
        Dim strPeriode As String = vbNullString
        Dim dDateFrom As Date = tbDateFrom.Text
        Dim dDateTo As Date = tbDateTo.Text
        Dim irow As Integer = 0
        Dim dtCommissionDate As Date
        Dim dblTotalAmount As Double = 0
        Dim dblCadeauLivreur As Double = 0
        Dim dblGrandTotal As Double = 0
        Dim dblGrandTotalTPS As Double = 0
        Dim dblGrandTotalTVQ As Double = 0
        Dim dtLivreur As DataTable
        Dim drlivreur As DataRow
        Dim dtCommission As DataTable
        Dim drCommission As DataRow
        Dim dtVendeurAvecCommission As DataTable
        Dim drVendeurAvecCommission As DataRow
        Dim dtRapportCommission As New DataTable
        Dim drRapportCommission As DataRow
        Dim iIDLivreur As String = ""
        Dim iIDLivreurString As String = ""
        Dim iIDRapport As Integer = 0
        Dim strSelectedValue As String = ""
        Dim iSelectedValue As Integer = 0
        Dim IDCommission As Integer = 0

        SpreadsheetInfo.SetLicense("ERDC-FN4O-YKYN-4DBM")

        '--------------------------------------------
        ' Genere la periode
        '--------------------------------------------



        If dDateFrom.Month <> dDateTo.Month Then
            strPeriode = dDateFrom.Day & " " & GetMonthName(dDateFrom, frenchCultureInfo) & "-" & dDateTo.Day & " " & GetMonthName(dDateTo, frenchCultureInfo) & " " & dDateTo.Year.ToString
        Else
            strPeriode = dDateFrom.Day & "-" & dDateTo.Day & " " & GetMonthName(dDateTo, frenchCultureInfo) & " " & dDateTo.Year.ToString
        End If


        My.Computer.FileSystem.CreateDirectory(go_Globals.LocationCommissions & "\" & strPeriode)


        For Each item As DevExpress.XtraEditors.Controls.CheckedListBoxItem In cbCommissions.Properties.Items
            If item.CheckState = CheckState.Checked Then
                strSelectedValue = strSelectedValue & item.Value & ","
                iSelectedValue = iSelectedValue + 1
            End If
        Next

        If strSelectedValue.ToString.Trim <> "" Then
            strSelectedValue = strSelectedValue.Remove(strSelectedValue.Length - 1)
        End If




        If iSelectedValue > 0 Then

            dtRapportCommission = oRapportCommissionData.SelectAllByID(strSelectedValue)
            If dtRapportCommission.Rows.Count > 0 Then
                For Each drRapportCommission In dtRapportCommission.Rows

                    iIDLivreur = If(IsDBNull(drRapportCommission("IDLivreur")), 0, CType(drRapportCommission("IDLivreur"), Int32?))
                    iIDLivreurString = iIDLivreurString & iIDLivreur & ","

                    iIDRapport = drRapportCommission("IDRapport")
                    dDateFrom = drRapportCommission("DateFrom")
                    dDateTo = drRapportCommission("DateTo")

                    If My.Computer.FileSystem.FileExists(drRapportCommission("CheminRapport")) Then
                        My.Computer.FileSystem.DeleteFile(drRapportCommission("CheminRapport"))
                    End If

                    Dim oRapportCommission As New RapportCommission
                    oRapportCommission.IDRapportCommission = iIDRapport
                    oRapportCommissionData.Delete(oRapportCommission)

                Next

                iIDLivreurString = iIDLivreurString.Remove(iIDLivreurString.Length - 1)

            End If

        Else
            Dim directoryName As String = go_Globals.LocationCommissions & "\" & strPeriode & "\"
            For Each deleteFile In Directory.GetFiles(directoryName, "*.*", SearchOption.TopDirectoryOnly)
                File.Delete(deleteFile)
            Next

            Dim oRapportCommission As New RapportCommission
            oRapportCommission.IDPeriode = IDPeriode
            oRapportCommissionData.DeletePeriodLCommission(oRapportCommission)

        End If


        dtVendeurAvecCommission = oCommissionData.SelectLivreurAvecPayeStringCommission(dDateFrom, dDateTo, iIDLivreurString)
        If dtVendeurAvecCommission.Rows.Count > 0 Then
            ProgressBarControl1.Properties.Step = 1
            ProgressBarControl1.Properties.PercentView = True
            ProgressBarControl1.Properties.Maximum = dtVendeurAvecCommission.Rows.Count
            ProgressBarControl1.Properties.Minimum = 0

            For Each drVendeurAvecCommission In dtVendeurAvecCommission.Rows



                dtLivreur = oLivreurData.SelectAllByID(drVendeurAvecCommission("IDLivreur"))
                For Each drlivreur In dtLivreur.Rows

                    Dim sExcelPath As String = Application.StartupPath() & "\Commission.xlsx"
                    Dim ef As ExcelFile = ExcelFile.Load(sExcelPath)

                    '--------------------------------------------
                    ' Genere le header de la facture
                    '--------------------------------------------
                    ef.Worksheets(0).Cells(1, 1).Value = IIf(drlivreur.IsNull("Livreur"), "", drlivreur("Livreur").ToString.Trim)
                    ef.Worksheets(0).Cells(2, 1).Value = IIf(drlivreur.IsNull("Adresse"), "", drlivreur("Adresse").ToString.Trim)
                    ef.Worksheets(0).Cells(3, 1).Value = IIf(drlivreur.IsNull("Email"), "", drlivreur("Email").ToString.Trim)
                    ef.Worksheets(0).Cells(4, 1).Value = IIf(drlivreur.IsNull("Telephone"), "", drlivreur("Telephone").ToString.Trim)

                    '--------------------------------------------
                    ' Selectionne les info de la paye
                    '--------------------------------------------
                    dtCommission = oCommissionData.SelectPayeByDateCommission(drlivreur("LivreurID"))
                    If dtCommission.Rows.Count > 0 Then

                        For Each drCommission In dtCommission.Rows

                            IDCommission = drCommission("IDCommission")
                            dtCommissionDate = drCommission("CommissionDate")

                            irow = 6
                            Dim sMonth As String = GetMonthName(Now, frenchCultureInfo).ToString

                            Select Case sMonth
                                Case "janvier"
                                    sMonth = "Janv"
                                Case "février"
                                    sMonth = "Fév"
                                Case "mars"
                                    sMonth = "Mars"
                                Case "avril"
                                    sMonth = "Avr"
                                Case "mai"
                                    sMonth = "Mai"
                                Case "juin"
                                    sMonth = "Juin"
                                Case "juillet"
                                    sMonth = "Juil"
                                Case "août"
                                    sMonth = "Août"
                                Case "septembre"
                                    sMonth = "Sept"
                                Case "octobre"
                                    sMonth = "Oct"
                                Case "novembre"
                                    sMonth = "Nov"
                                Case "décembre"
                                    sMonth = "Déc"

                            End Select



                            ef.Worksheets(0).Cells(2, 5).Value = Now.Day & " " & sMonth & " " & Now.Year
                            ef.Worksheets(0).Cells(3, 5).Value = drCommission("CommissionNumber").ToString.Trim
                            ef.Worksheets(0).Cells(5, 0).Value = strPeriode

                            '--------------------------------------------
                            ' Genere le detail de la facture
                            '--------------------------------------------
                            dtCommissionLine = oCommissionLineData.SelectAllByDateCommission(IDCommission)



                            Dim dtInvDate As Date
                            If dtCommissionLine.Rows.Count > 0 Then
                                For Each drInvoiceLine In dtCommissionLine.Rows

                                    'If Not IsDBNull(drInvoiceLine("OrganisationID")) Then
                                    '    Dim oOrg As New Organisations
                                    '    Dim oOrg1 As New Organisations
                                    '    oOrg.OrganisationID = drInvoiceLine("OrganisationID")
                                    '    oOrg1 = oOrganisationData.Select_Record(oOrg)
                                    '    If Not IsNothing(oOrg1) Then
                                    '        Select Case oOrg1.MetodeCalculationLivreur
                                    '            Case 7
                                    '                dtInvDate = drInvoiceLine("PayeDate")
                                    '                ef.Worksheets(0).Cells(irow, 0).Value = dtInvDate
                                    '            Case Else
                                    '                ef.Worksheets(0).Cells(irow, 0).Value = strPeriode
                                    '        End Select
                                    '    Else
                                    '        ef.Worksheets(0).Cells(irow, 0).Value = strPeriode
                                    '    End If
                                    ef.Worksheets(0).Cells(irow, 0).Value = strPeriode





                                    Select Case drInvoiceLine("Description").ToString.Trim
                                        Case "Extras"
                                            ef.Worksheets(0).Cells(irow, 1).Value = "Extras"
                                        Case "Glaciers"
                                            ef.Worksheets(0).Cells(irow, 1).Value = "Glaciers"

                                        Case Else

                                            ef.Worksheets(0).Cells(irow, 1).Value = drInvoiceLine("Description").ToString.Trim
                                    End Select




                                    ef.Worksheets(0).Cells(irow, 3).Value = IIf(drInvoiceLine.IsNull("Unite"), 0, drInvoiceLine("Unite"))
                                    ef.Worksheets(0).Cells(irow, 4).Value = If(IsDBNull(drInvoiceLine("Prix")), 0, CType(drInvoiceLine("Prix"), Decimal?))
                                    ef.Worksheets(0).Cells(irow, 5).Style.NumberFormat = "$0.00"
                                    ef.Worksheets(0).Cells(irow, 5).Value = If(IsDBNull(drInvoiceLine("Montant")), 0, CType(drInvoiceLine("Montant"), Decimal?))

                                    Select Case drInvoiceLine("TypeAjout")
                                        Case 0 ' Ajout
                                            ef.Worksheets(0).Cells(irow, 1).Value = drInvoiceLine("Description").ToString.Trim
                                            ef.Worksheets(0).Cells(irow, 5).Style.NumberFormat = "$0.00"
                                            dblTotalAmount = dblTotalAmount + If(IsDBNull(drInvoiceLine("Montant")), 0, CType(drInvoiceLine("Montant"), Decimal?))
                                        Case 1 ' Glacier
                                            ef.Worksheets(0).Cells(irow, 1).Value = drInvoiceLine("Description").ToString.Trim
                                            ef.Worksheets(0).Cells(irow, 5).Style.NumberFormat = "$0.00"
                                            dblTotalAmount = dblTotalAmount + If(IsDBNull(drInvoiceLine("Montant")), 0, CType(drInvoiceLine("Montant"), Decimal?))
                                        Case 2 ' Credit
                                            ef.Worksheets(0).Cells(irow, 1).Value = drInvoiceLine("Description").ToString.Trim
                                            ef.Worksheets(0).Cells(irow, 4).Style.Font.Color = SpreadsheetColor.FromName(ColorName.Red)
                                            ef.Worksheets(0).Cells(irow, 4).Style.NumberFormat = "$0.00"
                                            ef.Worksheets(0).Cells(irow, 4).Value = If(IsDBNull(drInvoiceLine("Prix")), 0, CType(drInvoiceLine("Prix"), Decimal?))


                                            dblTotalAmount = dblTotalAmount - If(IsDBNull(drInvoiceLine("Montant")), 0, CType(drInvoiceLine("Montant"), Decimal?))
                                            ef.Worksheets(0).Cells(irow, 5).Style.Font.Color = SpreadsheetColor.FromName(ColorName.Red)
                                            ef.Worksheets(0).Cells(irow, 5).Style.NumberFormat = "$0.00"
                                            ef.Worksheets(0).Cells(irow, 5).Value = If(IsDBNull(drInvoiceLine("Montant")), 0, CType(drInvoiceLine("Montant"), Decimal?))
                                        Case 3 ' Debit
                                            ef.Worksheets(0).Cells(irow, 1).Value = drInvoiceLine("Description").ToString.Trim
                                            dblTotalAmount = dblTotalAmount + If(IsDBNull(drInvoiceLine("Montant")), 0, CType(drInvoiceLine("Montant"), Decimal?))

                                        Case 4 ' Extra
                                            ef.Worksheets(0).Cells(irow, 1).Value = drInvoiceLine("Description").ToString.Trim
                                            dblTotalAmount = dblTotalAmount + If(IsDBNull(drInvoiceLine("Montant")), 0, CType(drInvoiceLine("Montant"), Decimal?))


                                        Case Else
                                            ef.Worksheets(0).Cells(irow, 1).Value = drInvoiceLine("Organisation").ToString.Trim
                                            dblTotalAmount = dblTotalAmount + If(IsDBNull(drInvoiceLine("Montant")), 0, CType(drInvoiceLine("Montant"), Decimal?))
                                    End Select

                                    irow = irow + 1
                                    ef.Worksheets(0).Cells(irow, 5).Style.NumberFormat = "$0.00"
                                Next
                            End If


                            irow = irow + 1
                            ef.Worksheets(0).Cells(irow, 5).Style.NumberFormat = "$0.00"
                        Next
                    End If

                    ef.Worksheets(0).Cells(irow, 5).Style.NumberFormat = "$0.00"

                    Dim oCommission As New Commission
                    oCommission.IDCommission = IDCommission

                    dblGrandTotal = dblGrandTotal + dblTotalAmount
                    oCommission.Amount = dblGrandTotal
                    '--------------------------------------------
                    ' Calcule le total
                    '--------------------------------------------

                    If drlivreur("CalculerTaxes") = True Then
                        dblGrandTotalTPS = dblGrandTotal * go_Globals.TPS
                        dblGrandTotalTVQ = dblGrandTotal * go_Globals.TVQ

                        ef.Worksheets(0).Cells(34, 4).Value = "TPS - 5%"
                        ef.Worksheets(0).Cells(35, 4).Value = "TVQ - 9.975%"
                        ef.Worksheets(0).Cells(34, 5).Style.NumberFormat = "$0.00"
                        ef.Worksheets(0).Cells(34, 5).Value = Math.Round(dblGrandTotalTPS, 2)
                        ef.Worksheets(0).Cells(35, 5).Style.NumberFormat = "$0.00"
                        oCommission.TPS = Math.Round(dblGrandTotalTPS, 2)
                        ef.Worksheets(0).Cells(35, 5).Value = Math.Round(dblGrandTotalTVQ, 2)
                        oCommission.TVQ = Math.Round(dblGrandTotalTVQ, 2)
                        ef.Worksheets(0).Cells(37, 5).Style.NumberFormat = "$0.00"
                        ef.Worksheets(0).Cells(37, 5).Value = Math.Round(dblGrandTotal + dblGrandTotalTPS + dblGrandTotalTVQ, 2)
                        oCommissionData.UpdateAmount(oCommission, oCommission)

                    Else

                        ef.Worksheets(0).Cells(37, 4).Value = "Total"
                        ef.Worksheets(0).Cells(37, 5).Style.NumberFormat = "$0.00"
                        ef.Worksheets(0).Cells(37, 5).Value = Math.Round(dblGrandTotal, 2)
                        oCommission.TVQ = 0
                        oCommission.TPS = 0
                        oCommissionData.UpdateAmount(oCommission, oCommission)
                    End If




                    Dim sFile As String
                    Dim Icount As Integer = 0
                    ef.Worksheets(0).PrintOptions.FitWorksheetWidthToPages = 1



                    sFile = drlivreur("Livreur").ToString.Trim & "-" & strPeriode & "-Commissions.pdf"
                    sFile = sFile.Replace("'", "")

CHECKFILE:
                    ' if the file name already exist, create a copy
                    If System.IO.File.Exists(go_Globals.LocationCommissions & "\" & strPeriode & "\" & sFile) Then
                        Icount = Icount + 1
                        sFile = drlivreur("Livreur").ToString.Trim & "-" & strPeriode & "_COPY" & Icount.ToString & "-Commissions.pdf"
                        GoTo CHECKFILE
                    End If


                    ef.Save(go_Globals.LocationCommissions & "\" & strPeriode & "\" & sFile)
                    ef = Nothing



                    Dim oRapportCommission As New RapportCommission
                    oRapportCommission.IDPeriode = IDPeriode
                    oRapportCommission.IDLivreur = drlivreur("LivreurID")
                    oRapportCommission.CheminRapport = go_Globals.LocationCommissions & "\" & strPeriode & "\" & sFile
                    oRapportCommission.NomFichier = sFile
                    oRapportCommission.Email = If(IsNothing(drlivreur("Email")), Nothing, drlivreur("Email").ToString.Trim)
                    oRapportCommission.Livreur = If(IsNothing(drlivreur("Livreur")), Nothing, drlivreur("Livreur").ToString.Trim)
                    oRapportCommission.DateFrom = dDateFrom
                    oRapportCommission.DateTo = dDateTo
                    oRapportCommissionData.Add(oRapportCommission)

                    dblTotalAmount = 0
                    dblCadeauLivreur = 0
                    dblGrandTotalTPS = 0
                    dblGrandTotalTVQ = 0
                    dblGrandTotal = 0


                Next

                ProgressBarControl1.PerformStep()
                ProgressBarControl1.Update()


            Next
        End If

        ProgressBarControl1.EditValue = 0
        Process.Start(go_Globals.LocationCommissions & "\" & strPeriode)
        MessageBox.Show("Génération des commissions complétés!.", "Commissions", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1)
        ImprimerCommissions = True
        Exit Function

ERR:

        MessageBox.Show(Err.Description)
        ImprimerCommissions = False




    End Function




    Private Function ImprimerPaie() As Boolean


        On Error GoTo ERR


        Dim oCadeauLivreurData As New CadeauOrganisationData
        Dim frenchCultureInfo = New Globalization.CultureInfo("fr-FR")
        Dim dtPayeLine As DataTable
        Dim drInvoiceLine As DataRow
        Dim strPeriode As String = vbNullString
        Dim dDateFrom As Date = tbDateFrom.Text
        Dim dDateTo As Date = tbDateTo.Text
        Dim irow As Integer = 0
        Dim dtPayeDate As Date
        Dim dblTotalAmount As Double = 0
        Dim dblCadeauLivreur As Double = 0
        Dim dblGrandTotal As Double = 0
        Dim dblGrandTotalTPS As Double = 0
        Dim dblGrandTotalTVQ As Double = 0
        Dim dtLivreur As DataTable
        Dim drlivreur As DataRow
        Dim dtPaye As DataTable
        Dim drPaye As DataRow
        Dim dtLivreurAvecPaye As DataTable
        Dim drLivreurAvecPaye As DataRow
        Dim dtRapportFacturePaye As New DataTable
        Dim drRapportFacturePaye As DataRow
        Dim iIDLivreur As String = ""
        Dim iIDLivreurString As String = ""
        Dim iIDRapport As Integer = 0
        Dim strSelectedValue As String = ""
        Dim iSelectedValue As Integer = 0
        Dim IDPaye As Integer = 0

        SpreadsheetInfo.SetLicense("ERDC-FN4O-YKYN-4DBM")

        '--------------------------------------------
        ' Genere la periode
        '--------------------------------------------



        If dDateFrom.Month <> dDateTo.Month Then
            strPeriode = dDateFrom.Day & " " & GetMonthName(dDateFrom, frenchCultureInfo) & "-" & dDateTo.Day & " " & GetMonthName(dDateTo, frenchCultureInfo) & " " & dDateTo.Year.ToString
        Else
            strPeriode = dDateFrom.Day & "-" & dDateTo.Day & " " & GetMonthName(dDateTo, frenchCultureInfo) & " " & dDateTo.Year.ToString
        End If


        My.Computer.FileSystem.CreateDirectory(go_Globals.LocationPaie & "\" & strPeriode)


        For Each item As DevExpress.XtraEditors.Controls.CheckedListBoxItem In cbPayes.Properties.Items
            If item.CheckState = CheckState.Checked Then
                strSelectedValue = strSelectedValue & item.Value & ","
                iSelectedValue = iSelectedValue + 1
            End If
        Next

        If strSelectedValue.ToString.Trim <> "" Then
            strSelectedValue = strSelectedValue.Remove(strSelectedValue.Length - 1)
        End If




        If iSelectedValue > 0 Then

            dtRapportFacturePaye = oRapportFacturePayeData.SelectAllByID(strSelectedValue)
            If dtRapportFacturePaye.Rows.Count > 0 Then
                For Each drRapportFacturePaye In dtRapportFacturePaye.Rows



                    iIDLivreur = If(IsDBNull(drRapportFacturePaye("IDLivreur")), 0, CType(drRapportFacturePaye("IDLivreur"), Int32?))
                    iIDLivreurString = iIDLivreurString & iIDLivreur & ","

                    iIDRapport = drRapportFacturePaye("IDRapport")
                    dDateFrom = drRapportFacturePaye("DateFrom")
                    dDateTo = drRapportFacturePaye("DateTo")

                    If My.Computer.FileSystem.FileExists(drRapportFacturePaye("CheminRapport")) Then
                        My.Computer.FileSystem.DeleteFile(drRapportFacturePaye("CheminRapport"))
                    End If

                    Dim oRapportFacture As New RapportFacturePaye
                    oRapportFacture.IDRapport = iIDRapport
                    oRapportFacturePayeData.Delete(oRapportFacture)

                Next

                iIDLivreurString = iIDLivreurString.Remove(iIDLivreurString.Length - 1)

            End If

        Else
            Dim directoryName As String = go_Globals.LocationPaie & "\" & strPeriode & "\"
            For Each deleteFile In Directory.GetFiles(directoryName, "*.*", SearchOption.TopDirectoryOnly)
                File.Delete(deleteFile)
            Next

            Dim oRapportFacture As New RapportFacturePaye
            oRapportFacture.IDPeriode = IDPeriode
            oRapportFacturePayeData.DeletePeriodL(oRapportFacture)
        End If


        dtLivreurAvecPaye = oPayeData.SelectLivreurAvecPayeString(dDateFrom, dDateTo, iIDLivreurString)
        If dtLivreurAvecPaye.Rows.Count > 0 Then
            ProgressBarControl1.Properties.Step = 1
            ProgressBarControl1.Properties.PercentView = True
            ProgressBarControl1.Properties.Maximum = dtLivreurAvecPaye.Rows.Count
            ProgressBarControl1.Properties.Minimum = 0

            For Each drLivreurAvecPaye In dtLivreurAvecPaye.Rows



                dtLivreur = oLivreurData.SelectAllByID(drLivreurAvecPaye("IDLivreur"))
                For Each drlivreur In dtLivreur.Rows

                    Dim sExcelPath As String = Application.StartupPath() & "\Commission.xlsx"
                    Dim ef As ExcelFile = ExcelFile.Load(sExcelPath)

                    '--------------------------------------------
                    ' Genere le header de la facture
                    '--------------------------------------------
                    ef.Worksheets(0).Cells(1, 1).Value = IIf(drlivreur.IsNull("Livreur"), "", drlivreur("Livreur").ToString.Trim)
                    ef.Worksheets(0).Cells(2, 1).Value = IIf(drlivreur.IsNull("Adresse"), "", drlivreur("Adresse").ToString.Trim)
                    ef.Worksheets(0).Cells(3, 1).Value = IIf(drlivreur.IsNull("Email"), "", drlivreur("Email").ToString.Trim)
                    ef.Worksheets(0).Cells(4, 1).Value = IIf(drlivreur.IsNull("Telephone"), "", drlivreur("Telephone").ToString.Trim)

                    '--------------------------------------------
                    ' Selectionne les info de la paye
                    '--------------------------------------------
                    dtPaye = oPayeData.SelectPayeByDate(drlivreur("LivreurID"))
                    If dtPaye.Rows.Count > 0 Then

                        For Each drPaye In dtPaye.Rows

                            IDPaye = drPaye("IDPaye")
                            dtPayeDate = drPaye("PayeDate")

                            irow = 6
                            Dim sMonth As String = GetMonthName(Now, frenchCultureInfo).ToString

                            Select Case sMonth
                                Case "janvier"
                                    sMonth = "Janv"
                                Case "février"
                                    sMonth = "Fév"
                                Case "mars"
                                    sMonth = "Mars"
                                Case "avril"
                                    sMonth = "Avr"
                                Case "mai"
                                    sMonth = "Mai"
                                Case "juin"
                                    sMonth = "Juin"
                                Case "juillet"
                                    sMonth = "Juil"
                                Case "août"
                                    sMonth = "Août"
                                Case "septembre"
                                    sMonth = "Sept"
                                Case "octobre"
                                    sMonth = "Oct"
                                Case "novembre"
                                    sMonth = "Nov"
                                Case "décembre"
                                    sMonth = "Déc"

                            End Select



                            ef.Worksheets(0).Cells(2, 5).Value = Now.Day & " " & sMonth & " " & Now.Year
                            ef.Worksheets(0).Cells(3, 5).Value = drPaye("PayeNumber").ToString.Trim
                            ef.Worksheets(0).Cells(5, 0).Value = strPeriode

                            '--------------------------------------------
                            ' Genere le detail de la facture
                            '--------------------------------------------
                            dtPayeLine = oPayeLineData.SelectAllByDate(drlivreur("LivreurID"))



                            Dim dtInvDate As Date
                            If dtPayeLine.Rows.Count > 0 Then
                                For Each drInvoiceLine In dtPayeLine.Rows

                                    If Not IsDBNull(drInvoiceLine("OrganisationID")) Then
                                        Dim oOrg As New Organisations
                                        Dim oOrg1 As New Organisations
                                        oOrg.OrganisationID = drInvoiceLine("OrganisationID")
                                        oOrg1 = oOrganisationData.Select_Record(oOrg)
                                        If Not IsNothing(oOrg1) Then
                                            Select Case oOrg1.MetodeCalculationLivreur
                                                Case 7
                                                    dtInvDate = drInvoiceLine("PayeDate")
                                                    ef.Worksheets(0).Cells(irow, 0).Value = strPeriode
                                                Case Else
                                                    ef.Worksheets(0).Cells(irow, 0).Value = strPeriode
                                            End Select
                                        Else
                                            ef.Worksheets(0).Cells(irow, 0).Value = strPeriode
                                        End If




                                    Else
                                        ef.Worksheets(0).Cells(irow, 0).Value = strPeriode
                                    End If








                                    Select Case drInvoiceLine("Description").ToString.Trim
                                        Case "Extras"
                                            ef.Worksheets(0).Cells(irow, 1).Value = "Extras"
                                        Case "Glaciers"
                                            ef.Worksheets(0).Cells(irow, 1).Value = "Glaciers"

                                        Case Else

                                            ef.Worksheets(0).Cells(irow, 1).Value = drInvoiceLine("Organisation").ToString.Trim
                                    End Select




                                    ef.Worksheets(0).Cells(irow, 3).Value = IIf(drInvoiceLine.IsNull("Unite"), 0, drInvoiceLine("Unite"))

                                    ef.Worksheets(0).Cells(irow, 4).Style.NumberFormat = "$0.00"
                                    ef.Worksheets(0).Cells(irow, 4).Style.HorizontalAlignment = HorizontalAlignmentStyle.Center

                                    ef.Worksheets(0).Cells(irow, 4).Value = If(IsDBNull(drInvoiceLine("Prix")), 0, CType(drInvoiceLine("Prix"), Decimal?))


                                    ef.Worksheets(0).Cells(irow, 5).Style.NumberFormat = "$0.00"
                                    ef.Worksheets(0).Cells(irow, 5).Style.HorizontalAlignment = HorizontalAlignmentStyle.Right
                                    ef.Worksheets(0).Cells(irow, 5).Value = If(IsDBNull(drInvoiceLine("Montant")), 0, CType(drInvoiceLine("Montant"), Decimal?))

                                    Select Case drInvoiceLine("TypeAjout")
                                        Case 0 ' Ajout
                                            ef.Worksheets(0).Cells(irow, 1).Value = drInvoiceLine("Organisation").ToString.Trim

                                            dblTotalAmount = dblTotalAmount + If(IsDBNull(drInvoiceLine("Montant")), 0, CType(drInvoiceLine("Montant"), Decimal?))
                                        Case 1 ' Glacier
                                            ef.Worksheets(0).Cells(irow, 1).Value = drInvoiceLine("Description").ToString.Trim
                                            dblTotalAmount = dblTotalAmount + If(IsDBNull(drInvoiceLine("Montant")), 0, CType(drInvoiceLine("Montant"), Decimal?))
                                        Case 2 ' Credit
                                            ef.Worksheets(0).Cells(irow, 1).Value = drInvoiceLine("Description").ToString.Trim
                                            ef.Worksheets(0).Cells(irow, 4).Style.Font.Color = SpreadsheetColor.FromName(ColorName.Red)
                                            ef.Worksheets(0).Cells(irow, 4).Style.NumberFormat = "$0.00"
                                            ef.Worksheets(0).Cells(irow, 4).Value = If(IsDBNull(drInvoiceLine("Prix")), 0, CType(drInvoiceLine("Prix"), Decimal?))

                                            ef.Worksheets(0).Cells(irow, 5).Style.NumberFormat = "$0.00"
                                            ef.Worksheets(0).Cells(irow, 5).Style.HorizontalAlignment = HorizontalAlignmentStyle.Right
                                            dblTotalAmount = dblTotalAmount - If(IsDBNull(drInvoiceLine("Montant")), 0, CType(drInvoiceLine("Montant"), Decimal?))
                                            ef.Worksheets(0).Cells(irow, 5).Style.Font.Color = SpreadsheetColor.FromName(ColorName.Red)
                                            ef.Worksheets(0).Cells(irow, 5).Style.NumberFormat = "$0.00"
                                            ef.Worksheets(0).Cells(irow, 5).Value = If(IsDBNull(drInvoiceLine("Montant")), 0, CType(drInvoiceLine("Montant"), Decimal?))
                                        Case 3 ' Debit
                                            ef.Worksheets(0).Cells(irow, 1).Value = drInvoiceLine("Description").ToString.Trim
                                            dblTotalAmount = dblTotalAmount + If(IsDBNull(drInvoiceLine("Montant")), 0, CType(drInvoiceLine("Montant"), Decimal?))

                                        Case 4 ' Extra
                                            ef.Worksheets(0).Cells(irow, 1).Value = drInvoiceLine("Description").ToString.Trim
                                            dblTotalAmount = dblTotalAmount + If(IsDBNull(drInvoiceLine("Montant")), 0, CType(drInvoiceLine("Montant"), Decimal?))


                                        Case Else
                                            ef.Worksheets(0).Cells(irow, 1).Value = drInvoiceLine("Organisation").ToString.Trim
                                            dblTotalAmount = dblTotalAmount + If(IsDBNull(drInvoiceLine("Montant")), 0, CType(drInvoiceLine("Montant"), Decimal?))
                                    End Select
                                    ef.Worksheets(0).Cells(irow, 4).Style.NumberFormat = "$0.00"
                                    ef.Worksheets(0).Cells(irow, 4).Style.HorizontalAlignment = HorizontalAlignmentStyle.Center

                                    ef.Worksheets(0).Cells(irow, 5).Style.NumberFormat = "$0.00"
                                    ef.Worksheets(0).Cells(irow, 5).Style.HorizontalAlignment = HorizontalAlignmentStyle.Right




                                    irow = irow + 1

                                    ef.Worksheets(0).Cells(irow, 4).Style.NumberFormat = "$0.00"
                                    ef.Worksheets(0).Cells(irow, 4).Style.HorizontalAlignment = HorizontalAlignmentStyle.Center

                                    ef.Worksheets(0).Cells(irow, 5).Style.NumberFormat = "$0.00"
                                    ef.Worksheets(0).Cells(irow, 5).Style.HorizontalAlignment = HorizontalAlignmentStyle.Right


                                Next
                            End If
                            ef.Worksheets(0).Cells(irow, 4).Style.NumberFormat = "$0.00"
                            ef.Worksheets(0).Cells(irow, 4).Style.HorizontalAlignment = HorizontalAlignmentStyle.Center

                            ef.Worksheets(0).Cells(irow, 5).Style.NumberFormat = "$0.00"
                            ef.Worksheets(0).Cells(irow, 5).Style.HorizontalAlignment = HorizontalAlignmentStyle.Right


                            irow = irow + 1
                            ef.Worksheets(0).Cells(irow, 4).Style.NumberFormat = "$0.00"
                            ef.Worksheets(0).Cells(irow, 4).Style.HorizontalAlignment = HorizontalAlignmentStyle.Center

                            ef.Worksheets(0).Cells(irow, 5).Style.NumberFormat = "$0.00"
                            ef.Worksheets(0).Cells(irow, 5).Style.HorizontalAlignment = HorizontalAlignmentStyle.Right

                        Next
                    End If

                    ef.Worksheets(0).Cells(irow, 4).Style.NumberFormat = "$0.00"
                    ef.Worksheets(0).Cells(irow, 4).Style.HorizontalAlignment = HorizontalAlignmentStyle.Center


                    ef.Worksheets(0).Cells(irow, 5).Style.NumberFormat = "$0.00"
                    ef.Worksheets(0).Cells(irow, 5).Style.HorizontalAlignment = HorizontalAlignmentStyle.Right

                    Dim oPaye As New Paye
                    oPaye.IDPaye = IDPaye

                    dblGrandTotal = dblGrandTotal + dblTotalAmount
                    oPaye.Amount = dblGrandTotal
                    '--------------------------------------------
                    ' Calcule le total
                    '--------------------------------------------

                    If drlivreur("CalculerTaxes") = True Then
                        dblGrandTotalTPS = dblGrandTotal * go_Globals.TPS
                        dblGrandTotalTVQ = dblGrandTotal * go_Globals.TVQ

                        ef.Worksheets(0).Cells(34, 4).Value = "TPS - 5%"
                        ef.Worksheets(0).Cells(35, 4).Value = "TVQ - 9.975%"
                        ef.Worksheets(0).Cells(34, 5).Value = Math.Round(dblGrandTotalTPS, 2)
                        oPaye.TPS = Math.Round(dblGrandTotalTPS, 2)
                        ef.Worksheets(0).Cells(35, 5).Value = Math.Round(dblGrandTotalTVQ, 2)
                        oPaye.TVQ = Math.Round(dblGrandTotalTVQ, 2)
                        ef.Worksheets(0).Cells(37, 5).Value = Math.Round(dblGrandTotal + dblGrandTotalTPS + dblGrandTotalTVQ, 2)
                        oPayeData.UpdateAmount(oPaye, oPaye)

                    Else

                        ef.Worksheets(0).Cells(37, 4).Value = "Total"
                        ef.Worksheets(0).Cells(37, 5).Value = Math.Round(dblGrandTotal, 2)
                        oPaye.TVQ = 0
                        oPaye.TPS = 0
                        oPayeData.UpdateAmount(oPaye, oPaye)


                    End If




                    Dim sFile As String
                    Dim Icount As Integer = 0
                    ef.Worksheets(0).PrintOptions.FitWorksheetWidthToPages = 1



                    sFile = drlivreur("Livreur").ToString.Trim & "-" & strPeriode & ".pdf"
                    sFile = sFile.Replace("'", "")

CHECKFILE:
                    ' if the file name already exist, create a copy
                    If System.IO.File.Exists(go_Globals.LocationPaie & "\" & strPeriode & "\" & sFile) Then
                        Icount = Icount + 1
                        sFile = drlivreur("Livreur").ToString.Trim & "-" & strPeriode & "_COPY" & Icount.ToString & ".pdf"
                        GoTo CHECKFILE
                    End If


                    ef.Save(go_Globals.LocationPaie & "\" & strPeriode & "\" & sFile)
                    ef = Nothing


                    If chkPRINT.Checked = True Then
                        GemBox.Pdf.ComponentInfo.SetLicense("AD0G-Y35C-Z376-E74D")

                        Using document As PdfDocument = PdfDocument.Load(go_Globals.LocationPaie & "\" & strPeriode & "\" & sFile)
                            ' Print PDF document to default printer (e.g. 'Microsoft Print to Pdf').
                            Dim printerName As String = Nothing
                            document.Print(printerName)
                        End Using
                    End If


                    Dim oRapportFacturePaye As New RapportFacturePaye
                    oRapportFacturePaye.IDPeriode = IDPeriode
                    oRapportFacturePaye.IDOrganisation = Nothing
                    oRapportFacturePaye.IDLivreur = drlivreur("LivreurID")
                    oRapportFacturePaye.CheminRapport = go_Globals.LocationPaie & "\" & strPeriode & "\" & sFile
                    oRapportFacturePaye.NomFichier = sFile
                    oRapportFacturePaye.Email = If(IsNothing(drlivreur("Email")), Nothing, drlivreur("Email").ToString.Trim)
                    oRapportFacturePaye.Email1 = Nothing
                    oRapportFacturePaye.Email2 = Nothing
                    oRapportFacturePaye.Organisation = Nothing
                    oRapportFacturePaye.Livreur = If(IsNothing(drlivreur("Livreur")), Nothing, drlivreur("Livreur").ToString.Trim)
                    oRapportFacturePaye.DateFrom = dDateFrom
                    oRapportFacturePaye.DateTo = dDateTo
                    oRapportFacturePaye.IsCommissionVendeur = False
                    oRapportFacturePayeData.Add(oRapportFacturePaye)

                    dblTotalAmount = 0
                    dblCadeauLivreur = 0
                    dblGrandTotalTPS = 0
                    dblGrandTotalTVQ = 0
                    dblGrandTotal = 0


                Next

                ProgressBarControl1.PerformStep()
                ProgressBarControl1.Update()


            Next
        End If

        ProgressBarControl1.EditValue = 0
        Process.Start(go_Globals.LocationPaie & "\" & strPeriode)
        MessageBox.Show("Génération de la paye complété!.", "Paie", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1)
        ImprimerPaie = True
        Exit Function


ERR:

        MessageBox.Show(Err.Description)
        ImprimerPaie = False




    End Function
    Private Sub bImportMobilus_Click(sender As Object, e As EventArgs) Handles bImportMobilus.Click
        If IsNothing(cbPeriodes.EditValue) Then
            MessageBox.Show("Vous devez selectionner une periode")
            Exit Sub
        End If

        If txtMobilusXLS.Text.ToString.Trim = "" Then
            Exit Sub
        End If





        Dim fd As OpenFileDialog = New OpenFileDialog()

        Try


            Dim oData_Set As New DataSet
            Dim oLivreursData As New LivreursData
            Dim oOrganisationData As New OrganisationsData
            Dim dDateConvert As DateTime
            My.Application.ChangeCulture("en-US")
            Dim iIDNomFichierImport As Integer
            Dim dDateFrom As Date = tbDateFrom.Text
            Dim dDateTo As Date = tbDateTo.Text

            Dim oNomFichierImport As New NomFichierImport
            Dim oNomFichierImportData As New NomFichierImportData
            Dim dtNomFichierImport As DataTable
            Dim drNomFichierImport As DataRow
            Dim frenchCultureInfo = New Globalization.CultureInfo("fr-CA")
            Dim oPeriode As New Periodes
            Dim dtPeriode As New DataTable
            Dim drPeriode As DataRow
            Dim blnCheckDate As Boolean = False





            Microsoft.Win32.Registry.SetValue("HKEY_CURRENT_USER\Control Panel\International", "sShortDate", "M/d/yyyy")

            My.Application.ChangeCulture("en-US")
            IsLoading = True
            fd.Title = "Sélectionner votre fichier PDF mobilus"
            fd.InitialDirectory = "C:\"
            fd.Filter = "Excel files (.xls)|*.xls|Excel Files(.xlsx)|*.xlsx;"

            fd.FilterIndex = 2
            fd.RestoreDirectory = True


            If dDateFrom.Month <> dDateTo.Month Then
                oPeriode.Periode = dDateFrom.Day & " " & GetMonthName(dDateFrom, frenchCultureInfo) & "-" & dDateTo.Day & " " & GetMonthName(dDateTo, frenchCultureInfo) & " " & dDateTo.Year.ToString
            Else
                oPeriode.Periode = dDateFrom.Day & "-" & dDateTo.Day & " " & GetMonthName(dDateTo, frenchCultureInfo) & " " & dDateTo.Year.ToString
            End If

            dtPeriode = oPeriodeData.SelectAllByPeriode(go_Globals.IDPeriode)
            If dtPeriode.Rows.Count > 0 Then
                For Each drPeriode In dtPeriode.Rows
                    IDPeriode = drPeriode("IDPeriode")
                Next
            Else
                Dim result As DialogResult = MessageBox.Show("Cette période n'existe pas dans le système, désirez-vous la créer?", "Periode", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
                If result = DialogResult.No Then
                    Exit Sub
                ElseIf result = DialogResult.Yes Then
                    IDPeriode = oPeriodeData.Add(oPeriode)
                    FillAllPeriodes()
                    cbPeriodes.ItemIndex = IDPeriode

                End If

            End If


            ' CHECK IF SELECTED DATE IS BETWEEEN PERIOde dATES







            oNomFichierImport.NomFichierImport = Filename & " / " & oPeriode.Periode
            oNomFichierImport.DateFrom = tbDateFrom.Text
            oNomFichierImport.DateTo = tbDateTo.Text



            dtNomFichierImport = oNomFichierImportData.CheckIfNomFichierImportExist(oNomFichierImport.NomFichierImport)
            If dtNomFichierImport.Rows.Count > 0 Then
                For Each drNomFichierImport In dtNomFichierImport.Rows
                    Dim sMessageBox As String = "Le fichier " & drNomFichierImport("NomFichierImport").ToString.Trim & " a déja été importé!. L'import est annulé"
                    MessageBox.Show(sMessageBox)
                    Exit Sub
                Next

            Else
                oNomFichierImport.IDPeriode = IDPeriode
                iIDNomFichierImport = oNomFichierImportData.Add(oNomFichierImport)
                oNomFichierImport.IDNomFichierImport = iIDNomFichierImport
            End If




            Me.Cursor = Cursors.WaitCursor
            If txtMobilusXLS.Text.ToString.Trim <> "" Then

                SpreadsheetInfo.SetLicense("ERDC-FN4O-YKYN-4DBM")

                Dim ef As ExcelFile = ExcelFile.Load(txtMobilusXLS.Text)

                Dim idx As Integer = 0
                Dim irow As Integer = 0

                Me.Cursor = Cursors.WaitCursor

                For Each sheet As ExcelWorksheet In ef.Worksheets

                    Application.DoEvents()

                    Process.GetCurrentProcess().PriorityClass = ProcessPriorityClass.RealTime


                    ProgressBarControl1.Properties.Step = 1
                    ProgressBarControl1.Properties.PercentView = True
                    ProgressBarControl1.Properties.Maximum = sheet.Rows.Count / 2
                    ProgressBarControl1.Properties.Minimum = 0

                    Do While sheet.Cells(irow, 0).Value <> "Reference" And sheet.Cells(irow, 0).Value <> "Référence"
                        irow = irow + 1
                    Loop
                    irow = irow + 1

                    Do While sheet.Cells(irow, 0).Value <> Nothing


                        If sheet.Cells(irow, 0).Value.ToString.Trim = "Reference" Then
                            GoTo NEXTROW
                        End If


                        Dim oImport As New Import

                        Dim dDateImport As DateTime = Calendar.DateTime.AddMinutes(1)
                        oImport.DateImport = New DateTime(dDateImport.Year, dDateImport.Month, dDateImport.Day, dDateImport.Hour, dDateImport.Minute, "00")

                        Dim questionableDate As Date = oImport.DateImport
                        Dim fromDate As Date = dDateFrom
                        Dim toDate As Date = dDateTo

                        If (questionableDate >= fromDate) AndAlso (questionableDate <= toDate) Then

                        Else
                            MsgBox("Vous devez sélectionner un date entre les 2 dates de la période!")
                            Exit Sub
                        End If

                        oImport.IDNomFichierImport = iIDNomFichierImport

                        If Not IsNothing(sheet.Cells(irow, 0).Value.ToString.Trim) Then
                            oImport.Reference = Trim(sheet.Cells(irow, 0).Value.ToString.Trim)
                        Else
                            GoTo NEXTROW
                            oImport.Reference = Nothing
                        End If

                        Dim sCode As String = ""
                        Dim sCodeSplit() As String
                        If Not IsNothing(sheet.Cells(irow, 0).Value.ToString.Trim) Then
                            sCode = Trim(sheet.Cells(irow, 0).Value.ToString.Trim)
                            sCodeSplit = Split(sCode, "-")
                            oImport.Code = sCodeSplit(1)

                        Else
                            GoTo NEXTROW
                            oImport.Code = Nothing
                        End If


                        If Not IsNothing(sheet.Cells(irow + 1, 0).Value) Then
                            oImport.Status = Trim(sheet.Cells(irow + 1, 0).Value.ToString.Trim)
                        Else
                            GoTo NEXTROW
                            oImport.Status = Nothing
                        End If


                        If Not IsNothing(sheet.Cells(irow, 1).Value) Then
                            oImport.Client = SysTemGlobals.DoubleQuote(Trim(sheet.Cells(irow, 1).Value.ToString.Trim))
                        Else
                            oImport.Client = Nothing
                        End If

                        If Not IsNothing(sheet.Cells(irow + 1, 1).Value.ToString.Trim) Then
                            oImport.ClientAddress = SysTemGlobals.DoubleQuote(Trim(sheet.Cells(irow + 1, 1).Value.ToString.Trim))
                        Else
                            oImport.ClientAddress = Nothing
                        End If

                        If Not IsNothing(sheet.Cells(irow, 2).Value) Then
                            oImport.MomentDePassage = SysTemGlobals.DoubleQuote(Trim(sheet.Cells(irow, 2).Value.ToString.Trim))
                        Else
                            oImport.MomentDePassage = Nothing
                        End If

                        If Not IsNothing(sheet.Cells(irow, 3).Value) Then
                            oImport.PreparePar = SysTemGlobals.DoubleQuote(Trim(sheet.Cells(irow, 3).Value.ToString.Trim))
                        Else
                            oImport.PreparePar = Nothing
                        End If

                        My.Application.ChangeCulture("en-US")



                        If Not IsNothing(sheet.Cells(irow, 4).Value) Then


                            Dim sTemp As String = sheet.Cells(irow, 4).Value


                            If sTemp.Contains("-") Then

                                Dim sSplit() As String
                                Dim sHour As String

                                sHour = sTemp.Substring(sTemp.Length - 5)
                                Dim sYear As String = Calendar.StartDate.Year
                                sSplit = Split(sTemp, "-")
                                dDateConvert = New DateTime(sYear, sSplit(1), sSplit(2).Substring(0, 2).ToString.Trim, sHour.Substring(0, 2), sHour.Substring(3, 2), "00")
                                oImport.HeureAppel = dDateConvert


                            ElseIf sTemp.Contains("/") Then
                                Dim sSplit() As String
                                Dim sHour As String


                                sHour = sTemp.Substring(sTemp.Length - 5)
                                Dim sYear As String = Now.Year
                                sSplit = Split(sTemp, "/")



                                dDateConvert = New DateTime(sYear, sSplit(1), sSplit(0), sHour.Substring(0, 2), sHour.Substring(3, 2), "00")
                                'DateEdit1.EditValue = dDateConvert
                                'dDateConvert = DateEdit1.Text
                                oImport.HeureAppel = dDateConvert


                            End If


                        Else
                            oImport.HeureAppel = Nothing
                        End If


                        If Not IsNothing(sheet.Cells(irow, 5).Value) Then



                            Dim sTemp As String = sheet.Cells(irow, 5).Value.ToString


                            If sTemp.Contains("-") Then

                                Dim sSplit() As String
                                Dim sHour As String

                                sHour = sTemp.Substring(sTemp.Length - 5)
                                Dim sYear As String = Calendar.StartDate.Year

                                sSplit = Split(sTemp, "-")
                                dDateConvert = New DateTime(sYear, sSplit(1), sSplit(2).Substring(0, 2).ToString.Trim, sHour.Substring(0, 2), sHour.Substring(3, 2), "00")
                                oImport.HeurePrep = dDateConvert


                            ElseIf sTemp.Contains("/") Then
                                Dim sSplit() As String
                                Dim sHour As String


                                sHour = sTemp.Substring(sTemp.Length - 5)
                                Dim sYear As String = Now.Year
                                sSplit = Split(sTemp, "/")



                                dDateConvert = New DateTime(sYear, sSplit(1), sSplit(0), sHour.Substring(0, 2), sHour.Substring(3, 2), "00")
                                'DateEdit1.EditValue = dDateConvert
                                'dDateConvert = DateEdit1.Text
                                oImport.HeurePrep = dDateConvert


                            End If

                        Else
                            oImport.HeurePrep = Nothing
                        End If


                        If Not IsNothing(sheet.Cells(irow, 6).Value) Then
                            Dim sTemp As String = sheet.Cells(irow, 6).Value


                            If sTemp.Contains("-") Then

                                Dim sSplit() As String
                                Dim sHour As String

                                sHour = sTemp.Substring(sTemp.Length - 5)
                                Dim sYear As String = Calendar.StartDate.Year

                                sSplit = Split(sTemp, "-")
                                dDateConvert = New DateTime(sYear, sSplit(1), sSplit(2).Substring(0, 2).ToString.Trim, sHour.Substring(0, 2), sHour.Substring(3, 2), "00")
                                'DateEdit1.EditValue = dDateConvert
                                'dDateConvert = DateEdit1.Text
                                oImport.HeureDepart = dDateConvert

                                If blnCheckDate = False Then

                                    If dDateConvert >= dDateFrom AndAlso dDateConvert <= dDateTo Then
                                        Me.Cursor = Cursors.WaitCursor
                                        blnCheckDate = True
                                    Else
                                        Me.Cursor = Cursors.Default
                                        Dim result As DialogResult = MessageBox.Show("Les dates sur le fichier d'import ne sont pas valides avec cette période, Vous avez peut-etre importer un mauvais fichier", "Dates en dehors de la période", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                                        If result = DialogResult.OK Then
                                            oNomFichierImportData.Delete(oNomFichierImport)
                                            Exit Sub
                                        End If

                                    End If
                                End If



                            ElseIf sTemp.Contains("/") Then
                                Dim sSplit() As String
                                Dim sHour As String


                                sHour = sTemp.Substring(sTemp.Length - 5)
                                Dim sYear As String = Now.Year
                                sSplit = Split(sTemp, "/")



                                dDateConvert = New DateTime(sYear, sSplit(1), sSplit(0), sHour.Substring(0, 2), sHour.Substring(3, 2), "00")
                                'DateEdit1.EditValue = dDateConvert
                                'dDateConvert = DateEdit1.Text
                                oImport.HeureDepart = dDateConvert

                                If blnCheckDate = False Then

                                    If dDateConvert > dDateFrom AndAlso dDateConvert < dDateTo Then
                                        Me.Cursor = Cursors.WaitCursor
                                        blnCheckDate = True
                                    Else
                                        Me.Cursor = Cursors.Default
                                        Dim result As DialogResult = MessageBox.Show("Les dates sur le fichier d'import ne sont pas valides avec cette période, Vous avez peut-etre importer un mauvais fichier", "Dates en dehors de la période", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                                        If result = DialogResult.OK Then
                                            oNomFichierImportData.Delete(oNomFichierImport)
                                            Exit Sub
                                        End If

                                    End If
                                End If



                            End If

                        Else
                            oImport.HeureDepart = Nothing

                        End If



                        If Not IsNothing(sheet.Cells(irow, 7).Value) Then
                            Dim sTemp As String = sheet.Cells(irow, 7).Value


                            If sTemp.Contains("-") Then

                                Dim sSplit() As String
                                Dim sHour As String

                                sHour = sTemp.Substring(sTemp.Length - 5)
                                Dim sYear As String = Now.Year
                                sSplit = Split(sTemp, "-")
                                dDateConvert = New DateTime(sYear, sSplit(1), sSplit(2).Substring(0, 2).ToString.Trim, sHour.Substring(0, 2), sHour.Substring(3, 2), "00")
                                oImport.HeureLivraison = dDateConvert


                            ElseIf sTemp.Contains("/") Then
                                Dim sSplit() As String
                                Dim sHour As String


                                sHour = sTemp.Substring(sTemp.Length - 5)
                                Dim sYear As String = Now.Year
                                sSplit = Split(sTemp, "/")



                                dDateConvert = New DateTime(sYear, sSplit(1), sSplit(0), sHour.Substring(0, 2), sHour.Substring(3, 2), "00")
                                'DateEdit1.EditValue = dDateConvert
                                'dDateConvert = DateEdit1.Text
                                oImport.HeureLivraison = dDateConvert


                            End If

                        Else
                            oImport.HeureLivraison = Nothing
                        End If

                        Dim dDateImport2 As DateTime = Calendar.DateTime.AddMinutes(1)
                        oImport.DateImport = New DateTime(dDateImport2.Year, dDateImport2.Month, dDateImport2.Day, dDateImport2.Hour, dDateImport2.Minute, "00")




                        If Not IsNothing(sheet.Cells(irow, 8).Value) Then
                            oImport.MessagePassage = SysTemGlobals.DoubleQuote(Trim(sheet.Cells(irow, 8).Value.ToString.Trim))
                        Else
                            oImport.MessagePassage = Nothing
                        End If

                        If Not IsNothing(sheet.Cells(irow, 9).Value) Then
                            oImport.TotalTempService = Trim(sheet.Cells(irow, 9).Value.ToString.Trim)
                        Else
                            oImport.TotalTempService = Nothing
                        End If
                        Dim dt2 As DataTable
                        If Not IsNothing(sheet.Cells(irow, 10).Value) Then
                            oImport.Livreur = SysTemGlobals.DoubleQuote(Trim(sheet.Cells(irow, 10).Value.ToString.Trim))
                            oImport.Livreur = oImport.Livreur.ToString.Trim
                            Dim oLivreurs As New livreurs



                            dt2 = oLivreursData.SelectByName(oImport.Livreur.ToString.Trim)
                            If dt2.Rows.Count = 0 Then


                                oLivreurs.Livreur = SysTemGlobals.DoubleQuote(oImport.Livreur)
                                oLivreurs.LivreurCache = SysTemGlobals.DoubleQuote(oImport.Livreur)
                                oLivreurs.Adresse = Nothing
                                oLivreurs.Telephone = Nothing
                                oLivreurs.CalculerTaxes = 0
                                oImport.LivreurID = oLivreursData.Add(oLivreurs)
                                If oImport.LivreurID = 0 Then oImport.LivreurID = Nothing
                            Else

                                Dim drl As DataRow
                                For Each drl In dt2.Rows
                                    oImport.LivreurID = drl("LivreurID")
                                Next

                            End If
                        Else
                            oImport.Livreur = Nothing
                        End If

                        Dim dblDistance As Double = 0
                        If Not IsNothing(sheet.Cells(irow, 11).Value) Then
                            dblDistance = Trim(sheet.Cells(irow, 11).Value.ToString.Trim)
                            oImport.Distance = Math.Round(dblDistance, 2)
                        Else
                            oImport.Distance = 0
                        End If


                        If Not IsNothing(sheet.Cells(irow, 12).Value) Then


                            oImport.Organisation = SysTemGlobals.DoubleQuote(Trim(sheet.Cells(irow, 12).Value.ToString.Trim))



                            Dim oOrganisation As New Organisations

                            Dim strOrganisation As String = oImport.Organisation.ToString.Trim


                            dt2 = oOrganisationData.SelectByName(oImport.Organisation.ToString.Trim)
                            If dt2.Rows.Count = 0 Then
                                oOrganisation.Organisation = SysTemGlobals.DoubleQuote(oImport.Organisation.ToString.Trim)
                                oOrganisation.OrganisationImport = SysTemGlobals.DoubleQuote(oImport.Organisation.ToString.Trim)
                                oOrganisation.Telephone = Nothing
                                oOrganisation.Adresse = Nothing
                                oOrganisation.KMMax = Nothing
                                oOrganisation.MontantBonus = Nothing
                                oOrganisation.MontantContrat = Nothing
                                oOrganisation.CreditParLivraison = Nothing
                                oOrganisation.MetodeCalculationOrganisation = Nothing
                                oOrganisation.MetodeCalculationLivreur = Nothing
                                oOrganisation.QuantiteLivraison = Nothing
                                oOrganisation.MontantContraJour = Nothing
                                oOrganisation.MontantContraJourLivreur = Nothing
                                oImport.OrganisationID = oOrganisationData.Add(oOrganisation)
                                If oImport.OrganisationID = 0 Then oImport.OrganisationID = Nothing
                            Else
                                Dim drO As DataRow
                                For Each drO In dt2.Rows
                                    oImport.OrganisationID = drO("OrganisationID")
                                Next

                            End If
                        Else

                            oImport.Organisation = Nothing
                        End If


                        Select Case oImport.Organisation.ToString.Trim
                            Case "PJC 35"

                                If oImport.Client.ToString.Trim = "PJC 305" Then

                                    oImport.Echange = 1
                                Else
                                    oImport.Echange = 0
                                End If


                            Case "PJC 305"
                                If oImport.Client.ToString.Trim = "PJC 035" Then

                                    oImport.Echange = 1
                                Else
                                    oImport.Echange = 0
                                End If

                            Case Else

                                oImport.Echange = 0

                        End Select






                        oImport.IDPeriode = IDPeriode
                        oImport.Debit = 0
                        oImport.Credit = 0
                        oImport.NombreGlacier = 0
                        oImport.MontantGlacierLivreur = 0
                        oImport.MontantGlacierOrganisation = 0
                        oImport.MontantContrat = 0
                        oImport.ExtraKM = 0
                        oImport.MontantLivreur = 0
                        oImport.Montant = 0
                        oImport.Debit = 0
                        oImport.Credit = 0
                        oImport.NombreGlacier = 0
                        oImport.MontantContrat = 0
                        oImport.Extra = 0
                        oImport.ExtraKMLivreur = 0
                        oImport.ExtraLivreur = 0
                        oImport.IsMobilus = True
                        oImport.IDClientAddresse = Nothing

                        Dim bSucess As Boolean
                        Dim oDataClass As New ImportData
                        bSucess = oDataClass.Add(oImport)
                        If bSucess = True Then
                            FillNomFichierImport()
                        Else
                            Me.Cursor = Cursors.Default
                            MessageBox.Show("Insertion d'une ligne a echouee")
                            Exit Sub
                        End If







NEXTROW:

                        ProgressBarControl1.PerformStep()
                        ProgressBarControl1.Update()


                        If irow = 0 Then
                            irow = irow + 1
                        Else
                            irow = irow + 2
                        End If



                    Loop

                    GoTo FINISH

NEXTTAB:
                Next

            End If


FINISH:


            SysTemGlobals.ExecuteSQL("update Livreurs set Livreur = LTRIM(RTRIM(Livreur))")

            Me.Cursor = Cursors.WaitCursor

            oImportData.CopyImportToBackupByIDFile(oNomFichierImport.IDNomFichierImport)

            grdImport.DataSource = oImportData.SelectAllFromDate(tbDateFrom.Text, tbDateTo.Text)

            GridView1.OptionsView.ColumnAutoWidth = False
            'GridView1.BestFitColumns()
            Dim viewInfo As DevExpress.XtraGrid.Views.Grid.ViewInfo.GridViewInfo = TryCast(GridView1.GetViewInfo(), GridViewInfo)
            If viewInfo.ViewRects.ColumnTotalWidth < viewInfo.ViewRects.ColumnPanelWidth Then
                GridView1.OptionsView.ColumnAutoWidth = True
            End If

            ProgressBarControl1.EditValue = 0
            bImportMobilus.Enabled = False


            FillAllPeriodes()
            FillNomFichierImport()
            FillInvoiceNumber()
            FillPaieNumber()

            Process.GetCurrentProcess().PriorityClass = ProcessPriorityClass.AboveNormal

            Me.Cursor = Cursors.Default




            Dim sMessage As String = "Import complété avec succès"

            MessageBox.Show("Import complété avec succès")
            IsLoading = False
            Exit Sub

        Catch ex As Exception
            Me.Cursor = Cursors.Default
            MessageBox.Show(ex.Message, "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try



        Exit Sub
    End Sub

    Private Sub bSelectfile_Click(sender As Object, e As EventArgs) Handles bSelectfile.Click

        Dim fd As New OpenFileDialog


        fd.Title = "Sélectionner votre fichier PDF mobilus"
        fd.Filter = "Excel files (.xls)|*.xls|Excel Files(.xlsx)|*.xlsx;"

        fd.FilterIndex = 2
        fd.RestoreDirectory = True

        If fd.ShowDialog() = DialogResult.OK Then
            Filename = fd.FileName
            Filename = System.IO.Path.GetFileName(Filename)

            Application.DoEvents()
            txtMobilusXLS.Text = fd.FileName
            bImportMobilus.Enabled = True


        Else
            txtMobilusXLS.Text = Nothing
        End If
        Exit Sub

ERR:

        Me.Cursor = Cursors.Default
        MessageBox.Show(Err.Description)



        Exit Sub
    End Sub
    Public Function ImprimerLesCommissions() As Boolean


        Try

            Me.Cursor = Cursors.WaitCursor


            Dim dDateFrom As Date = tbDateFrom.Text
            Dim dDateTo As Date = tbDateTo.Text
            Dim oPeriode As New Periodes
            Dim dtPeriode As New DataTable
            Dim drPeriode As DataRow
            Dim frenchCultureInfo = New Globalization.CultureInfo("fr-CA")


            If dDateFrom.Month <> dDateTo.Month Then
                oPeriode.Periode = dDateFrom.Day & " " & GetMonthName(dDateFrom, frenchCultureInfo) & "-" & dDateTo.Day & " " & GetMonthName(dDateTo, frenchCultureInfo) & " " & dDateTo.Year.ToString
            Else
                oPeriode.Periode = dDateFrom.Day & "-" & dDateTo.Day & " " & GetMonthName(dDateTo, frenchCultureInfo) & " " & dDateTo.Year.ToString
            End If

            dtPeriode = oPeriodeData.SelectAllByPeriode(go_Globals.IDPeriode)
            If dtPeriode.Rows.Count > 0 Then
                For Each drPeriode In dtPeriode.Rows
                    IDPeriode = drPeriode("IDPeriode")
                Next
            Else
                MessageBox.Show("La période n'existe pas!, Veuillez la créer en appuisant sur <Filtrer>", "Période manquante", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1)
                Return False
                Exit Function
            End If

            ImprimerCommissions()


            FillPaieNumber()

            Return True
            Me.Cursor = Cursors.Default


        Catch ex As Exception
            Me.Cursor = Cursors.Default
            MessageBox.Show(ex.Message, "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try


    End Function
    Public Function ImprimerLaPaie() As Boolean


        Try

            Me.Cursor = Cursors.WaitCursor


            Dim dDateFrom As Date = tbDateFrom.Text
            Dim dDateTo As Date = tbDateTo.Text
            Dim oPeriode As New Periodes
            Dim dtPeriode As New DataTable
            Dim drPeriode As DataRow
            Dim frenchCultureInfo = New Globalization.CultureInfo("fr-CA")


            If dDateFrom.Month <> dDateTo.Month Then
                oPeriode.Periode = dDateFrom.Day & " " & GetMonthName(dDateFrom, frenchCultureInfo) & "-" & dDateTo.Day & " " & GetMonthName(dDateTo, frenchCultureInfo) & " " & dDateTo.Year.ToString
            Else
                oPeriode.Periode = dDateFrom.Day & "-" & dDateTo.Day & " " & GetMonthName(dDateTo, frenchCultureInfo) & " " & dDateTo.Year.ToString
            End If

            dtPeriode = oPeriodeData.SelectAllByPeriode(go_Globals.IDPeriode)
            If dtPeriode.Rows.Count > 0 Then
                For Each drPeriode In dtPeriode.Rows
                    IDPeriode = drPeriode("IDPeriode")
                Next
            Else
                MessageBox.Show("La période n'existe pas!, Veuillez la créer en appuisant sur <Filtrer>", "Période manquante", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1)
                Return False
                Exit Function
            End If

            ImprimerPaie()


            FillPaieNumber()

            Return True
            Me.Cursor = Cursors.Default


        Catch ex As Exception
            Me.Cursor = Cursors.Default
            MessageBox.Show(ex.Message, "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try


    End Function


    Private Sub bFactures_ItemClick(sender As Object, e As ItemClickEventArgs) Handles bFactures.ItemClick
        If frmFacture.Visible = True Then
            frmFacture.WindowState = FormWindowState.Normal
            frmFacture.BringToFront()
        End If

        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        frmFacture.Show()
        Me.Cursor = System.Windows.Forms.Cursors.Default
    End Sub

    Private Sub bPayes_ItemClick(sender As Object, e As ItemClickEventArgs) Handles bPayes.ItemClick
        If frmPaye.Visible = True Then
            frmPaye.WindowState = FormWindowState.Normal
            frmPaye.BringToFront()
        End If

        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        frmPaye.Show()
        Me.Cursor = System.Windows.Forms.Cursors.Default
    End Sub

    Private Sub bEnvoyerFacture_Click(sender As Object, e As EventArgs) Handles bEnvoyerFacture.Click
        Dim dDateFrom As Date = tbDateFrom.Text
        Dim dDateTo As Date = tbDateTo.Text
        Dim oPeriode As New Periodes
        Dim frenchCultureInfo = New Globalization.CultureInfo("fr-CA")
        Dim dtRapportFacturePaye As New DataTable
        Dim drRapportFacturePaye As DataRow
        Dim oMail As New Mail
        Dim iIDRapport As Integer = 0
        Dim strSelectedValue As String = ""
        Dim iSelectedValue As Integer = 0

        Dim strEmails As String = ""

        If IsNothing(cbPeriodes.EditValue) Then
            MessageBox.Show("Vous devez selectionner une periode")
            Exit Sub
        End If


        If dDateFrom.Month <> dDateTo.Month Then
            oPeriode.Periode = dDateFrom.Day & " " & GetMonthName(dDateFrom, frenchCultureInfo) & "-" & dDateTo.Day & " " & GetMonthName(dDateTo, frenchCultureInfo) & " " & dDateTo.Year.ToString
        Else
            oPeriode.Periode = dDateFrom.Day & "-" & dDateTo.Day & " " & GetMonthName(dDateTo, frenchCultureInfo) & " " & dDateTo.Year.ToString
        End If



        IDPeriode = cbPeriodes.EditValue



        Me.Cursor = Cursors.Default
        Dim result As DialogResult = MessageBox.Show("Envoyer les factures aux clients?", "Facturation", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
        If result = DialogResult.No Then
            Exit Sub
        ElseIf result = DialogResult.Yes Then


            Try
                Me.Cursor = Cursors.WaitCursor


                For Each item As DevExpress.XtraEditors.Controls.CheckedListBoxItem In cbFactures.Properties.Items
                    If item.CheckState = CheckState.Checked Then
                        strSelectedValue = strSelectedValue & item.Value & ","
                        iSelectedValue = iSelectedValue + 1
                    End If
                Next

                If strSelectedValue.ToString.Trim <> "" Then
                    strSelectedValue = strSelectedValue.Remove(strSelectedValue.Length - 1)
                End If


                dtRapportFacturePaye = oRapportFacturePayeData.SelectAllDataByPeriodesF(IDPeriode, strSelectedValue)
                If dtRapportFacturePaye.Rows.Count > 0 Then

                    Me.Cursor = Cursors.WaitCursor

                    ProgressBarControl1.Properties.Step = 1
                    ProgressBarControl1.Properties.PercentView = True
                    ProgressBarControl1.Properties.Maximum = dtRapportFacturePaye.Rows.Count
                    ProgressBarControl1.Properties.Minimum = 0


                    For Each drRapportFacturePaye In dtRapportFacturePaye.Rows


                        Dim oOrganisation As New Organisations
                        Dim oOrganisation2 As New Organisations
                        oOrganisation.OrganisationID = drRapportFacturePaye("IDOrganisation")
                        oOrganisation2 = oOrganisationData.Select_Record(oOrganisation)

                        If Not IsNothing(oOrganisation2) Then


                            strEmails = ""

                            Dim sEmail As String = If(IsNothing(oOrganisation2.Email), "", oOrganisation2.Email.ToString.Trim)
                            Dim sEmail1 As String = If(IsNothing(oOrganisation2.Email1), "", oOrganisation2.Email1.ToString.Trim)
                            Dim sEmail2 As String = If(IsNothing(oOrganisation2.Email2), "", oOrganisation2.Email2.ToString.Trim)
                            Dim sEmail3 As String = If(IsNothing(oOrganisation2.Email3), "", oOrganisation2.Email3.ToString.Trim)
                            Dim sSubject As String = "IDS Livraison Express:  Votre facture pour la période " & oPeriode.Periode
                            Dim sMessage As String = oOrganisation2.Organisation & ", Nous vous remercions de choisir IDS Livraison Express "
                            Dim sAttachment As String = If(IsNothing(drRapportFacturePaye("CheminRapport")), Nothing, drRapportFacturePaye("CheminRapport").ToString.Trim)

                            If sEmail.ToString.Trim <> "" Then
                                strEmails = strEmails & sEmail & ","
                            End If

                            If sEmail1.ToString.Trim <> "" Then
                                strEmails = strEmails & sEmail1 & ","
                            End If


                            If sEmail2.ToString.Trim <> "" Then
                                strEmails = strEmails & sEmail2 & ","
                            End If

                            If sEmail3.ToString.Trim <> "" Then
                                strEmails = strEmails & sEmail3 & ","
                            End If

                            If Not IsNothing(strEmails) And strEmails.ToString.Trim <> "" Then

                                strEmails = strEmails.Remove(strEmails.Length - 1)

                                Dim GGmail As New GGSMTP_GMAIL("idslivraisonexpressrapport@gmail.com", "2015livraison",  )

                                Dim ToAddressies As String() = {strEmails}
                                Dim attachs() As String = {sAttachment}
                                Dim subject As String = sSubject
                                Dim body As String = sMessage


                                result = GGmail.SendMail(ToAddressies, subject, body, attachs)
                                If result Then

                                Else
                                    MsgBox(GGmail.ErrorText, MsgBoxStyle.Critical)
                                End If
                            End If

                        Else
                            MessageBox.Show(drRapportFacturePaye("Organisation") & " a pas de courriels de configurés")

                        End If

                        ProgressBarControl1.PerformStep()
                        ProgressBarControl1.Update()

                    Next

                    ProgressBarControl1.EditValue = 0
                    Me.Cursor = Cursors.Default
                    MessageBox.Show("Envoi des factures complété avec succès?")


                Else
                    Me.Cursor = Cursors.Default
                    MessageBox.Show("Aucunes facture n'a été trouvé pour cette période!", "Factures non générées", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1)
                    Exit Sub
                End If
            Catch ex As Exception
                Me.Cursor = Cursors.Default
                MessageBox.Show(ex.Message, "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End Try


        End If
    End Sub

    Private Sub bEnvoyerPaye_Click(sender As Object, e As EventArgs) Handles bEnvoyerPaye.Click


        Dim dDateFrom As Date = tbDateFrom.Text
        Dim dDateTo As Date = tbDateTo.Text
        Dim oPeriode As New Periodes
        Dim frenchCultureInfo = New Globalization.CultureInfo("fr-CA")
        Dim oRapportFacturePaye As New RapportFacturePaye
        Dim dtRapportFacturePaye As New DataTable
        Dim drRapportFacturePaye As DataRow
        Dim oMail As New Mail
        Dim iIDRapport As Integer = 0
        Dim strSelectedValue As String = ""
        Dim iSelectedValue As Integer = 0

        If IsNothing(cbPeriodes.EditValue) Then
            MessageBox.Show("Vous devez selectionner une periode")
            Exit Sub
        End If


        If dDateFrom.Month <> dDateTo.Month Then
            oPeriode.Periode = dDateFrom.Day & " " & GetMonthName(dDateFrom, frenchCultureInfo) & "-" & dDateTo.Day & " " & GetMonthName(dDateTo, frenchCultureInfo) & " " & dDateTo.Year.ToString
        Else
            oPeriode.Periode = dDateFrom.Day & "-" & dDateTo.Day & " " & GetMonthName(dDateTo, frenchCultureInfo) & " " & dDateTo.Year.ToString
        End If

        IDPeriode = cbPeriodes.EditValue

        Me.Cursor = Cursors.Default
        Dim result As DialogResult = MessageBox.Show("Envoyer la paie au livreurs?", "Paie", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
        If result = DialogResult.No Then
            Exit Sub
        ElseIf result = DialogResult.Yes Then

            Try

                Me.Cursor = Cursors.WaitCursor

                For Each item As DevExpress.XtraEditors.Controls.CheckedListBoxItem In cbPayes.Properties.Items
                    If item.CheckState = CheckState.Checked Then
                        strSelectedValue = strSelectedValue & item.Value & ","
                        iSelectedValue = iSelectedValue + 1
                    End If
                Next

                If strSelectedValue.ToString.Trim <> "" Then
                    strSelectedValue = strSelectedValue.Remove(strSelectedValue.Length - 1)
                End If



                dtRapportFacturePaye = oRapportFacturePayeData.SelectAllDataByPeriodesP(IDPeriode, strSelectedValue)
                If dtRapportFacturePaye.Rows.Count > 0 Then
                    Me.Cursor = Cursors.WaitCursor
                    ProgressBarControl1.Properties.Step = 1
                    ProgressBarControl1.Properties.PercentView = True
                    ProgressBarControl1.Properties.Maximum = dtRapportFacturePaye.Rows.Count
                    ProgressBarControl1.Properties.Minimum = 0

                    For Each drRapportFacturePaye In dtRapportFacturePaye.Rows

                        Dim olivreur As New livreurs
                        Dim olivreur2 As New livreurs
                        olivreur.LivreurID = drRapportFacturePaye("IDLivreur")
                        olivreur2 = oLivreurData.Select_Record(olivreur)

                        If Not IsNothing(olivreur2) Then

                            Dim sEmails As String

                            sEmails = ""

                            Dim sEmail As String = If(IsNothing(olivreur2.Email), "", olivreur2.Email.ToString.Trim)
                            Dim sEmail1 As String = If(IsNothing(olivreur2.Email1), "", olivreur2.Email1.ToString.Trim)
                            Dim sEmail2 As String = If(IsNothing(olivreur2.Email2), "", olivreur2.Email2.ToString.Trim)

                            If sEmail.ToString.Trim <> "" Then
                                sEmails = sEmails & sEmail & ","
                            End If

                            If sEmail1.ToString.Trim <> "" Then
                                sEmails = sEmails & sEmail1 & ","
                            End If


                            If sEmail2.ToString.Trim <> "" Then
                                sEmails = sEmails & sEmail2 & ","
                            End If


                            Dim sSubject As String = olivreur2.Livreur & ",  Votre fiche de paye pour la période " & oPeriode.Periode
                            Dim sMessage As String = olivreur2.Livreur & ", Merci pour de faire partie de notre équipe"
                            Dim sAttachment As String = If(IsNothing(drRapportFacturePaye("CheminRapport")), Nothing, drRapportFacturePaye("CheminRapport").ToString.Trim)

                            If Not IsNothing(sEmails) Then

                                sEmails = sEmails.Remove(sEmails.Length - 1)


                                Dim GGmail As New GGSMTP_GMAIL("idslivraisonexpressrapport@gmail.com", "2015livraison", )
                                Dim ToAddressies As String() = {sEmails}
                                Dim attachs() As String = {sAttachment}
                                Dim subject As String = sSubject
                                Dim body As String = sMessage

                                result = GGmail.SendMail(ToAddressies, subject, body, attachs)

                                If result Then

                                Else
                                    MsgBox(GGmail.ErrorText, MsgBoxStyle.Critical)
                                End If

                            End If

                        End If

                        ProgressBarControl1.PerformStep()
                        ProgressBarControl1.Update()
                    Next

                    ProgressBarControl1.EditValue = 0
                    Me.Cursor = Cursors.Default
                    MessageBox.Show("Envoi des paies complété avec succès?")


                Else
                    Me.Cursor = Cursors.Default
                    MessageBox.Show("Aucune paie n'a été trouvé pour cette période!", "Paie", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1)
                    Exit Sub
                End If

            Catch ex As Exception
                Me.Cursor = Cursors.Default
                MessageBox.Show(ex.Message, "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End Try

        End If



    End Sub





    Private Sub bSupprimerI_Click(sender As Object, e As EventArgs) Handles bSupprimerI.Click

        Dim oNomFichierImport As New NomFichierImport
        Dim oNomFichierImportData As New NomFichierImportData

        If IsNothing(cbFichierImport.EditValue) Then
            MessageBox.Show("Vous devez sélectionner un fichier d'import pour supprimer cet importation")
            Exit Sub

        End If
        Try


            Dim result As DialogResult = MessageBox.Show("Attention, Vous aller supprimer l'import relié a ce nom de fichier, les données ne pourrront pas etre récupée. Continuer?", "Supprimer Import", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
            If result = DialogResult.No Then
                Exit Sub
            ElseIf result = DialogResult.Yes Then


                Me.Cursor = Cursors.WaitCursor
                oNomFichierImport.IDNomFichierImport = cbFichierImport.EditValue

                oImportData.DeleteImport(oNomFichierImport)
                oImportData.DeleteImportBackup(oNomFichierImport)

                oNomFichierImportData.Delete(oNomFichierImport)
                FillNomFichierImport()
                grdImport.DataSource = oImportData.SelectAllFromDate(tbDateFrom.Text, tbDateTo.Text)

                GridView1.OptionsView.ColumnAutoWidth = False
                'GridView1.BestFitColumns()
                Dim viewInfo As DevExpress.XtraGrid.Views.Grid.ViewInfo.GridViewInfo = TryCast(GridView1.GetViewInfo(), GridViewInfo)
                If viewInfo.ViewRects.ColumnTotalWidth < viewInfo.ViewRects.ColumnPanelWidth Then
                    GridView1.OptionsView.ColumnAutoWidth = True
                End If

            End If
            Me.Cursor = Cursors.Default


        Catch ex As Exception
            Me.Cursor = Cursors.Default
            MessageBox.Show(ex.Message, "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try


    End Sub

    Private Sub cmdSupprimerPeriode_Click(sender As Object, e As EventArgs)

        If Not IsNothing(cbPeriodes.EditValue) Then

            Dim result As DialogResult = MessageBox.Show("Supprimer une période va supprimer Les paye et la facturation pour la période...Continuer?", "Supprimer période", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
            If result = DialogResult.No Then
                Exit Sub
            ElseIf result = DialogResult.Yes Then

                Try
                    Me.Cursor = Cursors.WaitCursor

                    Dim oPeriode As New Periodes
                    oPeriode.IDPeriode = cbPeriodes.EditValue


                    Dim opaye As New Paye
                    opaye.IDPeriode = oPeriode.IDPeriode
                    oPayeData.DeleteByPeriod(opaye)

                    Dim opayeline As New PayeLine
                    opayeline.IDPeriode = oPeriode.IDPeriode
                    oPayeLineData.DeleteByPeriod(opayeline)

                    Dim oinvoice As New Invoice
                    oinvoice.IDPeriode = oPeriode.IDPeriode
                    oInvoiceData.DeleteByPeriod(oinvoice)

                    Dim oinvoiceLine As New InvoiceLine
                    oinvoiceLine.IDPeriode = oPeriode.IDPeriode
                    oInvoiceLineData.DeleteByPeriod(oinvoiceLine)

                    oPeriodeData.Delete(oPeriode)


                    Dim oRapportFacturePaye As New RapportFacturePaye

                    oRapportFacturePaye.IDPeriode = oPeriode.IDPeriode
                    oRapportFacturePayeData.DeletePeriodO(oRapportFacturePaye)

                    FillPaieNumber()
                    FillInvoiceNumber()
                    FillNomFichierImport()
                    FillAllPeriodes()

                    Me.Cursor = Cursors.Default
                    MessageBox.Show("La supression des factures et des payes a été complétée avec succès!")



                Catch ex As Exception
                    Me.Cursor = Cursors.Default
                    MessageBox.Show(ex.Message, "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                End Try
            End If
        Else
            MessageBox.Show("Veuillez sélectionner une période dans la liste", "Période sélection manquante", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1)
            Exit Sub
        End If



    End Sub

    Private Sub btnGenererFacturation_Click(sender As Object, e As EventArgs)

        Dim iTotalCalcul As Integer = 0
        Dim dDateFrom As Date = tbDateFrom.Text
        Dim dDateTo As Date = tbDateTo.Text



        ' verifie que les lignes rouges et vertes sont regles
        For i As Integer = 0 To GridView1.DataRowCount - 1
            If GridView1.GetRowCellValue(i, "Distance").ToString() = 0 Or GridView1.GetRowCellValue(i, "Distance").ToString() > 400 Then
                MessageBox.Show("Vous avez des lignes rouges et/ou verte a fixer dans la grille. Veuillez les fixer avant de générer la facturation!")
                Exit Sub
            End If
        Next i



        '----------------------------------------------------------------------------------------
        ' Verifie que tous les methodes de calculs ont ete referencer dans le systeme
        '----------------------------------------------------------------------------------------


        If IsNothing(cbPeriodes.EditValue) Then
            MessageBox.Show("Vous devez selectionner une periode")
            Exit Sub
        End If


        iTotalCalcul = oOrganisationData.SelectAllMetodeCalculationOrganisation()

        If iTotalCalcul > 0 Then
            Me.Cursor = Cursors.Default
            MessageBox.Show("il y a " & (iTotalCalcul) & " organisations qui n'ont pas de méthode de calcul, Veuillez affecter une méthode de calcul a toutes les organisations")
            Exit Sub
        End If



        '----------------------------------------------------------------------------------------
        ' Facturation et paye generation
        '----------------------------------------------------------------------------------------

        Dim result As DialogResult = MessageBox.Show("Générer la facturation?", "Facturation", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
        If result = DialogResult.No Then
            Me.Cursor = Cursors.Default
        ElseIf result = DialogResult.Yes Then

            If GenererToutesLesFactures() = False Then
                Exit Sub
            End If

            CalculerFactureTotaux()

        End If



        grdImport.DataSource = oImportData.SelectAllFromDate(dDateFrom, dDateTo)
        GridView1.OptionsView.ColumnAutoWidth = False
        'GridView1.BestFitColumns()
        Dim viewInfo As DevExpress.XtraGrid.Views.Grid.ViewInfo.GridViewInfo = TryCast(GridView1.GetViewInfo(), GridViewInfo)
        If viewInfo.ViewRects.ColumnTotalWidth < viewInfo.ViewRects.ColumnPanelWidth Then
            GridView1.OptionsView.ColumnAutoWidth = True
        End If

        Me.Cursor = Cursors.Default





    End Sub

    Private Sub bImprimerFactures_Click(sender As Object, e As EventArgs)
        If IsNothing(cbPeriodes.EditValue) Then
            MessageBox.Show("Vous devez selectionner une periode")
            Exit Sub
        End If



        Dim result As DialogResult = MessageBox.Show("Désirez-vous imprimer les factures?", "Imprimer Factures", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
        If result = DialogResult.No Then

        ElseIf result = DialogResult.Yes Then
            Me.Cursor = Cursors.WaitCursor
            If ImprimerLesFactures() = False Then

                Me.Cursor = Cursors.Default
                Exit Sub
            End If

        End If


        FillInvoiceNumber()


        Me.Cursor = Cursors.Default


    End Sub




    Private Sub tbMontantGlacierOrganisation_KeyPress(sender As Object, e As KeyPressEventArgs)
        If e.KeyChar = "." Or e.KeyChar = "," Or e.KeyChar = Convert.ToChar(Keys.Back) Or e.KeyChar = Convert.ToChar(Keys.Back) Then
            e.Handled = e.KeyChar = ","
        Else
            e.Handled = Not (Char.IsDigit(e.KeyChar))
        End If
    End Sub

    Private Sub tbMontantGlacierLivreur_KeyPress(sender As Object, e As KeyPressEventArgs)
        If e.KeyChar = "." Or e.KeyChar = "," Or e.KeyChar = Convert.ToChar(Keys.Back) Or e.KeyChar = Convert.ToChar(Keys.Back) Then
            e.Handled = e.KeyChar = ","
        Else
            e.Handled = Not (Char.IsDigit(e.KeyChar))
        End If
    End Sub

    Private Sub tbDebit_KeyPress(sender As Object, e As KeyPressEventArgs)
        If e.KeyChar = "." Or e.KeyChar = "," Or e.KeyChar = Convert.ToChar(Keys.Back) Or e.KeyChar = Convert.ToChar(Keys.Back) Then
            e.Handled = e.KeyChar = ","
        Else
            e.Handled = Not (Char.IsDigit(e.KeyChar))
        End If
    End Sub

    Private Sub tbCredit_KeyPress(sender As Object, e As KeyPressEventArgs)
        If e.KeyChar = "." Or e.KeyChar = "," Or e.KeyChar = Convert.ToChar(Keys.Back) Or e.KeyChar = Convert.ToChar(Keys.Back) Then
            e.Handled = e.KeyChar = ","
        Else
            e.Handled = Not (Char.IsDigit(e.KeyChar))
        End If
    End Sub

    Private Sub cbInvoiceNumber_SelectedIndexChanged(sender As Object, e As EventArgs)


        If IsLoading = True Then Exit Sub

        Dim iIDRapport As Integer = 0
        Dim dtRapportFacturePaye As New DataTable
        Dim drRapportFacturePaye As DataRow
        Dim iIDOrganisation As Integer = 0

        If cbFactures.EditValue > -1 Then
            iIDRapport = cbFactures.EditValue
            dtRapportFacturePaye = oRapportFacturePayeData.SelectAllByID(iIDRapport)
            If dtRapportFacturePaye.Rows.Count > 0 Then

                Dim result As DialogResult = MessageBox.Show("Attention, la période courante et les dates seront changées. Continuer?", "Facturation", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
                If result = DialogResult.No Then
                    Exit Sub
                ElseIf result = DialogResult.Yes Then


                    For Each drRapportFacturePaye In dtRapportFacturePaye.Rows

                        tbDateFrom.Text = drRapportFacturePaye("DateFrom")
                        tbDateTo.Text = drRapportFacturePaye("DateTo")
                        go_Globals.IDPeriode = drRapportFacturePaye("IDPeriode")
                        IDPeriode = go_Globals.IDPeriode
                    Next
                End If

            End If
        End If
    End Sub

    Private Sub cbPayeNumber_SelectedIndexChanged(sender As Object, e As EventArgs)

        If IsLoading = True Then Exit Sub

        Dim dtRapportFacturePaye As New DataTable
        Dim drRapportFacturePaye As DataRow
        Dim iIDLivreur As Integer = 0
        Dim iIDRapport As Integer = 0



        If cbPayes.EditValue > -1 Then
            iIDRapport = cbPayes.EditValue
            dtRapportFacturePaye = oRapportFacturePayeData.SelectAllByID(iIDRapport)
            If dtRapportFacturePaye.Rows.Count > 0 Then

                For Each drRapportFacturePaye In dtRapportFacturePaye.Rows

                    tbDateFrom.Text = drRapportFacturePaye("DateFrom")
                    tbDateTo.Text = drRapportFacturePaye("DateTo")
                    go_Globals.IDPeriode = drRapportFacturePaye("IDPeriode")
                    IDPeriode = go_Globals.IDPeriode

                Next
            End If

        End If



    End Sub

    Private Sub cbPeriode_EditValueChanged(sender As Object, e As EventArgs) Handles cbPeriodes.EditValueChanged
        Dim oPeriode As New Periodes
        Dim dtPeriode As New DataTable
        Dim drPeriode As DataRow


        If IsLoading = True Then Exit Sub

        bFactures.Enabled = True
        bPayes.Enabled = True

        If Not IsNothing(cbPeriodes.EditValue) Then

            Me.Cursor = Cursors.WaitCursor

            'tabMain.TabPages(2).PageVisible = True

            IDPeriode = cbPeriodes.EditValue
            go_Globals.IDPeriode = IDPeriode

            dtPeriode = oPeriodeData.SelectAllByID(IDPeriode)
            If dtPeriode.Rows.Count > 0 Then
                For Each drPeriode In dtPeriode.Rows

                    btnGenererFacturation.Enabled = True
                    bGenererLaPaye.Enabled = True
                    bImprimerFactures.Enabled = True
                    bImprimerCommissionRapport.Enabled = True

                    bLivraisonParRestaurant2.Enabled = True
                    bLivraisonsParLivreurs.Enabled = True
                    bAjouterUneCommande.Enabled = True
                    bAchats.Enabled = True
                    bVentes.Enabled = True
                    bFactures.Enabled = True
                    bPayes.Enabled = True
                    bEpargnes.Enabled = True
                    bOrganisations.Enabled = True


                    tbDateFrom.EditValue = drPeriode("DateFrom")
                    tbDateTo.EditValue = drPeriode("DateTo")

                    go_Globals.PeriodeDateFrom = drPeriode("DateFrom")
                    go_Globals.PeriodeDateTo = drPeriode("DateTo")


                    Calendar.StartDate = tbDateFrom.EditValue

                    oImportData.DeleteAll()
                    oImportData.CopyBackupToImport(IDPeriode)
                    grdImport.DataSource = oImportData.SelectAllImportFromPeriode(IDPeriode)



                    Dim dDatefrom As DateTime = GetLastSunday()
                    Dim dDateTo As DateTime = GetNextSaturday(dDatefrom)


                    GridView1.OptionsView.ColumnAutoWidth = False
                    'GridView1.BestFitColumns()
                    Dim viewInfo As DevExpress.XtraGrid.Views.Grid.ViewInfo.GridViewInfo = TryCast(GridView1.GetViewInfo(), GridViewInfo)
                    If viewInfo.ViewRects.ColumnTotalWidth < viewInfo.ViewRects.ColumnPanelWidth Then
                        GridView1.OptionsView.ColumnAutoWidth = True
                    End If


                    FillCommission()
                    FillPaieNumber()
                    FillInvoiceNumber()
                    FillNomFichierImport()
                    FillAllRapportLivraisonRestaurant()
                    FillRapportTaxation()
                    FillRapportRevenu()




                    If IDPeriode > 0 Then

                        bCommisssion.Enabled = True
                        LayoutDroite.Enabled = True
                        LayoutBas.Enabled = True
                        btnGenererFacturation.Enabled = True
                        bGenererLaPaye.Enabled = True
                        bImprimerFactures.Enabled = True
                        bImprimerCommissionRapport.Enabled = True
                        bOrganisations.Enabled = True
                        bLivraisonParRestaurant2.Enabled = True
                        bLivraisonParLivreur.Enabled = True
                        bAjouterUneCommande.Enabled = True
                        bAchats.Enabled = True
                        bVentes.Enabled = True
                        bFactures.Enabled = True
                        bPayes.Enabled = True
                        bImprimerCommisssions.Enabled = True
                        bGenererLesCommission.Enabled = True
                        bEtats.Enabled = False

                        'bGenererPaye.Enabled = True
                        bImprimerFactures.Enabled = True
                        btnGenererFacturation.Enabled = True
                        'bGenererPaye.Enabled = True
                        bImportMobilus.Enabled = True
                        bEnvoyerPaye.Enabled = True
                        bEnvoyerFacture.Enabled = True
                        bSelectfile.Enabled = True
                        bConsolidation.Enabled = True
                        bImprimerCommissionRapport.Enabled = True
                        bAjouterUneCommande.Enabled = True
                        bGenererLaPaye.Enabled = True
                        bImprimerCommission.Enabled = True
                        bEpargnes.Enabled = True

                    Else
                        bCommisssion.Enabled = False
                        go_Globals.IDPeriode = 0
                        LayoutDroite.Enabled = False
                        LayoutBas.Enabled = False
                        bOrganisations.Enabled = False
                        bEtats.Enabled = False
                        bGenererLesCommission.Enabled = False
                        bImprimerCommisssions.Enabled = False
                        bAchats.Enabled = False
                        bVentes.Enabled = False
                        bFactures.Enabled = False
                        bPayes.Enabled = False



                        bGenererLaPaye.Enabled = False
                        bConsolidation.Enabled = False
                        'bGenererPaye.Enabled = False
                        bImprimerFactures.Enabled = False
                        btnGenererFacturation.Enabled = False
                        'bGenererPaye.Enabled = False
                        bImportMobilus.Enabled = False
                        bEnvoyerPaye.Enabled = False
                        bEnvoyerFacture.Enabled = False
                        bSelectfile.Enabled = False
                        bImprimerCommissionRapport.Enabled = False
                        bLivraisonParRestaurant2.Enabled = False
                        bLivraisonParLivreur.Enabled = False
                        bAjouterUneCommande.Enabled = False
                        bGenererLaPaye.Enabled = False
                        bImprimerCommission.Enabled = False
                        bEpargnes.Enabled = False
                    End If



                Next
            End If

            Select Case optTypePeriode.SelectedIndex

                Case 0

                    go_Globals.IDTypePeriode = 0

                    LayoutDroite.Enabled = True

                    LayoutEtat.Enabled = False
                    LayoutEtatLivraisonRestaurant.Enabled = True
                    LayoutCommissions.Enabled = True
                    LayoutFactures.Enabled = True
                    LayoutPayes.Enabled = True
                    bImprimerCommission.Enabled = True
                    bLivraisonParRestaurant2.Enabled = True

                Case 1



                    LayoutDroite.Enabled = True
                    LayoutEtat.Enabled = True
                    LayoutEtatLivraisonRestaurant.Enabled = False
                    LayoutCommissions.Enabled = False
                    LayoutFactures.Enabled = False
                    LayoutPayes.Enabled = False


                    go_Globals.IDTypePeriode = 1

                    bCommisssion.Enabled = False
                    LayoutBas.Enabled = False

                    bCommisssion.Enabled = False
                    bEtats.Enabled = True
                    bGenererLesCommission.Enabled = False
                    bImprimerCommisssions.Enabled = False
                    bAchats.Enabled = False
                    bVentes.Enabled = False
                    bFactures.Enabled = False
                    bPayes.Enabled = False
                    bGenererLaPaye.Enabled = False
                    bConsolidation.Enabled = False
                    bImprimerFactures.Enabled = False
                    btnGenererFacturation.Enabled = False
                    bImportMobilus.Enabled = False
                    bEnvoyerPaye.Enabled = False
                    bEnvoyerFacture.Enabled = False
                    bSelectfile.Enabled = False
                    bImprimerCommissionRapport.Enabled = False
                    bLivraisonParLivreur.Enabled = False
                    bAjouterUneCommande.Enabled = False
                    bLivraisonParRestaurant2.Enabled = False
                    bImprimerCommission.Enabled = False

                    FillAllEtatsDeCompte()
                    FillAllRapportLivraisonRestaurant()

            End Select


            Me.Cursor = Cursors.Default
            Exit Sub



            Me.Cursor = Cursors.Default

        End If

        Me.Cursor = Cursors.Default
    End Sub

    Private Sub cbPayes_EditValueChanged(sender As Object, e As EventArgs)

        If IsLoading = True Then Exit Sub

        Dim dtRapportFacturePaye As New DataTable
        Dim drRapportFacturePaye As DataRow
        Dim iIDLivreur As Integer = 0
        Dim iIDRapport As Integer = 0



        If cbPayes.EditValue > -1 Then

            IDSelectedPaye = cbPayes.EditValue
            iIDRapport = cbPayes.EditValue
            dtRapportFacturePaye = oRapportFacturePayeData.SelectAllByID(iIDRapport)
            If dtRapportFacturePaye.Rows.Count > 0 Then

                For Each drRapportFacturePaye In dtRapportFacturePaye.Rows

                    tbDateFrom.Text = drRapportFacturePaye("DateFrom")
                    tbDateTo.Text = drRapportFacturePaye("DateTo")
                    go_Globals.IDPeriode = drRapportFacturePaye("IDPeriode")
                    IDPeriode = go_Globals.IDPeriode

                Next
            End If

        End If

    End Sub







    Private Sub bAnnulerFichierImport_Click(sender As Object, e As EventArgs) Handles bAnnulerFichierImport.Click
        cbFichierImport.EditValue = Nothing
        cbFichierImport.Text = String.Empty
    End Sub

    Private Sub bAnnulerPeriode_Click(sender As Object, e As EventArgs)
        cbPeriodes.EditValue = Nothing
        cbPeriodes.Text = String.Empty
        bAchats.Enabled = False
        bVentes.Enabled = False


        bGenererPaye.Enabled = False
        bConsolidation.Enabled = False
        'bGenererPaye.Enabled = False
        bImprimerFactures.Enabled = False
        btnGenererFacturation.Enabled = False
        'bGenererPaye.Enabled = False
        bImportMobilus.Enabled = False
        bEnvoyerPaye.Enabled = False
        bEnvoyerFacture.Enabled = False
        bSelectfile.Enabled = False
        bImprimerCommissionRapport.Enabled = False
    End Sub

    Private Sub bAnnulerFactures_Click(sender As Object, e As EventArgs) Handles bAnnulerFactures.Click
        cbFactures.EditValue = Nothing
        cbFactures.SelectedText = Nothing
        cbFactures.Text = Nothing
    End Sub

    Private Sub bAnnulerPayes_Click(sender As Object, e As EventArgs) Handles bAnnulerPayes.Click
        cbPayes.EditValue = Nothing
        cbPayes.SelectedText = String.Empty
        cbPayes.Text = String.Empty

    End Sub

    Private Sub bExportToExcel_Click(sender As Object, e As EventArgs) Handles bExportToExcel.Click
        Dim oPeriode As New Periodes
        Dim dDateFrom As Date = tbDateFrom.Text
        Dim dDateTo As Date = tbDateTo.Text
        Dim frenchCultureInfo = New Globalization.CultureInfo("fr-CA")

        Dim sFile As String = ""

        If dDateFrom.Month <> dDateTo.Month Then
            oPeriode.Periode = dDateFrom.Day & " " & GetMonthName(dDateFrom, frenchCultureInfo) & "-" & dDateTo.Day & " " & GetMonthName(dDateTo, frenchCultureInfo) & " " & dDateTo.Year.ToString
        Else
            oPeriode.Periode = dDateFrom.Day & "-" & dDateTo.Day & " " & GetMonthName(dDateTo, frenchCultureInfo) & " " & dDateTo.Year.ToString
        End If

        sFile = My.Computer.FileSystem.SpecialDirectories.Desktop & "\" & oPeriode.Periode & ".xlsx"

        grdImport.ExportToXlsx(sFile)
        System.Diagnostics.Process.Start(sFile)
    End Sub

    Private Sub bExportToHTML_Click(sender As Object, e As EventArgs) Handles bExportToHTML.Click
        Dim oPeriode As New Periodes
        Dim dDateFrom As Date = tbDateFrom.Text
        Dim dDateTo As Date = tbDateTo.Text
        Dim frenchCultureInfo = New Globalization.CultureInfo("fr-CA")

        Dim sFile As String = ""

        If dDateFrom.Month <> dDateTo.Month Then
            oPeriode.Periode = dDateFrom.Day & " " & GetMonthName(dDateFrom, frenchCultureInfo) & "-" & dDateTo.Day & " " & GetMonthName(dDateTo, frenchCultureInfo) & " " & dDateTo.Year.ToString
        Else
            oPeriode.Periode = dDateFrom.Day & "-" & dDateTo.Day & " " & GetMonthName(dDateTo, frenchCultureInfo) & " " & dDateTo.Year.ToString
        End If

        sFile = My.Computer.FileSystem.SpecialDirectories.Desktop & "\" & oPeriode.Periode & ".html"

        grdImport.ExportToHtml(sFile)
        System.Diagnostics.Process.Start(sFile)
    End Sub

    Private Sub bExportToDoc_Click(sender As Object, e As EventArgs) Handles bExportToDoc.Click
        Dim oPeriode As New Periodes
        Dim dDateFrom As Date = tbDateFrom.Text
        Dim dDateTo As Date = tbDateTo.Text
        Dim frenchCultureInfo = New Globalization.CultureInfo("fr-CA")

        Dim sFile As String = ""

        If dDateFrom.Month <> dDateTo.Month Then
            oPeriode.Periode = dDateFrom.Day & " " & GetMonthName(dDateFrom, frenchCultureInfo) & "-" & dDateTo.Day & " " & GetMonthName(dDateTo, frenchCultureInfo) & " " & dDateTo.Year.ToString
        Else
            oPeriode.Periode = dDateFrom.Day & "-" & dDateTo.Day & " " & GetMonthName(dDateTo, frenchCultureInfo) & " " & dDateTo.Year.ToString
        End If

        sFile = My.Computer.FileSystem.SpecialDirectories.Desktop & "\" & oPeriode.Periode & ".docx"

        grdImport.ExportToDocx(sFile)
        System.Diagnostics.Process.Start(sFile)
    End Sub

    Private Sub bExportToPDF_Click(sender As Object, e As EventArgs) Handles bExportToPDF.Click

        Dim oPeriode As New Periodes
        Dim dDateFrom As Date = tbDateFrom.Text
        Dim dDateTo As Date = tbDateTo.Text
        Dim frenchCultureInfo = New Globalization.CultureInfo("fr-CA")

        Dim sFile As String = ""

        If dDateFrom.Month <> dDateTo.Month Then
            oPeriode.Periode = dDateFrom.Day & " " & GetMonthName(dDateFrom, frenchCultureInfo) & "-" & dDateTo.Day & " " & GetMonthName(dDateTo, frenchCultureInfo) & " " & dDateTo.Year.ToString
        Else
            oPeriode.Periode = dDateFrom.Day & "-" & dDateTo.Day & " " & GetMonthName(dDateTo, frenchCultureInfo) & " " & dDateTo.Year.ToString
        End If

        sFile = My.Computer.FileSystem.SpecialDirectories.Desktop & "\" & oPeriode.Periode & ".pdf"

        grdImport.ExportToPdf(sFile)
        System.Diagnostics.Process.Start(sFile)

    End Sub

    Private Sub cbFactures_EditValueChanged(sender As Object, e As EventArgs)

        If IsLoading = True Then Exit Sub

        Dim dtRapportFacturePaye As New DataTable
        Dim drRapportFacturePaye As DataRow
        Dim iIDLivreur As Integer = 0
        Dim iIDRapport As Integer = 0



        If Not IsNothing(cbFactures.EditValue) Then
            IDSelectedFacture = cbFactures.EditValue
            iIDRapport = cbFactures.EditValue
            dtRapportFacturePaye = oRapportFacturePayeData.SelectAllByID(iIDRapport)
            If dtRapportFacturePaye.Rows.Count > 0 Then

                For Each drRapportFacturePaye In dtRapportFacturePaye.Rows

                    tbDateFrom.Text = drRapportFacturePaye("DateFrom")
                    tbDateTo.Text = drRapportFacturePaye("DateTo")
                    go_Globals.IDPeriode = drRapportFacturePaye("IDPeriode")
                    IDPeriode = go_Globals.IDPeriode

                Next
            End If

        End If



    End Sub





    Private Sub bConsolidationDate_Click(sender As Object, e As EventArgs) Handles bConsolidationDate.Click
        Dim firstDayOfMonth = New DateTime(Now.Year, Now.Month, 1)
        Dim lastDayOfMonth As DateTime = firstDayOfMonth.AddMonths(1).AddDays(-1)
        tbDateFrom.EditValue = New DateTime(firstDayOfMonth.Year, firstDayOfMonth.Month, firstDayOfMonth.Day, 0, 0, 0, 0)
        tbDateTo.EditValue = New DateTime(lastDayOfMonth.Year, lastDayOfMonth.Month, lastDayOfMonth.Day, 23, 59, 0, 0)
        IDPeriode = 0
        go_Globals.IDPeriode = 0

        cbPeriodes.EditValue = Nothing
        cbPeriodes.Text = String.Empty
    End Sub

    Private Sub bResetDate_Click(sender As Object, e As EventArgs) Handles bResetDate.Click

        Dim dDatefrom As DateTime = GetLastSunday()
        Dim dDateTo As DateTime = GetNextSaturday(dDatefrom)

        tbDateFrom.EditValue = New DateTime(dDatefrom.Year, dDatefrom.Month, dDatefrom.Day, 0, 0, 0, 0)
        tbDateTo.EditValue = New DateTime(dDateTo.Year, dDateTo.Month, dDateTo.Day, 23, 59, 0, 0)

        cbPeriodes.EditValue = Nothing
        cbPeriodes.Text = String.Empty

    End Sub

    Private Sub bEtatDesComptes_ItemClick(sender As Object, e As ItemClickEventArgs) Handles bEtatDesComptes.ItemClick

        Dim dDateFrom As Date = tbDateFrom.Text
        Dim dDateTo As Date = tbDateTo.Text
        Dim oInvoiceData As New InvoiceData
        Dim frenchCultureInfo = New Globalization.CultureInfo("fr-CA")
        Dim dtOrganisationFacture As New DataTable
        Dim drOrganisationFacture As DataRow
        Dim dtInvoice As New DataTable
        Dim dtOrganisation As New DataTable
        Dim drInvoiceO As DataRow
        Dim intNumEmail As Integer = 0
        Dim strCurrentEmail As String = ""
        Dim iRow As Integer = 0
        Dim oPeriode As New Periodes
        SpreadsheetInfo.SetLicense("ERDC-FN4O-YKYN-4DBM")
        Dim strPeriode As String = vbNullString
        Dim sFile As String = ""
        Dim Icount As Integer = 0
        Dim ef As ExcelFile = Nothing
        Dim strOrganisation As String = ""
        Dim IDOrganisation As Integer = 0
        Dim sMonthFrom2 As String = ""
        Dim sMonthTo2 As String = ""
        Dim strPeriodeInvoice As String = ""
        Dim strVendeur As String = ""
        Dim IDVendeur As Integer = 0
        Dim dblTotal As Double = 0
        Dim sFooterMessage As String = ""
        Dim dtEtats As New DataTable
        Dim drEtats As DataRow
        Dim result As DialogResult
        Dim oEtats As New Etats
        Dim oEtatsData As New EtatsData

        On Error GoTo ErrHandler


        Dim sMonthFROM As String = GetMonthName(dDateFrom, frenchCultureInfo).ToString
        Dim sMonthTO As String = GetMonthName(dDateTo, frenchCultureInfo).ToString


        If Not IsNothing(cbPeriodes.EditValue) Then
            IDPeriode = cbPeriodes.EditValue
        Else
            MessageBox.Show("Vous devez sélectionner une période dans la liste des périodes d'états")
            Exit Sub
        End If


        dtEtats = oEtatsData.SelectAllByPeriode(IDPeriode)
        If dtEtats.Rows.Count > 0 Then

            result = MessageBox.Show("Les états de comptes ont deja été générés, Voulez-vous les Re-générer?", "Etats de comptes", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
            If result = DialogResult.No Then
                Me.Cursor = Cursors.Default
            ElseIf result = DialogResult.Yes Then

                For Each drEtats In dtEtats.Rows
                    Dim sNomFichier As String = drEtats("CheminRapport")

                    If My.Computer.FileSystem.FileExists(sNomFichier) Then
                        My.Computer.FileSystem.DeleteFile(sNomFichier)
                    End If
                Next

                oEtatsData.DeleteByPeriode(IDPeriode)
                GoTo GENERERERETAT


            End If
        End If







        result = MessageBox.Show("Désirez-vous imprimer l'État de compte?", "État des ventes", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
        If result = DialogResult.No Then
            Exit Sub
        ElseIf result = DialogResult.Yes Then

GENERERERETAT:

            If dDateFrom.Month <> dDateTo.Month Then
                oPeriode.Periode = dDateFrom.Day & " " & sMonthFROM & "-" & dDateTo.Day & " " & sMonthTO & " " & dDateTo.Year.ToString
            Else
                oPeriode.Periode = dDateFrom.Day & "-" & dDateTo.Day & " " & sMonthTO & " " & dDateTo.Year.ToString
            End If

            strPeriode = oPeriode.Periode

            My.Computer.FileSystem.CreateDirectory(go_Globals.LocationConsolidation & "\" & strPeriode)

            ' Select all the organisations with an invoice and not affected to a vendeur
            dtOrganisationFacture = oOrganisationData.SelectAllInvoicesParOrganisationEtatDeCompte(dDateFrom, dDateTo)
            If dtOrganisationFacture.Rows.Count > 0 Then

                ProgressBarControl1.Properties.Step = 1
                ProgressBarControl1.Properties.PercentView = True
                ProgressBarControl1.Properties.Maximum = dtOrganisationFacture.Rows.Count
                ProgressBarControl1.Properties.Minimum = 0



                iRow = 8
                For Each drOrganisationFacture In dtOrganisationFacture.Rows


                    Dim sExcelPath As String = Application.StartupPath() & "\Etatdecompte.xlsx"
                    ef = ExcelFile.Load(sExcelPath)

                    Dim oOrganisation As New Organisations
                    Dim oOrganisationSel As New Organisations
                    oOrganisation.OrganisationID = drOrganisationFacture("OrganisationID")
                    oOrganisationSel = oOrganisationData.Select_Record(oOrganisation)

                    If Not IsNothing(oOrganisationSel) Then
                        ef.Worksheets(0).Cells(2, 2).Value = If(IsNothing(oOrganisationSel.Organisation), Nothing, oOrganisationSel.Organisation.ToString.Trim)
                        IDOrganisation = If(IsNothing(oOrganisationSel.OrganisationID), Nothing, CType(oOrganisationSel.OrganisationID, Int32?))
                        strOrganisation = If(IsNothing(oOrganisationSel.Organisation), Nothing, oOrganisationSel.Organisation.ToString.Trim)
                        ef.Worksheets(0).Cells(3, 2).Value = If(IsNothing(oOrganisationSel.Adresse), Nothing, oOrganisationSel.Adresse.ToString.Trim)
                        ef.Worksheets(0).Cells(4, 2).Value = If(IsNothing(oOrganisationSel.Email), Nothing, oOrganisationSel.Email.ToString.Trim)
                        ef.Worksheets(0).Cells(5, 2).Value = If(IsNothing(oOrganisationSel.Telephone), Nothing, oOrganisationSel.Telephone.ToString.Trim)
                        sFooterMessage = If(IsNothing(oOrganisationSel.MessageEtat), "", oOrganisationSel.MessageEtat.ToString.Trim)
                    End If


                    Dim dtInvoicesO As New DataTable
                    Dim drInvoicesO As DataRow
                    Dim strPeriode2 As String = ""

                    dtInvoicesO = oInvoiceData.SelectPaidInvoiceByDate(dDateFrom, dDateTo, drOrganisationFacture("OrganisationID"))

                    For Each drInvoicesO In dtInvoicesO.Rows
                        Dim ddDateFrom As DateTime = drInvoicesO("DateFrom")
                        Dim ddDateTo As DateTime = drInvoicesO("DateTo")
                        sMonthFrom2 = GetMonthName(ddDateFrom, frenchCultureInfo).ToString
                        sMonthTo2 = GetMonthName(ddDateTo, frenchCultureInfo).ToString

                        If ddDateFrom.Month <> ddDateTo.Month Then
                            strPeriode2 = ddDateFrom.Day & " " & sMonthFrom2 & "-" & ddDateTo.Day & " " & sMonthTo2 & " " & dDateTo.Year.ToString
                        Else
                            strPeriode2 = ddDateFrom.Day & "-" & ddDateTo.Day & " " & sMonthTo2 & " " & ddDateTo.Year.ToString
                        End If

                        Dim dInvoiceDate As Date = drInvoicesO("InvoiceDate")

                        Dim style As New CellStyle
                        style.HorizontalAlignment = HorizontalAlignmentStyle.Right
                        style.VerticalAlignment = VerticalAlignmentStyle.Center


                        Dim sFirstCellM As String = "E" & iRow.ToString
                        Dim sLastCellM As String = "F" & iRow.ToString
                        ef.Worksheets(0).Cells.GetSubrange(sFirstCellM, sLastCellM).Merged = True

                        ef.Worksheets(0).Cells(iRow, 0).Value = strPeriode2
                        ef.Worksheets(0).Cells(iRow, 1).Value = dInvoiceDate.ToShortDateString
                        ef.Worksheets(0).Cells(iRow, 2).Value = If(IsNothing(drInvoicesO("InvoiceNumber")), Nothing, drInvoicesO("InvoiceNumber").ToString.Trim)
                        ef.Worksheets(0).Cells(iRow, 3).Value = 1
                        dblTotal = dblTotal + If(IsNothing(drInvoicesO("Total")), Nothing, CType(drInvoicesO("Total"), Decimal?))

                        ef.Worksheets(0).Cells(iRow, 4).Style.NumberFormat = "$0.00"
                        ef.Worksheets(0).Cells(iRow, 4).Value = If(IsNothing(drInvoicesO("Total")), Nothing, CType(drInvoicesO("Total"), Decimal?))

                        iRow = iRow + 1

                    Next

                    Dim sFirstCellM2 As String = "E" & iRow.ToString
                    Dim sLastCellM2 As String = "F" & iRow.ToString
                    sFirstCellM2 = "E" & iRow.ToString
                    sLastCellM2 = "F" & iRow.ToString
                    ef.Worksheets(0).Cells.GetSubrange(sFirstCellM2, sLastCellM2).Merged = True
                    ef.Worksheets(0).Cells.GetSubrange(sFirstCellM2, sLastCellM2).Merged = True


                    iRow = iRow + 1


                    ef.Worksheets(0).Cells(iRow, 0).Value = "TPS: " & go_Globals.TPSNumero.ToString
                    ef.Worksheets(0).Cells(iRow, 0).Style.Font.Weight = ExcelFont.BoldWeight
                    ef.Worksheets(0).Cells(iRow + 1, 0).Value = "TVQ: " & go_Globals.TVQNumero.ToString
                    ef.Worksheets(0).Cells(iRow + 1, 0).Style.Font.Weight = ExcelFont.BoldWeight

                    ef.Worksheets(0).Cells(iRow, 4).Value = "Total"
                    ef.Worksheets(0).Cells(iRow, 4).Style.Font.Weight = ExcelFont.BoldWeight
                    ef.Worksheets(0).Cells(iRow, 5).Style.NumberFormat = "$0.00"
                    ef.Worksheets(0).Cells(iRow, 5).Value = dblTotal
                    ef.Worksheets(0).Cells(iRow, 5).Style.Font.Weight = ExcelFont.BoldWeight

                    iRow = iRow + 5


                    Dim styleFooterMessage As New CellStyle
                    styleFooterMessage.HorizontalAlignment = HorizontalAlignmentStyle.Center
                    styleFooterMessage.VerticalAlignment = VerticalAlignmentStyle.Center
                    styleFooterMessage.WrapText = True
                    styleFooterMessage.FillPattern.SetSolid(Color.WhiteSmoke)


                    Dim sFirstCell As String = "A" & iRow.ToString
                    Dim sLastCell As String = "F" & iRow + 2

                    If sFooterMessage.ToString.Trim <> "" Then
                        ef.Worksheets(0).Cells.GetSubrange(sFirstCell, sLastCell).Merged = True
                        ef.Worksheets(0).Cells.GetSubrange(sFirstCell, sLastCell).Style = styleFooterMessage
                        ef.Worksheets(0).Cells(iRow, 2).Value = sFooterMessage
                    End If

                    sFile = "Etat de compte_" & strOrganisation.ToString.Trim & "-" & strPeriode & ".xlsx"
                    sFile = sFile.Replace("'", "")


                    oEtats.IDPeriode = IDPeriode
                    oEtats.DateFrom = dDateFrom
                    oEtats.DateTo = dDateTo
                    oEtats.IDOrganisation = IDOrganisation
                    oEtats.Organisation = strOrganisation
                    oEtats.IDVendeur = Nothing
                    oEtats.Vendeur = Nothing
                    oEtats.Email = strCurrentEmail
                    oEtats.Email1 = Nothing
                    oEtats.Email2 = Nothing
                    oEtats.CheminRapport = go_Globals.LocationConsolidation & "\" & strPeriode & "\" & sFile
                    oEtats.NomFichier = sFile
                    oEtats.DateFrom = dDateFrom
                    oEtats.DateTo = dDateTo
                    oEtatsData.Add(oEtats)


                    If My.Computer.FileSystem.FileExists(go_Globals.LocationConsolidation & "\" & strPeriode & "\" & sFile) Then
                        My.Computer.FileSystem.DeleteFile(go_Globals.LocationConsolidation & "\" & strPeriode & "\" & sFile)
                    End If

                    ef.Save(go_Globals.LocationConsolidation & "\" & strPeriode & "\" & sFile)
                    ef = Nothing
                    IDOrganisation = 0
                    IDVendeur = 0
                    strVendeur = ""
                    strOrganisation = ""
                    iRow = 7
                    Icount = 0
                    dblTotal = 0

                    ProgressBarControl1.PerformStep()
                    ProgressBarControl1.Update()

                Next
            End If

        End If

        '' ===================================================================================================================================================================================================
        '' ETAT DE COMPTES VENDEURS
        '' ===================================================================================================================================================================================================

        Dim dtVendeurs As New DataTable
        Dim drVendeur As DataRow

        dtVendeurs = oLivreurData.SelectAllVendeurs()

        If dtVendeurs.Rows.Count > 0 Then

            ProgressBarControl1.Properties.Step = 1
            ProgressBarControl1.Properties.PercentView = True
            ProgressBarControl1.Properties.Maximum = dtVendeurs.Rows.Count
            ProgressBarControl1.Properties.Minimum = 0


            For Each drVendeur In dtVendeurs.Rows


                Dim sExcelPath As String = Application.StartupPath() & "\Etatdecompte.xlsx"
                ef = ExcelFile.Load(sExcelPath)

                iRow = 8

                IDVendeur = If(IsNothing(drVendeur("LivreurID")), Nothing, CType(drVendeur("LivreurID"), Int32?))
                strVendeur = If(IsNothing(drVendeur("Livreur")), "", drVendeur("Livreur").ToString.Trim)
                ef.Worksheets(0).Cells(2, 2).Value = If(IsNothing(strVendeur), Nothing, strVendeur)
                ef.Worksheets(0).Cells(3, 2).Value = If(IsNothing(drVendeur("Adresse")), Nothing, drVendeur("Adresse").ToString.Trim)
                ef.Worksheets(0).Cells(4, 2).Value = If(IsNothing(drVendeur("Email")), Nothing, drVendeur("Email").ToString.Trim)
                ef.Worksheets(0).Cells(5, 2).Value = If(IsNothing(drVendeur("Telephone")), Nothing, drVendeur("Telephone").ToString.Trim)


                dtOrganisation = oInvoiceData.SelectAllInvoicesParOrganisationVendeurs(dDateFrom, dDateTo, IDVendeur)
                If dtOrganisation.Rows.Count > 0 Then
                    For Each drOrganisation In dtOrganisation.Rows


                        Dim oOrganisation As New Organisations
                        Dim oOrganisationSel As New Organisations
                        oOrganisation.OrganisationID = drOrganisation("OrganisationID")
                        oOrganisationSel = oOrganisationData.Select_RecordVendeur(oOrganisation)


                        Dim styleH As New CellStyle
                        styleH.HorizontalAlignment = HorizontalAlignmentStyle.Left
                        styleH.VerticalAlignment = VerticalAlignmentStyle.Center
                        styleH.WrapText = True
                        styleH.FillPattern.SetSolid(Color.Beige)

                        Dim sFirstCellM As String = "A" & iRow.ToString
                        Dim sLastCellM As String = "C" & iRow.ToString
                        ef.Worksheets(0).Cells.GetSubrange(sFirstCellM, sLastCellM).Merged = True
                        ef.Worksheets(0).Cells.GetSubrange(sFirstCellM, sLastCellM).Style = styleH


                        If Not IsNothing(oOrganisationSel) Then
                            ef.Worksheets(0).Cells(3, 5).Value = dDateTo.Day & " " & sMonthTO & " " & dDateTo.Year
                            ef.Worksheets(0).Cells(iRow, 0).Value = If(IsNothing(oOrganisationSel.Organisation), Nothing, oOrganisationSel.Organisation.ToString.Trim)
                            ef.Worksheets(0).Cells(iRow, 0).Style.Font.Weight = ExcelFont.BoldWeight
                        End If


                        iRow = iRow + 1

                        Dim dtInvoicesO As New DataTable
                        Dim drInvoicesO As DataRow
                        Dim strPeriode2 As String = ""

                        dtInvoicesO = oInvoiceData.SelectPaidInvoiceByDate(dDateFrom, dDateTo, drOrganisation("OrganisationID"))
                        If Not dtInvoicesO.Rows.Count Then
                            For Each drInvoicesO In dtInvoicesO.Rows
                                Dim ddDateFrom As DateTime = drInvoicesO("DateFrom")
                                Dim ddDateTo As DateTime = drInvoicesO("DateTo")
                                sMonthFrom2 = GetMonthName(ddDateFrom, frenchCultureInfo).ToString
                                sMonthTo2 = GetMonthName(ddDateTo, frenchCultureInfo).ToString

                                If ddDateFrom.Month <> ddDateTo.Month Then
                                    strPeriode2 = ddDateFrom.Day & " " & sMonthFrom2 & "-" & ddDateTo.Day & " " & sMonthTo2 & " " & dDateTo.Year.ToString
                                Else
                                    strPeriode2 = ddDateFrom.Day & "-" & ddDateTo.Day & " " & sMonthTo2 & " " & ddDateTo.Year.ToString
                                End If

                                Dim dInvoiceDate As Date = drInvoicesO("InvoiceDate")

                                Dim style As New CellStyle
                                style.HorizontalAlignment = HorizontalAlignmentStyle.Right
                                style.VerticalAlignment = VerticalAlignmentStyle.Center


                                sFirstCellM = "E" & iRow.ToString
                                sLastCellM = "F" & iRow.ToString
                                ef.Worksheets(0).Cells.GetSubrange(sFirstCellM, sLastCellM).Merged = True

                                ef.Worksheets(0).Cells(iRow, 0).Value = strPeriode2
                                ef.Worksheets(0).Cells(iRow, 1).Value = dInvoiceDate.ToShortDateString
                                ef.Worksheets(0).Cells(iRow, 2).Value = If(IsNothing(drInvoicesO("InvoiceNumber")), Nothing, drInvoicesO("InvoiceNumber").ToString.Trim)
                                ef.Worksheets(0).Cells(iRow, 3).Value = 1
                                dblTotal = dblTotal + If(IsNothing(drInvoicesO("Total")), Nothing, CType(drInvoicesO("Total"), Decimal?))

                                ef.Worksheets(0).Cells(iRow, 4).Style.NumberFormat = "$0.00"
                                ef.Worksheets(0).Cells(iRow, 4).Value = If(IsNothing(drInvoicesO("Total")), Nothing, CType(drInvoicesO("Total"), Decimal?))

                                iRow = iRow + 1

                            Next ' NEXT INVOICES

                            Dim sFirstCellM2 As String = "E" & iRow.ToString
                            Dim sLastCellM2 As String = "F" & iRow.ToString
                            sFirstCellM2 = "E" & iRow.ToString
                            sLastCellM2 = "F" & iRow.ToString
                            ef.Worksheets(0).Cells.GetSubrange(sFirstCellM2, sLastCellM2).Merged = True
                            ef.Worksheets(0).Cells.GetSubrange(sFirstCellM2, sLastCellM2).Merged = True
                            iRow = iRow + 1

                        End If
                    Next ' NEXT ORGANISATION

                    iRow = iRow + 1


                    ef.Worksheets(0).Cells(iRow, 0).Value = "TPS: " & go_Globals.TPSNumero.ToString
                    ef.Worksheets(0).Cells(iRow, 0).Style.Font.Weight = ExcelFont.BoldWeight
                    ef.Worksheets(0).Cells(iRow + 1, 0).Value = "TVQ: " & go_Globals.TVQNumero.ToString
                    ef.Worksheets(0).Cells(iRow + 1, 0).Style.Font.Weight = ExcelFont.BoldWeight

                    ef.Worksheets(0).Cells(iRow, 4).Value = "Total"
                    ef.Worksheets(0).Cells(iRow, 4).Style.Font.Weight = ExcelFont.BoldWeight
                    ef.Worksheets(0).Cells(iRow, 5).Style.NumberFormat = "$0.00"
                    ef.Worksheets(0).Cells(iRow, 5).Value = dblTotal
                    ef.Worksheets(0).Cells(iRow, 5).Style.Font.Weight = ExcelFont.BoldWeight

                    iRow = iRow + 5


                    Dim styleFooterMessage As New CellStyle
                    styleFooterMessage.HorizontalAlignment = HorizontalAlignmentStyle.Center
                    styleFooterMessage.VerticalAlignment = VerticalAlignmentStyle.Center
                    styleFooterMessage.WrapText = True
                    styleFooterMessage.FillPattern.SetSolid(Color.WhiteSmoke)


                    Dim sFirstCell As String = "A" & iRow.ToString
                    Dim sLastCell As String = "F" & iRow + 2

                    If sFooterMessage.ToString.Trim <> "" Then

                        ef.Worksheets(0).Cells.GetSubrange(sFirstCell, sLastCell).Merged = True
                        ef.Worksheets(0).Cells.GetSubrange(sFirstCell, sLastCell).Style = styleFooterMessage
                        ef.Worksheets(0).Cells(iRow, 2).Value = sFooterMessage
                    End If

                    sFile = "Etat de compte_" & strVendeur.ToString.Trim & "-" & strPeriode & ".xlsx"
                    sFile = sFile.Replace("'", "")

                    oEtats.IDPeriode = IDPeriode
                    oEtats.IDOrganisation = Nothing
                    oEtats.Organisation = Nothing
                    oEtats.IDVendeur = IDVendeur
                    oEtats.Vendeur = strVendeur.ToString.Trim
                    oEtats.IDOrganisation = IDOrganisation
                    oEtats.Organisation = strOrganisation
                    oEtats.Email = strCurrentEmail
                    oEtats.Email1 = Nothing
                    oEtats.Email2 = Nothing
                    oEtats.CheminRapport = go_Globals.LocationConsolidation & "\" & strPeriode & "\" & sFile
                    oEtats.NomFichier = sFile
                    oEtats.DateFrom = dDateFrom
                    oEtats.DateTo = dDateTo
                    oEtatsData.Add(oEtats)

                    If My.Computer.FileSystem.FileExists(go_Globals.LocationConsolidation & "\" & strPeriode & "\" & sFile) Then
                        My.Computer.FileSystem.DeleteFile(go_Globals.LocationConsolidation & "\" & strPeriode & "\" & sFile)
                    End If

                    ef.Save(go_Globals.LocationConsolidation & "\" & strPeriode & "\" & sFile)
                    ef = Nothing
                    IDOrganisation = 0
                    IDVendeur = 0
                    strVendeur = ""
                    strOrganisation = ""
                    iRow = 7
                    Icount = 0
                    dblTotal = 0

                End If


            Next ' NEXT VENDEUR
        End If




















        FillAllEtatsDeCompte()
        Process.Start(go_Globals.LocationConsolidation & "\" & strPeriode)
        Me.Cursor = Cursors.Default
        Exit Sub


ErrHandler:
        MessageBox.Show(Err.Description, "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        Resume Next

    End Sub

    Private Sub bEtatDesVentes_ItemClick(sender As Object, e As ItemClickEventArgs) Handles bEtatDesVentes.ItemClick
        Dim dDateFrom As Date = tbDateFrom.Text
        Dim dDateTo As Date = tbDateTo.Text
        Dim frenchCultureInfo = New Globalization.CultureInfo("fr-CA")
        Dim dtVentes As New DataTable
        Dim drVentes As DataRow
        Dim drORG As DataRow
        Dim dtORG As New DataTable
        Dim dtCom As New DataTable
        Dim drInvoice As DataRow
        Dim dtInvoice As New DataTable
        Dim iRow As Integer = 0
        Dim oPeriode As New Periodes
        SpreadsheetInfo.SetLicense("ERDC-FN4O-YKYN-4DBM")
        Dim strPeriode As String = vbNullString
        Dim sFile As String = ""
        Dim Icount As Integer = 0
        Dim ef As ExcelFile = Nothing
        Dim dblTotal As Double = 0
        Dim iFromMonth As Integer = 0
        Dim iToMonth As Integer = 0
        Dim iCurMonth As Integer = 0
        Dim dblTotalAmount As Double = 0
        Dim dblTotalTPS As Double = 0
        Dim dblTotalTVQ As Double = 0
        Dim dblTotalAmountCommission As Double = 0
        Dim dblTotalTPSCommission As Double = 0
        Dim dblTotalTVQCommission As Double = 0
        Dim iNumMonth As Integer = 0
        Dim dCurrentDateFrom As DateTime
        Dim dCurrentDateTo As DateTime
        Dim dblGrandTotalAmount As Double = 0
        Dim dblGrandTotalAmountCommission As Double = 0
        Dim dtEtats As New DataTable
        Dim drEtats As DataRow
        Dim result2 As DialogResult

        Try


            iFromMonth = dDateFrom.Month
            iToMonth = dDateTo.Month

            Dim sMonthFROM As String = GetMonthName(dDateFrom, frenchCultureInfo).ToString
            Dim sMonthTO As String = GetMonthName(dDateTo, frenchCultureInfo).ToString


            If Not IsNothing(cbPeriodes.EditValue) Then
                IDPeriode = cbPeriodes.EditValue
            Else
                MessageBox.Show("Vous devez sélectionner une période dans la liste des périodes d'états")
                Exit Sub
            End If


            dtEtats = oEtatsVentesData.SelectAllByPeriode(IDPeriode)
            If dtEtats.Rows.Count > 0 Then

                result2 = MessageBox.Show("Les états de ventes ont deja été générés, Voulez-vous les Re-générer?", "Etats de ventes", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
                If result2 = DialogResult.No Then
                    Me.Cursor = Cursors.Default
                ElseIf result2 = DialogResult.Yes Then

                    For Each drEtats In dtEtats.Rows
                        Dim sNomFichier As String = drEtats("CheminRapport")

                        If My.Computer.FileSystem.FileExists(sNomFichier) Then
                            My.Computer.FileSystem.DeleteFile(sNomFichier)
                        End If
                    Next

                    oEtatsData.DeleteByPeriode(IDPeriode)
                    GoTo GENERERERETAT
                End If
            End If



            Dim Result = MessageBox.Show("Désirez-vous imprimer l'État des ventes?", "État des ventes", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
            If Result = DialogResult.No Then
                Exit Sub
            ElseIf Result = DialogResult.Yes Then



GENERERERETAT:


                If dDateFrom.Month <> dDateTo.Month Then
                    oPeriode.Periode = dDateFrom.Day & " " & sMonthFROM & "-" & dDateTo.Day & " " & sMonthTO & " " & dDateTo.Year.ToString
                Else
                    oPeriode.Periode = dDateFrom.Day & "-" & dDateTo.Day & " " & sMonthTO & " " & dDateTo.Year.ToString
                End If

                strPeriode = oPeriode.Periode

                My.Computer.FileSystem.CreateDirectory(go_Globals.LocationVentes & "\" & strPeriode)


                Dim sExcelPath As String = Application.StartupPath() & "\Ventes.xlsx"
                ef = ExcelFile.Load(sExcelPath)

                Dim dtO As New DataTable
                Dim drO As DataRow
                Dim iRowL As Integer = 6



                ' Affiche la liste des organisations sur le tab des commissions
                dtO = oInvoiceData.SelectDISTINCTAllByDate(dDateFrom, dDateTo)
                If dtO.Rows.Count > 0 Then
                    For Each drO In dtO.Rows
                        ef.Worksheets(0).Cells(iRowL, 0).Value = drO("OrganisationID")
                        ef.Worksheets(0).Cells(iRowL, 1).Value = drO("Organisation").ToString.Trim
                        iRowL = iRowL + 1
                    Next
                End If







                dtORG = oInvoiceData.SelectAllInvoicesParOrganisation(dDateFrom, dDateTo)


                iRow = 7

                If dtORG.Rows.Count > 0 Then

                    ProgressBarControl1.Properties.Step = 1
                    ProgressBarControl1.Properties.PercentView = True
                    ProgressBarControl1.Properties.Maximum = dtORG.Rows.Count
                    ProgressBarControl1.Properties.Minimum = 1

                    '------------------------------------------------------------------------------------------------------
                    '  All Organisations with invoices
                    '------------------------------------------------------------------------------------------------------
                    For Each drORG In dtORG.Rows

                        '------------------------------------------------------------------------------------------------------
                        '  All invoices for an organisation
                        '------------------------------------------------------------------------------------------------------
                        dtInvoice = oInvoiceData.SelectInvoiceByDate(dDateFrom, dDateTo, drORG("OrganisationID"))
                        If dtInvoice.Rows.Count > 0 Then

                            For Each drInvoice In dtInvoice.Rows


                                dblTotalAmount = If(drInvoice.IsNull("Amount"), 0, drInvoice("Amount").ToString.Trim)
                                dblTotalTPS = If(drInvoice.IsNull("TPS"), 0, drInvoice("TPS").ToString.Trim)
                                dblTotalTVQ = If(drInvoice.IsNull("TVQ"), 0, drInvoice("TVQ").ToString.Trim)
                                dblTotal = If(drInvoice.IsNull("MontantTotal"), 0, drInvoice("MontantTotal").ToString.Trim)




                                dCurrentDateFrom = drInvoice("DateFrom")
                                dCurrentDateTo = drInvoice("DateTo")

                                iCurMonth = dCurrentDateFrom.Month


                                sMonthFROM = GetMonthName(dCurrentDateFrom, frenchCultureInfo).ToString
                                sMonthTO = GetMonthName(dCurrentDateTo, frenchCultureInfo).ToString


                                ef.Worksheets(iCurMonth).Cells(2, 0).Value = Now.Day.ToString & " " & GetMonthName(Now, frenchCultureInfo).ToString & " " & Now.Year.ToString


                                If dDateFrom.Month <> dDateTo.Month Then
                                    oPeriode.Periode = dCurrentDateFrom.Day & " " & sMonthFROM & "-" & dCurrentDateTo.Day & " " & sMonthTO & " " & dCurrentDateTo.Year.ToString
                                Else
                                    oPeriode.Periode = dCurrentDateFrom.Day & "-" & dCurrentDateTo.Day & " " & sMonthTO & " " & dCurrentDateFrom.Year.ToString
                                End If


                                ' Find next available row
                                Dim IrowFind As Integer = 5
                                Do While ef.Worksheets(iCurMonth).Cells(IrowFind, 0).Value <> Nothing
                                    IrowFind = IrowFind + 1
                                Loop

                                ef.Worksheets(iCurMonth).Cells(IrowFind, 0).Value = oPeriode.Periode
                                ef.Worksheets(iCurMonth).Cells(IrowFind, 1).Value = IIf(drInvoice.IsNull("Organisation"), "", drInvoice("Organisation").ToString.Trim)
                                ef.Worksheets(iCurMonth).Cells(IrowFind, 3).Style.NumberFormat = "$0.00"
                                ef.Worksheets(iCurMonth).Cells(IrowFind, 3).Value = dblTotalAmount
                                ef.Worksheets(iCurMonth).Cells(IrowFind, 6).Style.NumberFormat = "$0.00"
                                ef.Worksheets(iCurMonth).Cells(IrowFind, 6).Value = dblTotalAmount

                                iRow = IrowFind

                                Dim iTabRowL As Integer = 6

                                ' Cherche pour le livreur sur le tab des commission et affiche son total
                                Do While ef.Worksheets(0).Cells(iTabRowL, 1).Value <> Nothing
                                    If ef.Worksheets(0).Cells(iTabRowL, 1).Value = drInvoice("Organisation").ToString.Trim Then
                                        ef.Worksheets(0).Cells(iTabRowL, iCurMonth + 1).Style.NumberFormat = "$0.00"
                                        ef.Worksheets(0).Cells(iTabRowL, iCurMonth + 1).Value = ef.Worksheets(0).Cells(iTabRowL, iCurMonth + 1).Value + dblTotalAmount

                                        Exit Do
                                    Else
                                        iTabRowL = iTabRowL + 1
                                    End If
                                Loop




                            Next
                        End If
                    Next
                End If





                '------------------------------------------------------------------------------------------------------
                ' Ajouts Ventes
                '------------------------------------------------------------------------------------------------------

                dtVentes = oVentesData.SelectAllByDate(dDateFrom, dDateTo)
                If dtVentes.Rows.Count > 0 Then



                    For Each drVentes In dtVentes.Rows


                        dblTotalAmount = If(drVentes.IsNull("Amount"), 0, drVentes("Amount").ToString.Trim)
                        dblTotalTPS = If(drVentes.IsNull("TPS"), 0, drVentes("TPS").ToString.Trim)
                        dblTotalTVQ = If(drVentes.IsNull("TVQ"), 0, drVentes("TVQ").ToString.Trim)
                        dblTotal = If(drVentes.IsNull("MontantTotal"), 0, drVentes("MontantTotal").ToString.Trim)

                        dCurrentDateFrom = drVentes("DateFrom")
                        dCurrentDateTo = drVentes("DateTo")

                        iCurMonth = dCurrentDateFrom.Month


                        sMonthFROM = GetMonthName(dCurrentDateFrom, frenchCultureInfo).ToString
                        sMonthTO = GetMonthName(dCurrentDateTo, frenchCultureInfo).ToString




                        If dDateFrom.Month <> dDateTo.Month Then
                            oPeriode.Periode = dCurrentDateFrom.Day & " " & sMonthFROM & "-" & dCurrentDateTo.Day & " " & sMonthTO & " " & dCurrentDateTo.Year.ToString
                        Else
                            oPeriode.Periode = dCurrentDateFrom.Day & "-" & dCurrentDateTo.Day & " " & sMonthTO & " " & dCurrentDateFrom.Year.ToString
                        End If



                        Dim IrowFind As Integer = iRow

                        Do While ef.Worksheets(iCurMonth).Cells(IrowFind, 1).Value <> Nothing
                            IrowFind = IrowFind + 1
                        Loop

                        ef.Worksheets(iCurMonth).Cells(IrowFind, 0).Style.Font.Weight = ExcelFont.BoldWeight
                        ef.Worksheets(iCurMonth).Cells(IrowFind, 0).Value = oPeriode.Periode
                        ef.Worksheets(iCurMonth).Cells(IrowFind, 1).Style.Font.Weight = ExcelFont.BoldWeight
                        ef.Worksheets(iCurMonth).Cells(IrowFind, 1).Value = IIf(drVentes.IsNull("Organisation"), "", drVentes("Organisation").ToString.Trim)
                        ef.Worksheets(iCurMonth).Cells(IrowFind, 3).Style.Font.Weight = ExcelFont.BoldWeight
                        ef.Worksheets(iCurMonth).Cells(IrowFind, 3).Style.NumberFormat = "$0.00"
                        ef.Worksheets(iCurMonth).Cells(IrowFind, 3).Value = dblTotalAmount
                        ef.Worksheets(iCurMonth).Cells(IrowFind, 6).Style.Font.Weight = ExcelFont.BoldWeight
                        ef.Worksheets(iCurMonth).Cells(IrowFind, 6).Style.NumberFormat = "$0.00"
                        ef.Worksheets(iCurMonth).Cells(IrowFind, 6).Value = dblTotalAmount
                        iRow = iRow + 1



                        Dim iTabRowL As Integer = 6

                        ' Cherche pour le livreur sur le tab des commission et affiche son total
                        Do While ef.Worksheets(0).Cells(iTabRowL, 1).Value <> Nothing
                            If ef.Worksheets(0).Cells(iTabRowL, 1).Value = drVentes("Organisation").ToString.Trim Then
                                ef.Worksheets(0).Cells(iTabRowL, iCurMonth + 1).Style.NumberFormat = "$0.00"
                                ef.Worksheets(0).Cells(iTabRowL, iCurMonth + 1).Value = ef.Worksheets(0).Cells(iTabRowL, iCurMonth + 1).Value + dblTotal

                                Exit Do
                            Else
                                iTabRowL = iTabRowL + 1
                            End If
                        Loop



                    Next

                End If




                '------------------------------------------------------------------------------------------------------
                ' Calculate Totals
                '------------------------------------------------------------------------------------------------------
                Dim iTab As Integer = 0
                Dim iTabRow As Integer = 6
                Dim dblTabTotal As Double

                For iTab = 0 To 11

                    Do While ef.Worksheets(iTab).Cells(iTabRow, 3).Value <> Nothing
                        dblTabTotal = dblTabTotal + ef.Worksheets(iTab).Cells(iTabRow, 3).Value
                        iTabRow = iTabRow + 1
                    Loop

                    If dblTabTotal > 0 Then
                        ef.Worksheets(iTab).Cells(4, 1).Value = dblTabTotal
                        ef.Worksheets(iTab).Cells(4, 1).Style.NumberFormat = "$0.00"
                    End If

                    dblTabTotal = 0
                    iTabRow = 6


                Next

                '------------------------------------------------------------------------------------------------------
                ' Calculate Totals Commissions
                '------------------------------------------------------------------------------------------------------
                Dim iTabRowC As Integer

                iTabRowC = 6
                Do While ef.Worksheets(0).Cells(iTabRowC, 1).Value <> Nothing
                    dblTotal = 0
                    For i = 2 To 13
                        dblTotal = dblTotal + Convert.ToDouble(ef.Worksheets(0).Cells(iTabRowC, i).Value)
                    Next
                    ef.Worksheets(0).Cells(iTabRowC, 14).Style.NumberFormat = "$0.00"
                    ef.Worksheets(0).Cells(iTabRowC, 14).Value = Convert.ToDouble(dblTotal)


                    iTabRowC = iTabRowC + 1
                Loop

                iTabRowC = 6
                dblTotal = 0
                Do While ef.Worksheets(0).Cells(iTabRowC, 1).Value <> Nothing
                    dblTotal = dblTotal + Convert.ToDouble(ef.Worksheets(0).Cells(iTabRowC, 14).Value)
                    iTabRowC = iTabRowC + 1
                Loop
                ef.Worksheets(0).Cells(4, 1).Style.NumberFormat = "$0.00"
                ef.Worksheets(0).Cells(4, 1).Value = Math.Round(Convert.ToDouble(dblTotal), 2)


                'calculer les totaux de chaque colonne sur commission


                iTabRow = 6
                iTab = 0
                Dim dblTabTotal1 As Double = 0
                Dim dblTabTotal2 As Double = 0
                Dim dblTabTotal3 As Double = 0
                Dim dblTabTotal4 As Double = 0
                Dim dblTabTotal5 As Double = 0
                Dim dblTabTotal6 As Double = 0
                Dim dblTabTotal7 As Double = 0
                Dim dblTabTotal8 As Double = 0
                Dim dblTabTotal9 As Double = 0
                Dim dblTabTotal10 As Double = 0
                Dim dblTabTotal11 As Double = 0
                Dim dblTabTotal12 As Double = 0
                Dim dblTabTotal13 As Double = 0


                Do While ef.Worksheets(iTab).Cells(iTabRow, 1).Value <> Nothing
                    dblTabTotal1 = dblTabTotal1 + ef.Worksheets(iTab).Cells(iTabRow, 2).Value
                    dblTabTotal2 = dblTabTotal2 + ef.Worksheets(iTab).Cells(iTabRow, 3).Value
                    dblTabTotal3 = dblTabTotal3 + ef.Worksheets(iTab).Cells(iTabRow, 4).Value
                    dblTabTotal4 = dblTabTotal4 + ef.Worksheets(iTab).Cells(iTabRow, 5).Value
                    dblTabTotal5 = dblTabTotal5 + ef.Worksheets(iTab).Cells(iTabRow, 6).Value
                    dblTabTotal6 = dblTabTotal6 + ef.Worksheets(iTab).Cells(iTabRow, 7).Value
                    dblTabTotal7 = dblTabTotal7 + ef.Worksheets(iTab).Cells(iTabRow, 8).Value
                    dblTabTotal8 = dblTabTotal8 + ef.Worksheets(iTab).Cells(iTabRow, 9).Value
                    dblTabTotal9 = dblTabTotal9 + ef.Worksheets(iTab).Cells(iTabRow, 10).Value
                    dblTabTotal10 = dblTabTotal10 + ef.Worksheets(iTab).Cells(iTabRow, 11).Value
                    dblTabTotal11 = dblTabTotal11 + ef.Worksheets(iTab).Cells(iTabRow, 12).Value
                    dblTabTotal12 = dblTabTotal12 + ef.Worksheets(iTab).Cells(iTabRow, 13).Value
                    dblTabTotal13 = dblTabTotal13 + ef.Worksheets(iTab).Cells(iTabRow, 14).Value
                    iTabRow = iTabRow + 1
                    Loop


                iTabRow = iTabRow + 3
                ef.Worksheets(iTab).Cells(iTabRow, 0).Value = "Total"


                ef.Worksheets(iTab).Cells(iTabRow, 2).Value = dblTabTotal1
                ef.Worksheets(iTab).Cells(iTabRow, 2).Style.NumberFormat = "$0.00"
                ef.Worksheets(iTab).Cells(iTabRow, 3).Value = dblTabTotal2
                ef.Worksheets(iTab).Cells(iTabRow, 3).Style.NumberFormat = "$0.00"
                ef.Worksheets(iTab).Cells(iTabRow, 4).Value = dblTabTotal3
                ef.Worksheets(iTab).Cells(iTabRow, 4).Style.NumberFormat = "$0.00"
                ef.Worksheets(iTab).Cells(iTabRow, 5).Value = dblTabTotal4
                ef.Worksheets(iTab).Cells(iTabRow, 5).Style.NumberFormat = "$0.00"
                ef.Worksheets(iTab).Cells(iTabRow, 6).Value = dblTabTotal5
                ef.Worksheets(iTab).Cells(iTabRow, 6).Style.NumberFormat = "$0.00"
                ef.Worksheets(iTab).Cells(iTabRow, 7).Value = dblTabTotal6
                ef.Worksheets(iTab).Cells(iTabRow, 7).Style.NumberFormat = "$0.00"
                ef.Worksheets(iTab).Cells(iTabRow, 8).Value = dblTabTotal7
                ef.Worksheets(iTab).Cells(iTabRow, 8).Style.NumberFormat = "$0.00"
                ef.Worksheets(iTab).Cells(iTabRow, 9).Value = dblTabTotal8
                ef.Worksheets(iTab).Cells(iTabRow, 9).Style.NumberFormat = "$0.00"
                ef.Worksheets(iTab).Cells(iTabRow, 10).Value = dblTabTotal9
                ef.Worksheets(iTab).Cells(iTabRow, 10).Style.NumberFormat = "$0.00"
                ef.Worksheets(iTab).Cells(iTabRow, 11).Value = dblTabTotal10
                ef.Worksheets(iTab).Cells(iTabRow, 11).Style.NumberFormat = "$0.00"
                ef.Worksheets(iTab).Cells(iTabRow, 12).Value = dblTabTotal11
                ef.Worksheets(iTab).Cells(iTabRow, 12).Style.NumberFormat = "$0.00"
                ef.Worksheets(iTab).Cells(iTabRow, 13).Value = dblTabTotal12
                ef.Worksheets(iTab).Cells(iTabRow, 13).Style.NumberFormat = "$0.00"
                ef.Worksheets(iTab).Cells(iTabRow, 14).Value = dblTabTotal13
                ef.Worksheets(iTab).Cells(iTabRow, 14).Style.NumberFormat = "$0.00"





                'calculer les totaux sur les autres tab
                For iTab = 1 To 12

                    iTabRow = 6


                    dblTabTotal2 = 0
                    dblTabTotal3 = 0
                    dblTabTotal4 = 0
                    dblTabTotal5 = 0


                    Do While ef.Worksheets(iTab).Cells(iTabRow, 1).Value <> Nothing
                        dblTabTotal2 = dblTabTotal2 + ef.Worksheets(iTab).Cells(iTabRow, 3).Value
                        dblTabTotal3 = dblTabTotal3 + ef.Worksheets(iTab).Cells(iTabRow, 4).Value
                        dblTabTotal4 = dblTabTotal4 + ef.Worksheets(iTab).Cells(iTabRow, 5).Value
                        dblTabTotal5 = dblTabTotal5 + ef.Worksheets(iTab).Cells(iTabRow, 6).Value
                        iTabRow = iTabRow + 1
                    Loop


                    iTabRow = iTabRow + 3
                    ef.Worksheets(iTab).Cells(iTabRow, 0).Value = "Total"



                    ef.Worksheets(iTab).Cells(iTabRow, 3).Value = dblTabTotal2
                    ef.Worksheets(iTab).Cells(iTabRow, 3).Style.NumberFormat = "$0.00"
                    ef.Worksheets(iTab).Cells(iTabRow, 4).Value = dblTabTotal3
                    ef.Worksheets(iTab).Cells(iTabRow, 4).Style.NumberFormat = "$0.00"
                    ef.Worksheets(iTab).Cells(iTabRow, 5).Value = dblTabTotal4
                    ef.Worksheets(iTab).Cells(iTabRow, 5).Style.NumberFormat = "$0.00"
                    ef.Worksheets(iTab).Cells(iTabRow, 6).Value = dblTabTotal5
                    ef.Worksheets(iTab).Cells(iTabRow, 6).Style.NumberFormat = "$0.00"

                    dblTabTotal = 0
                    iTabRow = 6


                Next












                ProgressBarControl1.PerformStep()
                ProgressBarControl1.Update()

                sMonthFROM = GetMonthName(dDateFrom, frenchCultureInfo).ToString
                sMonthTO = GetMonthName(dDateTo, frenchCultureInfo).ToString


                If dDateFrom.Month <> dDateTo.Month Then
                    oPeriode.Periode = dDateFrom.Day & " " & sMonthFROM & "-" & dDateTo.Day & " " & sMonthTO & " " & dDateTo.Year.ToString
                Else
                    oPeriode.Periode = dDateFrom.Day & "-" & dDateTo.Day & " " & sMonthTO & " " & dDateTo.Year.ToString
                End If

                strPeriode = oPeriode.Periode


                sFile = "Etat des Ventes_" & strPeriode & ".xlsx"
                sFile = sFile.Replace("'", "")




                Dim oEtatsVentes As New EtatsVentes

                oEtatsVentes.IDPeriode = IDPeriode
                oEtatsVentes.DateFrom = dDateFrom
                oEtatsVentes.DateTo = dDateTo
                oEtatsVentes.NomFichier = sFile
                oEtatsVentes.CheminRapport = go_Globals.LocationVentes & "\" & strPeriode & "\" & sFile
                oEtatsVentesData.Add(oEtatsVentes)



                ef.Save(go_Globals.LocationVentes & "\" & strPeriode & "\" & sFile)
                ef = Nothing

                iRow = 7
                Icount = 0
                dblTotal = 0




            End If


            ProgressBarControl1.EditValue = 0
            ProgressBarControl1.Update()
            Process.Start(go_Globals.LocationVentes & "\" & strPeriode)
            Me.Cursor = Cursors.Default

        Catch ex As Exception
            MessageBox.Show(ex.Message, "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try

    End Sub

    Public Function GetCompleteMonthCount(ByVal d1 As DateTime, ByVal d2 As DateTime) As Integer

        'If d1.Day <> 1 Then
        '    d1 = d1.AddMonths(1)
        '    d1 = New DateTime(d1.Year, d1.Month, 1)
        'End If

        'If d2.Day <> 1 Then
        '    d2 = New DateTime(d2.Year, d2.Month, 1)
        'End If

        Return DateDiff(DateInterval.Month, d1, d2)
    End Function


    Private Sub bEtatDesAchats_ItemClick(sender As Object, e As ItemClickEventArgs) Handles bEtatDesAchats.ItemClick
        Dim dDateFrom As Date = tbDateFrom.Text
        Dim dDateTo As Date = tbDateTo.Text
        Dim oInvoiceData As New InvoiceData
        SpreadsheetInfo.SetLicense("ERDC-FN4O-YKYN-4DBM")
        Dim frenchCultureInfo = New Globalization.CultureInfo("fr-CA")
        Dim dtCommission As New DataTable
        Dim drCommission As DataRow
        Dim dtAchatLivreur As New DataTable
        Dim drAchatLivreur As DataRow
        Dim dtLivreurPaye As New DataTable
        Dim drLivreurPaye As DataRow
        Dim dtAchatFournisseur As New DataTable
        Dim dtPaye As New DataTable
        Dim drPaye As DataRow
        Dim iRow As Integer = 0
        Dim oPeriode As New Periodes
        Dim strPeriode As String = vbNullString
        Dim sFile As String = ""
        Dim Icount As Integer = 0
        Dim ef As ExcelFile = Nothing
        Dim dblTotal As Double = 0
        Dim iFromMonth As Integer = 0
        Dim iToMonth As Integer = 0
        Dim iCurMonth As Integer = 0
        Dim dblTotalAmount As Double = 0
        Dim dblTotalTPS As Double = 0
        Dim dblTotalTVQ As Double = 0
        Dim dblTotalAmountCommission As Double = 0
        Dim dblTotalTPSCommission As Double = 0
        Dim dblTotalTVQCommission As Double = 0
        Dim iNumMonth As Integer = 0
        Dim dCurrentDateFrom As DateTime
        Dim dCurrentDateTo As DateTime
        Dim dblGrandTotalAmount As Double = 0
        Dim dblGrandTotalAmountCommission As Double = 0
        Dim dtEtats As New DataTable
        Dim drEtats As DataRow
        Dim result2 As DialogResult
        Dim iTabRowL As Integer = 0

        Try
            'Dim styleA As New CellStyle
            'styleA.HorizontalAlignment = HorizontalAlignmentStyle.Left
            'styleA.VerticalAlignment = VerticalAlignmentStyle.Center
            'styleA.WrapText = True



            'Dim styleB As New CellStyle
            'styleB.HorizontalAlignment = HorizontalAlignmentStyle.Left
            'styleB.VerticalAlignment = VerticalAlignmentStyle.Center
            'styleB.WrapText = True
            'styleB.FillPattern.SetSolid(Color.YellowGreen)

            'Dim styleX As New CellStyle
            'styleX.HorizontalAlignment = HorizontalAlignmentStyle.Left
            'styleX.VerticalAlignment = VerticalAlignmentStyle.Center
            'styleX.WrapText = True
            'styleX.FillPattern.SetSolid(Color.Aquamarine)


            'Dim styleCC As New CellStyle
            'styleB.HorizontalAlignment = HorizontalAlignmentStyle.Left
            'styleB.VerticalAlignment = VerticalAlignmentStyle.Center
            'styleB.WrapText = True
            'styleB.FillPattern.SetSolid(Color.YellowGreen)

            iFromMonth = dDateFrom.Month
            iToMonth = dDateTo.Month

            Dim sMonthFROM As String = GetMonthName(dDateFrom, frenchCultureInfo).ToString
            Dim sMonthTO As String = GetMonthName(dDateTo, frenchCultureInfo).ToString


            If Not IsNothing(cbPeriodes.EditValue) Then
                IDPeriode = cbPeriodes.EditValue
            Else
                MessageBox.Show("Vous devez sélectionner une période dans la liste des périodes d'états")
                Exit Sub
            End If


            dtEtats = oEtatsAchatsData.SelectAllByPeriode(IDPeriode)
            If dtEtats.Rows.Count > 0 Then

                result2 = MessageBox.Show("Les états de achats ont deja été générés, Voulez-vous les Re-générer?", "achats", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
                If result2 = DialogResult.No Then
                    Me.Cursor = Cursors.Default
                ElseIf result2 = DialogResult.Yes Then

                    For Each drEtats In dtEtats.Rows
                        Dim sNomFichier As String = drEtats("CheminRapport")

                        If My.Computer.FileSystem.FileExists(sNomFichier) Then
                            My.Computer.FileSystem.DeleteFile(sNomFichier)
                        End If
                    Next

                    oEtatsData.DeleteByPeriode(IDPeriode)

                    GoTo GENERERERETAT
                End If
            End If




            Dim Result = MessageBox.Show("Désirez-vous imprimer l'État des achats?", "État des achats", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
            If Result = DialogResult.No Then
                Exit Sub
            ElseIf Result = DialogResult.Yes Then


GENERERERETAT:

                If dDateFrom.Month <> dDateTo.Month Then
                    oPeriode.Periode = dDateFrom.Day & " " & sMonthFROM & "-" & dDateTo.Day & " " & sMonthTO & " " & dDateTo.Year.ToString
                Else
                    oPeriode.Periode = dDateFrom.Day & "-" & dDateTo.Day & " " & sMonthTO & " " & dDateTo.Year.ToString
                End If

                strPeriode = oPeriode.Periode



                My.Computer.FileSystem.CreateDirectory(go_Globals.LocationAchats & "\" & strPeriode)

                Dim sExcelPath As String = Application.StartupPath() & "\Achats.xlsx"
                ef = ExcelFile.Load(sExcelPath)


                ' Select All livreur with paye
                dtLivreurPaye = oPayeData.SelectAllLivreurs(dDateFrom, dDateTo)
                If dtLivreurPaye.Rows.Count > 0 Then

                    ProgressBarControl1.Properties.Step = 1
                    ProgressBarControl1.Properties.PercentView = True
                    ProgressBarControl1.Properties.Maximum = dtPaye.Rows.Count
                    ProgressBarControl1.Properties.Minimum = 1

                    'Ajoute les livreurs sur le tab commission pour l'ajout des donnees Commissions
                    Dim drLivreur As DataRow
                    Dim iRowL As Integer = 6

                    For Each drLivreur In dtLivreurPaye.Rows


                        ef.Worksheets(0).Cells(iRowL, 0).Style.HorizontalAlignment = HorizontalAlignmentStyle.Left
                        ef.Worksheets(0).Cells(iRowL, 0).Value = drLivreur("IDLivreur")
                        ef.Worksheets(0).Cells(iRowL, 1).Style.HorizontalAlignment = HorizontalAlignmentStyle.Left
                        ef.Worksheets(0).Cells(iRowL, 1).Value = drLivreur("Livreur").ToString.Trim
                        iRowL = iRowL + 1
                    Next

                    dtAchatLivreur = oAchatsData.SelectDISTINCTAllByDate(dDateFrom, dDateTo)
                    If dtAchatLivreur.Rows.Count > 0 Then
                        For Each drAchatLivreur In dtAchatLivreur.Rows
                            'ef.Worksheets(0).Cells(iRowL, 0).Value = drAchatLivreur("IDFournisseur")
                            ef.Worksheets(0).Cells(iRowL, 1).Style.HorizontalAlignment = HorizontalAlignmentStyle.Left
                            ef.Worksheets(0).Cells(iRowL, 1).Value = drAchatLivreur("Fournisseur").ToString.Trim
                            iRowL = iRowL + 1
                        Next
                    End If


                    dtCommission = oCommissionData.SelectAllLivreurCommission(dDateFrom, dDateTo)
                    If dtCommission.Rows.Count > 0 Then
                        For Each drCommission In dtCommission.Rows
                            ef.Worksheets(0).Cells(iRowL, 0).Value = drCommission("IDLivreur")
                            'ef.Worksheets(0).Cells(iRowL, 0).Style = styleX
                            ef.Worksheets(0).Cells(iRowL, 1).Value = drCommission("Livreur").ToString.Trim
                            'ef.Worksheets(0).Cells(iRowL, 1).Style = styleX
                            iRowL = iRowL + 1
                        Next
                    End If




                    For Each drLivreurPaye In dtLivreurPaye.Rows

                        'If drLivreurPaye("IDLivreur") = 24 Then

                        '    MessageBox.Show("")
                        'End If
                        '------------------------------------------------------------------------------------------------------
                        ' Ajoute tous les payes des livreurs
                        '------------------------------------------------------------------------------------------------------
                        dtPaye = oPayeData.SelectAllPayeParLivreur(dDateFrom, dDateTo, drLivreurPaye("IDLivreur"), drLivreurPaye("Livreur"))
                        If dtPaye.Rows.Count > 0 Then
                            For Each drPaye In dtPaye.Rows


                                dblTotalAmount = If(drPaye.IsNull("Amount"), 0, drPaye("Amount").ToString.Trim)
                                dblTotalTPS = If(drPaye.IsNull("TPS"), 0, drPaye("TPS").ToString.Trim)
                                dblTotalTVQ = If(drPaye.IsNull("TVQ"), 0, drPaye("TVQ").ToString.Trim)
                                dblTotal = dblTotalAmount + dblTotalTPS + dblTotalTVQ

                                dCurrentDateFrom = drPaye("DateFrom")
                                dCurrentDateTo = drPaye("DateTo")

                                iCurMonth = dCurrentDateFrom.Month


                                sMonthFROM = GetMonthName(dCurrentDateFrom, frenchCultureInfo).ToString
                                sMonthTO = GetMonthName(dCurrentDateTo, frenchCultureInfo).ToString


                                ef.Worksheets(iCurMonth).Cells(2, 0).Value = Now.Day.ToString & " " & GetMonthName(Now, frenchCultureInfo).ToString & " " & Now.Year.ToString


                                If dDateFrom.Month <> dDateTo.Month Then
                                    oPeriode.Periode = dCurrentDateFrom.Day & " " & sMonthFROM & "-" & dCurrentDateTo.Day & " " & sMonthTO & " " & dCurrentDateTo.Year.ToString
                                Else
                                    oPeriode.Periode = dCurrentDateFrom.Day & "-" & dCurrentDateTo.Day & " " & sMonthTO & " " & dCurrentDateFrom.Year.ToString
                                End If


                                ' Find next available row
                                Dim IrowFind As Integer = 5
                                Do While ef.Worksheets(iCurMonth).Cells(IrowFind, 0).Value <> Nothing
                                    IrowFind = IrowFind + 1
                                Loop

                                ef.Worksheets(iCurMonth).Cells(IrowFind, 0).Value = oPeriode.Periode
                                ef.Worksheets(iCurMonth).Cells(IrowFind, 1).Value = IIf(drPaye.IsNull("Livreur"), "", drPaye("Livreur").ToString.Trim)
                                ef.Worksheets(iCurMonth).Cells(IrowFind, 3).Style.NumberFormat = "$0.00"
                                ef.Worksheets(iCurMonth).Cells(IrowFind, 3).Value = dblTotalAmount
                                ef.Worksheets(iCurMonth).Cells(IrowFind, 6).Style.NumberFormat = "$0.00"
                                ef.Worksheets(iCurMonth).Cells(IrowFind, 6).Value = dblTotalTPS
                                ef.Worksheets(iCurMonth).Cells(IrowFind, 7).Style.NumberFormat = "$0.00"
                                ef.Worksheets(iCurMonth).Cells(IrowFind, 7).Value = dblTotalTVQ
                                ef.Worksheets(iCurMonth).Cells(IrowFind, 8).Style.NumberFormat = "$0.00"
                                ef.Worksheets(iCurMonth).Cells(IrowFind, 8).Value = dblTotal


                                iTabRowL = 6


                                ' Cherche pour le livreur sur le tab des commission et affiche son total
                                Do While ef.Worksheets(0).Cells(iTabRowL, 1).Value <> Nothing
                                    If ef.Worksheets(0).Cells(iTabRowL, 1).Value = drPaye("Livreur").ToString.Trim Then

                                        ef.Worksheets(0).Cells(iTabRowL, iCurMonth + 1).Value = ef.Worksheets(0).Cells(iTabRowL, iCurMonth + 1).Value + dblTotal
                                        ef.Worksheets(0).Cells(iTabRowL, iCurMonth + 1).Style.NumberFormat = "$0.00"

                                        Exit Do
                                    Else
                                        iTabRowL = iTabRowL + 1
                                    End If
                                Loop


                            Next

                        End If
                    Next
                End If



                '------------------------------------------------------------------------------------------------------
                ' Ajouts Les achats de l'ecran achats
                '------------------------------------------------------------------------------------------------------


                dtAchatLivreur = oAchatsData.SelectAllByDate(dDateFrom, dDateTo)
                If dtAchatLivreur.Rows.Count > 0 Then

                    For Each drAchatLivreur In dtAchatLivreur.Rows


                        dblTotalAmount = If(drAchatLivreur.IsNull("Montant"), 0, drAchatLivreur("Montant").ToString.Trim)
                        dblTotalTPS = If(drAchatLivreur.IsNull("TPS"), 0, drAchatLivreur("TPS").ToString.Trim)
                        dblTotalTVQ = If(drAchatLivreur.IsNull("TVQ"), 0, drAchatLivreur("TVQ").ToString.Trim)
                        dblTotal = If(drAchatLivreur.IsNull("Total"), 0, drAchatLivreur("Total").ToString.Trim)


                        dCurrentDateFrom = drAchatLivreur("DateAchat")


                        iCurMonth = dCurrentDateFrom.Month

                        sMonthFROM = GetMonthName(dCurrentDateFrom, frenchCultureInfo).ToString
                        sMonthTO = GetMonthName(dCurrentDateTo, frenchCultureInfo).ToString



                        ' Find next available row
                        Dim IrowFind As Integer = 6
                        Do While ef.Worksheets(iCurMonth).Cells(IrowFind, 1).Value <> Nothing
                            If ef.Worksheets(iCurMonth).Cells(IrowFind, 1).Value.ToString.Trim = drAchatLivreur("Fournisseur").ToString.Trim Then
                                Exit Do
                            Else
                                IrowFind = IrowFind + 1
                            End If

                        Loop



                        'ef.Worksheets(iCurMonth).Cells(IrowFind, 0).Style = styleCC
                        ef.Worksheets(iCurMonth).Cells(IrowFind, 0).Value = GetMonthNameNumber(iCurMonth)
                        'ef.Worksheets(iCurMonth).Cells(IrowFind, 0).Style = styleCC
                        'ef.Worksheets(iCurMonth).Cells(IrowFind, 1).Style = styleCC
                        ef.Worksheets(iCurMonth).Cells(IrowFind, 1).Style.Font.Weight = ExcelFont.BoldWeight
                        ef.Worksheets(iCurMonth).Cells(IrowFind, 1).Value = IIf(drAchatLivreur.IsNull("Fournisseur"), "", drAchatLivreur("Fournisseur").ToString.Trim)
                        'ef.Worksheets(iCurMonth).Cells(IrowFind, 2).Style = styleCC
                        'ef.Worksheets(iCurMonth).Cells(IrowFind, 3).Style = styleCC

                        ef.Worksheets(iCurMonth).Cells(IrowFind, 3).Value = ef.Worksheets(iCurMonth).Cells(IrowFind, 3).Value + dblTotalAmount
                        ef.Worksheets(iCurMonth).Cells(IrowFind, 3).Style.NumberFormat = "$0.00"
                        'ef.Worksheets(iCurMonth).Cells(IrowFind, 4).Style = styleCC
                        'ef.Worksheets(iCurMonth).Cells(IrowFind, 5).Style = styleCC
                        'ef.Worksheets(iCurMonth).Cells(IrowFind, 6).Style = styleCC

                        ef.Worksheets(iCurMonth).Cells(IrowFind, 6).Value = ef.Worksheets(iCurMonth).Cells(IrowFind, 6).Value + dblTotalTPS
                        ef.Worksheets(iCurMonth).Cells(IrowFind, 6).Style.NumberFormat = "$0.00"
                        'ef.Worksheets(iCurMonth).Cells(IrowFind, 7).Style = styleCC

                        ef.Worksheets(iCurMonth).Cells(IrowFind, 7).Value = ef.Worksheets(iCurMonth).Cells(IrowFind, 7).Value + dblTotalTVQ
                        ef.Worksheets(iCurMonth).Cells(IrowFind, 7).Style.NumberFormat = "$0.00"
                        'ef.Worksheets(iCurMonth).Cells(IrowFind, 8).Style = styleCC

                        ef.Worksheets(iCurMonth).Cells(IrowFind, 8).Value = ef.Worksheets(iCurMonth).Cells(IrowFind, 8).Value + dblTotal
                        ef.Worksheets(iCurMonth).Cells(IrowFind, 8).Style.NumberFormat = "$0.00"
                        iRow = iRow + 1



                        iTabRowL = 6

                        ' Cherche pour le livreur sur le tab des commission et affiche son total
                        Do While ef.Worksheets(0).Cells(iTabRowL, 1).Value <> Nothing
                            'ef.Worksheets(0).Cells(iTabRowL, 1).Style = styleCC
                            If ef.Worksheets(0).Cells(iTabRowL, 1).Value = drAchatLivreur("Fournisseur").ToString.Trim Then
                                'ef.Worksheets(0).Cells(iTabRowL, 1).Style = styleCC

                                'ef.Worksheets(0).Cells(iTabRowL, iCurMonth + 1).Style = styleCC
                                'ef.Worksheets(0).Cells(iTabRowL, 14).Style = styleB

                                ef.Worksheets(0).Cells(iTabRowL, iCurMonth + 1).Value = ef.Worksheets(0).Cells(iTabRowL, iCurMonth + 1).Value + dblTotal
                                ef.Worksheets(0).Cells(iTabRowL, iCurMonth + 1).Style.NumberFormat = "$0.00"


                                Exit Do
                            Else
                                iTabRowL = iTabRowL + 1
                            End If
                        Loop


                    Next
                End If



                '------------------------------------------------------------------------------------------------------
                ' Ajoute Les commissions
                '------------------------------------------------------------------------------------------------------
                dtCommission = oCommissionData.SelectAllByDate(dDateFrom, dDateTo)
                If dtCommission.Rows.Count > 0 Then

                    For Each drCommission In dtCommission.Rows


                        dblTotalAmount = If(drCommission.IsNull("Amount"), 0, drCommission("Amount").ToString.Trim)
                        dblTotalTPS = If(drCommission.IsNull("TPS"), 0, drCommission("TPS").ToString.Trim)
                        dblTotalTVQ = If(drCommission.IsNull("TVQ"), 0, drCommission("TVQ").ToString.Trim)
                        dblTotal = If(drCommission.IsNull("Total"), 0, drCommission("Total").ToString.Trim)


                        dCurrentDateFrom = drCommission("DateFrom")
                        dCurrentDateTo = drCommission("DateTo")

                        iCurMonth = dCurrentDateFrom.Month

                        sMonthFROM = GetMonthName(dCurrentDateFrom, frenchCultureInfo).ToString
                        sMonthTO = GetMonthName(dCurrentDateTo, frenchCultureInfo).ToString


                        ef.Worksheets(iCurMonth).Cells(2, 0).Value = Now.Day.ToString & " " & GetMonthName(Now, frenchCultureInfo).ToString & " " & Now.Year.ToString


                        If dDateFrom.Month <> dDateTo.Month Then
                            oPeriode.Periode = dCurrentDateFrom.Day & " " & sMonthFROM & "-" & dCurrentDateTo.Day & " " & sMonthTO & " " & dCurrentDateTo.Year.ToString
                        Else
                            oPeriode.Periode = dCurrentDateFrom.Day & "-" & dCurrentDateTo.Day & " " & sMonthTO & " " & dCurrentDateFrom.Year.ToString
                        End If



                        ' Find next available row
                        Dim IrowFind As Integer = 5
                        Do While ef.Worksheets(iCurMonth).Cells(IrowFind, 0).Value <> Nothing
                            IrowFind = IrowFind + 1
                        Loop

                        'ef.Worksheets(iCurMonth).Cells(IrowFind, 0).Style = styleX
                        ef.Worksheets(iCurMonth).Cells(IrowFind, 0).Value = oPeriode.Periode
                        ef.Worksheets(iCurMonth).Cells(IrowFind, 1).Style.Font.Weight = ExcelFont.BoldWeight
                        'ef.Worksheets(iCurMonth).Cells(IrowFind, 1).Style = styleX
                        ef.Worksheets(iCurMonth).Cells(IrowFind, 1).Value = IIf(drCommission.IsNull("Livreur"), "", drCommission("Livreur").ToString.Trim)

                        'ef.Worksheets(iCurMonth).Cells(IrowFind, 2).Style = styleX
                        'ef.Worksheets(iCurMonth).Cells(IrowFind, 3).Style = styleX

                        ef.Worksheets(iCurMonth).Cells(IrowFind, 3).Value = dblTotalAmount
                        ef.Worksheets(iCurMonth).Cells(IrowFind, 3).Style.NumberFormat = "$0.00"

                        ef.Worksheets(iCurMonth).Cells(IrowFind, 6).Value = dblTotalTPS
                        ef.Worksheets(iCurMonth).Cells(IrowFind, 6).Style.NumberFormat = "$0.00"

                        ef.Worksheets(iCurMonth).Cells(IrowFind, 7).Value = dblTotalTVQ
                        ef.Worksheets(iCurMonth).Cells(IrowFind, 7).Style.NumberFormat = "$0.00"

                        ef.Worksheets(iCurMonth).Cells(IrowFind, 8).Value = dblTotal
                        ef.Worksheets(iCurMonth).Cells(IrowFind, 8).Style.NumberFormat = "$0.00"
                        iRow = iRow + 1



                        iTabRowL = 6

                        ' Cherche pour le livreur sur le tab des commission et affiche son total
                        Do While ef.Worksheets(0).Cells(iTabRowL, 1).Value <> Nothing
                            If ef.Worksheets(0).Cells(iTabRowL, 1).Value = drCommission("Livreur").ToString.Trim Then


                                ef.Worksheets(0).Cells(iTabRowL, 1).Value = drCommission("Livreur").ToString.Trim
                                'ef.Worksheets(0).Cells(iTabRowL, 1).Style = styleA
                                'ef.Worksheets(0).Cells(iTabRowL, iCurMonth + 1).Style = styleA


                                ef.Worksheets(0).Cells(iTabRowL, iCurMonth + 1).Value = ef.Worksheets(0).Cells(iTabRowL, iCurMonth + 1).Value + dblTotal
                                ef.Worksheets(0).Cells(iTabRowL, iCurMonth + 1).Style.NumberFormat = "$0.00"

                                'ef.Worksheets(0).Cells(iTabRowL, 0).Style = styleX
                                'ef.Worksheets(0).Cells(iTabRowL, 1).Style = styleX
                                'ef.Worksheets(0).Cells(iTabRowL, 2).Style = styleX
                                'ef.Worksheets(0).Cells(iTabRowL, 3).Style = styleX
                                'ef.Worksheets(0).Cells(iTabRowL, 4).Style = styleX
                                'ef.Worksheets(0).Cells(iTabRowL, 5).Style = styleX
                                'ef.Worksheets(0).Cells(iTabRowL, 6).Style = styleX
                                'ef.Worksheets(0).Cells(iTabRowL, 7).Style = styleX
                                'ef.Worksheets(0).Cells(iTabRowL, 8).Style = styleX
                                'ef.Worksheets(0).Cells(iTabRowL, 9).Style = styleX
                                'ef.Worksheets(0).Cells(iTabRowL, 10).Style = styleX
                                'ef.Worksheets(0).Cells(iTabRowL, 11).Style = styleX
                                'ef.Worksheets(0).Cells(iTabRowL, 12).Style = styleX
                                'ef.Worksheets(0).Cells(iTabRowL, 13).Style = styleX
                                'ef.Worksheets(0).Cells(iTabRowL, 14).Style = styleX

                                Exit Do
                            Else
                                iTabRowL = iTabRowL + 1
                            End If
                        Loop




                    Next
                End If


                '------------------------------------------------------------------------------------------------------
                ' Calculate Totals
                '------------------------------------------------------------------------------------------------------
                Dim iTab As Integer = 0
                Dim iTabRow As Integer = 6
                Dim dblTabTotal As Double

                For iTab = 1 To 12

                    Do While ef.Worksheets(iTab).Cells(iTabRow, 0).Value <> Nothing


                        dblTotal = Convert.ToDouble(ef.Worksheets(iTab).Cells(iTabRow, 8).Value)
                        Debug.Print(dblTotal)

                        dblTabTotal = dblTabTotal + dblTotal
                        iTabRow = iTabRow + 1
                    Loop

                    If dblTabTotal > 0 Then
                        ef.Worksheets(iTab).Cells(4, 1).Value = dblTabTotal
                        ef.Worksheets(iTab).Cells(4, 1).Style.NumberFormat = "$0.00"
                    End If

                    dblTabTotal = 0
                    iTabRow = 6

                Next


                '------------------------------------------------------------------------------------------------------
                ' Calculate Totals Commissions
                '------------------------------------------------------------------------------------------------------

                iTabRowL = 6
                Do While ef.Worksheets(0).Cells(iTabRowL, 1).Value <> Nothing
                    dblTotal = 0
                    For i = 2 To 13
                        dblTotal = dblTotal + Convert.ToDouble(ef.Worksheets(0).Cells(iTabRowL, i).Value)
                    Next
                    ef.Worksheets(0).Cells(iTabRowL, 14).Value = Convert.ToDouble(dblTotal)
                    ef.Worksheets(0).Cells(iTabRowL, 14).Style.NumberFormat = "$0.00"

                    iTabRowL = iTabRowL + 1
                Loop

                iTabRowL = 6
                dblTotal = 0
                Do While ef.Worksheets(0).Cells(iTabRowL, 1).Value <> Nothing
                    dblTotal = dblTotal + Convert.ToDouble(ef.Worksheets(0).Cells(iTabRowL, 14).Value)
                    iTabRowL = iTabRowL + 1
                Loop
                ef.Worksheets(0).Cells(4, 1).Value = Math.Round(Convert.ToDouble(dblTotal), 2)





                'calculer les totaux de chaque colonne sur commission


                iTabRow = 6
                iTab = 0
                Dim dblTabTotal1 As Double = 0
                Dim dblTabTotal2 As Double = 0
                Dim dblTabTotal3 As Double = 0
                Dim dblTabTotal4 As Double = 0
                Dim dblTabTotal5 As Double = 0
                Dim dblTabTotal6 As Double = 0
                Dim dblTabTotal7 As Double = 0
                Dim dblTabTotal8 As Double = 0
                Dim dblTabTotal9 As Double = 0
                Dim dblTabTotal10 As Double = 0
                Dim dblTabTotal11 As Double = 0
                Dim dblTabTotal12 As Double = 0
                Dim dblTabTotal13 As Double = 0


                Do While ef.Worksheets(iTab).Cells(iTabRow, 1).Value <> Nothing
                    dblTabTotal1 = dblTabTotal1 + ef.Worksheets(iTab).Cells(iTabRow, 2).Value
                    dblTabTotal2 = dblTabTotal2 + ef.Worksheets(iTab).Cells(iTabRow, 3).Value
                    dblTabTotal3 = dblTabTotal3 + ef.Worksheets(iTab).Cells(iTabRow, 4).Value
                    dblTabTotal4 = dblTabTotal4 + ef.Worksheets(iTab).Cells(iTabRow, 5).Value
                    dblTabTotal5 = dblTabTotal5 + ef.Worksheets(iTab).Cells(iTabRow, 6).Value
                    dblTabTotal6 = dblTabTotal6 + ef.Worksheets(iTab).Cells(iTabRow, 7).Value
                    dblTabTotal7 = dblTabTotal7 + ef.Worksheets(iTab).Cells(iTabRow, 8).Value
                    dblTabTotal8 = dblTabTotal8 + ef.Worksheets(iTab).Cells(iTabRow, 9).Value
                    dblTabTotal9 = dblTabTotal9 + ef.Worksheets(iTab).Cells(iTabRow, 10).Value
                    dblTabTotal10 = dblTabTotal10 + ef.Worksheets(iTab).Cells(iTabRow, 11).Value
                    dblTabTotal11 = dblTabTotal11 + ef.Worksheets(iTab).Cells(iTabRow, 12).Value
                    dblTabTotal12 = dblTabTotal12 + ef.Worksheets(iTab).Cells(iTabRow, 13).Value
                    dblTabTotal13 = dblTabTotal13 + ef.Worksheets(iTab).Cells(iTabRow, 14).Value
                    iTabRow = iTabRow + 1
                Loop


                iTabRow = iTabRow + 3
                ef.Worksheets(iTab).Cells(iTabRow, 0).Value = "Total"


                ef.Worksheets(iTab).Cells(iTabRow, 2).Value = dblTabTotal1
                ef.Worksheets(iTab).Cells(iTabRow, 2).Style.NumberFormat = "$0.00"
                ef.Worksheets(iTab).Cells(iTabRow, 3).Value = dblTabTotal2
                ef.Worksheets(iTab).Cells(iTabRow, 3).Style.NumberFormat = "$0.00"
                ef.Worksheets(iTab).Cells(iTabRow, 4).Value = dblTabTotal3
                ef.Worksheets(iTab).Cells(iTabRow, 4).Style.NumberFormat = "$0.00"
                ef.Worksheets(iTab).Cells(iTabRow, 5).Value = dblTabTotal4
                ef.Worksheets(iTab).Cells(iTabRow, 5).Style.NumberFormat = "$0.00"
                ef.Worksheets(iTab).Cells(iTabRow, 6).Value = dblTabTotal5
                ef.Worksheets(iTab).Cells(iTabRow, 6).Style.NumberFormat = "$0.00"
                ef.Worksheets(iTab).Cells(iTabRow, 7).Value = dblTabTotal6
                ef.Worksheets(iTab).Cells(iTabRow, 7).Style.NumberFormat = "$0.00"
                ef.Worksheets(iTab).Cells(iTabRow, 8).Value = dblTabTotal7
                ef.Worksheets(iTab).Cells(iTabRow, 8).Style.NumberFormat = "$0.00"
                ef.Worksheets(iTab).Cells(iTabRow, 9).Value = dblTabTotal8
                ef.Worksheets(iTab).Cells(iTabRow, 9).Style.NumberFormat = "$0.00"
                ef.Worksheets(iTab).Cells(iTabRow, 10).Value = dblTabTotal9
                ef.Worksheets(iTab).Cells(iTabRow, 10).Style.NumberFormat = "$0.00"
                ef.Worksheets(iTab).Cells(iTabRow, 11).Value = dblTabTotal10
                ef.Worksheets(iTab).Cells(iTabRow, 11).Style.NumberFormat = "$0.00"
                ef.Worksheets(iTab).Cells(iTabRow, 12).Value = dblTabTotal11
                ef.Worksheets(iTab).Cells(iTabRow, 12).Style.NumberFormat = "$0.00"
                ef.Worksheets(iTab).Cells(iTabRow, 13).Value = dblTabTotal12
                ef.Worksheets(iTab).Cells(iTabRow, 13).Style.NumberFormat = "$0.00"
                ef.Worksheets(iTab).Cells(iTabRow, 14).Value = dblTabTotal13
                ef.Worksheets(iTab).Cells(iTabRow, 14).Style.NumberFormat = "$0.00"





                'calculer les totaux sur les autres tab
                For iTab = 1 To 12

                    iTabRow = 6


                    dblTabTotal2 = 0
                    dblTabTotal3 = 0
                    dblTabTotal4 = 0
                    dblTabTotal5 = 0


                    Do While ef.Worksheets(iTab).Cells(iTabRow, 1).Value <> Nothing
                        dblTabTotal2 = dblTabTotal2 + ef.Worksheets(iTab).Cells(iTabRow, 3).Value
                        dblTabTotal3 = dblTabTotal3 + ef.Worksheets(iTab).Cells(iTabRow, 6).Value
                        dblTabTotal4 = dblTabTotal4 + ef.Worksheets(iTab).Cells(iTabRow, 7).Value
                        dblTabTotal5 = dblTabTotal5 + ef.Worksheets(iTab).Cells(iTabRow, 8).Value
                        iTabRow = iTabRow + 1
                    Loop


                    iTabRow = iTabRow + 3
                    ef.Worksheets(iTab).Cells(iTabRow, 0).Value = "Total"



                    ef.Worksheets(iTab).Cells(iTabRow, 3).Value = dblTabTotal2
                    ef.Worksheets(iTab).Cells(iTabRow, 3).Style.NumberFormat = "$0.00"
                    ef.Worksheets(iTab).Cells(iTabRow, 6).Value = dblTabTotal3
                    ef.Worksheets(iTab).Cells(iTabRow, 6).Style.NumberFormat = "$0.00"
                    ef.Worksheets(iTab).Cells(iTabRow, 7).Value = dblTabTotal4
                    ef.Worksheets(iTab).Cells(iTabRow, 7).Style.NumberFormat = "$0.00"
                    ef.Worksheets(iTab).Cells(iTabRow, 8).Value = dblTabTotal5
                    ef.Worksheets(iTab).Cells(iTabRow, 8).Style.NumberFormat = "$0.00"

                    dblTabTotal = 0
                    iTabRow = 6


                Next







                sFile = "Etat des depenses_" & strPeriode & ".xlsx"
                sFile = sFile.Replace("'", "")




                Dim oEtatsAchats As New EtatsAchats

                oEtatsAchats.IDPeriode = IDPeriode
                oEtatsAchats.DateFrom = dDateFrom
                oEtatsAchats.DateTo = dDateTo
                oEtatsAchats.NomFichier = sFile
                oEtatsAchats.CheminRapport = go_Globals.LocationAchats & "\" & strPeriode & "\" & sFile
                oEtatsAchatsData.Add(oEtatsAchats)



                ef.Save(go_Globals.LocationAchats & "\" & strPeriode & "\" & sFile)
                ef = Nothing

                iRow = 7
                Icount = 0
                dblTotal = 0



                ProgressBarControl1.PerformStep()
                ProgressBarControl1.Update()

            End If


            ProgressBarControl1.EditValue = 0
            ProgressBarControl1.Update()
            Process.Start(go_Globals.LocationAchats & "\" & strPeriode)
            Me.Cursor = Cursors.Default

        Catch ex As Exception
            MessageBox.Show(ex.Message, "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try

    End Sub

    Private Sub bAchats_ItemClick(sender As Object, e As ItemClickEventArgs) Handles bAchats.ItemClick
        If frmAchats.Visible = True Then
            frmAchats.WindowState = FormWindowState.Normal
            frmAchats.BringToFront()
        End If

        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        frmAchats.Show()
        Me.Cursor = System.Windows.Forms.Cursors.Default
    End Sub

    Private Sub bVentes_ItemClick(sender As Object, e As ItemClickEventArgs) Handles bVentes.ItemClick
        If frmVentes.Visible = True Then
            frmVentes.WindowState = FormWindowState.Normal
            frmVentes.BringToFront()
        End If

        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        frmVentes.Show()
        Me.Cursor = System.Windows.Forms.Cursors.Default
    End Sub

    Private Sub bImprimerCommissionRapport_Click(sender As Object, e As EventArgs)

        If IsNothing(cbPeriodes.EditValue) Then
            MessageBox.Show("Vous devez selectionner une periode")
            Exit Sub
        End If




        Me.Cursor = Cursors.Default
        Dim result = MessageBox.Show("Désirez-vous imprimer les payes?", "Imprimer Payes", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
        If result = DialogResult.No Then

        ElseIf result = DialogResult.Yes Then
            Me.Cursor = Cursors.WaitCursor
            If ImprimerLaPaie() = False Then
                Me.Cursor = Cursors.Default
                Exit Sub
            End If

        End If

        FillPaieNumber()

        Me.Cursor = Cursors.Default
    End Sub

    Private Sub bGenererPaye_Click_1(sender As Object, e As EventArgs)

        Dim iTotalCalcul As Integer = 0
        Dim dDateFrom As Date = tbDateFrom.Text
        Dim dDateTo As Date = tbDateTo.Text

        '----------------------------------------------------------------------------------------
        ' Verifie que tous les methodes de calculs ont ete referencer dans le systeme
        '----------------------------------------------------------------------------------------


        If IsNothing(cbPeriodes.EditValue) Then
            MessageBox.Show("Vous devez selectionner une periode")
            Exit Sub
        End If




        iTotalCalcul = oOrganisationData.SelectAllMetodeCalculationLivreur()

        If iTotalCalcul > 0 Then
            Me.Cursor = Cursors.Default
            MessageBox.Show("il y a " & (iTotalCalcul) & " Livreur(s) qui n'ont pas de méthode de calcul, Veuillez affecter une méthode de calcul a toutes les Livreurs")
            Exit Sub
        End If


        '----------------------------------------------------------------------------------------
        ' Facturation et paye generation
        '----------------------------------------------------------------------------------------


        Dim Result = MessageBox.Show("Générer la  paie?", "Paie", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
        If Result = DialogResult.No Then
            Me.Cursor = Cursors.Default
            Exit Sub
        ElseIf Result = DialogResult.Yes Then

            If GenererTousLesPaye() = False Then
                Me.Cursor = Cursors.Default
                Exit Sub
            End If

            CalculerPayeTotaux()

        End If


        grdImport.DataSource = oImportData.SelectAllFromDate(dDateFrom, dDateTo)
        GridView1.OptionsView.ColumnAutoWidth = False
        'GridView1.BestFitColumns()
        Dim viewInfo As DevExpress.XtraGrid.Views.Grid.ViewInfo.GridViewInfo = TryCast(GridView1.GetViewInfo(), GridViewInfo)
        If viewInfo.ViewRects.ColumnTotalWidth < viewInfo.ViewRects.ColumnPanelWidth Then
            GridView1.OptionsView.ColumnAutoWidth = True
        End If

        Me.Cursor = Cursors.Default



    End Sub

    Private Sub grdImport_Click(sender As Object, e As EventArgs) Handles grdImport.Click

    End Sub



    Private Sub cbPeriodes_EditValueChanged(sender As Object, e As EventArgs)
        Dim oPeriode As New Periodes

        Dim dtPeriode As New DataTable
        Dim drPeriode As DataRow


        If Not IsNothing(cbPeriodes.EditValue) Then

            IDPeriode = cbPeriodes.EditValue
            go_Globals.IDPeriode = IDPeriode


            dtPeriode = oPeriodeData.SelectAllByID(IDPeriode)
            If dtPeriode.Rows.Count > 0 Then
                For Each drPeriode In dtPeriode.Rows

                    tbDateFrom.EditValue = drPeriode("DateFrom")
                    tbDateTo.EditValue = drPeriode("DateTo")
                    go_Globals.PeriodeDateFrom = drPeriode("DateFrom")
                    go_Globals.PeriodeDateTo = drPeriode("DateTo")

                    bEnvoyerLivraisonsRestaurants.Enabled = True
                    bEtats.Enabled = True
                    FillAllEtatsDeCompte()

                Next
            Else
                bEnvoyerLivraisonsRestaurants.Enabled = False
                bEtats.Enabled = False


            End If
        End If


    End Sub


    Private Sub bEmailEtatDecomptes_Click(sender As Object, e As EventArgs) Handles bEnvoyerEtatCompte.Click

        Dim dDateFrom As Date = tbDateFrom.Text
        Dim dDateTo As Date = tbDateTo.Text
        Dim oPeriode As New Periodes
        Dim dtPeriode As New DataTable

        Dim frenchCultureInfo = New Globalization.CultureInfo("fr-CA")
        Dim dtEtat As New DataTable
        Dim drEtat As DataRow
        Dim oMail As New Mail
        Dim iIDRapport As Integer = 0
        Dim strSelectedValue As String = ""
        Dim iSelectedValue As Integer = 0

        Dim strEmails As String = ""

        If IsNothing(cbPeriodes.EditValue) Then
            MessageBox.Show("Vous devez selectionner une periode")
            Exit Sub
        End If


        If dDateFrom.Month <> dDateTo.Month Then
            oPeriode.Periode = dDateFrom.Day & " " & GetMonthName(dDateFrom, frenchCultureInfo) & "-" & dDateTo.Day & " " & GetMonthName(dDateTo, frenchCultureInfo) & " " & dDateTo.Year.ToString
        Else
            oPeriode.Periode = dDateFrom.Day & "-" & dDateTo.Day & " " & GetMonthName(dDateTo, frenchCultureInfo) & " " & dDateTo.Year.ToString
        End If

        IDPeriode = cbPeriodes.EditValue



        Me.Cursor = Cursors.Default
        Dim result As DialogResult = MessageBox.Show("Envoyer les Etats de comptes aux clients?", "Etats de comptes", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
        If result = DialogResult.No Then
            Exit Sub
        ElseIf result = DialogResult.Yes Then


            Try
                Me.Cursor = Cursors.WaitCursor


                For Each item As DevExpress.XtraEditors.Controls.CheckedListBoxItem In cbEtatsDeComptes.Properties.Items
                    If item.CheckState = CheckState.Checked Then
                        strSelectedValue = strSelectedValue & item.Value & ","
                        iSelectedValue = iSelectedValue + 1
                    End If
                Next

                If strSelectedValue.ToString.Trim <> "" Then
                    strSelectedValue = strSelectedValue.Remove(strSelectedValue.Length - 1)
                End If


                dtEtat = oEtatsData.SelectAllDataByPeriodes(IDPeriode, strSelectedValue)
                If dtEtat.Rows.Count > 0 Then

                    Me.Cursor = Cursors.WaitCursor

                    ProgressBarControl1.Properties.Step = 1
                    ProgressBarControl1.Properties.PercentView = True
                    ProgressBarControl1.Properties.Maximum = dtEtat.Rows.Count
                    ProgressBarControl1.Properties.Minimum = 0


                    For Each drEtat In dtEtat.Rows



                        If Not IsDBNull(drEtat("IDVendeur")) Then


                            Dim oLivreur As New livreurs
                            Dim oLivreur2 As New livreurs


                            oLivreur.LivreurID = drEtat("IDVendeur")
                            oLivreur2 = oLivreurData.Select_Record(oLivreur)


                            If Not IsNothing(oLivreur2) Then

                                strEmails = ""

                                Dim sEmail As String = If(IsNothing(oLivreur2.Email), "", oLivreur2.Email.ToString.Trim)
                                Dim sEmail1 As String = ""
                                Dim sEmail2 As String = ""
                                Dim sEmail3 As String = ""
                                Dim sSubject As String = "IDS Livraison Express:  Votre état de compte pour la période " & oPeriode.Periode
                                Dim sMessage As String = oLivreur2.Livreur.ToString.Trim & ", Nous vous remercions de choisir IDS Livraison Express "
                                Dim sAttachment As String = If(IsNothing(drEtat("CheminRapport")), Nothing, drEtat("CheminRapport").ToString.Trim)

                                If sEmail.ToString.Trim <> "" Then
                                    strEmails = strEmails & sEmail & ","
                                End If



                                If Not IsNothing(strEmails) Then

                                    strEmails = strEmails.Remove(strEmails.Length - 1)

                                    Dim GGmail As New GGSMTP_GMAIL("idslivraisonexpressrapport@gmail.com", "2015livraison",  )

                                    Dim ToAddressies As String() = {strEmails}
                                    Dim attachs() As String = {sAttachment}
                                    Dim subject As String = sSubject
                                    Dim body As String = sMessage


                                    result = GGmail.SendMail(ToAddressies, subject, body, attachs)
                                    If result Then

                                    Else
                                        MsgBox(GGmail.ErrorText, MsgBoxStyle.Critical)
                                    End If

                                End If


                            End If










                        Else


                            Dim oOrganisation As New Organisations
                            Dim oOrganisation2 As New Organisations
                            oOrganisation.OrganisationID = drEtat("IDOrganisation")
                            oOrganisation2 = oOrganisationData.Select_Record(oOrganisation)


                            If Not IsNothing(oOrganisation2) Then

                                strEmails = ""

                                Dim sEmail As String = If(IsNothing(oOrganisation2.Email), "", oOrganisation2.Email.ToString.Trim)
                                Dim sEmail1 As String = If(IsNothing(oOrganisation2.Email1), "", oOrganisation2.Email1.ToString.Trim)
                                Dim sEmail2 As String = If(IsNothing(oOrganisation2.Email2), "", oOrganisation2.Email2.ToString.Trim)
                                Dim sEmail3 As String = If(IsNothing(oOrganisation2.Email3), "", oOrganisation2.Email3.ToString.Trim)
                                Dim sSubject As String = "IDS Livraison Express:  Votre état de compte pour la période " & oPeriode.Periode
                                Dim sMessage As String = oOrganisation2.Organisation & ", Nous vous remercions de choisir IDS Livraison Express "
                                Dim sAttachment As String = If(IsNothing(drEtat("CheminRapport")), Nothing, drEtat("CheminRapport").ToString.Trim)

                                If sEmail.ToString.Trim <> "" Then
                                    strEmails = strEmails & sEmail & ","
                                End If

                                If sEmail1.ToString.Trim <> "" Then
                                    strEmails = strEmails & sEmail1 & ","
                                End If

                                If sEmail2.ToString.Trim <> "" Then
                                    strEmails = strEmails & sEmail2 & ","
                                End If

                                If sEmail3.ToString.Trim <> "" Then
                                    strEmails = strEmails & sEmail3 & ","
                                End If


                                If Not IsNothing(strEmails) Then

                                    strEmails = strEmails.Remove(strEmails.Length - 1)

                                    Dim GGmail As New GGSMTP_GMAIL("idslivraisonexpressrapport@gmail.com", "2015livraison",  )

                                    Dim ToAddressies As String() = {strEmails}
                                    Dim attachs() As String = {sAttachment}
                                    Dim subject As String = sSubject
                                    Dim body As String = sMessage


                                    result = GGmail.SendMail(ToAddressies, subject, body, attachs)
                                    If result Then

                                    Else
                                        MsgBox(GGmail.ErrorText, MsgBoxStyle.Critical)
                                    End If

                                End If


                            End If





                        End If











                        ProgressBarControl1.PerformStep()
                        ProgressBarControl1.Update()

                    Next

                    ProgressBarControl1.EditValue = 0
                    Me.Cursor = Cursors.Default
                    MessageBox.Show("Envoi des états de compte complété avec succès?")


                Else
                    Me.Cursor = Cursors.Default
                    MessageBox.Show("Aucune état de compte n'a été trouvé pour cette période!", "Etat de compte", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1)
                    Exit Sub
                End If
            Catch ex As Exception
                Me.Cursor = Cursors.Default
                MessageBox.Show(ex.Message, "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End Try


        End If

    End Sub

    Private Sub cbAnnee_EditValueChanged(sender As Object, e As EventArgs) Handles cbAnnee.EditValueChanged

        If Not IsNothing(cbAnnee.EditValue) Then
            go_Globals.Year = cbAnnee.Text
            FillAllPeriodes()
        End If
    End Sub


    Private Sub bResetEtatdeCompte_Click(sender As Object, e As EventArgs) Handles bResetEtatdeCompte.Click
        cbEtatsDeComptes.EditValue = Nothing
        cbEtatsDeComptes.Text = String.Empty
        cbEtatsDeComptes.SelectedText = Nothing
    End Sub

    Private Sub grdImport_MouseClick(sender As Object, e As MouseEventArgs) Handles grdImport.MouseClick

        Try

            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

            If GridView1.FocusedRowHandle < 0 Then Exit Sub




            Dim hitInfo = GridView1.CalcHitInfo(e.Location)
            Dim row As System.Data.DataRow = GridView1.GetDataRow(GridView1.FocusedRowHandle)

            SelectedImportRow = GridView1.FocusedRowHandle

            ImportID = row("ImportID")
            tbImportID.EditValue = ImportID
            If hitInfo.InRowCell Then
                Dim rowHandle As Integer = hitInfo.RowHandle
                Dim column As DevExpress.XtraGrid.Columns.GridColumn = hitInfo.Column

                If column.Name.ToString = "Distance" Then
                    DockEdit.FloatLocation = New Point(CInt((Me.Width - DockEdit.Width) / 2), CInt((Me.Height - DockEdit.Height) / 2))
                    DockEdit.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always
                    tbDistance.Text = row("Distance")
                    tbDistance.Focus()
                End If






            End If

            Me.Cursor = System.Windows.Forms.Cursors.Default

        Catch ex As Exception
            Me.Cursor = System.Windows.Forms.Cursors.Default

            MessageBox.Show(ex.Message, "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)

        End Try



    End Sub

    Private Sub bSauvegarde_Click(sender As Object, e As EventArgs) Handles bSauvegarde.Click

        SelectedRow = GridView1.FocusedRowHandle


        Dim oImport As New Import

        oImport.Distance = tbDistance.Text
        oImport.ImportID = tbImportID.Text


        oImportData.UpdateKM(oImport, oImport)
        grdImport.DataSource = oImportData.SelectAllImportFromPeriode(IDPeriode)

        oImportData.DeleteFromBackup(IDPeriode)
        oImportData.CopyImportToBackup()

        DockEdit.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Hidden


    End Sub

    Private Sub btnAnnuler_Click(sender As Object, e As EventArgs) Handles btnAnnuler.Click
        DockEdit.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Hidden
    End Sub

    Private Sub bResetPeriode_Click(sender As Object, e As EventArgs) Handles bResetPeriode.Click
        cbPeriodes.EditValue = Nothing
        cbPeriodes.Text = String.Empty

        LayoutDroite.Enabled = False
        LayoutBas.Enabled = False

        'bEtats.Enabled = False
        bGenererLesCommission.Enabled = False
        bImprimerCommisssions.Enabled = False
        bAchats.Enabled = False
        bVentes.Enabled = False
        bFactures.Enabled = False
        bPayes.Enabled = False

        bGenererLaPaye.Enabled = False
        bConsolidation.Enabled = False
        'bGenererPaye.Enabled = False
        bImprimerFactures.Enabled = False
        btnGenererFacturation.Enabled = False
        'bGenererPaye.Enabled = False
        bImportMobilus.Enabled = False
        bEnvoyerPaye.Enabled = False
        bEnvoyerFacture.Enabled = False
        bSelectfile.Enabled = False
        bImprimerCommissionRapport.Enabled = False
        go_Globals.IDPeriode = 0


    End Sub
    Private Sub ClearFilters()
        GridView1.Columns("Client").FilterInfo = New ColumnFilterInfo("")
        GridView1.Columns("ClientAddress").FilterInfo = New ColumnFilterInfo("")
        GridView1.Columns("Organisation").FilterInfo = New ColumnFilterInfo("")
        GridView1.Columns("Livreur").FilterInfo = New ColumnFilterInfo("")

    End Sub
    Private Sub bAddToImport_Click(sender As Object, e As EventArgs) Handles bAddToImport.Click



        Dim oData_Set As New DataSet
        Dim dDateLivraison As DateTime = tbDateLivraison.Text
        Dim dDateFrom As Date = tbDateFrom.Text
        Dim dDateTo As Date = tbDateTo.Text
        Dim dDateImport As Date = tbDateLivraison.Text
        Dim oImport As New Import
        Dim oPeriode As New Periodes
        Dim dtPeriode As New DataTable
        Dim blnCheckDate As Boolean = False
        Dim frenchCultureInfo = New Globalization.CultureInfo("fr-CA")
        My.Application.ChangeCulture("en-US")

        If IsNothing(cbPeriodes.EditValue) Then
            MessageBox.Show("Vous devez selectionner une periode")
            tabMain.SelectedTabPage = tabMain.TabPages(0)
            Exit Sub
        End If


        If IsNothing(cbRestaurant.EditValue) Then
            MessageBox.Show("Vous devez selectionner un Commerce")
            Exit Sub
        End If


        If IsNothing(cblivreur.EditValue) Then
            MessageBox.Show("Vous devez selectionner un livreur")
            Exit Sub
        End If


        If tbCustomerAddressID.Text > 0 Then



            If tbAdresseNo.Text.ToString.Trim = "" Then
                MessageBox.Show("Vous devez entrer un numéro d'addresse")
                Exit Sub
            End If
            If tbAdresse.Text.ToString.Trim = "" Then
                MessageBox.Show("Vous devez entrer une addresse")
                Exit Sub
            End If

            If tbTelephone.Text.ToString.Trim = "" Then
                MessageBox.Show("Vous devez entrer un téléphone")
                Exit Sub
            End If



            bMiseAJourAddresse_Click(sender, e)

        End If

        If tbCustomerAddressID.Text = 0 Then

            If tbAdresseNo.Text.ToString.Trim = "" Then
                MessageBox.Show("Vous devez entrer un numéro d'addresse")
                Exit Sub
            End If
            If tbAdresse.Text.ToString.Trim = "" Then
                MessageBox.Show("Vous devez entrer une addresse")
                Exit Sub
            End If

            If tbTelephone.Text.ToString.Trim = "" Then
                MessageBox.Show("Vous devez entrer un téléphone")
                Exit Sub
            End If

            bAjouterAddresse(sender, e)

        End If


        Dim result As DialogResult = MessageBox.Show("Sauvegarder cette livraison?", "Nouvelle livraison", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
        If result = DialogResult.No Then
            Me.Cursor = Cursors.Default
            Exit Sub
        ElseIf result = DialogResult.Yes Then

        End If




        If dDateImport > dDateFrom AndAlso dDateImport < dDateTo Then

        Else
            Me.Cursor = Cursors.Default
            MessageBox.Show("La date d'aujourd'hui ne correspond pas a la période sélectionée", "Mauvaise période sélectionnée", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Exit Sub
        End If


        Try


            IsLoading = True

            Me.Cursor = Cursors.WaitCursor


            Application.DoEvents()

            Process.GetCurrentProcess().PriorityClass = ProcessPriorityClass.RealTime

            oImport.IDNomFichierImport = Nothing
            oImport.Reference = Nothing
            oImport.Code = Nothing
            oImport.Status = Nothing
            oImport.Client = tbNom.Text.ToString.Trim
            oImport.ClientAddress = tbAdresseNo.Text.ToString.Trim & " " & tbAdresse.Text.ToString.Trim
            oImport.MomentDePassage = Nothing
            oImport.PreparePar = Nothing
            oImport.HeureAppel = dDateLivraison
            oImport.HeurePrep = dDateLivraison
            oImport.HeureDepart = dDateLivraison
            oImport.HeureLivraison = dDateLivraison
            oImport.MessagePassage = Nothing
            oImport.TotalTempService = Nothing
            oImport.Livreur = cblivreur.Text
            oImport.LivreurID = cblivreur.EditValue
            If tbDistanceR.Text > 0 Then
                oImport.Distance = tbDistanceR.Text
            Else
                oImport.Distance = Nothing
            End If

            oImport.Organisation = cbRestaurant.Text.ToString.Trim
            oImport.OrganisationID = cbRestaurant.EditValue
            oImport.IDPeriode = IDPeriode
            oImport.Debit = 0
            oImport.Credit = 0
            oImport.NombreGlacier = 0
            oImport.MontantGlacierLivreur = 0
            oImport.MontantGlacierOrganisation = 0
            oImport.MontantContrat = 0
            oImport.ExtraKM = 0
            oImport.MontantLivreur = tbMontantLivreur.Text
            oImport.Montant = tbMontantOrganisation.Text
            oImport.Debit = 0
            oImport.Credit = 0
            oImport.NombreGlacier = 0
            oImport.Extra = 0
            oImport.ExtraKMLivreur = 0
            oImport.ExtraLivreur = 0
            oImport.IDSAmount = 0
            oImport.DateImport = dDateLivraison
            oImport.IsMobilus = False
            oImport.Echange = False
            oImport.IDClientAddresse = CustomerAddressID

            Dim bSucess As Integer
            Dim oDataClass As New ImportData
            bSucess = oDataClass.AddLivraison(oImport)

            If bSucess > 0 Then

                tbImportIDL.Text = bSucess
                oImport.ImportID = bSucess

                oImportData.DeleteFromBackup(IDPeriode)
                oImportData.CopyImportToBackup()

                IsLoading = False

                tabMain.TabPages(2).PageVisible = False
                tabMain.SelectedTabPage = tabMain.TabPages(1)


            Else
                Me.Cursor = Cursors.Default
                MessageBox.Show("L'insertion de cette livraison a échouée")
            End If


            grdImport.DataSource = oImportData.SelectAllFromDate(tbDateFrom.Text, tbDateTo.Text)

            GridView1.OptionsView.ColumnAutoWidth = False
            'GridView1.BestFitColumns()
            Dim viewInfo As DevExpress.XtraGrid.Views.Grid.ViewInfo.GridViewInfo = TryCast(GridView1.GetViewInfo(), GridViewInfo)
            If viewInfo.ViewRects.ColumnTotalWidth < viewInfo.ViewRects.ColumnPanelWidth Then
                GridView1.OptionsView.ColumnAutoWidth = True
            End If

            Dim rowHandle As Integer = GridView1.LocateByValue("ImportID", oImport.ImportID)
            If rowHandle <> DevExpress.XtraGrid.GridControl.InvalidRowHandle Then GridView1.FocusedRowHandle = rowHandle
            grdClientsAddresse.DataSource = oClientAddresseData.SelectAll()



            Me.Cursor = Cursors.Default


            Exit Sub

        Catch ex As Exception
            Me.Cursor = Cursors.Default
            MessageBox.Show(ex.Message, "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try



        Exit Sub



    End Sub

    Private Sub bReturnToAction_Click(sender As Object, e As EventArgs) Handles bReturnToAction.Click
        tabMain.TabPages(2).PageVisible = False
        tabMain.SelectedTabPage = tabMain.TabPages(1)
    End Sub





    Private Sub tbAdresseNo_EditValueChanged(sender As Object, e As EventArgs) Handles tbAdresseNo.EditValueChanged
        If IsGridClick = True Then Exit Sub
        Dim sFiltre As String = ""

        sFiltre = "[AdresseNo] Like '" & tbAdresseNo.Text & "%'"
        GridView3.Columns("AdresseNo").FilterInfo = New ColumnFilterInfo(sFiltre)
        GridView3.FocusedRowHandle = 1
    End Sub

    Private Sub tbAdresse_EditValueChanged(sender As Object, e As EventArgs) Handles tbAdresse.EditValueChanged
        If IsGridClick = True Then Exit Sub
        Dim sFiltre As String = ""

        sFiltre = "[Adresse] Like '" & tbAdresse.Text & "%'"
        GridView3.Columns("Adresse").FilterInfo = New ColumnFilterInfo(sFiltre)
        GridView3.FocusedRowHandle = 1
    End Sub


    Private Sub tbTelephone_EditValueChanged(sender As Object, e As EventArgs) Handles tbTelephone.EditValueChanged
        If IsGridClick = True Then Exit Sub
        Dim sFiltre As String = ""

        sFiltre = "[Telephone] LIKE '" & tbTelephone.Text & "%'"
        GridView3.Columns("Telephone").FilterInfo = New ColumnFilterInfo(sFiltre)
        GridView3.FocusedRowHandle = 1
    End Sub

    Private Sub tbAppartement_EditValueChanged(sender As Object, e As EventArgs) Handles tbAppartement.EditValueChanged
        If IsGridClick = True Then Exit Sub
        Dim sFiltre As String = ""

        sFiltre = "[Appartement] LIKE '" & tbAppartement.Text & "%'"
        GridView3.Columns("Appartement").FilterInfo = New ColumnFilterInfo(sFiltre)
        GridView3.FocusedRowHandle = 1
    End Sub

    Private Sub ClearRecord()

        'tbImportIDL.Text = 0
        'cbRestaurant.SelectedIndex = -1
        tbCustomerAddressID.Text = 0
        tbNom.Text = Nothing
        tbAdresseNo.Text = Nothing
        tbAdresse.Text = Nothing
        cbVille.EditValue = Nothing
        tbCodePostale.Text = Nothing
        tbAppartement.Text = Nothing
        tbCodeEntree.Text = Nothing
        tbTelephone.Text = Nothing
        tbMobile.Text = Nothing
        tbMobile.Text = Nothing





    End Sub
    Private Sub grdClientsAddresse_Click(sender As Object, e As EventArgs) Handles grdClientsAddresse.Click
        If GridView3.FocusedRowHandle < 0 Then Exit Sub

        IsGridClick = True

        bSupprimerAddresse.Enabled = True


        ClearRecord()

        Dim row As System.Data.DataRow = GridView3.GetDataRow(GridView3.FocusedRowHandle)

        Dim clsClientAddresse As New ClientAddresse
        clsClientAddresse.IDClientAddress = System.Convert.ToInt32(row("IDClientAddress"))
        clsClientAddresse = oClientAddresseData.Select_Record(clsClientAddresse)

        If Not clsClientAddresse Is Nothing Then
            Try

                tbCustomerAddressID.Text = System.Convert.ToInt32(clsClientAddresse.IDClientAddress)
                tbNom.Text = If(IsNothing(clsClientAddresse.Nom), Nothing, clsClientAddresse.Nom.ToString.Trim)
                tbAdresseNo.Text = If(IsNothing(clsClientAddresse.AdresseNo), Nothing, clsClientAddresse.AdresseNo.ToString.Trim)
                tbAdresse.Text = If(IsNothing(clsClientAddresse.Adresse), Nothing, clsClientAddresse.Adresse.ToString.Trim)
                cbVille.EditValue = If(IsNothing(clsClientAddresse.VilleID), Nothing, clsClientAddresse.VilleID)
                tbCodePostale.Text = If(IsNothing(clsClientAddresse.CodePostale), Nothing, clsClientAddresse.CodePostale.ToUpper.ToString.Trim)
                tbAppartement.Text = If(IsNothing(clsClientAddresse.Appartement), Nothing, clsClientAddresse.Appartement.ToString.Trim)
                tbCodeEntree.Text = If(IsNothing(clsClientAddresse.CodeEntree), Nothing, clsClientAddresse.CodeEntree.ToString.Trim)
                tbTelephone.Text = If(IsNothing(clsClientAddresse.Telephone), Nothing, GetNumbers(clsClientAddresse.Telephone.ToString.Trim))
                tbMobile.Text = If(IsNothing(clsClientAddresse.Mobile), Nothing, GetNumbers(clsClientAddresse.Mobile.ToString.Trim))

            Catch
            End Try
        End If

        IsGridClick = False

    End Sub
    Private Sub LoadClientAddresse()

        Dim dtClientAddress As New DataTable

        dtClientAddress = oClientAddresseData.SelectAll()
        grdClientsAddresse.DataSource = dtClientAddress
    End Sub



    Private Sub bSupprimerAddresse_Click(sender As Object, e As EventArgs) Handles bSupprimerAddresse.Click

        Dim result As DialogResult = MessageBox.Show("Supprimer cette addresse?", "Supprimer addresse", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
        If result = DialogResult.No Then
            Me.Cursor = Cursors.Default
            Exit Sub
        ElseIf result = DialogResult.Yes Then

        End If

        If tbCustomerAddressID.Text.ToString.Trim = "" Then
            MessageBox.Show("Vous devez sélectionner une addresse sur la grille d'addresse")
            Exit Sub
        End If


        Dim row As System.Data.DataRow = GridView3.GetDataRow(GridView3.FocusedRowHandle)
        oClientAddresse.IDClientAddress = System.Convert.ToInt32(row("IDClientAddress").ToString)
        oClientAddresseData.Delete(oClientAddresse)

        LoadClientAddresse()

        GridView3.FocusedRowHandle = SelectedRow
        bSupprimerAddresse.Enabled = False



    End Sub

    Private Sub cbRestaurant_SelectedIndexChanged(sender As Object, e As EventArgs)
        If IsLoading = True Then Exit Sub

        Dim dtOrganisation As New DataTable
        Dim oOrganisation As New Organisations

        If cbRestaurant.EditValue > 0 Then

            oOrganisation.OrganisationID = cbRestaurant.EditValue
            oOrganisation = oOrganisationData.Select_Record(oOrganisation)

            If Not oOrganisation Is Nothing Then
                Try

                    tbMontantOrganisation.Text = If(IsNothing(oOrganisation.MontantContrat), 0, oOrganisation.MontantContrat)
                    tbMontantLivreur.Text = If(IsNothing(oOrganisation.PayeLivreur), 0, oOrganisation.PayeLivreur)

                Catch
                End Try
            End If





        End If



    End Sub

    Private Sub cbVille_SelectedIndexChanged(sender As Object, e As EventArgs)

    End Sub

    Public Shared Function GetNumbers(ByVal text As String) As String
        text = If(text, String.Empty)
        Return New String(text.Where(Function(p) Char.IsDigit(p)).ToArray())
    End Function

    Private Sub bMiseAJourAddresse_Click(sender As Object, e As EventArgs)

        Dim row As System.Data.DataRow = GridView3.GetDataRow(GridView3.FocusedRowHandle)

        Dim SelectedRow As Integer = GridView3.FocusedRowHandle

        Dim clsClientAddresse As New ClientAddresse



        With clsClientAddresse
            CustomerAddressID = If(String.IsNullOrEmpty(tbCustomerAddressID.Text), Nothing, tbCustomerAddressID.Text)
            .IDClientAddress = If(String.IsNullOrEmpty(tbCustomerAddressID.Text), Nothing, tbCustomerAddressID.Text)
            .Nom = If(String.IsNullOrEmpty(tbNom.Text), Nothing, tbNom.Text)
            .AdresseNo = If(String.IsNullOrEmpty(tbAdresseNo.Text), Nothing, tbAdresseNo.Text)
            .Adresse = If(String.IsNullOrEmpty(tbAdresse.Text), Nothing, tbAdresse.Text)
            .Telephone = If(String.IsNullOrEmpty(tbTelephone.Text), Nothing, tbTelephone.Text)
            .VilleID = cbVille.EditValue
            .CodePostale = If(String.IsNullOrEmpty(tbCodePostale.Text), Nothing, tbCodePostale.Text)
            .Appartement = If(String.IsNullOrEmpty(tbAppartement.Text), Nothing, tbAppartement.Text)
            .CodeEntree = If(String.IsNullOrEmpty(tbCodeEntree.Text), Nothing, tbCodeEntree.Text)
            .Mobile = If(String.IsNullOrEmpty(tbMobile.Text), Nothing, tbMobile.Text)
        End With


        Dim bSucess As Boolean
        bSucess = oClientAddresseData.Update(clsClientAddresse, clsClientAddresse)
        If bSucess = True Then
            LoadClientAddresse()
            GridView3.FocusedRowHandle = SelectedRow
            bSupprimerAddresse.Enabled = False

        Else
            MsgBox("La mise a jour échouée.", MsgBoxStyle.OkOnly, "Erreur")
        End If
    End Sub

    Private Sub bAjouterAddresse(sender As Object, e As EventArgs)
        Dim clsClientAddresse As New ClientAddresse


        With clsClientAddresse

            .Nom = If(String.IsNullOrEmpty(tbNom.Text), Nothing, tbNom.Text)
            .AdresseNo = If(String.IsNullOrEmpty(tbAdresseNo.Text), Nothing, tbAdresseNo.Text)
            .Adresse = If(String.IsNullOrEmpty(tbAdresse.Text), Nothing, tbAdresse.Text)
            .Telephone = If(String.IsNullOrEmpty(tbTelephone.Text), Nothing, tbTelephone.Text)
            .VilleID = cbVille.EditValue
            .CodePostale = If(String.IsNullOrEmpty(tbCodePostale.Text), Nothing, tbCodePostale.Text)
            .Appartement = If(String.IsNullOrEmpty(tbAppartement.Text), Nothing, tbAppartement.Text)
            .CodeEntree = If(String.IsNullOrEmpty(tbCodeEntree.Text), Nothing, tbCodeEntree.Text)
            .Mobile = If(String.IsNullOrEmpty(tbMobile.Text), Nothing, tbMobile.Text)
        End With


        Dim bSucess As Integer
        bSucess = oClientAddresseData.Add(clsClientAddresse)
        If bSucess > 0 Then
            tbCustomerAddressID.Text = bSucess
            CustomerAddressID = bSucess
            LoadClientAddresse()
            GridView3.FocusedRowHandle = SelectedRow
            bSupprimerAddresse.Enabled = False


            IsLoading = False
        Else
            MsgBox("L'insertion a échouée.", MsgBoxStyle.OkOnly, "Erreur")
        End If
    End Sub
    Private Sub ClearCommande()
        tbImportIDL.Text = 0
        tbDistanceR.Text = 0
        cbRestaurant.EditValue = Nothing
        cbRestaurant.Text = String.Empty
        tbCustomerAddressID.Text = 0
        tbNom.Text = Nothing
        tbAdresseNo.Text = Nothing
        tbAdresse.Text = Nothing
        cbVille.EditValue = Nothing
        cbVille.Text = String.Empty
        tbCodePostale.Text = Nothing
        tbAppartement.Text = Nothing
        tbCodeEntree.Text = Nothing
        tbTelephone.Text = Nothing
        tbMobile.Text = Nothing
        tbMobile.Text = Nothing
        cblivreur.EditValue = Nothing
        cblivreur.Text = String.Empty

        tbMontantOrganisation.Text = 0
        tbMontantLivreur.Text = 0
        bImportMiseaJour.Enabled = False
        bAddToImport.Enabled = True

        bAddToImport.Enabled = True

    End Sub
    Private Sub bAjouterUneCommande_ItemClick(sender As Object, e As ItemClickEventArgs) Handles bAjouterUneCommande.ItemClick
        If IsNothing(cbPeriodes.EditValue) Then
            MessageBox.Show("Vous devez selectionner une periode")
            Exit Sub
        End If

        FillAllLivreur()
        FillAllRestaurants()
        GridView3.ClearColumnsFilter()
        'grdClientsAddresse.DataSource = oClientAddresseData.SelectAll()

        Dim clsImport As New Import
        clsImport.ImportID = oImportData.Select_LastID()

        If clsImport.ImportID = 0 Then GoTo NEXTSTEP
        clsImport = oImportData.Select_Record(clsImport)

        tbImportIDL.Text = clsImport.ImportID
        cbRestaurant.EditValue = clsImport.OrganisationID
        cblivreur.EditValue = clsImport.LivreurID
        tbDateLivraison.Text = DateTime.Now



        If Not IsDBNull(clsImport.IDClientAddresse) Then
            Dim clsClientAddresse As New ClientAddresse
            clsClientAddresse.IDClientAddress = System.Convert.ToInt32(clsImport.IDClientAddresse)
            clsClientAddresse = oClientAddresseData.Select_Record(clsClientAddresse)

            If Not clsClientAddresse Is Nothing Then
                Try

                    tbCustomerAddressID.Text = System.Convert.ToInt32(clsClientAddresse.IDClientAddress)
                    tbNom.Text = If(IsNothing(clsClientAddresse.Nom), Nothing, clsClientAddresse.Nom.ToString.Trim)
                    tbAdresseNo.Text = If(IsNothing(clsClientAddresse.AdresseNo), Nothing, clsClientAddresse.AdresseNo.ToString.Trim)
                    tbAdresse.Text = If(IsNothing(clsClientAddresse.Adresse), Nothing, clsClientAddresse.Adresse.ToString.Trim)
                    If IsNothing(clsClientAddresse.VilleID) Then
                        cbVille.EditValue = Nothing
                    Else
                        cbVille.EditValue = clsClientAddresse.VilleID

                    End If


                    tbCodePostale.Text = If(IsNothing(clsClientAddresse.CodePostale), Nothing, clsClientAddresse.CodePostale.ToUpper.ToString.Trim)
                    tbAppartement.Text = If(IsNothing(clsClientAddresse.Appartement), Nothing, clsClientAddresse.Appartement.ToString.Trim)
                    tbCodeEntree.Text = If(IsNothing(clsClientAddresse.CodeEntree), Nothing, clsClientAddresse.CodeEntree.ToString.Trim)
                    tbTelephone.Text = If(IsNothing(clsClientAddresse.Telephone), Nothing, GetNumbers(clsClientAddresse.Telephone.ToString.Trim))
                    tbMobile.Text = If(IsNothing(clsClientAddresse.Mobile), Nothing, GetNumbers(clsClientAddresse.Mobile.ToString.Trim))

                    bSupprimerAddresse.Enabled = True


                    tbMontantOrganisation.Text = If(IsDBNull(clsImport.Montant), 0, CType(clsImport.Montant, Decimal?))

                    tbMontantLivreur.Text = If(IsDBNull(clsImport.MontantLivreur), 0, CType(clsImport.MontantLivreur, Decimal?))

                Catch ex As Exception

                    Me.Cursor = System.Windows.Forms.Cursors.Default
                    MessageBox.Show("Ajouter une commande clic()    " & ex.Message, "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)


                End Try

            End If

NEXTSTEP:

            tbDateLivraison.Text = DateTime.Now



            bImportMiseaJour.Enabled = True
            bAddToImport.Enabled = False

        End If

        tabMain.TabPages(2).PageVisible = True
        tabMain.SelectedTabPage = tabMain.TabPages(2)
        IsGridClick = False

        bAddToImport.Enabled = True
        bImportMiseaJour.Enabled = False
        bSupprimerAddresse.Enabled = False
        grdClientsAddresse.DataSource = oClientAddresseData.SelectAll()
        tabMain.TabPages(2).PageVisible = True

        tabMain.SelectedTabPage = tabMain.TabPages(2)

        IsLoading = False
    End Sub


    Private Sub tbCodePostale_KeyPress(sender As Object, e As KeyPressEventArgs) Handles tbCodePostale.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(Keys.Return) Then
            tbCodeEntree.Focus()
            e.Handled = True
        End If
    End Sub

    Private Sub bImportMiseaJour_Click(sender As Object, e As EventArgs) Handles bImportMiseaJour.Click

        Dim oData_Set As New DataSet
        Dim dDateLivraison As DateTime = tbDateLivraison.Text
        Dim dDateFrom As Date = tbDateFrom.Text
        Dim dDateTo As Date = tbDateTo.Text
        Dim dDateImport As Date = Now
        Dim oImport As New Import
        Dim oPeriode As New Periodes
        Dim dtPeriode As New DataTable
        Dim blnCheckDate As Boolean = False
        Dim frenchCultureInfo = New Globalization.CultureInfo("fr-CA")
        My.Application.ChangeCulture("en-US")

        If IsNothing(cbPeriodes.EditValue) Then
            MessageBox.Show("Vous devez selectionner une periode")
            tabMain.SelectedTabPage = tabMain.TabPages(0)
            Exit Sub
        End If


        If IsNothing(cbRestaurant.EditValue) Then
            MessageBox.Show("Vous devez selectionner un Commerce")
            Exit Sub
        End If


        If IsNothing(cblivreur.EditValue) Then
            MessageBox.Show("Vous devez selectionner un livreur")
            Exit Sub
        End If

        If tbImportIDL.EditValue > 0 Then

        Else
            MessageBox.Show("Cet enregistrement n'a pas de ID, Vous ne pouvez le sauvegarder!")
            Exit Sub

        End If





        If tbCustomerAddressID.Text > 0 Then

            If tbAdresseNo.Text.ToString.Trim = "" Then
                MessageBox.Show("Vous devez entrer un numéro d'addresse")
                Exit Sub
            End If
            If tbAdresse.Text.ToString.Trim = "" Then
                MessageBox.Show("Vous devez entrer une addresse")
                Exit Sub
            End If

            If tbTelephone.Text.ToString.Trim = "" Then
                MessageBox.Show("Vous devez entrer un téléphone")
                Exit Sub
            End If


            bMiseAJourAddresse_Click(sender, e)

        Else

            If tbAdresseNo.Text.ToString.Trim = "" Then
                MessageBox.Show("Vous devez entrer un numéro d'addresse")
                Exit Sub
            End If
            If tbAdresse.Text.ToString.Trim = "" Then
                MessageBox.Show("Vous devez entrer une addresse")
                Exit Sub
            End If

            If tbTelephone.Text.ToString.Trim = "" Then
                MessageBox.Show("Vous devez entrer un téléphone")
                Exit Sub
            End If

            bAjouterAddresse(sender, e)

        End If


        Dim result As DialogResult = MessageBox.Show("Mettre a jour cette livraison?", "Mise a jour  livraison", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
        If result = DialogResult.No Then
            Me.Cursor = Cursors.Default
            Exit Sub
        ElseIf result = DialogResult.Yes Then

        End If





        Try


            IsLoading = True

            If dDateFrom.Month <> dDateTo.Month Then
                oPeriode.Periode = dDateFrom.Day & " " & GetMonthName(dDateFrom, frenchCultureInfo) & "-" & dDateTo.Day & " " & GetMonthName(dDateTo, frenchCultureInfo) & " " & dDateTo.Year.ToString
            Else
                oPeriode.Periode = dDateFrom.Day & "-" & dDateTo.Day & " " & GetMonthName(dDateTo, frenchCultureInfo) & " " & dDateTo.Year.ToString
            End If

            Me.Cursor = Cursors.WaitCursor



            Application.DoEvents()


            oImport.ImportID = tbImportIDL.Text
            oImport.IDNomFichierImport = Nothing
            oImport.Reference = Nothing
            oImport.Code = Nothing
            oImport.Status = Nothing
            oImport.Client = tbNom.Text
            oImport.ClientAddress = tbAdresseNo.Text & " " & tbAdresse.Text
            oImport.MomentDePassage = Nothing
            oImport.PreparePar = Nothing
            oImport.HeureAppel = dDateLivraison
            oImport.HeurePrep = dDateLivraison
            oImport.HeureDepart = dDateLivraison
            oImport.HeureLivraison = dDateLivraison
            oImport.MessagePassage = Nothing
            oImport.TotalTempService = Nothing
            oImport.Livreur = cblivreur.Text
            oImport.LivreurID = cblivreur.EditValue
            If tbDistanceR.Text > 0 Then
                oImport.Distance = tbDistanceR.Text
            Else
                oImport.Distance = Nothing
            End If
            oImport.Organisation = cbRestaurant.Text
            oImport.OrganisationID = cbRestaurant.EditValue
            oImport.IDPeriode = IDPeriode
            oImport.Debit = 0
            oImport.Credit = 0
            oImport.NombreGlacier = 0
            oImport.MontantGlacierLivreur = 0
            oImport.MontantGlacierOrganisation = 0
            oImport.MontantContrat = 0
            oImport.ExtraKM = 0
            oImport.MontantLivreur = tbMontantLivreur.Text
            oImport.Montant = tbMontantOrganisation.Text
            oImport.Debit = 0
            oImport.Credit = 0
            oImport.NombreGlacier = 0
            oImport.Extra = 0
            oImport.ExtraKMLivreur = 0
            oImport.ExtraLivreur = 0
            oImport.IDSAmount = 0
            oImport.DateImport = dDateLivraison
            oImport.IsMobilus = 0
            oImport.Echange = False
            oImport.IDClientAddresse = If(String.IsNullOrEmpty(tbCustomerAddressID.Text), Nothing, CType(tbCustomerAddressID.Text, Int32?))

            Dim bSucess As Boolean
            Dim oDataClass As New ImportData
            bSucess = oImportData.Update(oImport, oImport)
            Me.Cursor = Cursors.Default
            If bSucess = True Then

                oImportData.DeleteFromBackup(IDPeriode)
                oImportData.CopyImportToBackup()

                IsLoading = False
                tabMain.SelectedTabPage = tabMain.TabPages(1)
                tabMain.TabPages(2).PageVisible = False



            Else
                Me.Cursor = Cursors.Default
                MessageBox.Show("la mise a jour de cette livraison a échouée")
            End If

            grdImport.DataSource = oImportData.SelectAllFromDate(tbDateFrom.Text, tbDateTo.Text)

            GridView1.OptionsView.ColumnAutoWidth = False
            'GridView1.BestFitColumns()
            Dim viewInfo As DevExpress.XtraGrid.Views.Grid.ViewInfo.GridViewInfo = TryCast(GridView1.GetViewInfo(), GridViewInfo)
            If viewInfo.ViewRects.ColumnTotalWidth < viewInfo.ViewRects.ColumnPanelWidth Then
                GridView1.OptionsView.ColumnAutoWidth = True
            End If

            Dim rowHandle As Integer = GridView1.LocateByValue("ImportID", oImport.ImportID)
            If rowHandle <> DevExpress.XtraGrid.GridControl.InvalidRowHandle Then GridView1.FocusedRowHandle = rowHandle


            bImportMiseaJour.Enabled = False


            Me.Cursor = Cursors.Default


            Exit Sub

        Catch ex As Exception
            Me.Cursor = Cursors.Default
            MessageBox.Show(ex.Message, "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try



        Exit Sub



    End Sub

    Private Sub btnGenererFacturation_ItemClick(sender As Object, e As ItemClickEventArgs) Handles btnGenererFacturation.ItemClick

        Dim iTotalCalcul As Integer = 0
        Dim dDateFrom As Date = tbDateFrom.Text
        Dim dDateTo As Date = tbDateTo.Text

        go_Globals.TotalEchange305 = 0
        go_Globals.TotalEchange35 = 0
        go_Globals.iNombreFree30535 = 0
        go_Globals.TotalEchange = 0



        ' verifie que les lignes rouges et vertes sont regles
        For i As Integer = 0 To GridView1.DataRowCount - 1
            If Not IsDBNull(GridView1.GetRowCellValue(i, "Distance")) Then
                If GridView1.GetRowCellValue(i, "Distance") = 0 Or GridView1.GetRowCellValue(i, "Distance") > 400 Then
                    MessageBox.Show("Vous avez des lignes rouges et/ou verte a fixer dans la grille. Veuillez les fixer avant de générer la facturation!")
                    Exit Sub
                End If
            End If

        Next i



        '----------------------------------------------------------------------------------------
        ' Verifie que tous les methodes de calculs ont ete referencer dans le systeme
        '----------------------------------------------------------------------------------------


        If IsNothing(cbPeriodes.EditValue) Then
            MessageBox.Show("Vous devez selectionner une periode")
            Exit Sub
        End If


        iTotalCalcul = oOrganisationData.SelectAllMetodeCalculationOrganisation()

        If iTotalCalcul > 0 Then
            Me.Cursor = Cursors.Default
            Exit Sub
        End If



        '----------------------------------------------------------------------------------------
        ' Facturation  generation
        '----------------------------------------------------------------------------------------

        Dim result As DialogResult = MessageBox.Show("Générer la facturation?", "Facturation", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
        If result = DialogResult.No Then
            Me.Cursor = Cursors.Default
        ElseIf result = DialogResult.Yes Then

            If GenererToutesLesFactures() = False Then
                Exit Sub
            End If

            CalculerFactureTotaux()

        End If



        grdImport.DataSource = oImportData.SelectAllFromDate(dDateFrom, dDateTo)
        GridView1.OptionsView.ColumnAutoWidth = False
        'GridView1.BestFitColumns()
        Dim viewInfo As DevExpress.XtraGrid.Views.Grid.ViewInfo.GridViewInfo = TryCast(GridView1.GetViewInfo(), GridViewInfo)
        If viewInfo.ViewRects.ColumnTotalWidth < viewInfo.ViewRects.ColumnPanelWidth Then
            GridView1.OptionsView.ColumnAutoWidth = True
        End If

        Me.Cursor = Cursors.Default





    End Sub

    Private Sub bGenererPaye_ItemClick(sender As Object, e As ItemClickEventArgs) Handles bGenererPaye.ItemClick, bGenererLaPaye.ItemClick

        Dim iTotalCalcul As Integer = 0
        Dim dDateFrom As Date = tbDateFrom.Text
        Dim dDateTo As Date = tbDateTo.Text

        '----------------------------------------------------------------------------------------
        ' Verifie que tous les methodes de calculs ont ete referencer dans le systeme
        '----------------------------------------------------------------------------------------


        If IsNothing(cbPeriodes.EditValue) Then
            MessageBox.Show("Vous devez selectionner une periode")
            Exit Sub
        End If




        iTotalCalcul = oOrganisationData.SelectAllMetodeCalculationLivreur()

        If iTotalCalcul > 0 Then
            Me.Cursor = Cursors.Default

            Exit Sub
        End If


        '----------------------------------------------------------------------------------------
        ' generation de la paye
        '----------------------------------------------------------------------------------------


        Dim Result = MessageBox.Show("Générer la  paie?", "Paie", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
        If Result = DialogResult.No Then
            Me.Cursor = Cursors.Default
        ElseIf Result = DialogResult.Yes Then

            If GenererTousLesPaye() = False Then
                Me.Cursor = Cursors.Default
                Exit Sub
            End If

            CalculerPayeTotaux()

        End If

        Me.Cursor = Cursors.Default


    End Sub

    Private Sub bImprimerFactures_ItemClick(sender As Object, e As ItemClickEventArgs) Handles bImprimerFactures.ItemClick
        If IsNothing(cbPeriodes.EditValue) Then
            MessageBox.Show("Vous devez selectionner une periode")
            Exit Sub
        End If



        Dim result As DialogResult = MessageBox.Show("Désirez-vous imprimer les factures?", "Imprimer Factures", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
        If result = DialogResult.No Then

        ElseIf result = DialogResult.Yes Then
            Me.Cursor = Cursors.WaitCursor
            If ImprimerFacturation() = False Then

                Me.Cursor = Cursors.Default
                Exit Sub
            End If

        End If


        FillInvoiceNumber()


        Me.Cursor = Cursors.Default
    End Sub

    Private Sub bImprimerCommissionRapport_ItemClick(sender As Object, e As ItemClickEventArgs)

    End Sub

    Private Sub grdImport_DoubleClick(sender As Object, e As EventArgs) Handles grdImport.DoubleClick

        IsGridClick = True

        ClearCommande()
        FillAllRestaurants()
        grdClientsAddresse.DataSource = oClientAddresseData.SelectAll()

        Dim row As System.Data.DataRow = GridView1.GetDataRow(GridView1.FocusedRowHandle)

        SelectedRow = GridView1.FocusedRowHandle
        Dim clsImport As New Import
        clsImport.ImportID = System.Convert.ToInt32(row("ImportID").ToString)
        tbDateLivraison.Text = If(IsNothing(row("DateImport").ToString), Nothing, row("DateImport").ToString)
        clsImport = oImportData.Select_Record(clsImport)

        If Not clsImport Is Nothing Then
            If clsImport.IsMobilus = True Then
                MessageBox.Show("Vous ne pouvez pas editer une livraison de mobilus, Cette fonctionalité est reservé aux restaurants!", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Exit Sub
            End If

            tbImportIDL.Text = clsImport.ImportID
            cbRestaurant.EditValue = clsImport.OrganisationID
            cblivreur.EditValue = clsImport.LivreurID
        End If

        If Not IsDBNull(row("IDClientAddresse")) Then
            Dim clsClientAddresse As New ClientAddresse
            clsClientAddresse.IDClientAddress = System.Convert.ToInt32(row("IDClientAddresse"))
            clsClientAddresse = oClientAddresseData.Select_Record(clsClientAddresse)

            If Not clsClientAddresse Is Nothing Then
                Try



                    tbCustomerAddressID.Text = System.Convert.ToInt32(clsClientAddresse.IDClientAddress)


                    tbNom.Text = If(IsNothing(clsClientAddresse.Nom), Nothing, clsClientAddresse.Nom.ToString.Trim)
                    tbAdresseNo.Text = If(IsNothing(clsClientAddresse.AdresseNo), Nothing, clsClientAddresse.AdresseNo.ToString.Trim)
                    tbAdresse.Text = If(IsNothing(clsClientAddresse.Adresse), Nothing, clsClientAddresse.Adresse.ToString.Trim)
                    If IsNothing(clsClientAddresse.VilleID) Then
                        cbVille.EditValue = Nothing
                    Else
                        cbVille.EditValue = clsClientAddresse.VilleID

                    End If


                    tbCodePostale.Text = If(IsNothing(clsClientAddresse.CodePostale), Nothing, clsClientAddresse.CodePostale.ToUpper.ToString.Trim)
                    tbAppartement.Text = If(IsNothing(clsClientAddresse.Appartement), Nothing, clsClientAddresse.Appartement.ToString.Trim)
                    tbCodeEntree.Text = If(IsNothing(clsClientAddresse.CodeEntree), Nothing, clsClientAddresse.CodeEntree.ToString.Trim)
                    tbTelephone.Text = If(IsNothing(clsClientAddresse.Telephone), Nothing, GetNumbers(clsClientAddresse.Telephone.ToString.Trim))
                    tbMobile.Text = If(IsNothing(clsClientAddresse.Mobile), Nothing, GetNumbers(clsClientAddresse.Mobile.ToString.Trim))

                    bSupprimerAddresse.Enabled = True


                    tbMontantOrganisation.Text = If(IsDBNull(row("Montant")), 0, CType(row("Montant"), Decimal?))

                    tbMontantLivreur.Text = If(IsDBNull(row("MontantLivreur")), 0, CType(row("MontantLivreur"), Decimal?))

                Catch ex As Exception

                    Me.Cursor = System.Windows.Forms.Cursors.Default
                    MessageBox.Show("grille double clic()    " & ex.Message, "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)


                End Try





            End If

            bImportMiseaJour.Enabled = True
            bAddToImport.Enabled = False

        End If

        tabMain.TabPages(2).PageVisible = True
        tabMain.SelectedTabPage = tabMain.TabPages(2)
        IsGridClick = False

    End Sub




    Private Sub optFiltreGrille_SelectedIndexChanged(sender As Object, e As EventArgs) Handles optFiltreGrille.SelectedIndexChanged
        If IsGridClick = True Then Exit Sub
        Dim sFiltre As String = ""
        Dim blnValue As Boolean = True

        GridView1.ClearColumnsFilter()

        Select Case optFiltreGrille.SelectedIndex
            Case 0

                GridView1.Columns("IsMobilus").FilterInfo = New ColumnFilterInfo("")
                GridView1.Columns("Distance").FilterInfo = New ColumnFilterInfo(sFiltre)
                sFiltre = "[IsMobilus] LIKE '" & "" & "%'"
                GridView1.Columns("IsMobilus").FilterInfo = New ColumnFilterInfo(sFiltre)
                GridView1.FocusedRowHandle = 1
            Case 1
                GridView1.Columns("IsMobilus").FilterInfo = New ColumnFilterInfo("")
                GridView1.Columns("Distance").FilterInfo = New ColumnFilterInfo(sFiltre)

                sFiltre = "[IsMobilus] LIKE '" & blnValue & "%'"
                GridView1.Columns("IsMobilus").FilterInfo = New ColumnFilterInfo(sFiltre)
                GridView1.FocusedRowHandle = 1
            Case 2
                GridView1.Columns("IsMobilus").FilterInfo = New ColumnFilterInfo("")
                GridView1.Columns("Distance").FilterInfo = New ColumnFilterInfo(sFiltre)
                blnValue = False
                sFiltre = "[IsMobilus] LIKE '" & blnValue & "%'"
                GridView1.Columns("IsMobilus").FilterInfo = New ColumnFilterInfo(sFiltre)
                GridView1.FocusedRowHandle = 1
            Case 3
                GridView1.Columns("IsMobilus").FilterInfo = New ColumnFilterInfo("")
                GridView1.Columns("Distance").FilterInfo = New ColumnFilterInfo(sFiltre)
                sFiltre = "[Distance] > 400 OR [Distance] = 0"
                GridView1.Columns("Distance").FilterInfo = New ColumnFilterInfo(sFiltre)
                GridView1.FocusedRowHandle = 1
        End Select




    End Sub

    Private Sub bRemoveFilter_Click(sender As Object, e As EventArgs) Handles bRemoveFilter.Click
        tbDistanceR.Text = 0
        tbCustomerAddressID.Text = 0
        tbNom.Text = Nothing
        tbAdresseNo.Text = Nothing
        tbAdresse.Text = Nothing
        cbVille.EditValue = Nothing
        tbCodePostale.Text = Nothing
        tbAppartement.Text = Nothing
        tbCodeEntree.Text = Nothing
        tbTelephone.Text = Nothing
        tbMobile.Text = Nothing
        tbMobile.Text = Nothing
        bSupprimerAddresse.Enabled = False
        GridView3.ClearColumnsFilter()
        grdClientsAddresse.DataSource = oClientAddresseData.SelectAll()

        IsLoading = False
    End Sub



    Private Sub bImprimerCommisssions_ItemClick(sender As Object, e As ItemClickEventArgs) Handles bImprimerCommisssions.ItemClick, bImprimerCommissionRapport.ItemClick
        If IsNothing(cbPeriodes.EditValue) Then
            MessageBox.Show("Vous devez selectionner une periode")
            Exit Sub
        End If


        Me.Cursor = Cursors.Default
        Dim result = MessageBox.Show("Désirez-vous imprimer les Commisssions?", "Imprimer Commisssions", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
        If result = DialogResult.No Then

        ElseIf result = DialogResult.Yes Then
            Me.Cursor = Cursors.WaitCursor
            If ImprimerCommissions() = False Then
                Me.Cursor = Cursors.Default
                Exit Sub
            End If

        End If

        FillPaieNumber()

        Me.Cursor = Cursors.Default
    End Sub

    Private Sub bGenererLesCommissionVendeurs_ItemClick(sender As Object, e As ItemClickEventArgs) Handles bGenererLesCommission.ItemClick

        Dim iTotalCalcul As Integer = 0
        Dim dDateFrom As Date = tbDateFrom.Text
        Dim dDateTo As Date = tbDateTo.Text

        '----------------------------------------------------------------------------------------
        ' Verifie que tous les methodes de calculs ont ete referencer dans le systeme
        '----------------------------------------------------------------------------------------


        If IsNothing(cbPeriodes.EditValue) Then
            MessageBox.Show("Vous devez selectionner une periode")
            Exit Sub
        End If


        '----------------------------------------------------------------------------------------
        ' generation des commissions vendeurs
        '----------------------------------------------------------------------------------------
        Dim Result = MessageBox.Show("Générer les commissions?", "Commissions", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
        If Result = DialogResult.No Then
            Me.Cursor = Cursors.Default
            Exit Sub
        ElseIf Result = DialogResult.Yes Then

            If GenererCommissionVendeursRestaurants() = False Then
                Me.Cursor = Cursors.Default
                Exit Sub
            End If

            GenererCommissionVendeurslivreurs()

            'CalculerCommissionsTotaux()

        End If







        Me.Cursor = Cursors.Default


    End Sub



    Private Sub bLivraisonParRestaurant_ItemClick(sender As Object, e As ItemClickEventArgs) Handles bLivraisonParRestaurant.ItemClick, bLivraisonsParRestaurant.ItemClick
        If IsNothing(cbPeriodes.EditValue) Then
            MessageBox.Show("Vous devez selectionner une periode")
            Exit Sub
        End If



        Dim result As DialogResult = MessageBox.Show("Désirez-vous imprimer les rapports de livraison des restaurants?", "Imprimer les rapports de livraison des restaurants", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
        If result = DialogResult.No Then

        ElseIf result = DialogResult.Yes Then
            Me.Cursor = Cursors.WaitCursor
            If ImprimerRapportDesLivraisonParRestaurant() = False Then

                Me.Cursor = Cursors.Default
                Exit Sub
            End If

        End If



        Me.Cursor = Cursors.Default
    End Sub

    Private Sub bResetLivraisonsRestaurant_Click(sender As Object, e As EventArgs) Handles bResetLivraisonsRestaurant.Click
        cbLivraisonRestaurants.EditValue = Nothing
        cbLivraisonRestaurants.Text = String.Empty
        cbLivraisonRestaurants.SelectedText = Nothing
    End Sub


    Private Sub tbCodeEntree_EditValueChanged(sender As Object, e As EventArgs) Handles tbCodeEntree.EditValueChanged
        If IsGridClick = True Then Exit Sub
        Dim sFiltre As String = ""

        sFiltre = "[CodeEntree] LIKE '" & tbCodeEntree.Text & "%'"
        GridView3.Columns("CodeEntree").FilterInfo = New ColumnFilterInfo(sFiltre)
        GridView3.FocusedRowHandle = 1
    End Sub

    Private Sub SimpleButton3_Click(sender As Object, e As EventArgs)

    End Sub

    Private Sub optTypePeriode_SelectedIndexChanged(sender As Object, e As EventArgs) Handles optTypePeriode.SelectedIndexChanged

        Select Case optTypePeriode.SelectedIndex

            Case 0
                go_Globals.IDTypePeriode = 0
                LayoutDroite.Enabled = True
                LayoutEtat.Enabled = True
                LayoutEtatLivraisonRestaurant.Enabled = True
                LayoutCommissions.Enabled = False
                LayoutFactures.Enabled = False
                LayoutPayes.Enabled = False


            Case 1

                LayoutDroite.Enabled = True
                LayoutEtat.Enabled = False
                LayoutEtatLivraisonRestaurant.Enabled = False
                LayoutCommissions.Enabled = True
                LayoutFactures.Enabled = True
                LayoutPayes.Enabled = True


                go_Globals.IDTypePeriode = 1

                bCommisssion.Enabled = False
                LayoutDroite.Enabled = False
                LayoutBas.Enabled = False

                bCommisssion.Enabled = False
                bEtats.Enabled = True
                bGenererLesCommission.Enabled = False
                bImprimerCommisssions.Enabled = False
                bAchats.Enabled = False
                bVentes.Enabled = False
                bFactures.Enabled = False
                bPayes.Enabled = False
                bGenererLaPaye.Enabled = False
                bConsolidation.Enabled = False
                bImprimerFactures.Enabled = False
                btnGenererFacturation.Enabled = False
                bImportMobilus.Enabled = False
                bEnvoyerPaye.Enabled = False
                bEnvoyerFacture.Enabled = False
                bSelectfile.Enabled = False
                bImprimerCommissionRapport.Enabled = False

                bLivraisonParLivreur.Enabled = False
                bAjouterUneCommande.Enabled = False
                bImprimerCommission.Enabled = False

        End Select

        FillAllPeriodes()
        bResetPeriode_Click(sender, e)

    End Sub

    Private Sub bPrintFacture_Click(sender As Object, e As EventArgs) Handles bPrintFacture.Click


        GemBox.Pdf.ComponentInfo.SetLicense("AD0G-Y35C-Z376-E74D")

        Dim strSelectedValue As String = ""
        Dim iSelectedValue As Integer = 0
        Dim dDateFrom As Date = tbDateFrom.Text
        Dim dDateTo As Date = tbDateTo.Text
        Dim oPeriode As New Periodes
        Dim frenchCultureInfo = New Globalization.CultureInfo("fr-CA")
        Dim dtRapportLivraison As New DataTable
        Dim drRapportLivraison As DataRow
        Dim iIDRapport As Integer = 0

        If IsNothing(cbPeriodes.EditValue) Then
            MessageBox.Show("Vous devez selectionner une periode")
            Exit Sub
        End If


        IDPeriode = cbPeriodes.EditValue

        Me.Cursor = Cursors.Default
        Dim result As DialogResult = MessageBox.Show("Imprimer les factures?", "Imprimer", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
        If result = DialogResult.No Then
            Exit Sub
        ElseIf result = DialogResult.Yes Then

            Me.Cursor = Cursors.WaitCursor


            For Each item As DevExpress.XtraEditors.Controls.CheckedListBoxItem In cbFactures.Properties.Items
                If item.CheckState = CheckState.Checked Then
                    strSelectedValue = strSelectedValue & item.Value & ","
                    iSelectedValue = iSelectedValue + 1
                End If
            Next

            If strSelectedValue.ToString.Trim <> "" Then
                strSelectedValue = strSelectedValue.Remove(strSelectedValue.Length - 1)
            End If


            dtRapportLivraison = oRapportFacturePayeData.SelectAllDataByPeriodesF(IDPeriode, strSelectedValue)
            If dtRapportLivraison.Rows.Count > 0 Then

                Me.Cursor = Cursors.WaitCursor

                ProgressBarControl1.Properties.Step = 1
                ProgressBarControl1.Properties.PercentView = True
                ProgressBarControl1.Properties.Maximum = dtRapportLivraison.Rows.Count
                ProgressBarControl1.Properties.Minimum = 0


                For Each drRapportLivraison In dtRapportLivraison.Rows
                    Using document As PdfDocument = PdfDocument.Load(drRapportLivraison("CheminRapport").ToString.Trim)
                        ' Print PDF document to default printer
                        Dim printerName As String = Nothing
                        document.Print(printerName)
                    End Using
                    ProgressBarControl1.PerformStep()
                    ProgressBarControl1.Update()

                Next

            End If


            ProgressBarControl1.EditValue = 0
            Me.Cursor = Cursors.Default
            MessageBox.Show("Impression des factures complétés avec succès?")


        Else
            Me.Cursor = Cursors.Default
            MessageBox.Show("Aucunes facture n'a été trouvé pour cette période!", "Factures", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1)
            Exit Sub
        End If





    End Sub

    Private Sub bPrintPaye_Click(sender As Object, e As EventArgs) Handles bPrintPaye.Click


        GemBox.Pdf.ComponentInfo.SetLicense("AD0G-Y35C-Z376-E74D")

        Dim strSelectedValue As String = ""
        Dim iSelectedValue As Integer = 0
        Dim dDateFrom As Date = tbDateFrom.Text
        Dim dDateTo As Date = tbDateTo.Text
        Dim oPeriode As New Periodes
        Dim frenchCultureInfo = New Globalization.CultureInfo("fr-CA")
        Dim dtRapportLivraison As New DataTable
        Dim drRapportLivraison As DataRow
        Dim iIDRapport As Integer = 0


        If IsNothing(cbPeriodes.EditValue) Then
            MessageBox.Show("Vous devez selectionner une periode")
            Exit Sub
        End If

        IDPeriode = cbPeriodes.EditValue

        Me.Cursor = Cursors.Default
        Dim result As DialogResult = MessageBox.Show("Imprimer les Paies?", "Imprimer", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
        If result = DialogResult.No Then
            Exit Sub
        ElseIf result = DialogResult.Yes Then

            Me.Cursor = Cursors.WaitCursor


            For Each item As DevExpress.XtraEditors.Controls.CheckedListBoxItem In cbPayes.Properties.Items
                If item.CheckState = CheckState.Checked Then
                    strSelectedValue = strSelectedValue & item.Value & ","
                    iSelectedValue = iSelectedValue + 1
                End If
            Next

            If strSelectedValue.ToString.Trim <> "" Then
                strSelectedValue = strSelectedValue.Remove(strSelectedValue.Length - 1)
            End If


            dtRapportLivraison = oRapportFacturePayeData.SelectAllDataByPeriodesP(IDPeriode, strSelectedValue)
            If dtRapportLivraison.Rows.Count > 0 Then

                Me.Cursor = Cursors.WaitCursor

                ProgressBarControl1.Properties.Step = 1
                ProgressBarControl1.Properties.PercentView = True
                ProgressBarControl1.Properties.Maximum = dtRapportLivraison.Rows.Count
                ProgressBarControl1.Properties.Minimum = 0


                For Each drRapportLivraison In dtRapportLivraison.Rows

                    Using document As PdfDocument = PdfDocument.Load(drRapportLivraison("CheminRapport").ToString.Trim)
                        ' Print PDF document to default printer
                        Dim printerName As String = Nothing
                        document.Print(printerName)
                    End Using


                    ProgressBarControl1.PerformStep()
                    ProgressBarControl1.Update()

                Next

            End If


            ProgressBarControl1.EditValue = 0
            Me.Cursor = Cursors.Default
            MessageBox.Show("Impression de la paie complétés avec succès?")


        Else
            Me.Cursor = Cursors.Default
            MessageBox.Show("Aucunes paie n'a été trouvé pour cette période!", "Paies", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1)
            Exit Sub
        End If



    End Sub

    Private Sub bPrintEtat_Click(sender As Object, e As EventArgs) Handles bPrintEtat.Click
        GemBox.Pdf.ComponentInfo.SetLicense("AD0G-Y35C-Z376-E74D")

        Dim dDateFrom As Date = tbDateFrom.Text
        Dim dDateTo As Date = tbDateTo.Text
        Dim frenchCultureInfo = New Globalization.CultureInfo("fr-CA")
        Dim dtRapportFacturePaye As New DataTable
        Dim drRapportFacturePaye As DataRow
        Dim oMail As New Mail
        Dim iIDRapport As Integer = 0
        Dim strSelectedValue As String = ""
        Dim iSelectedValue As Integer = 0


        If IsNothing(cbPeriodes.EditValue) Then
            MessageBox.Show("Vous devez selectionner une periode d'etat")
            Exit Sub
        End If


        IDPeriode = cbPeriodes.EditValue



        Me.Cursor = Cursors.Default
        Dim result As DialogResult = MessageBox.Show("Imprimer les Etats de comptes?", "Imprimer", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
        If result = DialogResult.No Then
            Exit Sub
        ElseIf result = DialogResult.Yes Then


            Try
                Me.Cursor = Cursors.WaitCursor


                For Each item As DevExpress.XtraEditors.Controls.CheckedListBoxItem In cbEtatsDeComptes.Properties.Items
                    If item.CheckState = CheckState.Checked Then
                        strSelectedValue = strSelectedValue & item.Value & ","
                        iSelectedValue = iSelectedValue + 1
                    End If
                Next

                If strSelectedValue.ToString.Trim <> "" Then
                    strSelectedValue = strSelectedValue.Remove(strSelectedValue.Length - 1)
                End If


                dtRapportFacturePaye = oEtatsData.SelectAllDataByPeriodes(IDPeriode, strSelectedValue)
                If dtRapportFacturePaye.Rows.Count > 0 Then

                    Me.Cursor = Cursors.WaitCursor

                    ProgressBarControl1.Properties.Step = 1
                    ProgressBarControl1.Properties.PercentView = True
                    ProgressBarControl1.Properties.Maximum = dtRapportFacturePaye.Rows.Count
                    ProgressBarControl1.Properties.Minimum = 0


                    For Each drRapportFacturePaye In dtRapportFacturePaye.Rows


                        Using document As PdfDocument = PdfDocument.Load(drRapportFacturePaye("CheminRapport").ToString.Trim)
                            ' Print PDF document to default printer
                            Dim printerName As String = Nothing
                            document.Print(printerName)
                        End Using




                        ProgressBarControl1.PerformStep()
                        ProgressBarControl1.Update()

                    Next

                    ProgressBarControl1.EditValue = 0
                    Me.Cursor = Cursors.Default
                    MessageBox.Show("Impression des etat de comptes complétés avec succès?")


                Else
                    Me.Cursor = Cursors.Default
                    MessageBox.Show("Aucuns etat de comptes n'a été trouvé pour cette période!", "Etat de comptes", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1)
                    Exit Sub
                End If
            Catch ex As Exception
                Me.Cursor = Cursors.Default
                MessageBox.Show(ex.Message, "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End Try


        End If





    End Sub

    Private Sub bPrintLivraisonR_Click(sender As Object, e As EventArgs) Handles bPrintLivraisonR.Click


        GemBox.Pdf.ComponentInfo.SetLicense("AD0G-Y35C-Z376-E74D")

        Dim strSelectedValue As String = ""
        Dim iSelectedValue As Integer = 0
        Dim dDateFrom As Date = tbDateFrom.Text
        Dim dDateTo As Date = tbDateTo.Text
        Dim oPeriode As New Periodes
        Dim frenchCultureInfo = New Globalization.CultureInfo("fr-CA")
        Dim dtRapportLivraison As New DataTable
        Dim drRapportLivraison As DataRow
        Dim iIDRapport As Integer = 0

        If IsNothing(cbPeriodes.EditValue) Then
            MessageBox.Show("Vous devez selectionner une periode")
            Exit Sub
        End If


        IDPeriode = cbPeriodes.EditValue

        Me.Cursor = Cursors.Default
        Dim result As DialogResult = MessageBox.Show("Imprimer des rapports de livraison restaurant?", "Imprimer", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
        If result = DialogResult.No Then
            Exit Sub
        ElseIf result = DialogResult.Yes Then

            Me.Cursor = Cursors.WaitCursor


            For Each item As DevExpress.XtraEditors.Controls.CheckedListBoxItem In cbLivraisonRestaurants.Properties.Items
                If item.CheckState = CheckState.Checked Then
                    strSelectedValue = strSelectedValue & item.Value & ","
                    iSelectedValue = iSelectedValue + 1
                End If
            Next

            If strSelectedValue.ToString.Trim <> "" Then
                strSelectedValue = strSelectedValue.Remove(strSelectedValue.Length - 1)
            End If


            dtRapportLivraison = oRapportLivraisonData.SelectAllDataByPeriodesF(IDPeriode, strSelectedValue)
            If dtRapportLivraison.Rows.Count > 0 Then

                Me.Cursor = Cursors.WaitCursor

                ProgressBarControl1.Properties.Step = 1
                ProgressBarControl1.Properties.PercentView = True
                ProgressBarControl1.Properties.Maximum = dtRapportLivraison.Rows.Count
                ProgressBarControl1.Properties.Minimum = 0


                For Each drRapportLivraison In dtRapportLivraison.Rows
                    Using document As PdfDocument = PdfDocument.Load(drRapportLivraison("CheminRapport").ToString.Trim)
                        ' Print PDF document to default printer
                        Dim printerName As String = Nothing
                        document.Print(printerName)
                    End Using
                    ProgressBarControl1.PerformStep()
                    ProgressBarControl1.Update()

                Next

            End If


            ProgressBarControl1.EditValue = 0
            Me.Cursor = Cursors.Default
            MessageBox.Show("Impression des rapports de livraison complétés avec succès?")


        Else
            Me.Cursor = Cursors.Default
            MessageBox.Show("Aucune rapport de livraison n'a été trouvé pour cette période!", "Rapport de livraison", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1)
            Exit Sub
        End If



    End Sub



    Private Sub bEnvoyerLivraisonsLivreurs_Click(sender As Object, e As EventArgs)


        Dim dDateFrom As Date = tbDateFrom.Text
        Dim dDateTo As Date = tbDateTo.Text
        Dim oPeriode As New Periodes
        Dim dtPeriode As New DataTable
        Dim frenchCultureInfo = New Globalization.CultureInfo("fr-CA")
        Dim oRapportFacturePaye As New RapportFacturePaye
        Dim dtRapportLivraison As New DataTable
        Dim drRapportLivraison As DataRow
        Dim oMail As New Mail
        Dim iIDRapport As Integer = 0

        Dim strSelectedValue As String = ""
        Dim iSelectedValue As Integer = 0

        If IsNothing(cbPeriodes.EditValue) Then
            MessageBox.Show("Vous devez selectionner une periode")
            Exit Sub
        End If



        If dDateFrom.Month <> dDateTo.Month Then
            oPeriode.Periode = dDateFrom.Day & " " & GetMonthName(dDateFrom, frenchCultureInfo) & "-" & dDateTo.Day & " " & GetMonthName(dDateTo, frenchCultureInfo) & " " & dDateTo.Year.ToString
        Else
            oPeriode.Periode = dDateFrom.Day & "-" & dDateTo.Day & " " & GetMonthName(dDateTo, frenchCultureInfo) & " " & dDateTo.Year.ToString
        End If

        IDPeriode = cbPeriodes.EditValue

        Me.Cursor = Cursors.Default
        Dim result As DialogResult = MessageBox.Show("Envoyer le rapport de livraison au livreurs?", "Rapport de livraison", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
        If result = DialogResult.No Then
            Exit Sub
        ElseIf result = DialogResult.Yes Then

            Try

                Me.Cursor = Cursors.WaitCursor

                For Each item As DevExpress.XtraEditors.Controls.CheckedListBoxItem In cbPayes.Properties.Items
                    If item.CheckState = CheckState.Checked Then
                        strSelectedValue = strSelectedValue & item.Value & ","
                        iSelectedValue = iSelectedValue + 1
                    End If
                Next

                If strSelectedValue.ToString.Trim <> "" Then
                    strSelectedValue = strSelectedValue.Remove(strSelectedValue.Length - 1)
                End If



                dtRapportLivraison = oRapportLivraisonData.SelectAllDataByPeriodesP(IDPeriode, strSelectedValue)
                If dtRapportLivraison.Rows.Count > 0 Then
                    Me.Cursor = Cursors.WaitCursor
                    ProgressBarControl1.Properties.Step = 1
                    ProgressBarControl1.Properties.PercentView = True
                    ProgressBarControl1.Properties.Maximum = dtRapportLivraison.Rows.Count
                    ProgressBarControl1.Properties.Minimum = 0


                    For Each drRapportLivraison In dtRapportLivraison.Rows


                        Dim olivreur As New livreurs
                        Dim olivreur2 As New livreurs
                        olivreur.LivreurID = drRapportLivraison("LivreurID")
                        olivreur2 = oLivreurData.Select_Record(olivreur)

                        If Not IsNothing(olivreur2) Then

                            Dim sEmail As String = If(IsNothing(olivreur2.Email), Nothing, olivreur2.Email.ToString.Trim)
                            Dim sSubject As String = olivreur2.Livreur & ",  Votre rapport de livraison pour la période " & oPeriode.Periode
                            Dim sMessage As String = olivreur2.Livreur & ", Merci pour de faire partie de notre équipe"
                            Dim sAttachment As String = If(IsNothing(drRapportLivraison("CheminRapport")), Nothing, drRapportLivraison("CheminRapport").ToString.Trim)

                            If Not IsNothing(sEmail) Then

                                Dim GGmail As New GGSMTP_GMAIL("idslivraisonexpressrapport@gmail.com", "2015livraison", )

                                Dim ToAddressies As String() = {sEmail}
                                Dim attachs() As String = {sAttachment}
                                Dim subject As String = sSubject
                                Dim body As String = sMessage


                                result = GGmail.SendMail(ToAddressies, subject, body, attachs)
                                If result Then

                                Else
                                    MsgBox(GGmail.ErrorText, MsgBoxStyle.Critical)
                                End If
                            End If
                        End If


                        ProgressBarControl1.PerformStep()
                        ProgressBarControl1.Update()
                    Next

                    ProgressBarControl1.EditValue = 0
                    Me.Cursor = Cursors.Default
                    MessageBox.Show("Envoi des rapports de livraison complété avec succès?")


                Else
                    Me.Cursor = Cursors.Default
                    MessageBox.Show("Aucun rapport de livraisons n'a été trouvé pour cette période!", "Rapports de livraison", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1)
                    Exit Sub
                End If

            Catch ex As Exception
                Me.Cursor = Cursors.Default
                MessageBox.Show(ex.Message, "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End Try
        End If


    End Sub

    Private Sub bEnvoyerLivraisonsRestaurants_Click(sender As Object, e As EventArgs) Handles bEnvoyerLivraisonsRestaurants.Click


        Dim dDateFrom As Date = tbDateFrom.Text
        Dim dDateTo As Date = tbDateTo.Text
        Dim oPeriode As New Periodes
        Dim frenchCultureInfo = New Globalization.CultureInfo("fr-CA")
        Dim dtRapportLivraison As New DataTable
        Dim drRapportLivraison As DataRow
        Dim oMail As New Mail
        Dim iIDRapport As Integer = 0
        Dim strSelectedValue As String = ""
        Dim iSelectedValue As Integer = 0

        Dim strEmails As String = ""

        If IsNothing(cbPeriodes.EditValue) Then
            MessageBox.Show("Vous devez selectionner une periode")
            Exit Sub
        End If

        go_Globals.TotalEchange305 = 0
        go_Globals.TotalEchange35 = 0

        If dDateFrom.Month <> dDateTo.Month Then
            oPeriode.Periode = dDateFrom.Day & " " & GetMonthName(dDateFrom, frenchCultureInfo) & "-" & dDateTo.Day & " " & GetMonthName(dDateTo, frenchCultureInfo) & " " & dDateTo.Year.ToString
        Else
            oPeriode.Periode = dDateFrom.Day & "-" & dDateTo.Day & " " & GetMonthName(dDateTo, frenchCultureInfo) & " " & dDateTo.Year.ToString
        End If



        Me.Cursor = Cursors.Default
        Dim result As DialogResult = MessageBox.Show("Envoyer rapports de livraisons aux restaurants?", "Rapports de livraisons", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
        If result = DialogResult.No Then
            Exit Sub
        ElseIf result = DialogResult.Yes Then


            Try
                Me.Cursor = Cursors.WaitCursor


                For Each item As DevExpress.XtraEditors.Controls.CheckedListBoxItem In cbLivraisonRestaurants.Properties.Items
                    If item.CheckState = CheckState.Checked Then
                        strSelectedValue = strSelectedValue & item.Value & ","
                        iSelectedValue = iSelectedValue + 1
                    End If
                Next

                If strSelectedValue.ToString.Trim <> "" Then
                    strSelectedValue = strSelectedValue.Remove(strSelectedValue.Length - 1)
                End If


                dtRapportLivraison = oRapportLivraisonData.SelectAllDataByPeriodesF(IDPeriode, strSelectedValue)
                If dtRapportLivraison.Rows.Count > 0 Then

                    Me.Cursor = Cursors.WaitCursor

                    ProgressBarControl1.Properties.Step = 1
                    ProgressBarControl1.Properties.PercentView = True
                    ProgressBarControl1.Properties.Maximum = dtRapportLivraison.Rows.Count
                    ProgressBarControl1.Properties.Minimum = 0


                    For Each drRapportLivraison In dtRapportLivraison.Rows

                        Dim oOrganisation As New Organisations
                        Dim oOrganisation2 As New Organisations
                        oOrganisation.OrganisationID = drRapportLivraison("IDOrganisation")
                        oOrganisation2 = oOrganisationData.Select_Record(oOrganisation)

                        If Not IsNothing(oOrganisation2) Then

                            strEmails = ""

                            Dim sEmail As String = If(IsNothing(oOrganisation2.Email), "", oOrganisation2.Email.ToString.Trim)
                            Dim sEmail1 As String = If(IsNothing(oOrganisation2.Email1), "", oOrganisation2.Email1.ToString.Trim)
                            Dim sEmail2 As String = If(IsNothing(oOrganisation2.Email2), "", oOrganisation2.Email2.ToString.Trim)
                            Dim sEmail3 As String = If(IsNothing(oOrganisation2.Email3), "", oOrganisation2.Email3.ToString.Trim)
                            Dim sSubject As String = "IDS Livraison Express:  Votre rapport de livraison pour la période " & oPeriode.Periode
                            Dim sMessage As String = oOrganisation2.Organisation & ", Nous vous remercions de choisir IDS Livraison Express "
                            Dim sAttachment As String = If(IsNothing(drRapportLivraison("CheminRapport")), Nothing, drRapportLivraison("CheminRapport").ToString.Trim)

                            If sEmail.ToString.Trim <> "" Then
                                strEmails = strEmails & sEmail & ","
                            End If

                            If sEmail1.ToString.Trim <> "" Then
                                strEmails = strEmails & sEmail1 & ","
                            End If

                            If sEmail2.ToString.Trim <> "" Then
                                strEmails = strEmails & sEmail2 & ","
                            End If

                            If sEmail3.ToString.Trim <> "" Then
                                strEmails = strEmails & sEmail3 & ","
                            End If




                            If Not IsNothing(strEmails) Then

                                strEmails = strEmails.Remove(strEmails.Length - 1)

                                Dim GGmail As New GGSMTP_GMAIL("idslivraisonexpressrapport@gmail.com", "2015livraison",  )

                                Dim ToAddressies As String() = {strEmails}
                                Dim attachs() As String = {sAttachment}
                                Dim subject As String = sSubject
                                Dim body As String = sMessage


                                result = GGmail.SendMail(ToAddressies, subject, body, attachs)
                                If result Then

                                Else
                                    MsgBox(GGmail.ErrorText, MsgBoxStyle.Critical)
                                End If

                            End If


                        End If



                        ProgressBarControl1.PerformStep()
                        ProgressBarControl1.Update()

                    Next

                    ProgressBarControl1.EditValue = 0
                    Me.Cursor = Cursors.Default
                    MessageBox.Show("Envoi des factures complété avec succès?")


                Else
                    Me.Cursor = Cursors.Default
                    MessageBox.Show("Aucunes facture n'a été trouvé pour cette période!", "Factures non générées", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1)
                    Exit Sub
                End If
            Catch ex As Exception
                Me.Cursor = Cursors.Default
                MessageBox.Show(ex.Message, "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End Try


        End If
    End Sub

    Private Sub cbRestaurant_EditValueChanged(sender As Object, e As EventArgs) Handles cbRestaurant.EditValueChanged
        If IsLoading = True Then Exit Sub

        Dim dtOrganisation As New DataTable
        Dim oOrganisation As New Organisations


        If Not IsNothing(cbRestaurant.EditValue) Then

            oOrganisation.OrganisationID = cbRestaurant.EditValue
            oOrganisation = oOrganisationData.Select_Record(oOrganisation)

            If Not oOrganisation Is Nothing Then
                Try

                    tbMontantOrganisation.Text = If(IsNothing(oOrganisation.MontantContrat), 0, oOrganisation.MontantContrat)
                    tbMontantLivreur.Text = If(IsNothing(oOrganisation.PayeLivreur), 0, oOrganisation.PayeLivreur)

                Catch
                End Try
            End If





        End If


    End Sub

    Private Sub tbDateLivraison_EditValueChanged(sender As Object, e As EventArgs) Handles tbDateLivraison.EditValueChanged

    End Sub

    Private Sub bEnvoyerCommissionVendeur_Click(sender As Object, e As EventArgs) Handles bEnvoyerCommissionVendeur.Click

        Dim dDateFrom As Date = tbDateFrom.Text
        Dim dDateTo As Date = tbDateTo.Text
        Dim oPeriode As New Periodes
        Dim frenchCultureInfo = New Globalization.CultureInfo("fr-CA")
        Dim oRapportFacturePaye As New RapportFacturePaye
        Dim dtRapportCommission As New DataTable
        Dim drRapportCommission As DataRow
        Dim oMail As New Mail
        Dim iIDRapport As Integer = 0
        Dim strSelectedValue As String = ""
        Dim iSelectedValue As Integer = 0

        If IsNothing(cbPeriodes.EditValue) Then
            MessageBox.Show("Vous devez selectionner une periode")
            Exit Sub
        End If


        If dDateFrom.Month <> dDateTo.Month Then
            oPeriode.Periode = dDateFrom.Day & " " & GetMonthName(dDateFrom, frenchCultureInfo) & "-" & dDateTo.Day & " " & GetMonthName(dDateTo, frenchCultureInfo) & " " & dDateTo.Year.ToString
        Else
            oPeriode.Periode = dDateFrom.Day & "-" & dDateTo.Day & " " & GetMonthName(dDateTo, frenchCultureInfo) & " " & dDateTo.Year.ToString
        End If

        IDPeriode = cbPeriodes.EditValue

        Me.Cursor = Cursors.Default
        Dim result As DialogResult = MessageBox.Show("Envoyer les commissions aux vendeurs?", "Commissions", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
        If result = DialogResult.No Then
            Exit Sub
        ElseIf result = DialogResult.Yes Then

            Try

                Me.Cursor = Cursors.WaitCursor

                For Each item As DevExpress.XtraEditors.Controls.CheckedListBoxItem In cbCommissions.Properties.Items
                    If item.CheckState = CheckState.Checked Then
                        strSelectedValue = strSelectedValue & item.Value & ","
                        iSelectedValue = iSelectedValue + 1
                    End If
                Next

                If strSelectedValue.ToString.Trim <> "" Then
                    strSelectedValue = strSelectedValue.Remove(strSelectedValue.Length - 1)
                End If



                dtRapportCommission = oRapportCommissionData.SelectAllDataByPeriodesP(IDPeriode, strSelectedValue)
                If dtRapportCommission.Rows.Count > 0 Then
                    Me.Cursor = Cursors.WaitCursor
                    ProgressBarControl1.Properties.Step = 1
                    ProgressBarControl1.Properties.PercentView = True
                    ProgressBarControl1.Properties.Maximum = dtRapportCommission.Rows.Count
                    ProgressBarControl1.Properties.Minimum = 0

                    For Each drRapportCommission In dtRapportCommission.Rows

                        Dim olivreur As New livreurs
                        Dim olivreurSelected As New livreurs
                        olivreur.LivreurID = drRapportCommission("IDLivreur")
                        olivreurSelected = oLivreurData.Select_Record(olivreur)

                        If Not IsNothing(olivreurSelected) Then

                            Dim sEmail As String = If(IsNothing(olivreurSelected.Email), Nothing, olivreurSelected.Email.ToString.Trim)
                            Dim sSubject As String = olivreurSelected.Livreur & ",  Votre fiche de commissions pour la période " & oPeriode.Periode
                            Dim sMessage As String = olivreurSelected.Livreur & ", Merci pour de faire partie de notre équipe"
                            Dim sAttachment As String = If(IsNothing(drRapportCommission("CheminRapport")), Nothing, drRapportCommission("CheminRapport").ToString.Trim)

                            If Not IsNothing(sEmail) Then

                                Dim GGmail As New GGSMTP_GMAIL("idslivraisonexpressrapport@gmail.com", "2015livraison", )
                                Dim ToAddressies As String() = {sEmail}
                                Dim attachs() As String = {sAttachment}
                                Dim subject As String = sSubject
                                Dim body As String = sMessage

                                result = GGmail.SendMail(ToAddressies, subject, body, attachs)

                                If result Then

                                Else
                                    MsgBox(GGmail.ErrorText, MsgBoxStyle.Critical)
                                End If

                            End If

                        End If

                        ProgressBarControl1.PerformStep()
                        ProgressBarControl1.Update()
                    Next

                    ProgressBarControl1.EditValue = 0
                    Me.Cursor = Cursors.Default
                    MessageBox.Show("Envoi des paies complété avec succès?")


                Else
                    Me.Cursor = Cursors.Default
                    MessageBox.Show("Aucune paie n'a été trouvé pour cette période!", "Paie", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1)
                    Exit Sub
                End If

            Catch ex As Exception
                Me.Cursor = Cursors.Default
                MessageBox.Show(ex.Message, "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End Try

        End If
    End Sub

    Private Sub bPrintCommissions_Click(sender As Object, e As EventArgs) Handles bPrintCommissions.Click


        GemBox.Pdf.ComponentInfo.SetLicense("AD0G-Y35C-Z376-E74D")

        Dim strSelectedValue As String = ""
        Dim iSelectedValue As Integer = 0
        Dim dDateFrom As Date = tbDateFrom.Text
        Dim dDateTo As Date = tbDateTo.Text
        Dim oPeriode As New Periodes
        Dim frenchCultureInfo = New Globalization.CultureInfo("fr-CA")
        Dim dtRapportCommission As New DataTable
        Dim drRapportCommission As DataRow
        Dim iIDRapport As Integer = 0


        If IsNothing(cbPeriodes.EditValue) Then
            MessageBox.Show("Vous devez selectionner une periode")
            Exit Sub
        End If

        IDPeriode = cbPeriodes.EditValue

        Me.Cursor = Cursors.Default
        Dim result As DialogResult = MessageBox.Show("Imprimer les Commission vendeurs?", "Imprimer", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
        If result = DialogResult.No Then
            Exit Sub
        ElseIf result = DialogResult.Yes Then

            Me.Cursor = Cursors.WaitCursor


            For Each item As DevExpress.XtraEditors.Controls.CheckedListBoxItem In cbCommissions.Properties.Items
                If item.CheckState = CheckState.Checked Then
                    strSelectedValue = strSelectedValue & item.Value & ","
                    iSelectedValue = iSelectedValue + 1
                End If
            Next

            If strSelectedValue.ToString.Trim <> "" Then
                strSelectedValue = strSelectedValue.Remove(strSelectedValue.Length - 1)
            End If


            dtRapportCommission = oRapportCommissionData.SelectAllDataByPeriodesP(IDPeriode, strSelectedValue)
            If dtRapportCommission.Rows.Count > 0 Then

                Me.Cursor = Cursors.WaitCursor

                ProgressBarControl1.Properties.Step = 1
                ProgressBarControl1.Properties.PercentView = True
                ProgressBarControl1.Properties.Maximum = dtRapportCommission.Rows.Count
                ProgressBarControl1.Properties.Minimum = 0


                For Each drRapportCommission In dtRapportCommission.Rows

                    Using document As PdfDocument = PdfDocument.Load(drRapportCommission("CheminRapport").ToString.Trim)
                        ' Print PDF document to default printer
                        Dim printerName As String = Nothing
                        document.Print(printerName)
                    End Using


                    ProgressBarControl1.PerformStep()
                    ProgressBarControl1.Update()

                Next

            End If


            ProgressBarControl1.EditValue = 0
            Me.Cursor = Cursors.Default
            MessageBox.Show("Impression des commissions vendeurs complétés avec succès?")


        Else
            Me.Cursor = Cursors.Default
            MessageBox.Show("Aucunes paie n'a été trouvé pour cette période!", "Paies", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1)
            Exit Sub
        End If

    End Sub

    Private Sub bAnnulerCommissions_Click(sender As Object, e As EventArgs) Handles bAnnulerCommissions.Click
        cbCommissions.EditValue = Nothing
        cbCommissions.SelectedText = String.Empty
        cbCommissions.Text = String.Empty
    End Sub

    Private Sub bCommisssion_ItemClick(sender As Object, e As ItemClickEventArgs) Handles bCommisssion.ItemClick
        If frmCommission.Visible = True Then
            frmCommission.WindowState = FormWindowState.Normal
            frmCommission.BringToFront()
        End If

        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        frmCommission.Show()
        Me.Cursor = System.Windows.Forms.Cursors.Default
    End Sub

    Private Sub bFournisseurs_ItemClick(sender As Object, e As ItemClickEventArgs) Handles bFournisseurs.ItemClick
        If frmFournisseurs.Visible = True Then
            frmFournisseurs.WindowState = FormWindowState.Normal
            frmFournisseurs.BringToFront()
        End If

        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        frmFournisseurs.Show()
        Me.Cursor = System.Windows.Forms.Cursors.Default
    End Sub


    Private Sub bImprimerCommission_ItemClick(sender As Object, e As ItemClickEventArgs) Handles bImprimerCommission.ItemClick
        If IsNothing(cbPeriodes.EditValue) Then
            MessageBox.Show("Vous devez selectionner une periode")
            Exit Sub
        End If




        Me.Cursor = Cursors.Default
        Dim result = MessageBox.Show("Désirez-vous imprimer les fiches de paye?", "Imprimer Fiches de paye", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
        If result = DialogResult.No Then

        ElseIf result = DialogResult.Yes Then
            Me.Cursor = Cursors.WaitCursor
            If ImprimerLaPaie() = False Then
                Me.Cursor = Cursors.Default
                Exit Sub
            End If

        End If

        FillPaieNumber()

        Me.Cursor = Cursors.Default
    End Sub

    Private Sub bSupprimerLivraisons_Click(sender As Object, e As EventArgs) Handles bSupprimerLivraisons.Click
        Dim I As Integer
        If GridView1.SelectedRowsCount() - 1 = -1 Then
            MessageBox.Show("Vous devez sélectionner au moins une ligne!", "Sélection manquante", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Exit Sub
        End If

        Dim result As Integer = MessageBox.Show("Supprimer la(les) livraison(s)?", "Supprimer livraison", MessageBoxButtons.YesNo)
        If result = DialogResult.Cancel Then

        ElseIf result = DialogResult.No Then
            Exit Sub
        ElseIf result = DialogResult.Yes Then

            Cursor = Cursors.WaitCursor

            For I = 0 To GridView1.SelectedRowsCount() - 1
                Application.DoEvents()
                If (GridView1.GetSelectedRows()(I) >= -1) Then
                    Dim row As System.Data.DataRow = GridView1.GetDataRow(GridView1.GetSelectedRows()(I))
                    SysTemGlobals.ExecuteSQL("delete from  dbo.Import WHERE ImportID =" & Convert.ToInt32(row("ImportID")))

                End If
            Next
        End If
        oImportData.DeleteFromBackup(IDPeriode)
        oImportData.CopyImportToBackup()

        grdImport.DataSource = oImportData.SelectAllFromDate(tbDateFrom.Text, tbDateTo.Text)

        Cursor = Cursors.Default
    End Sub

    Private Sub bPrêts_ItemClick(sender As Object, e As ItemClickEventArgs) Handles bPrêts.ItemClick
        If frmPrets.Visible = True Then
            frmPrets.WindowState = FormWindowState.Normal
            frmPrets.BringToFront()
        End If

        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        frmPrets.Show()
        Me.Cursor = System.Windows.Forms.Cursors.Default
    End Sub

    Private Sub tbAdresseNo_KeyPress(sender As Object, e As KeyPressEventArgs) Handles tbAdresseNo.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(Keys.Return) Then
            tbAppartement.Focus()
            e.Handled = True
        End If
    End Sub

    Private Sub tbAppartement_KeyPress(sender As Object, e As KeyPressEventArgs) Handles tbAppartement.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(Keys.Return) Then
            tbTelephone.Focus()
            e.Handled = True
        End If
    End Sub

    Private Sub tbTelephone_KeyPress(sender As Object, e As KeyPressEventArgs) Handles tbTelephone.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(Keys.Return) Then
            tbAdresse.Focus()
            e.Handled = True
        End If
    End Sub

    Private Sub tbAdresse_KeyPress(sender As Object, e As KeyPressEventArgs) Handles tbAdresse.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(Keys.Return) Then
            tbCodePostale.Focus()
            e.Handled = True
        End If
    End Sub

    Private Sub tbCodeEntree_KeyPress(sender As Object, e As KeyPressEventArgs) Handles tbCodeEntree.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(Keys.Return) Then
            tbMobile.Focus()
            e.Handled = True
        End If
    End Sub

    Private Sub tbMobile_KeyPress(sender As Object, e As KeyPressEventArgs) Handles tbMobile.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(Keys.Return) Then
            tbDistanceR.Focus()
            e.Handled = True
        End If
    End Sub

    Private Sub tbNom_KeyPress(sender As Object, e As KeyPressEventArgs) Handles tbNom.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(Keys.Return) Then
            tbAdresseNo.Focus()
            e.Handled = True
        End If
    End Sub

    Private Sub bEnvoyerLivraisonsRestaurants_BindingContextChanged(sender As Object, e As EventArgs) Handles bEnvoyerLivraisonsRestaurants.BindingContextChanged

    End Sub

    Private Sub tbCustomerAddressID_EditValueChanged(sender As Object, e As EventArgs) Handles tbCustomerAddressID.EditValueChanged

        If tbCustomerAddressID.Text = 0 Then
            layoutAddressID.Text = "Ajouter Addresse"
        Else
            layoutAddressID.Text = "Modifier Addresse"
        End If


    End Sub

    Private Sub tbImportIDL_EditValueChanged(sender As Object, e As EventArgs) Handles tbImportIDL.EditValueChanged

    End Sub


    Private Sub tbMobile_EditValueChanged(sender As Object, e As EventArgs)
        'If IsGridClick = True Then Exit Sub
        'Dim sFiltre As String = ""

        'sFiltre = "[Mobile] LIKE '" & tbTelephone.Text & "%'"
        'GridView3.Columns("Mobile").FilterInfo = New ColumnFilterInfo(sFiltre)
        'GridView3.FocusedRowHandle = 1
    End Sub

    Private Sub grdClientsAddresse_Click_1(sender As Object, e As EventArgs)
        If GridView3.FocusedRowHandle < 0 Then Exit Sub

        IsGridClick = True

        bSupprimerAddresse.Enabled = True


        ClearRecord()

        Dim row As System.Data.DataRow = GridView3.GetDataRow(GridView3.FocusedRowHandle)

        Dim clsClientAddresse As New ClientAddresse
        clsClientAddresse.IDClientAddress = System.Convert.ToInt32(row("IDClientAddress"))
        clsClientAddresse = oClientAddresseData.Select_Record(clsClientAddresse)

        If Not clsClientAddresse Is Nothing Then
            Try

                tbCustomerAddressID.Text = System.Convert.ToInt32(clsClientAddresse.IDClientAddress)
                tbNom.Text = If(IsNothing(clsClientAddresse.Nom), Nothing, clsClientAddresse.Nom.ToString.Trim)
                tbAdresseNo.Text = If(IsNothing(clsClientAddresse.AdresseNo), Nothing, clsClientAddresse.AdresseNo.ToString.Trim)
                tbAdresse.Text = If(IsNothing(clsClientAddresse.Adresse), Nothing, clsClientAddresse.Adresse.ToString.Trim)
                cbVille.EditValue = If(IsNothing(clsClientAddresse.VilleID), Nothing, clsClientAddresse.VilleID)
                tbCodePostale.Text = If(IsNothing(clsClientAddresse.CodePostale), Nothing, clsClientAddresse.CodePostale.ToUpper.ToString.Trim)
                tbAppartement.Text = If(IsNothing(clsClientAddresse.Appartement), Nothing, clsClientAddresse.Appartement.ToString.Trim)
                tbCodeEntree.Text = If(IsNothing(clsClientAddresse.CodeEntree), Nothing, clsClientAddresse.CodeEntree.ToString.Trim)
                tbTelephone.Text = If(IsNothing(clsClientAddresse.Telephone), Nothing, GetNumbers(clsClientAddresse.Telephone.ToString.Trim))
                tbMobile.Text = If(IsNothing(clsClientAddresse.Mobile), Nothing, GetNumbers(clsClientAddresse.Mobile.ToString.Trim))

            Catch
            End Try
        End If

        IsGridClick = False
    End Sub

    Private Sub bEpargnes_ItemClick(sender As Object, e As ItemClickEventArgs) Handles bEpargnes.ItemClick
        If frmEpargne.Visible = True Then
            frmEpargne.WindowState = FormWindowState.Normal
            frmEpargne.BringToFront()
        End If

        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        frmEpargne.Show()
        Me.Cursor = System.Windows.Forms.Cursors.Default
    End Sub

    Private Sub bResidences_ItemClick(sender As Object, e As ItemClickEventArgs) Handles bResidences.ItemClick
        If frmResidence.Visible = True Then
            frmResidence.WindowState = FormWindowState.Normal
            frmResidence.BringToFront()
        End If

        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        frmResidence.Show()
        Me.Cursor = System.Windows.Forms.Cursors.Default
    End Sub

    Private Sub bAnnulerResidence_Click(sender As Object, e As EventArgs) Handles bAnnulerResidence.Click
        DockResidence.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Hidden
    End Sub

    Private Sub bAppliquerResidence_Click(sender As Object, e As EventArgs) Handles bAppliquerResidence.Click

        Dim I As Integer
        If IsNothing(cbResidence.EditValue) Then
            MessageBox.Show("Vous devez selectionner une résidence")
            Exit Sub
        End If



        If GridView1.SelectedRowsCount() - 1 = -1 Then
            MessageBox.Show("Vous devez sélectionner au moins une ligne!", "Sélection manquante", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Exit Sub
        End If

        Dim result As Integer = MessageBox.Show("Affecter cette résidence aux lignes sélectionnées ?", "Affectation d'une résidence", MessageBoxButtons.YesNo)
        If result = DialogResult.Cancel Then

        ElseIf result = DialogResult.No Then
            Exit Sub
        ElseIf result = DialogResult.Yes Then

            Cursor = Cursors.WaitCursor

            For I = 0 To GridView1.SelectedRowsCount() - 1
                Application.DoEvents()
                If (GridView1.GetSelectedRows()(I) >= -1) Then
                    Dim row As System.Data.DataRow = GridView1.GetDataRow(GridView1.GetSelectedRows()(I))
                    Dim oImport As New Import
                    Dim oImportData As New ImportData

                    oImport.IDResidence = cbResidence.EditValue
                    oImport.Residence = cbResidence.Text.ToString.Trim
                    oImport.ImportID = Convert.ToInt32(row("ImportID"))
                    oImportData.UpdateResidence(oImport, oImport)
                End If
            Next
        End If
        oImportData.DeleteFromBackup(IDPeriode)
        oImportData.CopyImportToBackup()
        grdImport.DataSource = oImportData.SelectAllImportFromPeriode(IDPeriode)
        DockResidence.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Hidden

        MessageBox.Show("Les addresses ont étés affectées a la résidence :" & cbResidence.Text.ToString.Trim)

        Cursor = Cursors.Default


    End Sub

    Private Sub bAffecterResidence_Click(sender As Object, e As EventArgs) Handles bAffecterResidence.Click


        If GridView1.SelectedRowsCount() - 1 = -1 Then
            MessageBox.Show("Vous devez sélectionner au moins une ligne sur la grille des livraisons!", "Sélection manquante", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Exit Sub
        End If

        FillAllResidence()
        DockResidence.FloatLocation = New Point(CInt((Me.Width - DockResidence.Width) / 2), CInt((Me.Height - DockResidence.Height) / 2))
        DockResidence.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always
        cbResidence.Focus()

    End Sub

    Private Sub bEffacerResidence_Click(sender As Object, e As EventArgs) Handles bEffacerResidence.Click




        If GridView1.SelectedRowsCount() - 1 = -1 Then
            MessageBox.Show("Vous devez sélectionner au moins une ligne!", "Sélection manquante", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Exit Sub
        End If

        Dim result As Integer = MessageBox.Show("Effacer la résidence des lignes sélectionnées ?", "Effacer l'affectation", MessageBoxButtons.YesNo)
        If result = DialogResult.Cancel Then

        ElseIf result = DialogResult.No Then
            Exit Sub
        ElseIf result = DialogResult.Yes Then

            Cursor = Cursors.WaitCursor

            For I = 0 To GridView1.SelectedRowsCount() - 1
                Application.DoEvents()
                If (GridView1.GetSelectedRows()(I) >= -1) Then
                    Dim row As System.Data.DataRow = GridView1.GetDataRow(GridView1.GetSelectedRows()(I))
                    Dim oImport As New Import
                    Dim oImportData As New ImportData

                    oImport.IDResidence = Nothing
                    oImport.Residence = Nothing

                    oImport.ImportID = Convert.ToInt32(row("ImportID"))
                    oImportData.UpdateResidence(oImport, oImport)
                End If
            Next
        End If
        oImportData.DeleteFromBackup(IDPeriode)
        oImportData.CopyImportToBackup()
        grdImport.DataSource = oImportData.SelectAllImportFromPeriode(IDPeriode)
        DockResidence.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Hidden

        MessageBox.Show("Les affectations de résidence ont etés effacées!")

        Cursor = Cursors.Default
    End Sub

    Private Sub bRapportTaxesLivreurs_ItemClick(sender As Object, e As ItemClickEventArgs) Handles bRapportTaxesLivreurs.ItemClick

        Dim frenchCultureInfo = New Globalization.CultureInfo("fr-CA")
        Dim strPeriode As String = vbNullString
        Dim strPeriodeFile As String = vbNullString
        Dim dDateFrom As Date = go_Globals.PeriodeDateFrom
        Dim dDateTo As Date = go_Globals.PeriodeDateTo
        Dim dDateFrom2 As DateTime
        Dim dDateTo2 As DateTime
        Dim sFile As String = ""
        Dim irow As Integer = 10
        Dim dtLivreurs As New DataTable
        Dim drLivreur As DataRow
        Dim dtLivreurs2 As New DataTable
        Dim drLivreur2 As DataRow
        Dim oLivreursData As New LivreursData
        Dim sFirstCell As String = ""
        Dim sLastCell As String = ""
        Dim oPayeData As New PayeData
        Dim dTotalAmount As Double = 0
        Dim dAmount As Double = 0
        Dim dTotalTVQ As Double = 0
        Dim dTotalTPS As Double = 0
        Dim strLivreur As String = ""
        Dim NomCompagnie As String = ""
        Dim NoTPS As String = ""
        Dim NoTVQ As String = ""

        Try



            If IsNothing(cbPeriodes.EditValue) Then
                MessageBox.Show("Vous devez selectionner une periode")
                Exit Sub
            End If


            Dim oRapportTaxationDataDELETe As New RapportTaxationData

            oRapportTaxationDataDELETe.DeletePeriode(go_Globals.IDPeriode)



            Dim result As DialogResult = MessageBox.Show("Générer les rapports de taxation livreurs?", "Rapport de taxation livreurs", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
            If result = DialogResult.No Then
                Me.Cursor = Cursors.Default
                Exit Sub

            ElseIf result = DialogResult.Yes Then
            End If

            Me.Cursor = Cursors.WaitCursor


            SpreadsheetInfo.SetLicense("ERDC-FN4O-YKYN-4DBM")
            Dim sExcelPath As String
            Dim ef As ExcelFile



            '--------------------------------------------
            ' Genere la periode et cree le repertoire pour sauvegarder les rapports.
            '--------------------------------------------
            If dDateFrom.Month <> dDateTo.Month Then
                strPeriodeFile = dDateFrom.Day & " " & GetMonthName(dDateFrom, frenchCultureInfo) & "-" & dDateTo.Day & " " & GetMonthName(dDateTo, frenchCultureInfo) & " " & dDateTo.Year.ToString
            Else
                strPeriodeFile = dDateFrom.Day & "-" & dDateTo.Day & " " & GetMonthName(dDateTo, frenchCultureInfo) & " " & dDateTo.Year.ToString
            End If

            My.Computer.FileSystem.CreateDirectory(go_Globals.LocationRapportDeTaxation & "\" & strPeriodeFile)


            'Charge tous les livreurs dont on calcul les taxes.
            dtLivreurs = oPayeData.SelectAllLivreursTaxable(dDateFrom, dDateTo)


            ProgressBarControl1.Properties.Step = 1
            ProgressBarControl1.Properties.PercentView = True
            ProgressBarControl1.Properties.Maximum = dtLivreurs.Rows.Count
            ProgressBarControl1.Properties.Minimum = 0

            For Each drLivreur In dtLivreurs.Rows

                sExcelPath = Application.StartupPath() & "\RapportTaxationLivreur.xlsx"
                ef = ExcelFile.Load(sExcelPath)

                irow = 9


                '--------------------------------------------
                ' Genere le header de la facture
                '--------------------------------------------
                dtLivreurs2 = oLivreurData.SelectLivreurByID(drLivreur("IDLivreur"))
                If dtLivreurs2.Rows.Count > 0 Then

                    For Each drLivreur2 In dtLivreurs2.Rows
                        strLivreur = drLivreur2("Livreur").ToString.Trim
                        ef.Worksheets(0).Cells(2, 2).Value = IIf(drLivreur2.IsNull("Livreur"), "", drLivreur2("Livreur").ToString.Trim)
                        ef.Worksheets(0).Cells(3, 2).Value = IIf(drLivreur2.IsNull("Adresse"), "", drLivreur2("Adresse").ToString.Trim)
                        ef.Worksheets(0).Cells(4, 2).Value = IIf(drLivreur2.IsNull("Telephone"), "", drLivreur2("Telephone").ToString.Trim)
                        ef.Worksheets(0).Cells(5, 2).Value = IIf(drLivreur2.IsNull("Email"), "", drLivreur2("Email").ToString.Trim)
                        NomCompagnie = IIf(drLivreur2.IsNull("NomCompagnie"), "", drLivreur2("NomCompagnie").ToString.Trim)
                        NoTPS = IIf(drLivreur2.IsNull("NoTPS"), "", drLivreur2("NoTPS").ToString.Trim)
                        NoTVQ = IIf(drLivreur2.IsNull("NoTVQ"), "", drLivreur2("NoTVQ").ToString.Trim)

                        ef.Worksheets(0).Cells(4, 6).Value = GetMonthName(dDateFrom, frenchCultureInfo)
                        ef.Worksheets(0).Cells(7, 1).Value = strPeriodeFile.ToString

                    Next
                End If


                Dim styleAmount As New CellStyle
                styleAmount.HorizontalAlignment = HorizontalAlignmentStyle.Right
                styleAmount.VerticalAlignment = VerticalAlignmentStyle.Center


                Dim stylePeriode As New CellStyle
                stylePeriode.HorizontalAlignment = HorizontalAlignmentStyle.Left
                stylePeriode.VerticalAlignment = VerticalAlignmentStyle.Center



                ' Calcul les taxes factures pour la periode selectionnee

                Dim dtPaye As New DataTable
                Dim drPaye As DataRow

                dtPaye = oPayeData.GetTotalPayeLivreur(dDateFrom, dDateTo, drLivreur("IDLivreur"))
                For Each drPaye In dtPaye.Rows

                    dDateFrom2 = dDateFrom
                    dDateTo2 = dDateTo


                    If dDateFrom2.Month <> dDateTo2.Month Then
                        strPeriode = dDateFrom2.Day & " " & GetMonthName(dDateFrom2, frenchCultureInfo) & "-" & dDateTo2.Day & " " & GetMonthName(dDateTo2, frenchCultureInfo) & " " & dDateTo2.Year.ToString
                    Else
                        strPeriode = dDateFrom2.Day & "-" & dDateTo2.Day & " " & GetMonthName(dDateTo2, frenchCultureInfo) & " " & dDateTo2.Year.ToString
                    End If


                    ef.Worksheets(0).Cells(irow, 1).Style = stylePeriode
                    ef.Worksheets(0).Cells(irow, 1).Value = strPeriode.ToString
                    ef.Worksheets(0).Cells(irow, 6).Style = styleAmount
                    ef.Worksheets(0).Cells(irow, 6).Style.NumberFormat = "$0.00"
                    dAmount = IIf(drPaye.IsNull("TotalPaye"), 0, drPaye("TotalPaye"))
                    ef.Worksheets(0).Cells(irow, 6).Value = dAmount




                    dTotalAmount = IIf(drPaye.IsNull("TotalPaye"), 0, drPaye("TotalPaye"))
                    dTotalTPS = oPayeData.GetTotalTPSLivreur(dDateFrom, dDateTo, drLivreur("IDLivreur"))

                    dTotalTVQ = oPayeData.GetTotalTVQLivreur(dDateFrom, dDateTo, drLivreur("IDLivreur"))
                    irow = irow + 1

                Next

                irow = irow + 2

                Dim styleTotal As New CellStyle
                styleTotal.HorizontalAlignment = HorizontalAlignmentStyle.Right
                styleTotal.VerticalAlignment = VerticalAlignmentStyle.Center
                styleTotal.FillPattern.SetSolid(Color.Yellow)



                irow = irow + 2
                ef.Worksheets(0).Cells(irow, 5).Value = "TOTAL"
                ef.Worksheets(0).Cells(irow, 6).Style = styleTotal
                ef.Worksheets(0).Cells(irow, 6).Style.NumberFormat = "$0.00"
                ef.Worksheets(0).Cells(irow, 6).Value = dTotalAmount


                irow = irow + 1
                ef.Worksheets(0).Cells(irow, 5).Value = "TPS"
                ef.Worksheets(0).Cells(irow, 6).Style = styleTotal
                ef.Worksheets(0).Cells(irow, 6).Style.NumberFormat = "$0.00"
                ef.Worksheets(0).Cells(irow, 6).Value = dTotalTPS

                irow = irow + 1
                ef.Worksheets(0).Cells(irow, 6).Style = styleTotal
                ef.Worksheets(0).Cells(irow, 6).Style.NumberFormat = "$0.00"
                ef.Worksheets(0).Cells(irow, 5).Value = "TVQ"
                ef.Worksheets(0).Cells(irow, 6).Value = dTotalTVQ

                irow = irow + 2



                Dim styleFooter As New CellStyle
                styleFooter.HorizontalAlignment = HorizontalAlignmentStyle.Center
                styleFooter.VerticalAlignment = VerticalAlignmentStyle.Center
                styleFooter.WrapText = True


                sFirstCell = "B" & irow.ToString
                sLastCell = "G" & irow + 9
                ef.Worksheets(0).Cells.GetSubrange(sFirstCell, sLastCell).Merged = True
                ef.Worksheets(0).Cells.GetSubrange(sFirstCell, sLastCell).Style = styleFooter

                Dim strFooter As String = NomCompagnie.ToString.Trim & vbCrLf
                strFooter = strFooter & "# TPS: " & NoTPS.ToString.Trim & " - " & "# TVQ: " & NoTVQ.ToString.Trim & vbCrLf

                ef.Worksheets(0).Cells(irow, 2).Value = strFooter


                ' Genere le nom du rapport base sur le  nom du livreur et la periode
                sFile = strLivreur.ToString.Trim & "-" & strPeriodeFile & ".pdf"
                sFile = sFile.Replace("'", "")

                Dim oRapportTaxation As New RapportTaxation
                Dim oRapportTaxationData As New RapportTaxationData

                oRapportTaxation.IDLivreur = drLivreur("IDLivreur")
                oRapportTaxation.Livreur = strLivreur.ToString.Trim
                oRapportTaxation.DateFrom = dDateFrom
                oRapportTaxation.DateTo = dDateTo
                oRapportTaxation.IDPeriode = go_Globals.IDPeriode
                oRapportTaxation.NomFichier = sFile.ToString.Trim
                oRapportTaxation.CheminRapport = go_Globals.LocationRapportDeTaxation & "\" & strPeriodeFile & "\" & sFile
                oRapportTaxationData.Add(oRapportTaxation)





CHECKFILE:


                '' Verifie si le rapport existe deja dans le repertoire.. Si oui, cree une copie
                'Dim Icount As Integer = 0

                'If System.IO.File.Exists(go_Globals.LocationRapportDeTaxation & "\" & strPeriodeFile & "\" & sFile) Then


                '    sFile = drLivreur("Livreur").ToString.Trim & "-" & strPeriodeFile & ".pdf"
                '    sFile = sFile.Replace("'", "")
                '    GoTo CHECKFILE
                'End If


                ' Sauvegarde le rapport dans le repertoire de taxation
                ef.Save(go_Globals.LocationRapportDeTaxation & "\" & strPeriodeFile & "\" & sFile)
                ef = Nothing

                ProgressBarControl1.PerformStep()
                ProgressBarControl1.Update()

                dTotalAmount = 0
                dTotalTVQ = 0
                dTotalTPS = 0
                NoTPS = ""
                NoTVQ = ""
                NomCompagnie = ""

                irow = 9
            Next


            ProgressBarControl1.EditValue = 0
            Process.Start(go_Globals.LocationRapportDeTaxation & "\" & strPeriodeFile)

            FillRapportTaxation()
            cbPeriode_EditValueChanged(sender, e)


            Me.Cursor = Cursors.Default


        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        End Try


    End Sub

    Private Sub bRapportTaxeLivreur_ItemClick(sender As Object, e As ItemClickEventArgs) Handles bRapportTaxeLivreur.ItemClick

    End Sub

    Private Sub bEmailTaxationLivreur_Click(sender As Object, e As EventArgs) Handles bEmailTaxationLivreur.Click


        Dim dDateFrom As Date = tbDateFrom.Text
        Dim dDateTo As Date = tbDateTo.Text
        Dim oPeriode As New Periodes
        Dim frenchCultureInfo = New Globalization.CultureInfo("fr-CA")
        Dim oRapportFacturePaye As New RapportFacturePaye
        Dim oRapportTaxationData As New RapportTaxationData
        Dim dtRapportTaxation As New DataTable
        Dim drRapportTaxation As DataRow
        Dim oMail As New Mail
        Dim iIDRapport As Integer = 0
        Dim strSelectedValue As String = ""
        Dim iSelectedValue As Integer = 0

        If IsNothing(cbPeriodes.EditValue) Then
            MessageBox.Show("Vous devez selectionner une periode")
            Exit Sub
        End If


        If dDateFrom.Month <> dDateTo.Month Then
            oPeriode.Periode = dDateFrom.Day & " " & GetMonthName(dDateFrom, frenchCultureInfo) & "-" & dDateTo.Day & " " & GetMonthName(dDateTo, frenchCultureInfo) & " " & dDateTo.Year.ToString
        Else
            oPeriode.Periode = dDateFrom.Day & "-" & dDateTo.Day & " " & GetMonthName(dDateTo, frenchCultureInfo) & " " & dDateTo.Year.ToString
        End If

        IDPeriode = cbPeriodes.EditValue

        Me.Cursor = Cursors.Default
        Dim result As DialogResult = MessageBox.Show("Envoyer les fiches de taxation aux livreurs?", "Envoyer les Fiches de Taxation", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
        If result = DialogResult.No Then
            Exit Sub
        ElseIf result = DialogResult.Yes Then

            Try

                Me.Cursor = Cursors.WaitCursor

                For Each item As DevExpress.XtraEditors.Controls.CheckedListBoxItem In cbRapportTaxation.Properties.Items
                    If item.CheckState = CheckState.Checked Then
                        strSelectedValue = strSelectedValue & item.Value & ","
                        iSelectedValue = iSelectedValue + 1
                    End If
                Next

                If strSelectedValue.ToString.Trim <> "" Then
                    strSelectedValue = strSelectedValue.Remove(strSelectedValue.Length - 1)
                End If



                dtRapportTaxation = oRapportTaxationData.SelectAllDataByDateRapport(strSelectedValue)
                If dtRapportTaxation.Rows.Count > 0 Then
                    Me.Cursor = Cursors.WaitCursor
                    ProgressBarControl1.Properties.Step = 1
                    ProgressBarControl1.Properties.PercentView = True
                    ProgressBarControl1.Properties.Maximum = dtRapportTaxation.Rows.Count
                    ProgressBarControl1.Properties.Minimum = 0

                    For Each drRapportTaxation In dtRapportTaxation.Rows

                        Dim olivreur As New livreurs
                        Dim olivreur2 As New livreurs
                        olivreur.LivreurID = drRapportTaxation("IDLivreur")
                        olivreur2 = oLivreurData.Select_Record(olivreur)

                        If Not IsNothing(olivreur2) Then

                            Dim sEmail As String = If(IsNothing(olivreur2.Email), Nothing, olivreur2.Email.ToString.Trim)
                            Dim sSubject As String = olivreur2.Livreur & ",  Votre fiche de taxation pour la période " & oPeriode.Periode
                            Dim sMessage As String = olivreur2.Livreur & ", Merci pour de faire partie de notre équipe"
                            Dim sAttachment As String = If(IsNothing(drRapportTaxation("CheminRapport")), Nothing, drRapportTaxation("CheminRapport").ToString.Trim)

                            If Not IsNothing(sEmail) Then

                                Dim GGmail As New GGSMTP_GMAIL("idslivraisonexpressrapport@gmail.com", "2015livraison", )
                                Dim ToAddressies As String() = {sEmail}
                                Dim attachs() As String = {sAttachment}
                                Dim subject As String = sSubject
                                Dim body As String = sMessage

                                result = GGmail.SendMail(ToAddressies, subject, body, attachs)

                                If result Then

                                Else
                                    MsgBox(GGmail.ErrorText, MsgBoxStyle.Critical)
                                End If

                            End If

                        End If

                        ProgressBarControl1.PerformStep()
                        ProgressBarControl1.Update()
                    Next

                    ProgressBarControl1.EditValue = 0
                    Me.Cursor = Cursors.Default
                    MessageBox.Show("Envoi fiches de taxe complété avec succès?")


                Else
                    Me.Cursor = Cursors.Default
                    MessageBox.Show("Aucune fiche de taxe n'a été trouvé pour cette période!", "Paie", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1)
                    Exit Sub
                End If

            Catch ex As Exception
                Me.Cursor = Cursors.Default
                MessageBox.Show(ex.Message, "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End Try

        End If

    End Sub

    Private Sub bPrintTaxationLivreur_Click(sender As Object, e As EventArgs) Handles bPrintTaxationLivreur.Click

        GemBox.Pdf.ComponentInfo.SetLicense("AD0G-Y35C-Z376-E74D")

        Dim strSelectedValue As String = ""
        Dim iSelectedValue As Integer = 0
        Dim dDateFrom As Date = tbDateFrom.Text
        Dim dDateTo As Date = tbDateTo.Text
        Dim oRapportTaxationData As New RapportTaxationData
        Dim oPeriode As New Periodes
        Dim frenchCultureInfo = New Globalization.CultureInfo("fr-CA")
        Dim dtRapportLivraison As New DataTable
        Dim drRapportLivraison As DataRow
        Dim iIDRapport As Integer = 0


        If IsNothing(cbPeriodes.EditValue) Then
            MessageBox.Show("Vous devez selectionner une periode")
            Exit Sub
        End If

        IDPeriode = cbPeriodes.EditValue

        Me.Cursor = Cursors.Default
        Dim result As DialogResult = MessageBox.Show("Imprimer les fiches de taxation?", "Imprimer fiches de taxation", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
        If result = DialogResult.No Then
            Exit Sub
        ElseIf result = DialogResult.Yes Then

            Me.Cursor = Cursors.WaitCursor


            For Each item As DevExpress.XtraEditors.Controls.CheckedListBoxItem In cbRapportTaxation.Properties.Items
                If item.CheckState = CheckState.Checked Then
                    strSelectedValue = strSelectedValue & item.Value & ","
                    iSelectedValue = iSelectedValue + 1
                End If
            Next

            If strSelectedValue.ToString.Trim <> "" Then
                strSelectedValue = strSelectedValue.Remove(strSelectedValue.Length - 1)
            End If


            dtRapportLivraison = oRapportTaxationData.SelectAllDataByDateRapport(strSelectedValue)
            If dtRapportLivraison.Rows.Count > 0 Then

                Me.Cursor = Cursors.WaitCursor

                ProgressBarControl1.Properties.Step = 1
                ProgressBarControl1.Properties.PercentView = True
                ProgressBarControl1.Properties.Maximum = dtRapportLivraison.Rows.Count
                ProgressBarControl1.Properties.Minimum = 0


                For Each drRapportLivraison In dtRapportLivraison.Rows

                    Using document As PdfDocument = PdfDocument.Load(drRapportLivraison("CheminRapport").ToString.Trim)
                        ' Print PDF document to default printer
                        Dim printerName As String = Nothing
                        document.Print(printerName)
                    End Using


                    ProgressBarControl1.PerformStep()
                    ProgressBarControl1.Update()

                Next

            End If


            ProgressBarControl1.EditValue = 0
            Me.Cursor = Cursors.Default
            MessageBox.Show("Impression des fiches de taxation complétés avec succès?")


        Else
            Me.Cursor = Cursors.Default
            MessageBox.Show("Aucunes fiche de taxation n'a été trouvé pour cette période!", "Fiches de taxation", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1)
            Exit Sub
        End If



    End Sub

    Private Sub bResetTaxationLivreur_Click(sender As Object, e As EventArgs) Handles bResetTaxationLivreur.Click
        cbRapportTaxation.EditValue = Nothing
        cbRapportTaxation.SelectedText = String.Empty
        cbRapportTaxation.Text = String.Empty
    End Sub

    Private Sub bCalendrier_ItemClick(sender As Object, e As ItemClickEventArgs) Handles bCalendrier.ItemClick

        Dim test As New dxSampleScheduler.frmCalendrier

        test.ShowDialog()


    End Sub

    Private Sub Calendar_Click(sender As Object, e As EventArgs) Handles Calendar.Click

    End Sub

    Private Sub btnRapportTaxeIDS_ItemClick(sender As Object, e As ItemClickEventArgs) Handles btnRapportTaxeIDS.ItemClick

        Dim frenchCultureInfo = New Globalization.CultureInfo("fr-CA")
        Dim strPeriode As String = vbNullString
        Dim strPeriodeFile As String = vbNullString
        Dim dDateFrom As Date = go_Globals.PeriodeDateFrom
        Dim dDateTo As Date = go_Globals.PeriodeDateTo


        Dim sFile As String = ""
        Dim irow As Integer = 10
        Dim dtLivreurs As New DataTable

        Dim dtLivreurs2 As New DataTable
        Dim oLivreursData As New LivreursData
        Dim sFirstCell As String = ""
        Dim sLastCell As String = ""
        Dim oPayeData As New PayeData
        Dim dTotalAmount As Double = 0
        Dim dAmount As Double = 0
        Dim dTotalTVQ As Double = 0
        Dim dTotalTPS As Double = 0
        Dim strLivreur As String = ""
        Dim NomCompagnie As String = ""
        Dim NoTPS As String = ""
        Dim NoTVQ As String = ""

        Try



            Dim result As DialogResult = MessageBox.Show("Générer les rapports de taxation IDS?", "Rapport de taxation IDS", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
            If result = DialogResult.No Then
                Me.Cursor = Cursors.Default
                Exit Sub

            ElseIf result = DialogResult.Yes Then
            End If

            Me.Cursor = Cursors.WaitCursor


            SpreadsheetInfo.SetLicense("ERDC-FN4O-YKYN-4DBM")
            Dim sExcelPath As String
            Dim ef As ExcelFile



            '--------------------------------------------
            ' Genere la periode et cree le repertoire pour sauvegarder les rapports.
            '--------------------------------------------
            If dDateFrom.Month <> dDateTo.Month Then
                strPeriodeFile = dDateFrom.Day & " " & GetMonthName(dDateFrom, frenchCultureInfo) & "-" & dDateTo.Day & " " & GetMonthName(dDateTo, frenchCultureInfo) & " " & dDateTo.Year.ToString
            Else
                strPeriodeFile = dDateFrom.Day & "-" & dDateTo.Day & " " & GetMonthName(dDateTo, frenchCultureInfo) & " " & dDateTo.Year.ToString
            End If

            My.Computer.FileSystem.CreateDirectory(go_Globals.LocationRapportDeTaxationIDS & "\" & strPeriodeFile)
            sFile = go_Globals.CompanyName.ToString.Trim & "-" & strPeriodeFile & ".pdf"
            sFile = sFile.Replace("'", "")

            If My.Computer.FileSystem.FileExists(go_Globals.LocationRapportDeTaxationIDS & "\" & strPeriodeFile & "\" & sFile) Then
                My.Computer.FileSystem.DeleteFile(go_Globals.LocationRapportDeTaxationIDS & "\" & strPeriodeFile & "\" & sFile)
            End If




            ProgressBarControl1.Properties.Step = 1
            ProgressBarControl1.Properties.PercentView = True
            ProgressBarControl1.Properties.Maximum = dtLivreurs.Rows.Count
            ProgressBarControl1.Properties.Minimum = 0

            sExcelPath = Application.StartupPath() & "\RapportTaxationIDS.xlsx"
            ef = ExcelFile.Load(sExcelPath)

            irow = 9


            '--------------------------------------------
            ' Genere le header de la facture
            '--------------------------------------------

            ef.Worksheets(0).Cells(2, 2).Value = go_Globals.CompanyName
            ef.Worksheets(0).Cells(3, 2).Value = go_Globals.CompanyAdresse
            ef.Worksheets(0).Cells(4, 2).Value = go_Globals.CompanyContactTelephone
            ef.Worksheets(0).Cells(5, 2).Value = go_Globals.Username.ToString.Trim
            NomCompagnie = go_Globals.CompanyName
            NoTPS = go_Globals.TPSNumero
            NoTVQ = go_Globals.TVQNumero

            ef.Worksheets(0).Cells(4, 6).Value = cbAnnee.Text.ToString.Trim
            ef.Worksheets(0).Cells(7, 4).Value = strPeriodeFile.ToString



            Dim styleAmount As New CellStyle
            styleAmount.HorizontalAlignment = HorizontalAlignmentStyle.Right
            styleAmount.VerticalAlignment = VerticalAlignmentStyle.Center


            Dim stylePeriode As New CellStyle
            stylePeriode.HorizontalAlignment = HorizontalAlignmentStyle.Left
            stylePeriode.VerticalAlignment = VerticalAlignmentStyle.Center




            ' INVOICE -------------------------------------------------------------------------------


            Dim dAmountTotal As Double = 0
            'ef.Worksheets(0).Cells(9, 4).Style = styleAmount
            ef.Worksheets(0).Cells(9, 4).Style.NumberFormat = "$0.00"
            dAmountTotal = oInvoiceData.GetChiffreAffaireIDS(dDateFrom, dDateTo)
            ef.Worksheets(0).Cells(9, 4).Value = dAmountTotal

            'TPS
            Dim dAmountTPSinv As Double = 0
            ef.Worksheets(0).Cells(13, 3).Style = styleAmount
            ef.Worksheets(0).Cells(13, 3).Style.NumberFormat = "$0.00"
            dAmountTPSinv = oInvoiceData.GetTotalTPSIDS(dDateFrom, dDateTo)
            ef.Worksheets(0).Cells(13, 3).Value = dAmountTPSinv

            'TVQ
            Dim dAmountTVQinv As Double = 0
            ef.Worksheets(0).Cells(13, 4).Style = styleAmount
            ef.Worksheets(0).Cells(13, 4).Style.NumberFormat = "$0.00"
            dAmountTVQinv = oInvoiceData.GetTotalTVQIDS(dDateFrom, dDateTo)
            ef.Worksheets(0).Cells(13, 4).Value = dAmountTVQinv

            ef.Worksheets(0).Cells(13, 6).Style.NumberFormat = "$0.00"
            ef.Worksheets(0).Cells(13, 6).Value = dAmountTPSinv + dAmountTVQinv

            ' PAYE -------------------------------------------------------------------------------

            'TPS
            Dim dAmountTPSP As Double = 0
            Dim dAmountTPSS As Double = 0
            ef.Worksheets(0).Cells(15, 3).Style = styleAmount
            ef.Worksheets(0).Cells(15, 3).Style.NumberFormat = "$0.00"
            dAmountTPSP = oPayeData.GetTotalTPSIDS(dDateFrom, dDateTo)
            dAmountTPSS = oAchatsData.GetTotalTPSIDS(dDateFrom, dDateTo)
            ef.Worksheets(0).Cells(15, 3).Value = dAmountTPSP +dAmountTPSS

            'TVQ
            Dim dAmountTVQP As Double = 0
            Dim dAmountTVQS As Double = 0
            ef.Worksheets(0).Cells(15, 4).Style = styleAmount
            ef.Worksheets(0).Cells(15, 4).Style.NumberFormat = "$0.00"
            dAmountTVQP = oPayeData.GetTotalTVQIDS(dDateFrom, dDateTo)
            dAmountTVQS = oAchatsData.GetTotalTVQIDS(dDateFrom, dDateTo)
            ef.Worksheets(0).Cells(15, 4).Value = dAmountTVQP + dAmountTVQS

            ef.Worksheets(0).Cells(15, 6).Style.NumberFormat = "$0.00"
            ef.Worksheets(0).Cells(15, 6).Value = dAmountTVQP + dAmountTPSP




            Dim dAmountTPST As Double = 0
            Dim dAmountTVQT As Double = 0

            ' TOTAl a Payer -------------------------------------------------------------------------------


            ef.Worksheets(0).Cells(18, 3).Style = styleAmount
            ef.Worksheets(0).Cells(18, 3).Style.NumberFormat = "$0.00"
            dAmountTPST = dAmountTPSinv - (dAmountTPSP + dAmountTPSS)
            ef.Worksheets(0).Cells(18, 3).Value = dAmountTPST

            'TVQ

            ef.Worksheets(0).Cells(18, 4).Style = styleAmount
            ef.Worksheets(0).Cells(18, 4).Style.NumberFormat = "$0.00"
            dAmountTVQT = dAmountTVQinv - (dAmountTVQP + dAmountTVQS)
            ef.Worksheets(0).Cells(18, 4).Value = dAmountTVQT

            ef.Worksheets(0).Cells(18, 6).Style.NumberFormat = "$0.00"
            ef.Worksheets(0).Cells(18, 6).Value = dAmountTPST + dAmountTVQT


            ef.Worksheets(0).Cells(20, 6).Style.NumberFormat = "$0.00"
            ef.Worksheets(0).Cells(20, 6).Value = dAmountTotal - (dAmountTPST + dAmountTVQT)


            irow = 24

            Dim styleTotal As New CellStyle
            styleTotal.HorizontalAlignment = HorizontalAlignmentStyle.Right
            styleTotal.VerticalAlignment = VerticalAlignmentStyle.Center
            styleTotal.FillPattern.SetSolid(Color.Yellow)


            Dim styleFooter As New CellStyle
            styleFooter.HorizontalAlignment = HorizontalAlignmentStyle.Center
            styleFooter.VerticalAlignment = VerticalAlignmentStyle.Center
            styleFooter.WrapText = True


            sFirstCell = "B" & irow.ToString
            sLastCell = "G" & irow + 9
            ef.Worksheets(0).Cells.GetSubrange(sFirstCell, sLastCell).Merged = True
            ef.Worksheets(0).Cells.GetSubrange(sFirstCell, sLastCell).Style = styleFooter

            Dim strFooter As String = go_Globals.CompanyName.ToString.Trim & vbCrLf
            strFooter = strFooter & "# TPS: " & go_Globals.TPSNumero.ToString.Trim & " - " & "# TVQ: " & go_Globals.TVQNumero.ToString.Trim & vbCrLf
            ef.Worksheets(0).Cells(irow, 2).Value = strFooter


            ' Genere le nom du rapport base sur le  nom du livreur et la periode
            sFile = go_Globals.CompanyName.ToString.Trim & "-" & strPeriodeFile & ".pdf"
            sFile = sFile.Replace("'", "")



CHECKFILE:




            ' Sauvegarde le rapport dans le repertoire de taxation
            ef.Save(go_Globals.LocationRapportDeTaxationIDS & "\" & strPeriodeFile & "\" & sFile)
            ef = Nothing

            Process.Start(go_Globals.LocationRapportDeTaxationIDS & "\" & strPeriodeFile)


            Me.Cursor = Cursors.Default


        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        End Try


    End Sub

    Private Sub bResetTaxationLivreurPaye_Click(sender As Object, e As EventArgs) Handles bResetTaxationLivreurPaye.Click
        cbRapportRevenuPaye.EditValue = Nothing
        cbRapportRevenuPaye.SelectedText = String.Empty
        cbRapportRevenuPaye.Text = String.Empty
    End Sub

    Private Sub bPrintTaxationLivreurPaye_Click(sender As Object, e As EventArgs) Handles bPrintTaxationLivreurPaye.Click
        GemBox.Pdf.ComponentInfo.SetLicense("AD0G-Y35C-Z376-E74D")

        Dim strSelectedValue As String = ""
        Dim iSelectedValue As Integer = 0
        Dim dDateFrom As Date = tbDateFrom.Text
        Dim dDateTo As Date = tbDateTo.Text
        Dim oRapportTaxationData As New RapportTaxationPayeLivreurData
        Dim oPeriode As New Periodes
        Dim frenchCultureInfo = New Globalization.CultureInfo("fr-CA")
        Dim dtRapportLivraison As New DataTable
        Dim drRapportLivraison As DataRow
        Dim iIDRapport As Integer = 0


        If IsNothing(cbPeriodes.EditValue) Then
            MessageBox.Show("Vous devez selectionner une periode")
            Exit Sub
        End If

        IDPeriode = cbPeriodes.EditValue

        Me.Cursor = Cursors.Default
        Dim result As DialogResult = MessageBox.Show("Imprimer les fiches de taxation?", "Imprimer fiches de taxation", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
        If result = DialogResult.No Then
            Exit Sub
        ElseIf result = DialogResult.Yes Then

            Me.Cursor = Cursors.WaitCursor


            For Each item As DevExpress.XtraEditors.Controls.CheckedListBoxItem In cbRapportRevenuPaye.Properties.Items
                If item.CheckState = CheckState.Checked Then
                    strSelectedValue = strSelectedValue & item.Value & ","
                    iSelectedValue = iSelectedValue + 1
                End If
            Next

            If strSelectedValue.ToString.Trim <> "" Then
                strSelectedValue = strSelectedValue.Remove(strSelectedValue.Length - 1)
            End If


            dtRapportLivraison = oRapportTaxationData.SelectAllDataByDateRapport(strSelectedValue)
            If dtRapportLivraison.Rows.Count > 0 Then

                Me.Cursor = Cursors.WaitCursor

                ProgressBarControl1.Properties.Step = 1
                ProgressBarControl1.Properties.PercentView = True
                ProgressBarControl1.Properties.Maximum = dtRapportLivraison.Rows.Count
                ProgressBarControl1.Properties.Minimum = 0


                For Each drRapportLivraison In dtRapportLivraison.Rows

                    Using document As PdfDocument = PdfDocument.Load(drRapportLivraison("CheminRapport").ToString.Trim)
                        ' Print PDF document to default printer
                        Dim printerName As String = Nothing
                        document.Print(printerName)
                    End Using


                    ProgressBarControl1.PerformStep()
                    ProgressBarControl1.Update()

                Next

            End If


            ProgressBarControl1.EditValue = 0
            Me.Cursor = Cursors.Default
            MessageBox.Show("Impression des fiches de taxation complétés avec succès?")


        Else
            Me.Cursor = Cursors.Default
            MessageBox.Show("Aucunes fiche de taxation n'a été trouvé pour cette période!", "Fiches de taxation", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1)
            Exit Sub
        End If


    End Sub

    Private Sub bEmailTaxationLivreurPaye_Click(sender As Object, e As EventArgs) Handles bEmailTaxationLivreurPaye.Click


        Dim dDateFrom As Date = tbDateFrom.Text
        Dim dDateTo As Date = tbDateTo.Text
        Dim oPeriode As New Periodes
        Dim frenchCultureInfo = New Globalization.CultureInfo("fr-CA")
        Dim oRapportTaxation As New RapportFacturePaye
        Dim oRapportTaxationData As New RapportTaxationPayeLivreurData
        Dim dtRapportTaxation As New DataTable
        Dim drRapportTaxation As DataRow
        Dim oMail As New Mail
        Dim iIDRapport As Integer = 0
        Dim strSelectedValue As String = ""
        Dim iSelectedValue As Integer = 0

        If IsNothing(cbPeriodes.EditValue) Then
            MessageBox.Show("Vous devez selectionner une periode")
            Exit Sub
        End If


        If dDateFrom.Month <> dDateTo.Month Then
            oPeriode.Periode = dDateFrom.Day & " " & GetMonthName(dDateFrom, frenchCultureInfo) & "-" & dDateTo.Day & " " & GetMonthName(dDateTo, frenchCultureInfo) & " " & dDateTo.Year.ToString
        Else
            oPeriode.Periode = dDateFrom.Day & "-" & dDateTo.Day & " " & GetMonthName(dDateTo, frenchCultureInfo) & " " & dDateTo.Year.ToString
        End If

        IDPeriode = cbPeriodes.EditValue

        Me.Cursor = Cursors.Default
        Dim result As DialogResult = MessageBox.Show("Envoyer les fiches de revenu aux livreurs?", "Envoyer les Fiches de Revenu", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
        If result = DialogResult.No Then
            Exit Sub
        ElseIf result = DialogResult.Yes Then

            Try

                Me.Cursor = Cursors.WaitCursor

                For Each item As DevExpress.XtraEditors.Controls.CheckedListBoxItem In cbRapportRevenuPaye.Properties.Items
                    If item.CheckState = CheckState.Checked Then
                        strSelectedValue = strSelectedValue & item.Value & ","
                        iSelectedValue = iSelectedValue + 1
                    End If
                Next

                If strSelectedValue.ToString.Trim <> "" Then
                    strSelectedValue = strSelectedValue.Remove(strSelectedValue.Length - 1)
                End If



                dtRapportTaxation = oRapportTaxationData.SelectAllDataByDateRapport(strSelectedValue)
                If dtRapportTaxation.Rows.Count > 0 Then
                    Me.Cursor = Cursors.WaitCursor
                    ProgressBarControl1.Properties.Step = 1
                    ProgressBarControl1.Properties.PercentView = True
                    ProgressBarControl1.Properties.Maximum = dtRapportTaxation.Rows.Count
                    ProgressBarControl1.Properties.Minimum = 0

                    For Each drRapportTaxation In dtRapportTaxation.Rows

                        Dim olivreur As New livreurs
                        Dim olivreur2 As New livreurs
                        olivreur.LivreurID = drRapportTaxation("IDLivreur")
                        olivreur2 = oLivreurData.Select_Record(olivreur)

                        If Not IsNothing(olivreur2) Then

                            Dim sEmail As String = If(IsNothing(olivreur2.Email), Nothing, olivreur2.Email.ToString.Trim)
                            Dim sSubject As String = olivreur2.Livreur & ",  Votre fiche de revenu pour la période " & oPeriode.Periode
                            Dim sMessage As String = olivreur2.Livreur & ", Merci pour de faire partie de notre équipe"
                            Dim sAttachment As String = If(IsNothing(drRapportTaxation("CheminRapport")), Nothing, drRapportTaxation("CheminRapport").ToString.Trim)

                            If Not IsNothing(sEmail) Then

                                Dim GGmail As New GGSMTP_GMAIL("idslivraisonexpressrapport@gmail.com", "2015livraison", )
                                Dim ToAddressies As String() = {sEmail}
                                Dim attachs() As String = {sAttachment}
                                Dim subject As String = sSubject
                                Dim body As String = sMessage

                                result = GGmail.SendMail(ToAddressies, subject, body, attachs)

                                If result Then

                                Else
                                    MsgBox(GGmail.ErrorText, MsgBoxStyle.Critical)
                                End If

                            End If

                        End If

                        ProgressBarControl1.PerformStep()
                        ProgressBarControl1.Update()
                    Next

                    ProgressBarControl1.EditValue = 0
                    Me.Cursor = Cursors.Default
                    MessageBox.Show("Envoi fiches de taxe complété avec succès?")


                Else
                    Me.Cursor = Cursors.Default
                    MessageBox.Show("Aucune fiche de taxe n'a été trouvé pour cette période!", "Paie", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1)
                    Exit Sub
                End If

            Catch ex As Exception
                Me.Cursor = Cursors.Default
                MessageBox.Show(ex.Message, "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End Try

        End If

    End Sub

    Private Sub btnRapportTaxePayeLivreur_ItemClick(sender As Object, e As ItemClickEventArgs) Handles btnRapportTaxePayeLivreur.ItemClick

        Dim frenchCultureInfo = New Globalization.CultureInfo("fr-CA")
        Dim strPeriode As String = vbNullString
        Dim strPeriodeFile As String = vbNullString
        Dim dDateFrom As Date = go_Globals.PeriodeDateFrom
        Dim dDateTo As Date = go_Globals.PeriodeDateTo
        Dim dDateFrom2 As DateTime
        Dim dDateTo2 As DateTime
        Dim sFile As String = ""
        Dim irow As Integer = 10
        Dim dtLivreurs As New DataTable
        Dim drLivreur As DataRow
        Dim dtLivreurs2 As New DataTable
        Dim drLivreur2 As DataRow
        Dim oLivreursData As New LivreursData
        Dim sFirstCell As String = ""
        Dim sLastCell As String = ""
        Dim oPayeData As New PayeData
        Dim dTotalAmount As Double = 0
        Dim dAmount As Double = 0
        Dim dTotalTVQ As Double = 0
        Dim dTotalTPS As Double = 0
        Dim strLivreur As String = ""
        Dim NomCompagnie As String = ""
        Dim NoTPS As String = ""
        Dim NoTVQ As String = ""

        Try


            If IsNothing(cbPeriodes.EditValue) Then
                MessageBox.Show("Vous devez selectionner une periode")
                Exit Sub
            End If


            Dim oRapportTaxationDataDELETE As New RapportTaxationPayeLivreurData

            oRapportTaxationDataDELETE.DeletePeriode(go_Globals.IDPeriode)



            Dim result As DialogResult = MessageBox.Show("Générer les rapports de revenu livreurs?", "Rapport de de revenu livreurs", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
            If result = DialogResult.No Then
                Me.Cursor = Cursors.Default
                Exit Sub

            ElseIf result = DialogResult.Yes Then
            End If

            Me.Cursor = Cursors.WaitCursor


            SpreadsheetInfo.SetLicense("ERDC-FN4O-YKYN-4DBM")
            Dim sExcelPath As String
            Dim ef As ExcelFile



            '--------------------------------------------
            ' Genere la periode et cree le repertoire pour sauvegarder les rapports.
            '--------------------------------------------
            If dDateFrom.Month <> dDateTo.Month Then
                strPeriodeFile = dDateFrom.Day & " " & GetMonthName(dDateFrom, frenchCultureInfo) & "-" & dDateTo.Day & " " & GetMonthName(dDateTo, frenchCultureInfo) & " " & dDateTo.Year.ToString
            Else
                strPeriodeFile = dDateFrom.Day & "-" & dDateTo.Day & " " & GetMonthName(dDateTo, frenchCultureInfo) & " " & dDateTo.Year.ToString
            End If

            My.Computer.FileSystem.CreateDirectory(go_Globals.RapportTaxationLivreurPaye & "\" & strPeriodeFile)


            'Charge tous les livreurs dont on calcul les taxes.
            dtLivreurs = oPayeData.SelectAllLivreursRevenu(dDateFrom, dDateTo)


            ProgressBarControl1.Properties.Step = 1
            ProgressBarControl1.Properties.PercentView = True
            ProgressBarControl1.Properties.Maximum = dtLivreurs.Rows.Count
            ProgressBarControl1.Properties.Minimum = 0

            For Each drLivreur In dtLivreurs.Rows

                sExcelPath = Application.StartupPath() & "\TaxationRevenuLivreur.xlsx"
                ef = ExcelFile.Load(sExcelPath)

                irow = 6


                '--------------------------------------------
                ' Genere le header de la facture
                '--------------------------------------------
                dtLivreurs2 = oLivreurData.SelectLivreurByID(drLivreur("IDLivreur"))
                If dtLivreurs2.Rows.Count > 0 Then

                    For Each drLivreur2 In dtLivreurs2.Rows
                        strLivreur = drLivreur2("Livreur").ToString.Trim
                        ef.Worksheets(0).Cells(1, 2).Value = IIf(drLivreur2.IsNull("Livreur"), "", drLivreur2("Livreur").ToString.Trim)
                        ef.Worksheets(0).Cells(2, 2).Value = IIf(drLivreur2.IsNull("Adresse"), "", drLivreur2("Adresse").ToString.Trim)
                        ef.Worksheets(0).Cells(3, 2).Value = IIf(drLivreur2.IsNull("Email"), "", drLivreur2("Email").ToString.Trim)
                        ef.Worksheets(0).Cells(4, 2).Value = IIf(drLivreur2.IsNull("Telephone"), "", drLivreur2("Telephone").ToString.Trim)




                        'Dim styleAmount As New CellStyle
                        'styleAmount.HorizontalAlignment = HorizontalAlignmentStyle.Right
                        'styleAmount.VerticalAlignment = VerticalAlignmentStyle.Center


                        'Dim stylePeriode As New CellStyle
                        'stylePeriode.HorizontalAlignment = HorizontalAlignmentStyle.Left
                        'stylePeriode.VerticalAlignment = VerticalAlignmentStyle.Center



                        ' Calcul les taxes factures pour la periode selectionnee

                        Dim dtPaye As New DataTable
                        Dim drPaye As DataRow

                        dtPaye = oPayeData.GetTotalPayeLivreur(dDateFrom, dDateTo, drLivreur("IDLivreur"))
                        For Each drPaye In dtPaye.Rows

                            dDateFrom2 = dDateFrom
                            dDateTo2 = dDateTo


                            If dDateFrom2.Month <> dDateTo2.Month Then
                                strPeriode = dDateFrom2.Day & " " & GetMonthName(dDateFrom2, frenchCultureInfo) & "-" & dDateTo2.Day & " " & GetMonthName(dDateTo2, frenchCultureInfo) & " " & dDateTo2.Year.ToString
                            Else
                                strPeriode = dDateFrom2.Day & "-" & dDateTo2.Day & " " & GetMonthName(dDateTo2, frenchCultureInfo) & " " & dDateTo2.Year.ToString
                            End If


                            ef.Worksheets(0).Cells(irow, 1).Value = strPeriode.ToString
                            ef.Worksheets(0).Cells(irow, 6).Style.NumberFormat = "$0.00"
                            dAmount = IIf(drPaye.IsNull("TotalPaye"), 0, drPaye("TotalPaye"))
                            ef.Worksheets(0).Cells(irow, 6).Value = dAmount

                            ef.Worksheets(0).Cells(irow, 1).Value = strPeriode.ToString
                            ef.Worksheets(0).Cells(irow, 5).Style.NumberFormat = "$0.00"
                            ef.Worksheets(0).Cells(irow, 5).Value = dAmount




                            dTotalAmount = dTotalAmount + IIf(drPaye.IsNull("TotalPaye"), 0, drPaye("TotalPaye"))
                            irow = irow + 1

                        Next
                        ef.Worksheets(0).Cells(16, 6).Style.NumberFormat = "$0.00"
                        ef.Worksheets(0).Cells(14, 6).Value = dTotalAmount

                        ' Genere le nom du rapport base sur le  nom du livreur et la periode
                        sFile = strLivreur.ToString.Trim & "-" & strPeriodeFile & ".pdf"
                        sFile = sFile.Replace("'", "")

                        Dim oRapportTaxation As New RapportTaxationPayeLivreur
                        Dim oRapportTaxationData As New RapportTaxationPayeLivreurData

                        oRapportTaxation.IDLivreur = drLivreur("IDLivreur")
                        oRapportTaxation.Livreur = strLivreur.ToString.Trim
                        oRapportTaxation.DateFrom = dDateFrom
                        oRapportTaxation.DateTo = dDateTo
                        oRapportTaxation.IDPeriode = go_Globals.IDPeriode
                        oRapportTaxation.NomFichier = sFile.ToString.Trim
                        oRapportTaxation.CheminRapport = go_Globals.RapportTaxationLivreurPaye & "\" & strPeriodeFile & "\" & sFile
                        oRapportTaxationData.Add(oRapportTaxation)


                        ' Sauvegarde le rapport dans le repertoire de taxation
                        ef.Save(go_Globals.RapportTaxationLivreurPaye & "\" & strPeriodeFile & "\" & sFile)
                        ef = Nothing



                    Next
                End If






CHECKFILE:





                ProgressBarControl1.PerformStep()
                ProgressBarControl1.Update()

                dTotalAmount = 0
                dTotalTVQ = 0
                dTotalTPS = 0
                NoTPS = ""
                NoTVQ = ""
                NomCompagnie = ""

                irow = 9
            Next


            ProgressBarControl1.EditValue = 0
            Process.Start(go_Globals.RapportTaxationLivreurPaye & "\" & strPeriodeFile)

            FillRapportRevenu()

            cbPeriode_EditValueChanged(sender, e)


            Me.Cursor = Cursors.Default


        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        End Try


    End Sub

    Private Sub bLivraisonParRestaurant2_ItemClick(sender As Object, e As ItemClickEventArgs) Handles bLivraisonParRestaurant2.ItemClick


        If IsNothing(cbPeriodes.EditValue) Then
            MessageBox.Show("Vous devez selectionner une periode")
            Exit Sub
        End If



        Dim result As DialogResult = MessageBox.Show("Désirez-vous imprimer les rapports de livraison des restaurants?", "Imprimer les rapports de livraison des restaurants", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
        If result = DialogResult.No Then

        ElseIf result = DialogResult.Yes Then
            Me.Cursor = Cursors.WaitCursor
            If ImprimerRapportDesLivraisonParRestaurant() = False Then

                Me.Cursor = Cursors.Default
                Exit Sub
            End If

        End If



        Me.Cursor = Cursors.Default
    End Sub
End Class