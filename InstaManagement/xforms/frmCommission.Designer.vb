﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmCommission
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmCommission))
        Me.TS = New System.Windows.Forms.ToolStrip()
        Me.Head = New System.Windows.Forms.ToolStripLabel()
        Me.sp1 = New System.Windows.Forms.ToolStripSeparator()
        Me.bNouvelleCommission = New System.Windows.Forms.ToolStripButton()
        Me.bDelete = New System.Windows.Forms.ToolStripButton()
        Me.sp3 = New System.Windows.Forms.ToolStripSeparator()
        Me.bAjouterLaCommission = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.bCancel = New System.Windows.Forms.ToolStripButton()
        Me.sp4 = New System.Windows.Forms.ToolStripSeparator()
        Me.ToolStripButton6 = New System.Windows.Forms.ToolStripButton()
        Me.LayoutControl1 = New DevExpress.XtraLayout.LayoutControl()
        Me.tabData = New DevExpress.XtraTab.XtraTabControl()
        Me.XtraTabPage11 = New DevExpress.XtraTab.XtraTabPage()
        Me.LayoutControl21 = New DevExpress.XtraLayout.LayoutControl()
        Me.grdCommission = New DevExpress.XtraGrid.GridControl()
        Me.GridView11 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colID = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colPayeNumber = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colPayeDate = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colIDLivreur = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colLivreur = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colDateFrom = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colDateTo = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.LayoutControlGroup11 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem2 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.XtraTabPage2 = New DevExpress.XtraTab.XtraTabPage()
        Me.LayoutControl3 = New DevExpress.XtraLayout.LayoutControl()
        Me.grdCommissionLine = New DevExpress.XtraGrid.GridControl()
        Me.GridView2 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colIDInvoiceLine = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colIDPaye = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colPayeNumber2 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colDateFrom2 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colDateTo2 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colIDLivreur2 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colLivreur2 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colOrganisation = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCredit = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colNombreGlacier = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colGlacier = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colKm = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colDescription = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colUnite = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colPrix = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colMontant = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colNombreGlacier2 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colTotalGlacier = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.LayoutControlGroup21 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem11 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.Root = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.Root1 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem3 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.DockManager1 = New DevExpress.XtraBars.Docking.DockManager(Me.components)
        Me.DockCommission = New DevExpress.XtraBars.Docking.DockPanel()
        Me.DockPanel2_Container = New DevExpress.XtraBars.Docking.ControlContainer()
        Me.LayoutControl4 = New DevExpress.XtraLayout.LayoutControl()
        Me.bSauvegarde = New DevExpress.XtraEditors.SimpleButton()
        Me.btnAnnuler = New DevExpress.XtraEditors.SimpleButton()
        Me.tbIDCommissionLine = New DevExpress.XtraEditors.TextEdit()
        Me.tbIDCommission = New DevExpress.XtraEditors.TextEdit()
        Me.tbCommissionNumber = New DevExpress.XtraEditors.TextEdit()
        Me.tbCommissionDate = New DevExpress.XtraEditors.DateEdit()
        Me.cbPeriodes = New DevExpress.XtraEditors.LookUpEdit()
        Me.tbDateFrom = New DevExpress.XtraEditors.DateEdit()
        Me.tbDateAu = New DevExpress.XtraEditors.DateEdit()
        Me.cbLivreur = New DevExpress.XtraEditors.LookUpEdit()
        Me.cbOrganisation = New DevExpress.XtraEditors.LookUpEdit()
        Me.tbDescription = New DevExpress.XtraEditors.TextEdit()
        Me.tbQuantite = New DevExpress.XtraEditors.TextEdit()
        Me.tbPrix = New DevExpress.XtraEditors.TextEdit()
        Me.tbTotal = New DevExpress.XtraEditors.TextEdit()
        Me.radChoice = New DevExpress.XtraEditors.RadioGroup()
        Me.chkAjout = New DevExpress.XtraEditors.CheckEdit()
        Me.LayoutControlGroup2 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlGroup5 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.EmptySpaceItem1 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutSave = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem16 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutNombreFree2 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutNombreFree1 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem10 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem18 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem15 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem13 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem14 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem20 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem27 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem12 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.lblQuantite = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutPayeAuLivreur = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutPayeAuLivreur1 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem21 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem32 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.DockNouvelleCommission = New DevExpress.XtraBars.Docking.DockPanel()
        Me.DockPanel1_Container = New DevExpress.XtraBars.Docking.ControlContainer()
        Me.LayoutControl2 = New DevExpress.XtraLayout.LayoutControl()
        Me.tbCommissionNumberNP = New DevExpress.XtraEditors.TextEdit()
        Me.cbLivreurNP = New DevExpress.XtraEditors.LookUpEdit()
        Me.tbDateFromNP = New DevExpress.XtraEditors.DateEdit()
        Me.tbDateAuNP = New DevExpress.XtraEditors.DateEdit()
        Me.cbPeriodesNP = New DevExpress.XtraEditors.LookUpEdit()
        Me.tbIDCommissionNP = New DevExpress.XtraEditors.TextEdit()
        Me.bSauvegarderNouvelleCommission = New DevExpress.XtraEditors.SimpleButton()
        Me.bAnnulerNouvelleCommission = New DevExpress.XtraEditors.SimpleButton()
        Me.tbCommissionDateNP = New DevExpress.XtraEditors.DateEdit()
        Me.LayoutControlGroup1 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlGroup3 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem23 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem24 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem25 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem26 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem28 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem29 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem30 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem31 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem1 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.TS.SuspendLayout()
        CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LayoutControl1.SuspendLayout()
        CType(Me.tabData, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabData.SuspendLayout()
        Me.XtraTabPage11.SuspendLayout()
        CType(Me.LayoutControl21, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LayoutControl21.SuspendLayout()
        CType(Me.grdCommission, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabPage2.SuspendLayout()
        CType(Me.LayoutControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LayoutControl3.SuspendLayout()
        CType(Me.grdCommissionLine, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup21, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Root, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Root1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DockManager1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.DockCommission.SuspendLayout()
        Me.DockPanel2_Container.SuspendLayout()
        CType(Me.LayoutControl4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LayoutControl4.SuspendLayout()
        CType(Me.tbIDCommissionLine.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbIDCommission.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbCommissionNumber.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbCommissionDate.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbCommissionDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbPeriodes.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbDateFrom.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbDateFrom.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbDateAu.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbDateAu.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbLivreur.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbOrganisation.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbDescription.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbQuantite.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbPrix.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbTotal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radChoice.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkAjout.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutSave, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem16, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutNombreFree2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutNombreFree1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem18, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem15, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem13, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem14, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem20, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem27, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblQuantite, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutPayeAuLivreur, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutPayeAuLivreur1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem21, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem32, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.DockNouvelleCommission.SuspendLayout()
        Me.DockPanel1_Container.SuspendLayout()
        CType(Me.LayoutControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LayoutControl2.SuspendLayout()
        CType(Me.tbCommissionNumberNP.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbLivreurNP.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbDateFromNP.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbDateFromNP.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbDateAuNP.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbDateAuNP.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbPeriodesNP.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbIDCommissionNP.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbCommissionDateNP.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbCommissionDateNP.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem23, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem24, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem25, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem26, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem28, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem29, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem30, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem31, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'TS
        '
        Me.TS.AutoSize = False
        Me.TS.Font = New System.Drawing.Font("Verdana", 9.75!)
        Me.TS.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Head, Me.sp1, Me.bNouvelleCommission, Me.bDelete, Me.sp3, Me.bAjouterLaCommission, Me.ToolStripSeparator2, Me.bCancel, Me.sp4, Me.ToolStripButton6})
        Me.TS.Location = New System.Drawing.Point(0, 0)
        Me.TS.Name = "TS"
        Me.TS.Size = New System.Drawing.Size(1379, 39)
        Me.TS.TabIndex = 9
        '
        'Head
        '
        Me.Head.Name = "Head"
        Me.Head.Size = New System.Drawing.Size(0, 36)
        '
        'sp1
        '
        Me.sp1.Name = "sp1"
        Me.sp1.Size = New System.Drawing.Size(6, 39)
        '
        'bNouvelleCommission
        '
        Me.bNouvelleCommission.Image = CType(resources.GetObject("bNouvelleCommission.Image"), System.Drawing.Image)
        Me.bNouvelleCommission.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.bNouvelleCommission.Name = "bNouvelleCommission"
        Me.bNouvelleCommission.Size = New System.Drawing.Size(163, 36)
        Me.bNouvelleCommission.Text = "Nouvelle Commission"
        '
        'bDelete
        '
        Me.bDelete.Image = CType(resources.GetObject("bDelete.Image"), System.Drawing.Image)
        Me.bDelete.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.bDelete.Name = "bDelete"
        Me.bDelete.Size = New System.Drawing.Size(93, 36)
        Me.bDelete.Text = "Supprimer"
        Me.bDelete.Visible = False
        '
        'sp3
        '
        Me.sp3.Name = "sp3"
        Me.sp3.Size = New System.Drawing.Size(6, 39)
        '
        'bAjouterLaCommission
        '
        Me.bAjouterLaCommission.Image = CType(resources.GetObject("bAjouterLaCommission.Image"), System.Drawing.Image)
        Me.bAjouterLaCommission.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.bAjouterLaCommission.Name = "bAjouterLaCommission"
        Me.bAjouterLaCommission.Size = New System.Drawing.Size(240, 36)
        Me.bAjouterLaCommission.Text = "Ajouter une ligne de commission"
        Me.bAjouterLaCommission.Visible = False
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        Me.ToolStripSeparator2.Size = New System.Drawing.Size(6, 39)
        '
        'bCancel
        '
        Me.bCancel.Image = CType(resources.GetObject("bCancel.Image"), System.Drawing.Image)
        Me.bCancel.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.bCancel.Name = "bCancel"
        Me.bCancel.Size = New System.Drawing.Size(246, 36)
        Me.bCancel.Text = "Retour a la liste des commissions"
        Me.bCancel.Visible = False
        '
        'sp4
        '
        Me.sp4.Name = "sp4"
        Me.sp4.Size = New System.Drawing.Size(6, 39)
        '
        'ToolStripButton6
        '
        Me.ToolStripButton6.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.ToolStripButton6.Image = CType(resources.GetObject("ToolStripButton6.Image"), System.Drawing.Image)
        Me.ToolStripButton6.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton6.Name = "ToolStripButton6"
        Me.ToolStripButton6.Size = New System.Drawing.Size(73, 36)
        Me.ToolStripButton6.Text = "Fermer"
        '
        'LayoutControl1
        '
        Me.LayoutControl1.Controls.Add(Me.tabData)
        Me.LayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LayoutControl1.Location = New System.Drawing.Point(0, 39)
        Me.LayoutControl1.Name = "LayoutControl1"
        Me.LayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = New System.Drawing.Rectangle(1021, 303, 650, 400)
        Me.LayoutControl1.Root = Me.Root
        Me.LayoutControl1.Size = New System.Drawing.Size(1379, 551)
        Me.LayoutControl1.TabIndex = 10
        Me.LayoutControl1.Text = "LayoutControl1"
        '
        'tabData
        '
        Me.tabData.Location = New System.Drawing.Point(22, 22)
        Me.tabData.Name = "tabData"
        Me.tabData.SelectedTabPage = Me.XtraTabPage11
        Me.tabData.Size = New System.Drawing.Size(1335, 507)
        Me.tabData.TabIndex = 9
        Me.tabData.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.XtraTabPage11, Me.XtraTabPage2})
        '
        'XtraTabPage11
        '
        Me.XtraTabPage11.Controls.Add(Me.LayoutControl21)
        Me.XtraTabPage11.Name = "XtraTabPage11"
        Me.XtraTabPage11.Size = New System.Drawing.Size(1330, 481)
        Me.XtraTabPage11.Text = "Liste des commissions"
        '
        'LayoutControl21
        '
        Me.LayoutControl21.Controls.Add(Me.grdCommission)
        Me.LayoutControl21.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LayoutControl21.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControl21.Name = "LayoutControl21"
        Me.LayoutControl21.Root = Me.LayoutControlGroup11
        Me.LayoutControl21.Size = New System.Drawing.Size(1330, 481)
        Me.LayoutControl21.TabIndex = 0
        Me.LayoutControl21.Text = "LayoutControl2"
        '
        'grdCommission
        '
        Me.grdCommission.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grdCommission.Location = New System.Drawing.Point(12, 12)
        Me.grdCommission.MainView = Me.GridView11
        Me.grdCommission.Name = "grdCommission"
        Me.grdCommission.Size = New System.Drawing.Size(1306, 457)
        Me.grdCommission.TabIndex = 5
        Me.grdCommission.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView11})
        '
        'GridView11
        '
        Me.GridView11.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colID, Me.colPayeNumber, Me.colPayeDate, Me.colIDLivreur, Me.colLivreur, Me.colDateFrom, Me.colDateTo})
        Me.GridView11.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.GridView11.GridControl = Me.grdCommission
        Me.GridView11.Name = "GridView11"
        Me.GridView11.OptionsBehavior.Editable = False
        Me.GridView11.OptionsCustomization.AllowGroup = False
        Me.GridView11.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridView11.OptionsSelection.UseIndicatorForSelection = False
        Me.GridView11.OptionsView.ShowAutoFilterRow = True
        Me.GridView11.OptionsView.ShowGroupPanel = False
        Me.GridView11.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.colPayeNumber, DevExpress.Data.ColumnSortOrder.Ascending)})
        '
        'colID
        '
        Me.colID.Caption = "ID"
        Me.colID.FieldName = "IDCommission"
        Me.colID.Name = "colID"
        Me.colID.Visible = True
        Me.colID.VisibleIndex = 0
        Me.colID.Width = 94
        '
        'colPayeNumber
        '
        Me.colPayeNumber.Caption = "Numéro"
        Me.colPayeNumber.FieldName = "CommissionNumber"
        Me.colPayeNumber.Name = "colPayeNumber"
        Me.colPayeNumber.Visible = True
        Me.colPayeNumber.VisibleIndex = 2
        Me.colPayeNumber.Width = 94
        '
        'colPayeDate
        '
        Me.colPayeDate.Caption = "Date"
        Me.colPayeDate.FieldName = "CommissionDate"
        Me.colPayeDate.Name = "colPayeDate"
        Me.colPayeDate.Visible = True
        Me.colPayeDate.VisibleIndex = 3
        Me.colPayeDate.Width = 94
        '
        'colIDLivreur
        '
        Me.colIDLivreur.Caption = "ID livreur"
        Me.colIDLivreur.FieldName = "IDLivreur"
        Me.colIDLivreur.Name = "colIDLivreur"
        '
        'colLivreur
        '
        Me.colLivreur.Caption = "Livreur"
        Me.colLivreur.FieldName = "Livreur"
        Me.colLivreur.Name = "colLivreur"
        Me.colLivreur.Visible = True
        Me.colLivreur.VisibleIndex = 1
        '
        'colDateFrom
        '
        Me.colDateFrom.Caption = "Période du"
        Me.colDateFrom.FieldName = "DateFrom"
        Me.colDateFrom.Name = "colDateFrom"
        Me.colDateFrom.Visible = True
        Me.colDateFrom.VisibleIndex = 4
        '
        'colDateTo
        '
        Me.colDateTo.Caption = "Période au"
        Me.colDateTo.FieldName = "DateTo"
        Me.colDateTo.Name = "colDateTo"
        Me.colDateTo.Visible = True
        Me.colDateTo.VisibleIndex = 5
        '
        'LayoutControlGroup11
        '
        Me.LayoutControlGroup11.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup11.GroupBordersVisible = False
        Me.LayoutControlGroup11.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem2})
        Me.LayoutControlGroup11.Name = "LayoutControlGroup11"
        Me.LayoutControlGroup11.Size = New System.Drawing.Size(1330, 481)
        Me.LayoutControlGroup11.TextVisible = False
        '
        'LayoutControlItem2
        '
        Me.LayoutControlItem2.Control = Me.grdCommission
        Me.LayoutControlItem2.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem2.Name = "LayoutControlItem2"
        Me.LayoutControlItem2.Size = New System.Drawing.Size(1310, 461)
        Me.LayoutControlItem2.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem2.TextVisible = False
        '
        'XtraTabPage2
        '
        Me.XtraTabPage2.Controls.Add(Me.LayoutControl3)
        Me.XtraTabPage2.Name = "XtraTabPage2"
        Me.XtraTabPage2.PageVisible = False
        Me.XtraTabPage2.Size = New System.Drawing.Size(1330, 481)
        Me.XtraTabPage2.Text = "Détail sur une Commission"
        '
        'LayoutControl3
        '
        Me.LayoutControl3.Controls.Add(Me.grdCommissionLine)
        Me.LayoutControl3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LayoutControl3.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControl3.Name = "LayoutControl3"
        Me.LayoutControl3.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = New System.Drawing.Rectangle(976, 223, 771, 350)
        Me.LayoutControl3.OptionsCustomizationForm.ShowPropertyGrid = True
        Me.LayoutControl3.Root = Me.LayoutControlGroup21
        Me.LayoutControl3.Size = New System.Drawing.Size(1330, 481)
        Me.LayoutControl3.TabIndex = 0
        Me.LayoutControl3.Text = "LayoutControl1"
        '
        'grdCommissionLine
        '
        Me.grdCommissionLine.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grdCommissionLine.Location = New System.Drawing.Point(12, 12)
        Me.grdCommissionLine.MainView = Me.GridView2
        Me.grdCommissionLine.Name = "grdCommissionLine"
        Me.grdCommissionLine.Size = New System.Drawing.Size(1306, 457)
        Me.grdCommissionLine.TabIndex = 4
        Me.grdCommissionLine.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView2})
        '
        'GridView2
        '
        Me.GridView2.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colIDInvoiceLine, Me.colIDPaye, Me.colPayeNumber2, Me.colDateFrom2, Me.colDateTo2, Me.colIDLivreur2, Me.colLivreur2, Me.colOrganisation, Me.colCredit, Me.colNombreGlacier, Me.colGlacier, Me.colKm, Me.colDescription, Me.colUnite, Me.colPrix, Me.colMontant, Me.colNombreGlacier2, Me.colTotalGlacier})
        Me.GridView2.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.GridView2.GridControl = Me.grdCommissionLine
        Me.GridView2.Name = "GridView2"
        Me.GridView2.OptionsBehavior.Editable = False
        Me.GridView2.OptionsCustomization.AllowGroup = False
        Me.GridView2.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridView2.OptionsSelection.UseIndicatorForSelection = False
        Me.GridView2.OptionsView.ShowAutoFilterRow = True
        Me.GridView2.OptionsView.ShowGroupPanel = False
        Me.GridView2.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.colPayeNumber2, DevExpress.Data.ColumnSortOrder.Ascending)})
        '
        'colIDInvoiceLine
        '
        Me.colIDInvoiceLine.Caption = "IDCommissionLine"
        Me.colIDInvoiceLine.FieldName = "IDCommissionLine"
        Me.colIDInvoiceLine.Name = "colIDInvoiceLine"
        Me.colIDInvoiceLine.Visible = True
        Me.colIDInvoiceLine.VisibleIndex = 15
        Me.colIDInvoiceLine.Width = 94
        '
        'colIDPaye
        '
        Me.colIDPaye.Caption = "IDCommission"
        Me.colIDPaye.FieldName = "IDCommission"
        Me.colIDPaye.Name = "colIDPaye"
        '
        'colPayeNumber2
        '
        Me.colPayeNumber2.Caption = "CommissionNumber"
        Me.colPayeNumber2.FieldName = "CommissionNumber"
        Me.colPayeNumber2.Name = "colPayeNumber2"
        Me.colPayeNumber2.Visible = True
        Me.colPayeNumber2.VisibleIndex = 0
        Me.colPayeNumber2.Width = 105
        '
        'colDateFrom2
        '
        Me.colDateFrom2.Caption = "Période du"
        Me.colDateFrom2.FieldName = "DateFrom"
        Me.colDateFrom2.Name = "colDateFrom2"
        Me.colDateFrom2.Visible = True
        Me.colDateFrom2.VisibleIndex = 1
        Me.colDateFrom2.Width = 105
        '
        'colDateTo2
        '
        Me.colDateTo2.Caption = "Période au"
        Me.colDateTo2.FieldName = "DateTo"
        Me.colDateTo2.Name = "colDateTo2"
        Me.colDateTo2.Visible = True
        Me.colDateTo2.VisibleIndex = 2
        Me.colDateTo2.Width = 84
        '
        'colIDLivreur2
        '
        Me.colIDLivreur2.Caption = "ID Livreur"
        Me.colIDLivreur2.FieldName = "IDLivreur"
        Me.colIDLivreur2.Name = "colIDLivreur2"
        '
        'colLivreur2
        '
        Me.colLivreur2.Caption = "Livreur"
        Me.colLivreur2.FieldName = "Livreur"
        Me.colLivreur2.Name = "colLivreur2"
        Me.colLivreur2.Visible = True
        Me.colLivreur2.VisibleIndex = 3
        Me.colLivreur2.Width = 84
        '
        'colOrganisation
        '
        Me.colOrganisation.Caption = "Organisation"
        Me.colOrganisation.FieldName = "Organisation"
        Me.colOrganisation.Name = "colOrganisation"
        Me.colOrganisation.Visible = True
        Me.colOrganisation.VisibleIndex = 5
        Me.colOrganisation.Width = 78
        '
        'colCredit
        '
        Me.colCredit.Caption = "Crédits"
        Me.colCredit.DisplayFormat.FormatString = "c"
        Me.colCredit.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.colCredit.FieldName = "Credit"
        Me.colCredit.Name = "colCredit"
        Me.colCredit.Visible = True
        Me.colCredit.VisibleIndex = 9
        Me.colCredit.Width = 78
        '
        'colNombreGlacier
        '
        Me.colNombreGlacier.Caption = "Nombre Glacier"
        Me.colNombreGlacier.FieldName = "NombreGlacier"
        Me.colNombreGlacier.Name = "colNombreGlacier"
        Me.colNombreGlacier.Visible = True
        Me.colNombreGlacier.VisibleIndex = 10
        Me.colNombreGlacier.Width = 78
        '
        'colGlacier
        '
        Me.colGlacier.Caption = "Total glaciers"
        Me.colGlacier.DisplayFormat.FormatString = "c"
        Me.colGlacier.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.colGlacier.FieldName = "Glacier"
        Me.colGlacier.Name = "colGlacier"
        Me.colGlacier.Visible = True
        Me.colGlacier.VisibleIndex = 11
        Me.colGlacier.Width = 78
        '
        'colKm
        '
        Me.colKm.Caption = "Km"
        Me.colKm.FieldName = "Km"
        Me.colKm.Name = "colKm"
        Me.colKm.Visible = True
        Me.colKm.VisibleIndex = 12
        Me.colKm.Width = 78
        '
        'colDescription
        '
        Me.colDescription.Caption = "Description"
        Me.colDescription.FieldName = "Description"
        Me.colDescription.Name = "colDescription"
        Me.colDescription.Visible = True
        Me.colDescription.VisibleIndex = 4
        Me.colDescription.Width = 137
        '
        'colUnite
        '
        Me.colUnite.Caption = "Unité"
        Me.colUnite.FieldName = "Unite"
        Me.colUnite.Name = "colUnite"
        Me.colUnite.Visible = True
        Me.colUnite.VisibleIndex = 6
        Me.colUnite.Width = 78
        '
        'colPrix
        '
        Me.colPrix.Caption = "Prix"
        Me.colPrix.DisplayFormat.FormatString = "c"
        Me.colPrix.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.colPrix.FieldName = "Prix"
        Me.colPrix.Name = "colPrix"
        Me.colPrix.Visible = True
        Me.colPrix.VisibleIndex = 7
        Me.colPrix.Width = 78
        '
        'colMontant
        '
        Me.colMontant.Caption = "Montant"
        Me.colMontant.DisplayFormat.FormatString = "c"
        Me.colMontant.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.colMontant.FieldName = "Montant"
        Me.colMontant.Name = "colMontant"
        Me.colMontant.Visible = True
        Me.colMontant.VisibleIndex = 8
        Me.colMontant.Width = 78
        '
        'colNombreGlacier2
        '
        Me.colNombreGlacier2.Caption = "Nombre Glacier"
        Me.colNombreGlacier2.FieldName = "NombreGlacier"
        Me.colNombreGlacier2.Name = "colNombreGlacier2"
        Me.colNombreGlacier2.Visible = True
        Me.colNombreGlacier2.VisibleIndex = 13
        Me.colNombreGlacier2.Width = 78
        '
        'colTotalGlacier
        '
        Me.colTotalGlacier.Caption = "Total Glacier"
        Me.colTotalGlacier.DisplayFormat.FormatString = "c"
        Me.colTotalGlacier.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.colTotalGlacier.FieldName = "Glacier"
        Me.colTotalGlacier.Name = "colTotalGlacier"
        Me.colTotalGlacier.Visible = True
        Me.colTotalGlacier.VisibleIndex = 14
        Me.colTotalGlacier.Width = 90
        '
        'LayoutControlGroup21
        '
        Me.LayoutControlGroup21.CustomizationFormText = "LayoutControlGroup1"
        Me.LayoutControlGroup21.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup21.GroupBordersVisible = False
        Me.LayoutControlGroup21.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem11})
        Me.LayoutControlGroup21.Name = "LayoutControlGroup21"
        Me.LayoutControlGroup21.Size = New System.Drawing.Size(1330, 481)
        Me.LayoutControlGroup21.TextVisible = False
        '
        'LayoutControlItem11
        '
        Me.LayoutControlItem11.Control = Me.grdCommissionLine
        Me.LayoutControlItem11.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem11.Name = "LayoutControlItem11"
        Me.LayoutControlItem11.Size = New System.Drawing.Size(1310, 461)
        Me.LayoutControlItem11.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem11.TextVisible = False
        '
        'Root
        '
        Me.Root.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.Root.GroupBordersVisible = False
        Me.Root.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.Root1})
        Me.Root.Name = "Root"
        Me.Root.Size = New System.Drawing.Size(1379, 551)
        Me.Root.TextVisible = False
        '
        'Root1
        '
        Me.Root1.CustomizationFormText = "Root"
        Me.Root1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.Root1.GroupBordersVisible = False
        Me.Root1.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem3})
        Me.Root1.Location = New System.Drawing.Point(0, 0)
        Me.Root1.Name = "Root1"
        Me.Root1.Size = New System.Drawing.Size(1359, 531)
        Me.Root1.Spacing = New DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0)
        Me.Root1.Text = "Root"
        Me.Root1.TextVisible = False
        '
        'LayoutControlItem3
        '
        Me.LayoutControlItem3.Control = Me.tabData
        Me.LayoutControlItem3.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem3.CustomizationFormText = "LayoutControlItem3"
        Me.LayoutControlItem3.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem3.Name = "LayoutControlItem3"
        Me.LayoutControlItem3.Size = New System.Drawing.Size(1339, 511)
        Me.LayoutControlItem3.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem3.TextVisible = False
        '
        'DockManager1
        '
        Me.DockManager1.Form = Me
        Me.DockManager1.RootPanels.AddRange(New DevExpress.XtraBars.Docking.DockPanel() {Me.DockCommission, Me.DockNouvelleCommission})
        Me.DockManager1.TopZIndexControls.AddRange(New String() {"DevExpress.XtraBars.BarDockControl", "DevExpress.XtraBars.StandaloneBarDockControl", "System.Windows.Forms.MenuStrip", "System.Windows.Forms.StatusStrip", "System.Windows.Forms.StatusBar", "DevExpress.XtraBars.Ribbon.RibbonStatusBar", "DevExpress.XtraBars.Ribbon.RibbonControl", "DevExpress.XtraBars.Navigation.OfficeNavigationBar", "DevExpress.XtraBars.Navigation.TileNavPane", "DevExpress.XtraBars.TabFormControl", "DevExpress.XtraBars.FluentDesignSystem.FluentDesignFormControl", "DevExpress.XtraBars.ToolbarForm.ToolbarFormControl"})
        '
        'DockCommission
        '
        Me.DockCommission.Controls.Add(Me.DockPanel2_Container)
        Me.DockCommission.Dock = DevExpress.XtraBars.Docking.DockingStyle.Float
        Me.DockCommission.FloatLocation = New System.Drawing.Point(523, 323)
        Me.DockCommission.FloatSize = New System.Drawing.Size(659, 470)
        Me.DockCommission.ID = New System.Guid("451d6da1-7e4c-466e-8054-850a72eb6f75")
        Me.DockCommission.Location = New System.Drawing.Point(0, 0)
        Me.DockCommission.Name = "DockCommission"
        Me.DockCommission.OriginalSize = New System.Drawing.Size(592, 200)
        Me.DockCommission.SavedDock = DevExpress.XtraBars.Docking.DockingStyle.Left
        Me.DockCommission.SavedIndex = 0
        Me.DockCommission.Size = New System.Drawing.Size(659, 470)
        Me.DockCommission.Text = "Nouvelle Ligne de commission"
        '
        'DockPanel2_Container
        '
        Me.DockPanel2_Container.Controls.Add(Me.LayoutControl4)
        Me.DockPanel2_Container.Location = New System.Drawing.Point(4, 30)
        Me.DockPanel2_Container.Name = "DockPanel2_Container"
        Me.DockPanel2_Container.Size = New System.Drawing.Size(651, 436)
        Me.DockPanel2_Container.TabIndex = 0
        '
        'LayoutControl4
        '
        Me.LayoutControl4.Controls.Add(Me.bSauvegarde)
        Me.LayoutControl4.Controls.Add(Me.btnAnnuler)
        Me.LayoutControl4.Controls.Add(Me.tbIDCommissionLine)
        Me.LayoutControl4.Controls.Add(Me.tbIDCommission)
        Me.LayoutControl4.Controls.Add(Me.tbCommissionNumber)
        Me.LayoutControl4.Controls.Add(Me.tbCommissionDate)
        Me.LayoutControl4.Controls.Add(Me.cbPeriodes)
        Me.LayoutControl4.Controls.Add(Me.tbDateFrom)
        Me.LayoutControl4.Controls.Add(Me.tbDateAu)
        Me.LayoutControl4.Controls.Add(Me.cbLivreur)
        Me.LayoutControl4.Controls.Add(Me.cbOrganisation)
        Me.LayoutControl4.Controls.Add(Me.tbDescription)
        Me.LayoutControl4.Controls.Add(Me.tbQuantite)
        Me.LayoutControl4.Controls.Add(Me.tbPrix)
        Me.LayoutControl4.Controls.Add(Me.tbTotal)
        Me.LayoutControl4.Controls.Add(Me.radChoice)
        Me.LayoutControl4.Controls.Add(Me.chkAjout)
        Me.LayoutControl4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LayoutControl4.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControl4.Name = "LayoutControl4"
        Me.LayoutControl4.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = New System.Drawing.Rectangle(1122, 462, 650, 400)
        Me.LayoutControl4.Root = Me.LayoutControlGroup2
        Me.LayoutControl4.Size = New System.Drawing.Size(651, 436)
        Me.LayoutControl4.TabIndex = 0
        Me.LayoutControl4.Text = "LayoutControl4"
        '
        'bSauvegarde
        '
        Me.bSauvegarde.Location = New System.Drawing.Point(22, 392)
        Me.bSauvegarde.Name = "bSauvegarde"
        Me.bSauvegarde.Size = New System.Drawing.Size(291, 22)
        Me.bSauvegarde.StyleController = Me.LayoutControl4
        Me.bSauvegarde.TabIndex = 134
        Me.bSauvegarde.Text = "Sauvegarder"
        '
        'btnAnnuler
        '
        Me.btnAnnuler.Location = New System.Drawing.Point(317, 392)
        Me.btnAnnuler.Name = "btnAnnuler"
        Me.btnAnnuler.Size = New System.Drawing.Size(312, 22)
        Me.btnAnnuler.StyleController = Me.LayoutControl4
        Me.btnAnnuler.TabIndex = 135
        Me.btnAnnuler.Text = "Fermer"
        '
        'tbIDCommissionLine
        '
        Me.tbIDCommissionLine.EditValue = "0"
        Me.tbIDCommissionLine.Enabled = False
        Me.tbIDCommissionLine.Location = New System.Drawing.Point(122, 22)
        Me.tbIDCommissionLine.Name = "tbIDCommissionLine"
        Me.tbIDCommissionLine.Properties.Appearance.BackColor = System.Drawing.Color.Linen
        Me.tbIDCommissionLine.Properties.Appearance.Options.UseBackColor = True
        Me.tbIDCommissionLine.Properties.Mask.EditMask = "n0"
        Me.tbIDCommissionLine.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.tbIDCommissionLine.Size = New System.Drawing.Size(314, 20)
        Me.tbIDCommissionLine.StyleController = Me.LayoutControl4
        Me.tbIDCommissionLine.TabIndex = 132
        '
        'tbIDCommission
        '
        Me.tbIDCommission.EditValue = "0"
        Me.tbIDCommission.Enabled = False
        Me.tbIDCommission.Location = New System.Drawing.Point(122, 46)
        Me.tbIDCommission.Name = "tbIDCommission"
        Me.tbIDCommission.Properties.Appearance.BackColor = System.Drawing.Color.Linen
        Me.tbIDCommission.Properties.Appearance.Options.UseBackColor = True
        Me.tbIDCommission.Properties.Mask.EditMask = "n0"
        Me.tbIDCommission.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.tbIDCommission.Size = New System.Drawing.Size(314, 20)
        Me.tbIDCommission.StyleController = Me.LayoutControl4
        Me.tbIDCommission.TabIndex = 132
        '
        'tbCommissionNumber
        '
        Me.tbCommissionNumber.Location = New System.Drawing.Point(122, 70)
        Me.tbCommissionNumber.Name = "tbCommissionNumber"
        Me.tbCommissionNumber.Size = New System.Drawing.Size(314, 20)
        Me.tbCommissionNumber.StyleController = Me.LayoutControl4
        Me.tbCommissionNumber.TabIndex = 4
        '
        'tbCommissionDate
        '
        Me.tbCommissionDate.EditValue = Nothing
        Me.tbCommissionDate.Location = New System.Drawing.Point(122, 94)
        Me.tbCommissionDate.Name = "tbCommissionDate"
        Me.tbCommissionDate.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.tbCommissionDate.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.tbCommissionDate.Properties.DisplayFormat.FormatString = "MM-dd-yy"
        Me.tbCommissionDate.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.tbCommissionDate.Properties.EditFormat.FormatString = "MM-dd-yy"
        Me.tbCommissionDate.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.tbCommissionDate.Properties.Mask.EditMask = "MM-dd-yy"
        Me.tbCommissionDate.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.tbCommissionDate.Size = New System.Drawing.Size(314, 20)
        Me.tbCommissionDate.StyleController = Me.LayoutControl4
        Me.tbCommissionDate.TabIndex = 22
        '
        'cbPeriodes
        '
        Me.cbPeriodes.Location = New System.Drawing.Point(122, 118)
        Me.cbPeriodes.Name = "cbPeriodes"
        Me.cbPeriodes.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbPeriodes.Properties.Appearance.Options.UseFont = True
        Me.cbPeriodes.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbPeriodes.Size = New System.Drawing.Size(314, 22)
        Me.cbPeriodes.StyleController = Me.LayoutControl4
        Me.cbPeriodes.TabIndex = 28
        '
        'tbDateFrom
        '
        Me.tbDateFrom.EditValue = Nothing
        Me.tbDateFrom.Location = New System.Drawing.Point(122, 144)
        Me.tbDateFrom.Name = "tbDateFrom"
        Me.tbDateFrom.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.tbDateFrom.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.tbDateFrom.Properties.DisplayFormat.FormatString = "MM-dd-yy H:mm"
        Me.tbDateFrom.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.tbDateFrom.Properties.EditFormat.FormatString = "MM-dd-yy H:mm"
        Me.tbDateFrom.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.tbDateFrom.Properties.Mask.EditMask = "MM-dd-yy H:mm"
        Me.tbDateFrom.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.tbDateFrom.Size = New System.Drawing.Size(314, 20)
        Me.tbDateFrom.StyleController = Me.LayoutControl4
        Me.tbDateFrom.TabIndex = 22
        '
        'tbDateAu
        '
        Me.tbDateAu.EditValue = Nothing
        Me.tbDateAu.Location = New System.Drawing.Point(122, 168)
        Me.tbDateAu.Name = "tbDateAu"
        Me.tbDateAu.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.tbDateAu.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.tbDateAu.Properties.DisplayFormat.FormatString = "MM-dd-yy H:mm"
        Me.tbDateAu.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.tbDateAu.Properties.EditFormat.FormatString = "MM-dd-yy H:mm"
        Me.tbDateAu.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.tbDateAu.Properties.Mask.EditMask = "MM-dd-yy H:mm"
        Me.tbDateAu.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.tbDateAu.Size = New System.Drawing.Size(314, 20)
        Me.tbDateAu.StyleController = Me.LayoutControl4
        Me.tbDateAu.TabIndex = 22
        '
        'cbLivreur
        '
        Me.cbLivreur.Location = New System.Drawing.Point(122, 192)
        Me.cbLivreur.Name = "cbLivreur"
        Me.cbLivreur.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbLivreur.Size = New System.Drawing.Size(314, 20)
        Me.cbLivreur.StyleController = Me.LayoutControl4
        Me.cbLivreur.TabIndex = 30
        '
        'cbOrganisation
        '
        Me.cbOrganisation.Location = New System.Drawing.Point(122, 216)
        Me.cbOrganisation.Name = "cbOrganisation"
        Me.cbOrganisation.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbOrganisation.Size = New System.Drawing.Size(314, 20)
        Me.cbOrganisation.StyleController = Me.LayoutControl4
        Me.cbOrganisation.TabIndex = 36
        '
        'tbDescription
        '
        Me.tbDescription.Location = New System.Drawing.Point(122, 240)
        Me.tbDescription.Name = "tbDescription"
        Me.tbDescription.Size = New System.Drawing.Size(314, 20)
        Me.tbDescription.StyleController = Me.LayoutControl4
        Me.tbDescription.TabIndex = 4
        '
        'tbQuantite
        '
        Me.tbQuantite.EditValue = "1"
        Me.tbQuantite.Location = New System.Drawing.Point(122, 264)
        Me.tbQuantite.Name = "tbQuantite"
        Me.tbQuantite.Properties.Mask.EditMask = "n0"
        Me.tbQuantite.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.tbQuantite.Size = New System.Drawing.Size(314, 20)
        Me.tbQuantite.StyleController = Me.LayoutControl4
        Me.tbQuantite.TabIndex = 132
        '
        'tbPrix
        '
        Me.tbPrix.EditValue = "0"
        Me.tbPrix.Location = New System.Drawing.Point(122, 288)
        Me.tbPrix.Name = "tbPrix"
        Me.tbPrix.Properties.DisplayFormat.FormatString = "c"
        Me.tbPrix.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.tbPrix.Properties.EditFormat.FormatString = "c"
        Me.tbPrix.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.tbPrix.Properties.Mask.EditMask = "c"
        Me.tbPrix.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.tbPrix.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.tbPrix.Size = New System.Drawing.Size(314, 20)
        Me.tbPrix.StyleController = Me.LayoutControl4
        Me.tbPrix.TabIndex = 128
        '
        'tbTotal
        '
        Me.tbTotal.EditValue = "0"
        Me.tbTotal.Location = New System.Drawing.Point(122, 312)
        Me.tbTotal.Name = "tbTotal"
        Me.tbTotal.Properties.DisplayFormat.FormatString = "c"
        Me.tbTotal.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.tbTotal.Properties.EditFormat.FormatString = "c"
        Me.tbTotal.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.tbTotal.Properties.Mask.EditMask = "c"
        Me.tbTotal.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.tbTotal.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.tbTotal.Size = New System.Drawing.Size(314, 20)
        Me.tbTotal.StyleController = Me.LayoutControl4
        Me.tbTotal.TabIndex = 128
        '
        'radChoice
        '
        Me.radChoice.Location = New System.Drawing.Point(440, 38)
        Me.radChoice.Name = "radChoice"
        Me.radChoice.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "Ajouter ligne de commission"), New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "Ajouter Glaciers"), New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "Ajouter crédit"), New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "Ajouter débit"), New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "Ajouter un extra")})
        Me.radChoice.Size = New System.Drawing.Size(189, 317)
        Me.radChoice.StyleController = Me.LayoutControl4
        Me.radChoice.TabIndex = 139
        '
        'chkAjout
        '
        Me.chkAjout.Location = New System.Drawing.Point(22, 336)
        Me.chkAjout.Name = "chkAjout"
        Me.chkAjout.Properties.Caption = "Ajout?"
        Me.chkAjout.Size = New System.Drawing.Size(414, 19)
        Me.chkAjout.StyleController = Me.LayoutControl4
        Me.chkAjout.TabIndex = 141
        '
        'LayoutControlGroup2
        '
        Me.LayoutControlGroup2.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup2.GroupBordersVisible = False
        Me.LayoutControlGroup2.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlGroup5})
        Me.LayoutControlGroup2.Name = "Root"
        Me.LayoutControlGroup2.Size = New System.Drawing.Size(651, 436)
        Me.LayoutControlGroup2.TextVisible = False
        '
        'LayoutControlGroup5
        '
        Me.LayoutControlGroup5.CustomizationFormText = "Root"
        Me.LayoutControlGroup5.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup5.GroupBordersVisible = False
        Me.LayoutControlGroup5.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.EmptySpaceItem1, Me.LayoutSave, Me.LayoutControlItem16, Me.LayoutNombreFree2, Me.LayoutNombreFree1, Me.LayoutControlItem10, Me.LayoutControlItem18, Me.LayoutControlItem15, Me.LayoutControlItem13, Me.LayoutControlItem14, Me.LayoutControlItem20, Me.LayoutControlItem27, Me.LayoutControlItem12, Me.lblQuantite, Me.LayoutPayeAuLivreur, Me.LayoutPayeAuLivreur1, Me.LayoutControlItem21, Me.LayoutControlItem32})
        Me.LayoutControlGroup5.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup5.Name = "LayoutControlGroup5"
        Me.LayoutControlGroup5.Size = New System.Drawing.Size(631, 416)
        Me.LayoutControlGroup5.Spacing = New DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0)
        Me.LayoutControlGroup5.Text = "Root"
        Me.LayoutControlGroup5.TextVisible = False
        '
        'EmptySpaceItem1
        '
        Me.EmptySpaceItem1.AllowHotTrack = False
        Me.EmptySpaceItem1.CustomizationFormText = "EmptySpaceItem1"
        Me.EmptySpaceItem1.Location = New System.Drawing.Point(0, 337)
        Me.EmptySpaceItem1.Name = "EmptySpaceItem1"
        Me.EmptySpaceItem1.Size = New System.Drawing.Size(611, 33)
        Me.EmptySpaceItem1.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutSave
        '
        Me.LayoutSave.Control = Me.bSauvegarde
        Me.LayoutSave.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutSave.CustomizationFormText = "LayoutSave"
        Me.LayoutSave.Location = New System.Drawing.Point(0, 370)
        Me.LayoutSave.Name = "LayoutSave"
        Me.LayoutSave.Size = New System.Drawing.Size(295, 26)
        Me.LayoutSave.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutSave.TextVisible = False
        '
        'LayoutControlItem16
        '
        Me.LayoutControlItem16.Control = Me.btnAnnuler
        Me.LayoutControlItem16.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem16.CustomizationFormText = "LayoutControlItem16"
        Me.LayoutControlItem16.Location = New System.Drawing.Point(295, 370)
        Me.LayoutControlItem16.Name = "LayoutControlItem16"
        Me.LayoutControlItem16.Size = New System.Drawing.Size(316, 26)
        Me.LayoutControlItem16.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem16.TextVisible = False
        '
        'LayoutNombreFree2
        '
        Me.LayoutNombreFree2.Control = Me.tbIDCommissionLine
        Me.LayoutNombreFree2.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutNombreFree2.CustomizationFormText = "Nombre maximum free echange (305 et 35)"
        Me.LayoutNombreFree2.Location = New System.Drawing.Point(0, 0)
        Me.LayoutNombreFree2.Name = "LayoutNombreFree2"
        Me.LayoutNombreFree2.Size = New System.Drawing.Size(418, 24)
        Me.LayoutNombreFree2.Text = "ID Ligne Commission"
        Me.LayoutNombreFree2.TextSize = New System.Drawing.Size(97, 13)
        '
        'LayoutNombreFree1
        '
        Me.LayoutNombreFree1.Control = Me.tbIDCommission
        Me.LayoutNombreFree1.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutNombreFree1.CustomizationFormText = "Nombre maximum free echange (305 et 35)"
        Me.LayoutNombreFree1.Location = New System.Drawing.Point(0, 24)
        Me.LayoutNombreFree1.Name = "LayoutNombreFree1"
        Me.LayoutNombreFree1.Size = New System.Drawing.Size(418, 24)
        Me.LayoutNombreFree1.Text = "ID Commission"
        Me.LayoutNombreFree1.TextSize = New System.Drawing.Size(97, 13)
        '
        'LayoutControlItem10
        '
        Me.LayoutControlItem10.Control = Me.tbCommissionNumber
        Me.LayoutControlItem10.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem10.CustomizationFormText = "Numéro de paye"
        Me.LayoutControlItem10.Location = New System.Drawing.Point(0, 48)
        Me.LayoutControlItem10.Name = "LayoutControlItem10"
        Me.LayoutControlItem10.Size = New System.Drawing.Size(418, 24)
        Me.LayoutControlItem10.Text = "Numéro de paye"
        Me.LayoutControlItem10.TextSize = New System.Drawing.Size(97, 13)
        '
        'LayoutControlItem18
        '
        Me.LayoutControlItem18.Control = Me.tbCommissionDate
        Me.LayoutControlItem18.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem18.CustomizationFormText = "Date du"
        Me.LayoutControlItem18.Location = New System.Drawing.Point(0, 72)
        Me.LayoutControlItem18.Name = "LayoutControlItem18"
        Me.LayoutControlItem18.Size = New System.Drawing.Size(418, 24)
        Me.LayoutControlItem18.Text = "Date paie"
        Me.LayoutControlItem18.TextSize = New System.Drawing.Size(97, 13)
        '
        'LayoutControlItem15
        '
        Me.LayoutControlItem15.Control = Me.cbPeriodes
        Me.LayoutControlItem15.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem15.CustomizationFormText = "Périodes"
        Me.LayoutControlItem15.Location = New System.Drawing.Point(0, 96)
        Me.LayoutControlItem15.Name = "LayoutControlItem15"
        Me.LayoutControlItem15.Size = New System.Drawing.Size(418, 26)
        Me.LayoutControlItem15.Text = "Période"
        Me.LayoutControlItem15.TextSize = New System.Drawing.Size(97, 13)
        '
        'LayoutControlItem13
        '
        Me.LayoutControlItem13.Control = Me.tbDateFrom
        Me.LayoutControlItem13.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem13.CustomizationFormText = "Date du"
        Me.LayoutControlItem13.Location = New System.Drawing.Point(0, 122)
        Me.LayoutControlItem13.Name = "LayoutControlItem13"
        Me.LayoutControlItem13.Size = New System.Drawing.Size(418, 24)
        Me.LayoutControlItem13.Text = "Date du"
        Me.LayoutControlItem13.TextSize = New System.Drawing.Size(97, 13)
        '
        'LayoutControlItem14
        '
        Me.LayoutControlItem14.Control = Me.tbDateAu
        Me.LayoutControlItem14.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem14.CustomizationFormText = "Date du"
        Me.LayoutControlItem14.Location = New System.Drawing.Point(0, 146)
        Me.LayoutControlItem14.Name = "LayoutControlItem14"
        Me.LayoutControlItem14.Size = New System.Drawing.Size(418, 24)
        Me.LayoutControlItem14.Text = "Date au"
        Me.LayoutControlItem14.TextSize = New System.Drawing.Size(97, 13)
        '
        'LayoutControlItem20
        '
        Me.LayoutControlItem20.Control = Me.cbLivreur
        Me.LayoutControlItem20.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem20.CustomizationFormText = "Liste des payes"
        Me.LayoutControlItem20.Location = New System.Drawing.Point(0, 170)
        Me.LayoutControlItem20.Name = "LayoutControlItem20"
        Me.LayoutControlItem20.Size = New System.Drawing.Size(418, 24)
        Me.LayoutControlItem20.Text = "Livreur"
        Me.LayoutControlItem20.TextSize = New System.Drawing.Size(97, 13)
        '
        'LayoutControlItem27
        '
        Me.LayoutControlItem27.Control = Me.cbOrganisation
        Me.LayoutControlItem27.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem27.CustomizationFormText = "Liste des factures"
        Me.LayoutControlItem27.Location = New System.Drawing.Point(0, 194)
        Me.LayoutControlItem27.Name = "LayoutControlItem27"
        Me.LayoutControlItem27.Size = New System.Drawing.Size(418, 24)
        Me.LayoutControlItem27.Text = "Organisation"
        Me.LayoutControlItem27.TextSize = New System.Drawing.Size(97, 13)
        '
        'LayoutControlItem12
        '
        Me.LayoutControlItem12.Control = Me.tbDescription
        Me.LayoutControlItem12.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem12.CustomizationFormText = "LayoutControlItem10"
        Me.LayoutControlItem12.Location = New System.Drawing.Point(0, 218)
        Me.LayoutControlItem12.Name = "LayoutControlItem12"
        Me.LayoutControlItem12.Size = New System.Drawing.Size(418, 24)
        Me.LayoutControlItem12.Text = "Description"
        Me.LayoutControlItem12.TextSize = New System.Drawing.Size(97, 13)
        '
        'lblQuantite
        '
        Me.lblQuantite.Control = Me.tbQuantite
        Me.lblQuantite.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.lblQuantite.CustomizationFormText = "Nombre maximum free echange (305 et 35)"
        Me.lblQuantite.Location = New System.Drawing.Point(0, 242)
        Me.lblQuantite.Name = "lblQuantite"
        Me.lblQuantite.Size = New System.Drawing.Size(418, 24)
        Me.lblQuantite.Text = "Quantité"
        Me.lblQuantite.TextSize = New System.Drawing.Size(97, 13)
        '
        'LayoutPayeAuLivreur
        '
        Me.LayoutPayeAuLivreur.Control = Me.tbPrix
        Me.LayoutPayeAuLivreur.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutPayeAuLivreur.CustomizationFormText = "Payé au livreur"
        Me.LayoutPayeAuLivreur.Location = New System.Drawing.Point(0, 266)
        Me.LayoutPayeAuLivreur.Name = "LayoutPayeAuLivreur"
        Me.LayoutPayeAuLivreur.Size = New System.Drawing.Size(418, 24)
        Me.LayoutPayeAuLivreur.Text = "Prix"
        Me.LayoutPayeAuLivreur.TextSize = New System.Drawing.Size(97, 13)
        '
        'LayoutPayeAuLivreur1
        '
        Me.LayoutPayeAuLivreur1.Control = Me.tbTotal
        Me.LayoutPayeAuLivreur1.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutPayeAuLivreur1.CustomizationFormText = "Payé au livreur"
        Me.LayoutPayeAuLivreur1.Location = New System.Drawing.Point(0, 290)
        Me.LayoutPayeAuLivreur1.Name = "LayoutPayeAuLivreur1"
        Me.LayoutPayeAuLivreur1.Size = New System.Drawing.Size(418, 24)
        Me.LayoutPayeAuLivreur1.Text = "Total"
        Me.LayoutPayeAuLivreur1.TextSize = New System.Drawing.Size(97, 13)
        '
        'LayoutControlItem21
        '
        Me.LayoutControlItem21.Control = Me.radChoice
        Me.LayoutControlItem21.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem21.CustomizationFormText = "Type d'ajout"
        Me.LayoutControlItem21.Location = New System.Drawing.Point(418, 0)
        Me.LayoutControlItem21.Name = "LayoutControlItem21"
        Me.LayoutControlItem21.Size = New System.Drawing.Size(193, 337)
        Me.LayoutControlItem21.Text = "Type d'ajout"
        Me.LayoutControlItem21.TextLocation = DevExpress.Utils.Locations.Top
        Me.LayoutControlItem21.TextSize = New System.Drawing.Size(97, 13)
        '
        'LayoutControlItem32
        '
        Me.LayoutControlItem32.Control = Me.chkAjout
        Me.LayoutControlItem32.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem32.CustomizationFormText = "LayoutControlItem32"
        Me.LayoutControlItem32.Location = New System.Drawing.Point(0, 314)
        Me.LayoutControlItem32.Name = "LayoutControlItem32"
        Me.LayoutControlItem32.Size = New System.Drawing.Size(418, 23)
        Me.LayoutControlItem32.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem32.TextVisible = False
        '
        'DockNouvelleCommission
        '
        Me.DockNouvelleCommission.Controls.Add(Me.DockPanel1_Container)
        Me.DockNouvelleCommission.Dock = DevExpress.XtraBars.Docking.DockingStyle.Float
        Me.DockNouvelleCommission.FloatLocation = New System.Drawing.Point(405, 462)
        Me.DockNouvelleCommission.FloatSize = New System.Drawing.Size(487, 287)
        Me.DockNouvelleCommission.ID = New System.Guid("2f6d2db2-664f-4c4e-bd56-e8f529d5e896")
        Me.DockNouvelleCommission.Location = New System.Drawing.Point(0, 0)
        Me.DockNouvelleCommission.Name = "DockNouvelleCommission"
        Me.DockNouvelleCommission.OriginalSize = New System.Drawing.Size(399, 200)
        Me.DockNouvelleCommission.Size = New System.Drawing.Size(487, 287)
        Me.DockNouvelleCommission.Text = "Nouvelle Commission"
        '
        'DockPanel1_Container
        '
        Me.DockPanel1_Container.Controls.Add(Me.LayoutControl2)
        Me.DockPanel1_Container.Location = New System.Drawing.Point(4, 30)
        Me.DockPanel1_Container.Name = "DockPanel1_Container"
        Me.DockPanel1_Container.Size = New System.Drawing.Size(479, 253)
        Me.DockPanel1_Container.TabIndex = 0
        '
        'LayoutControl2
        '
        Me.LayoutControl2.Controls.Add(Me.tbCommissionNumberNP)
        Me.LayoutControl2.Controls.Add(Me.cbLivreurNP)
        Me.LayoutControl2.Controls.Add(Me.tbDateFromNP)
        Me.LayoutControl2.Controls.Add(Me.tbDateAuNP)
        Me.LayoutControl2.Controls.Add(Me.cbPeriodesNP)
        Me.LayoutControl2.Controls.Add(Me.tbIDCommissionNP)
        Me.LayoutControl2.Controls.Add(Me.bSauvegarderNouvelleCommission)
        Me.LayoutControl2.Controls.Add(Me.bAnnulerNouvelleCommission)
        Me.LayoutControl2.Controls.Add(Me.tbCommissionDateNP)
        Me.LayoutControl2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LayoutControl2.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControl2.Name = "LayoutControl2"
        Me.LayoutControl2.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = New System.Drawing.Rectangle(445, 414, 650, 400)
        Me.LayoutControl2.Root = Me.LayoutControlGroup1
        Me.LayoutControl2.Size = New System.Drawing.Size(479, 253)
        Me.LayoutControl2.TabIndex = 0
        Me.LayoutControl2.Text = "LayoutControl2"
        '
        'tbCommissionNumberNP
        '
        Me.tbCommissionNumberNP.Location = New System.Drawing.Point(104, 144)
        Me.tbCommissionNumberNP.Name = "tbCommissionNumberNP"
        Me.tbCommissionNumberNP.Size = New System.Drawing.Size(353, 20)
        Me.tbCommissionNumberNP.StyleController = Me.LayoutControl2
        Me.tbCommissionNumberNP.TabIndex = 4
        '
        'cbLivreurNP
        '
        Me.cbLivreurNP.Location = New System.Drawing.Point(104, 168)
        Me.cbLivreurNP.Name = "cbLivreurNP"
        Me.cbLivreurNP.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbLivreurNP.Size = New System.Drawing.Size(353, 20)
        Me.cbLivreurNP.StyleController = Me.LayoutControl2
        Me.cbLivreurNP.TabIndex = 30
        '
        'tbDateFromNP
        '
        Me.tbDateFromNP.EditValue = Nothing
        Me.tbDateFromNP.Location = New System.Drawing.Point(104, 96)
        Me.tbDateFromNP.Name = "tbDateFromNP"
        Me.tbDateFromNP.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.tbDateFromNP.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.tbDateFromNP.Properties.DisplayFormat.FormatString = "MM-dd-yy H:mm"
        Me.tbDateFromNP.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.tbDateFromNP.Properties.EditFormat.FormatString = "MM-dd-yy H:mm"
        Me.tbDateFromNP.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.tbDateFromNP.Properties.Mask.EditMask = "MM-dd-yy H:mm"
        Me.tbDateFromNP.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.tbDateFromNP.Size = New System.Drawing.Size(353, 20)
        Me.tbDateFromNP.StyleController = Me.LayoutControl2
        Me.tbDateFromNP.TabIndex = 22
        '
        'tbDateAuNP
        '
        Me.tbDateAuNP.EditValue = Nothing
        Me.tbDateAuNP.Location = New System.Drawing.Point(104, 120)
        Me.tbDateAuNP.Name = "tbDateAuNP"
        Me.tbDateAuNP.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.tbDateAuNP.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.tbDateAuNP.Properties.DisplayFormat.FormatString = "MM-dd-yy H:mm"
        Me.tbDateAuNP.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.tbDateAuNP.Properties.EditFormat.FormatString = "MM-dd-yy H:mm"
        Me.tbDateAuNP.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.tbDateAuNP.Properties.Mask.EditMask = "MM-dd-yy H:mm"
        Me.tbDateAuNP.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.tbDateAuNP.Size = New System.Drawing.Size(353, 20)
        Me.tbDateAuNP.StyleController = Me.LayoutControl2
        Me.tbDateAuNP.TabIndex = 22
        '
        'cbPeriodesNP
        '
        Me.cbPeriodesNP.Location = New System.Drawing.Point(104, 70)
        Me.cbPeriodesNP.Name = "cbPeriodesNP"
        Me.cbPeriodesNP.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbPeriodesNP.Properties.Appearance.Options.UseFont = True
        Me.cbPeriodesNP.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbPeriodesNP.Size = New System.Drawing.Size(353, 22)
        Me.cbPeriodesNP.StyleController = Me.LayoutControl2
        Me.cbPeriodesNP.TabIndex = 28
        '
        'tbIDCommissionNP
        '
        Me.tbIDCommissionNP.EditValue = "0"
        Me.tbIDCommissionNP.Enabled = False
        Me.tbIDCommissionNP.Location = New System.Drawing.Point(104, 22)
        Me.tbIDCommissionNP.Name = "tbIDCommissionNP"
        Me.tbIDCommissionNP.Properties.Appearance.BackColor = System.Drawing.Color.Linen
        Me.tbIDCommissionNP.Properties.Appearance.Options.UseBackColor = True
        Me.tbIDCommissionNP.Properties.Mask.EditMask = "n0"
        Me.tbIDCommissionNP.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.tbIDCommissionNP.Size = New System.Drawing.Size(353, 20)
        Me.tbIDCommissionNP.StyleController = Me.LayoutControl2
        Me.tbIDCommissionNP.TabIndex = 132
        '
        'bSauvegarderNouvelleCommission
        '
        Me.bSauvegarderNouvelleCommission.Location = New System.Drawing.Point(22, 192)
        Me.bSauvegarderNouvelleCommission.Name = "bSauvegarderNouvelleCommission"
        Me.bSauvegarderNouvelleCommission.Size = New System.Drawing.Size(207, 22)
        Me.bSauvegarderNouvelleCommission.StyleController = Me.LayoutControl2
        Me.bSauvegarderNouvelleCommission.TabIndex = 136
        Me.bSauvegarderNouvelleCommission.Text = "Sauvegarder"
        '
        'bAnnulerNouvelleCommission
        '
        Me.bAnnulerNouvelleCommission.Location = New System.Drawing.Point(233, 192)
        Me.bAnnulerNouvelleCommission.Name = "bAnnulerNouvelleCommission"
        Me.bAnnulerNouvelleCommission.Size = New System.Drawing.Size(224, 22)
        Me.bAnnulerNouvelleCommission.StyleController = Me.LayoutControl2
        Me.bAnnulerNouvelleCommission.TabIndex = 137
        Me.bAnnulerNouvelleCommission.Text = "Annuler"
        '
        'tbCommissionDateNP
        '
        Me.tbCommissionDateNP.EditValue = Nothing
        Me.tbCommissionDateNP.Location = New System.Drawing.Point(104, 46)
        Me.tbCommissionDateNP.Name = "tbCommissionDateNP"
        Me.tbCommissionDateNP.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.tbCommissionDateNP.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.tbCommissionDateNP.Properties.DisplayFormat.FormatString = "MM-dd-yy"
        Me.tbCommissionDateNP.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.tbCommissionDateNP.Properties.EditFormat.FormatString = "MM-dd-yy"
        Me.tbCommissionDateNP.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.tbCommissionDateNP.Properties.Mask.EditMask = "MM-dd-yy"
        Me.tbCommissionDateNP.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.tbCommissionDateNP.Size = New System.Drawing.Size(353, 20)
        Me.tbCommissionDateNP.StyleController = Me.LayoutControl2
        Me.tbCommissionDateNP.TabIndex = 22
        '
        'LayoutControlGroup1
        '
        Me.LayoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup1.GroupBordersVisible = False
        Me.LayoutControlGroup1.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlGroup3})
        Me.LayoutControlGroup1.Name = "Root"
        Me.LayoutControlGroup1.Size = New System.Drawing.Size(479, 253)
        Me.LayoutControlGroup1.TextVisible = False
        '
        'LayoutControlGroup3
        '
        Me.LayoutControlGroup3.CustomizationFormText = "Root"
        Me.LayoutControlGroup3.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup3.GroupBordersVisible = False
        Me.LayoutControlGroup3.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem23, Me.LayoutControlItem24, Me.LayoutControlItem25, Me.LayoutControlItem26, Me.LayoutControlItem28, Me.LayoutControlItem29, Me.LayoutControlItem30, Me.LayoutControlItem31, Me.LayoutControlItem1})
        Me.LayoutControlGroup3.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup3.Name = "LayoutControlGroup3"
        Me.LayoutControlGroup3.Size = New System.Drawing.Size(459, 233)
        Me.LayoutControlGroup3.Spacing = New DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0)
        Me.LayoutControlGroup3.Text = "Root"
        Me.LayoutControlGroup3.TextVisible = False
        '
        'LayoutControlItem23
        '
        Me.LayoutControlItem23.Control = Me.tbCommissionNumberNP
        Me.LayoutControlItem23.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem23.CustomizationFormText = "Numéro de paye"
        Me.LayoutControlItem23.Location = New System.Drawing.Point(0, 122)
        Me.LayoutControlItem23.Name = "LayoutControlItem23"
        Me.LayoutControlItem23.Size = New System.Drawing.Size(439, 24)
        Me.LayoutControlItem23.Text = "Numéro de paye"
        Me.LayoutControlItem23.TextSize = New System.Drawing.Size(79, 13)
        '
        'LayoutControlItem24
        '
        Me.LayoutControlItem24.Control = Me.cbLivreurNP
        Me.LayoutControlItem24.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem24.CustomizationFormText = "Liste des payes"
        Me.LayoutControlItem24.Location = New System.Drawing.Point(0, 146)
        Me.LayoutControlItem24.Name = "LayoutControlItem24"
        Me.LayoutControlItem24.Size = New System.Drawing.Size(439, 24)
        Me.LayoutControlItem24.Text = "Livreur"
        Me.LayoutControlItem24.TextSize = New System.Drawing.Size(79, 13)
        '
        'LayoutControlItem25
        '
        Me.LayoutControlItem25.Control = Me.tbDateFromNP
        Me.LayoutControlItem25.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem25.CustomizationFormText = "Date du"
        Me.LayoutControlItem25.Location = New System.Drawing.Point(0, 74)
        Me.LayoutControlItem25.Name = "LayoutControlItem25"
        Me.LayoutControlItem25.Size = New System.Drawing.Size(439, 24)
        Me.LayoutControlItem25.Text = "Date du"
        Me.LayoutControlItem25.TextSize = New System.Drawing.Size(79, 13)
        '
        'LayoutControlItem26
        '
        Me.LayoutControlItem26.Control = Me.tbDateAuNP
        Me.LayoutControlItem26.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem26.CustomizationFormText = "Date du"
        Me.LayoutControlItem26.Location = New System.Drawing.Point(0, 98)
        Me.LayoutControlItem26.Name = "LayoutControlItem26"
        Me.LayoutControlItem26.Size = New System.Drawing.Size(439, 24)
        Me.LayoutControlItem26.Text = "Date au"
        Me.LayoutControlItem26.TextSize = New System.Drawing.Size(79, 13)
        '
        'LayoutControlItem28
        '
        Me.LayoutControlItem28.Control = Me.cbPeriodesNP
        Me.LayoutControlItem28.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem28.CustomizationFormText = "Périodes"
        Me.LayoutControlItem28.Location = New System.Drawing.Point(0, 48)
        Me.LayoutControlItem28.Name = "LayoutControlItem28"
        Me.LayoutControlItem28.Size = New System.Drawing.Size(439, 26)
        Me.LayoutControlItem28.Text = "Période"
        Me.LayoutControlItem28.TextSize = New System.Drawing.Size(79, 13)
        '
        'LayoutControlItem29
        '
        Me.LayoutControlItem29.Control = Me.tbIDCommissionNP
        Me.LayoutControlItem29.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem29.CustomizationFormText = "Nombre maximum free echange (305 et 35)"
        Me.LayoutControlItem29.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem29.Name = "LayoutControlItem29"
        Me.LayoutControlItem29.Size = New System.Drawing.Size(439, 24)
        Me.LayoutControlItem29.Text = "ID Paie"
        Me.LayoutControlItem29.TextSize = New System.Drawing.Size(79, 13)
        '
        'LayoutControlItem30
        '
        Me.LayoutControlItem30.Control = Me.bSauvegarderNouvelleCommission
        Me.LayoutControlItem30.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem30.CustomizationFormText = "LayoutControlItem30"
        Me.LayoutControlItem30.Location = New System.Drawing.Point(0, 170)
        Me.LayoutControlItem30.Name = "LayoutControlItem30"
        Me.LayoutControlItem30.Size = New System.Drawing.Size(211, 43)
        Me.LayoutControlItem30.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem30.TextVisible = False
        '
        'LayoutControlItem31
        '
        Me.LayoutControlItem31.Control = Me.bAnnulerNouvelleCommission
        Me.LayoutControlItem31.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem31.CustomizationFormText = "LayoutControlItem31"
        Me.LayoutControlItem31.Location = New System.Drawing.Point(211, 170)
        Me.LayoutControlItem31.Name = "LayoutControlItem31"
        Me.LayoutControlItem31.Size = New System.Drawing.Size(228, 43)
        Me.LayoutControlItem31.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem31.TextVisible = False
        '
        'LayoutControlItem1
        '
        Me.LayoutControlItem1.Control = Me.tbCommissionDateNP
        Me.LayoutControlItem1.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem1.CustomizationFormText = "Date du"
        Me.LayoutControlItem1.Location = New System.Drawing.Point(0, 24)
        Me.LayoutControlItem1.Name = "LayoutControlItem1"
        Me.LayoutControlItem1.Size = New System.Drawing.Size(439, 24)
        Me.LayoutControlItem1.Text = "Date paie"
        Me.LayoutControlItem1.TextSize = New System.Drawing.Size(79, 13)
        '
        'frmCommission
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1379, 590)
        Me.Controls.Add(Me.LayoutControl1)
        Me.Controls.Add(Me.TS)
        Me.Name = "frmCommission"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Commissions"
        Me.TS.ResumeLayout(False)
        Me.TS.PerformLayout()
        CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LayoutControl1.ResumeLayout(False)
        CType(Me.tabData, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabData.ResumeLayout(False)
        Me.XtraTabPage11.ResumeLayout(False)
        CType(Me.LayoutControl21, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LayoutControl21.ResumeLayout(False)
        CType(Me.grdCommission, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabPage2.ResumeLayout(False)
        CType(Me.LayoutControl3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LayoutControl3.ResumeLayout(False)
        CType(Me.grdCommissionLine, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup21, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Root, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Root1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DockManager1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.DockCommission.ResumeLayout(False)
        Me.DockPanel2_Container.ResumeLayout(False)
        CType(Me.LayoutControl4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LayoutControl4.ResumeLayout(False)
        CType(Me.tbIDCommissionLine.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbIDCommission.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbCommissionNumber.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbCommissionDate.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbCommissionDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbPeriodes.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbDateFrom.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbDateFrom.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbDateAu.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbDateAu.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbLivreur.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbOrganisation.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbDescription.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbQuantite.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbPrix.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbTotal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radChoice.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkAjout.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutSave, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem16, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutNombreFree2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutNombreFree1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem18, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem15, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem13, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem14, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem20, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem27, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblQuantite, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutPayeAuLivreur, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutPayeAuLivreur1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem21, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem32, System.ComponentModel.ISupportInitialize).EndInit()
        Me.DockNouvelleCommission.ResumeLayout(False)
        Me.DockPanel1_Container.ResumeLayout(False)
        CType(Me.LayoutControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LayoutControl2.ResumeLayout(False)
        CType(Me.tbCommissionNumberNP.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbLivreurNP.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbDateFromNP.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbDateFromNP.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbDateAuNP.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbDateAuNP.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbPeriodesNP.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbIDCommissionNP.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbCommissionDateNP.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbCommissionDateNP.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem23, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem24, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem25, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem26, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem28, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem29, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem30, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem31, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents TS As ToolStrip
    Friend WithEvents Head As ToolStripLabel
    Friend WithEvents sp1 As ToolStripSeparator
    Friend WithEvents bDelete As ToolStripButton
    Friend WithEvents bNouvelleCommission As ToolStripButton
    Friend WithEvents sp3 As ToolStripSeparator
    Friend WithEvents bAjouterLaCommission As ToolStripButton
    Friend WithEvents ToolStripSeparator2 As ToolStripSeparator
    Friend WithEvents bCancel As ToolStripButton
    Friend WithEvents sp4 As ToolStripSeparator
    Friend WithEvents ToolStripButton6 As ToolStripButton
    Friend WithEvents LayoutControl1 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents tabData As DevExpress.XtraTab.XtraTabControl
    Friend WithEvents XtraTabPage11 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents LayoutControl21 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents grdCommission As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView11 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colID As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colPayeNumber As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colPayeDate As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colIDLivreur As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colLivreur As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colDateFrom As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colDateTo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents LayoutControlGroup11 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem2 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents XtraTabPage2 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents LayoutControl3 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents grdCommissionLine As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView2 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colIDInvoiceLine As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colIDPaye As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colPayeNumber2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colDateFrom2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colDateTo2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colIDLivreur2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colLivreur2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colOrganisation As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCredit As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colNombreGlacier As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colGlacier As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colKm As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colDescription As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colUnite As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colPrix As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colMontant As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colNombreGlacier2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colTotalGlacier As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents LayoutControlGroup21 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem11 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents Root As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents Root1 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem3 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents DockManager1 As DevExpress.XtraBars.Docking.DockManager
    Friend WithEvents DockNouvelleCommission As DevExpress.XtraBars.Docking.DockPanel
    Friend WithEvents DockPanel1_Container As DevExpress.XtraBars.Docking.ControlContainer
    Friend WithEvents DockCommission As DevExpress.XtraBars.Docking.DockPanel
    Friend WithEvents DockPanel2_Container As DevExpress.XtraBars.Docking.ControlContainer
    Friend WithEvents LayoutControl2 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents tbCommissionNumberNP As DevExpress.XtraEditors.TextEdit
    Friend WithEvents cbLivreurNP As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents tbDateFromNP As DevExpress.XtraEditors.DateEdit
    Friend WithEvents tbDateAuNP As DevExpress.XtraEditors.DateEdit
    Friend WithEvents cbPeriodesNP As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents tbIDCommissionNP As DevExpress.XtraEditors.TextEdit
    Friend WithEvents bSauvegarderNouvelleCommission As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents bAnnulerNouvelleCommission As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents tbCommissionDateNP As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LayoutControlGroup1 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlGroup3 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem23 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem24 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem25 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem26 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem28 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem29 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem30 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem31 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem1 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControl4 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents bSauvegarde As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnAnnuler As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents tbIDCommissionLine As DevExpress.XtraEditors.TextEdit
    Friend WithEvents tbIDCommission As DevExpress.XtraEditors.TextEdit
    Friend WithEvents tbCommissionNumber As DevExpress.XtraEditors.TextEdit
    Friend WithEvents tbCommissionDate As DevExpress.XtraEditors.DateEdit
    Friend WithEvents cbPeriodes As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents tbDateFrom As DevExpress.XtraEditors.DateEdit
    Friend WithEvents tbDateAu As DevExpress.XtraEditors.DateEdit
    Friend WithEvents cbLivreur As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents cbOrganisation As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents tbDescription As DevExpress.XtraEditors.TextEdit
    Friend WithEvents tbQuantite As DevExpress.XtraEditors.TextEdit
    Friend WithEvents tbPrix As DevExpress.XtraEditors.TextEdit
    Friend WithEvents tbTotal As DevExpress.XtraEditors.TextEdit
    Friend WithEvents radChoice As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents chkAjout As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LayoutControlGroup2 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlGroup5 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents EmptySpaceItem1 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents LayoutSave As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem16 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutNombreFree2 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutNombreFree1 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem10 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem18 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem15 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem13 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem14 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem20 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem27 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem12 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents lblQuantite As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutPayeAuLivreur As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutPayeAuLivreur1 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem21 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem32 As DevExpress.XtraLayout.LayoutControlItem
End Class
