﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmOrganisations
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmOrganisations))
        Me.TS = New System.Windows.Forms.ToolStrip()
        Me.Head = New System.Windows.Forms.ToolStripLabel()
        Me.bAdd = New System.Windows.Forms.ToolStripButton()
        Me.sp1 = New System.Windows.Forms.ToolStripSeparator()
        Me.bEdit = New System.Windows.Forms.ToolStripButton()
        Me.sp2 = New System.Windows.Forms.ToolStripSeparator()
        Me.bDelete = New System.Windows.Forms.ToolStripButton()
        Me.sp3 = New System.Windows.Forms.ToolStripSeparator()
        Me.bCancel = New System.Windows.Forms.ToolStripButton()
        Me.sp4 = New System.Windows.Forms.ToolStripSeparator()
        Me.bRefresh = New System.Windows.Forms.ToolStripButton()
        Me.sp5 = New System.Windows.Forms.ToolStripSeparator()
        Me.ToolStripButton6 = New System.Windows.Forms.ToolStripButton()
        Me.tabData = New DevExpress.XtraTab.XtraTabControl()
        Me.tabParheure = New DevExpress.XtraTab.XtraTabPage()
        Me.LayoutControl3 = New DevExpress.XtraLayout.LayoutControl()
        Me.tbHeureDepartH = New DevExpress.XtraEditors.DateEdit()
        Me.grdParHeure = New DevExpress.XtraGrid.GridControl()
        Me.GridView3 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.IDHoraire = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.HeureDepart = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.HeureFin = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn30 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.IDOrganisation = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.IDLivreur = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.Livreur = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.MontantOrganisation = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.bAnnulerLivreur = New DevExpress.XtraEditors.SimpleButton()
        Me.cbLivreursH = New DevExpress.XtraEditors.LookUpEdit()
        Me.bMiseAJourChargeParHeure = New DevExpress.XtraEditors.SimpleButton()
        Me.bSupprimerChargeParHeure = New DevExpress.XtraEditors.SimpleButton()
        Me.bAjouterChargeParHeure = New DevExpress.XtraEditors.SimpleButton()
        Me.tbMontantParHeureLivreurH = New DevExpress.XtraEditors.TextEdit()
        Me.tbIDHoraire = New DevExpress.XtraEditors.TextEdit()
        Me.tbMontantParHeureOrganisationH = New DevExpress.XtraEditors.TextEdit()
        Me.tbHeureFinH = New DevExpress.XtraEditors.DateEdit()
        Me.LayoutControlGroup2 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlGroup4 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.EmptySpaceItem1 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlItem19 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem21 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem50 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem55 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem35 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem17 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlGroup14 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem18 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem77 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem72 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem85 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem16 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem4 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.XtraTabPage1 = New DevExpress.XtraTab.XtraTabPage()
        Me.grdOrganisations = New DevExpress.XtraGrid.GridControl()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn8 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn9 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn10 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn11 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn12 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.XtraTabPage2 = New DevExpress.XtraTab.XtraTabPage()
        Me.LayoutControl1 = New DevExpress.XtraLayout.LayoutControl()
        Me.tbCommissionVendeur = New DevExpress.XtraEditors.TextEdit()
        Me.chkIsCommission = New DevExpress.XtraEditors.CheckEdit()
        Me.chkIsEtatDeCompte = New DevExpress.XtraEditors.CheckEdit()
        Me.cbVendeur = New DevExpress.XtraEditors.LookUpEdit()
        Me.butClose = New DevExpress.XtraEditors.SimpleButton()
        Me.butCancel = New DevExpress.XtraEditors.SimpleButton()
        Me.butApply = New DevExpress.XtraEditors.SimpleButton()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.chkRestaurant = New DevExpress.XtraEditors.CheckEdit()
        Me.bAnnulerVendeur = New DevExpress.XtraEditors.SimpleButton()
        Me.chkCacheEmail4 = New DevExpress.XtraEditors.CheckEdit()
        Me.chkCacheEmail3 = New DevExpress.XtraEditors.CheckEdit()
        Me.chkCacheEmail2 = New DevExpress.XtraEditors.CheckEdit()
        Me.chkCacheEmail1 = New DevExpress.XtraEditors.CheckEdit()
        Me.tbNombreFree30535 = New DevExpress.XtraEditors.TextEdit()
        Me.optCalculationOrganisations = New DevExpress.XtraEditors.RadioGroup()
        Me.tbPayeLivreur = New DevExpress.XtraEditors.TextEdit()
        Me.tbMontantContrat = New DevExpress.XtraEditors.TextEdit()
        Me.tbTelephone = New DevExpress.XtraEditors.TextEdit()
        Me.tbAdresse = New DevExpress.XtraEditors.MemoEdit()
        Me.tbOrganisation = New DevExpress.XtraEditors.TextEdit()
        Me.nudOrganisationID = New DevExpress.XtraEditors.TextEdit()
        Me.tbCreditParLivraison = New DevExpress.XtraEditors.TextEdit()
        Me.tbQuantiteLivraison = New DevExpress.XtraEditors.TextEdit()
        Me.tbMontantGlacierOrganisation = New DevExpress.XtraEditors.TextEdit()
        Me.tbMontantGlacierLivreur = New DevExpress.XtraEditors.TextEdit()
        Me.tbCourriel1 = New DevExpress.XtraEditors.TextEdit()
        Me.tbCourriel2 = New DevExpress.XtraEditors.TextEdit()
        Me.tbCourriel3 = New DevExpress.XtraEditors.TextEdit()
        Me.optCalculationLivreur = New DevExpress.XtraEditors.RadioGroup()
        Me.tbOrganisationImport = New DevExpress.XtraEditors.TextEdit()
        Me.tbMontantMobilus = New DevExpress.XtraEditors.TextEdit()
        Me.tbCourriel4 = New DevExpress.XtraEditors.TextEdit()
        Me.tbMontantFixe = New DevExpress.XtraEditors.TextEdit()
        Me.tbMessageFacture = New DevExpress.XtraEditors.MemoEdit()
        Me.tbMessageEtat = New DevExpress.XtraEditors.MemoEdit()
        Me.tbMaximumLivraison = New DevExpress.XtraEditors.TextEdit()
        Me.tbMontantMaxLivraisoneExtra = New DevExpress.XtraEditors.TextEdit()
        Me.tbMontantPrixFixeSem = New DevExpress.XtraEditors.TextEdit()
        Me.tbMontantParPorteOrganisation = New DevExpress.XtraEditors.TextEdit()
        Me.tbMontantParPorteLivreur = New DevExpress.XtraEditors.TextEdit()
        Me.LayoutControlGroup1 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem5 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem8 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutPaymentInfo = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutContrat = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutMontantContrat = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutPayeAuLivreur = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem15 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutMontantParPorteO = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutMontantParPorteL = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutMontantSpeciaux = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutNombreFree30535 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutMontantMobilus = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutGlaciers = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem39 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem40 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutCredit = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem12 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem11 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutPrixFixeExtra = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutMontantContrat3 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem21 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutMaximumLivraison = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutMontantContrat2 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutPrixFixe = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutMontantContrat1 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem78 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutCalculationLivreur = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem30 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem6 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem11 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.EmptySpaceItem12 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlItem80 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem81 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem42 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem43 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem70 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem68 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem69 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem71 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem41 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem10 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutCommission = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutVendeurs = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem73 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem20 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem67 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem82 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem83 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem84 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem13 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.EmptySpaceItem14 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlGroup13 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem4 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem65 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem100 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem102 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem103 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem104 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.TabInfo = New DevExpress.XtraTab.XtraTabPage()
        Me.LayoutControl2 = New DevExpress.XtraLayout.LayoutControl()
        Me.tbHeureDepart = New DevExpress.XtraEditors.TimeEdit()
        Me.bMiseaJourChargesParKm = New DevExpress.XtraEditors.SimpleButton()
        Me.bSupprimerChargesParKm = New DevExpress.XtraEditors.SimpleButton()
        Me.bAjouterChargesParKm = New DevExpress.XtraEditors.SimpleButton()
        Me.grdParKM = New DevExpress.XtraGrid.GridControl()
        Me.GridView2 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colIDCommissionKM = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colOrganisation2 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn3 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn4 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.Montant = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn6 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn7 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colHeureDepart = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colHeureFin = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.tbKMDebut = New DevExpress.XtraEditors.TextEdit()
        Me.tbKMFin = New DevExpress.XtraEditors.TextEdit()
        Me.tbMontantCharge = New DevExpress.XtraEditors.TextEdit()
        Me.tbChargeExtra = New DevExpress.XtraEditors.TextEdit()
        Me.tbIDCommissionKM = New DevExpress.XtraEditors.TextEdit()
        Me.tbHeureFin = New DevExpress.XtraEditors.TimeEdit()
        Me.tbChargeExtraParNbKm = New DevExpress.XtraEditors.TextEdit()
        Me.Root = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem22 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlGroup5 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutKMDebut = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutKMFin = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem13 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutChargesSupplementaires = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutChargeExtra = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutParNombreKm = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem28 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem27 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem2 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.EmptySpaceItem3 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutIDCommissionKM = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem49 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutParHeures = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem14 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem8 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlItem25 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.tabLivreurs = New DevExpress.XtraTab.XtraTabPage()
        Me.LayoutControl4 = New DevExpress.XtraLayout.LayoutControl()
        Me.bMiseAJourChargesParKmCL = New DevExpress.XtraEditors.SimpleButton()
        Me.grdParKMCL = New DevExpress.XtraGrid.GridControl()
        Me.GridView5 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn2 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn17 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn18 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn19 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn20 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn21 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn22 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn23 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.bSupprimerChargesParKmCL = New DevExpress.XtraEditors.SimpleButton()
        Me.bAjouterChargesParKmCL = New DevExpress.XtraEditors.SimpleButton()
        Me.tbMontantChargeCL = New DevExpress.XtraEditors.TextEdit()
        Me.tbChargeExtraCL = New DevExpress.XtraEditors.TextEdit()
        Me.tbIDCommissionKMCL = New DevExpress.XtraEditors.TextEdit()
        Me.tbKMDebutCL = New DevExpress.XtraEditors.TextEdit()
        Me.tbKMFinCL = New DevExpress.XtraEditors.TextEdit()
        Me.tbHeureDepartCL = New DevExpress.XtraEditors.TimeEdit()
        Me.tbHeureFinCL = New DevExpress.XtraEditors.TimeEdit()
        Me.tbChargeExtraParNbKmCL = New DevExpress.XtraEditors.TextEdit()
        Me.LayoutControlGroup7 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlGroup8 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem33 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem32 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem6 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlItem7 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutChargesSupplementairesCL = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem9 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutParNombreKmCL = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem5 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlItem1 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem51 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutParHeuresCL = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem2 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem3 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem15 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem29 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem26 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.tabMaxPayable = New DevExpress.XtraTab.XtraTabPage()
        Me.LayoutControl5 = New DevExpress.XtraLayout.LayoutControl()
        Me.bMiseAJourMP = New DevExpress.XtraEditors.SimpleButton()
        Me.bSupprimerMP = New DevExpress.XtraEditors.SimpleButton()
        Me.bAjouterMP = New DevExpress.XtraEditors.SimpleButton()
        Me.grdMaxPayable = New DevExpress.XtraGrid.GridControl()
        Me.GridView4 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colIDMaxPayable = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colNombreLivraison = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colMaxPayable = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.tbIDMaxPayable = New DevExpress.XtraEditors.TextEdit()
        Me.tbMaximumPayable = New DevExpress.XtraEditors.TextEdit()
        Me.tbNombreLivraisons = New DevExpress.XtraEditors.TextEdit()
        Me.LayoutControlGroup9 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem45 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlGroup10 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem46 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem48 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem47 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem54 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem53 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem52 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem7 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.XtraTabPage3 = New DevExpress.XtraTab.XtraTabPage()
        Me.LayoutControl6 = New DevExpress.XtraLayout.LayoutControl()
        Me.bMAJCadeauO = New DevExpress.XtraEditors.SimpleButton()
        Me.bSupprimerCadeauO = New DevExpress.XtraEditors.SimpleButton()
        Me.bAjouterCadeauO = New DevExpress.XtraEditors.SimpleButton()
        Me.grdCadeauOrganisation = New DevExpress.XtraGrid.GridControl()
        Me.GridView6 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn13 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn14 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn26 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn15 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn16 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn24 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.tbMontantCadeauO = New DevExpress.XtraEditors.TextEdit()
        Me.tbDateFromCadeauO = New DevExpress.XtraEditors.DateEdit()
        Me.tbDateToCadeauO = New DevExpress.XtraEditors.DateEdit()
        Me.tbIIDCadeauOrganisation = New DevExpress.XtraEditors.TextEdit()
        Me.LayoutControlGroup3 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem23 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlGroup11 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem31 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem59 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem10 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlItem61 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem60 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem36 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem37 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem63 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.XtraTabPage4 = New DevExpress.XtraTab.XtraTabPage()
        Me.LayoutControl7 = New DevExpress.XtraLayout.LayoutControl()
        Me.bAnnulerLivreurL = New DevExpress.XtraEditors.SimpleButton()
        Me.cbLivreursCadeauL = New DevExpress.XtraEditors.LookUpEdit()
        Me.bMAJCadeauL = New DevExpress.XtraEditors.SimpleButton()
        Me.bSupprimerCadeauL = New DevExpress.XtraEditors.SimpleButton()
        Me.bAjouterCadeauL = New DevExpress.XtraEditors.SimpleButton()
        Me.grdCadeauLivreur = New DevExpress.XtraGrid.GridControl()
        Me.GridView7 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colIDCadeauLivreur = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colIDLivreur = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn25 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colDateFrom = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colDateTo = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.Montant1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.tbMontantCadeauL = New DevExpress.XtraEditors.TextEdit()
        Me.tbDateFromCadeauL = New DevExpress.XtraEditors.DateEdit()
        Me.tbDateToCadeauL = New DevExpress.XtraEditors.DateEdit()
        Me.tbIDCadeauLivreur = New DevExpress.XtraEditors.TextEdit()
        Me.LayoutControlGroup6 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem24 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlGroup12 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem34 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem38 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem44 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem56 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem58 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem57 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem9 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlItem62 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem64 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem66 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.XtraTabPage5 = New DevExpress.XtraTab.XtraTabPage()
        Me.LayoutControl8 = New DevExpress.XtraLayout.LayoutControl()
        Me.bAjouterPrixFixe = New DevExpress.XtraEditors.SimpleButton()
        Me.bSupprimerPrixFixe = New DevExpress.XtraEditors.SimpleButton()
        Me.bMiseAJourPrixFixe = New DevExpress.XtraEditors.SimpleButton()
        Me.tbIDPrixFixe = New DevExpress.XtraEditors.TextEdit()
        Me.cbLivreursP = New DevExpress.XtraEditors.LookUpEdit()
        Me.bAnnulerLivreurP = New DevExpress.XtraEditors.SimpleButton()
        Me.tbMontantPrixFixe = New DevExpress.XtraEditors.TextEdit()
        Me.grdPrixFixe = New DevExpress.XtraGrid.GridControl()
        Me.GridView31 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.IDHoraire1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn301 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.IDOrganisation1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.IDLivreur1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.Livreur1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.IDJourSemaine1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.LayoutControlGroup16 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlGroup18 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.EmptySpaceItem16 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlItem74 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem75 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem76 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem86 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem87 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem88 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlGroup19 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem89 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem93 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem17 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.XtraTabPage6 = New DevExpress.XtraTab.XtraTabPage()
        Me.LayoutControl9 = New DevExpress.XtraLayout.LayoutControl()
        Me.bAjouterChargeParJour = New DevExpress.XtraEditors.SimpleButton()
        Me.bSupprimerChargeParJour = New DevExpress.XtraEditors.SimpleButton()
        Me.bMiseAJourChargeParJour = New DevExpress.XtraEditors.SimpleButton()
        Me.tbIDHoraireJ = New DevExpress.XtraEditors.TextEdit()
        Me.cbLivreursJ = New DevExpress.XtraEditors.LookUpEdit()
        Me.bAnnulerLivreurH = New DevExpress.XtraEditors.SimpleButton()
        Me.tbMontantParHeureLivreurJ = New DevExpress.XtraEditors.TextEdit()
        Me.tbMontantParHeureOrganisationJ = New DevExpress.XtraEditors.TextEdit()
        Me.tbDateJ = New DevExpress.XtraEditors.DateEdit()
        Me.grdParJour = New DevExpress.XtraGrid.GridControl()
        Me.GridView32 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.IDHoraireJour = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.DateTravail = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn302 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.IDOrganisation2 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.IDLivreur2 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.Livreur2 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.MontantOrganisation1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.LayoutControlGroup17 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlGroup21 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.EmptySpaceItem18 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlItem90 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem91 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem92 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem94 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem95 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem96 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlGroup22 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem97 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem98 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem99 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem101 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem19 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutCalculationOrganisation = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.TS.SuspendLayout()
        CType(Me.tabData, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabData.SuspendLayout()
        Me.tabParheure.SuspendLayout()
        CType(Me.LayoutControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LayoutControl3.SuspendLayout()
        CType(Me.tbHeureDepartH.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbHeureDepartH.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grdParHeure, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbLivreursH.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbMontantParHeureLivreurH.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbIDHoraire.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbMontantParHeureOrganisationH.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbHeureFinH.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbHeureFinH.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem19, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem21, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem50, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem55, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem35, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem17, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup14, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem18, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem77, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem72, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem85, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem16, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabPage1.SuspendLayout()
        CType(Me.grdOrganisations, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabPage2.SuspendLayout()
        CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LayoutControl1.SuspendLayout()
        CType(Me.tbCommissionVendeur.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkIsCommission.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkIsEtatDeCompte.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbVendeur.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkRestaurant.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkCacheEmail4.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkCacheEmail3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkCacheEmail2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkCacheEmail1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbNombreFree30535.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.optCalculationOrganisations.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbPayeLivreur.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbMontantContrat.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbTelephone.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbAdresse.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbOrganisation.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudOrganisationID.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbCreditParLivraison.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbQuantiteLivraison.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbMontantGlacierOrganisation.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbMontantGlacierLivreur.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbCourriel1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbCourriel2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbCourriel3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.optCalculationLivreur.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbOrganisationImport.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbMontantMobilus.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbCourriel4.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbMontantFixe.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbMessageFacture.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbMessageEtat.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbMaximumLivraison.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbMontantMaxLivraisoneExtra.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbMontantPrixFixeSem.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbMontantParPorteOrganisation.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbMontantParPorteLivreur.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutPaymentInfo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutContrat, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutMontantContrat, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutPayeAuLivreur, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem15, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutMontantParPorteO, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutMontantParPorteL, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutMontantSpeciaux, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutNombreFree30535, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutMontantMobilus, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutGlaciers, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem39, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem40, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutCredit, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutPrixFixeExtra, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutMontantContrat3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem21, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutMaximumLivraison, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutMontantContrat2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutPrixFixe, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutMontantContrat1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem78, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutCalculationLivreur, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem30, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem80, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem81, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem42, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem43, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem70, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem68, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem69, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem71, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem41, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutCommission, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutVendeurs, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem73, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem20, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem67, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem82, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem83, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem84, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem13, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem14, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup13, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem65, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem100, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem102, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem103, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem104, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabInfo.SuspendLayout()
        CType(Me.LayoutControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LayoutControl2.SuspendLayout()
        CType(Me.tbHeureDepart.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grdParKM, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbKMDebut.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbKMFin.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbMontantCharge.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbChargeExtra.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbIDCommissionKM.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbHeureFin.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbChargeExtraParNbKm.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Root, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem22, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutKMDebut, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutKMFin, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem13, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutChargesSupplementaires, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutChargeExtra, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutParNombreKm, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem28, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem27, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutIDCommissionKM, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem49, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutParHeures, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem14, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem25, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabLivreurs.SuspendLayout()
        CType(Me.LayoutControl4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LayoutControl4.SuspendLayout()
        CType(Me.grdParKMCL, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbMontantChargeCL.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbChargeExtraCL.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbIDCommissionKMCL.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbKMDebutCL.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbKMFinCL.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbHeureDepartCL.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbHeureFinCL.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbChargeExtraParNbKmCL.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem33, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem32, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutChargesSupplementairesCL, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutParNombreKmCL, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem51, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutParHeuresCL, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem15, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem29, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem26, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabMaxPayable.SuspendLayout()
        CType(Me.LayoutControl5, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LayoutControl5.SuspendLayout()
        CType(Me.grdMaxPayable, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbIDMaxPayable.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbMaximumPayable.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbNombreLivraisons.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem45, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem46, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem48, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem47, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem54, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem53, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem52, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem7, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabPage3.SuspendLayout()
        CType(Me.LayoutControl6, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LayoutControl6.SuspendLayout()
        CType(Me.grdCadeauOrganisation, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbMontantCadeauO.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbDateFromCadeauO.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbDateFromCadeauO.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbDateToCadeauO.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbDateToCadeauO.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbIIDCadeauOrganisation.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem23, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem31, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem59, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem61, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem60, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem36, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem37, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem63, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabPage4.SuspendLayout()
        CType(Me.LayoutControl7, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LayoutControl7.SuspendLayout()
        CType(Me.cbLivreursCadeauL.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grdCadeauLivreur, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbMontantCadeauL.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbDateFromCadeauL.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbDateFromCadeauL.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbDateToCadeauL.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbDateToCadeauL.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbIDCadeauLivreur.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem24, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem34, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem38, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem44, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem56, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem58, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem57, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem62, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem64, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem66, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabPage5.SuspendLayout()
        CType(Me.LayoutControl8, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LayoutControl8.SuspendLayout()
        CType(Me.tbIDPrixFixe.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbLivreursP.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbMontantPrixFixe.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grdPrixFixe, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView31, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup16, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup18, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem16, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem74, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem75, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem76, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem86, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem87, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem88, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup19, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem89, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem93, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem17, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabPage6.SuspendLayout()
        CType(Me.LayoutControl9, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LayoutControl9.SuspendLayout()
        CType(Me.tbIDHoraireJ.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbLivreursJ.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbMontantParHeureLivreurJ.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbMontantParHeureOrganisationJ.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbDateJ.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbDateJ.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grdParJour, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView32, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup17, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup21, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem18, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem90, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem91, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem92, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem94, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem95, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem96, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup22, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem97, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem98, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem99, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem101, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem19, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutCalculationOrganisation, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'TS
        '
        Me.TS.AutoSize = False
        Me.TS.Font = New System.Drawing.Font("Verdana", 9.75!)
        Me.TS.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Head, Me.bAdd, Me.sp1, Me.bEdit, Me.sp2, Me.bDelete, Me.sp3, Me.bCancel, Me.sp4, Me.bRefresh, Me.sp5, Me.ToolStripButton6})
        Me.TS.Location = New System.Drawing.Point(0, 0)
        Me.TS.Name = "TS"
        Me.TS.Size = New System.Drawing.Size(1331, 39)
        Me.TS.TabIndex = 6
        '
        'Head
        '
        Me.Head.Name = "Head"
        Me.Head.Size = New System.Drawing.Size(0, 36)
        '
        'bAdd
        '
        Me.bAdd.Image = CType(resources.GetObject("bAdd.Image"), System.Drawing.Image)
        Me.bAdd.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.bAdd.Name = "bAdd"
        Me.bAdd.Size = New System.Drawing.Size(76, 36)
        Me.bAdd.Text = "Ajouter"
        '
        'sp1
        '
        Me.sp1.Name = "sp1"
        Me.sp1.Size = New System.Drawing.Size(6, 39)
        '
        'bEdit
        '
        Me.bEdit.Image = CType(resources.GetObject("bEdit.Image"), System.Drawing.Image)
        Me.bEdit.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.bEdit.Name = "bEdit"
        Me.bEdit.Size = New System.Drawing.Size(72, 36)
        Me.bEdit.Text = "Edition"
        '
        'sp2
        '
        Me.sp2.Name = "sp2"
        Me.sp2.Size = New System.Drawing.Size(6, 39)
        '
        'bDelete
        '
        Me.bDelete.Image = CType(resources.GetObject("bDelete.Image"), System.Drawing.Image)
        Me.bDelete.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.bDelete.Name = "bDelete"
        Me.bDelete.Size = New System.Drawing.Size(93, 36)
        Me.bDelete.Text = "Supprimer"
        '
        'sp3
        '
        Me.sp3.Name = "sp3"
        Me.sp3.Size = New System.Drawing.Size(6, 39)
        '
        'bCancel
        '
        Me.bCancel.Image = CType(resources.GetObject("bCancel.Image"), System.Drawing.Image)
        Me.bCancel.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.bCancel.Name = "bCancel"
        Me.bCancel.Size = New System.Drawing.Size(77, 36)
        Me.bCancel.Text = "Annuler"
        '
        'sp4
        '
        Me.sp4.Name = "sp4"
        Me.sp4.Size = New System.Drawing.Size(6, 39)
        '
        'bRefresh
        '
        Me.bRefresh.Image = CType(resources.GetObject("bRefresh.Image"), System.Drawing.Image)
        Me.bRefresh.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.bRefresh.Name = "bRefresh"
        Me.bRefresh.Size = New System.Drawing.Size(89, 36)
        Me.bRefresh.Text = "Rafraîchir"
        '
        'sp5
        '
        Me.sp5.Name = "sp5"
        Me.sp5.Size = New System.Drawing.Size(6, 39)
        '
        'ToolStripButton6
        '
        Me.ToolStripButton6.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.ToolStripButton6.Image = CType(resources.GetObject("ToolStripButton6.Image"), System.Drawing.Image)
        Me.ToolStripButton6.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton6.Name = "ToolStripButton6"
        Me.ToolStripButton6.Size = New System.Drawing.Size(73, 36)
        Me.ToolStripButton6.Text = "Fermer"
        '
        'tabData
        '
        Me.tabData.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tabData.Location = New System.Drawing.Point(0, 39)
        Me.tabData.Name = "tabData"
        Me.tabData.SelectedTabPage = Me.tabParheure
        Me.tabData.Size = New System.Drawing.Size(1331, 805)
        Me.tabData.TabIndex = 7
        Me.tabData.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.XtraTabPage1, Me.XtraTabPage2, Me.TabInfo, Me.tabParheure, Me.tabLivreurs, Me.tabMaxPayable, Me.XtraTabPage3, Me.XtraTabPage4, Me.XtraTabPage5, Me.XtraTabPage6})
        '
        'tabParheure
        '
        Me.tabParheure.Controls.Add(Me.LayoutControl3)
        Me.tabParheure.Name = "tabParheure"
        Me.tabParheure.PageVisible = False
        Me.tabParheure.Size = New System.Drawing.Size(1326, 779)
        Me.tabParheure.Text = "Par Heure"
        '
        'LayoutControl3
        '
        Me.LayoutControl3.Controls.Add(Me.tbHeureDepartH)
        Me.LayoutControl3.Controls.Add(Me.grdParHeure)
        Me.LayoutControl3.Controls.Add(Me.bAnnulerLivreur)
        Me.LayoutControl3.Controls.Add(Me.cbLivreursH)
        Me.LayoutControl3.Controls.Add(Me.bMiseAJourChargeParHeure)
        Me.LayoutControl3.Controls.Add(Me.bSupprimerChargeParHeure)
        Me.LayoutControl3.Controls.Add(Me.bAjouterChargeParHeure)
        Me.LayoutControl3.Controls.Add(Me.tbMontantParHeureLivreurH)
        Me.LayoutControl3.Controls.Add(Me.tbIDHoraire)
        Me.LayoutControl3.Controls.Add(Me.tbMontantParHeureOrganisationH)
        Me.LayoutControl3.Controls.Add(Me.tbHeureFinH)
        Me.LayoutControl3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LayoutControl3.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControl3.Name = "LayoutControl3"
        Me.LayoutControl3.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = New System.Drawing.Rectangle(994, 395, 650, 400)
        Me.LayoutControl3.Root = Me.LayoutControlGroup2
        Me.LayoutControl3.Size = New System.Drawing.Size(1326, 779)
        Me.LayoutControl3.TabIndex = 0
        Me.LayoutControl3.Text = "LayoutControl3"
        '
        'tbHeureDepartH
        '
        Me.tbHeureDepartH.EditValue = Nothing
        Me.tbHeureDepartH.Location = New System.Drawing.Point(192, 126)
        Me.tbHeureDepartH.Name = "tbHeureDepartH"
        Me.tbHeureDepartH.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.tbHeureDepartH.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.tbHeureDepartH.Properties.DisplayFormat.FormatString = "MM-dd-yy H:mm"
        Me.tbHeureDepartH.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.tbHeureDepartH.Properties.EditFormat.FormatString = "MM-dd-yy H:mm"
        Me.tbHeureDepartH.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.tbHeureDepartH.Properties.Mask.EditMask = "MM-dd-yy H:mm"
        Me.tbHeureDepartH.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.tbHeureDepartH.Size = New System.Drawing.Size(254, 20)
        Me.tbHeureDepartH.StyleController = Me.LayoutControl3
        Me.tbHeureDepartH.TabIndex = 135
        '
        'grdParHeure
        '
        Me.grdParHeure.Location = New System.Drawing.Point(24, 234)
        Me.grdParHeure.MainView = Me.GridView3
        Me.grdParHeure.Name = "grdParHeure"
        Me.grdParHeure.Size = New System.Drawing.Size(1278, 521)
        Me.grdParHeure.TabIndex = 10
        Me.grdParHeure.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView3})
        '
        'GridView3
        '
        Me.GridView3.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.IDHoraire, Me.HeureDepart, Me.HeureFin, Me.GridColumn30, Me.IDOrganisation, Me.IDLivreur, Me.Livreur, Me.MontantOrganisation})
        Me.GridView3.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.GridView3.GridControl = Me.grdParHeure
        Me.GridView3.Name = "GridView3"
        Me.GridView3.OptionsBehavior.Editable = False
        Me.GridView3.OptionsCustomization.AllowGroup = False
        Me.GridView3.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridView3.OptionsSelection.UseIndicatorForSelection = False
        Me.GridView3.OptionsView.ShowAutoFilterRow = True
        Me.GridView3.OptionsView.ShowGroupPanel = False
        '
        'IDHoraire
        '
        Me.IDHoraire.Caption = "ID Horaire"
        Me.IDHoraire.FieldName = "IDHoraire"
        Me.IDHoraire.Name = "IDHoraire"
        Me.IDHoraire.Visible = True
        Me.IDHoraire.VisibleIndex = 0
        Me.IDHoraire.Width = 94
        '
        'HeureDepart
        '
        Me.HeureDepart.Caption = "Heure départ"
        Me.HeureDepart.DisplayFormat.FormatString = "g"
        Me.HeureDepart.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.HeureDepart.FieldName = "HeureDepart"
        Me.HeureDepart.Name = "HeureDepart"
        Me.HeureDepart.Visible = True
        Me.HeureDepart.VisibleIndex = 2
        '
        'HeureFin
        '
        Me.HeureFin.Caption = "Heure Fin"
        Me.HeureFin.DisplayFormat.FormatString = "g"
        Me.HeureFin.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.HeureFin.FieldName = "HeureFin"
        Me.HeureFin.Name = "HeureFin"
        Me.HeureFin.Visible = True
        Me.HeureFin.VisibleIndex = 3
        '
        'GridColumn30
        '
        Me.GridColumn30.Caption = "Montant Livreur"
        Me.GridColumn30.DisplayFormat.FormatString = "c"
        Me.GridColumn30.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.GridColumn30.FieldName = "MontantLivreur"
        Me.GridColumn30.Name = "GridColumn30"
        Me.GridColumn30.Visible = True
        Me.GridColumn30.VisibleIndex = 4
        '
        'IDOrganisation
        '
        Me.IDOrganisation.Caption = "IDOrganisation"
        Me.IDOrganisation.FieldName = "IDOrganisation"
        Me.IDOrganisation.Name = "IDOrganisation"
        '
        'IDLivreur
        '
        Me.IDLivreur.Caption = "ID Livreur"
        Me.IDLivreur.FieldName = "IDLivreur"
        Me.IDLivreur.Name = "IDLivreur"
        '
        'Livreur
        '
        Me.Livreur.Caption = "Livreur"
        Me.Livreur.FieldName = "Livreur"
        Me.Livreur.Name = "Livreur"
        Me.Livreur.Visible = True
        Me.Livreur.VisibleIndex = 1
        '
        'MontantOrganisation
        '
        Me.MontantOrganisation.Caption = "Montant Organisation"
        Me.MontantOrganisation.DisplayFormat.FormatString = "c"
        Me.MontantOrganisation.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.MontantOrganisation.FieldName = "MontantOrganisation"
        Me.MontantOrganisation.Name = "MontantOrganisation"
        Me.MontantOrganisation.Visible = True
        Me.MontantOrganisation.VisibleIndex = 5
        '
        'bAnnulerLivreur
        '
        Me.bAnnulerLivreur.Location = New System.Drawing.Point(424, 68)
        Me.bAnnulerLivreur.Name = "bAnnulerLivreur"
        Me.bAnnulerLivreur.Size = New System.Drawing.Size(34, 22)
        Me.bAnnulerLivreur.StyleController = Me.LayoutControl3
        Me.bAnnulerLivreur.TabIndex = 134
        Me.bAnnulerLivreur.Text = "..."
        '
        'cbLivreursH
        '
        Me.cbLivreursH.Location = New System.Drawing.Point(180, 68)
        Me.cbLivreursH.Name = "cbLivreursH"
        Me.cbLivreursH.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbLivreursH.Size = New System.Drawing.Size(240, 20)
        Me.cbLivreursH.StyleController = Me.LayoutControl3
        Me.cbLivreursH.TabIndex = 133
        '
        'bMiseAJourChargeParHeure
        '
        Me.bMiseAJourChargeParHeure.Enabled = False
        Me.bMiseAJourChargeParHeure.Location = New System.Drawing.Point(724, 44)
        Me.bMiseAJourChargeParHeure.Name = "bMiseAJourChargeParHeure"
        Me.bMiseAJourChargeParHeure.Size = New System.Drawing.Size(122, 65)
        Me.bMiseAJourChargeParHeure.StyleController = Me.LayoutControl3
        Me.bMiseAJourChargeParHeure.TabIndex = 131
        Me.bMiseAJourChargeParHeure.Text = "Mise a jour"
        '
        'bSupprimerChargeParHeure
        '
        Me.bSupprimerChargeParHeure.Enabled = False
        Me.bSupprimerChargeParHeure.Location = New System.Drawing.Point(599, 44)
        Me.bSupprimerChargeParHeure.Name = "bSupprimerChargeParHeure"
        Me.bSupprimerChargeParHeure.Size = New System.Drawing.Size(121, 65)
        Me.bSupprimerChargeParHeure.StyleController = Me.LayoutControl3
        Me.bSupprimerChargeParHeure.TabIndex = 126
        Me.bSupprimerChargeParHeure.Text = "Supprimer"
        '
        'bAjouterChargeParHeure
        '
        Me.bAjouterChargeParHeure.Location = New System.Drawing.Point(462, 44)
        Me.bAjouterChargeParHeure.Name = "bAjouterChargeParHeure"
        Me.bAjouterChargeParHeure.Size = New System.Drawing.Size(133, 65)
        Me.bAjouterChargeParHeure.StyleController = Me.LayoutControl3
        Me.bAjouterChargeParHeure.TabIndex = 125
        Me.bAjouterChargeParHeure.Text = "Ajouter"
        '
        'tbMontantParHeureLivreurH
        '
        Me.tbMontantParHeureLivreurH.EditValue = "0"
        Me.tbMontantParHeureLivreurH.Location = New System.Drawing.Point(192, 174)
        Me.tbMontantParHeureLivreurH.Name = "tbMontantParHeureLivreurH"
        Me.tbMontantParHeureLivreurH.Properties.DisplayFormat.FormatString = "c"
        Me.tbMontantParHeureLivreurH.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.tbMontantParHeureLivreurH.Properties.EditFormat.FormatString = "c"
        Me.tbMontantParHeureLivreurH.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.tbMontantParHeureLivreurH.Properties.Mask.EditMask = "c"
        Me.tbMontantParHeureLivreurH.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.tbMontantParHeureLivreurH.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.tbMontantParHeureLivreurH.Size = New System.Drawing.Size(254, 20)
        Me.tbMontantParHeureLivreurH.StyleController = Me.LayoutControl3
        Me.tbMontantParHeureLivreurH.TabIndex = 124
        '
        'tbIDHoraire
        '
        Me.tbIDHoraire.EditValue = "0"
        Me.tbIDHoraire.Enabled = False
        Me.tbIDHoraire.Location = New System.Drawing.Point(180, 44)
        Me.tbIDHoraire.Name = "tbIDHoraire"
        Me.tbIDHoraire.Properties.Appearance.BackColor = System.Drawing.Color.Azure
        Me.tbIDHoraire.Properties.Appearance.Options.UseBackColor = True
        Me.tbIDHoraire.Properties.Mask.EditMask = "n0"
        Me.tbIDHoraire.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.tbIDHoraire.Size = New System.Drawing.Size(278, 20)
        Me.tbIDHoraire.StyleController = Me.LayoutControl3
        Me.tbIDHoraire.TabIndex = 126
        '
        'tbMontantParHeureOrganisationH
        '
        Me.tbMontantParHeureOrganisationH.EditValue = "0"
        Me.tbMontantParHeureOrganisationH.Location = New System.Drawing.Point(192, 198)
        Me.tbMontantParHeureOrganisationH.Name = "tbMontantParHeureOrganisationH"
        Me.tbMontantParHeureOrganisationH.Properties.DisplayFormat.FormatString = "c"
        Me.tbMontantParHeureOrganisationH.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.tbMontantParHeureOrganisationH.Properties.EditFormat.FormatString = "c"
        Me.tbMontantParHeureOrganisationH.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.tbMontantParHeureOrganisationH.Properties.Mask.EditMask = "c"
        Me.tbMontantParHeureOrganisationH.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.tbMontantParHeureOrganisationH.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.tbMontantParHeureOrganisationH.Size = New System.Drawing.Size(254, 20)
        Me.tbMontantParHeureOrganisationH.StyleController = Me.LayoutControl3
        Me.tbMontantParHeureOrganisationH.TabIndex = 124
        '
        'tbHeureFinH
        '
        Me.tbHeureFinH.EditValue = Nothing
        Me.tbHeureFinH.Location = New System.Drawing.Point(192, 150)
        Me.tbHeureFinH.Name = "tbHeureFinH"
        Me.tbHeureFinH.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.tbHeureFinH.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.tbHeureFinH.Properties.DisplayFormat.FormatString = "MM-dd-yy H:mm"
        Me.tbHeureFinH.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.tbHeureFinH.Properties.EditFormat.FormatString = "MM-dd-yy H:mm"
        Me.tbHeureFinH.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.tbHeureFinH.Properties.Mask.EditMask = "MM-dd-yy H:mm"
        Me.tbHeureFinH.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.tbHeureFinH.Size = New System.Drawing.Size(254, 20)
        Me.tbHeureFinH.StyleController = Me.LayoutControl3
        Me.tbHeureFinH.TabIndex = 135
        '
        'LayoutControlGroup2
        '
        Me.LayoutControlGroup2.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup2.GroupBordersVisible = False
        Me.LayoutControlGroup2.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlGroup4})
        Me.LayoutControlGroup2.Name = "Root"
        Me.LayoutControlGroup2.Size = New System.Drawing.Size(1326, 779)
        Me.LayoutControlGroup2.TextVisible = False
        '
        'LayoutControlGroup4
        '
        Me.LayoutControlGroup4.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.EmptySpaceItem1, Me.LayoutControlItem19, Me.LayoutControlItem21, Me.LayoutControlItem50, Me.LayoutControlItem55, Me.LayoutControlItem35, Me.LayoutControlItem17, Me.LayoutControlGroup14, Me.LayoutControlItem16, Me.EmptySpaceItem4})
        Me.LayoutControlGroup4.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup4.Name = "LayoutControlGroup4"
        Me.LayoutControlGroup4.Size = New System.Drawing.Size(1306, 759)
        Me.LayoutControlGroup4.Text = "Ajouter un Livreur a la liste"
        '
        'EmptySpaceItem1
        '
        Me.EmptySpaceItem1.AllowHotTrack = False
        Me.EmptySpaceItem1.Location = New System.Drawing.Point(826, 0)
        Me.EmptySpaceItem1.Name = "EmptySpaceItem1"
        Me.EmptySpaceItem1.Size = New System.Drawing.Size(456, 190)
        Me.EmptySpaceItem1.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlItem19
        '
        Me.LayoutControlItem19.Control = Me.bAjouterChargeParHeure
        Me.LayoutControlItem19.Location = New System.Drawing.Point(438, 0)
        Me.LayoutControlItem19.MinSize = New System.Drawing.Size(49, 26)
        Me.LayoutControlItem19.Name = "LayoutControlItem19"
        Me.LayoutControlItem19.Size = New System.Drawing.Size(137, 69)
        Me.LayoutControlItem19.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem19.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem19.TextVisible = False
        '
        'LayoutControlItem21
        '
        Me.LayoutControlItem21.Control = Me.bSupprimerChargeParHeure
        Me.LayoutControlItem21.Location = New System.Drawing.Point(575, 0)
        Me.LayoutControlItem21.MinSize = New System.Drawing.Size(61, 26)
        Me.LayoutControlItem21.Name = "LayoutControlItem21"
        Me.LayoutControlItem21.Size = New System.Drawing.Size(125, 69)
        Me.LayoutControlItem21.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem21.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem21.TextVisible = False
        '
        'LayoutControlItem50
        '
        Me.LayoutControlItem50.Control = Me.bMiseAJourChargeParHeure
        Me.LayoutControlItem50.Location = New System.Drawing.Point(700, 0)
        Me.LayoutControlItem50.MinSize = New System.Drawing.Size(65, 26)
        Me.LayoutControlItem50.Name = "LayoutControlItem50"
        Me.LayoutControlItem50.Size = New System.Drawing.Size(126, 69)
        Me.LayoutControlItem50.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem50.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem50.TextVisible = False
        '
        'LayoutControlItem55
        '
        Me.LayoutControlItem55.Control = Me.tbIDHoraire
        Me.LayoutControlItem55.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem55.CustomizationFormText = "Bonus par kilométrage démarre a"
        Me.LayoutControlItem55.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem55.Name = "LayoutControlItem55"
        Me.LayoutControlItem55.Size = New System.Drawing.Size(438, 24)
        Me.LayoutControlItem55.Text = "ID Horaire"
        Me.LayoutControlItem55.TextSize = New System.Drawing.Size(152, 13)
        '
        'LayoutControlItem35
        '
        Me.LayoutControlItem35.Control = Me.cbLivreursH
        Me.LayoutControlItem35.Location = New System.Drawing.Point(0, 24)
        Me.LayoutControlItem35.Name = "LayoutControlItem35"
        Me.LayoutControlItem35.Size = New System.Drawing.Size(400, 26)
        Me.LayoutControlItem35.Text = "Livreur"
        Me.LayoutControlItem35.TextSize = New System.Drawing.Size(152, 13)
        '
        'LayoutControlItem17
        '
        Me.LayoutControlItem17.Control = Me.bAnnulerLivreur
        Me.LayoutControlItem17.Location = New System.Drawing.Point(400, 24)
        Me.LayoutControlItem17.Name = "LayoutControlItem17"
        Me.LayoutControlItem17.Size = New System.Drawing.Size(38, 26)
        Me.LayoutControlItem17.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem17.TextVisible = False
        '
        'LayoutControlGroup14
        '
        Me.LayoutControlGroup14.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem18, Me.LayoutControlItem77, Me.LayoutControlItem72, Me.LayoutControlItem85})
        Me.LayoutControlGroup14.Location = New System.Drawing.Point(0, 50)
        Me.LayoutControlGroup14.Name = "LayoutControlGroup14"
        Me.LayoutControlGroup14.Size = New System.Drawing.Size(438, 140)
        Me.LayoutControlGroup14.Text = "Configuration Horaire"
        '
        'LayoutControlItem18
        '
        Me.LayoutControlItem18.Control = Me.tbMontantParHeureLivreurH
        Me.LayoutControlItem18.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem18.CustomizationFormText = "Bonus Amount"
        Me.LayoutControlItem18.Location = New System.Drawing.Point(0, 48)
        Me.LayoutControlItem18.Name = "LayoutControlItem18"
        Me.LayoutControlItem18.Size = New System.Drawing.Size(414, 24)
        Me.LayoutControlItem18.Text = "Montant par heure livreur"
        Me.LayoutControlItem18.TextSize = New System.Drawing.Size(152, 13)
        '
        'LayoutControlItem77
        '
        Me.LayoutControlItem77.Control = Me.tbMontantParHeureOrganisationH
        Me.LayoutControlItem77.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem77.CustomizationFormText = "Bonus Amount"
        Me.LayoutControlItem77.Location = New System.Drawing.Point(0, 72)
        Me.LayoutControlItem77.Name = "LayoutControlItem77"
        Me.LayoutControlItem77.Size = New System.Drawing.Size(414, 24)
        Me.LayoutControlItem77.Text = "Montant par heure organisation"
        Me.LayoutControlItem77.TextSize = New System.Drawing.Size(152, 13)
        '
        'LayoutControlItem72
        '
        Me.LayoutControlItem72.Control = Me.tbHeureDepartH
        Me.LayoutControlItem72.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem72.Name = "LayoutControlItem72"
        Me.LayoutControlItem72.Size = New System.Drawing.Size(414, 24)
        Me.LayoutControlItem72.Text = "Date / heure début"
        Me.LayoutControlItem72.TextSize = New System.Drawing.Size(152, 13)
        '
        'LayoutControlItem85
        '
        Me.LayoutControlItem85.Control = Me.tbHeureFinH
        Me.LayoutControlItem85.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem85.CustomizationFormText = "Date / heure fin"
        Me.LayoutControlItem85.Location = New System.Drawing.Point(0, 24)
        Me.LayoutControlItem85.Name = "LayoutControlItem85"
        Me.LayoutControlItem85.Size = New System.Drawing.Size(414, 24)
        Me.LayoutControlItem85.Text = "Date / heure fin"
        Me.LayoutControlItem85.TextSize = New System.Drawing.Size(152, 13)
        '
        'LayoutControlItem16
        '
        Me.LayoutControlItem16.Control = Me.grdParHeure
        Me.LayoutControlItem16.Location = New System.Drawing.Point(0, 190)
        Me.LayoutControlItem16.Name = "LayoutControlItem16"
        Me.LayoutControlItem16.Size = New System.Drawing.Size(1282, 525)
        Me.LayoutControlItem16.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem16.TextVisible = False
        '
        'EmptySpaceItem4
        '
        Me.EmptySpaceItem4.AllowHotTrack = False
        Me.EmptySpaceItem4.Location = New System.Drawing.Point(438, 69)
        Me.EmptySpaceItem4.Name = "EmptySpaceItem4"
        Me.EmptySpaceItem4.Size = New System.Drawing.Size(388, 121)
        Me.EmptySpaceItem4.TextSize = New System.Drawing.Size(0, 0)
        '
        'XtraTabPage1
        '
        Me.XtraTabPage1.Controls.Add(Me.grdOrganisations)
        Me.XtraTabPage1.Name = "XtraTabPage1"
        Me.XtraTabPage1.Size = New System.Drawing.Size(1326, 779)
        Me.XtraTabPage1.Text = "Liste des Organisations"
        '
        'grdOrganisations
        '
        Me.grdOrganisations.Dock = System.Windows.Forms.DockStyle.Fill
        Me.grdOrganisations.Location = New System.Drawing.Point(0, 0)
        Me.grdOrganisations.MainView = Me.GridView1
        Me.grdOrganisations.Name = "grdOrganisations"
        Me.grdOrganisations.Size = New System.Drawing.Size(1326, 779)
        Me.grdOrganisations.TabIndex = 7
        Me.grdOrganisations.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'GridView1
        '
        Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn8, Me.GridColumn9, Me.GridColumn10, Me.GridColumn11, Me.GridColumn12})
        Me.GridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.GridView1.GridControl = Me.grdOrganisations
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsBehavior.Editable = False
        Me.GridView1.OptionsCustomization.AllowGroup = False
        Me.GridView1.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridView1.OptionsSelection.UseIndicatorForSelection = False
        Me.GridView1.OptionsView.ShowAutoFilterRow = True
        Me.GridView1.OptionsView.ShowGroupPanel = False
        Me.GridView1.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.GridColumn9, DevExpress.Data.ColumnSortOrder.Ascending)})
        '
        'GridColumn8
        '
        Me.GridColumn8.Caption = "Organisation ID"
        Me.GridColumn8.FieldName = "OrganisationID"
        Me.GridColumn8.Name = "GridColumn8"
        Me.GridColumn8.Visible = True
        Me.GridColumn8.VisibleIndex = 0
        Me.GridColumn8.Width = 94
        '
        'GridColumn9
        '
        Me.GridColumn9.Caption = "Organisation"
        Me.GridColumn9.FieldName = "Organisation"
        Me.GridColumn9.Name = "GridColumn9"
        Me.GridColumn9.Visible = True
        Me.GridColumn9.VisibleIndex = 1
        Me.GridColumn9.Width = 94
        '
        'GridColumn10
        '
        Me.GridColumn10.Caption = "KM Max"
        Me.GridColumn10.FieldName = "KMMax"
        Me.GridColumn10.Name = "GridColumn10"
        Me.GridColumn10.Width = 94
        '
        'GridColumn11
        '
        Me.GridColumn11.Caption = "Montant Bonus"
        Me.GridColumn11.DisplayFormat.FormatString = "c"
        Me.GridColumn11.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.GridColumn11.FieldName = "MontantBonus"
        Me.GridColumn11.Name = "GridColumn11"
        Me.GridColumn11.Width = 119
        '
        'GridColumn12
        '
        Me.GridColumn12.Caption = "Montant Contrat"
        Me.GridColumn12.DisplayFormat.FormatString = "c"
        Me.GridColumn12.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.GridColumn12.FieldName = "MontantContrat"
        Me.GridColumn12.Name = "GridColumn12"
        '
        'XtraTabPage2
        '
        Me.XtraTabPage2.Controls.Add(Me.LayoutControl1)
        Me.XtraTabPage2.Name = "XtraTabPage2"
        Me.XtraTabPage2.PageVisible = False
        Me.XtraTabPage2.Size = New System.Drawing.Size(1326, 779)
        Me.XtraTabPage2.Text = "Détail sur un Organisation"
        '
        'LayoutControl1
        '
        Me.LayoutControl1.Controls.Add(Me.tbCommissionVendeur)
        Me.LayoutControl1.Controls.Add(Me.chkIsCommission)
        Me.LayoutControl1.Controls.Add(Me.chkIsEtatDeCompte)
        Me.LayoutControl1.Controls.Add(Me.cbVendeur)
        Me.LayoutControl1.Controls.Add(Me.butClose)
        Me.LayoutControl1.Controls.Add(Me.butCancel)
        Me.LayoutControl1.Controls.Add(Me.butApply)
        Me.LayoutControl1.Controls.Add(Me.Label2)
        Me.LayoutControl1.Controls.Add(Me.Label1)
        Me.LayoutControl1.Controls.Add(Me.chkRestaurant)
        Me.LayoutControl1.Controls.Add(Me.bAnnulerVendeur)
        Me.LayoutControl1.Controls.Add(Me.chkCacheEmail4)
        Me.LayoutControl1.Controls.Add(Me.chkCacheEmail3)
        Me.LayoutControl1.Controls.Add(Me.chkCacheEmail2)
        Me.LayoutControl1.Controls.Add(Me.chkCacheEmail1)
        Me.LayoutControl1.Controls.Add(Me.tbNombreFree30535)
        Me.LayoutControl1.Controls.Add(Me.optCalculationOrganisations)
        Me.LayoutControl1.Controls.Add(Me.tbPayeLivreur)
        Me.LayoutControl1.Controls.Add(Me.tbMontantContrat)
        Me.LayoutControl1.Controls.Add(Me.tbTelephone)
        Me.LayoutControl1.Controls.Add(Me.tbAdresse)
        Me.LayoutControl1.Controls.Add(Me.tbOrganisation)
        Me.LayoutControl1.Controls.Add(Me.nudOrganisationID)
        Me.LayoutControl1.Controls.Add(Me.tbCreditParLivraison)
        Me.LayoutControl1.Controls.Add(Me.tbQuantiteLivraison)
        Me.LayoutControl1.Controls.Add(Me.tbMontantGlacierOrganisation)
        Me.LayoutControl1.Controls.Add(Me.tbMontantGlacierLivreur)
        Me.LayoutControl1.Controls.Add(Me.tbCourriel1)
        Me.LayoutControl1.Controls.Add(Me.tbCourriel2)
        Me.LayoutControl1.Controls.Add(Me.tbCourriel3)
        Me.LayoutControl1.Controls.Add(Me.optCalculationLivreur)
        Me.LayoutControl1.Controls.Add(Me.tbOrganisationImport)
        Me.LayoutControl1.Controls.Add(Me.tbMontantMobilus)
        Me.LayoutControl1.Controls.Add(Me.tbCourriel4)
        Me.LayoutControl1.Controls.Add(Me.tbMontantFixe)
        Me.LayoutControl1.Controls.Add(Me.tbMessageFacture)
        Me.LayoutControl1.Controls.Add(Me.tbMessageEtat)
        Me.LayoutControl1.Controls.Add(Me.tbMaximumLivraison)
        Me.LayoutControl1.Controls.Add(Me.tbMontantMaxLivraisoneExtra)
        Me.LayoutControl1.Controls.Add(Me.tbMontantPrixFixeSem)
        Me.LayoutControl1.Controls.Add(Me.tbMontantParPorteOrganisation)
        Me.LayoutControl1.Controls.Add(Me.tbMontantParPorteLivreur)
        Me.LayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LayoutControl1.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControl1.Name = "LayoutControl1"
        Me.LayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = New System.Drawing.Rectangle(869, 447, 771, 351)
        Me.LayoutControl1.OptionsCustomizationForm.ShowPropertyGrid = True
        Me.LayoutControl1.Root = Me.LayoutControlGroup1
        Me.LayoutControl1.Size = New System.Drawing.Size(1326, 779)
        Me.LayoutControl1.TabIndex = 0
        Me.LayoutControl1.Text = "LayoutControl1"
        '
        'tbCommissionVendeur
        '
        Me.tbCommissionVendeur.EditValue = "0"
        Me.tbCommissionVendeur.Location = New System.Drawing.Point(236, 508)
        Me.tbCommissionVendeur.Name = "tbCommissionVendeur"
        Me.tbCommissionVendeur.Properties.DisplayFormat.FormatString = "c"
        Me.tbCommissionVendeur.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.tbCommissionVendeur.Properties.EditFormat.FormatString = "c"
        Me.tbCommissionVendeur.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.tbCommissionVendeur.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.tbCommissionVendeur.Properties.MaskSettings.Set("MaskManagerType", GetType(DevExpress.Data.Mask.NumericMaskManager))
        Me.tbCommissionVendeur.Properties.MaskSettings.Set("mask", "c")
        Me.tbCommissionVendeur.Size = New System.Drawing.Size(205, 20)
        Me.tbCommissionVendeur.StyleController = Me.LayoutControl1
        Me.tbCommissionVendeur.TabIndex = 148
        '
        'chkIsCommission
        '
        Me.chkIsCommission.Location = New System.Drawing.Point(12, 409)
        Me.chkIsCommission.Name = "chkIsCommission"
        Me.chkIsCommission.Properties.Caption = "Commissions ?"
        Me.chkIsCommission.Size = New System.Drawing.Size(441, 19)
        Me.chkIsCommission.StyleController = Me.LayoutControl1
        Me.chkIsCommission.TabIndex = 147
        '
        'chkIsEtatDeCompte
        '
        Me.chkIsEtatDeCompte.Location = New System.Drawing.Point(12, 386)
        Me.chkIsEtatDeCompte.Name = "chkIsEtatDeCompte"
        Me.chkIsEtatDeCompte.Properties.Caption = "Etat des comptes ?"
        Me.chkIsEtatDeCompte.Size = New System.Drawing.Size(441, 19)
        Me.chkIsEtatDeCompte.StyleController = Me.LayoutControl1
        Me.chkIsEtatDeCompte.TabIndex = 146
        '
        'cbVendeur
        '
        Me.cbVendeur.Location = New System.Drawing.Point(236, 482)
        Me.cbVendeur.Name = "cbVendeur"
        Me.cbVendeur.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbVendeur.Size = New System.Drawing.Size(180, 20)
        Me.cbVendeur.StyleController = Me.LayoutControl1
        Me.cbVendeur.TabIndex = 145
        '
        'butClose
        '
        Me.butClose.Location = New System.Drawing.Point(742, 12)
        Me.butClose.Name = "butClose"
        Me.butClose.Size = New System.Drawing.Size(270, 22)
        Me.butClose.StyleController = Me.LayoutControl1
        Me.butClose.TabIndex = 144
        Me.butClose.Text = "Fermer"
        '
        'butCancel
        '
        Me.butCancel.Location = New System.Drawing.Point(491, 12)
        Me.butCancel.Name = "butCancel"
        Me.butCancel.Size = New System.Drawing.Size(247, 22)
        Me.butCancel.StyleController = Me.LayoutControl1
        Me.butCancel.TabIndex = 143
        Me.butCancel.Text = "Retour a la grille"
        '
        'butApply
        '
        Me.butApply.Location = New System.Drawing.Point(242, 12)
        Me.butApply.Name = "butApply"
        Me.butApply.Size = New System.Drawing.Size(245, 22)
        Me.butApply.StyleController = Me.LayoutControl1
        Me.butApply.TabIndex = 142
        Me.butApply.Text = "Appliquer"
        '
        'Label2
        '
        Me.Label2.BackColor = System.Drawing.Color.Yellow
        Me.Label2.Location = New System.Drawing.Point(610, 576)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(295, 20)
        Me.Label2.TabIndex = 141
        Me.Label2.Text = "Livreur"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label1
        '
        Me.Label1.BackColor = System.Drawing.Color.Yellow
        Me.Label1.Location = New System.Drawing.Point(325, 576)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(281, 20)
        Me.Label1.TabIndex = 140
        Me.Label1.Text = "Restaurant"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'chkRestaurant
        '
        Me.chkRestaurant.Location = New System.Drawing.Point(12, 62)
        Me.chkRestaurant.Name = "chkRestaurant"
        Me.chkRestaurant.Properties.Caption = "Restaurant?"
        Me.chkRestaurant.Size = New System.Drawing.Size(441, 19)
        Me.chkRestaurant.StyleController = Me.LayoutControl1
        Me.chkRestaurant.TabIndex = 138
        '
        'bAnnulerVendeur
        '
        Me.bAnnulerVendeur.Location = New System.Drawing.Point(420, 482)
        Me.bAnnulerVendeur.Name = "bAnnulerVendeur"
        Me.bAnnulerVendeur.Size = New System.Drawing.Size(21, 22)
        Me.bAnnulerVendeur.StyleController = Me.LayoutControl1
        Me.bAnnulerVendeur.TabIndex = 137
        Me.bAnnulerVendeur.Text = "..."
        '
        'chkCacheEmail4
        '
        Me.chkCacheEmail4.Location = New System.Drawing.Point(428, 273)
        Me.chkCacheEmail4.Name = "chkCacheEmail4"
        Me.chkCacheEmail4.Properties.Caption = ""
        Me.chkCacheEmail4.Size = New System.Drawing.Size(25, 19)
        Me.chkCacheEmail4.StyleController = Me.LayoutControl1
        Me.chkCacheEmail4.TabIndex = 136
        '
        'chkCacheEmail3
        '
        Me.chkCacheEmail3.Location = New System.Drawing.Point(428, 249)
        Me.chkCacheEmail3.Name = "chkCacheEmail3"
        Me.chkCacheEmail3.Properties.Caption = ""
        Me.chkCacheEmail3.Size = New System.Drawing.Size(25, 19)
        Me.chkCacheEmail3.StyleController = Me.LayoutControl1
        Me.chkCacheEmail3.TabIndex = 135
        '
        'chkCacheEmail2
        '
        Me.chkCacheEmail2.Location = New System.Drawing.Point(428, 225)
        Me.chkCacheEmail2.Name = "chkCacheEmail2"
        Me.chkCacheEmail2.Properties.Caption = ""
        Me.chkCacheEmail2.Size = New System.Drawing.Size(25, 19)
        Me.chkCacheEmail2.StyleController = Me.LayoutControl1
        Me.chkCacheEmail2.TabIndex = 134
        '
        'chkCacheEmail1
        '
        Me.chkCacheEmail1.Location = New System.Drawing.Point(428, 201)
        Me.chkCacheEmail1.Name = "chkCacheEmail1"
        Me.chkCacheEmail1.Properties.Caption = ""
        Me.chkCacheEmail1.Size = New System.Drawing.Size(25, 19)
        Me.chkCacheEmail1.StyleController = Me.LayoutControl1
        Me.chkCacheEmail1.TabIndex = 133
        '
        'tbNombreFree30535
        '
        Me.tbNombreFree30535.EditValue = "0"
        Me.tbNombreFree30535.Location = New System.Drawing.Point(693, 310)
        Me.tbNombreFree30535.Name = "tbNombreFree30535"
        Me.tbNombreFree30535.Properties.Mask.EditMask = "n0"
        Me.tbNombreFree30535.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.tbNombreFree30535.Size = New System.Drawing.Size(142, 20)
        Me.tbNombreFree30535.StyleController = Me.LayoutControl1
        Me.tbNombreFree30535.TabIndex = 132
        '
        'optCalculationOrganisations
        '
        Me.optCalculationOrganisations.AutoSizeInLayoutControl = True
        Me.optCalculationOrganisations.Location = New System.Drawing.Point(325, 600)
        Me.optCalculationOrganisations.Name = "optCalculationOrganisations"
        Me.optCalculationOrganisations.Properties.ColumnIndent = 0
        Me.optCalculationOrganisations.Properties.Columns = 1
        Me.optCalculationOrganisations.Properties.FlowLayoutItemHorzIndent = 0
        Me.optCalculationOrganisations.Properties.FlowLayoutItemVertIndent = 0
        Me.optCalculationOrganisations.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "Pharmaplus / Massicote"), New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "Prix Fixe"), New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "Par Kilométrage"), New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "Par Kilométrage + Xtra"), New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "Par Kilométrage + Xtra(Heure)"), New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "Par Nombre KM fixe + extra"), New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "Par Jour"), New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "A l'heure"), New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "Prix Fixe/Sem + Extra"), New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "Par résidence")})
        Me.optCalculationOrganisations.Size = New System.Drawing.Size(281, 160)
        Me.optCalculationOrganisations.StyleController = Me.LayoutControl1
        Me.optCalculationOrganisations.TabIndex = 130
        '
        'tbPayeLivreur
        '
        Me.tbPayeLivreur.EditValue = "0"
        Me.tbPayeLivreur.Location = New System.Drawing.Point(693, 126)
        Me.tbPayeLivreur.Name = "tbPayeLivreur"
        Me.tbPayeLivreur.Properties.DisplayFormat.FormatString = "c"
        Me.tbPayeLivreur.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.tbPayeLivreur.Properties.EditFormat.FormatString = "c"
        Me.tbPayeLivreur.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.tbPayeLivreur.Properties.Mask.EditMask = "c"
        Me.tbPayeLivreur.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.tbPayeLivreur.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.tbPayeLivreur.Size = New System.Drawing.Size(55, 20)
        Me.tbPayeLivreur.StyleController = Me.LayoutControl1
        Me.tbPayeLivreur.TabIndex = 128
        '
        'tbMontantContrat
        '
        Me.tbMontantContrat.EditValue = "0"
        Me.tbMontantContrat.Location = New System.Drawing.Point(693, 102)
        Me.tbMontantContrat.Name = "tbMontantContrat"
        Me.tbMontantContrat.Properties.DisplayFormat.FormatString = "c"
        Me.tbMontantContrat.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.tbMontantContrat.Properties.EditFormat.FormatString = "c"
        Me.tbMontantContrat.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.tbMontantContrat.Properties.Mask.EditMask = "c"
        Me.tbMontantContrat.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.tbMontantContrat.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.tbMontantContrat.Size = New System.Drawing.Size(55, 20)
        Me.tbMontantContrat.StyleController = Me.LayoutControl1
        Me.tbMontantContrat.TabIndex = 127
        '
        'tbTelephone
        '
        Me.tbTelephone.Location = New System.Drawing.Point(224, 177)
        Me.tbTelephone.Name = "tbTelephone"
        Me.tbTelephone.Properties.Mask.EditMask = "(\d?\d?\d?) \d\d\d-\d\d\d\d"
        Me.tbTelephone.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Regular
        Me.tbTelephone.Properties.Mask.SaveLiteral = False
        Me.tbTelephone.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.tbTelephone.Size = New System.Drawing.Size(229, 20)
        Me.tbTelephone.StyleController = Me.LayoutControl1
        Me.tbTelephone.TabIndex = 9
        '
        'tbAdresse
        '
        Me.tbAdresse.Location = New System.Drawing.Point(12, 313)
        Me.tbAdresse.Name = "tbAdresse"
        Me.tbAdresse.Size = New System.Drawing.Size(441, 69)
        Me.tbAdresse.StyleController = Me.LayoutControl1
        Me.tbAdresse.TabIndex = 31
        '
        'tbOrganisation
        '
        Me.tbOrganisation.Location = New System.Drawing.Point(236, 117)
        Me.tbOrganisation.Name = "tbOrganisation"
        Me.tbOrganisation.Size = New System.Drawing.Size(205, 20)
        Me.tbOrganisation.StyleController = Me.LayoutControl1
        Me.tbOrganisation.TabIndex = 7
        '
        'nudOrganisationID
        '
        Me.nudOrganisationID.Enabled = False
        Me.nudOrganisationID.Location = New System.Drawing.Point(224, 38)
        Me.nudOrganisationID.Name = "nudOrganisationID"
        Me.nudOrganisationID.Properties.Appearance.BackColor = System.Drawing.Color.Azure
        Me.nudOrganisationID.Properties.Appearance.Options.UseBackColor = True
        Me.nudOrganisationID.Size = New System.Drawing.Size(229, 20)
        Me.nudOrganisationID.StyleController = Me.LayoutControl1
        Me.nudOrganisationID.TabIndex = 5
        '
        'tbCreditParLivraison
        '
        Me.tbCreditParLivraison.EditValue = "0"
        Me.tbCreditParLivraison.Location = New System.Drawing.Point(1075, 378)
        Me.tbCreditParLivraison.Name = "tbCreditParLivraison"
        Me.tbCreditParLivraison.Properties.DisplayFormat.FormatString = "c"
        Me.tbCreditParLivraison.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.tbCreditParLivraison.Properties.EditFormat.FormatString = "c"
        Me.tbCreditParLivraison.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.tbCreditParLivraison.Properties.Mask.EditMask = "c"
        Me.tbCreditParLivraison.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.tbCreditParLivraison.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.tbCreditParLivraison.Size = New System.Drawing.Size(198, 20)
        Me.tbCreditParLivraison.StyleController = Me.LayoutControl1
        Me.tbCreditParLivraison.TabIndex = 124
        '
        'tbQuantiteLivraison
        '
        Me.tbQuantiteLivraison.EditValue = "0"
        Me.tbQuantiteLivraison.Location = New System.Drawing.Point(1075, 354)
        Me.tbQuantiteLivraison.Name = "tbQuantiteLivraison"
        Me.tbQuantiteLivraison.Properties.Mask.EditMask = "n0"
        Me.tbQuantiteLivraison.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.tbQuantiteLivraison.Size = New System.Drawing.Size(198, 20)
        Me.tbQuantiteLivraison.StyleController = Me.LayoutControl1
        Me.tbQuantiteLivraison.TabIndex = 126
        '
        'tbMontantGlacierOrganisation
        '
        Me.tbMontantGlacierOrganisation.EditValue = "0"
        Me.tbMontantGlacierOrganisation.Location = New System.Drawing.Point(705, 366)
        Me.tbMontantGlacierOrganisation.Name = "tbMontantGlacierOrganisation"
        Me.tbMontantGlacierOrganisation.Properties.DisplayFormat.FormatString = "c"
        Me.tbMontantGlacierOrganisation.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.tbMontantGlacierOrganisation.Properties.EditFormat.FormatString = "c"
        Me.tbMontantGlacierOrganisation.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.tbMontantGlacierOrganisation.Properties.Mask.EditMask = "c"
        Me.tbMontantGlacierOrganisation.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.tbMontantGlacierOrganisation.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.tbMontantGlacierOrganisation.Size = New System.Drawing.Size(118, 20)
        Me.tbMontantGlacierOrganisation.StyleController = Me.LayoutControl1
        Me.tbMontantGlacierOrganisation.TabIndex = 124
        '
        'tbMontantGlacierLivreur
        '
        Me.tbMontantGlacierLivreur.EditValue = "0"
        Me.tbMontantGlacierLivreur.Location = New System.Drawing.Point(705, 390)
        Me.tbMontantGlacierLivreur.Name = "tbMontantGlacierLivreur"
        Me.tbMontantGlacierLivreur.Properties.DisplayFormat.FormatString = "c"
        Me.tbMontantGlacierLivreur.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.tbMontantGlacierLivreur.Properties.EditFormat.FormatString = "c"
        Me.tbMontantGlacierLivreur.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.tbMontantGlacierLivreur.Properties.Mask.EditMask = "c"
        Me.tbMontantGlacierLivreur.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.tbMontantGlacierLivreur.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.tbMontantGlacierLivreur.Size = New System.Drawing.Size(118, 20)
        Me.tbMontantGlacierLivreur.StyleController = Me.LayoutControl1
        Me.tbMontantGlacierLivreur.TabIndex = 124
        '
        'tbCourriel1
        '
        Me.tbCourriel1.Location = New System.Drawing.Point(224, 201)
        Me.tbCourriel1.Name = "tbCourriel1"
        Me.tbCourriel1.Properties.Mask.SaveLiteral = False
        Me.tbCourriel1.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.tbCourriel1.Size = New System.Drawing.Size(200, 20)
        Me.tbCourriel1.StyleController = Me.LayoutControl1
        Me.tbCourriel1.TabIndex = 9
        '
        'tbCourriel2
        '
        Me.tbCourriel2.Location = New System.Drawing.Point(224, 225)
        Me.tbCourriel2.Name = "tbCourriel2"
        Me.tbCourriel2.Properties.Mask.SaveLiteral = False
        Me.tbCourriel2.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.tbCourriel2.Size = New System.Drawing.Size(200, 20)
        Me.tbCourriel2.StyleController = Me.LayoutControl1
        Me.tbCourriel2.TabIndex = 9
        '
        'tbCourriel3
        '
        Me.tbCourriel3.Location = New System.Drawing.Point(224, 249)
        Me.tbCourriel3.Name = "tbCourriel3"
        Me.tbCourriel3.Properties.Mask.SaveLiteral = False
        Me.tbCourriel3.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.tbCourriel3.Size = New System.Drawing.Size(200, 20)
        Me.tbCourriel3.StyleController = Me.LayoutControl1
        Me.tbCourriel3.TabIndex = 9
        '
        'optCalculationLivreur
        '
        Me.optCalculationLivreur.Location = New System.Drawing.Point(610, 600)
        Me.optCalculationLivreur.Name = "optCalculationLivreur"
        Me.optCalculationLivreur.Properties.ColumnIndent = 0
        Me.optCalculationLivreur.Properties.Columns = 1
        Me.optCalculationLivreur.Properties.FlowLayoutItemHorzIndent = 0
        Me.optCalculationLivreur.Properties.FlowLayoutItemVertIndent = 0
        Me.optCalculationLivreur.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.[Default]
        Me.optCalculationLivreur.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "Pharmaplus / Massicote"), New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "Prix Fixe"), New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "Par Kilométrage"), New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "Par Kilométrage + Xtra"), New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "Par Kilométrage + Xtra(Heure)"), New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "Par Nombre KM fixe + extra"), New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "Par Jour"), New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "A l'heure"), New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "Par résidence")})
        Me.optCalculationLivreur.Size = New System.Drawing.Size(295, 160)
        Me.optCalculationLivreur.StyleController = Me.LayoutControl1
        Me.optCalculationLivreur.TabIndex = 129
        '
        'tbOrganisationImport
        '
        Me.tbOrganisationImport.Location = New System.Drawing.Point(236, 141)
        Me.tbOrganisationImport.Name = "tbOrganisationImport"
        Me.tbOrganisationImport.Properties.Appearance.BackColor = System.Drawing.Color.Ivory
        Me.tbOrganisationImport.Properties.Appearance.Options.UseBackColor = True
        Me.tbOrganisationImport.Size = New System.Drawing.Size(205, 20)
        Me.tbOrganisationImport.StyleController = Me.LayoutControl1
        Me.tbOrganisationImport.TabIndex = 7
        '
        'tbMontantMobilus
        '
        Me.tbMontantMobilus.EditValue = "0"
        Me.tbMontantMobilus.Location = New System.Drawing.Point(693, 286)
        Me.tbMontantMobilus.Name = "tbMontantMobilus"
        Me.tbMontantMobilus.Properties.DisplayFormat.FormatString = "c"
        Me.tbMontantMobilus.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.tbMontantMobilus.Properties.EditFormat.FormatString = "c"
        Me.tbMontantMobilus.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.tbMontantMobilus.Properties.Mask.EditMask = "c"
        Me.tbMontantMobilus.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.tbMontantMobilus.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.tbMontantMobilus.Size = New System.Drawing.Size(142, 20)
        Me.tbMontantMobilus.StyleController = Me.LayoutControl1
        Me.tbMontantMobilus.TabIndex = 128
        '
        'tbCourriel4
        '
        Me.tbCourriel4.Location = New System.Drawing.Point(224, 273)
        Me.tbCourriel4.Name = "tbCourriel4"
        Me.tbCourriel4.Properties.Mask.SaveLiteral = False
        Me.tbCourriel4.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.tbCourriel4.Size = New System.Drawing.Size(200, 20)
        Me.tbCourriel4.StyleController = Me.LayoutControl1
        Me.tbCourriel4.TabIndex = 9
        '
        'tbMontantFixe
        '
        Me.tbMontantFixe.EditValue = "0"
        Me.tbMontantFixe.Location = New System.Drawing.Point(1075, 286)
        Me.tbMontantFixe.Name = "tbMontantFixe"
        Me.tbMontantFixe.Properties.DisplayFormat.FormatString = "c"
        Me.tbMontantFixe.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.tbMontantFixe.Properties.EditFormat.FormatString = "c"
        Me.tbMontantFixe.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.tbMontantFixe.Properties.Mask.EditMask = "c"
        Me.tbMontantFixe.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.tbMontantFixe.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.tbMontantFixe.Size = New System.Drawing.Size(198, 20)
        Me.tbMontantFixe.StyleController = Me.LayoutControl1
        Me.tbMontantFixe.TabIndex = 127
        '
        'tbMessageFacture
        '
        Me.tbMessageFacture.Location = New System.Drawing.Point(457, 468)
        Me.tbMessageFacture.Name = "tbMessageFacture"
        Me.tbMessageFacture.Size = New System.Drawing.Size(390, 69)
        Me.tbMessageFacture.StyleController = Me.LayoutControl1
        Me.tbMessageFacture.TabIndex = 31
        '
        'tbMessageEtat
        '
        Me.tbMessageEtat.Location = New System.Drawing.Point(851, 466)
        Me.tbMessageEtat.Name = "tbMessageEtat"
        Me.tbMessageEtat.Size = New System.Drawing.Size(446, 69)
        Me.tbMessageEtat.StyleController = Me.LayoutControl1
        Me.tbMessageEtat.TabIndex = 31
        '
        'tbMaximumLivraison
        '
        Me.tbMaximumLivraison.Location = New System.Drawing.Point(693, 218)
        Me.tbMaximumLivraison.Name = "tbMaximumLivraison"
        Me.tbMaximumLivraison.Properties.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.tbMaximumLivraison.Properties.Appearance.Options.UseBackColor = True
        Me.tbMaximumLivraison.Size = New System.Drawing.Size(158, 20)
        Me.tbMaximumLivraison.StyleController = Me.LayoutControl1
        Me.tbMaximumLivraison.TabIndex = 5
        '
        'tbMontantMaxLivraisoneExtra
        '
        Me.tbMontantMaxLivraisoneExtra.EditValue = "0"
        Me.tbMontantMaxLivraisoneExtra.Location = New System.Drawing.Point(1067, 194)
        Me.tbMontantMaxLivraisoneExtra.Name = "tbMontantMaxLivraisoneExtra"
        Me.tbMontantMaxLivraisoneExtra.Properties.DisplayFormat.FormatString = "c"
        Me.tbMontantMaxLivraisoneExtra.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.tbMontantMaxLivraisoneExtra.Properties.EditFormat.FormatString = "c"
        Me.tbMontantMaxLivraisoneExtra.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.tbMontantMaxLivraisoneExtra.Properties.Mask.EditMask = "c"
        Me.tbMontantMaxLivraisoneExtra.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.tbMontantMaxLivraisoneExtra.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.tbMontantMaxLivraisoneExtra.Size = New System.Drawing.Size(206, 20)
        Me.tbMontantMaxLivraisoneExtra.StyleController = Me.LayoutControl1
        Me.tbMontantMaxLivraisoneExtra.TabIndex = 127
        '
        'tbMontantPrixFixeSem
        '
        Me.tbMontantPrixFixeSem.EditValue = "0"
        Me.tbMontantPrixFixeSem.Location = New System.Drawing.Point(693, 194)
        Me.tbMontantPrixFixeSem.Name = "tbMontantPrixFixeSem"
        Me.tbMontantPrixFixeSem.Properties.DisplayFormat.FormatString = "c"
        Me.tbMontantPrixFixeSem.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.tbMontantPrixFixeSem.Properties.EditFormat.FormatString = "c"
        Me.tbMontantPrixFixeSem.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.tbMontantPrixFixeSem.Properties.Mask.EditMask = "c"
        Me.tbMontantPrixFixeSem.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.tbMontantPrixFixeSem.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.tbMontantPrixFixeSem.Size = New System.Drawing.Size(158, 20)
        Me.tbMontantPrixFixeSem.StyleController = Me.LayoutControl1
        Me.tbMontantPrixFixeSem.TabIndex = 127
        '
        'tbMontantParPorteOrganisation
        '
        Me.tbMontantParPorteOrganisation.EditValue = "0"
        Me.tbMontantParPorteOrganisation.Location = New System.Drawing.Point(964, 102)
        Me.tbMontantParPorteOrganisation.Name = "tbMontantParPorteOrganisation"
        Me.tbMontantParPorteOrganisation.Properties.DisplayFormat.FormatString = "c"
        Me.tbMontantParPorteOrganisation.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.tbMontantParPorteOrganisation.Properties.EditFormat.FormatString = "c"
        Me.tbMontantParPorteOrganisation.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.tbMontantParPorteOrganisation.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.tbMontantParPorteOrganisation.Properties.MaskSettings.Set("MaskManagerType", GetType(DevExpress.Data.Mask.NumericMaskManager))
        Me.tbMontantParPorteOrganisation.Properties.MaskSettings.Set("mask", "c")
        Me.tbMontantParPorteOrganisation.Size = New System.Drawing.Size(62, 20)
        Me.tbMontantParPorteOrganisation.StyleController = Me.LayoutControl1
        Me.tbMontantParPorteOrganisation.TabIndex = 127
        '
        'tbMontantParPorteLivreur
        '
        Me.tbMontantParPorteLivreur.EditValue = "0"
        Me.tbMontantParPorteLivreur.Location = New System.Drawing.Point(964, 126)
        Me.tbMontantParPorteLivreur.Name = "tbMontantParPorteLivreur"
        Me.tbMontantParPorteLivreur.Properties.DisplayFormat.FormatString = "c"
        Me.tbMontantParPorteLivreur.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.tbMontantParPorteLivreur.Properties.EditFormat.FormatString = "c"
        Me.tbMontantParPorteLivreur.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.tbMontantParPorteLivreur.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.tbMontantParPorteLivreur.Properties.MaskSettings.Set("MaskManagerType", GetType(DevExpress.Data.Mask.NumericMaskManager))
        Me.tbMontantParPorteLivreur.Properties.MaskSettings.Set("mask", "c")
        Me.tbMontantParPorteLivreur.Size = New System.Drawing.Size(62, 20)
        Me.tbMontantParPorteLivreur.StyleController = Me.LayoutControl1
        Me.tbMontantParPorteLivreur.TabIndex = 127
        '
        'LayoutControlGroup1
        '
        Me.LayoutControlGroup1.CustomizationFormText = "LayoutControlGroup1"
        Me.LayoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup1.GroupBordersVisible = False
        Me.LayoutControlGroup1.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem5, Me.LayoutControlItem8, Me.LayoutPaymentInfo, Me.LayoutControlItem78, Me.LayoutCalculationLivreur, Me.LayoutControlItem42, Me.LayoutControlItem43, Me.LayoutControlItem70, Me.LayoutControlItem68, Me.LayoutControlItem69, Me.LayoutControlItem71, Me.LayoutControlItem41, Me.LayoutControlItem10, Me.LayoutCommission, Me.LayoutControlItem67, Me.LayoutControlItem82, Me.LayoutControlItem83, Me.LayoutControlItem84, Me.EmptySpaceItem13, Me.EmptySpaceItem14, Me.LayoutControlGroup13, Me.LayoutControlItem100, Me.LayoutControlItem102, Me.LayoutControlItem103, Me.LayoutControlItem104})
        Me.LayoutControlGroup1.Name = "Root"
        Me.LayoutControlGroup1.Size = New System.Drawing.Size(1309, 784)
        Me.LayoutControlGroup1.TextVisible = False
        '
        'LayoutControlItem5
        '
        Me.LayoutControlItem5.Control = Me.tbTelephone
        Me.LayoutControlItem5.CustomizationFormText = "LayoutControlItem5"
        Me.LayoutControlItem5.Location = New System.Drawing.Point(0, 165)
        Me.LayoutControlItem5.Name = "LayoutControlItem5"
        Me.LayoutControlItem5.Size = New System.Drawing.Size(445, 24)
        Me.LayoutControlItem5.Text = "Téléphone"
        Me.LayoutControlItem5.TextSize = New System.Drawing.Size(208, 13)
        '
        'LayoutControlItem8
        '
        Me.LayoutControlItem8.Control = Me.nudOrganisationID
        Me.LayoutControlItem8.Location = New System.Drawing.Point(0, 26)
        Me.LayoutControlItem8.Name = "LayoutControlItem8"
        Me.LayoutControlItem8.Size = New System.Drawing.Size(445, 24)
        Me.LayoutControlItem8.Text = "Organisation ID"
        Me.LayoutControlItem8.TextSize = New System.Drawing.Size(208, 13)
        '
        'LayoutPaymentInfo
        '
        Me.LayoutPaymentInfo.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutContrat, Me.LayoutMontantSpeciaux, Me.LayoutCredit, Me.LayoutPrixFixeExtra, Me.LayoutPrixFixe})
        Me.LayoutPaymentInfo.Location = New System.Drawing.Point(445, 26)
        Me.LayoutPaymentInfo.Name = "LayoutPaymentInfo"
        Me.LayoutPaymentInfo.Size = New System.Drawing.Size(844, 412)
        Me.LayoutPaymentInfo.Text = "Information de facturation"
        '
        'LayoutContrat
        '
        Me.LayoutContrat.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutMontantContrat, Me.LayoutPayeAuLivreur, Me.EmptySpaceItem15, Me.LayoutMontantParPorteO, Me.LayoutMontantParPorteL})
        Me.LayoutContrat.Location = New System.Drawing.Point(0, 0)
        Me.LayoutContrat.Name = "LayoutContrat"
        Me.LayoutContrat.Size = New System.Drawing.Size(820, 92)
        Me.LayoutContrat.Text = "Contrat"
        '
        'LayoutMontantContrat
        '
        Me.LayoutMontantContrat.Control = Me.tbMontantContrat
        Me.LayoutMontantContrat.Location = New System.Drawing.Point(0, 0)
        Me.LayoutMontantContrat.Name = "LayoutMontantContrat"
        Me.LayoutMontantContrat.Size = New System.Drawing.Size(271, 24)
        Me.LayoutMontantContrat.Text = "Montant Contrat"
        Me.LayoutMontantContrat.TextSize = New System.Drawing.Size(208, 13)
        '
        'LayoutPayeAuLivreur
        '
        Me.LayoutPayeAuLivreur.Control = Me.tbPayeLivreur
        Me.LayoutPayeAuLivreur.Location = New System.Drawing.Point(0, 24)
        Me.LayoutPayeAuLivreur.Name = "LayoutPayeAuLivreur"
        Me.LayoutPayeAuLivreur.Size = New System.Drawing.Size(271, 24)
        Me.LayoutPayeAuLivreur.Text = "Payé au livreur"
        Me.LayoutPayeAuLivreur.TextSize = New System.Drawing.Size(208, 13)
        '
        'EmptySpaceItem15
        '
        Me.EmptySpaceItem15.AllowHotTrack = False
        Me.EmptySpaceItem15.Location = New System.Drawing.Point(549, 0)
        Me.EmptySpaceItem15.Name = "EmptySpaceItem15"
        Me.EmptySpaceItem15.Size = New System.Drawing.Size(247, 48)
        Me.EmptySpaceItem15.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutMontantParPorteO
        '
        Me.LayoutMontantParPorteO.Control = Me.tbMontantParPorteOrganisation
        Me.LayoutMontantParPorteO.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutMontantParPorteO.CustomizationFormText = "Montant Contrat"
        Me.LayoutMontantParPorteO.Location = New System.Drawing.Point(271, 0)
        Me.LayoutMontantParPorteO.Name = "LayoutMontantParPorteO"
        Me.LayoutMontantParPorteO.Size = New System.Drawing.Size(278, 24)
        Me.LayoutMontantParPorteO.Text = "Montant par porte organisation"
        Me.LayoutMontantParPorteO.TextSize = New System.Drawing.Size(208, 13)
        '
        'LayoutMontantParPorteL
        '
        Me.LayoutMontantParPorteL.Control = Me.tbMontantParPorteLivreur
        Me.LayoutMontantParPorteL.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutMontantParPorteL.CustomizationFormText = "Montant Contrat"
        Me.LayoutMontantParPorteL.Location = New System.Drawing.Point(271, 24)
        Me.LayoutMontantParPorteL.Name = "LayoutMontantParPorteL"
        Me.LayoutMontantParPorteL.Size = New System.Drawing.Size(278, 24)
        Me.LayoutMontantParPorteL.Text = "Montant par porte chauffeur"
        Me.LayoutMontantParPorteL.TextSize = New System.Drawing.Size(208, 13)
        '
        'LayoutMontantSpeciaux
        '
        Me.LayoutMontantSpeciaux.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutNombreFree30535, Me.LayoutMontantMobilus, Me.LayoutGlaciers})
        Me.LayoutMontantSpeciaux.Location = New System.Drawing.Point(0, 184)
        Me.LayoutMontantSpeciaux.Name = "LayoutMontantSpeciaux"
        Me.LayoutMontantSpeciaux.Size = New System.Drawing.Size(382, 184)
        Me.LayoutMontantSpeciaux.Text = "Charges autres"
        '
        'LayoutNombreFree30535
        '
        Me.LayoutNombreFree30535.Control = Me.tbNombreFree30535
        Me.LayoutNombreFree30535.Location = New System.Drawing.Point(0, 24)
        Me.LayoutNombreFree30535.Name = "LayoutNombreFree30535"
        Me.LayoutNombreFree30535.Size = New System.Drawing.Size(358, 24)
        Me.LayoutNombreFree30535.Text = "Nombre maximum free echange (305 et 35)"
        Me.LayoutNombreFree30535.TextSize = New System.Drawing.Size(208, 13)
        '
        'LayoutMontantMobilus
        '
        Me.LayoutMontantMobilus.Control = Me.tbMontantMobilus
        Me.LayoutMontantMobilus.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutMontantMobilus.CustomizationFormText = "Payé au livreur"
        Me.LayoutMontantMobilus.Location = New System.Drawing.Point(0, 0)
        Me.LayoutMontantMobilus.Name = "LayoutMontantMobilus"
        Me.LayoutMontantMobilus.Size = New System.Drawing.Size(358, 24)
        Me.LayoutMontantMobilus.Text = "Montant mobilus"
        Me.LayoutMontantMobilus.TextSize = New System.Drawing.Size(208, 13)
        '
        'LayoutGlaciers
        '
        Me.LayoutGlaciers.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem39, Me.LayoutControlItem40})
        Me.LayoutGlaciers.Location = New System.Drawing.Point(0, 48)
        Me.LayoutGlaciers.Name = "LayoutGlaciers"
        Me.LayoutGlaciers.Size = New System.Drawing.Size(358, 92)
        Me.LayoutGlaciers.Text = "Glaciers"
        '
        'LayoutControlItem39
        '
        Me.LayoutControlItem39.Control = Me.tbMontantGlacierOrganisation
        Me.LayoutControlItem39.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem39.CustomizationFormText = "Bonus Amount"
        Me.LayoutControlItem39.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem39.Name = "LayoutControlItem39"
        Me.LayoutControlItem39.Size = New System.Drawing.Size(334, 24)
        Me.LayoutControlItem39.Text = "Montant chargé a l'organisation"
        Me.LayoutControlItem39.TextSize = New System.Drawing.Size(208, 13)
        '
        'LayoutControlItem40
        '
        Me.LayoutControlItem40.Control = Me.tbMontantGlacierLivreur
        Me.LayoutControlItem40.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem40.CustomizationFormText = "Bonus Amount"
        Me.LayoutControlItem40.Location = New System.Drawing.Point(0, 24)
        Me.LayoutControlItem40.Name = "LayoutControlItem40"
        Me.LayoutControlItem40.Size = New System.Drawing.Size(334, 24)
        Me.LayoutControlItem40.Text = "Montant payé au livreur par retour"
        Me.LayoutControlItem40.TextSize = New System.Drawing.Size(208, 13)
        '
        'LayoutCredit
        '
        Me.LayoutCredit.AllowCustomizeChildren = False
        Me.LayoutCredit.AppearanceGroup.BackColor = System.Drawing.Color.LemonChiffon
        Me.LayoutCredit.AppearanceGroup.BackColor2 = System.Drawing.Color.AntiqueWhite
        Me.LayoutCredit.AppearanceGroup.Options.UseBackColor = True
        Me.LayoutCredit.AppearanceItemCaption.BackColor = System.Drawing.Color.Transparent
        Me.LayoutCredit.AppearanceItemCaption.Options.UseBackColor = True
        Me.LayoutCredit.AppearanceTabPage.Header.BackColor = System.Drawing.Color.Blue
        Me.LayoutCredit.AppearanceTabPage.Header.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.LayoutCredit.AppearanceTabPage.Header.Options.UseBackColor = True
        Me.LayoutCredit.AppearanceTabPage.HeaderActive.BackColor = System.Drawing.Color.Aqua
        Me.LayoutCredit.AppearanceTabPage.HeaderActive.BackColor2 = System.Drawing.Color.Yellow
        Me.LayoutCredit.AppearanceTabPage.HeaderActive.Options.UseBackColor = True
        Me.LayoutCredit.AppearanceTabPage.HeaderDisabled.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.LayoutCredit.AppearanceTabPage.HeaderDisabled.BackColor2 = System.Drawing.Color.Aqua
        Me.LayoutCredit.AppearanceTabPage.HeaderDisabled.Options.UseBackColor = True
        Me.LayoutCredit.AppearanceTabPage.HeaderHotTracked.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.LayoutCredit.AppearanceTabPage.HeaderHotTracked.BackColor2 = System.Drawing.Color.Yellow
        Me.LayoutCredit.AppearanceTabPage.HeaderHotTracked.Options.UseBackColor = True
        Me.LayoutCredit.AppearanceTabPage.PageClient.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.LayoutCredit.AppearanceTabPage.PageClient.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.LayoutCredit.AppearanceTabPage.PageClient.Options.UseBackColor = True
        Me.LayoutCredit.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem12, Me.LayoutControlItem11})
        Me.LayoutCredit.Location = New System.Drawing.Point(382, 252)
        Me.LayoutCredit.Name = "LayoutCredit"
        Me.LayoutCredit.Size = New System.Drawing.Size(438, 116)
        Me.LayoutCredit.Text = "Crédits "
        '
        'LayoutControlItem12
        '
        Me.LayoutControlItem12.Control = Me.tbQuantiteLivraison
        Me.LayoutControlItem12.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem12.CustomizationFormText = "KM Bonus start"
        Me.LayoutControlItem12.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem12.Name = "LayoutControlItem12"
        Me.LayoutControlItem12.Size = New System.Drawing.Size(414, 24)
        Me.LayoutControlItem12.Text = "Quantité livraison"
        Me.LayoutControlItem12.TextSize = New System.Drawing.Size(208, 13)
        '
        'LayoutControlItem11
        '
        Me.LayoutControlItem11.Control = Me.tbCreditParLivraison
        Me.LayoutControlItem11.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem11.CustomizationFormText = "Bonus Amount"
        Me.LayoutControlItem11.Location = New System.Drawing.Point(0, 24)
        Me.LayoutControlItem11.Name = "LayoutControlItem11"
        Me.LayoutControlItem11.Size = New System.Drawing.Size(414, 48)
        Me.LayoutControlItem11.Text = "Montant de crédit par livraison"
        Me.LayoutControlItem11.TextSize = New System.Drawing.Size(208, 13)
        '
        'LayoutPrixFixeExtra
        '
        Me.LayoutPrixFixeExtra.CustomizationFormText = "Glaciers"
        Me.LayoutPrixFixeExtra.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutMontantContrat3, Me.EmptySpaceItem21, Me.LayoutMaximumLivraison, Me.LayoutMontantContrat2})
        Me.LayoutPrixFixeExtra.Location = New System.Drawing.Point(0, 92)
        Me.LayoutPrixFixeExtra.Name = "LayoutPrixFixeExtra"
        Me.LayoutPrixFixeExtra.Size = New System.Drawing.Size(820, 92)
        Me.LayoutPrixFixeExtra.Text = "Prix Fixe/Sem + Extra"
        '
        'LayoutMontantContrat3
        '
        Me.LayoutMontantContrat3.Control = Me.tbMontantPrixFixeSem
        Me.LayoutMontantContrat3.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutMontantContrat3.CustomizationFormText = "Montant Contrat"
        Me.LayoutMontantContrat3.Location = New System.Drawing.Point(0, 0)
        Me.LayoutMontantContrat3.Name = "LayoutMontantContrat3"
        Me.LayoutMontantContrat3.Size = New System.Drawing.Size(374, 24)
        Me.LayoutMontantContrat3.Text = "Montant Contrat"
        Me.LayoutMontantContrat3.TextSize = New System.Drawing.Size(208, 13)
        '
        'EmptySpaceItem21
        '
        Me.EmptySpaceItem21.AllowHotTrack = False
        Me.EmptySpaceItem21.Location = New System.Drawing.Point(374, 24)
        Me.EmptySpaceItem21.Name = "EmptySpaceItem21"
        Me.EmptySpaceItem21.Size = New System.Drawing.Size(422, 24)
        Me.EmptySpaceItem21.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutMaximumLivraison
        '
        Me.LayoutMaximumLivraison.Control = Me.tbMaximumLivraison
        Me.LayoutMaximumLivraison.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutMaximumLivraison.CustomizationFormText = "Organisation ID"
        Me.LayoutMaximumLivraison.Location = New System.Drawing.Point(0, 24)
        Me.LayoutMaximumLivraison.Name = "LayoutMaximumLivraison"
        Me.LayoutMaximumLivraison.Size = New System.Drawing.Size(374, 24)
        Me.LayoutMaximumLivraison.Text = "Maximum Livraisons"
        Me.LayoutMaximumLivraison.TextSize = New System.Drawing.Size(208, 13)
        '
        'LayoutMontantContrat2
        '
        Me.LayoutMontantContrat2.Control = Me.tbMontantMaxLivraisoneExtra
        Me.LayoutMontantContrat2.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutMontantContrat2.CustomizationFormText = "Montant Contrat"
        Me.LayoutMontantContrat2.Location = New System.Drawing.Point(374, 0)
        Me.LayoutMontantContrat2.Name = "LayoutMontantContrat2"
        Me.LayoutMontantContrat2.Size = New System.Drawing.Size(422, 24)
        Me.LayoutMontantContrat2.Text = "Montant Extra Livraison"
        Me.LayoutMontantContrat2.TextSize = New System.Drawing.Size(208, 13)
        '
        'LayoutPrixFixe
        '
        Me.LayoutPrixFixe.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutMontantContrat1})
        Me.LayoutPrixFixe.Location = New System.Drawing.Point(382, 184)
        Me.LayoutPrixFixe.Name = "LayoutPrixFixe"
        Me.LayoutPrixFixe.Size = New System.Drawing.Size(438, 68)
        Me.LayoutPrixFixe.Text = "Prix fixe de fin de semaine"
        '
        'LayoutMontantContrat1
        '
        Me.LayoutMontantContrat1.Control = Me.tbMontantFixe
        Me.LayoutMontantContrat1.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutMontantContrat1.CustomizationFormText = "Montant Contrat"
        Me.LayoutMontantContrat1.Location = New System.Drawing.Point(0, 0)
        Me.LayoutMontantContrat1.Name = "LayoutMontantContrat1"
        Me.LayoutMontantContrat1.Size = New System.Drawing.Size(414, 24)
        Me.LayoutMontantContrat1.Text = "Prix fixe de fin de semaine"
        Me.LayoutMontantContrat1.TextSize = New System.Drawing.Size(208, 13)
        '
        'LayoutControlItem78
        '
        Me.LayoutControlItem78.Control = Me.chkRestaurant
        Me.LayoutControlItem78.Location = New System.Drawing.Point(0, 50)
        Me.LayoutControlItem78.Name = "LayoutControlItem78"
        Me.LayoutControlItem78.Size = New System.Drawing.Size(445, 23)
        Me.LayoutControlItem78.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem78.TextVisible = False
        '
        'LayoutCalculationLivreur
        '
        Me.LayoutCalculationLivreur.CustomizationFormText = "Méthodes de calculation"
        Me.LayoutCalculationLivreur.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem30, Me.LayoutControlItem6, Me.EmptySpaceItem11, Me.EmptySpaceItem12, Me.LayoutControlItem80, Me.LayoutControlItem81})
        Me.LayoutCalculationLivreur.Location = New System.Drawing.Point(0, 532)
        Me.LayoutCalculationLivreur.Name = "LayoutCalculationLivreur"
        Me.LayoutCalculationLivreur.ShowTabPageCloseButton = True
        Me.LayoutCalculationLivreur.Size = New System.Drawing.Size(1289, 232)
        Me.LayoutCalculationLivreur.Text = "Méthodes de calculations"
        '
        'LayoutControlItem30
        '
        Me.LayoutControlItem30.Control = Me.optCalculationLivreur
        Me.LayoutControlItem30.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem30.CustomizationFormText = "Type de Calculations"
        Me.LayoutControlItem30.Location = New System.Drawing.Point(586, 24)
        Me.LayoutControlItem30.Name = "LayoutControlItem30"
        Me.LayoutControlItem30.Size = New System.Drawing.Size(299, 164)
        Me.LayoutControlItem30.Text = "Type de Calculations"
        Me.LayoutControlItem30.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem30.TextVisible = False
        '
        'LayoutControlItem6
        '
        Me.LayoutControlItem6.Control = Me.optCalculationOrganisations
        Me.LayoutControlItem6.Location = New System.Drawing.Point(301, 24)
        Me.LayoutControlItem6.Name = "LayoutControlItem6"
        Me.LayoutControlItem6.Size = New System.Drawing.Size(285, 164)
        Me.LayoutControlItem6.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem6.TextVisible = False
        '
        'EmptySpaceItem11
        '
        Me.EmptySpaceItem11.AllowHotTrack = False
        Me.EmptySpaceItem11.Location = New System.Drawing.Point(0, 0)
        Me.EmptySpaceItem11.Name = "EmptySpaceItem11"
        Me.EmptySpaceItem11.Size = New System.Drawing.Size(301, 188)
        Me.EmptySpaceItem11.TextSize = New System.Drawing.Size(0, 0)
        '
        'EmptySpaceItem12
        '
        Me.EmptySpaceItem12.AllowHotTrack = False
        Me.EmptySpaceItem12.Location = New System.Drawing.Point(885, 0)
        Me.EmptySpaceItem12.Name = "EmptySpaceItem12"
        Me.EmptySpaceItem12.Size = New System.Drawing.Size(380, 188)
        Me.EmptySpaceItem12.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlItem80
        '
        Me.LayoutControlItem80.Control = Me.Label1
        Me.LayoutControlItem80.Location = New System.Drawing.Point(301, 0)
        Me.LayoutControlItem80.Name = "LayoutControlItem80"
        Me.LayoutControlItem80.Size = New System.Drawing.Size(285, 24)
        Me.LayoutControlItem80.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem80.TextVisible = False
        '
        'LayoutControlItem81
        '
        Me.LayoutControlItem81.Control = Me.Label2
        Me.LayoutControlItem81.Location = New System.Drawing.Point(586, 0)
        Me.LayoutControlItem81.Name = "LayoutControlItem81"
        Me.LayoutControlItem81.Size = New System.Drawing.Size(299, 24)
        Me.LayoutControlItem81.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem81.TextVisible = False
        '
        'LayoutControlItem42
        '
        Me.LayoutControlItem42.Control = Me.tbCourriel2
        Me.LayoutControlItem42.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem42.CustomizationFormText = "LayoutControlItem5"
        Me.LayoutControlItem42.Location = New System.Drawing.Point(0, 213)
        Me.LayoutControlItem42.Name = "LayoutControlItem42"
        Me.LayoutControlItem42.Size = New System.Drawing.Size(416, 24)
        Me.LayoutControlItem42.Text = "Courriel 2"
        Me.LayoutControlItem42.TextSize = New System.Drawing.Size(208, 13)
        '
        'LayoutControlItem43
        '
        Me.LayoutControlItem43.Control = Me.tbCourriel3
        Me.LayoutControlItem43.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem43.CustomizationFormText = "LayoutControlItem5"
        Me.LayoutControlItem43.Location = New System.Drawing.Point(0, 237)
        Me.LayoutControlItem43.Name = "LayoutControlItem43"
        Me.LayoutControlItem43.Size = New System.Drawing.Size(416, 24)
        Me.LayoutControlItem43.Text = "Courriel 3"
        Me.LayoutControlItem43.TextSize = New System.Drawing.Size(208, 13)
        '
        'LayoutControlItem70
        '
        Me.LayoutControlItem70.Control = Me.tbCourriel4
        Me.LayoutControlItem70.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem70.CustomizationFormText = "LayoutControlItem5"
        Me.LayoutControlItem70.Location = New System.Drawing.Point(0, 261)
        Me.LayoutControlItem70.Name = "LayoutControlItem70"
        Me.LayoutControlItem70.Size = New System.Drawing.Size(416, 24)
        Me.LayoutControlItem70.Text = "Courriel 4"
        Me.LayoutControlItem70.TextSize = New System.Drawing.Size(208, 13)
        '
        'LayoutControlItem68
        '
        Me.LayoutControlItem68.Control = Me.chkCacheEmail2
        Me.LayoutControlItem68.Location = New System.Drawing.Point(416, 213)
        Me.LayoutControlItem68.Name = "LayoutControlItem68"
        Me.LayoutControlItem68.Size = New System.Drawing.Size(29, 24)
        Me.LayoutControlItem68.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem68.TextVisible = False
        '
        'LayoutControlItem69
        '
        Me.LayoutControlItem69.Control = Me.chkCacheEmail3
        Me.LayoutControlItem69.Location = New System.Drawing.Point(416, 237)
        Me.LayoutControlItem69.Name = "LayoutControlItem69"
        Me.LayoutControlItem69.Size = New System.Drawing.Size(29, 24)
        Me.LayoutControlItem69.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem69.TextVisible = False
        '
        'LayoutControlItem71
        '
        Me.LayoutControlItem71.Control = Me.chkCacheEmail4
        Me.LayoutControlItem71.Location = New System.Drawing.Point(416, 261)
        Me.LayoutControlItem71.Name = "LayoutControlItem71"
        Me.LayoutControlItem71.Size = New System.Drawing.Size(29, 24)
        Me.LayoutControlItem71.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem71.TextVisible = False
        '
        'LayoutControlItem41
        '
        Me.LayoutControlItem41.Control = Me.tbCourriel1
        Me.LayoutControlItem41.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem41.CustomizationFormText = "LayoutControlItem5"
        Me.LayoutControlItem41.Location = New System.Drawing.Point(0, 189)
        Me.LayoutControlItem41.Name = "LayoutControlItem41"
        Me.LayoutControlItem41.Size = New System.Drawing.Size(416, 24)
        Me.LayoutControlItem41.Text = "Courriel 1"
        Me.LayoutControlItem41.TextSize = New System.Drawing.Size(208, 13)
        '
        'LayoutControlItem10
        '
        Me.LayoutControlItem10.Control = Me.tbAdresse
        Me.LayoutControlItem10.CustomizationFormText = "Adresse"
        Me.LayoutControlItem10.Location = New System.Drawing.Point(0, 285)
        Me.LayoutControlItem10.MaxSize = New System.Drawing.Size(445, 89)
        Me.LayoutControlItem10.MinSize = New System.Drawing.Size(445, 89)
        Me.LayoutControlItem10.Name = "LayoutControlItem10"
        Me.LayoutControlItem10.Size = New System.Drawing.Size(445, 89)
        Me.LayoutControlItem10.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem10.Text = "Adresse Facturation"
        Me.LayoutControlItem10.TextLocation = DevExpress.Utils.Locations.Top
        Me.LayoutControlItem10.TextSize = New System.Drawing.Size(208, 13)
        '
        'LayoutCommission
        '
        Me.LayoutCommission.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutVendeurs, Me.LayoutControlItem73, Me.LayoutControlItem20})
        Me.LayoutCommission.Location = New System.Drawing.Point(0, 438)
        Me.LayoutCommission.Name = "LayoutCommission"
        Me.LayoutCommission.Size = New System.Drawing.Size(445, 94)
        Me.LayoutCommission.Text = "Vendeurs"
        '
        'LayoutVendeurs
        '
        Me.LayoutVendeurs.Control = Me.cbVendeur
        Me.LayoutVendeurs.Location = New System.Drawing.Point(0, 0)
        Me.LayoutVendeurs.Name = "LayoutVendeurs"
        Me.LayoutVendeurs.Size = New System.Drawing.Size(396, 26)
        Me.LayoutVendeurs.Text = "Vendeurs"
        Me.LayoutVendeurs.TextSize = New System.Drawing.Size(208, 13)
        '
        'LayoutControlItem73
        '
        Me.LayoutControlItem73.Control = Me.bAnnulerVendeur
        Me.LayoutControlItem73.Location = New System.Drawing.Point(396, 0)
        Me.LayoutControlItem73.Name = "LayoutControlItem73"
        Me.LayoutControlItem73.Size = New System.Drawing.Size(25, 26)
        Me.LayoutControlItem73.Text = "Vendeur"
        Me.LayoutControlItem73.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem73.TextVisible = False
        '
        'LayoutControlItem20
        '
        Me.LayoutControlItem20.Control = Me.tbCommissionVendeur
        Me.LayoutControlItem20.Location = New System.Drawing.Point(0, 26)
        Me.LayoutControlItem20.Name = "LayoutControlItem20"
        Me.LayoutControlItem20.Size = New System.Drawing.Size(421, 24)
        Me.LayoutControlItem20.Text = "Montant de commission par livraison"
        Me.LayoutControlItem20.TextSize = New System.Drawing.Size(208, 13)
        '
        'LayoutControlItem67
        '
        Me.LayoutControlItem67.Control = Me.chkCacheEmail1
        Me.LayoutControlItem67.Location = New System.Drawing.Point(416, 189)
        Me.LayoutControlItem67.Name = "LayoutControlItem67"
        Me.LayoutControlItem67.Size = New System.Drawing.Size(29, 24)
        Me.LayoutControlItem67.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem67.TextVisible = False
        '
        'LayoutControlItem82
        '
        Me.LayoutControlItem82.Control = Me.butApply
        Me.LayoutControlItem82.Location = New System.Drawing.Point(230, 0)
        Me.LayoutControlItem82.Name = "LayoutControlItem82"
        Me.LayoutControlItem82.Size = New System.Drawing.Size(249, 26)
        Me.LayoutControlItem82.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem82.TextVisible = False
        '
        'LayoutControlItem83
        '
        Me.LayoutControlItem83.Control = Me.butCancel
        Me.LayoutControlItem83.Location = New System.Drawing.Point(479, 0)
        Me.LayoutControlItem83.Name = "LayoutControlItem83"
        Me.LayoutControlItem83.Size = New System.Drawing.Size(251, 26)
        Me.LayoutControlItem83.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem83.TextVisible = False
        '
        'LayoutControlItem84
        '
        Me.LayoutControlItem84.Control = Me.butClose
        Me.LayoutControlItem84.Location = New System.Drawing.Point(730, 0)
        Me.LayoutControlItem84.Name = "LayoutControlItem84"
        Me.LayoutControlItem84.Size = New System.Drawing.Size(274, 26)
        Me.LayoutControlItem84.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem84.TextVisible = False
        '
        'EmptySpaceItem13
        '
        Me.EmptySpaceItem13.AllowHotTrack = False
        Me.EmptySpaceItem13.Location = New System.Drawing.Point(0, 0)
        Me.EmptySpaceItem13.Name = "EmptySpaceItem13"
        Me.EmptySpaceItem13.Size = New System.Drawing.Size(230, 26)
        Me.EmptySpaceItem13.TextSize = New System.Drawing.Size(0, 0)
        '
        'EmptySpaceItem14
        '
        Me.EmptySpaceItem14.AllowHotTrack = False
        Me.EmptySpaceItem14.Location = New System.Drawing.Point(1004, 0)
        Me.EmptySpaceItem14.Name = "EmptySpaceItem14"
        Me.EmptySpaceItem14.Size = New System.Drawing.Size(285, 26)
        Me.EmptySpaceItem14.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlGroup13
        '
        Me.LayoutControlGroup13.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem4, Me.LayoutControlItem65})
        Me.LayoutControlGroup13.Location = New System.Drawing.Point(0, 73)
        Me.LayoutControlGroup13.Name = "LayoutControlGroup13"
        Me.LayoutControlGroup13.Size = New System.Drawing.Size(445, 92)
        Me.LayoutControlGroup13.Text = "Nom Organisation et nom import"
        '
        'LayoutControlItem4
        '
        Me.LayoutControlItem4.Control = Me.tbOrganisation
        Me.LayoutControlItem4.CustomizationFormText = "Contact #1"
        Me.LayoutControlItem4.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem4.Name = "LayoutControlItem4"
        Me.LayoutControlItem4.Size = New System.Drawing.Size(421, 24)
        Me.LayoutControlItem4.Text = "Organisation"
        Me.LayoutControlItem4.TextSize = New System.Drawing.Size(208, 13)
        '
        'LayoutControlItem65
        '
        Me.LayoutControlItem65.Control = Me.tbOrganisationImport
        Me.LayoutControlItem65.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem65.CustomizationFormText = "Contact #1"
        Me.LayoutControlItem65.Location = New System.Drawing.Point(0, 24)
        Me.LayoutControlItem65.Name = "LayoutControlItem65"
        Me.LayoutControlItem65.Size = New System.Drawing.Size(421, 24)
        Me.LayoutControlItem65.Text = "OrganisationImport"
        Me.LayoutControlItem65.TextSize = New System.Drawing.Size(208, 13)
        '
        'LayoutControlItem100
        '
        Me.LayoutControlItem100.ContentHorzAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.LayoutControlItem100.ContentVertAlignment = DevExpress.Utils.VertAlignment.Center
        Me.LayoutControlItem100.Control = Me.tbMessageFacture
        Me.LayoutControlItem100.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem100.CustomizationFormText = "Adresse"
        Me.LayoutControlItem100.Location = New System.Drawing.Point(445, 438)
        Me.LayoutControlItem100.MaxSize = New System.Drawing.Size(0, 89)
        Me.LayoutControlItem100.MinSize = New System.Drawing.Size(1, 89)
        Me.LayoutControlItem100.Name = "LayoutControlItem100"
        Me.LayoutControlItem100.Size = New System.Drawing.Size(394, 94)
        Me.LayoutControlItem100.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem100.Text = "Message facture"
        Me.LayoutControlItem100.TextLocation = DevExpress.Utils.Locations.Top
        Me.LayoutControlItem100.TextSize = New System.Drawing.Size(208, 13)
        '
        'LayoutControlItem102
        '
        Me.LayoutControlItem102.AppearanceItemCaption.Options.UseTextOptions = True
        Me.LayoutControlItem102.AppearanceItemCaption.TextOptions.Trimming = DevExpress.Utils.Trimming.None
        Me.LayoutControlItem102.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.LayoutControlItem102.Control = Me.tbMessageEtat
        Me.LayoutControlItem102.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem102.CustomizationFormText = "Adresse"
        Me.LayoutControlItem102.Location = New System.Drawing.Point(839, 438)
        Me.LayoutControlItem102.MaxSize = New System.Drawing.Size(0, 89)
        Me.LayoutControlItem102.MinSize = New System.Drawing.Size(1, 89)
        Me.LayoutControlItem102.Name = "LayoutControlItem102"
        Me.LayoutControlItem102.Size = New System.Drawing.Size(450, 94)
        Me.LayoutControlItem102.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem102.Text = "Message Etat"
        Me.LayoutControlItem102.TextLocation = DevExpress.Utils.Locations.Top
        Me.LayoutControlItem102.TextSize = New System.Drawing.Size(208, 13)
        '
        'LayoutControlItem103
        '
        Me.LayoutControlItem103.Control = Me.chkIsEtatDeCompte
        Me.LayoutControlItem103.Location = New System.Drawing.Point(0, 374)
        Me.LayoutControlItem103.Name = "LayoutControlItem103"
        Me.LayoutControlItem103.Size = New System.Drawing.Size(445, 23)
        Me.LayoutControlItem103.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem103.TextVisible = False
        '
        'LayoutControlItem104
        '
        Me.LayoutControlItem104.Control = Me.chkIsCommission
        Me.LayoutControlItem104.Location = New System.Drawing.Point(0, 397)
        Me.LayoutControlItem104.Name = "LayoutControlItem104"
        Me.LayoutControlItem104.Size = New System.Drawing.Size(445, 41)
        Me.LayoutControlItem104.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem104.TextVisible = False
        '
        'TabInfo
        '
        Me.TabInfo.Controls.Add(Me.LayoutControl2)
        Me.TabInfo.Name = "TabInfo"
        Me.TabInfo.PageVisible = False
        Me.TabInfo.Size = New System.Drawing.Size(1326, 779)
        Me.TabInfo.Text = "Par Kilométrage"
        '
        'LayoutControl2
        '
        Me.LayoutControl2.Controls.Add(Me.tbHeureDepart)
        Me.LayoutControl2.Controls.Add(Me.bMiseaJourChargesParKm)
        Me.LayoutControl2.Controls.Add(Me.bSupprimerChargesParKm)
        Me.LayoutControl2.Controls.Add(Me.bAjouterChargesParKm)
        Me.LayoutControl2.Controls.Add(Me.grdParKM)
        Me.LayoutControl2.Controls.Add(Me.tbKMDebut)
        Me.LayoutControl2.Controls.Add(Me.tbKMFin)
        Me.LayoutControl2.Controls.Add(Me.tbMontantCharge)
        Me.LayoutControl2.Controls.Add(Me.tbChargeExtra)
        Me.LayoutControl2.Controls.Add(Me.tbIDCommissionKM)
        Me.LayoutControl2.Controls.Add(Me.tbHeureFin)
        Me.LayoutControl2.Controls.Add(Me.tbChargeExtraParNbKm)
        Me.LayoutControl2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LayoutControl2.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControl2.Name = "LayoutControl2"
        Me.LayoutControl2.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = New System.Drawing.Rectangle(994, 395, 650, 400)
        Me.LayoutControl2.Root = Me.Root
        Me.LayoutControl2.Size = New System.Drawing.Size(1326, 779)
        Me.LayoutControl2.TabIndex = 0
        Me.LayoutControl2.Text = "LayoutControl2"
        '
        'tbHeureDepart
        '
        Me.tbHeureDepart.EditValue = New Date(2020, 7, 16, 0, 0, 0, 0)
        Me.tbHeureDepart.Location = New System.Drawing.Point(156, 264)
        Me.tbHeureDepart.Name = "tbHeureDepart"
        Me.tbHeureDepart.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.tbHeureDepart.Properties.DisplayFormat.FormatString = "HH:mm"
        Me.tbHeureDepart.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.tbHeureDepart.Properties.EditFormat.FormatString = "HH:mm"
        Me.tbHeureDepart.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.tbHeureDepart.Properties.Mask.EditMask = "HH:mm"
        Me.tbHeureDepart.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.tbHeureDepart.Size = New System.Drawing.Size(373, 20)
        Me.tbHeureDepart.StyleController = Me.LayoutControl2
        Me.tbHeureDepart.TabIndex = 131
        '
        'bMiseaJourChargesParKm
        '
        Me.bMiseaJourChargesParKm.Enabled = False
        Me.bMiseaJourChargesParKm.Location = New System.Drawing.Point(800, 44)
        Me.bMiseaJourChargesParKm.Name = "bMiseaJourChargesParKm"
        Me.bMiseaJourChargesParKm.Size = New System.Drawing.Size(121, 68)
        Me.bMiseaJourChargesParKm.StyleController = Me.LayoutControl2
        Me.bMiseaJourChargesParKm.TabIndex = 130
        Me.bMiseaJourChargesParKm.Text = "Mise a jour"
        '
        'bSupprimerChargesParKm
        '
        Me.bSupprimerChargesParKm.Enabled = False
        Me.bSupprimerChargesParKm.Location = New System.Drawing.Point(677, 44)
        Me.bSupprimerChargesParKm.Name = "bSupprimerChargesParKm"
        Me.bSupprimerChargesParKm.Size = New System.Drawing.Size(119, 68)
        Me.bSupprimerChargesParKm.StyleController = Me.LayoutControl2
        Me.bSupprimerChargesParKm.TabIndex = 128
        Me.bSupprimerChargesParKm.Text = "Supprimer"
        '
        'bAjouterChargesParKm
        '
        Me.bAjouterChargesParKm.Location = New System.Drawing.Point(545, 44)
        Me.bAjouterChargesParKm.Name = "bAjouterChargesParKm"
        Me.bAjouterChargesParKm.Size = New System.Drawing.Size(128, 68)
        Me.bAjouterChargesParKm.StyleController = Me.LayoutControl2
        Me.bAjouterChargesParKm.TabIndex = 127
        Me.bAjouterChargesParKm.Text = "Ajouter"
        '
        'grdParKM
        '
        Me.grdParKM.Location = New System.Drawing.Point(12, 412)
        Me.grdParKM.MainView = Me.GridView2
        Me.grdParKM.Name = "grdParKM"
        Me.grdParKM.Size = New System.Drawing.Size(1302, 355)
        Me.grdParKM.TabIndex = 9
        Me.grdParKM.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView2})
        '
        'GridView2
        '
        Me.GridView2.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colIDCommissionKM, Me.colOrganisation2, Me.GridColumn3, Me.GridColumn4, Me.Montant, Me.GridColumn6, Me.GridColumn7, Me.colHeureDepart, Me.colHeureFin})
        Me.GridView2.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.GridView2.GridControl = Me.grdParKM
        Me.GridView2.Name = "GridView2"
        Me.GridView2.OptionsBehavior.Editable = False
        Me.GridView2.OptionsCustomization.AllowGroup = False
        Me.GridView2.OptionsCustomization.AllowSort = False
        Me.GridView2.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridView2.OptionsSelection.UseIndicatorForSelection = False
        Me.GridView2.OptionsView.ShowAutoFilterRow = True
        Me.GridView2.OptionsView.ShowGroupPanel = False
        Me.GridView2.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.colOrganisation2, DevExpress.Data.ColumnSortOrder.Ascending)})
        '
        'colIDCommissionKM
        '
        Me.colIDCommissionKM.Caption = "ID CommissionKM"
        Me.colIDCommissionKM.DisplayFormat.FormatString = "f"
        Me.colIDCommissionKM.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.colIDCommissionKM.FieldName = "IDCommissionKM"
        Me.colIDCommissionKM.Name = "colIDCommissionKM"
        Me.colIDCommissionKM.Width = 94
        '
        'colOrganisation2
        '
        Me.colOrganisation2.Caption = "Organisation"
        Me.colOrganisation2.FieldName = "Organisation"
        Me.colOrganisation2.Name = "colOrganisation2"
        Me.colOrganisation2.Visible = True
        Me.colOrganisation2.VisibleIndex = 0
        Me.colOrganisation2.Width = 94
        '
        'GridColumn3
        '
        Me.GridColumn3.Caption = "KM Début"
        Me.GridColumn3.FieldName = "KMFrom"
        Me.GridColumn3.Name = "GridColumn3"
        Me.GridColumn3.Visible = True
        Me.GridColumn3.VisibleIndex = 1
        Me.GridColumn3.Width = 94
        '
        'GridColumn4
        '
        Me.GridColumn4.Caption = "KM Fin"
        Me.GridColumn4.FieldName = "KMTo"
        Me.GridColumn4.Name = "GridColumn4"
        Me.GridColumn4.Visible = True
        Me.GridColumn4.VisibleIndex = 2
        Me.GridColumn4.Width = 119
        '
        'Montant
        '
        Me.Montant.Caption = "Montant"
        Me.Montant.DisplayFormat.FormatString = "c"
        Me.Montant.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.Montant.FieldName = "Montant"
        Me.Montant.Name = "Montant"
        Me.Montant.Visible = True
        Me.Montant.VisibleIndex = 5
        '
        'GridColumn6
        '
        Me.GridColumn6.Caption = "Extra Charge"
        Me.GridColumn6.DisplayFormat.FormatString = "c"
        Me.GridColumn6.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.GridColumn6.FieldName = "ExtraChargeParKM"
        Me.GridColumn6.Name = "GridColumn6"
        Me.GridColumn6.Visible = True
        Me.GridColumn6.VisibleIndex = 6
        '
        'GridColumn7
        '
        Me.GridColumn7.Caption = "Par nombre de  KM"
        Me.GridColumn7.FieldName = "PerKMNumber"
        Me.GridColumn7.Name = "GridColumn7"
        Me.GridColumn7.Visible = True
        Me.GridColumn7.VisibleIndex = 7
        '
        'colHeureDepart
        '
        Me.colHeureDepart.Caption = "Heure départ"
        Me.colHeureDepart.DisplayFormat.FormatString = "HH:mm"
        Me.colHeureDepart.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.colHeureDepart.FieldName = "HeureDepart"
        Me.colHeureDepart.Name = "colHeureDepart"
        Me.colHeureDepart.Visible = True
        Me.colHeureDepart.VisibleIndex = 3
        '
        'colHeureFin
        '
        Me.colHeureFin.Caption = "Heure Fin"
        Me.colHeureFin.DisplayFormat.FormatString = "HH:mm"
        Me.colHeureFin.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.colHeureFin.FieldName = "HeureFin"
        Me.colHeureFin.Name = "colHeureFin"
        Me.colHeureFin.Visible = True
        Me.colHeureFin.VisibleIndex = 4
        '
        'tbKMDebut
        '
        Me.tbKMDebut.EditValue = "0"
        Me.tbKMDebut.Location = New System.Drawing.Point(144, 68)
        Me.tbKMDebut.Name = "tbKMDebut"
        Me.tbKMDebut.Properties.DisplayFormat.FormatString = "n"
        Me.tbKMDebut.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.tbKMDebut.Properties.EditFormat.FormatString = "n"
        Me.tbKMDebut.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.tbKMDebut.Properties.Mask.EditMask = "n"
        Me.tbKMDebut.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.tbKMDebut.Size = New System.Drawing.Size(397, 20)
        Me.tbKMDebut.StyleController = Me.LayoutControl2
        Me.tbKMDebut.TabIndex = 126
        '
        'tbKMFin
        '
        Me.tbKMFin.EditValue = "0"
        Me.tbKMFin.Location = New System.Drawing.Point(144, 92)
        Me.tbKMFin.Name = "tbKMFin"
        Me.tbKMFin.Properties.DisplayFormat.FormatString = "n"
        Me.tbKMFin.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.tbKMFin.Properties.EditFormat.FormatString = "n"
        Me.tbKMFin.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.tbKMFin.Properties.Mask.EditMask = "n"
        Me.tbKMFin.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.tbKMFin.Size = New System.Drawing.Size(397, 20)
        Me.tbKMFin.StyleController = Me.LayoutControl2
        Me.tbKMFin.TabIndex = 126
        '
        'tbMontantCharge
        '
        Me.tbMontantCharge.EditValue = "0"
        Me.tbMontantCharge.Location = New System.Drawing.Point(144, 116)
        Me.tbMontantCharge.Name = "tbMontantCharge"
        Me.tbMontantCharge.Properties.DisplayFormat.FormatString = "c"
        Me.tbMontantCharge.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.tbMontantCharge.Properties.EditFormat.FormatString = "c"
        Me.tbMontantCharge.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.tbMontantCharge.Properties.Mask.EditMask = "c"
        Me.tbMontantCharge.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.tbMontantCharge.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.tbMontantCharge.Size = New System.Drawing.Size(397, 20)
        Me.tbMontantCharge.StyleController = Me.LayoutControl2
        Me.tbMontantCharge.TabIndex = 124
        '
        'tbChargeExtra
        '
        Me.tbChargeExtra.EditValue = "0"
        Me.tbChargeExtra.Location = New System.Drawing.Point(156, 172)
        Me.tbChargeExtra.Name = "tbChargeExtra"
        Me.tbChargeExtra.Properties.DisplayFormat.FormatString = "c"
        Me.tbChargeExtra.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.tbChargeExtra.Properties.EditFormat.FormatString = "c"
        Me.tbChargeExtra.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.tbChargeExtra.Properties.Mask.EditMask = "c"
        Me.tbChargeExtra.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.tbChargeExtra.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.tbChargeExtra.Size = New System.Drawing.Size(373, 20)
        Me.tbChargeExtra.StyleController = Me.LayoutControl2
        Me.tbChargeExtra.TabIndex = 124
        '
        'tbIDCommissionKM
        '
        Me.tbIDCommissionKM.EditValue = "0"
        Me.tbIDCommissionKM.Enabled = False
        Me.tbIDCommissionKM.Location = New System.Drawing.Point(144, 44)
        Me.tbIDCommissionKM.Name = "tbIDCommissionKM"
        Me.tbIDCommissionKM.Properties.Appearance.BackColor = System.Drawing.Color.Azure
        Me.tbIDCommissionKM.Properties.Appearance.Options.UseBackColor = True
        Me.tbIDCommissionKM.Properties.Mask.EditMask = "n0"
        Me.tbIDCommissionKM.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.tbIDCommissionKM.Size = New System.Drawing.Size(397, 20)
        Me.tbIDCommissionKM.StyleController = Me.LayoutControl2
        Me.tbIDCommissionKM.TabIndex = 126
        '
        'tbHeureFin
        '
        Me.tbHeureFin.EditValue = New Date(2020, 7, 16, 0, 0, 0, 0)
        Me.tbHeureFin.Location = New System.Drawing.Point(156, 288)
        Me.tbHeureFin.Name = "tbHeureFin"
        Me.tbHeureFin.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.tbHeureFin.Properties.DisplayFormat.FormatString = "HH:mm"
        Me.tbHeureFin.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.tbHeureFin.Properties.EditFormat.FormatString = "HH:mm"
        Me.tbHeureFin.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.tbHeureFin.Properties.Mask.EditMask = "HH:mm"
        Me.tbHeureFin.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.tbHeureFin.Size = New System.Drawing.Size(373, 20)
        Me.tbHeureFin.StyleController = Me.LayoutControl2
        Me.tbHeureFin.TabIndex = 131
        '
        'tbChargeExtraParNbKm
        '
        Me.tbChargeExtraParNbKm.EditValue = "0"
        Me.tbChargeExtraParNbKm.Location = New System.Drawing.Point(156, 196)
        Me.tbChargeExtraParNbKm.Name = "tbChargeExtraParNbKm"
        Me.tbChargeExtraParNbKm.Properties.DisplayFormat.FormatString = "n"
        Me.tbChargeExtraParNbKm.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.tbChargeExtraParNbKm.Properties.EditFormat.FormatString = "n"
        Me.tbChargeExtraParNbKm.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.tbChargeExtraParNbKm.Properties.Mask.EditMask = "n"
        Me.tbChargeExtraParNbKm.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.tbChargeExtraParNbKm.Size = New System.Drawing.Size(373, 20)
        Me.tbChargeExtraParNbKm.StyleController = Me.LayoutControl2
        Me.tbChargeExtraParNbKm.TabIndex = 126
        '
        'Root
        '
        Me.Root.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.Root.GroupBordersVisible = False
        Me.Root.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem22, Me.LayoutControlGroup5})
        Me.Root.Name = "Root"
        Me.Root.Size = New System.Drawing.Size(1326, 779)
        Me.Root.TextVisible = False
        '
        'LayoutControlItem22
        '
        Me.LayoutControlItem22.Control = Me.grdParKM
        Me.LayoutControlItem22.Location = New System.Drawing.Point(0, 400)
        Me.LayoutControlItem22.Name = "LayoutControlItem22"
        Me.LayoutControlItem22.Size = New System.Drawing.Size(1306, 359)
        Me.LayoutControlItem22.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem22.TextVisible = False
        '
        'LayoutControlGroup5
        '
        Me.LayoutControlGroup5.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutKMDebut, Me.LayoutKMFin, Me.LayoutControlItem13, Me.LayoutChargesSupplementaires, Me.LayoutControlItem28, Me.LayoutControlItem27, Me.EmptySpaceItem2, Me.EmptySpaceItem3, Me.LayoutIDCommissionKM, Me.LayoutControlItem49, Me.LayoutParHeures})
        Me.LayoutControlGroup5.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup5.Name = "LayoutControlGroup5"
        Me.LayoutControlGroup5.Size = New System.Drawing.Size(1306, 400)
        Me.LayoutControlGroup5.Text = "Charges par Kilomètre"
        '
        'LayoutKMDebut
        '
        Me.LayoutKMDebut.Control = Me.tbKMDebut
        Me.LayoutKMDebut.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutKMDebut.CustomizationFormText = "Bonus par kilométrage démarre a"
        Me.LayoutKMDebut.Location = New System.Drawing.Point(0, 24)
        Me.LayoutKMDebut.Name = "LayoutKMDebut"
        Me.LayoutKMDebut.Size = New System.Drawing.Size(521, 24)
        Me.LayoutKMDebut.Text = "Kilomètre départ"
        Me.LayoutKMDebut.TextSize = New System.Drawing.Size(116, 13)
        '
        'LayoutKMFin
        '
        Me.LayoutKMFin.Control = Me.tbKMFin
        Me.LayoutKMFin.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutKMFin.CustomizationFormText = "Bonus par kilométrage démarre a"
        Me.LayoutKMFin.Location = New System.Drawing.Point(0, 48)
        Me.LayoutKMFin.Name = "LayoutKMFin"
        Me.LayoutKMFin.Size = New System.Drawing.Size(521, 24)
        Me.LayoutKMFin.Text = "Kilomètre fin"
        Me.LayoutKMFin.TextSize = New System.Drawing.Size(116, 13)
        '
        'LayoutControlItem13
        '
        Me.LayoutControlItem13.Control = Me.tbMontantCharge
        Me.LayoutControlItem13.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem13.CustomizationFormText = "Montant de bonus"
        Me.LayoutControlItem13.Location = New System.Drawing.Point(0, 72)
        Me.LayoutControlItem13.Name = "LayoutControlItem13"
        Me.LayoutControlItem13.Size = New System.Drawing.Size(521, 24)
        Me.LayoutControlItem13.Text = "Montant chargé"
        Me.LayoutControlItem13.TextSize = New System.Drawing.Size(116, 13)
        '
        'LayoutChargesSupplementaires
        '
        Me.LayoutChargesSupplementaires.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutChargeExtra, Me.LayoutParNombreKm})
        Me.LayoutChargesSupplementaires.Location = New System.Drawing.Point(0, 96)
        Me.LayoutChargesSupplementaires.Name = "LayoutChargesSupplementaires"
        Me.LayoutChargesSupplementaires.Size = New System.Drawing.Size(521, 92)
        Me.LayoutChargesSupplementaires.Text = "Charges supplémentaires"
        '
        'LayoutChargeExtra
        '
        Me.LayoutChargeExtra.Control = Me.tbChargeExtra
        Me.LayoutChargeExtra.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutChargeExtra.CustomizationFormText = "Montant de bonus"
        Me.LayoutChargeExtra.Location = New System.Drawing.Point(0, 0)
        Me.LayoutChargeExtra.Name = "LayoutChargeExtra"
        Me.LayoutChargeExtra.Size = New System.Drawing.Size(497, 24)
        Me.LayoutChargeExtra.Text = "Charge Extra"
        Me.LayoutChargeExtra.TextSize = New System.Drawing.Size(116, 13)
        '
        'LayoutParNombreKm
        '
        Me.LayoutParNombreKm.Control = Me.tbChargeExtraParNbKm
        Me.LayoutParNombreKm.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutParNombreKm.CustomizationFormText = "Bonus par kilométrage démarre a"
        Me.LayoutParNombreKm.Location = New System.Drawing.Point(0, 24)
        Me.LayoutParNombreKm.Name = "LayoutParNombreKm"
        Me.LayoutParNombreKm.Size = New System.Drawing.Size(497, 24)
        Me.LayoutParNombreKm.Text = "Par nombre de kilomètre"
        Me.LayoutParNombreKm.TextSize = New System.Drawing.Size(116, 13)
        '
        'LayoutControlItem28
        '
        Me.LayoutControlItem28.Control = Me.bAjouterChargesParKm
        Me.LayoutControlItem28.Location = New System.Drawing.Point(521, 0)
        Me.LayoutControlItem28.MinSize = New System.Drawing.Size(49, 26)
        Me.LayoutControlItem28.Name = "LayoutControlItem28"
        Me.LayoutControlItem28.Size = New System.Drawing.Size(132, 72)
        Me.LayoutControlItem28.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem28.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem28.TextVisible = False
        '
        'LayoutControlItem27
        '
        Me.LayoutControlItem27.Control = Me.bSupprimerChargesParKm
        Me.LayoutControlItem27.Location = New System.Drawing.Point(653, 0)
        Me.LayoutControlItem27.MinSize = New System.Drawing.Size(61, 26)
        Me.LayoutControlItem27.Name = "LayoutControlItem27"
        Me.LayoutControlItem27.Size = New System.Drawing.Size(123, 72)
        Me.LayoutControlItem27.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem27.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem27.TextVisible = False
        '
        'EmptySpaceItem2
        '
        Me.EmptySpaceItem2.AllowHotTrack = False
        Me.EmptySpaceItem2.Location = New System.Drawing.Point(901, 0)
        Me.EmptySpaceItem2.Name = "EmptySpaceItem2"
        Me.EmptySpaceItem2.Size = New System.Drawing.Size(381, 356)
        Me.EmptySpaceItem2.TextSize = New System.Drawing.Size(0, 0)
        '
        'EmptySpaceItem3
        '
        Me.EmptySpaceItem3.AllowHotTrack = False
        Me.EmptySpaceItem3.Location = New System.Drawing.Point(521, 72)
        Me.EmptySpaceItem3.Name = "EmptySpaceItem3"
        Me.EmptySpaceItem3.Size = New System.Drawing.Size(380, 284)
        Me.EmptySpaceItem3.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutIDCommissionKM
        '
        Me.LayoutIDCommissionKM.Control = Me.tbIDCommissionKM
        Me.LayoutIDCommissionKM.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutIDCommissionKM.CustomizationFormText = "Bonus par kilométrage démarre a"
        Me.LayoutIDCommissionKM.Location = New System.Drawing.Point(0, 0)
        Me.LayoutIDCommissionKM.Name = "LayoutIDCommissionKM"
        Me.LayoutIDCommissionKM.Size = New System.Drawing.Size(521, 24)
        Me.LayoutIDCommissionKM.Text = "ID"
        Me.LayoutIDCommissionKM.TextSize = New System.Drawing.Size(116, 13)
        '
        'LayoutControlItem49
        '
        Me.LayoutControlItem49.Control = Me.bMiseaJourChargesParKm
        Me.LayoutControlItem49.Location = New System.Drawing.Point(776, 0)
        Me.LayoutControlItem49.MinSize = New System.Drawing.Size(1, 1)
        Me.LayoutControlItem49.Name = "LayoutControlItem49"
        Me.LayoutControlItem49.Size = New System.Drawing.Size(125, 72)
        Me.LayoutControlItem49.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem49.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem49.TextVisible = False
        '
        'LayoutParHeures
        '
        Me.LayoutParHeures.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem14, Me.EmptySpaceItem8, Me.LayoutControlItem25})
        Me.LayoutParHeures.Location = New System.Drawing.Point(0, 188)
        Me.LayoutParHeures.Name = "LayoutParHeures"
        Me.LayoutParHeures.Size = New System.Drawing.Size(521, 168)
        Me.LayoutParHeures.Text = "Par heure"
        '
        'LayoutControlItem14
        '
        Me.LayoutControlItem14.Control = Me.tbHeureDepart
        Me.LayoutControlItem14.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem14.Name = "LayoutControlItem14"
        Me.LayoutControlItem14.Size = New System.Drawing.Size(497, 24)
        Me.LayoutControlItem14.Text = "Heure départ"
        Me.LayoutControlItem14.TextSize = New System.Drawing.Size(116, 13)
        '
        'EmptySpaceItem8
        '
        Me.EmptySpaceItem8.AllowHotTrack = False
        Me.EmptySpaceItem8.Location = New System.Drawing.Point(0, 48)
        Me.EmptySpaceItem8.Name = "EmptySpaceItem8"
        Me.EmptySpaceItem8.Size = New System.Drawing.Size(497, 76)
        Me.EmptySpaceItem8.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlItem25
        '
        Me.LayoutControlItem25.Control = Me.tbHeureFin
        Me.LayoutControlItem25.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem25.CustomizationFormText = "LayoutControlItem14"
        Me.LayoutControlItem25.Location = New System.Drawing.Point(0, 24)
        Me.LayoutControlItem25.Name = "LayoutControlItem25"
        Me.LayoutControlItem25.Size = New System.Drawing.Size(497, 24)
        Me.LayoutControlItem25.Text = "Heure Fin"
        Me.LayoutControlItem25.TextSize = New System.Drawing.Size(116, 13)
        '
        'tabLivreurs
        '
        Me.tabLivreurs.Controls.Add(Me.LayoutControl4)
        Me.tabLivreurs.Name = "tabLivreurs"
        Me.tabLivreurs.PageVisible = False
        Me.tabLivreurs.Size = New System.Drawing.Size(1326, 779)
        Me.tabLivreurs.Text = "Par Kilométrage / Livreurs"
        '
        'LayoutControl4
        '
        Me.LayoutControl4.Controls.Add(Me.bMiseAJourChargesParKmCL)
        Me.LayoutControl4.Controls.Add(Me.grdParKMCL)
        Me.LayoutControl4.Controls.Add(Me.bSupprimerChargesParKmCL)
        Me.LayoutControl4.Controls.Add(Me.bAjouterChargesParKmCL)
        Me.LayoutControl4.Controls.Add(Me.tbMontantChargeCL)
        Me.LayoutControl4.Controls.Add(Me.tbChargeExtraCL)
        Me.LayoutControl4.Controls.Add(Me.tbIDCommissionKMCL)
        Me.LayoutControl4.Controls.Add(Me.tbKMDebutCL)
        Me.LayoutControl4.Controls.Add(Me.tbKMFinCL)
        Me.LayoutControl4.Controls.Add(Me.tbHeureDepartCL)
        Me.LayoutControl4.Controls.Add(Me.tbHeureFinCL)
        Me.LayoutControl4.Controls.Add(Me.tbChargeExtraParNbKmCL)
        Me.LayoutControl4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LayoutControl4.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControl4.Name = "LayoutControl4"
        Me.LayoutControl4.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = New System.Drawing.Rectangle(931, 639, 650, 400)
        Me.LayoutControl4.Root = Me.LayoutControlGroup7
        Me.LayoutControl4.Size = New System.Drawing.Size(1326, 779)
        Me.LayoutControl4.TabIndex = 0
        Me.LayoutControl4.Text = "LayoutControl4"
        '
        'bMiseAJourChargesParKmCL
        '
        Me.bMiseAJourChargesParKmCL.Enabled = False
        Me.bMiseAJourChargesParKmCL.Location = New System.Drawing.Point(772, 44)
        Me.bMiseAJourChargesParKmCL.Name = "bMiseAJourChargesParKmCL"
        Me.bMiseAJourChargesParKmCL.Size = New System.Drawing.Size(143, 78)
        Me.bMiseAJourChargesParKmCL.StyleController = Me.LayoutControl4
        Me.bMiseAJourChargesParKmCL.TabIndex = 132
        Me.bMiseAJourChargesParKmCL.Text = "Mise a jour"
        '
        'grdParKMCL
        '
        Me.grdParKMCL.Location = New System.Drawing.Point(12, 336)
        Me.grdParKMCL.MainView = Me.GridView5
        Me.grdParKMCL.Name = "grdParKMCL"
        Me.grdParKMCL.Size = New System.Drawing.Size(1302, 431)
        Me.grdParKMCL.TabIndex = 131
        Me.grdParKMCL.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView5})
        '
        'GridView5
        '
        Me.GridView5.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn1, Me.GridColumn2, Me.GridColumn17, Me.GridColumn18, Me.GridColumn19, Me.GridColumn20, Me.GridColumn21, Me.GridColumn22, Me.GridColumn23})
        Me.GridView5.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.GridView5.GridControl = Me.grdParKMCL
        Me.GridView5.Name = "GridView5"
        Me.GridView5.OptionsBehavior.Editable = False
        Me.GridView5.OptionsCustomization.AllowGroup = False
        Me.GridView5.OptionsCustomization.AllowSort = False
        Me.GridView5.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridView5.OptionsSelection.UseIndicatorForSelection = False
        Me.GridView5.OptionsView.ShowAutoFilterRow = True
        Me.GridView5.OptionsView.ShowGroupPanel = False
        Me.GridView5.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.GridColumn2, DevExpress.Data.ColumnSortOrder.Ascending)})
        '
        'GridColumn1
        '
        Me.GridColumn1.Caption = "ID CommissionKM"
        Me.GridColumn1.FieldName = "IDCommissionKMLivreur"
        Me.GridColumn1.Name = "GridColumn1"
        Me.GridColumn1.Width = 94
        '
        'GridColumn2
        '
        Me.GridColumn2.Caption = "Organisation"
        Me.GridColumn2.FieldName = "Organisation"
        Me.GridColumn2.Name = "GridColumn2"
        Me.GridColumn2.Width = 94
        '
        'GridColumn17
        '
        Me.GridColumn17.Caption = "KM Début"
        Me.GridColumn17.FieldName = "KMFrom"
        Me.GridColumn17.Name = "GridColumn17"
        Me.GridColumn17.Visible = True
        Me.GridColumn17.VisibleIndex = 0
        Me.GridColumn17.Width = 94
        '
        'GridColumn18
        '
        Me.GridColumn18.Caption = "KM Fin"
        Me.GridColumn18.FieldName = "KMTo"
        Me.GridColumn18.Name = "GridColumn18"
        Me.GridColumn18.Visible = True
        Me.GridColumn18.VisibleIndex = 1
        Me.GridColumn18.Width = 119
        '
        'GridColumn19
        '
        Me.GridColumn19.Caption = "Montant"
        Me.GridColumn19.DisplayFormat.FormatString = "c"
        Me.GridColumn19.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.GridColumn19.FieldName = "Montant"
        Me.GridColumn19.Name = "GridColumn19"
        Me.GridColumn19.Visible = True
        Me.GridColumn19.VisibleIndex = 4
        '
        'GridColumn20
        '
        Me.GridColumn20.Caption = "Extra Charge"
        Me.GridColumn20.DisplayFormat.FormatString = "c"
        Me.GridColumn20.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.GridColumn20.FieldName = "ExtraChargeParKM"
        Me.GridColumn20.Name = "GridColumn20"
        Me.GridColumn20.Visible = True
        Me.GridColumn20.VisibleIndex = 5
        '
        'GridColumn21
        '
        Me.GridColumn21.Caption = "Par nombre de  KM"
        Me.GridColumn21.FieldName = "PerKMNumber"
        Me.GridColumn21.Name = "GridColumn21"
        Me.GridColumn21.Visible = True
        Me.GridColumn21.VisibleIndex = 6
        '
        'GridColumn22
        '
        Me.GridColumn22.Caption = "Heure départ"
        Me.GridColumn22.DisplayFormat.FormatString = "HH:mm"
        Me.GridColumn22.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.GridColumn22.FieldName = "HeureDepart"
        Me.GridColumn22.Name = "GridColumn22"
        Me.GridColumn22.Visible = True
        Me.GridColumn22.VisibleIndex = 2
        '
        'GridColumn23
        '
        Me.GridColumn23.Caption = "Heure Fin"
        Me.GridColumn23.DisplayFormat.FormatString = "HH:mm"
        Me.GridColumn23.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.GridColumn23.FieldName = "HeureFin"
        Me.GridColumn23.Name = "GridColumn23"
        Me.GridColumn23.Visible = True
        Me.GridColumn23.VisibleIndex = 3
        '
        'bSupprimerChargesParKmCL
        '
        Me.bSupprimerChargesParKmCL.Enabled = False
        Me.bSupprimerChargesParKmCL.Location = New System.Drawing.Point(634, 44)
        Me.bSupprimerChargesParKmCL.Name = "bSupprimerChargesParKmCL"
        Me.bSupprimerChargesParKmCL.Size = New System.Drawing.Size(134, 78)
        Me.bSupprimerChargesParKmCL.StyleController = Me.LayoutControl4
        Me.bSupprimerChargesParKmCL.TabIndex = 130
        Me.bSupprimerChargesParKmCL.Text = "Supprimer"
        '
        'bAjouterChargesParKmCL
        '
        Me.bAjouterChargesParKmCL.Location = New System.Drawing.Point(487, 44)
        Me.bAjouterChargesParKmCL.Name = "bAjouterChargesParKmCL"
        Me.bAjouterChargesParKmCL.Size = New System.Drawing.Size(143, 78)
        Me.bAjouterChargesParKmCL.StyleController = Me.LayoutControl4
        Me.bAjouterChargesParKmCL.TabIndex = 129
        Me.bAjouterChargesParKmCL.Text = "Ajouter"
        '
        'tbMontantChargeCL
        '
        Me.tbMontantChargeCL.EditValue = "0"
        Me.tbMontantChargeCL.Location = New System.Drawing.Point(144, 116)
        Me.tbMontantChargeCL.Name = "tbMontantChargeCL"
        Me.tbMontantChargeCL.Properties.DisplayFormat.FormatString = "c"
        Me.tbMontantChargeCL.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.tbMontantChargeCL.Properties.EditFormat.FormatString = "c"
        Me.tbMontantChargeCL.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.tbMontantChargeCL.Properties.Mask.EditMask = "c"
        Me.tbMontantChargeCL.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.tbMontantChargeCL.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.tbMontantChargeCL.Size = New System.Drawing.Size(339, 20)
        Me.tbMontantChargeCL.StyleController = Me.LayoutControl4
        Me.tbMontantChargeCL.TabIndex = 124
        '
        'tbChargeExtraCL
        '
        Me.tbChargeExtraCL.EditValue = "0"
        Me.tbChargeExtraCL.Location = New System.Drawing.Point(156, 172)
        Me.tbChargeExtraCL.Name = "tbChargeExtraCL"
        Me.tbChargeExtraCL.Properties.DisplayFormat.FormatString = "c"
        Me.tbChargeExtraCL.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.tbChargeExtraCL.Properties.EditFormat.FormatString = "c"
        Me.tbChargeExtraCL.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.tbChargeExtraCL.Properties.Mask.EditMask = "c"
        Me.tbChargeExtraCL.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.tbChargeExtraCL.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.tbChargeExtraCL.Size = New System.Drawing.Size(315, 20)
        Me.tbChargeExtraCL.StyleController = Me.LayoutControl4
        Me.tbChargeExtraCL.TabIndex = 124
        '
        'tbIDCommissionKMCL
        '
        Me.tbIDCommissionKMCL.EditValue = "0"
        Me.tbIDCommissionKMCL.Enabled = False
        Me.tbIDCommissionKMCL.Location = New System.Drawing.Point(144, 44)
        Me.tbIDCommissionKMCL.Name = "tbIDCommissionKMCL"
        Me.tbIDCommissionKMCL.Properties.Appearance.BackColor = System.Drawing.Color.Azure
        Me.tbIDCommissionKMCL.Properties.Appearance.Options.UseBackColor = True
        Me.tbIDCommissionKMCL.Properties.Mask.EditMask = "n0"
        Me.tbIDCommissionKMCL.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.tbIDCommissionKMCL.Size = New System.Drawing.Size(339, 20)
        Me.tbIDCommissionKMCL.StyleController = Me.LayoutControl4
        Me.tbIDCommissionKMCL.TabIndex = 126
        '
        'tbKMDebutCL
        '
        Me.tbKMDebutCL.EditValue = "0"
        Me.tbKMDebutCL.Location = New System.Drawing.Point(144, 68)
        Me.tbKMDebutCL.Name = "tbKMDebutCL"
        Me.tbKMDebutCL.Properties.DisplayFormat.FormatString = "n"
        Me.tbKMDebutCL.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.tbKMDebutCL.Properties.EditFormat.FormatString = "n"
        Me.tbKMDebutCL.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.tbKMDebutCL.Properties.Mask.EditMask = "n"
        Me.tbKMDebutCL.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.tbKMDebutCL.Size = New System.Drawing.Size(339, 20)
        Me.tbKMDebutCL.StyleController = Me.LayoutControl4
        Me.tbKMDebutCL.TabIndex = 126
        '
        'tbKMFinCL
        '
        Me.tbKMFinCL.EditValue = "0"
        Me.tbKMFinCL.Location = New System.Drawing.Point(144, 92)
        Me.tbKMFinCL.Name = "tbKMFinCL"
        Me.tbKMFinCL.Properties.DisplayFormat.FormatString = "n"
        Me.tbKMFinCL.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.tbKMFinCL.Properties.EditFormat.FormatString = "n"
        Me.tbKMFinCL.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.tbKMFinCL.Properties.Mask.EditMask = "n"
        Me.tbKMFinCL.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.tbKMFinCL.Size = New System.Drawing.Size(339, 20)
        Me.tbKMFinCL.StyleController = Me.LayoutControl4
        Me.tbKMFinCL.TabIndex = 126
        '
        'tbHeureDepartCL
        '
        Me.tbHeureDepartCL.EditValue = New Date(2020, 7, 16, 0, 0, 0, 0)
        Me.tbHeureDepartCL.Location = New System.Drawing.Point(156, 264)
        Me.tbHeureDepartCL.Name = "tbHeureDepartCL"
        Me.tbHeureDepartCL.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.tbHeureDepartCL.Properties.DisplayFormat.FormatString = "HH:mm"
        Me.tbHeureDepartCL.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.tbHeureDepartCL.Properties.EditFormat.FormatString = "HH:mm"
        Me.tbHeureDepartCL.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.tbHeureDepartCL.Properties.Mask.EditMask = "HH:mm"
        Me.tbHeureDepartCL.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.tbHeureDepartCL.Size = New System.Drawing.Size(315, 20)
        Me.tbHeureDepartCL.StyleController = Me.LayoutControl4
        Me.tbHeureDepartCL.TabIndex = 131
        '
        'tbHeureFinCL
        '
        Me.tbHeureFinCL.EditValue = New Date(2020, 7, 16, 0, 0, 0, 0)
        Me.tbHeureFinCL.Location = New System.Drawing.Point(156, 288)
        Me.tbHeureFinCL.Name = "tbHeureFinCL"
        Me.tbHeureFinCL.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.tbHeureFinCL.Properties.DisplayFormat.FormatString = "HH:mm"
        Me.tbHeureFinCL.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.tbHeureFinCL.Properties.EditFormat.FormatString = "HH:mm"
        Me.tbHeureFinCL.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.tbHeureFinCL.Properties.Mask.EditMask = "HH:mm"
        Me.tbHeureFinCL.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.tbHeureFinCL.Size = New System.Drawing.Size(315, 20)
        Me.tbHeureFinCL.StyleController = Me.LayoutControl4
        Me.tbHeureFinCL.TabIndex = 131
        '
        'tbChargeExtraParNbKmCL
        '
        Me.tbChargeExtraParNbKmCL.EditValue = "0"
        Me.tbChargeExtraParNbKmCL.Location = New System.Drawing.Point(156, 196)
        Me.tbChargeExtraParNbKmCL.Name = "tbChargeExtraParNbKmCL"
        Me.tbChargeExtraParNbKmCL.Properties.DisplayFormat.FormatString = "n"
        Me.tbChargeExtraParNbKmCL.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.tbChargeExtraParNbKmCL.Properties.EditFormat.FormatString = "n"
        Me.tbChargeExtraParNbKmCL.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.tbChargeExtraParNbKmCL.Properties.Mask.EditMask = "n"
        Me.tbChargeExtraParNbKmCL.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.tbChargeExtraParNbKmCL.Size = New System.Drawing.Size(315, 20)
        Me.tbChargeExtraParNbKmCL.StyleController = Me.LayoutControl4
        Me.tbChargeExtraParNbKmCL.TabIndex = 126
        '
        'LayoutControlGroup7
        '
        Me.LayoutControlGroup7.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup7.GroupBordersVisible = False
        Me.LayoutControlGroup7.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlGroup8, Me.LayoutControlItem26})
        Me.LayoutControlGroup7.Name = "Root"
        Me.LayoutControlGroup7.Size = New System.Drawing.Size(1326, 779)
        Me.LayoutControlGroup7.TextVisible = False
        '
        'LayoutControlGroup8
        '
        Me.LayoutControlGroup8.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem33, Me.LayoutControlItem32, Me.EmptySpaceItem6, Me.LayoutControlItem7, Me.LayoutChargesSupplementairesCL, Me.EmptySpaceItem5, Me.LayoutControlItem1, Me.LayoutControlItem51, Me.LayoutParHeuresCL, Me.LayoutControlItem15, Me.LayoutControlItem29})
        Me.LayoutControlGroup8.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup8.Name = "LayoutControlGroup8"
        Me.LayoutControlGroup8.Size = New System.Drawing.Size(1306, 324)
        Me.LayoutControlGroup8.Text = "Commission pour les livreurs"
        '
        'LayoutControlItem33
        '
        Me.LayoutControlItem33.Control = Me.bAjouterChargesParKmCL
        Me.LayoutControlItem33.Location = New System.Drawing.Point(463, 0)
        Me.LayoutControlItem33.MinSize = New System.Drawing.Size(49, 26)
        Me.LayoutControlItem33.Name = "LayoutControlItem33"
        Me.LayoutControlItem33.Size = New System.Drawing.Size(147, 82)
        Me.LayoutControlItem33.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem33.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem33.TextVisible = False
        '
        'LayoutControlItem32
        '
        Me.LayoutControlItem32.Control = Me.bSupprimerChargesParKmCL
        Me.LayoutControlItem32.Location = New System.Drawing.Point(610, 0)
        Me.LayoutControlItem32.MinSize = New System.Drawing.Size(61, 26)
        Me.LayoutControlItem32.Name = "LayoutControlItem32"
        Me.LayoutControlItem32.Size = New System.Drawing.Size(138, 82)
        Me.LayoutControlItem32.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem32.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem32.TextVisible = False
        '
        'EmptySpaceItem6
        '
        Me.EmptySpaceItem6.AllowHotTrack = False
        Me.EmptySpaceItem6.Location = New System.Drawing.Point(895, 0)
        Me.EmptySpaceItem6.Name = "EmptySpaceItem6"
        Me.EmptySpaceItem6.Size = New System.Drawing.Size(387, 280)
        Me.EmptySpaceItem6.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlItem7
        '
        Me.LayoutControlItem7.Control = Me.tbMontantChargeCL
        Me.LayoutControlItem7.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem7.CustomizationFormText = "Montant de bonus"
        Me.LayoutControlItem7.Location = New System.Drawing.Point(0, 72)
        Me.LayoutControlItem7.Name = "LayoutControlItem7"
        Me.LayoutControlItem7.Size = New System.Drawing.Size(463, 24)
        Me.LayoutControlItem7.Text = "Montant chargé"
        Me.LayoutControlItem7.TextSize = New System.Drawing.Size(116, 13)
        '
        'LayoutChargesSupplementairesCL
        '
        Me.LayoutChargesSupplementairesCL.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem9, Me.LayoutParNombreKmCL})
        Me.LayoutChargesSupplementairesCL.Location = New System.Drawing.Point(0, 96)
        Me.LayoutChargesSupplementairesCL.Name = "LayoutChargesSupplementairesCL"
        Me.LayoutChargesSupplementairesCL.Size = New System.Drawing.Size(463, 92)
        Me.LayoutChargesSupplementairesCL.Text = "Charges supplémentaires"
        '
        'LayoutControlItem9
        '
        Me.LayoutControlItem9.Control = Me.tbChargeExtraCL
        Me.LayoutControlItem9.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem9.CustomizationFormText = "Montant de bonus"
        Me.LayoutControlItem9.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem9.Name = "LayoutControlItem9"
        Me.LayoutControlItem9.Size = New System.Drawing.Size(439, 24)
        Me.LayoutControlItem9.Text = "Charge Extra"
        Me.LayoutControlItem9.TextSize = New System.Drawing.Size(116, 13)
        '
        'LayoutParNombreKmCL
        '
        Me.LayoutParNombreKmCL.Control = Me.tbChargeExtraParNbKmCL
        Me.LayoutParNombreKmCL.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutParNombreKmCL.CustomizationFormText = "Bonus par kilométrage démarre a"
        Me.LayoutParNombreKmCL.Location = New System.Drawing.Point(0, 24)
        Me.LayoutParNombreKmCL.Name = "LayoutParNombreKmCL"
        Me.LayoutParNombreKmCL.Size = New System.Drawing.Size(439, 24)
        Me.LayoutParNombreKmCL.Text = "Par nombre de kilomètre"
        Me.LayoutParNombreKmCL.TextSize = New System.Drawing.Size(116, 13)
        '
        'EmptySpaceItem5
        '
        Me.EmptySpaceItem5.AllowHotTrack = False
        Me.EmptySpaceItem5.Location = New System.Drawing.Point(463, 82)
        Me.EmptySpaceItem5.Name = "EmptySpaceItem5"
        Me.EmptySpaceItem5.Size = New System.Drawing.Size(432, 198)
        Me.EmptySpaceItem5.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlItem1
        '
        Me.LayoutControlItem1.Control = Me.tbIDCommissionKMCL
        Me.LayoutControlItem1.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem1.CustomizationFormText = "Bonus par kilométrage démarre a"
        Me.LayoutControlItem1.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem1.Name = "LayoutControlItem1"
        Me.LayoutControlItem1.Size = New System.Drawing.Size(463, 24)
        Me.LayoutControlItem1.Text = "ID"
        Me.LayoutControlItem1.TextSize = New System.Drawing.Size(116, 13)
        '
        'LayoutControlItem51
        '
        Me.LayoutControlItem51.Control = Me.bMiseAJourChargesParKmCL
        Me.LayoutControlItem51.Location = New System.Drawing.Point(748, 0)
        Me.LayoutControlItem51.MinSize = New System.Drawing.Size(65, 26)
        Me.LayoutControlItem51.Name = "LayoutControlItem51"
        Me.LayoutControlItem51.Size = New System.Drawing.Size(147, 82)
        Me.LayoutControlItem51.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem51.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem51.TextVisible = False
        '
        'LayoutParHeuresCL
        '
        Me.LayoutParHeuresCL.CustomizationFormText = "Par heure"
        Me.LayoutParHeuresCL.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem2, Me.LayoutControlItem3})
        Me.LayoutParHeuresCL.Location = New System.Drawing.Point(0, 188)
        Me.LayoutParHeuresCL.Name = "LayoutParHeuresCL"
        Me.LayoutParHeuresCL.Size = New System.Drawing.Size(463, 92)
        Me.LayoutParHeuresCL.Text = "Par heure"
        '
        'LayoutControlItem2
        '
        Me.LayoutControlItem2.Control = Me.tbHeureDepartCL
        Me.LayoutControlItem2.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem2.CustomizationFormText = "Heure départ"
        Me.LayoutControlItem2.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem2.Name = "LayoutControlItem2"
        Me.LayoutControlItem2.Size = New System.Drawing.Size(439, 24)
        Me.LayoutControlItem2.Text = "Heure départ"
        Me.LayoutControlItem2.TextSize = New System.Drawing.Size(116, 13)
        '
        'LayoutControlItem3
        '
        Me.LayoutControlItem3.Control = Me.tbHeureFinCL
        Me.LayoutControlItem3.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem3.CustomizationFormText = "LayoutControlItem14"
        Me.LayoutControlItem3.Location = New System.Drawing.Point(0, 24)
        Me.LayoutControlItem3.Name = "LayoutControlItem3"
        Me.LayoutControlItem3.Size = New System.Drawing.Size(439, 24)
        Me.LayoutControlItem3.Text = "Heure Fin"
        Me.LayoutControlItem3.TextSize = New System.Drawing.Size(116, 13)
        '
        'LayoutControlItem15
        '
        Me.LayoutControlItem15.Control = Me.tbKMDebutCL
        Me.LayoutControlItem15.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem15.CustomizationFormText = "Bonus par kilométrage démarre a"
        Me.LayoutControlItem15.Location = New System.Drawing.Point(0, 24)
        Me.LayoutControlItem15.Name = "LayoutControlItem15"
        Me.LayoutControlItem15.Size = New System.Drawing.Size(463, 24)
        Me.LayoutControlItem15.Text = "Kilomètre départ"
        Me.LayoutControlItem15.TextSize = New System.Drawing.Size(116, 13)
        '
        'LayoutControlItem29
        '
        Me.LayoutControlItem29.Control = Me.tbKMFinCL
        Me.LayoutControlItem29.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem29.CustomizationFormText = "Bonus par kilométrage démarre a"
        Me.LayoutControlItem29.Location = New System.Drawing.Point(0, 48)
        Me.LayoutControlItem29.Name = "LayoutControlItem29"
        Me.LayoutControlItem29.Size = New System.Drawing.Size(463, 24)
        Me.LayoutControlItem29.Text = "Kilomètre fin"
        Me.LayoutControlItem29.TextSize = New System.Drawing.Size(116, 13)
        '
        'LayoutControlItem26
        '
        Me.LayoutControlItem26.Control = Me.grdParKMCL
        Me.LayoutControlItem26.Location = New System.Drawing.Point(0, 324)
        Me.LayoutControlItem26.Name = "LayoutControlItem26"
        Me.LayoutControlItem26.Size = New System.Drawing.Size(1306, 435)
        Me.LayoutControlItem26.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem26.TextVisible = False
        '
        'tabMaxPayable
        '
        Me.tabMaxPayable.Controls.Add(Me.LayoutControl5)
        Me.tabMaxPayable.Name = "tabMaxPayable"
        Me.tabMaxPayable.PageVisible = False
        Me.tabMaxPayable.Size = New System.Drawing.Size(1326, 779)
        Me.tabMaxPayable.Text = "Maximum Payable pour les Livreurs"
        '
        'LayoutControl5
        '
        Me.LayoutControl5.Controls.Add(Me.bMiseAJourMP)
        Me.LayoutControl5.Controls.Add(Me.bSupprimerMP)
        Me.LayoutControl5.Controls.Add(Me.bAjouterMP)
        Me.LayoutControl5.Controls.Add(Me.grdMaxPayable)
        Me.LayoutControl5.Controls.Add(Me.tbIDMaxPayable)
        Me.LayoutControl5.Controls.Add(Me.tbMaximumPayable)
        Me.LayoutControl5.Controls.Add(Me.tbNombreLivraisons)
        Me.LayoutControl5.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LayoutControl5.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControl5.Name = "LayoutControl5"
        Me.LayoutControl5.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = New System.Drawing.Rectangle(188, 398, 650, 400)
        Me.LayoutControl5.Root = Me.LayoutControlGroup9
        Me.LayoutControl5.Size = New System.Drawing.Size(1326, 779)
        Me.LayoutControl5.TabIndex = 0
        Me.LayoutControl5.Text = "LayoutControl5"
        '
        'bMiseAJourMP
        '
        Me.bMiseAJourMP.Enabled = False
        Me.bMiseAJourMP.Location = New System.Drawing.Point(644, 44)
        Me.bMiseAJourMP.Name = "bMiseAJourMP"
        Me.bMiseAJourMP.Size = New System.Drawing.Size(137, 68)
        Me.bMiseAJourMP.StyleController = Me.LayoutControl5
        Me.bMiseAJourMP.TabIndex = 135
        Me.bMiseAJourMP.Text = "Mise a jour"
        '
        'bSupprimerMP
        '
        Me.bSupprimerMP.Enabled = False
        Me.bSupprimerMP.Location = New System.Drawing.Point(494, 44)
        Me.bSupprimerMP.Name = "bSupprimerMP"
        Me.bSupprimerMP.Size = New System.Drawing.Size(146, 68)
        Me.bSupprimerMP.StyleController = Me.LayoutControl5
        Me.bSupprimerMP.TabIndex = 134
        Me.bSupprimerMP.Text = "Supprimer"
        '
        'bAjouterMP
        '
        Me.bAjouterMP.Location = New System.Drawing.Point(348, 44)
        Me.bAjouterMP.Name = "bAjouterMP"
        Me.bAjouterMP.Size = New System.Drawing.Size(142, 68)
        Me.bAjouterMP.StyleController = Me.LayoutControl5
        Me.bAjouterMP.TabIndex = 133
        Me.bAjouterMP.Text = "Ajouter"
        '
        'grdMaxPayable
        '
        Me.grdMaxPayable.Location = New System.Drawing.Point(12, 128)
        Me.grdMaxPayable.MainView = Me.GridView4
        Me.grdMaxPayable.Name = "grdMaxPayable"
        Me.grdMaxPayable.Size = New System.Drawing.Size(1302, 639)
        Me.grdMaxPayable.TabIndex = 132
        Me.grdMaxPayable.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView4})
        '
        'GridView4
        '
        Me.GridView4.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colIDMaxPayable, Me.colNombreLivraison, Me.colMaxPayable})
        Me.GridView4.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.GridView4.GridControl = Me.grdMaxPayable
        Me.GridView4.Name = "GridView4"
        Me.GridView4.OptionsBehavior.Editable = False
        Me.GridView4.OptionsCustomization.AllowGroup = False
        Me.GridView4.OptionsCustomization.AllowSort = False
        Me.GridView4.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridView4.OptionsSelection.UseIndicatorForSelection = False
        Me.GridView4.OptionsView.ShowAutoFilterRow = True
        Me.GridView4.OptionsView.ShowGroupPanel = False
        Me.GridView4.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.colNombreLivraison, DevExpress.Data.ColumnSortOrder.Ascending)})
        '
        'colIDMaxPayable
        '
        Me.colIDMaxPayable.Caption = "IDMaxPayable"
        Me.colIDMaxPayable.DisplayFormat.FormatString = "c"
        Me.colIDMaxPayable.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.colIDMaxPayable.FieldName = "IDMaxPayable"
        Me.colIDMaxPayable.Name = "colIDMaxPayable"
        Me.colIDMaxPayable.Width = 94
        '
        'colNombreLivraison
        '
        Me.colNombreLivraison.Caption = "Nombre de Livraisons"
        Me.colNombreLivraison.FieldName = "NombreLivraison"
        Me.colNombreLivraison.Name = "colNombreLivraison"
        Me.colNombreLivraison.Visible = True
        Me.colNombreLivraison.VisibleIndex = 0
        Me.colNombreLivraison.Width = 94
        '
        'colMaxPayable
        '
        Me.colMaxPayable.Caption = "Maximum payable"
        Me.colMaxPayable.DisplayFormat.FormatString = "c"
        Me.colMaxPayable.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.colMaxPayable.FieldName = "MaxPayable"
        Me.colMaxPayable.Name = "colMaxPayable"
        Me.colMaxPayable.Visible = True
        Me.colMaxPayable.VisibleIndex = 1
        Me.colMaxPayable.Width = 94
        '
        'tbIDMaxPayable
        '
        Me.tbIDMaxPayable.EditValue = "0"
        Me.tbIDMaxPayable.Enabled = False
        Me.tbIDMaxPayable.EnterMoveNextControl = True
        Me.tbIDMaxPayable.Location = New System.Drawing.Point(127, 44)
        Me.tbIDMaxPayable.Name = "tbIDMaxPayable"
        Me.tbIDMaxPayable.Properties.Appearance.BackColor = System.Drawing.Color.Azure
        Me.tbIDMaxPayable.Properties.Appearance.Options.UseBackColor = True
        Me.tbIDMaxPayable.Properties.Mask.EditMask = "n0"
        Me.tbIDMaxPayable.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.tbIDMaxPayable.Size = New System.Drawing.Size(217, 20)
        Me.tbIDMaxPayable.StyleController = Me.LayoutControl5
        Me.tbIDMaxPayable.TabIndex = 126
        '
        'tbMaximumPayable
        '
        Me.tbMaximumPayable.EditValue = "0"
        Me.tbMaximumPayable.Location = New System.Drawing.Point(127, 92)
        Me.tbMaximumPayable.Name = "tbMaximumPayable"
        Me.tbMaximumPayable.Properties.DisplayFormat.FormatString = "c"
        Me.tbMaximumPayable.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.tbMaximumPayable.Properties.EditFormat.FormatString = "c"
        Me.tbMaximumPayable.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.tbMaximumPayable.Properties.Mask.EditMask = "c"
        Me.tbMaximumPayable.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.tbMaximumPayable.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.tbMaximumPayable.Size = New System.Drawing.Size(217, 20)
        Me.tbMaximumPayable.StyleController = Me.LayoutControl5
        Me.tbMaximumPayable.TabIndex = 124
        '
        'tbNombreLivraisons
        '
        Me.tbNombreLivraisons.EditValue = "0"
        Me.tbNombreLivraisons.Location = New System.Drawing.Point(127, 68)
        Me.tbNombreLivraisons.Name = "tbNombreLivraisons"
        Me.tbNombreLivraisons.Properties.Mask.EditMask = "n0"
        Me.tbNombreLivraisons.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.tbNombreLivraisons.Size = New System.Drawing.Size(217, 20)
        Me.tbNombreLivraisons.StyleController = Me.LayoutControl5
        Me.tbNombreLivraisons.TabIndex = 126
        '
        'LayoutControlGroup9
        '
        Me.LayoutControlGroup9.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup9.GroupBordersVisible = False
        Me.LayoutControlGroup9.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem45, Me.LayoutControlGroup10, Me.EmptySpaceItem7})
        Me.LayoutControlGroup9.Name = "Root"
        Me.LayoutControlGroup9.Size = New System.Drawing.Size(1326, 779)
        Me.LayoutControlGroup9.TextVisible = False
        '
        'LayoutControlItem45
        '
        Me.LayoutControlItem45.Control = Me.grdMaxPayable
        Me.LayoutControlItem45.Location = New System.Drawing.Point(0, 116)
        Me.LayoutControlItem45.Name = "LayoutControlItem45"
        Me.LayoutControlItem45.Size = New System.Drawing.Size(1306, 643)
        Me.LayoutControlItem45.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem45.TextVisible = False
        '
        'LayoutControlGroup10
        '
        Me.LayoutControlGroup10.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem46, Me.LayoutControlItem48, Me.LayoutControlItem47, Me.LayoutControlItem54, Me.LayoutControlItem53, Me.LayoutControlItem52})
        Me.LayoutControlGroup10.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup10.Name = "LayoutControlGroup10"
        Me.LayoutControlGroup10.Size = New System.Drawing.Size(785, 116)
        Me.LayoutControlGroup10.Text = "Maximum payable par nombre de livraison"
        '
        'LayoutControlItem46
        '
        Me.LayoutControlItem46.Control = Me.tbIDMaxPayable
        Me.LayoutControlItem46.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem46.CustomizationFormText = "Bonus par kilométrage démarre a"
        Me.LayoutControlItem46.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem46.Name = "LayoutControlItem46"
        Me.LayoutControlItem46.Size = New System.Drawing.Size(324, 24)
        Me.LayoutControlItem46.Text = "ID"
        Me.LayoutControlItem46.TextSize = New System.Drawing.Size(99, 13)
        '
        'LayoutControlItem48
        '
        Me.LayoutControlItem48.Control = Me.tbNombreLivraisons
        Me.LayoutControlItem48.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem48.CustomizationFormText = "Bonus par kilométrage démarre a"
        Me.LayoutControlItem48.Location = New System.Drawing.Point(0, 24)
        Me.LayoutControlItem48.Name = "LayoutControlItem48"
        Me.LayoutControlItem48.Size = New System.Drawing.Size(324, 24)
        Me.LayoutControlItem48.Text = "Nombre de livraisons"
        Me.LayoutControlItem48.TextSize = New System.Drawing.Size(99, 13)
        '
        'LayoutControlItem47
        '
        Me.LayoutControlItem47.Control = Me.tbMaximumPayable
        Me.LayoutControlItem47.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem47.CustomizationFormText = "Montant de bonus"
        Me.LayoutControlItem47.Location = New System.Drawing.Point(0, 48)
        Me.LayoutControlItem47.Name = "LayoutControlItem47"
        Me.LayoutControlItem47.Size = New System.Drawing.Size(324, 24)
        Me.LayoutControlItem47.Text = "Maximum Payable"
        Me.LayoutControlItem47.TextSize = New System.Drawing.Size(99, 13)
        '
        'LayoutControlItem54
        '
        Me.LayoutControlItem54.Control = Me.bAjouterMP
        Me.LayoutControlItem54.Location = New System.Drawing.Point(324, 0)
        Me.LayoutControlItem54.MinSize = New System.Drawing.Size(49, 26)
        Me.LayoutControlItem54.Name = "LayoutControlItem54"
        Me.LayoutControlItem54.Size = New System.Drawing.Size(146, 72)
        Me.LayoutControlItem54.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem54.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem54.TextVisible = False
        '
        'LayoutControlItem53
        '
        Me.LayoutControlItem53.Control = Me.bSupprimerMP
        Me.LayoutControlItem53.Location = New System.Drawing.Point(470, 0)
        Me.LayoutControlItem53.MinSize = New System.Drawing.Size(61, 26)
        Me.LayoutControlItem53.Name = "LayoutControlItem53"
        Me.LayoutControlItem53.Size = New System.Drawing.Size(150, 72)
        Me.LayoutControlItem53.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem53.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem53.TextVisible = False
        '
        'LayoutControlItem52
        '
        Me.LayoutControlItem52.Control = Me.bMiseAJourMP
        Me.LayoutControlItem52.Location = New System.Drawing.Point(620, 0)
        Me.LayoutControlItem52.MinSize = New System.Drawing.Size(65, 26)
        Me.LayoutControlItem52.Name = "LayoutControlItem52"
        Me.LayoutControlItem52.Size = New System.Drawing.Size(141, 72)
        Me.LayoutControlItem52.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem52.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem52.TextVisible = False
        '
        'EmptySpaceItem7
        '
        Me.EmptySpaceItem7.AllowHotTrack = False
        Me.EmptySpaceItem7.Location = New System.Drawing.Point(785, 0)
        Me.EmptySpaceItem7.Name = "EmptySpaceItem7"
        Me.EmptySpaceItem7.Size = New System.Drawing.Size(521, 116)
        Me.EmptySpaceItem7.TextSize = New System.Drawing.Size(0, 0)
        '
        'XtraTabPage3
        '
        Me.XtraTabPage3.Controls.Add(Me.LayoutControl6)
        Me.XtraTabPage3.Name = "XtraTabPage3"
        Me.XtraTabPage3.Size = New System.Drawing.Size(1326, 779)
        Me.XtraTabPage3.Text = "Cadeau Organisation"
        '
        'LayoutControl6
        '
        Me.LayoutControl6.Controls.Add(Me.bMAJCadeauO)
        Me.LayoutControl6.Controls.Add(Me.bSupprimerCadeauO)
        Me.LayoutControl6.Controls.Add(Me.bAjouterCadeauO)
        Me.LayoutControl6.Controls.Add(Me.grdCadeauOrganisation)
        Me.LayoutControl6.Controls.Add(Me.tbMontantCadeauO)
        Me.LayoutControl6.Controls.Add(Me.tbDateFromCadeauO)
        Me.LayoutControl6.Controls.Add(Me.tbDateToCadeauO)
        Me.LayoutControl6.Controls.Add(Me.tbIIDCadeauOrganisation)
        Me.LayoutControl6.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LayoutControl6.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControl6.Name = "LayoutControl6"
        Me.LayoutControl6.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = New System.Drawing.Rectangle(1144, 433, 650, 400)
        Me.LayoutControl6.Root = Me.LayoutControlGroup3
        Me.LayoutControl6.Size = New System.Drawing.Size(1326, 779)
        Me.LayoutControl6.TabIndex = 0
        Me.LayoutControl6.Text = "LayoutControl6"
        '
        'bMAJCadeauO
        '
        Me.bMAJCadeauO.Enabled = False
        Me.bMAJCadeauO.Location = New System.Drawing.Point(574, 44)
        Me.bMAJCadeauO.Name = "bMAJCadeauO"
        Me.bMAJCadeauO.Size = New System.Drawing.Size(129, 92)
        Me.bMAJCadeauO.StyleController = Me.LayoutControl6
        Me.bMAJCadeauO.TabIndex = 138
        Me.bMAJCadeauO.Text = "Mise a jour"
        '
        'bSupprimerCadeauO
        '
        Me.bSupprimerCadeauO.Enabled = False
        Me.bSupprimerCadeauO.Location = New System.Drawing.Point(447, 44)
        Me.bSupprimerCadeauO.Name = "bSupprimerCadeauO"
        Me.bSupprimerCadeauO.Size = New System.Drawing.Size(123, 92)
        Me.bSupprimerCadeauO.StyleController = Me.LayoutControl6
        Me.bSupprimerCadeauO.TabIndex = 137
        Me.bSupprimerCadeauO.Text = "Supprimer"
        '
        'bAjouterCadeauO
        '
        Me.bAjouterCadeauO.Location = New System.Drawing.Point(309, 44)
        Me.bAjouterCadeauO.Name = "bAjouterCadeauO"
        Me.bAjouterCadeauO.Size = New System.Drawing.Size(134, 92)
        Me.bAjouterCadeauO.StyleController = Me.LayoutControl6
        Me.bAjouterCadeauO.TabIndex = 136
        Me.bAjouterCadeauO.Text = "Ajouter"
        '
        'grdCadeauOrganisation
        '
        Me.grdCadeauOrganisation.Location = New System.Drawing.Point(12, 152)
        Me.grdCadeauOrganisation.MainView = Me.GridView6
        Me.grdCadeauOrganisation.Name = "grdCadeauOrganisation"
        Me.grdCadeauOrganisation.Size = New System.Drawing.Size(1302, 615)
        Me.grdCadeauOrganisation.TabIndex = 134
        Me.grdCadeauOrganisation.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView6})
        '
        'GridView6
        '
        Me.GridView6.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn13, Me.GridColumn14, Me.GridColumn26, Me.GridColumn15, Me.GridColumn16, Me.GridColumn24})
        Me.GridView6.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.GridView6.GridControl = Me.grdCadeauOrganisation
        Me.GridView6.Name = "GridView6"
        Me.GridView6.OptionsBehavior.Editable = False
        Me.GridView6.OptionsCustomization.AllowGroup = False
        Me.GridView6.OptionsCustomization.AllowSort = False
        Me.GridView6.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridView6.OptionsSelection.UseIndicatorForSelection = False
        Me.GridView6.OptionsView.ShowAutoFilterRow = True
        Me.GridView6.OptionsView.ShowGroupPanel = False
        Me.GridView6.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.GridColumn14, DevExpress.Data.ColumnSortOrder.Ascending)})
        '
        'GridColumn13
        '
        Me.GridColumn13.Caption = "IDCadeauOrganisation"
        Me.GridColumn13.DisplayFormat.FormatString = "c"
        Me.GridColumn13.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.GridColumn13.FieldName = "IDOrganisation"
        Me.GridColumn13.Name = "GridColumn13"
        Me.GridColumn13.Width = 94
        '
        'GridColumn14
        '
        Me.GridColumn14.Caption = "IDOrganisation"
        Me.GridColumn14.FieldName = "IDOrganisation"
        Me.GridColumn14.Name = "GridColumn14"
        Me.GridColumn14.Width = 94
        '
        'GridColumn26
        '
        Me.GridColumn26.Caption = "Organisation"
        Me.GridColumn26.FieldName = "Organisation"
        Me.GridColumn26.Name = "GridColumn26"
        Me.GridColumn26.Visible = True
        Me.GridColumn26.VisibleIndex = 0
        '
        'GridColumn15
        '
        Me.GridColumn15.Caption = "Date du"
        Me.GridColumn15.DisplayFormat.FormatString = "c"
        Me.GridColumn15.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.GridColumn15.FieldName = "DateFrom"
        Me.GridColumn15.Name = "GridColumn15"
        Me.GridColumn15.Visible = True
        Me.GridColumn15.VisibleIndex = 1
        Me.GridColumn15.Width = 94
        '
        'GridColumn16
        '
        Me.GridColumn16.Caption = "Date au"
        Me.GridColumn16.FieldName = "DateTo"
        Me.GridColumn16.Name = "GridColumn16"
        Me.GridColumn16.Visible = True
        Me.GridColumn16.VisibleIndex = 2
        '
        'GridColumn24
        '
        Me.GridColumn24.Caption = "Montant"
        Me.GridColumn24.FieldName = "Montant"
        Me.GridColumn24.Name = "GridColumn24"
        Me.GridColumn24.Visible = True
        Me.GridColumn24.VisibleIndex = 3
        '
        'tbMontantCadeauO
        '
        Me.tbMontantCadeauO.EditValue = "0"
        Me.tbMontantCadeauO.Location = New System.Drawing.Point(106, 116)
        Me.tbMontantCadeauO.Name = "tbMontantCadeauO"
        Me.tbMontantCadeauO.Properties.DisplayFormat.FormatString = "c"
        Me.tbMontantCadeauO.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.tbMontantCadeauO.Properties.EditFormat.FormatString = "c"
        Me.tbMontantCadeauO.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.tbMontantCadeauO.Properties.Mask.EditMask = "c"
        Me.tbMontantCadeauO.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.tbMontantCadeauO.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.tbMontantCadeauO.Size = New System.Drawing.Size(199, 20)
        Me.tbMontantCadeauO.StyleController = Me.LayoutControl6
        Me.tbMontantCadeauO.TabIndex = 127
        '
        'tbDateFromCadeauO
        '
        Me.tbDateFromCadeauO.EditValue = Nothing
        Me.tbDateFromCadeauO.Location = New System.Drawing.Point(106, 68)
        Me.tbDateFromCadeauO.Name = "tbDateFromCadeauO"
        Me.tbDateFromCadeauO.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.tbDateFromCadeauO.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.tbDateFromCadeauO.Properties.DisplayFormat.FormatString = "MM-dd-yy H:mm"
        Me.tbDateFromCadeauO.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.tbDateFromCadeauO.Properties.EditFormat.FormatString = "MM-dd-yy H:mm"
        Me.tbDateFromCadeauO.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.tbDateFromCadeauO.Properties.Mask.EditMask = "MM-dd-yy H:mm"
        Me.tbDateFromCadeauO.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.tbDateFromCadeauO.Size = New System.Drawing.Size(199, 20)
        Me.tbDateFromCadeauO.StyleController = Me.LayoutControl6
        Me.tbDateFromCadeauO.TabIndex = 8
        '
        'tbDateToCadeauO
        '
        Me.tbDateToCadeauO.EditValue = Nothing
        Me.tbDateToCadeauO.Location = New System.Drawing.Point(106, 92)
        Me.tbDateToCadeauO.Name = "tbDateToCadeauO"
        Me.tbDateToCadeauO.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.tbDateToCadeauO.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.tbDateToCadeauO.Properties.DisplayFormat.FormatString = "MM-dd-yy H:mm"
        Me.tbDateToCadeauO.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.tbDateToCadeauO.Properties.EditFormat.FormatString = "MM-dd-yy H:mm"
        Me.tbDateToCadeauO.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.tbDateToCadeauO.Properties.Mask.EditMask = "MM-dd-yy H:mm"
        Me.tbDateToCadeauO.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.tbDateToCadeauO.Size = New System.Drawing.Size(199, 20)
        Me.tbDateToCadeauO.StyleController = Me.LayoutControl6
        Me.tbDateToCadeauO.TabIndex = 8
        '
        'tbIIDCadeauOrganisation
        '
        Me.tbIIDCadeauOrganisation.EditValue = "0"
        Me.tbIIDCadeauOrganisation.Enabled = False
        Me.tbIIDCadeauOrganisation.Location = New System.Drawing.Point(106, 44)
        Me.tbIIDCadeauOrganisation.Name = "tbIIDCadeauOrganisation"
        Me.tbIIDCadeauOrganisation.Properties.Appearance.BackColor = System.Drawing.Color.Azure
        Me.tbIIDCadeauOrganisation.Properties.Appearance.Options.UseBackColor = True
        Me.tbIIDCadeauOrganisation.Properties.Mask.EditMask = "n0"
        Me.tbIIDCadeauOrganisation.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.tbIIDCadeauOrganisation.Size = New System.Drawing.Size(199, 20)
        Me.tbIIDCadeauOrganisation.StyleController = Me.LayoutControl6
        Me.tbIIDCadeauOrganisation.TabIndex = 126
        '
        'LayoutControlGroup3
        '
        Me.LayoutControlGroup3.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup3.GroupBordersVisible = False
        Me.LayoutControlGroup3.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem23, Me.LayoutControlGroup11})
        Me.LayoutControlGroup3.Name = "Root"
        Me.LayoutControlGroup3.Size = New System.Drawing.Size(1326, 779)
        Me.LayoutControlGroup3.TextVisible = False
        '
        'LayoutControlItem23
        '
        Me.LayoutControlItem23.Control = Me.grdCadeauOrganisation
        Me.LayoutControlItem23.Location = New System.Drawing.Point(0, 140)
        Me.LayoutControlItem23.Name = "LayoutControlItem23"
        Me.LayoutControlItem23.Size = New System.Drawing.Size(1306, 619)
        Me.LayoutControlItem23.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem23.TextVisible = False
        '
        'LayoutControlGroup11
        '
        Me.LayoutControlGroup11.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem31, Me.LayoutControlItem59, Me.EmptySpaceItem10, Me.LayoutControlItem61, Me.LayoutControlItem60, Me.LayoutControlItem36, Me.LayoutControlItem37, Me.LayoutControlItem63})
        Me.LayoutControlGroup11.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup11.Name = "LayoutControlGroup11"
        Me.LayoutControlGroup11.Size = New System.Drawing.Size(1306, 140)
        Me.LayoutControlGroup11.Text = "Montant du cadeau pour l'organisation"
        '
        'LayoutControlItem31
        '
        Me.LayoutControlItem31.Control = Me.tbMontantCadeauO
        Me.LayoutControlItem31.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem31.CustomizationFormText = "Montant cadeau"
        Me.LayoutControlItem31.Location = New System.Drawing.Point(0, 72)
        Me.LayoutControlItem31.Name = "LayoutControlItem31"
        Me.LayoutControlItem31.Size = New System.Drawing.Size(285, 24)
        Me.LayoutControlItem31.Text = "Montant cadeau"
        Me.LayoutControlItem31.TextSize = New System.Drawing.Size(78, 13)
        '
        'LayoutControlItem59
        '
        Me.LayoutControlItem59.Control = Me.bMAJCadeauO
        Me.LayoutControlItem59.Location = New System.Drawing.Point(550, 0)
        Me.LayoutControlItem59.MinSize = New System.Drawing.Size(65, 26)
        Me.LayoutControlItem59.Name = "LayoutControlItem59"
        Me.LayoutControlItem59.Size = New System.Drawing.Size(133, 96)
        Me.LayoutControlItem59.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem59.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem59.TextVisible = False
        '
        'EmptySpaceItem10
        '
        Me.EmptySpaceItem10.AllowHotTrack = False
        Me.EmptySpaceItem10.Location = New System.Drawing.Point(683, 0)
        Me.EmptySpaceItem10.Name = "EmptySpaceItem10"
        Me.EmptySpaceItem10.Size = New System.Drawing.Size(599, 96)
        Me.EmptySpaceItem10.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlItem61
        '
        Me.LayoutControlItem61.Control = Me.bAjouterCadeauO
        Me.LayoutControlItem61.Location = New System.Drawing.Point(285, 0)
        Me.LayoutControlItem61.MinSize = New System.Drawing.Size(49, 26)
        Me.LayoutControlItem61.Name = "LayoutControlItem61"
        Me.LayoutControlItem61.Size = New System.Drawing.Size(138, 96)
        Me.LayoutControlItem61.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem61.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem61.TextVisible = False
        '
        'LayoutControlItem60
        '
        Me.LayoutControlItem60.Control = Me.bSupprimerCadeauO
        Me.LayoutControlItem60.Location = New System.Drawing.Point(423, 0)
        Me.LayoutControlItem60.MinSize = New System.Drawing.Size(61, 26)
        Me.LayoutControlItem60.Name = "LayoutControlItem60"
        Me.LayoutControlItem60.Size = New System.Drawing.Size(127, 96)
        Me.LayoutControlItem60.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem60.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem60.TextVisible = False
        '
        'LayoutControlItem36
        '
        Me.LayoutControlItem36.Control = Me.tbDateFromCadeauO
        Me.LayoutControlItem36.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem36.CustomizationFormText = "Date Du"
        Me.LayoutControlItem36.Location = New System.Drawing.Point(0, 24)
        Me.LayoutControlItem36.Name = "LayoutControlItem36"
        Me.LayoutControlItem36.Size = New System.Drawing.Size(285, 24)
        Me.LayoutControlItem36.Text = "Période du"
        Me.LayoutControlItem36.TextSize = New System.Drawing.Size(78, 13)
        '
        'LayoutControlItem37
        '
        Me.LayoutControlItem37.Control = Me.tbDateToCadeauO
        Me.LayoutControlItem37.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem37.CustomizationFormText = "Date au"
        Me.LayoutControlItem37.Location = New System.Drawing.Point(0, 48)
        Me.LayoutControlItem37.Name = "LayoutControlItem37"
        Me.LayoutControlItem37.Size = New System.Drawing.Size(285, 24)
        Me.LayoutControlItem37.Text = "Période au"
        Me.LayoutControlItem37.TextSize = New System.Drawing.Size(78, 13)
        '
        'LayoutControlItem63
        '
        Me.LayoutControlItem63.Control = Me.tbIIDCadeauOrganisation
        Me.LayoutControlItem63.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem63.CustomizationFormText = "Bonus par kilométrage démarre a"
        Me.LayoutControlItem63.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem63.Name = "LayoutControlItem63"
        Me.LayoutControlItem63.Size = New System.Drawing.Size(285, 24)
        Me.LayoutControlItem63.Text = "ID"
        Me.LayoutControlItem63.TextSize = New System.Drawing.Size(78, 13)
        '
        'XtraTabPage4
        '
        Me.XtraTabPage4.Controls.Add(Me.LayoutControl7)
        Me.XtraTabPage4.Name = "XtraTabPage4"
        Me.XtraTabPage4.Size = New System.Drawing.Size(1326, 779)
        Me.XtraTabPage4.Text = "Cadeau Livreur"
        '
        'LayoutControl7
        '
        Me.LayoutControl7.Controls.Add(Me.bAnnulerLivreurL)
        Me.LayoutControl7.Controls.Add(Me.cbLivreursCadeauL)
        Me.LayoutControl7.Controls.Add(Me.bMAJCadeauL)
        Me.LayoutControl7.Controls.Add(Me.bSupprimerCadeauL)
        Me.LayoutControl7.Controls.Add(Me.bAjouterCadeauL)
        Me.LayoutControl7.Controls.Add(Me.grdCadeauLivreur)
        Me.LayoutControl7.Controls.Add(Me.tbMontantCadeauL)
        Me.LayoutControl7.Controls.Add(Me.tbDateFromCadeauL)
        Me.LayoutControl7.Controls.Add(Me.tbDateToCadeauL)
        Me.LayoutControl7.Controls.Add(Me.tbIDCadeauLivreur)
        Me.LayoutControl7.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LayoutControl7.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControl7.Name = "LayoutControl7"
        Me.LayoutControl7.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = New System.Drawing.Rectangle(1144, 552, 650, 400)
        Me.LayoutControl7.Root = Me.LayoutControlGroup6
        Me.LayoutControl7.Size = New System.Drawing.Size(1326, 779)
        Me.LayoutControl7.TabIndex = 0
        Me.LayoutControl7.Text = "LayoutControl7"
        '
        'bAnnulerLivreurL
        '
        Me.bAnnulerLivreurL.Location = New System.Drawing.Point(594, 68)
        Me.bAnnulerLivreurL.Name = "bAnnulerLivreurL"
        Me.bAnnulerLivreurL.Size = New System.Drawing.Size(39, 22)
        Me.bAnnulerLivreurL.StyleController = Me.LayoutControl7
        Me.bAnnulerLivreurL.TabIndex = 141
        Me.bAnnulerLivreurL.Text = "..."
        '
        'cbLivreursCadeauL
        '
        Me.cbLivreursCadeauL.Location = New System.Drawing.Point(106, 68)
        Me.cbLivreursCadeauL.Name = "cbLivreursCadeauL"
        Me.cbLivreursCadeauL.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbLivreursCadeauL.Size = New System.Drawing.Size(484, 20)
        Me.cbLivreursCadeauL.StyleController = Me.LayoutControl7
        Me.cbLivreursCadeauL.TabIndex = 140
        '
        'bMAJCadeauL
        '
        Me.bMAJCadeauL.Enabled = False
        Me.bMAJCadeauL.Location = New System.Drawing.Point(912, 44)
        Me.bMAJCadeauL.Name = "bMAJCadeauL"
        Me.bMAJCadeauL.Size = New System.Drawing.Size(142, 118)
        Me.bMAJCadeauL.StyleController = Me.LayoutControl7
        Me.bMAJCadeauL.TabIndex = 138
        Me.bMAJCadeauL.Text = "Mise a jour"
        '
        'bSupprimerCadeauL
        '
        Me.bSupprimerCadeauL.Enabled = False
        Me.bSupprimerCadeauL.Location = New System.Drawing.Point(774, 44)
        Me.bSupprimerCadeauL.Name = "bSupprimerCadeauL"
        Me.bSupprimerCadeauL.Size = New System.Drawing.Size(134, 118)
        Me.bSupprimerCadeauL.StyleController = Me.LayoutControl7
        Me.bSupprimerCadeauL.TabIndex = 137
        Me.bSupprimerCadeauL.Text = "Supprimer"
        '
        'bAjouterCadeauL
        '
        Me.bAjouterCadeauL.Location = New System.Drawing.Point(637, 44)
        Me.bAjouterCadeauL.Name = "bAjouterCadeauL"
        Me.bAjouterCadeauL.Size = New System.Drawing.Size(133, 118)
        Me.bAjouterCadeauL.StyleController = Me.LayoutControl7
        Me.bAjouterCadeauL.TabIndex = 136
        Me.bAjouterCadeauL.Text = "Ajouter"
        '
        'grdCadeauLivreur
        '
        Me.grdCadeauLivreur.Location = New System.Drawing.Point(12, 178)
        Me.grdCadeauLivreur.MainView = Me.GridView7
        Me.grdCadeauLivreur.Name = "grdCadeauLivreur"
        Me.grdCadeauLivreur.Size = New System.Drawing.Size(1302, 589)
        Me.grdCadeauLivreur.TabIndex = 134
        Me.grdCadeauLivreur.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView7})
        '
        'GridView7
        '
        Me.GridView7.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colIDCadeauLivreur, Me.colIDLivreur, Me.GridColumn25, Me.colDateFrom, Me.colDateTo, Me.Montant1})
        Me.GridView7.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.GridView7.GridControl = Me.grdCadeauLivreur
        Me.GridView7.Name = "GridView7"
        Me.GridView7.OptionsBehavior.Editable = False
        Me.GridView7.OptionsCustomization.AllowGroup = False
        Me.GridView7.OptionsCustomization.AllowSort = False
        Me.GridView7.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridView7.OptionsSelection.UseIndicatorForSelection = False
        Me.GridView7.OptionsView.ShowAutoFilterRow = True
        Me.GridView7.OptionsView.ShowGroupPanel = False
        Me.GridView7.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.colIDLivreur, DevExpress.Data.ColumnSortOrder.Ascending)})
        '
        'colIDCadeauLivreur
        '
        Me.colIDCadeauLivreur.Caption = "IDCadeauLivreur"
        Me.colIDCadeauLivreur.DisplayFormat.FormatString = "c"
        Me.colIDCadeauLivreur.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.colIDCadeauLivreur.FieldName = "IDCadeauLivreur"
        Me.colIDCadeauLivreur.Name = "colIDCadeauLivreur"
        Me.colIDCadeauLivreur.Width = 94
        '
        'colIDLivreur
        '
        Me.colIDLivreur.Caption = "IDLivreur"
        Me.colIDLivreur.FieldName = "IDLivreur"
        Me.colIDLivreur.Name = "colIDLivreur"
        Me.colIDLivreur.Width = 94
        '
        'GridColumn25
        '
        Me.GridColumn25.Caption = "Livreur"
        Me.GridColumn25.FieldName = "Livreur"
        Me.GridColumn25.Name = "GridColumn25"
        Me.GridColumn25.Visible = True
        Me.GridColumn25.VisibleIndex = 0
        '
        'colDateFrom
        '
        Me.colDateFrom.Caption = "Date du"
        Me.colDateFrom.DisplayFormat.FormatString = "c"
        Me.colDateFrom.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.colDateFrom.FieldName = "DateFrom"
        Me.colDateFrom.Name = "colDateFrom"
        Me.colDateFrom.Visible = True
        Me.colDateFrom.VisibleIndex = 1
        Me.colDateFrom.Width = 94
        '
        'colDateTo
        '
        Me.colDateTo.Caption = "Date au"
        Me.colDateTo.DisplayFormat.FormatString = "c"
        Me.colDateTo.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.colDateTo.FieldName = "DateTo"
        Me.colDateTo.Name = "colDateTo"
        Me.colDateTo.Visible = True
        Me.colDateTo.VisibleIndex = 2
        '
        'Montant1
        '
        Me.Montant1.Caption = "Montant"
        Me.Montant1.FieldName = "Montant"
        Me.Montant1.Name = "Montant1"
        Me.Montant1.Visible = True
        Me.Montant1.VisibleIndex = 3
        '
        'tbMontantCadeauL
        '
        Me.tbMontantCadeauL.EditValue = "0"
        Me.tbMontantCadeauL.Location = New System.Drawing.Point(106, 142)
        Me.tbMontantCadeauL.Name = "tbMontantCadeauL"
        Me.tbMontantCadeauL.Properties.DisplayFormat.FormatString = "c"
        Me.tbMontantCadeauL.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.tbMontantCadeauL.Properties.EditFormat.FormatString = "c"
        Me.tbMontantCadeauL.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.tbMontantCadeauL.Properties.Mask.EditMask = "c"
        Me.tbMontantCadeauL.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.tbMontantCadeauL.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.tbMontantCadeauL.Size = New System.Drawing.Size(527, 20)
        Me.tbMontantCadeauL.StyleController = Me.LayoutControl7
        Me.tbMontantCadeauL.TabIndex = 127
        '
        'tbDateFromCadeauL
        '
        Me.tbDateFromCadeauL.EditValue = Nothing
        Me.tbDateFromCadeauL.Location = New System.Drawing.Point(106, 94)
        Me.tbDateFromCadeauL.Name = "tbDateFromCadeauL"
        Me.tbDateFromCadeauL.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.tbDateFromCadeauL.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.tbDateFromCadeauL.Properties.DisplayFormat.FormatString = "MM-dd-yy H:mm"
        Me.tbDateFromCadeauL.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.tbDateFromCadeauL.Properties.EditFormat.FormatString = "MM-dd-yy H:mm"
        Me.tbDateFromCadeauL.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.tbDateFromCadeauL.Properties.Mask.EditMask = "MM-dd-yy H:mm"
        Me.tbDateFromCadeauL.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.tbDateFromCadeauL.Size = New System.Drawing.Size(527, 20)
        Me.tbDateFromCadeauL.StyleController = Me.LayoutControl7
        Me.tbDateFromCadeauL.TabIndex = 8
        '
        'tbDateToCadeauL
        '
        Me.tbDateToCadeauL.EditValue = Nothing
        Me.tbDateToCadeauL.Location = New System.Drawing.Point(106, 118)
        Me.tbDateToCadeauL.Name = "tbDateToCadeauL"
        Me.tbDateToCadeauL.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.tbDateToCadeauL.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.tbDateToCadeauL.Properties.DisplayFormat.FormatString = "MM-dd-yy H:mm"
        Me.tbDateToCadeauL.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.tbDateToCadeauL.Properties.EditFormat.FormatString = "MM-dd-yy H:mm"
        Me.tbDateToCadeauL.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.tbDateToCadeauL.Properties.Mask.EditMask = "MM-dd-yy H:mm"
        Me.tbDateToCadeauL.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.tbDateToCadeauL.Size = New System.Drawing.Size(527, 20)
        Me.tbDateToCadeauL.StyleController = Me.LayoutControl7
        Me.tbDateToCadeauL.TabIndex = 8
        '
        'tbIDCadeauLivreur
        '
        Me.tbIDCadeauLivreur.EditValue = "0"
        Me.tbIDCadeauLivreur.Enabled = False
        Me.tbIDCadeauLivreur.Location = New System.Drawing.Point(106, 44)
        Me.tbIDCadeauLivreur.Name = "tbIDCadeauLivreur"
        Me.tbIDCadeauLivreur.Properties.Appearance.BackColor = System.Drawing.Color.Azure
        Me.tbIDCadeauLivreur.Properties.Appearance.Options.UseBackColor = True
        Me.tbIDCadeauLivreur.Properties.Mask.EditMask = "n0"
        Me.tbIDCadeauLivreur.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.tbIDCadeauLivreur.Size = New System.Drawing.Size(527, 20)
        Me.tbIDCadeauLivreur.StyleController = Me.LayoutControl7
        Me.tbIDCadeauLivreur.TabIndex = 126
        '
        'LayoutControlGroup6
        '
        Me.LayoutControlGroup6.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup6.GroupBordersVisible = False
        Me.LayoutControlGroup6.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem24, Me.LayoutControlGroup12})
        Me.LayoutControlGroup6.Name = "Root"
        Me.LayoutControlGroup6.Size = New System.Drawing.Size(1326, 779)
        Me.LayoutControlGroup6.TextVisible = False
        '
        'LayoutControlItem24
        '
        Me.LayoutControlItem24.Control = Me.grdCadeauLivreur
        Me.LayoutControlItem24.Location = New System.Drawing.Point(0, 166)
        Me.LayoutControlItem24.Name = "LayoutControlItem24"
        Me.LayoutControlItem24.Size = New System.Drawing.Size(1306, 593)
        Me.LayoutControlItem24.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem24.TextVisible = False
        '
        'LayoutControlGroup12
        '
        Me.LayoutControlGroup12.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem34, Me.LayoutControlItem38, Me.LayoutControlItem44, Me.LayoutControlItem56, Me.LayoutControlItem58, Me.LayoutControlItem57, Me.EmptySpaceItem9, Me.LayoutControlItem62, Me.LayoutControlItem64, Me.LayoutControlItem66})
        Me.LayoutControlGroup12.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup12.Name = "LayoutControlGroup12"
        Me.LayoutControlGroup12.Size = New System.Drawing.Size(1306, 166)
        Me.LayoutControlGroup12.Text = "Montant du cadeau pour un livreur"
        '
        'LayoutControlItem34
        '
        Me.LayoutControlItem34.Control = Me.tbMontantCadeauL
        Me.LayoutControlItem34.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem34.CustomizationFormText = "Montant cadeau"
        Me.LayoutControlItem34.Location = New System.Drawing.Point(0, 98)
        Me.LayoutControlItem34.Name = "LayoutControlItem34"
        Me.LayoutControlItem34.Size = New System.Drawing.Size(613, 24)
        Me.LayoutControlItem34.Text = "Montant cadeau"
        Me.LayoutControlItem34.TextSize = New System.Drawing.Size(78, 13)
        '
        'LayoutControlItem38
        '
        Me.LayoutControlItem38.Control = Me.tbDateFromCadeauL
        Me.LayoutControlItem38.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem38.CustomizationFormText = "Date Du"
        Me.LayoutControlItem38.Location = New System.Drawing.Point(0, 50)
        Me.LayoutControlItem38.Name = "LayoutControlItem38"
        Me.LayoutControlItem38.Size = New System.Drawing.Size(613, 24)
        Me.LayoutControlItem38.Text = "Période du"
        Me.LayoutControlItem38.TextSize = New System.Drawing.Size(78, 13)
        '
        'LayoutControlItem44
        '
        Me.LayoutControlItem44.Control = Me.tbDateToCadeauL
        Me.LayoutControlItem44.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem44.CustomizationFormText = "Date Du"
        Me.LayoutControlItem44.Location = New System.Drawing.Point(0, 74)
        Me.LayoutControlItem44.Name = "LayoutControlItem44"
        Me.LayoutControlItem44.Size = New System.Drawing.Size(613, 24)
        Me.LayoutControlItem44.Text = "Période au"
        Me.LayoutControlItem44.TextSize = New System.Drawing.Size(78, 13)
        '
        'LayoutControlItem56
        '
        Me.LayoutControlItem56.Control = Me.bMAJCadeauL
        Me.LayoutControlItem56.Location = New System.Drawing.Point(888, 0)
        Me.LayoutControlItem56.MinSize = New System.Drawing.Size(65, 26)
        Me.LayoutControlItem56.Name = "LayoutControlItem56"
        Me.LayoutControlItem56.Size = New System.Drawing.Size(146, 122)
        Me.LayoutControlItem56.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem56.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem56.TextVisible = False
        '
        'LayoutControlItem58
        '
        Me.LayoutControlItem58.Control = Me.bAjouterCadeauL
        Me.LayoutControlItem58.Location = New System.Drawing.Point(613, 0)
        Me.LayoutControlItem58.MinSize = New System.Drawing.Size(49, 26)
        Me.LayoutControlItem58.Name = "LayoutControlItem58"
        Me.LayoutControlItem58.Size = New System.Drawing.Size(137, 122)
        Me.LayoutControlItem58.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem58.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem58.TextVisible = False
        '
        'LayoutControlItem57
        '
        Me.LayoutControlItem57.Control = Me.bSupprimerCadeauL
        Me.LayoutControlItem57.Location = New System.Drawing.Point(750, 0)
        Me.LayoutControlItem57.MinSize = New System.Drawing.Size(61, 26)
        Me.LayoutControlItem57.Name = "LayoutControlItem57"
        Me.LayoutControlItem57.Size = New System.Drawing.Size(138, 122)
        Me.LayoutControlItem57.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem57.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem57.TextVisible = False
        '
        'EmptySpaceItem9
        '
        Me.EmptySpaceItem9.AllowHotTrack = False
        Me.EmptySpaceItem9.Location = New System.Drawing.Point(1034, 0)
        Me.EmptySpaceItem9.Name = "EmptySpaceItem9"
        Me.EmptySpaceItem9.Size = New System.Drawing.Size(248, 122)
        Me.EmptySpaceItem9.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlItem62
        '
        Me.LayoutControlItem62.Control = Me.tbIDCadeauLivreur
        Me.LayoutControlItem62.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem62.CustomizationFormText = "Bonus par kilométrage démarre a"
        Me.LayoutControlItem62.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem62.Name = "LayoutControlItem62"
        Me.LayoutControlItem62.Size = New System.Drawing.Size(613, 24)
        Me.LayoutControlItem62.Text = "ID"
        Me.LayoutControlItem62.TextSize = New System.Drawing.Size(78, 13)
        '
        'LayoutControlItem64
        '
        Me.LayoutControlItem64.Control = Me.cbLivreursCadeauL
        Me.LayoutControlItem64.Location = New System.Drawing.Point(0, 24)
        Me.LayoutControlItem64.Name = "LayoutControlItem64"
        Me.LayoutControlItem64.Size = New System.Drawing.Size(570, 26)
        Me.LayoutControlItem64.Text = "Livreur"
        Me.LayoutControlItem64.TextSize = New System.Drawing.Size(78, 13)
        '
        'LayoutControlItem66
        '
        Me.LayoutControlItem66.Control = Me.bAnnulerLivreurL
        Me.LayoutControlItem66.Location = New System.Drawing.Point(570, 24)
        Me.LayoutControlItem66.Name = "LayoutControlItem66"
        Me.LayoutControlItem66.Size = New System.Drawing.Size(43, 26)
        Me.LayoutControlItem66.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem66.TextVisible = False
        '
        'XtraTabPage5
        '
        Me.XtraTabPage5.Controls.Add(Me.LayoutControl8)
        Me.XtraTabPage5.Name = "XtraTabPage5"
        Me.XtraTabPage5.PageVisible = False
        Me.XtraTabPage5.Size = New System.Drawing.Size(1326, 779)
        Me.XtraTabPage5.Text = "Prix fixe de fin de semaine / Livreurs"
        '
        'LayoutControl8
        '
        Me.LayoutControl8.Controls.Add(Me.bAjouterPrixFixe)
        Me.LayoutControl8.Controls.Add(Me.bSupprimerPrixFixe)
        Me.LayoutControl8.Controls.Add(Me.bMiseAJourPrixFixe)
        Me.LayoutControl8.Controls.Add(Me.tbIDPrixFixe)
        Me.LayoutControl8.Controls.Add(Me.cbLivreursP)
        Me.LayoutControl8.Controls.Add(Me.bAnnulerLivreurP)
        Me.LayoutControl8.Controls.Add(Me.tbMontantPrixFixe)
        Me.LayoutControl8.Controls.Add(Me.grdPrixFixe)
        Me.LayoutControl8.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LayoutControl8.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControl8.Name = "LayoutControl8"
        Me.LayoutControl8.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = New System.Drawing.Rectangle(1164, 557, 650, 400)
        Me.LayoutControl8.Root = Me.LayoutControlGroup16
        Me.LayoutControl8.Size = New System.Drawing.Size(1326, 779)
        Me.LayoutControl8.TabIndex = 0
        Me.LayoutControl8.Text = "LayoutControl8"
        '
        'bAjouterPrixFixe
        '
        Me.bAjouterPrixFixe.Location = New System.Drawing.Point(462, 44)
        Me.bAjouterPrixFixe.Name = "bAjouterPrixFixe"
        Me.bAjouterPrixFixe.Size = New System.Drawing.Size(133, 69)
        Me.bAjouterPrixFixe.StyleController = Me.LayoutControl8
        Me.bAjouterPrixFixe.TabIndex = 125
        Me.bAjouterPrixFixe.Text = "Ajouter"
        '
        'bSupprimerPrixFixe
        '
        Me.bSupprimerPrixFixe.Enabled = False
        Me.bSupprimerPrixFixe.Location = New System.Drawing.Point(599, 44)
        Me.bSupprimerPrixFixe.Name = "bSupprimerPrixFixe"
        Me.bSupprimerPrixFixe.Size = New System.Drawing.Size(121, 69)
        Me.bSupprimerPrixFixe.StyleController = Me.LayoutControl8
        Me.bSupprimerPrixFixe.TabIndex = 126
        Me.bSupprimerPrixFixe.Text = "Supprimer"
        '
        'bMiseAJourPrixFixe
        '
        Me.bMiseAJourPrixFixe.Enabled = False
        Me.bMiseAJourPrixFixe.Location = New System.Drawing.Point(724, 44)
        Me.bMiseAJourPrixFixe.Name = "bMiseAJourPrixFixe"
        Me.bMiseAJourPrixFixe.Size = New System.Drawing.Size(122, 69)
        Me.bMiseAJourPrixFixe.StyleController = Me.LayoutControl8
        Me.bMiseAJourPrixFixe.TabIndex = 131
        Me.bMiseAJourPrixFixe.Text = "Mise a jour"
        '
        'tbIDPrixFixe
        '
        Me.tbIDPrixFixe.EditValue = "0"
        Me.tbIDPrixFixe.Enabled = False
        Me.tbIDPrixFixe.Location = New System.Drawing.Point(168, 44)
        Me.tbIDPrixFixe.Name = "tbIDPrixFixe"
        Me.tbIDPrixFixe.Properties.Appearance.BackColor = System.Drawing.Color.Azure
        Me.tbIDPrixFixe.Properties.Appearance.Options.UseBackColor = True
        Me.tbIDPrixFixe.Properties.Mask.EditMask = "n0"
        Me.tbIDPrixFixe.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.tbIDPrixFixe.Size = New System.Drawing.Size(290, 20)
        Me.tbIDPrixFixe.StyleController = Me.LayoutControl8
        Me.tbIDPrixFixe.TabIndex = 126
        '
        'cbLivreursP
        '
        Me.cbLivreursP.Location = New System.Drawing.Point(168, 68)
        Me.cbLivreursP.Name = "cbLivreursP"
        Me.cbLivreursP.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbLivreursP.Size = New System.Drawing.Size(252, 20)
        Me.cbLivreursP.StyleController = Me.LayoutControl8
        Me.cbLivreursP.TabIndex = 133
        '
        'bAnnulerLivreurP
        '
        Me.bAnnulerLivreurP.Location = New System.Drawing.Point(424, 68)
        Me.bAnnulerLivreurP.Name = "bAnnulerLivreurP"
        Me.bAnnulerLivreurP.Size = New System.Drawing.Size(34, 22)
        Me.bAnnulerLivreurP.StyleController = Me.LayoutControl8
        Me.bAnnulerLivreurP.TabIndex = 134
        Me.bAnnulerLivreurP.Text = "..."
        '
        'tbMontantPrixFixe
        '
        Me.tbMontantPrixFixe.EditValue = "0"
        Me.tbMontantPrixFixe.Location = New System.Drawing.Point(180, 126)
        Me.tbMontantPrixFixe.Name = "tbMontantPrixFixe"
        Me.tbMontantPrixFixe.Properties.DisplayFormat.FormatString = "c"
        Me.tbMontantPrixFixe.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.tbMontantPrixFixe.Properties.EditFormat.FormatString = "c"
        Me.tbMontantPrixFixe.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.tbMontantPrixFixe.Properties.Mask.EditMask = "c"
        Me.tbMontantPrixFixe.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.tbMontantPrixFixe.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.tbMontantPrixFixe.Size = New System.Drawing.Size(266, 20)
        Me.tbMontantPrixFixe.StyleController = Me.LayoutControl8
        Me.tbMontantPrixFixe.TabIndex = 124
        '
        'grdPrixFixe
        '
        Me.grdPrixFixe.Location = New System.Drawing.Point(24, 162)
        Me.grdPrixFixe.MainView = Me.GridView31
        Me.grdPrixFixe.Name = "grdPrixFixe"
        Me.grdPrixFixe.Size = New System.Drawing.Size(1278, 593)
        Me.grdPrixFixe.TabIndex = 10
        Me.grdPrixFixe.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView31})
        '
        'GridView31
        '
        Me.GridView31.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.IDHoraire1, Me.GridColumn301, Me.IDOrganisation1, Me.IDLivreur1, Me.Livreur1, Me.IDJourSemaine1})
        Me.GridView31.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.GridView31.GridControl = Me.grdPrixFixe
        Me.GridView31.Name = "GridView31"
        Me.GridView31.OptionsBehavior.Editable = False
        Me.GridView31.OptionsCustomization.AllowGroup = False
        Me.GridView31.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridView31.OptionsSelection.UseIndicatorForSelection = False
        Me.GridView31.OptionsView.ShowAutoFilterRow = True
        Me.GridView31.OptionsView.ShowGroupPanel = False
        '
        'IDHoraire1
        '
        Me.IDHoraire1.Caption = "ID Prix Fixe"
        Me.IDHoraire1.FieldName = "IDPrixFixe"
        Me.IDHoraire1.Name = "IDHoraire1"
        Me.IDHoraire1.Width = 94
        '
        'GridColumn301
        '
        Me.GridColumn301.Caption = "Montant Livreur"
        Me.GridColumn301.DisplayFormat.FormatString = "c"
        Me.GridColumn301.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.GridColumn301.FieldName = "MontantLivreur"
        Me.GridColumn301.Name = "GridColumn301"
        Me.GridColumn301.Visible = True
        Me.GridColumn301.VisibleIndex = 1
        '
        'IDOrganisation1
        '
        Me.IDOrganisation1.Caption = "IDOrganisation"
        Me.IDOrganisation1.FieldName = "IDOrganisation"
        Me.IDOrganisation1.Name = "IDOrganisation1"
        '
        'IDLivreur1
        '
        Me.IDLivreur1.Caption = "ID Livreur"
        Me.IDLivreur1.FieldName = "IDLivreur"
        Me.IDLivreur1.Name = "IDLivreur1"
        '
        'Livreur1
        '
        Me.Livreur1.Caption = "Livreur"
        Me.Livreur1.FieldName = "Livreur"
        Me.Livreur1.Name = "Livreur1"
        Me.Livreur1.Visible = True
        Me.Livreur1.VisibleIndex = 0
        '
        'IDJourSemaine1
        '
        Me.IDJourSemaine1.Caption = "IDJourSemaine"
        Me.IDJourSemaine1.FieldName = "IDJourSemaine"
        Me.IDJourSemaine1.Name = "IDJourSemaine1"
        '
        'LayoutControlGroup16
        '
        Me.LayoutControlGroup16.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup16.GroupBordersVisible = False
        Me.LayoutControlGroup16.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlGroup18})
        Me.LayoutControlGroup16.Name = "Root"
        Me.LayoutControlGroup16.Size = New System.Drawing.Size(1326, 779)
        Me.LayoutControlGroup16.TextVisible = False
        '
        'LayoutControlGroup18
        '
        Me.LayoutControlGroup18.CustomizationFormText = "Ajouter un Livreur a la liste"
        Me.LayoutControlGroup18.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.EmptySpaceItem16, Me.LayoutControlItem74, Me.LayoutControlItem75, Me.LayoutControlItem76, Me.LayoutControlItem86, Me.LayoutControlItem87, Me.LayoutControlItem88, Me.LayoutControlGroup19, Me.LayoutControlItem93, Me.EmptySpaceItem17})
        Me.LayoutControlGroup18.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup18.Name = "LayoutControlGroup18"
        Me.LayoutControlGroup18.Size = New System.Drawing.Size(1306, 759)
        Me.LayoutControlGroup18.Text = "Ajouter un Livreur a la liste"
        '
        'EmptySpaceItem16
        '
        Me.EmptySpaceItem16.AllowHotTrack = False
        Me.EmptySpaceItem16.CustomizationFormText = "EmptySpaceItem1"
        Me.EmptySpaceItem16.Location = New System.Drawing.Point(826, 0)
        Me.EmptySpaceItem16.Name = "EmptySpaceItem16"
        Me.EmptySpaceItem16.Size = New System.Drawing.Size(456, 118)
        Me.EmptySpaceItem16.Text = "EmptySpaceItem1"
        Me.EmptySpaceItem16.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlItem74
        '
        Me.LayoutControlItem74.Control = Me.bAjouterPrixFixe
        Me.LayoutControlItem74.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem74.CustomizationFormText = "LayoutControlItem19"
        Me.LayoutControlItem74.Location = New System.Drawing.Point(438, 0)
        Me.LayoutControlItem74.MinSize = New System.Drawing.Size(49, 26)
        Me.LayoutControlItem74.Name = "LayoutControlItem74"
        Me.LayoutControlItem74.Size = New System.Drawing.Size(137, 73)
        Me.LayoutControlItem74.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem74.Text = "LayoutControlItem19"
        Me.LayoutControlItem74.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem74.TextVisible = False
        '
        'LayoutControlItem75
        '
        Me.LayoutControlItem75.Control = Me.bSupprimerPrixFixe
        Me.LayoutControlItem75.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem75.CustomizationFormText = "LayoutControlItem21"
        Me.LayoutControlItem75.Location = New System.Drawing.Point(575, 0)
        Me.LayoutControlItem75.MinSize = New System.Drawing.Size(61, 26)
        Me.LayoutControlItem75.Name = "LayoutControlItem75"
        Me.LayoutControlItem75.Size = New System.Drawing.Size(125, 73)
        Me.LayoutControlItem75.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem75.Text = "LayoutControlItem21"
        Me.LayoutControlItem75.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem75.TextVisible = False
        '
        'LayoutControlItem76
        '
        Me.LayoutControlItem76.Control = Me.bMiseAJourPrixFixe
        Me.LayoutControlItem76.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem76.CustomizationFormText = "LayoutControlItem50"
        Me.LayoutControlItem76.Location = New System.Drawing.Point(700, 0)
        Me.LayoutControlItem76.MinSize = New System.Drawing.Size(65, 26)
        Me.LayoutControlItem76.Name = "LayoutControlItem76"
        Me.LayoutControlItem76.Size = New System.Drawing.Size(126, 73)
        Me.LayoutControlItem76.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem76.Text = "LayoutControlItem50"
        Me.LayoutControlItem76.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem76.TextVisible = False
        '
        'LayoutControlItem86
        '
        Me.LayoutControlItem86.Control = Me.tbIDPrixFixe
        Me.LayoutControlItem86.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem86.CustomizationFormText = "Bonus par kilométrage démarre a"
        Me.LayoutControlItem86.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem86.Name = "LayoutControlItem86"
        Me.LayoutControlItem86.Size = New System.Drawing.Size(438, 24)
        Me.LayoutControlItem86.Text = "IDPrixFixe"
        Me.LayoutControlItem86.TextSize = New System.Drawing.Size(140, 13)
        '
        'LayoutControlItem87
        '
        Me.LayoutControlItem87.Control = Me.cbLivreursP
        Me.LayoutControlItem87.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem87.CustomizationFormText = "Livreur"
        Me.LayoutControlItem87.Location = New System.Drawing.Point(0, 24)
        Me.LayoutControlItem87.Name = "LayoutControlItem87"
        Me.LayoutControlItem87.Size = New System.Drawing.Size(400, 26)
        Me.LayoutControlItem87.Text = "Livreur"
        Me.LayoutControlItem87.TextSize = New System.Drawing.Size(140, 13)
        '
        'LayoutControlItem88
        '
        Me.LayoutControlItem88.Control = Me.bAnnulerLivreurP
        Me.LayoutControlItem88.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem88.CustomizationFormText = "LayoutControlItem17"
        Me.LayoutControlItem88.Location = New System.Drawing.Point(400, 24)
        Me.LayoutControlItem88.Name = "LayoutControlItem88"
        Me.LayoutControlItem88.Size = New System.Drawing.Size(38, 26)
        Me.LayoutControlItem88.Text = "LayoutControlItem17"
        Me.LayoutControlItem88.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem88.TextVisible = False
        '
        'LayoutControlGroup19
        '
        Me.LayoutControlGroup19.CustomizationFormText = "Configuration Horaire"
        Me.LayoutControlGroup19.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem89})
        Me.LayoutControlGroup19.Location = New System.Drawing.Point(0, 50)
        Me.LayoutControlGroup19.Name = "LayoutControlGroup19"
        Me.LayoutControlGroup19.Size = New System.Drawing.Size(438, 68)
        Me.LayoutControlGroup19.Text = "Configuration Horaire"
        '
        'LayoutControlItem89
        '
        Me.LayoutControlItem89.Control = Me.tbMontantPrixFixe
        Me.LayoutControlItem89.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem89.CustomizationFormText = "Bonus Amount"
        Me.LayoutControlItem89.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem89.Name = "LayoutControlItem89"
        Me.LayoutControlItem89.Size = New System.Drawing.Size(414, 24)
        Me.LayoutControlItem89.Text = "Montant fixe / fin de semaine"
        Me.LayoutControlItem89.TextSize = New System.Drawing.Size(140, 13)
        '
        'LayoutControlItem93
        '
        Me.LayoutControlItem93.Control = Me.grdPrixFixe
        Me.LayoutControlItem93.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem93.CustomizationFormText = "LayoutControlItem16"
        Me.LayoutControlItem93.Location = New System.Drawing.Point(0, 118)
        Me.LayoutControlItem93.Name = "LayoutControlItem93"
        Me.LayoutControlItem93.Size = New System.Drawing.Size(1282, 597)
        Me.LayoutControlItem93.Text = "LayoutControlItem16"
        Me.LayoutControlItem93.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem93.TextVisible = False
        '
        'EmptySpaceItem17
        '
        Me.EmptySpaceItem17.AllowHotTrack = False
        Me.EmptySpaceItem17.Location = New System.Drawing.Point(438, 73)
        Me.EmptySpaceItem17.Name = "EmptySpaceItem17"
        Me.EmptySpaceItem17.Size = New System.Drawing.Size(388, 45)
        Me.EmptySpaceItem17.TextSize = New System.Drawing.Size(0, 0)
        '
        'XtraTabPage6
        '
        Me.XtraTabPage6.Controls.Add(Me.LayoutControl9)
        Me.XtraTabPage6.Name = "XtraTabPage6"
        Me.XtraTabPage6.PageVisible = False
        Me.XtraTabPage6.Size = New System.Drawing.Size(1326, 779)
        Me.XtraTabPage6.Text = "Par jour"
        '
        'LayoutControl9
        '
        Me.LayoutControl9.Controls.Add(Me.bAjouterChargeParJour)
        Me.LayoutControl9.Controls.Add(Me.bSupprimerChargeParJour)
        Me.LayoutControl9.Controls.Add(Me.bMiseAJourChargeParJour)
        Me.LayoutControl9.Controls.Add(Me.tbIDHoraireJ)
        Me.LayoutControl9.Controls.Add(Me.cbLivreursJ)
        Me.LayoutControl9.Controls.Add(Me.bAnnulerLivreurH)
        Me.LayoutControl9.Controls.Add(Me.tbMontantParHeureLivreurJ)
        Me.LayoutControl9.Controls.Add(Me.tbMontantParHeureOrganisationJ)
        Me.LayoutControl9.Controls.Add(Me.tbDateJ)
        Me.LayoutControl9.Controls.Add(Me.grdParJour)
        Me.LayoutControl9.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LayoutControl9.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControl9.Name = "LayoutControl9"
        Me.LayoutControl9.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = New System.Drawing.Rectangle(1270, 589, 650, 400)
        Me.LayoutControl9.Root = Me.LayoutControlGroup17
        Me.LayoutControl9.Size = New System.Drawing.Size(1326, 779)
        Me.LayoutControl9.TabIndex = 0
        Me.LayoutControl9.Text = "LayoutControl9"
        '
        'bAjouterChargeParJour
        '
        Me.bAjouterChargeParJour.Location = New System.Drawing.Point(462, 44)
        Me.bAjouterChargeParJour.Name = "bAjouterChargeParJour"
        Me.bAjouterChargeParJour.Size = New System.Drawing.Size(133, 56)
        Me.bAjouterChargeParJour.StyleController = Me.LayoutControl9
        Me.bAjouterChargeParJour.TabIndex = 125
        Me.bAjouterChargeParJour.Text = "Ajouter"
        '
        'bSupprimerChargeParJour
        '
        Me.bSupprimerChargeParJour.Enabled = False
        Me.bSupprimerChargeParJour.Location = New System.Drawing.Point(599, 44)
        Me.bSupprimerChargeParJour.Name = "bSupprimerChargeParJour"
        Me.bSupprimerChargeParJour.Size = New System.Drawing.Size(121, 56)
        Me.bSupprimerChargeParJour.StyleController = Me.LayoutControl9
        Me.bSupprimerChargeParJour.TabIndex = 126
        Me.bSupprimerChargeParJour.Text = "Supprimer"
        '
        'bMiseAJourChargeParJour
        '
        Me.bMiseAJourChargeParJour.Enabled = False
        Me.bMiseAJourChargeParJour.Location = New System.Drawing.Point(724, 44)
        Me.bMiseAJourChargeParJour.Name = "bMiseAJourChargeParJour"
        Me.bMiseAJourChargeParJour.Size = New System.Drawing.Size(122, 56)
        Me.bMiseAJourChargeParJour.StyleController = Me.LayoutControl9
        Me.bMiseAJourChargeParJour.TabIndex = 131
        Me.bMiseAJourChargeParJour.Text = "Mise a jour"
        '
        'tbIDHoraireJ
        '
        Me.tbIDHoraireJ.AllowDrop = True
        Me.tbIDHoraireJ.EditValue = "0"
        Me.tbIDHoraireJ.Enabled = False
        Me.tbIDHoraireJ.Location = New System.Drawing.Point(171, 44)
        Me.tbIDHoraireJ.Name = "tbIDHoraireJ"
        Me.tbIDHoraireJ.Properties.Appearance.BackColor = System.Drawing.Color.Azure
        Me.tbIDHoraireJ.Properties.Appearance.Options.UseBackColor = True
        Me.tbIDHoraireJ.Properties.Mask.EditMask = "n0"
        Me.tbIDHoraireJ.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.tbIDHoraireJ.Size = New System.Drawing.Size(287, 20)
        Me.tbIDHoraireJ.StyleController = Me.LayoutControl9
        Me.tbIDHoraireJ.TabIndex = 126
        '
        'cbLivreursJ
        '
        Me.cbLivreursJ.Location = New System.Drawing.Point(171, 68)
        Me.cbLivreursJ.Name = "cbLivreursJ"
        Me.cbLivreursJ.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbLivreursJ.Size = New System.Drawing.Size(249, 20)
        Me.cbLivreursJ.StyleController = Me.LayoutControl9
        Me.cbLivreursJ.TabIndex = 133
        '
        'bAnnulerLivreurH
        '
        Me.bAnnulerLivreurH.Location = New System.Drawing.Point(424, 68)
        Me.bAnnulerLivreurH.Name = "bAnnulerLivreurH"
        Me.bAnnulerLivreurH.Size = New System.Drawing.Size(34, 22)
        Me.bAnnulerLivreurH.StyleController = Me.LayoutControl9
        Me.bAnnulerLivreurH.TabIndex = 134
        Me.bAnnulerLivreurH.Text = "..."
        '
        'tbMontantParHeureLivreurJ
        '
        Me.tbMontantParHeureLivreurJ.EditValue = "0"
        Me.tbMontantParHeureLivreurJ.Location = New System.Drawing.Point(183, 150)
        Me.tbMontantParHeureLivreurJ.Name = "tbMontantParHeureLivreurJ"
        Me.tbMontantParHeureLivreurJ.Properties.DisplayFormat.FormatString = "c"
        Me.tbMontantParHeureLivreurJ.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.tbMontantParHeureLivreurJ.Properties.EditFormat.FormatString = "c"
        Me.tbMontantParHeureLivreurJ.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.tbMontantParHeureLivreurJ.Properties.Mask.EditMask = "c"
        Me.tbMontantParHeureLivreurJ.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.tbMontantParHeureLivreurJ.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.tbMontantParHeureLivreurJ.Size = New System.Drawing.Size(263, 20)
        Me.tbMontantParHeureLivreurJ.StyleController = Me.LayoutControl9
        Me.tbMontantParHeureLivreurJ.TabIndex = 124
        '
        'tbMontantParHeureOrganisationJ
        '
        Me.tbMontantParHeureOrganisationJ.EditValue = "0"
        Me.tbMontantParHeureOrganisationJ.Location = New System.Drawing.Point(183, 174)
        Me.tbMontantParHeureOrganisationJ.Name = "tbMontantParHeureOrganisationJ"
        Me.tbMontantParHeureOrganisationJ.Properties.DisplayFormat.FormatString = "c"
        Me.tbMontantParHeureOrganisationJ.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.tbMontantParHeureOrganisationJ.Properties.EditFormat.FormatString = "c"
        Me.tbMontantParHeureOrganisationJ.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.tbMontantParHeureOrganisationJ.Properties.Mask.EditMask = "c"
        Me.tbMontantParHeureOrganisationJ.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.tbMontantParHeureOrganisationJ.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.tbMontantParHeureOrganisationJ.Size = New System.Drawing.Size(263, 20)
        Me.tbMontantParHeureOrganisationJ.StyleController = Me.LayoutControl9
        Me.tbMontantParHeureOrganisationJ.TabIndex = 124
        '
        'tbDateJ
        '
        Me.tbDateJ.EditValue = Nothing
        Me.tbDateJ.Location = New System.Drawing.Point(183, 126)
        Me.tbDateJ.Name = "tbDateJ"
        Me.tbDateJ.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.tbDateJ.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.tbDateJ.Properties.DisplayFormat.FormatString = "MM-dd-yy"
        Me.tbDateJ.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.tbDateJ.Properties.EditFormat.FormatString = "MM-dd-yy"
        Me.tbDateJ.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.tbDateJ.Properties.Mask.EditMask = "MM-dd-yy"
        Me.tbDateJ.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.tbDateJ.Size = New System.Drawing.Size(263, 20)
        Me.tbDateJ.StyleController = Me.LayoutControl9
        Me.tbDateJ.TabIndex = 135
        '
        'grdParJour
        '
        Me.grdParJour.Location = New System.Drawing.Point(24, 210)
        Me.grdParJour.MainView = Me.GridView32
        Me.grdParJour.Name = "grdParJour"
        Me.grdParJour.Size = New System.Drawing.Size(1278, 545)
        Me.grdParJour.TabIndex = 10
        Me.grdParJour.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView32})
        '
        'GridView32
        '
        Me.GridView32.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.IDHoraireJour, Me.DateTravail, Me.GridColumn302, Me.IDOrganisation2, Me.IDLivreur2, Me.Livreur2, Me.MontantOrganisation1})
        Me.GridView32.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.GridView32.GridControl = Me.grdParJour
        Me.GridView32.Name = "GridView32"
        Me.GridView32.OptionsBehavior.Editable = False
        Me.GridView32.OptionsCustomization.AllowGroup = False
        Me.GridView32.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridView32.OptionsSelection.UseIndicatorForSelection = False
        Me.GridView32.OptionsView.ShowAutoFilterRow = True
        Me.GridView32.OptionsView.ShowGroupPanel = False
        '
        'IDHoraireJour
        '
        Me.IDHoraireJour.Caption = "ID Horaire"
        Me.IDHoraireJour.FieldName = "IDHoraireJour"
        Me.IDHoraireJour.Name = "IDHoraireJour"
        Me.IDHoraireJour.Visible = True
        Me.IDHoraireJour.VisibleIndex = 0
        Me.IDHoraireJour.Width = 94
        '
        'DateTravail
        '
        Me.DateTravail.Caption = "Date"
        Me.DateTravail.FieldName = "DateTravail"
        Me.DateTravail.Name = "DateTravail"
        Me.DateTravail.Visible = True
        Me.DateTravail.VisibleIndex = 2
        '
        'GridColumn302
        '
        Me.GridColumn302.Caption = "Montant Livreur"
        Me.GridColumn302.DisplayFormat.FormatString = "c"
        Me.GridColumn302.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.GridColumn302.FieldName = "MontantLivreur"
        Me.GridColumn302.Name = "GridColumn302"
        Me.GridColumn302.Visible = True
        Me.GridColumn302.VisibleIndex = 3
        '
        'IDOrganisation2
        '
        Me.IDOrganisation2.Caption = "IDOrganisation"
        Me.IDOrganisation2.FieldName = "IDOrganisation"
        Me.IDOrganisation2.Name = "IDOrganisation2"
        '
        'IDLivreur2
        '
        Me.IDLivreur2.Caption = "ID Livreur"
        Me.IDLivreur2.FieldName = "IDLivreur"
        Me.IDLivreur2.Name = "IDLivreur2"
        '
        'Livreur2
        '
        Me.Livreur2.Caption = "Livreur"
        Me.Livreur2.FieldName = "Livreur"
        Me.Livreur2.Name = "Livreur2"
        Me.Livreur2.Visible = True
        Me.Livreur2.VisibleIndex = 1
        '
        'MontantOrganisation1
        '
        Me.MontantOrganisation1.Caption = "Montant Organisation"
        Me.MontantOrganisation1.DisplayFormat.FormatString = "c"
        Me.MontantOrganisation1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.MontantOrganisation1.FieldName = "MontantOrganisation"
        Me.MontantOrganisation1.Name = "MontantOrganisation1"
        Me.MontantOrganisation1.Visible = True
        Me.MontantOrganisation1.VisibleIndex = 4
        '
        'LayoutControlGroup17
        '
        Me.LayoutControlGroup17.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup17.GroupBordersVisible = False
        Me.LayoutControlGroup17.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlGroup21})
        Me.LayoutControlGroup17.Name = "Root"
        Me.LayoutControlGroup17.Size = New System.Drawing.Size(1326, 779)
        Me.LayoutControlGroup17.TextVisible = False
        '
        'LayoutControlGroup21
        '
        Me.LayoutControlGroup21.CustomizationFormText = "Ajouter un Livreur a la liste"
        Me.LayoutControlGroup21.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.EmptySpaceItem18, Me.LayoutControlItem90, Me.LayoutControlItem91, Me.LayoutControlItem92, Me.LayoutControlItem94, Me.LayoutControlItem95, Me.LayoutControlItem96, Me.LayoutControlGroup22, Me.LayoutControlItem101, Me.EmptySpaceItem19})
        Me.LayoutControlGroup21.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup21.Name = "LayoutControlGroup21"
        Me.LayoutControlGroup21.Size = New System.Drawing.Size(1306, 759)
        Me.LayoutControlGroup21.Text = "Ajouter un Livreur a la liste"
        '
        'EmptySpaceItem18
        '
        Me.EmptySpaceItem18.AllowHotTrack = False
        Me.EmptySpaceItem18.CustomizationFormText = "EmptySpaceItem1"
        Me.EmptySpaceItem18.Location = New System.Drawing.Point(826, 0)
        Me.EmptySpaceItem18.Name = "EmptySpaceItem18"
        Me.EmptySpaceItem18.Size = New System.Drawing.Size(456, 166)
        Me.EmptySpaceItem18.Text = "EmptySpaceItem1"
        Me.EmptySpaceItem18.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlItem90
        '
        Me.LayoutControlItem90.Control = Me.bAjouterChargeParJour
        Me.LayoutControlItem90.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem90.CustomizationFormText = "LayoutControlItem19"
        Me.LayoutControlItem90.Location = New System.Drawing.Point(438, 0)
        Me.LayoutControlItem90.MinSize = New System.Drawing.Size(49, 26)
        Me.LayoutControlItem90.Name = "LayoutControlItem90"
        Me.LayoutControlItem90.Size = New System.Drawing.Size(137, 60)
        Me.LayoutControlItem90.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem90.Text = "LayoutControlItem19"
        Me.LayoutControlItem90.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem90.TextVisible = False
        '
        'LayoutControlItem91
        '
        Me.LayoutControlItem91.Control = Me.bSupprimerChargeParJour
        Me.LayoutControlItem91.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem91.CustomizationFormText = "LayoutControlItem21"
        Me.LayoutControlItem91.Location = New System.Drawing.Point(575, 0)
        Me.LayoutControlItem91.MinSize = New System.Drawing.Size(61, 26)
        Me.LayoutControlItem91.Name = "LayoutControlItem91"
        Me.LayoutControlItem91.Size = New System.Drawing.Size(125, 60)
        Me.LayoutControlItem91.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem91.Text = "LayoutControlItem21"
        Me.LayoutControlItem91.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem91.TextVisible = False
        '
        'LayoutControlItem92
        '
        Me.LayoutControlItem92.Control = Me.bMiseAJourChargeParJour
        Me.LayoutControlItem92.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem92.CustomizationFormText = "LayoutControlItem50"
        Me.LayoutControlItem92.Location = New System.Drawing.Point(700, 0)
        Me.LayoutControlItem92.MinSize = New System.Drawing.Size(65, 26)
        Me.LayoutControlItem92.Name = "LayoutControlItem92"
        Me.LayoutControlItem92.Size = New System.Drawing.Size(126, 60)
        Me.LayoutControlItem92.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem92.Text = "LayoutControlItem50"
        Me.LayoutControlItem92.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem92.TextVisible = False
        '
        'LayoutControlItem94
        '
        Me.LayoutControlItem94.Control = Me.tbIDHoraireJ
        Me.LayoutControlItem94.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem94.CustomizationFormText = "Bonus par kilométrage démarre a"
        Me.LayoutControlItem94.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem94.Name = "LayoutControlItem94"
        Me.LayoutControlItem94.Size = New System.Drawing.Size(438, 24)
        Me.LayoutControlItem94.Text = "ID Horaire Jour"
        Me.LayoutControlItem94.TextSize = New System.Drawing.Size(143, 13)
        '
        'LayoutControlItem95
        '
        Me.LayoutControlItem95.Control = Me.cbLivreursJ
        Me.LayoutControlItem95.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem95.CustomizationFormText = "Livreur"
        Me.LayoutControlItem95.Location = New System.Drawing.Point(0, 24)
        Me.LayoutControlItem95.Name = "LayoutControlItem95"
        Me.LayoutControlItem95.Size = New System.Drawing.Size(400, 26)
        Me.LayoutControlItem95.Text = "Livreur"
        Me.LayoutControlItem95.TextSize = New System.Drawing.Size(143, 13)
        '
        'LayoutControlItem96
        '
        Me.LayoutControlItem96.Control = Me.bAnnulerLivreurH
        Me.LayoutControlItem96.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem96.CustomizationFormText = "LayoutControlItem17"
        Me.LayoutControlItem96.Location = New System.Drawing.Point(400, 24)
        Me.LayoutControlItem96.Name = "LayoutControlItem96"
        Me.LayoutControlItem96.Size = New System.Drawing.Size(38, 26)
        Me.LayoutControlItem96.Text = "LayoutControlItem17"
        Me.LayoutControlItem96.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem96.TextVisible = False
        '
        'LayoutControlGroup22
        '
        Me.LayoutControlGroup22.CustomizationFormText = "Configuration Horaire"
        Me.LayoutControlGroup22.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem97, Me.LayoutControlItem98, Me.LayoutControlItem99})
        Me.LayoutControlGroup22.Location = New System.Drawing.Point(0, 50)
        Me.LayoutControlGroup22.Name = "LayoutControlGroup22"
        Me.LayoutControlGroup22.Size = New System.Drawing.Size(438, 116)
        Me.LayoutControlGroup22.Text = "Configuration Horaire"
        '
        'LayoutControlItem97
        '
        Me.LayoutControlItem97.Control = Me.tbMontantParHeureLivreurJ
        Me.LayoutControlItem97.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem97.CustomizationFormText = "Bonus Amount"
        Me.LayoutControlItem97.Location = New System.Drawing.Point(0, 24)
        Me.LayoutControlItem97.Name = "LayoutControlItem97"
        Me.LayoutControlItem97.Size = New System.Drawing.Size(414, 24)
        Me.LayoutControlItem97.Text = "Montant par jour livreur"
        Me.LayoutControlItem97.TextSize = New System.Drawing.Size(143, 13)
        '
        'LayoutControlItem98
        '
        Me.LayoutControlItem98.Control = Me.tbMontantParHeureOrganisationJ
        Me.LayoutControlItem98.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem98.CustomizationFormText = "Bonus Amount"
        Me.LayoutControlItem98.Location = New System.Drawing.Point(0, 48)
        Me.LayoutControlItem98.Name = "LayoutControlItem98"
        Me.LayoutControlItem98.Size = New System.Drawing.Size(414, 24)
        Me.LayoutControlItem98.Text = "Montant par jour organisation"
        Me.LayoutControlItem98.TextSize = New System.Drawing.Size(143, 13)
        '
        'LayoutControlItem99
        '
        Me.LayoutControlItem99.Control = Me.tbDateJ
        Me.LayoutControlItem99.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem99.CustomizationFormText = "Date / heure début"
        Me.LayoutControlItem99.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem99.Name = "LayoutControlItem99"
        Me.LayoutControlItem99.Size = New System.Drawing.Size(414, 24)
        Me.LayoutControlItem99.Text = "Date"
        Me.LayoutControlItem99.TextSize = New System.Drawing.Size(143, 13)
        '
        'LayoutControlItem101
        '
        Me.LayoutControlItem101.Control = Me.grdParJour
        Me.LayoutControlItem101.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem101.CustomizationFormText = "LayoutControlItem16"
        Me.LayoutControlItem101.Location = New System.Drawing.Point(0, 166)
        Me.LayoutControlItem101.Name = "LayoutControlItem101"
        Me.LayoutControlItem101.Size = New System.Drawing.Size(1282, 549)
        Me.LayoutControlItem101.Text = "LayoutControlItem16"
        Me.LayoutControlItem101.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem101.TextVisible = False
        '
        'EmptySpaceItem19
        '
        Me.EmptySpaceItem19.AllowHotTrack = False
        Me.EmptySpaceItem19.CustomizationFormText = "EmptySpaceItem4"
        Me.EmptySpaceItem19.Location = New System.Drawing.Point(438, 60)
        Me.EmptySpaceItem19.Name = "EmptySpaceItem19"
        Me.EmptySpaceItem19.Size = New System.Drawing.Size(388, 106)
        Me.EmptySpaceItem19.Text = "EmptySpaceItem4"
        Me.EmptySpaceItem19.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutCalculationOrganisation
        '
        Me.LayoutCalculationOrganisation.Location = New System.Drawing.Point(455, 412)
        Me.LayoutCalculationOrganisation.Name = "LayoutCalculationOrganisation"
        Me.LayoutCalculationOrganisation.Size = New System.Drawing.Size(173, 163)
        Me.LayoutCalculationOrganisation.Text = "Méthodes de calculation pour l'organisation"
        '
        'frmOrganisations
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1331, 844)
        Me.Controls.Add(Me.tabData)
        Me.Controls.Add(Me.TS)
        Me.Name = "frmOrganisations"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Organisation"
        Me.TS.ResumeLayout(False)
        Me.TS.PerformLayout()
        CType(Me.tabData, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabData.ResumeLayout(False)
        Me.tabParheure.ResumeLayout(False)
        CType(Me.LayoutControl3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LayoutControl3.ResumeLayout(False)
        CType(Me.tbHeureDepartH.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbHeureDepartH.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grdParHeure, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbLivreursH.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbMontantParHeureLivreurH.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbIDHoraire.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbMontantParHeureOrganisationH.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbHeureFinH.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbHeureFinH.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem19, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem21, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem50, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem55, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem35, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem17, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup14, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem18, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem77, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem72, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem85, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem16, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabPage1.ResumeLayout(False)
        CType(Me.grdOrganisations, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabPage2.ResumeLayout(False)
        CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LayoutControl1.ResumeLayout(False)
        CType(Me.tbCommissionVendeur.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkIsCommission.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkIsEtatDeCompte.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbVendeur.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkRestaurant.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkCacheEmail4.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkCacheEmail3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkCacheEmail2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkCacheEmail1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbNombreFree30535.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.optCalculationOrganisations.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbPayeLivreur.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbMontantContrat.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbTelephone.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbAdresse.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbOrganisation.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudOrganisationID.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbCreditParLivraison.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbQuantiteLivraison.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbMontantGlacierOrganisation.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbMontantGlacierLivreur.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbCourriel1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbCourriel2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbCourriel3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.optCalculationLivreur.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbOrganisationImport.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbMontantMobilus.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbCourriel4.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbMontantFixe.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbMessageFacture.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbMessageEtat.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbMaximumLivraison.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbMontantMaxLivraisoneExtra.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbMontantPrixFixeSem.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbMontantParPorteOrganisation.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbMontantParPorteLivreur.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutPaymentInfo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutContrat, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutMontantContrat, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutPayeAuLivreur, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem15, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutMontantParPorteO, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutMontantParPorteL, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutMontantSpeciaux, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutNombreFree30535, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutMontantMobilus, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutGlaciers, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem39, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem40, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutCredit, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutPrixFixeExtra, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutMontantContrat3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem21, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutMaximumLivraison, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutMontantContrat2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutPrixFixe, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutMontantContrat1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem78, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutCalculationLivreur, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem30, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem80, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem81, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem42, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem43, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem70, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem68, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem69, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem71, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem41, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutCommission, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutVendeurs, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem73, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem20, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem67, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem82, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem83, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem84, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem13, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem14, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup13, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem65, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem100, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem102, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem103, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem104, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabInfo.ResumeLayout(False)
        CType(Me.LayoutControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LayoutControl2.ResumeLayout(False)
        CType(Me.tbHeureDepart.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grdParKM, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbKMDebut.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbKMFin.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbMontantCharge.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbChargeExtra.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbIDCommissionKM.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbHeureFin.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbChargeExtraParNbKm.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Root, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem22, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutKMDebut, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutKMFin, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem13, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutChargesSupplementaires, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutChargeExtra, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutParNombreKm, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem28, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem27, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutIDCommissionKM, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem49, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutParHeures, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem14, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem25, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabLivreurs.ResumeLayout(False)
        CType(Me.LayoutControl4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LayoutControl4.ResumeLayout(False)
        CType(Me.grdParKMCL, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbMontantChargeCL.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbChargeExtraCL.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbIDCommissionKMCL.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbKMDebutCL.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbKMFinCL.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbHeureDepartCL.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbHeureFinCL.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbChargeExtraParNbKmCL.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem33, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem32, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutChargesSupplementairesCL, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutParNombreKmCL, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem51, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutParHeuresCL, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem15, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem29, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem26, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabMaxPayable.ResumeLayout(False)
        CType(Me.LayoutControl5, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LayoutControl5.ResumeLayout(False)
        CType(Me.grdMaxPayable, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbIDMaxPayable.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbMaximumPayable.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbNombreLivraisons.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem45, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem46, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem48, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem47, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem54, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem53, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem52, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem7, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabPage3.ResumeLayout(False)
        CType(Me.LayoutControl6, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LayoutControl6.ResumeLayout(False)
        CType(Me.grdCadeauOrganisation, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbMontantCadeauO.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbDateFromCadeauO.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbDateFromCadeauO.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbDateToCadeauO.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbDateToCadeauO.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbIIDCadeauOrganisation.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem23, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem31, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem59, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem61, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem60, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem36, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem37, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem63, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabPage4.ResumeLayout(False)
        CType(Me.LayoutControl7, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LayoutControl7.ResumeLayout(False)
        CType(Me.cbLivreursCadeauL.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grdCadeauLivreur, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbMontantCadeauL.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbDateFromCadeauL.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbDateFromCadeauL.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbDateToCadeauL.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbDateToCadeauL.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbIDCadeauLivreur.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem24, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem34, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem38, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem44, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem56, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem58, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem57, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem62, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem64, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem66, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabPage5.ResumeLayout(False)
        CType(Me.LayoutControl8, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LayoutControl8.ResumeLayout(False)
        CType(Me.tbIDPrixFixe.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbLivreursP.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbMontantPrixFixe.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grdPrixFixe, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView31, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup16, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup18, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem16, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem74, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem75, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem76, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem86, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem87, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem88, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup19, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem89, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem93, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem17, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabPage6.ResumeLayout(False)
        CType(Me.LayoutControl9, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LayoutControl9.ResumeLayout(False)
        CType(Me.tbIDHoraireJ.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbLivreursJ.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbMontantParHeureLivreurJ.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbMontantParHeureOrganisationJ.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbDateJ.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbDateJ.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grdParJour, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView32, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup17, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup21, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem18, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem90, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem91, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem92, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem94, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem95, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem96, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup22, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem97, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem98, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem99, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem101, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem19, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutCalculationOrganisation, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents TS As System.Windows.Forms.ToolStrip
    Friend WithEvents Head As System.Windows.Forms.ToolStripLabel
    Friend WithEvents bAdd As System.Windows.Forms.ToolStripButton
    Friend WithEvents sp1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents bEdit As System.Windows.Forms.ToolStripButton
    Friend WithEvents sp2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents bDelete As System.Windows.Forms.ToolStripButton
    Friend WithEvents sp3 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents bCancel As System.Windows.Forms.ToolStripButton
    Friend WithEvents sp4 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents bRefresh As System.Windows.Forms.ToolStripButton
    Friend WithEvents sp5 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripButton6 As System.Windows.Forms.ToolStripButton
    Friend WithEvents tabData As DevExpress.XtraTab.XtraTabControl
    Friend WithEvents XtraTabPage1 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents XtraTabPage2 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents LayoutControl1 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents tbPayeLivreur As DevExpress.XtraEditors.TextEdit
    Friend WithEvents tbMontantContrat As DevExpress.XtraEditors.TextEdit
    Friend WithEvents tbTelephone As DevExpress.XtraEditors.TextEdit
    Friend WithEvents tbAdresse As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents tbOrganisation As DevExpress.XtraEditors.TextEdit
    Friend WithEvents nudOrganisationID As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlGroup1 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem4 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutContrat As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutMontantContrat As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutPayeAuLivreur As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem10 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem5 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem8 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents tbCreditParLivraison As DevExpress.XtraEditors.TextEdit
    Friend WithEvents tbQuantiteLivraison As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TabInfo As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents LayoutControl2 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents Root As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutCredit As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents grdOrganisations As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn8 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn9 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn10 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn11 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn12 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents tabParheure As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents LayoutControl3 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents LayoutControlGroup2 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlGroup4 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents bSupprimerChargeParHeure As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents bAjouterChargeParHeure As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents tbMontantParHeureLivreurH As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlItem18 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem1 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents LayoutControlItem19 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem21 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents grdParKM As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView2 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colIDCommissionKM As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colOrganisation2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn3 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn4 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents Montant As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn6 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn7 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents LayoutControlItem22 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlGroup5 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents tbKMDebut As DevExpress.XtraEditors.TextEdit
    Friend WithEvents tbKMFin As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutKMDebut As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutKMFin As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents tbMontantCharge As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlItem13 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents tbChargeExtra As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutChargeExtra As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents bSupprimerChargesParKm As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents bAjouterChargesParKm As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LayoutChargesSupplementaires As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem28 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem27 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutParHeures As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents colHeureDepart As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colHeureFin As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents EmptySpaceItem2 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents EmptySpaceItem3 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents tabLivreurs As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents LayoutControl4 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents LayoutControlGroup7 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents tbMontantChargeCL As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlItem7 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents bSupprimerChargesParKmCL As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents bAjouterChargesParKmCL As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents tbChargeExtraCL As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlGroup8 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutChargesSupplementairesCL As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem9 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem33 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem32 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem6 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents LayoutParHeuresCL As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents EmptySpaceItem5 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents tbMontantGlacierOrganisation As DevExpress.XtraEditors.TextEdit
    Friend WithEvents tbMontantGlacierLivreur As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutGlaciers As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem39 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem40 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents tbCourriel1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents tbCourriel2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents tbCourriel3 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlItem41 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem42 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem43 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents tbIDCommissionKM As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutIDCommissionKM As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents grdParKMCL As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView5 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn17 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn18 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn19 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn20 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn21 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn22 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn23 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents LayoutControlItem26 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents tbIDCommissionKMCL As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlItem1 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents tabMaxPayable As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents LayoutControl5 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents grdMaxPayable As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView4 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colIDMaxPayable As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colNombreLivraison As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colMaxPayable As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents LayoutControlGroup9 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem45 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents tbIDMaxPayable As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlItem46 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents tbMaximumPayable As DevExpress.XtraEditors.TextEdit
    Friend WithEvents tbNombreLivraisons As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlItem48 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem47 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents bMiseAJourChargeParHeure As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LayoutControlItem50 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents bMiseaJourChargesParKm As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LayoutControlItem49 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents bMiseAJourChargesParKmCL As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LayoutControlItem51 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents bMiseAJourMP As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents bSupprimerMP As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents bAjouterMP As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LayoutControlGroup10 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem54 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem53 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem52 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem7 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents tbIDHoraire As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlItem55 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents optCalculationLivreur As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents LayoutCalculationLivreur As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem30 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents tbHeureDepart As DevExpress.XtraEditors.TimeEdit
    Friend WithEvents LayoutControlItem14 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem8 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents tbHeureFin As DevExpress.XtraEditors.TimeEdit
    Friend WithEvents LayoutControlItem25 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents tbKMDebutCL As DevExpress.XtraEditors.TextEdit
    Friend WithEvents tbKMFinCL As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlItem15 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem29 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents tbHeureDepartCL As DevExpress.XtraEditors.TimeEdit
    Friend WithEvents tbHeureFinCL As DevExpress.XtraEditors.TimeEdit
    Friend WithEvents LayoutControlItem2 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem3 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents XtraTabPage3 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents LayoutControl6 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents LayoutControlGroup3 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents XtraTabPage4 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents LayoutControl7 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents LayoutControlGroup6 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents grdCadeauOrganisation As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView6 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn13 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn14 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn26 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn15 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn16 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn24 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents LayoutControlItem23 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents grdCadeauLivreur As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView7 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colIDCadeauLivreur As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colIDLivreur As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn25 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colDateFrom As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colDateTo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents Montant1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents LayoutControlItem24 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents tbMontantCadeauO As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlGroup11 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem31 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents tbMontantCadeauL As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlItem34 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlGroup12 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents bMAJCadeauO As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents bSupprimerCadeauO As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents bAjouterCadeauO As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents tbDateFromCadeauO As DevExpress.XtraEditors.DateEdit
    Friend WithEvents tbDateToCadeauO As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LayoutControlItem36 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem37 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem59 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem10 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents LayoutControlItem61 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem60 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents bMAJCadeauL As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents bSupprimerCadeauL As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents bAjouterCadeauL As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents tbDateFromCadeauL As DevExpress.XtraEditors.DateEdit
    Friend WithEvents tbDateToCadeauL As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LayoutControlItem38 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem44 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem56 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem58 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem57 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem9 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents tbIIDCadeauOrganisation As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlItem63 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents tbIDCadeauLivreur As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlItem62 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutCalculationOrganisation As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutPaymentInfo As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents tbOrganisationImport As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlItem65 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents tbChargeExtraParNbKm As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutParNombreKm As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents tbChargeExtraParNbKmCL As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutParNombreKmCL As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents optCalculationOrganisations As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents cbLivreursH As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LayoutControlItem35 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents cbLivreursCadeauL As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LayoutControlItem64 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents bAnnulerLivreur As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LayoutControlItem17 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents bAnnulerLivreurL As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LayoutControlItem66 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents tbNombreFree30535 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutNombreFree30535 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents tbMontantMobilus As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutMontantMobilus As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutMontantSpeciaux As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents chkCacheEmail3 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents chkCacheEmail2 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents chkCacheEmail1 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LayoutControlItem67 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem68 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem69 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents chkCacheEmail4 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents tbCourriel4 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlItem70 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem71 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents bAnnulerVendeur As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LayoutControlItem73 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents grdParHeure As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView3 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents IDHoraire As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents HeureDepart As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents HeureFin As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn30 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents IDOrganisation As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents IDLivreur As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents Livreur As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents LayoutControlGroup14 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem16 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents MontantOrganisation As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents tbMontantParHeureOrganisationH As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlItem77 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem4 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents chkRestaurant As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LayoutControlItem78 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem12 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem11 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutCommission As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem6 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem11 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents EmptySpaceItem12 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents Label2 As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents LayoutControlItem80 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem81 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents butClose As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents butCancel As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents butApply As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LayoutControlItem82 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem83 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem84 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem13 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents EmptySpaceItem14 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents cbVendeur As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LayoutVendeurs As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents tbMontantFixe As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutMontantContrat1 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents tbHeureDepartH As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LayoutControlItem72 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlGroup13 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents XtraTabPage5 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents LayoutControl8 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents LayoutControlGroup16 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents bAjouterPrixFixe As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents bSupprimerPrixFixe As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents bMiseAJourPrixFixe As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents tbIDPrixFixe As DevExpress.XtraEditors.TextEdit
    Friend WithEvents cbLivreursP As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents bAnnulerLivreurP As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents tbMontantPrixFixe As DevExpress.XtraEditors.TextEdit
    Friend WithEvents grdPrixFixe As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView31 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents IDHoraire1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn301 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents IDOrganisation1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents IDLivreur1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents Livreur1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents IDJourSemaine1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents LayoutControlGroup18 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents EmptySpaceItem16 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents LayoutControlItem74 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem75 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem76 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem86 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem87 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem88 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlGroup19 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem89 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem93 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem17 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents LayoutPrixFixe As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents XtraTabPage6 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents LayoutControl9 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents bAjouterChargeParJour As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents bSupprimerChargeParJour As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents bMiseAJourChargeParJour As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents tbIDHoraireJ As DevExpress.XtraEditors.TextEdit
    Friend WithEvents cbLivreursJ As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents bAnnulerLivreurH As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents tbMontantParHeureLivreurJ As DevExpress.XtraEditors.TextEdit
    Friend WithEvents tbMontantParHeureOrganisationJ As DevExpress.XtraEditors.TextEdit
    Friend WithEvents tbDateJ As DevExpress.XtraEditors.DateEdit
    Friend WithEvents grdParJour As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView32 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents IDHoraireJour As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents DateTravail As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn302 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents IDOrganisation2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents IDLivreur2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents Livreur2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents MontantOrganisation1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents LayoutControlGroup17 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlGroup21 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents EmptySpaceItem18 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents LayoutControlItem90 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem91 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem92 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem94 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem95 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem96 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlGroup22 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem97 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem98 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem99 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem101 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem19 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents tbHeureFinH As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LayoutControlItem85 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents tbMessageFacture As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents LayoutControlItem100 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents tbMessageEtat As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents LayoutControlItem102 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents chkIsCommission As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents chkIsEtatDeCompte As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LayoutControlItem103 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem104 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents tbMaximumLivraison As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutMaximumLivraison As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents tbMontantMaxLivraisoneExtra As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutMontantContrat2 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutPrixFixeExtra As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents tbMontantPrixFixeSem As DevExpress.XtraEditors.TextEdit
    Friend WithEvents EmptySpaceItem15 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents LayoutMontantContrat3 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem21 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents tbCommissionVendeur As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlItem20 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents tbMontantParPorteOrganisation As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutMontantParPorteO As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents tbMontantParPorteLivreur As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutMontantParPorteL As DevExpress.XtraLayout.LayoutControlItem
End Class
