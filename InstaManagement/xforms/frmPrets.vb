﻿Imports System.Data.SqlClient
Imports System.Globalization
Imports System.Threading
Imports System.Text
Imports DevExpress.XtraGrid.Views.Grid.ViewInfo
Public Class frmPrets

    Private oPretsData As New PretsData
    Private oPretsPaiementsData As New PretsPaiementsData
    Private oLivreursData As New LivreursData
    Dim oPeriodeData As New PeriodesData

    Private bload As Boolean = False
    Private bAddMode As Boolean = False
    Private bEditMode As Boolean = False
    Private bDeleteMode As Boolean = False
    Public blnGridClick As Boolean = False
    Private IDFournisseur As Integer
    Private iRowGrid As Int32 = 0
    Dim ssql As String
    Private Sub FillAllLivreurs()


        Dim dtLivreur As DataTable
        Dim drLivreur As DataRow
        Dim oLivreurData As New LivreursData

        Dim table As New DataTable
        table.Columns.Add("value", Type.GetType("System.Int32"))
        table.Columns.Add("Livreur", Type.GetType("System.String"))
        dtLivreur = oLivreurData.SelectAll



        For Each drLivreur In dtLivreur.Rows
            Dim teller As Integer = 0

            teller = drLivreur("LivreurID")
            Dim toto As String = drLivreur("Livreur")
            table.Rows.Add(New Object() {teller, toto})
        Next


        cbLivreur.Properties.DataSource = table
        cbLivreur.Properties.NullText = "Selectionner un livreur"
        cbLivreur.Properties.DisplayMember = "Livreur"
        cbLivreur.Properties.ValueMember = "value"
        cbLivreur.Properties.PopupFormMinSize = New Size(50, 200)
        cbLivreur.Properties.PopulateColumns()


    End Sub

    'Get the first day of the month
    Public Function FirstDayOfMonth(ByVal sourceDate As DateTime) As DateTime
        Return New DateTime(sourceDate.Year, sourceDate.Month, 1)
    End Function

    'Get the last day of the month
    Public Function LastDayOfMonth(ByVal sourceDate As DateTime) As DateTime
        Dim lastDay As DateTime = New DateTime(sourceDate.Year, sourceDate.Month, 1)
        Return lastDay.AddMonths(1).AddDays(-1)
    End Function


    Private Sub frmPrets_Load(sender As Object, e As EventArgs) Handles Me.Load
        bload = True
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

        Thread.CurrentThread.CurrentCulture = New CultureInfo("en-US", True)

        FillAllLivreurs()


        LoadGridCode()

        bload = False

        Me.Cursor = System.Windows.Forms.Cursors.Default
    End Sub

    Private Sub LoadGridCode()
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        Try

            grdPrets.DataSource = oPretsData.SelectAll()
            GridView1.ClearSorting()
            GridView1.Columns("IDPret").SortOrder = DevExpress.Data.ColumnSortOrder.Ascending

        Catch
        End Try
        Me.Cursor = System.Windows.Forms.Cursors.Default
    End Sub

    Private Sub Delete()
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        Me.AddMode = False
        Me.EditMode = False
        Me.DeleteMode = True
        GetData()

        EnableRecord(False)
        tabData.SelectedTabPage = tabData.TabPages(1)
        tabData.TabPages(1).PageVisible = True
        tabData.TabPages(0).PageVisible = False
        ShowToolStripItems("Delete")
        Me.Cursor = System.Windows.Forms.Cursors.Default
    End Sub


    Private Sub Add()
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        Me.AddMode = True
        Me.EditMode = False
        Me.DeleteMode = False

        ClearRecord()
        EnableRecord(True)
        Dim dDate As DateTime = go_Globals.PeriodeDateTo

        'tbDate.Text = dDate.ToShortDateString


        tabData.SelectedTabPage = tabData.TabPages(1)
        tabData.TabPages(1).PageVisible = True
        tabData.TabPages(0).PageVisible = False
        ShowToolStripItems("Add")
        tbDatePret.Text = DateTime.Now.ToShortDateString

        Me.Cursor = System.Windows.Forms.Cursors.Default
    End Sub
    Private Sub Edit()
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        Me.AddMode = False
        Me.EditMode = True
        Me.DeleteMode = False
        ClearRecord()
        GetData()

        EnableRecord(True)


        Me.Cursor = System.Windows.Forms.Cursors.Default
    End Sub

    Private Sub GoBack_To_Grid()
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        Dim gridOK As Boolean = False
        Try
            ShowToolStripItems("Cancel")
            LoadGridCode()

            gridOK = True
        Catch
            'Hide Erreur message.
        Finally
            If gridOK = False Then
                ''''
                ShowToolStripItems("Cancel")
                ''''
            End If
        End Try
        Me.Cursor = System.Windows.Forms.Cursors.Default
    End Sub

    Private Sub ShowToolStripItems(ByVal Item As String)
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        bAdd.Enabled = False
        bEdit.Enabled = False
        bDelete.Enabled = False
        bCancel.Enabled = False
        bRefresh.Enabled = False
        Select Case Item
            Case "Add"
                bCancel.Enabled = True
            Case "Edit"
                bCancel.Enabled = True
            Case "Delete"
                bCancel.Enabled = True
            Case "Cancel"
                bAdd.Enabled = True
                bEdit.Enabled = True
                bDelete.Enabled = True
                bRefresh.Enabled = True
                tabData.SelectedTabPage = tabData.TabPages(0)
                tabData.TabPages(1).PageVisible = False
                tabData.TabPages(0).PageVisible = True
            Case "Refresh"
                bAdd.Enabled = True
                bEdit.Enabled = True
                bDelete.Enabled = True
                bRefresh.Enabled = True
                LoadGridCode()
            Case "No Record"
                bAdd.Enabled = True
                bRefresh.Enabled = True
        End Select

        Me.Cursor = System.Windows.Forms.Cursors.Default
    End Sub

    Public Property AddMode() As Boolean
        Get
            Return bAddMode
        End Get
        Set(ByVal value As Boolean)
            bAddMode = value
        End Set
    End Property

    Public Property EditMode() As Boolean
        Get
            Return bEditMode
        End Get
        Set(ByVal value As Boolean)
            bEditMode = value
        End Set
    End Property

    Public Property DeleteMode() As Boolean
        Get
            Return bDeleteMode
        End Get
        Set(ByVal value As Boolean)
            bDeleteMode = value
        End Set
    End Property


    Private Sub ClearRecord()
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor


        Me.nudIDPret.Text = Nothing
        Me.tbDatePret.Text = Nothing
        Me.cbLivreur.EditValue = Nothing
        Me.tbMontantPret.Text = Nothing
        Me.tbBalancePret.Text = Nothing


        Me.Cursor = System.Windows.Forms.Cursors.Default
    End Sub

    Private Sub EnableRecord(ByVal YesNo As Boolean)
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor


        Me.tbDatePret.Enabled = YesNo
        Me.cbLivreur.Enabled = YesNo
        Me.tbMontantPret.Enabled = YesNo
        Me.tbBalancePret.Enabled = YesNo

        Me.Cursor = System.Windows.Forms.Cursors.Default
    End Sub

    Public Property SelectedRowFournisseur() As Int32
        Get
            Return iRowGrid
        End Get
        Set(ByVal value As Int32)
            iRowGrid = value
        End Set
    End Property

    Public Property SelectedRow() As Int32
        Get
            Return iRowGrid
        End Get
        Set(ByVal value As Int32)
            iRowGrid = value
        End Set
    End Property

    Private Sub bAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        tabData.SelectedTabPage = tabData.TabPages(1)
        tabData.TabPages(1).PageVisible = True
        tabData.TabPages(0).PageVisible = False
    End Sub

    Private Sub bEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        tabData.SelectedTabPage = tabData.TabPages(1)
        tabData.TabPages(1).PageVisible = True
        tabData.TabPages(0).PageVisible = False
    End Sub

    Private Sub bCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        tabData.SelectedTabPage = tabData.TabPages(0)
        tabData.TabPages(1).PageVisible = False
        tabData.TabPages(0).PageVisible = True
    End Sub

    Private Sub bDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        tabData.SelectedTabPage = tabData.TabPages(0)
        tabData.TabPages(1).PageVisible = False
        tabData.TabPages(0).PageVisible = True
    End Sub

    Private Sub butCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butCancel.Click
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        ShowToolStripItems("Cancel")
        tabData.SelectedTabPage = tabData.TabPages(0)
        tabData.TabPages(1).PageVisible = False
        LoadGridCode()
        tabData.TabPages(0).PageVisible = True


        Me.Cursor = System.Windows.Forms.Cursors.Default

    End Sub


    Private Sub butClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butClose.Click, ToolStripButton6.Click
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        Me.Close()
        Me.Cursor = System.Windows.Forms.Cursors.Default
    End Sub

    Private Sub GetData()


        ClearRecord()


        Dim row As System.Data.DataRow = GridView1.GetDataRow(GridView1.FocusedRowHandle)

        Dim clsPret As New Prets
        clsPret.IDPret = System.Convert.ToInt32(row("IDPret").ToString)
        clsPret = oPretsData.Select_Record(clsPret)





        If Not clsPret Is Nothing Then
            Try
                nudIDPret.Text = System.Convert.ToInt32(clsPret.IDPret)
                tbDatePret.Text = If(IsNothing(clsPret.DatePret), Nothing, clsPret.DatePret.ToString.Trim)
                cbLivreur.EditValue = If(IsNothing(clsPret.IDLivreur), Nothing, clsPret.IDLivreur)
                tbMontantPret.Text = If(IsNothing(clsPret.MontantPret), 0, clsPret.MontantPret)
                tbBalancePret.Text = If(IsNothing(clsPret.BalancePret), 0, clsPret.BalancePret)
                tbNombreSemaine.Text = If(IsNothing(clsPret.NombreSemaine), 0, clsPret.NombreSemaine)
                tbPaiementParPeriode.Text = If(IsNothing(clsPret.PaiementParPeriode), 0, clsPret.PaiementParPeriode)

                'Fill payment Grid

                grdPaiements.DataSource = oPretsPaiementsData.SelectAllByID(clsPret.IDPret)
                GridView2.ClearSorting()
                GridView2.Columns("IDPretPaiment").SortOrder = DevExpress.Data.ColumnSortOrder.Ascending


            Catch
            End Try
        End If


    End Sub

    Private Sub SetData(ByVal clsPret As Prets)
        With clsPret

            .IDPret = If(String.IsNullOrEmpty(nudIDPret.Text), Nothing, nudIDPret.Text)
            .DatePret = If(String.IsNullOrEmpty(tbDatePret.Text), Nothing, tbDatePret.Text)

            If IsNothing(cbLivreur.EditValue) Then
                .IDLivreur = Nothing
                .Livreur = Nothing
            Else
                .IDLivreur = If(String.IsNullOrEmpty(cbLivreur.EditValue), Nothing, cbLivreur.EditValue)
                .Livreur = If(String.IsNullOrEmpty(cbLivreur.Text), Nothing, cbLivreur.Text)
            End If

            .MontantPret = If(String.IsNullOrEmpty(tbMontantPret.EditValue), 0, Convert.ToDouble(tbMontantPret.EditValue))
            .BalancePret = If(String.IsNullOrEmpty(tbBalancePret.EditValue), 0, Convert.ToDouble(tbBalancePret.EditValue))
            .NombreSemaine = If(String.IsNullOrEmpty(tbNombreSemaine.EditValue), 0, Convert.ToInt32(tbNombreSemaine.Text))
            .PaiementParPeriode = If(String.IsNullOrEmpty(tbPaiementParPeriode.EditValue), 0, Convert.ToDouble(tbPaiementParPeriode.EditValue))




        End With
    End Sub
    Private Function VerifyData() As Boolean

        If cbLivreur.Text.ToString.Trim = "" Then
            MessageBox.Show("Vous devez sélectionner un livreur")
            cbLivreur.Focus()
            Return False
        End If

        If tbMontantPret.EditValue = 0 Then
            MessageBox.Show("Vous devez sélectionner un total")
            tbMontantPret.Focus()
            Return False
        End If

        Return True
    End Function



    Private Sub UpdateRecord()
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor



        Dim row As System.Data.DataRow = GridView1.GetDataRow(GridView1.FocusedRowHandle)
        Dim clsPret As New Prets
        clsPret.IDPret = System.Convert.ToInt32(nudIDPret.EditValue)
        clsPret = oPretsData.Select_Record(clsPret)

        If VerifyData() = True Then
            SetData(clsPret)
            Dim bSucess As Boolean
            bSucess = oPretsData.Update(clsPret, clsPret)
            If bSucess = True Then
                GoBack_To_Grid()
            Else
                MsgBox("Update failed.", MsgBoxStyle.OkOnly, "Erreur")
            End If
        End If
        Me.Cursor = System.Windows.Forms.Cursors.Default
    End Sub

    Private Sub DeleteRecord()
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

        Dim row As System.Data.DataRow = GridView1.GetDataRow(GridView1.FocusedRowHandle)

        Dim clsPret As New Prets
        clsPret.IDPret = System.Convert.ToInt32(nudIDPret.EditValue)
        clsPret = oPretsData.Select_Record(clsPret)


        clsPret.IDPret = System.Convert.ToInt32(nudIDPret.EditValue)
        Dim result As DialogResult = MessageBox.Show("Êtes-vous sur? Supprimer cet enregistrement?", "Supprimer", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
        If result = DialogResult.Yes Then
            SetData(clsPret)
            Dim bSucess As Boolean
            bSucess = oPretsData.Delete(clsPret)
            oPretsPaiementsData.DeleteByID(nudIDPret.EditValue)

            If bSucess = True Then
                GoBack_To_Grid()
            Else
                MsgBox("La supression a echouée.", MsgBoxStyle.OkOnly, "Erreur")
            End If
        End If
        Me.Cursor = System.Windows.Forms.Cursors.Default
    End Sub

    Private Sub InsertRecord()
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        Dim clsPret As New Prets
        If VerifyData() = True Then
            SetData(clsPret)
            Dim bSucess As Boolean
            bSucess = oPretsData.Add(clsPret)
            If bSucess = True Then
                GoBack_To_Grid()

            Else
                MsgBox("L'insertion a échouée.", MsgBoxStyle.OkOnly, "Erreur")
            End If
        End If
        Me.Cursor = System.Windows.Forms.Cursors.Default
    End Sub
    Private Sub butApply_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butApply.Click
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        If Me.AddMode = True Then
            Me.InsertRecord()
        ElseIf Me.EditMode = True Then
            Me.UpdateRecord()
        ElseIf Me.DeleteMode = True Then
            Me.DeleteRecord()
        End If
        Me.Cursor = System.Windows.Forms.Cursors.Default


    End Sub

    Private Sub TS_ItemClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolStripItemClickedEventArgs) Handles TS.ItemClicked
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

        Select Case e.ClickedItem.Text
            Case "Ajouter"

                tabData.SelectedTabPage = tabData.TabPages(1)
                tabData.TabPages(1).PageVisible = True
                tabData.TabPages(0).PageVisible = False
                Add()


            Case "Edition"
                tabData.SelectedTabPage = tabData.TabPages(1)
                tabData.TabPages(1).PageVisible = True
                tabData.TabPages(0).PageVisible = False
                Edit()

            Case "Supprimer"
                tabData.SelectedTabPage = tabData.TabPages(0)
                tabData.TabPages(0).PageVisible = True
                tabData.TabPages(1).PageVisible = False
                Delete()
            Case "Annuler"
                ShowToolStripItems("Cancel")
            Case "Rafraîchir"
                GoBack_To_Grid()
        End Select
        Me.Cursor = System.Windows.Forms.Cursors.Default
    End Sub

    Private Sub grdPrets_Click(sender As Object, e As EventArgs) Handles grdPrets.Click
        If GridView1.FocusedRowHandle < 0 Then Exit Sub

        Dim row As System.Data.DataRow = GridView1.GetDataRow(GridView1.FocusedRowHandle)


        SelectedRow = GridView1.FocusedRowHandle
    End Sub

    Private Sub grdPrets_DoubleClick(sender As Object, e As EventArgs) Handles grdPrets.DoubleClick
        Dim row As System.Data.DataRow = GridView1.GetDataRow(GridView1.FocusedRowHandle)

        SelectedRow = GridView1.FocusedRowHandle
        Edit()

        tabData.SelectedTabPage = tabData.TabPages(1)
        tabData.TabPages(1).PageVisible = True
        tabData.TabPages(0).PageVisible = False
    End Sub

    Private Sub tbMontantPret_EditValueChanged(sender As Object, e As EventArgs) Handles tbMontantPret.EditValueChanged
        If Not IsNothing(tbMontantPret.EditValue) And Not IsNothing(tbNombreSemaine.EditValue) Then
            tbPaiementParPeriode.EditValue = Math.Round(tbMontantPret.EditValue / tbNombreSemaine.Text, 2)

        End If
    End Sub

    Private Sub tbNombreSemaine_EditValueChanged(sender As Object, e As EventArgs) Handles tbNombreSemaine.EditValueChanged

    End Sub

    Private Sub tbMontantPret_KeyUp(sender As Object, e As KeyEventArgs) Handles tbMontantPret.KeyUp
        tbBalancePret.Text = tbMontantPret.EditValue
    End Sub

    Private Sub tbNombreSemaine_KeyPress(sender As Object, e As KeyPressEventArgs) Handles tbNombreSemaine.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(Keys.Return) Then
            If Not IsNothing(tbMontantPret.EditValue) And Not IsNothing(tbNombreSemaine.EditValue) Then
                tbPaiementParPeriode.EditValue = Math.Round(tbMontantPret.EditValue / tbNombreSemaine.Text, 2)
            End If
            e.Handled = True
        End If
    End Sub

    Private Sub tbPaiementParPeriode_KeyPress(sender As Object, e As KeyPressEventArgs) Handles tbPaiementParPeriode.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(Keys.Return) Then
            If Not IsNothing(tbMontantPret.EditValue) And Not IsNothing(tbPaiementParPeriode.EditValue) Then
                tbNombreSemaine.EditValue = Math.Round(tbMontantPret.EditValue / tbPaiementParPeriode.Text, 2)
            End If
            e.Handled = True
        End If
    End Sub


End Class