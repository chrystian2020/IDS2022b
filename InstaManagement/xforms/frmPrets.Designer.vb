﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmPrets
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmPrets))
        Me.TS = New System.Windows.Forms.ToolStrip()
        Me.Head = New System.Windows.Forms.ToolStripLabel()
        Me.bAdd = New System.Windows.Forms.ToolStripButton()
        Me.sp1 = New System.Windows.Forms.ToolStripSeparator()
        Me.bEdit = New System.Windows.Forms.ToolStripButton()
        Me.sp2 = New System.Windows.Forms.ToolStripSeparator()
        Me.bDelete = New System.Windows.Forms.ToolStripButton()
        Me.sp3 = New System.Windows.Forms.ToolStripSeparator()
        Me.bCancel = New System.Windows.Forms.ToolStripButton()
        Me.sp4 = New System.Windows.Forms.ToolStripSeparator()
        Me.bRefresh = New System.Windows.Forms.ToolStripButton()
        Me.sp5 = New System.Windows.Forms.ToolStripSeparator()
        Me.ToolStripButton6 = New System.Windows.Forms.ToolStripButton()
        Me.LayoutControl1 = New DevExpress.XtraLayout.LayoutControl()
        Me.tabData = New DevExpress.XtraTab.XtraTabControl()
        Me.XtraTabPage1 = New DevExpress.XtraTab.XtraTabPage()
        Me.grdPrets = New DevExpress.XtraGrid.GridControl()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colIDAchat = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colDateFrom = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn3 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colMontant = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn8 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn9 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.XtraTabPage2 = New DevExpress.XtraTab.XtraTabPage()
        Me.LayoutControl2 = New DevExpress.XtraLayout.LayoutControl()
        Me.tbPaiementParPeriode = New DevExpress.XtraEditors.TextEdit()
        Me.grdPaiements = New DevExpress.XtraGrid.GridControl()
        Me.GridView2 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.IDPretPaiment = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.DatePaiement = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.Periode = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.Livreur = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.Paiement = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.IDLivreur = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.IDPret = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.TLPBut = New System.Windows.Forms.TableLayoutPanel()
        Me.butApply = New System.Windows.Forms.Button()
        Me.butClose = New System.Windows.Forms.Button()
        Me.butCancel = New System.Windows.Forms.Button()
        Me.nudIDPret = New DevExpress.XtraEditors.TextEdit()
        Me.tbDatePret = New DevExpress.XtraEditors.DateEdit()
        Me.tbMontantPret = New DevExpress.XtraEditors.TextEdit()
        Me.cbLivreur = New DevExpress.XtraEditors.LookUpEdit()
        Me.tbBalancePret = New DevExpress.XtraEditors.TextEdit()
        Me.tbNombreSemaine = New DevExpress.XtraEditors.TextEdit()
        Me.LayoutControlGroup1 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem20 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem8 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem18 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem7 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutAjouterPaiement = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.EmptySpaceItem2 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutPayeAuLivreur1 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutPayeAuLivreur2 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem3 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem4 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlGroup2 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem1 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.Root = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem2 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.TS.SuspendLayout()
        CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LayoutControl1.SuspendLayout()
        CType(Me.tabData, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabData.SuspendLayout()
        Me.XtraTabPage1.SuspendLayout()
        CType(Me.grdPrets, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabPage2.SuspendLayout()
        CType(Me.LayoutControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LayoutControl2.SuspendLayout()
        CType(Me.tbPaiementParPeriode.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grdPaiements, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TLPBut.SuspendLayout()
        CType(Me.nudIDPret.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbDatePret.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbDatePret.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbMontantPret.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbLivreur.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbBalancePret.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbNombreSemaine.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem20, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem18, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutAjouterPaiement, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutPayeAuLivreur1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutPayeAuLivreur2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Root, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'TS
        '
        Me.TS.AutoSize = False
        Me.TS.Font = New System.Drawing.Font("Verdana", 9.75!)
        Me.TS.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Head, Me.bAdd, Me.sp1, Me.bEdit, Me.sp2, Me.bDelete, Me.sp3, Me.bCancel, Me.sp4, Me.bRefresh, Me.sp5, Me.ToolStripButton6})
        Me.TS.Location = New System.Drawing.Point(0, 0)
        Me.TS.Name = "TS"
        Me.TS.Size = New System.Drawing.Size(941, 39)
        Me.TS.TabIndex = 8
        '
        'Head
        '
        Me.Head.Name = "Head"
        Me.Head.Size = New System.Drawing.Size(0, 36)
        '
        'bAdd
        '
        Me.bAdd.Image = CType(resources.GetObject("bAdd.Image"), System.Drawing.Image)
        Me.bAdd.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.bAdd.Name = "bAdd"
        Me.bAdd.Size = New System.Drawing.Size(76, 36)
        Me.bAdd.Text = "Ajouter"
        '
        'sp1
        '
        Me.sp1.Name = "sp1"
        Me.sp1.Size = New System.Drawing.Size(6, 39)
        '
        'bEdit
        '
        Me.bEdit.Image = CType(resources.GetObject("bEdit.Image"), System.Drawing.Image)
        Me.bEdit.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.bEdit.Name = "bEdit"
        Me.bEdit.Size = New System.Drawing.Size(72, 36)
        Me.bEdit.Text = "Edition"
        '
        'sp2
        '
        Me.sp2.Name = "sp2"
        Me.sp2.Size = New System.Drawing.Size(6, 39)
        '
        'bDelete
        '
        Me.bDelete.Image = CType(resources.GetObject("bDelete.Image"), System.Drawing.Image)
        Me.bDelete.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.bDelete.Name = "bDelete"
        Me.bDelete.Size = New System.Drawing.Size(93, 36)
        Me.bDelete.Text = "Supprimer"
        '
        'sp3
        '
        Me.sp3.Name = "sp3"
        Me.sp3.Size = New System.Drawing.Size(6, 39)
        '
        'bCancel
        '
        Me.bCancel.Image = CType(resources.GetObject("bCancel.Image"), System.Drawing.Image)
        Me.bCancel.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.bCancel.Name = "bCancel"
        Me.bCancel.Size = New System.Drawing.Size(77, 36)
        Me.bCancel.Text = "Annuler"
        '
        'sp4
        '
        Me.sp4.Name = "sp4"
        Me.sp4.Size = New System.Drawing.Size(6, 39)
        '
        'bRefresh
        '
        Me.bRefresh.Image = CType(resources.GetObject("bRefresh.Image"), System.Drawing.Image)
        Me.bRefresh.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.bRefresh.Name = "bRefresh"
        Me.bRefresh.Size = New System.Drawing.Size(89, 36)
        Me.bRefresh.Text = "Rafraîchir"
        '
        'sp5
        '
        Me.sp5.Name = "sp5"
        Me.sp5.Size = New System.Drawing.Size(6, 39)
        '
        'ToolStripButton6
        '
        Me.ToolStripButton6.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.ToolStripButton6.Image = CType(resources.GetObject("ToolStripButton6.Image"), System.Drawing.Image)
        Me.ToolStripButton6.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton6.Name = "ToolStripButton6"
        Me.ToolStripButton6.Size = New System.Drawing.Size(73, 36)
        Me.ToolStripButton6.Text = "Fermer"
        '
        'LayoutControl1
        '
        Me.LayoutControl1.Controls.Add(Me.tabData)
        Me.LayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LayoutControl1.Location = New System.Drawing.Point(0, 39)
        Me.LayoutControl1.Name = "LayoutControl1"
        Me.LayoutControl1.Root = Me.Root
        Me.LayoutControl1.Size = New System.Drawing.Size(941, 695)
        Me.LayoutControl1.TabIndex = 9
        Me.LayoutControl1.Text = "LayoutControl1"
        '
        'tabData
        '
        Me.tabData.Location = New System.Drawing.Point(12, 12)
        Me.tabData.Name = "tabData"
        Me.tabData.SelectedTabPage = Me.XtraTabPage1
        Me.tabData.Size = New System.Drawing.Size(917, 671)
        Me.tabData.TabIndex = 9
        Me.tabData.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.XtraTabPage1, Me.XtraTabPage2})
        '
        'XtraTabPage1
        '
        Me.XtraTabPage1.Controls.Add(Me.grdPrets)
        Me.XtraTabPage1.Name = "XtraTabPage1"
        Me.XtraTabPage1.Size = New System.Drawing.Size(912, 645)
        Me.XtraTabPage1.Text = "Liste des prêts"
        '
        'grdPrets
        '
        Me.grdPrets.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grdPrets.Location = New System.Drawing.Point(0, 0)
        Me.grdPrets.MainView = Me.GridView1
        Me.grdPrets.Name = "grdPrets"
        Me.grdPrets.Size = New System.Drawing.Size(916, 627)
        Me.grdPrets.TabIndex = 0
        Me.grdPrets.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'GridView1
        '
        Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colIDAchat, Me.colDateFrom, Me.GridColumn3, Me.colMontant, Me.GridColumn8, Me.GridColumn9})
        Me.GridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.GridView1.GridControl = Me.grdPrets
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsBehavior.Editable = False
        Me.GridView1.OptionsCustomization.AllowGroup = False
        Me.GridView1.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridView1.OptionsSelection.UseIndicatorForSelection = False
        Me.GridView1.OptionsView.ShowAutoFilterRow = True
        Me.GridView1.OptionsView.ShowGroupPanel = False
        Me.GridView1.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.colDateFrom, DevExpress.Data.ColumnSortOrder.Ascending)})
        '
        'colIDAchat
        '
        Me.colIDAchat.Caption = "IDPret"
        Me.colIDAchat.FieldName = "IDPret"
        Me.colIDAchat.Name = "colIDAchat"
        Me.colIDAchat.Visible = True
        Me.colIDAchat.VisibleIndex = 0
        Me.colIDAchat.Width = 196
        '
        'colDateFrom
        '
        Me.colDateFrom.Caption = "Date du Prêt"
        Me.colDateFrom.FieldName = "DatePret"
        Me.colDateFrom.Name = "colDateFrom"
        Me.colDateFrom.Visible = True
        Me.colDateFrom.VisibleIndex = 1
        Me.colDateFrom.Width = 196
        '
        'GridColumn3
        '
        Me.GridColumn3.Caption = "Livreur"
        Me.GridColumn3.FieldName = "Livreur"
        Me.GridColumn3.Name = "GridColumn3"
        Me.GridColumn3.Visible = True
        Me.GridColumn3.VisibleIndex = 2
        Me.GridColumn3.Width = 152
        '
        'colMontant
        '
        Me.colMontant.Caption = "Montant"
        Me.colMontant.DisplayFormat.FormatString = "c"
        Me.colMontant.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.colMontant.FieldName = "MontantPret"
        Me.colMontant.Name = "colMontant"
        Me.colMontant.Visible = True
        Me.colMontant.VisibleIndex = 3
        Me.colMontant.Width = 204
        '
        'GridColumn8
        '
        Me.GridColumn8.Caption = "IDLivreur"
        Me.GridColumn8.FieldName = "IDLivreur"
        Me.GridColumn8.Name = "GridColumn8"
        '
        'GridColumn9
        '
        Me.GridColumn9.Caption = "Balance"
        Me.GridColumn9.Name = "GridColumn9"
        Me.GridColumn9.Visible = True
        Me.GridColumn9.VisibleIndex = 4
        '
        'XtraTabPage2
        '
        Me.XtraTabPage2.Controls.Add(Me.LayoutControl2)
        Me.XtraTabPage2.Name = "XtraTabPage2"
        Me.XtraTabPage2.PageVisible = False
        Me.XtraTabPage2.Size = New System.Drawing.Size(912, 645)
        Me.XtraTabPage2.Text = "Détail sur un prêt"
        '
        'LayoutControl2
        '
        Me.LayoutControl2.Controls.Add(Me.tbPaiementParPeriode)
        Me.LayoutControl2.Controls.Add(Me.grdPaiements)
        Me.LayoutControl2.Controls.Add(Me.TLPBut)
        Me.LayoutControl2.Controls.Add(Me.nudIDPret)
        Me.LayoutControl2.Controls.Add(Me.tbDatePret)
        Me.LayoutControl2.Controls.Add(Me.tbMontantPret)
        Me.LayoutControl2.Controls.Add(Me.cbLivreur)
        Me.LayoutControl2.Controls.Add(Me.tbBalancePret)
        Me.LayoutControl2.Controls.Add(Me.tbNombreSemaine)
        Me.LayoutControl2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LayoutControl2.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControl2.Name = "LayoutControl2"
        Me.LayoutControl2.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = New System.Drawing.Rectangle(976, 223, 771, 350)
        Me.LayoutControl2.OptionsCustomizationForm.ShowPropertyGrid = True
        Me.LayoutControl2.Root = Me.LayoutControlGroup1
        Me.LayoutControl2.Size = New System.Drawing.Size(912, 645)
        Me.LayoutControl2.TabIndex = 0
        Me.LayoutControl2.Text = "LayoutControl2"
        '
        'tbPaiementParPeriode
        '
        Me.tbPaiementParPeriode.Location = New System.Drawing.Point(501, 92)
        Me.tbPaiementParPeriode.Name = "tbPaiementParPeriode"
        Me.tbPaiementParPeriode.Properties.DisplayFormat.FormatString = "c"
        Me.tbPaiementParPeriode.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.tbPaiementParPeriode.Properties.EditFormat.FormatString = "c"
        Me.tbPaiementParPeriode.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.tbPaiementParPeriode.Properties.Mask.EditMask = "c"
        Me.tbPaiementParPeriode.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.tbPaiementParPeriode.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.tbPaiementParPeriode.Size = New System.Drawing.Size(387, 20)
        Me.tbPaiementParPeriode.StyleController = Me.LayoutControl2
        Me.tbPaiementParPeriode.TabIndex = 130
        '
        'grdPaiements
        '
        Me.grdPaiements.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grdPaiements.Location = New System.Drawing.Point(24, 202)
        Me.grdPaiements.MainView = Me.GridView2
        Me.grdPaiements.Name = "grdPaiements"
        Me.grdPaiements.Size = New System.Drawing.Size(864, 302)
        Me.grdPaiements.TabIndex = 129
        Me.grdPaiements.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView2})
        '
        'GridView2
        '
        Me.GridView2.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.IDPretPaiment, Me.DatePaiement, Me.Periode, Me.Livreur, Me.Paiement, Me.IDLivreur, Me.IDPret})
        Me.GridView2.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.GridView2.GridControl = Me.grdPaiements
        Me.GridView2.Name = "GridView2"
        Me.GridView2.OptionsBehavior.Editable = False
        Me.GridView2.OptionsCustomization.AllowGroup = False
        Me.GridView2.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridView2.OptionsSelection.UseIndicatorForSelection = False
        Me.GridView2.OptionsView.ShowAutoFilterRow = True
        Me.GridView2.OptionsView.ShowGroupPanel = False
        Me.GridView2.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.DatePaiement, DevExpress.Data.ColumnSortOrder.Ascending)})
        '
        'IDPretPaiment
        '
        Me.IDPretPaiment.Caption = "ID Pret Paiement"
        Me.IDPretPaiment.FieldName = "IDPretPaiment"
        Me.IDPretPaiment.Name = "IDPretPaiment"
        Me.IDPretPaiment.Width = 196
        '
        'DatePaiement
        '
        Me.DatePaiement.Caption = "Date de paiement"
        Me.DatePaiement.FieldName = "DatePaiement"
        Me.DatePaiement.Name = "DatePaiement"
        Me.DatePaiement.Visible = True
        Me.DatePaiement.VisibleIndex = 0
        Me.DatePaiement.Width = 348
        '
        'Periode
        '
        Me.Periode.Caption = "Période"
        Me.Periode.FieldName = "Periode"
        Me.Periode.Name = "Periode"
        Me.Periode.Visible = True
        Me.Periode.VisibleIndex = 1
        Me.Periode.Width = 228
        '
        'Livreur
        '
        Me.Livreur.Caption = "Livreur"
        Me.Livreur.FieldName = "Livreur"
        Me.Livreur.Name = "Livreur"
        Me.Livreur.Width = 152
        '
        'Paiement
        '
        Me.Paiement.Caption = "Paiement"
        Me.Paiement.DisplayFormat.FormatString = "c"
        Me.Paiement.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.Paiement.FieldName = "Paiement"
        Me.Paiement.Name = "Paiement"
        Me.Paiement.Visible = True
        Me.Paiement.VisibleIndex = 2
        Me.Paiement.Width = 269
        '
        'IDLivreur
        '
        Me.IDLivreur.Caption = "IDLivreur"
        Me.IDLivreur.FieldName = "IDLivreur"
        Me.IDLivreur.Name = "IDLivreur"
        '
        'IDPret
        '
        Me.IDPret.Caption = "IDPret"
        Me.IDPret.FieldName = "IDPret"
        Me.IDPret.Name = "IDPret"
        '
        'TLPBut
        '
        Me.TLPBut.ColumnCount = 7
        Me.TLPBut.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TLPBut.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100.0!))
        Me.TLPBut.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TLPBut.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100.0!))
        Me.TLPBut.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TLPBut.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100.0!))
        Me.TLPBut.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TLPBut.Controls.Add(Me.butApply, 1, 1)
        Me.TLPBut.Controls.Add(Me.butClose, 5, 1)
        Me.TLPBut.Controls.Add(Me.butCancel, 3, 1)
        Me.TLPBut.Location = New System.Drawing.Point(12, 520)
        Me.TLPBut.Name = "TLPBut"
        Me.TLPBut.RowCount = 2
        Me.TLPBut.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26.0!))
        Me.TLPBut.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 34.0!))
        Me.TLPBut.Size = New System.Drawing.Size(888, 113)
        Me.TLPBut.TabIndex = 8
        '
        'butApply
        '
        Me.butApply.Dock = System.Windows.Forms.DockStyle.Top
        Me.butApply.Location = New System.Drawing.Point(277, 29)
        Me.butApply.Name = "butApply"
        Me.butApply.Size = New System.Drawing.Size(94, 28)
        Me.butApply.TabIndex = 20
        Me.butApply.Text = "Appliquer"
        '
        'butClose
        '
        Me.butClose.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.butClose.Dock = System.Windows.Forms.DockStyle.Top
        Me.butClose.Location = New System.Drawing.Point(517, 29)
        Me.butClose.Name = "butClose"
        Me.butClose.Size = New System.Drawing.Size(94, 28)
        Me.butClose.TabIndex = 22
        Me.butClose.Text = "Fermer"
        '
        'butCancel
        '
        Me.butCancel.Dock = System.Windows.Forms.DockStyle.Top
        Me.butCancel.Location = New System.Drawing.Point(397, 29)
        Me.butCancel.Name = "butCancel"
        Me.butCancel.Size = New System.Drawing.Size(94, 28)
        Me.butCancel.TabIndex = 21
        Me.butCancel.Text = "Retour a la grille"
        '
        'nudIDPret
        '
        Me.nudIDPret.Enabled = False
        Me.nudIDPret.Location = New System.Drawing.Point(136, 12)
        Me.nudIDPret.Name = "nudIDPret"
        Me.nudIDPret.Properties.Appearance.BackColor = System.Drawing.Color.Azure
        Me.nudIDPret.Properties.Appearance.Options.UseBackColor = True
        Me.nudIDPret.Size = New System.Drawing.Size(225, 20)
        Me.nudIDPret.StyleController = Me.LayoutControl2
        Me.nudIDPret.TabIndex = 5
        '
        'tbDatePret
        '
        Me.tbDatePret.EditValue = Nothing
        Me.tbDatePret.Location = New System.Drawing.Point(136, 36)
        Me.tbDatePret.Name = "tbDatePret"
        Me.tbDatePret.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.tbDatePret.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.tbDatePret.Properties.DisplayFormat.FormatString = "MM-dd-yy"
        Me.tbDatePret.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.tbDatePret.Properties.EditFormat.FormatString = "MM-dd-yy"
        Me.tbDatePret.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.tbDatePret.Properties.Mask.EditMask = "MM-dd-yy"
        Me.tbDatePret.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.tbDatePret.Size = New System.Drawing.Size(225, 20)
        Me.tbDatePret.StyleController = Me.LayoutControl2
        Me.tbDatePret.TabIndex = 22
        '
        'tbMontantPret
        '
        Me.tbMontantPret.EditValue = "0"
        Me.tbMontantPret.Enabled = False
        Me.tbMontantPret.Location = New System.Drawing.Point(501, 44)
        Me.tbMontantPret.Name = "tbMontantPret"
        Me.tbMontantPret.Properties.Appearance.BackColor = System.Drawing.SystemColors.Info
        Me.tbMontantPret.Properties.Appearance.Options.UseBackColor = True
        Me.tbMontantPret.Properties.DisplayFormat.FormatString = "c"
        Me.tbMontantPret.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.tbMontantPret.Properties.EditFormat.FormatString = "c"
        Me.tbMontantPret.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.tbMontantPret.Properties.Mask.EditMask = "c"
        Me.tbMontantPret.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.tbMontantPret.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.tbMontantPret.Size = New System.Drawing.Size(387, 20)
        Me.tbMontantPret.StyleController = Me.LayoutControl2
        Me.tbMontantPret.TabIndex = 128
        '
        'cbLivreur
        '
        Me.cbLivreur.Location = New System.Drawing.Point(136, 60)
        Me.cbLivreur.Name = "cbLivreur"
        Me.cbLivreur.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbLivreur.Size = New System.Drawing.Size(225, 20)
        Me.cbLivreur.StyleController = Me.LayoutControl2
        Me.cbLivreur.TabIndex = 30
        '
        'tbBalancePret
        '
        Me.tbBalancePret.EditValue = "0"
        Me.tbBalancePret.Enabled = False
        Me.tbBalancePret.Location = New System.Drawing.Point(501, 116)
        Me.tbBalancePret.Name = "tbBalancePret"
        Me.tbBalancePret.Properties.Appearance.BackColor = System.Drawing.SystemColors.Info
        Me.tbBalancePret.Properties.Appearance.Options.UseBackColor = True
        Me.tbBalancePret.Properties.DisplayFormat.FormatString = "c"
        Me.tbBalancePret.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.tbBalancePret.Properties.EditFormat.FormatString = "c"
        Me.tbBalancePret.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.tbBalancePret.Properties.Mask.EditMask = "c"
        Me.tbBalancePret.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.tbBalancePret.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.tbBalancePret.Size = New System.Drawing.Size(387, 20)
        Me.tbBalancePret.StyleController = Me.LayoutControl2
        Me.tbBalancePret.TabIndex = 128
        '
        'tbNombreSemaine
        '
        Me.tbNombreSemaine.Location = New System.Drawing.Point(501, 68)
        Me.tbNombreSemaine.Name = "tbNombreSemaine"
        Me.tbNombreSemaine.Size = New System.Drawing.Size(387, 20)
        Me.tbNombreSemaine.StyleController = Me.LayoutControl2
        Me.tbNombreSemaine.TabIndex = 130
        '
        'LayoutControlGroup1
        '
        Me.LayoutControlGroup1.CustomizationFormText = "LayoutControlGroup1"
        Me.LayoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup1.GroupBordersVisible = False
        Me.LayoutControlGroup1.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem20, Me.LayoutControlItem8, Me.LayoutControlItem18, Me.LayoutControlItem7, Me.LayoutAjouterPaiement, Me.LayoutControlGroup2})
        Me.LayoutControlGroup1.Name = "Root"
        Me.LayoutControlGroup1.Size = New System.Drawing.Size(912, 645)
        Me.LayoutControlGroup1.TextVisible = False
        '
        'LayoutControlItem20
        '
        Me.LayoutControlItem20.Control = Me.TLPBut
        Me.LayoutControlItem20.CustomizationFormText = "LayoutControlItem20"
        Me.LayoutControlItem20.Location = New System.Drawing.Point(0, 508)
        Me.LayoutControlItem20.Name = "LayoutControlItem20"
        Me.LayoutControlItem20.Size = New System.Drawing.Size(892, 117)
        Me.LayoutControlItem20.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem20.TextVisible = False
        '
        'LayoutControlItem8
        '
        Me.LayoutControlItem8.Control = Me.nudIDPret
        Me.LayoutControlItem8.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem8.Name = "LayoutControlItem8"
        Me.LayoutControlItem8.Size = New System.Drawing.Size(353, 24)
        Me.LayoutControlItem8.Text = "Prêt ID"
        Me.LayoutControlItem8.TextSize = New System.Drawing.Size(121, 13)
        '
        'LayoutControlItem18
        '
        Me.LayoutControlItem18.Control = Me.tbDatePret
        Me.LayoutControlItem18.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem18.CustomizationFormText = "Date du"
        Me.LayoutControlItem18.Location = New System.Drawing.Point(0, 24)
        Me.LayoutControlItem18.Name = "LayoutControlItem18"
        Me.LayoutControlItem18.Size = New System.Drawing.Size(353, 24)
        Me.LayoutControlItem18.Text = "Date de création du  prêt"
        Me.LayoutControlItem18.TextSize = New System.Drawing.Size(121, 13)
        '
        'LayoutControlItem7
        '
        Me.LayoutControlItem7.Control = Me.cbLivreur
        Me.LayoutControlItem7.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem7.CustomizationFormText = "Liste des payes"
        Me.LayoutControlItem7.Location = New System.Drawing.Point(0, 48)
        Me.LayoutControlItem7.Name = "LayoutControlItem7"
        Me.LayoutControlItem7.Size = New System.Drawing.Size(353, 102)
        Me.LayoutControlItem7.Text = "Nom du Livreur"
        Me.LayoutControlItem7.TextSize = New System.Drawing.Size(121, 13)
        '
        'LayoutAjouterPaiement
        '
        Me.LayoutAjouterPaiement.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.EmptySpaceItem2, Me.LayoutPayeAuLivreur1, Me.LayoutPayeAuLivreur2, Me.LayoutControlItem3, Me.LayoutControlItem4})
        Me.LayoutAjouterPaiement.Location = New System.Drawing.Point(353, 0)
        Me.LayoutAjouterPaiement.Name = "LayoutAjouterPaiement"
        Me.LayoutAjouterPaiement.Size = New System.Drawing.Size(539, 150)
        Me.LayoutAjouterPaiement.Text = "Information sur le prêts"
        '
        'EmptySpaceItem2
        '
        Me.EmptySpaceItem2.AllowHotTrack = False
        Me.EmptySpaceItem2.Location = New System.Drawing.Point(0, 96)
        Me.EmptySpaceItem2.Name = "EmptySpaceItem2"
        Me.EmptySpaceItem2.Size = New System.Drawing.Size(515, 10)
        Me.EmptySpaceItem2.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutPayeAuLivreur1
        '
        Me.LayoutPayeAuLivreur1.Control = Me.tbMontantPret
        Me.LayoutPayeAuLivreur1.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutPayeAuLivreur1.CustomizationFormText = "Payé au livreur"
        Me.LayoutPayeAuLivreur1.Location = New System.Drawing.Point(0, 0)
        Me.LayoutPayeAuLivreur1.Name = "LayoutPayeAuLivreur1"
        Me.LayoutPayeAuLivreur1.Size = New System.Drawing.Size(515, 24)
        Me.LayoutPayeAuLivreur1.Text = "Montant du prêt"
        Me.LayoutPayeAuLivreur1.TextSize = New System.Drawing.Size(121, 13)
        '
        'LayoutPayeAuLivreur2
        '
        Me.LayoutPayeAuLivreur2.Control = Me.tbBalancePret
        Me.LayoutPayeAuLivreur2.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutPayeAuLivreur2.CustomizationFormText = "Payé au livreur"
        Me.LayoutPayeAuLivreur2.Location = New System.Drawing.Point(0, 72)
        Me.LayoutPayeAuLivreur2.Name = "LayoutPayeAuLivreur2"
        Me.LayoutPayeAuLivreur2.Size = New System.Drawing.Size(515, 24)
        Me.LayoutPayeAuLivreur2.Text = "Balance du prêt"
        Me.LayoutPayeAuLivreur2.TextSize = New System.Drawing.Size(121, 13)
        '
        'LayoutControlItem3
        '
        Me.LayoutControlItem3.Control = Me.tbPaiementParPeriode
        Me.LayoutControlItem3.Location = New System.Drawing.Point(0, 48)
        Me.LayoutControlItem3.Name = "LayoutControlItem3"
        Me.LayoutControlItem3.Size = New System.Drawing.Size(515, 24)
        Me.LayoutControlItem3.Text = "Paiement par période"
        Me.LayoutControlItem3.TextSize = New System.Drawing.Size(121, 13)
        '
        'LayoutControlItem4
        '
        Me.LayoutControlItem4.Control = Me.tbNombreSemaine
        Me.LayoutControlItem4.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem4.CustomizationFormText = "Nombre de semaines"
        Me.LayoutControlItem4.Location = New System.Drawing.Point(0, 24)
        Me.LayoutControlItem4.Name = "LayoutControlItem4"
        Me.LayoutControlItem4.Size = New System.Drawing.Size(515, 24)
        Me.LayoutControlItem4.Text = "Nombre de semaines"
        Me.LayoutControlItem4.TextSize = New System.Drawing.Size(121, 13)
        '
        'LayoutControlGroup2
        '
        Me.LayoutControlGroup2.AppearanceGroup.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlGroup2.AppearanceGroup.Options.UseFont = True
        Me.LayoutControlGroup2.AppearanceGroup.Options.UseTextOptions = True
        Me.LayoutControlGroup2.AppearanceGroup.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.LayoutControlGroup2.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem1})
        Me.LayoutControlGroup2.Location = New System.Drawing.Point(0, 150)
        Me.LayoutControlGroup2.Name = "LayoutControlGroup2"
        Me.LayoutControlGroup2.Size = New System.Drawing.Size(892, 358)
        Me.LayoutControlGroup2.Text = "Paiements sur le prêts"
        '
        'LayoutControlItem1
        '
        Me.LayoutControlItem1.Control = Me.grdPaiements
        Me.LayoutControlItem1.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem1.Name = "LayoutControlItem1"
        Me.LayoutControlItem1.Size = New System.Drawing.Size(868, 306)
        Me.LayoutControlItem1.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem1.TextVisible = False
        '
        'Root
        '
        Me.Root.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.Root.GroupBordersVisible = False
        Me.Root.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem2})
        Me.Root.Name = "Root"
        Me.Root.Size = New System.Drawing.Size(941, 695)
        Me.Root.TextVisible = False
        '
        'LayoutControlItem2
        '
        Me.LayoutControlItem2.Control = Me.tabData
        Me.LayoutControlItem2.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem2.Name = "LayoutControlItem2"
        Me.LayoutControlItem2.Size = New System.Drawing.Size(921, 675)
        Me.LayoutControlItem2.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem2.TextVisible = False
        '
        'frmPrets
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(941, 734)
        Me.Controls.Add(Me.LayoutControl1)
        Me.Controls.Add(Me.TS)
        Me.Name = "frmPrets"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Gestion des prets"
        Me.TS.ResumeLayout(False)
        Me.TS.PerformLayout()
        CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LayoutControl1.ResumeLayout(False)
        CType(Me.tabData, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabData.ResumeLayout(False)
        Me.XtraTabPage1.ResumeLayout(False)
        CType(Me.grdPrets, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabPage2.ResumeLayout(False)
        CType(Me.LayoutControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LayoutControl2.ResumeLayout(False)
        CType(Me.tbPaiementParPeriode.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grdPaiements, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TLPBut.ResumeLayout(False)
        CType(Me.nudIDPret.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbDatePret.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbDatePret.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbMontantPret.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbLivreur.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbBalancePret.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbNombreSemaine.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem20, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem18, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutAjouterPaiement, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutPayeAuLivreur1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutPayeAuLivreur2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Root, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents TS As ToolStrip
    Friend WithEvents Head As ToolStripLabel
    Friend WithEvents bAdd As ToolStripButton
    Friend WithEvents sp1 As ToolStripSeparator
    Friend WithEvents bEdit As ToolStripButton
    Friend WithEvents sp2 As ToolStripSeparator
    Friend WithEvents bDelete As ToolStripButton
    Friend WithEvents sp3 As ToolStripSeparator
    Friend WithEvents bCancel As ToolStripButton
    Friend WithEvents sp4 As ToolStripSeparator
    Friend WithEvents bRefresh As ToolStripButton
    Friend WithEvents sp5 As ToolStripSeparator
    Friend WithEvents ToolStripButton6 As ToolStripButton
    Friend WithEvents LayoutControl1 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents Root As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents tabData As DevExpress.XtraTab.XtraTabControl
    Friend WithEvents XtraTabPage1 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents grdPrets As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colIDAchat As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colDateFrom As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn3 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colMontant As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents XtraTabPage2 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents LayoutControl2 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents grdPaiements As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView2 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents IDPretPaiment As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents DatePaiement As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents Livreur As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents Paiement As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents IDLivreur As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents IDPret As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents TLPBut As TableLayoutPanel
    Friend WithEvents butApply As Button
    Friend WithEvents butClose As Button
    Friend WithEvents butCancel As Button
    Friend WithEvents nudIDPret As DevExpress.XtraEditors.TextEdit
    Friend WithEvents tbDatePret As DevExpress.XtraEditors.DateEdit
    Friend WithEvents tbMontantPret As DevExpress.XtraEditors.TextEdit
    Friend WithEvents cbLivreur As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LayoutControlGroup1 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem20 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem8 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem18 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem7 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutPayeAuLivreur1 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem1 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutAjouterPaiement As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents EmptySpaceItem2 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents LayoutControlItem2 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents tbBalancePret As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutPayeAuLivreur2 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents GridColumn8 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn9 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents tbPaiementParPeriode As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlItem3 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents tbNombreSemaine As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlItem4 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlGroup2 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents Periode As DevExpress.XtraGrid.Columns.GridColumn
End Class
