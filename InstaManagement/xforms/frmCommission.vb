﻿Public Class frmCommission



    Dim PAGEVISIBLEINDEX As Integer
    Dim oCommissionData As New CommissionData
    Dim oCommissionlineData As New CommissionLineData
    Dim oCommissionLine As New CommissionLine
    Dim oCommission As New Commission
    Dim oImportData As New ImportData
    Dim SelectedRow As Integer
    Dim iID As Integer
    Dim oPaie As New Commission
    Dim oPaieData As New CommissionData
    Dim oGenerationCommission As New GenerationCommissions
    Dim dtOrganisation As DataTable
    Dim drOrganisation As DataRow
    Dim dtCommissionLine As DataTable
    Dim drCommissionLine As DataRow
    Private m_IDCommission As Int32
    Private m_CommissionNumber As String
    Private m_CommissionDate As Nullable(Of DateTime)
    Private m_IDLivreur As Nullable(Of Int32)
    Private m_Livreur As String
    Private m_DateFrom As Nullable(Of DateTime)
    Private m_DateTo As Nullable(Of DateTime)
    Private m_IDPeriode As Nullable(Of Int32)
    Private m_OrganisationID As Nullable(Of Int32)
    Private m_Organisation As String
    Private m_IDCommissionLine As Int32
    Public Property IDCommissionLine() As Int32
        Get
            Return m_IDCommissionLine
        End Get
        Set(ByVal value As Int32)
            m_IDCommissionLine = value
        End Set
    End Property
    Public Property IDCommission() As Int32
        Get
            Return m_IDCommission
        End Get
        Set(ByVal value As Int32)
            m_IDCommission = value
        End Set
    End Property

    Public Property CommissionNumber() As String
        Get
            Return m_CommissionNumber
        End Get
        Set(ByVal value As String)
            m_CommissionNumber = value
        End Set
    End Property

    Public Property CommissionDate() As Nullable(Of DateTime)
        Get
            Return m_CommissionDate
        End Get
        Set(ByVal value As Nullable(Of DateTime))
            m_CommissionDate = value
        End Set
    End Property

    Public Property IDLivreur() As Nullable(Of Int32)
        Get
            Return m_IDLivreur
        End Get
        Set(ByVal value As Nullable(Of Int32))
            m_IDLivreur = value
        End Set
    End Property

    Public Property Livreur() As String
        Get
            Return m_Livreur
        End Get
        Set(ByVal value As String)
            m_Livreur = value
        End Set
    End Property

    Public Property DateFrom() As Nullable(Of DateTime)
        Get
            Return m_DateFrom
        End Get
        Set(ByVal value As Nullable(Of DateTime))
            m_DateFrom = value
        End Set
    End Property

    Public Property DateTo() As Nullable(Of DateTime)
        Get
            Return m_DateTo
        End Get
        Set(ByVal value As Nullable(Of DateTime))
            m_DateTo = value
        End Set
    End Property

    Public Property IDPeriode() As Nullable(Of Int32)
        Get
            Return m_IDPeriode
        End Get
        Set(ByVal value As Nullable(Of Int32))
            m_IDPeriode = value
        End Set
    End Property



    Public Property OrganisationID() As Nullable(Of Int32)
        Get
            Return m_OrganisationID
        End Get
        Set(ByVal value As Nullable(Of Int32))
            m_OrganisationID = value
        End Set
    End Property

    Public Property Organisation() As String
        Get
            Return m_Organisation
        End Get
        Set(ByVal value As String)
            m_Organisation = value
        End Set
    End Property
    Private Sub FillAllVendeurs()


        Dim dtLivreur As DataTable
        Dim drLivreur As DataRow
        Dim oLivreurData As New LivreursData

        Dim table As New DataTable
        table.Columns.Add("value", Type.GetType("System.Int32"))
        table.Columns.Add("Livreur", Type.GetType("System.String"))
        dtLivreur = oLivreurData.SelectAllVendeurs



        For Each drLivreur In dtLivreur.Rows
            Dim teller As Integer = 0

            teller = drLivreur("LivreurID")
            Dim toto As String = drLivreur("Livreur")
            table.Rows.Add(New Object() {teller, toto})
        Next


        cbLivreur.Properties.DataSource = table
        cbLivreur.Properties.NullText = "Selectionner un vendeur"
        cbLivreur.Properties.DisplayMember = "Livreur"
        cbLivreur.Properties.ValueMember = "value"
        cbLivreur.Properties.PopupFormMinSize = New Size(50, 200)
        cbLivreurNP.Properties.DataSource = table
        cbLivreurNP.Properties.NullText = "Selectionner un vendeur"
        cbLivreurNP.Properties.DisplayMember = "Livreur"
        cbLivreurNP.Properties.ValueMember = "value"
        cbLivreurNP.Properties.PopupFormMinSize = New Size(50, 200)

    End Sub


    Private Sub FillOrganisations()
        Dim drOrganisation As DataRow
        Dim dtOrganisation As DataTable


        Dim oOrganisationData As New OrganisationsData

        Dim table As New DataTable
        table.Columns.Add("value", Type.GetType("System.Int32"))
        table.Columns.Add("displayText", Type.GetType("System.String"))
        dtOrganisation = oOrganisationData.SelectAll()


        For Each drOrganisation In dtOrganisation.Rows
            Dim teller As Integer = 0

            teller = drOrganisation("OrganisationID")
            Dim toto As String = drOrganisation("Organisation").ToString.Trim

            table.Rows.Add(New Object() {teller, toto})
        Next

        With cbOrganisation.Properties
            .DataSource = table
            .NullText = "Selectionner un organisation"
            .DisplayMember = "displayText"
            .ValueMember = "value"
            .PopulateColumns()
            .PopupFormMinSize = New Size(50, 200)
            '.Columns("value").Visible = True
            '.Columns("displayText").Caption = "Organisation"
        End With

    End Sub
    Private Sub FillPeriode()

        Dim dtperiode As DataTable
        Dim drPeriode As DataRow

        Dim oPeriodeData As New PeriodesData

        Dim table As New DataTable
        table.Columns.Add("value", Type.GetType("System.Int32"))
        table.Columns.Add("displayText", Type.GetType("System.String"))
        dtperiode = oPeriodeData.SelectAllPeriode(0)


        For Each drPeriode In dtperiode.Rows
            Dim teller As Integer = 0

            teller = drPeriode("IDPeriode")
            Dim toto As String = drPeriode("Periode")

            table.Rows.Add(New Object() {teller, toto})
        Next



        With cbPeriodes.Properties
            .DataSource = table
            .NullText = "Selectionner une période"
            .DisplayMember = "displayText"
            .ValueMember = "value"
            .PopulateColumns()
            .PopupFormMinSize = New Size(50, 200)
            '.Columns("value").Visible = True
            '.Columns("displayText").Caption = "Périodes"
        End With

        With cbPeriodesNP.Properties
            .DataSource = table
            .NullText = "Selectionner une période"
            .DisplayMember = "displayText"
            .ValueMember = "value"
            .PopulateColumns()
            .PopupFormMinSize = New Size(50, 200)
            '.Columns("value").Visible = True
            '.Columns("displayText").Caption = "Périodes"
        End With

    End Sub


    Private Sub grdCommission_Click(sender As Object, e As EventArgs) Handles grdCommission.Click
        bDelete.Visible = True
    End Sub

    Private Sub grdCommission_DoubleClick(sender As Object, e As EventArgs) Handles grdCommission.DoubleClick


        If GridView11.FocusedRowHandle < 0 Then Exit Sub

        Dim row As System.Data.DataRow = GridView11.GetDataRow(GridView11.FocusedRowHandle)

        iID = row("IDCommission")

        IDCommission = Nothing
        CommissionNumber = Nothing
        CommissionDate = Nothing
        IDLivreur = Nothing
        Livreur = Nothing
        DateFrom = Nothing
        DateTo = Nothing
        IDPeriode = Nothing
        OrganisationID = Nothing
        Organisation = Nothing


        oCommissionLine.IDCommission = Nothing
        oCommissionLine.CommissionNumber = Nothing
        oCommissionLine.CommissionDate = Nothing
        oCommissionLine.IDLivreur = Nothing
        oCommissionLine.Livreur = Nothing
        oCommissionLine.OrganisationID = Nothing
        oCommissionLine.Organisation = Nothing
        oCommissionLine.Code = Nothing
        oCommissionLine.Km = Nothing
        oCommissionLine.Description = Nothing
        oCommissionLine.Unite = Nothing
        oCommissionLine.Prix = Nothing
        oCommissionLine.Montant = Nothing
        oCommissionLine.Credit = Nothing
        oCommissionLine.DateFrom = Nothing
        oCommissionLine.DateTo = Nothing
        oCommissionLine.NombreGlacier = Nothing
        oCommissionLine.Glacier = Nothing
        oCommissionLine.IDPeriode = Nothing

        IDCommission = iID
        DateFrom = row("DateFrom")
        DateTo = row("DateTo")
        CommissionDate = row("CommissionDate")
        IDLivreur = row("IDLivreur")
        Livreur = row("Livreur")
        CommissionNumber = row("CommissionNumber")
        IDPeriode = go_Globals.IDPeriode

        grdCommissionLine.DataSource = oCommissionlineData.SelectAllByID(iID)

        bCancel.Visible = True
        bNouvelleCommission.Visible = False
        bAjouterLaCommission.Visible = True
        bDelete.Visible = False

        DockCommission.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Hidden
        SelectedRow = GridView2.FocusedRowHandle
        tabData.SelectedTabPage = tabData.TabPages(1)
        tabData.TabPages(1).PageVisible = True
        tabData.TabPages(0).PageVisible = False
        PAGEVISIBLEINDEX = 1
    End Sub

    Private Sub frmCommission_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        FillOrganisations()
        FillAllVendeurs()
        FillPeriode()
        grdCommission.DataSource = oCommissionData.SelectAllByDate(go_Globals.PeriodeDateFrom, go_Globals.PeriodeDateTo)

        radChoice.SelectedIndex = -1
        PAGEVISIBLEINDEX = 0


        DockNouvelleCommission.FloatLocation = New Point(CInt((Screen.PrimaryScreen.Bounds.Width - DockNouvelleCommission.Width) / 2), CInt(Screen.PrimaryScreen.Bounds.Height - DockNouvelleCommission.Height) / 2)
        DockNouvelleCommission.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Hidden

        DockCommission.FloatLocation = New Point(CInt((Screen.PrimaryScreen.Bounds.Width - DockCommission.Width) / 2), CInt(Screen.PrimaryScreen.Bounds.Height - DockCommission.Height) / 2)


        DockCommission.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Hidden



    End Sub

    Private Sub bSauvegarderNouvelleCommission_Click(sender As Object, e As EventArgs) Handles bSauvegarderNouvelleCommission.Click

        If cbPeriodesNP.EditValue = Nothing Then
            MsgBox("Vous devez sélectionner une période .", MsgBoxStyle.OkOnly, "Erreur")
            cbPeriodesNP.Focus()
            Exit Sub
        End If

        If cbLivreurNP.EditValue = Nothing Then
            MsgBox("Vous devez sélectionner une Livreur.", MsgBoxStyle.OkOnly, "Erreur")
            cbLivreurNP.Focus()
            Exit Sub
        End If

        IDCommission = Nothing
        DateFrom = Nothing
        DateTo = Nothing
        CommissionDate = Nothing
        IDLivreur = Nothing
        Livreur = Nothing
        IDPeriode = Nothing
        OrganisationID = Nothing
        Organisation = Nothing


        Dim result As DialogResult = MessageBox.Show("Sauvegarder la fiche?", "Ajouter une nouvelle Commission ? ", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
        If result = DialogResult.No Then
            Me.Cursor = Cursors.Default
        ElseIf result = DialogResult.Yes Then
            Me.Cursor = Cursors.WaitCursor

            oCommission.CommissionNumber = tbCommissionNumberNP.Text
            oCommission.CommissionDate = Now.ToShortDateString
            oCommission.IDLivreur = cbLivreurNP.EditValue
            oCommission.Livreur = cbLivreurNP.Text
            oCommission.DateFrom = tbDateFromNP.Text
            oCommission.DateTo = tbDateAuNP.Text
            oCommission.IDPeriode = go_Globals.IDPeriode
            iID = oCommissionData.Add(oCommission)



            IDCommission = iID
            CommissionDate = oCommission.CommissionDate
            CommissionNumber = oCommission.CommissionNumber
            DateFrom = oCommission.DateFrom
            DateTo = oCommission.DateTo
            IDLivreur = oCommission.IDLivreur
            Livreur = oCommission.Livreur
            IDPeriode = oCommission.IDPeriode


            tbIDCommission.Text = iID
            tbCommissionNumber.Text = oCommission.CommissionNumber
            cbPeriodes.EditValue = go_Globals.IDPeriode
            tbDateFrom.EditValue = go_Globals.PeriodeDateFrom
            tbDateAu.EditValue = go_Globals.PeriodeDateTo
            cbLivreur.EditValue = oCommission.IDLivreur


            grdCommission.DataSource = oCommissionData.SelectAllByDate(go_Globals.PeriodeDateFrom, go_Globals.PeriodeDateTo)
            grdCommissionLine.DataSource = oCommissionlineData.SelectAllByID(iID)

            tabData.SelectedTabPage = tabData.TabPages(1)
            tabData.TabPages(1).PageVisible = True
            tabData.TabPages(0).PageVisible = False
            bCancel.Visible = True
            bNouvelleCommission.Visible = False
            bAjouterLaCommission.Visible = True

            DockNouvelleCommission.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Hidden
            DockCommission.FloatLocation = New Point(CInt((Screen.PrimaryScreen.Bounds.Width - DockCommission.Width) / 2), CInt(Screen.PrimaryScreen.Bounds.Height - DockCommission.Height) / 2)
            DockCommission.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Visible

            Me.Cursor = Cursors.Default

        End If
    End Sub

    Private Sub bAnnulerNouvelleCommission_Click(sender As Object, e As EventArgs) Handles bAnnulerNouvelleCommission.Click
        DockNouvelleCommission.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Hidden
    End Sub

    Private Sub bSauvegarde_Click(sender As Object, e As EventArgs) Handles bSauvegarde.Click



        If radChoice.SelectedIndex = -1 Then
            MsgBox("Vous devez sélectionner une type d'ajout .", MsgBoxStyle.OkOnly, "Erreur")
            radChoice.Focus()
            Exit Sub
        End If

        If cbPeriodes.EditValue = Nothing Then
            MsgBox("Vous devez sélectionner une période .", MsgBoxStyle.OkOnly, "Erreur")
            cbPeriodes.Focus()
            Exit Sub
        End If

        If cbLivreur.EditValue = Nothing Then
            MsgBox("Vous devez sélectionner une Livreur.", MsgBoxStyle.OkOnly, "Erreur")
            cbLivreur.Focus()
            Exit Sub
        End If


        If cbOrganisation.EditValue = Nothing Then
            MsgBox("Vous devez sélectionner une Organisation .", MsgBoxStyle.OkOnly, "Erreur")
            cbOrganisation.Focus()
            Exit Sub
        End If

        If tbDescription.Text.ToString.Trim = "" Then
            MsgBox("Vous devez entrer une description .", MsgBoxStyle.OkOnly, "Erreur")
            tbDescription.Focus()
            Exit Sub
        End If


        Dim result As DialogResult = MessageBox.Show("Sauvegarder la fiche?", "Ajouter une ligne a la paie", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
        If result = DialogResult.No Then
            Me.Cursor = Cursors.Default
        ElseIf result = DialogResult.Yes Then


ADDGLACIER:

            oCommissionLine.IDCommission = tbIDCommission.Text
            oCommissionLine.CommissionNumber = tbCommissionNumber.Text
            oCommissionLine.CommissionDate = Now.ToShortDateString
            oCommissionLine.IDLivreur = cbLivreur.EditValue
            oCommissionLine.Livreur = cbLivreur.Text
            oCommissionLine.OrganisationID = cbOrganisation.EditValue
            oCommissionLine.Organisation = cbOrganisation.Text
            oCommissionLine.Code = Nothing
            oCommissionLine.Km = 0
            oCommissionLine.Description = tbDescription.Text
            oCommissionLine.Unite = tbQuantite.Text


            If chkAjout.Checked = True Then
                oCommissionLine.Ajout = True
            Else
                oCommissionLine.Ajout = False
            End If


            oCommissionLine.Prix = Convert.ToDouble(tbPrix.EditValue)
            oCommissionLine.Montant = Convert.ToDouble(tbTotal.EditValue)

            oCommissionLine.Credit = 0
            oCommissionLine.DateFrom = tbDateFrom.Text
            oCommissionLine.DateTo = tbDateAu.Text
            oCommissionLine.NombreGlacier = 0
            oCommissionLine.Glacier = 0
            oCommissionLine.TypeAjout = radChoice.SelectedIndex
            oCommissionLine.Ajout = True
            oCommissionLine.IsRestaurant = False
            oCommissionLine.IDPeriode = go_Globals.IDPeriode

            oCommissionlineData.Add(oCommissionLine)
        End If

        grdCommissionLine.DataSource = oCommissionlineData.SelectAllByID(iID)



        bDelete.Visible = False
        DockCommission.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Hidden
    End Sub

    Private Sub btnAnnuler_Click(sender As Object, e As EventArgs) Handles btnAnnuler.Click

        DockCommission.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Hidden
    End Sub



    Private Sub radChoice_SelectedIndexChanged(sender As Object, e As EventArgs) Handles radChoice.SelectedIndexChanged

        Select Case radChoice.SelectedIndex
            Case 0
                tbQuantite.Text = 1
                lblQuantite.Text = "Quantité"
                tbDescription.Text = vbNullString
            Case 1
                tbQuantite.Text = 1
                lblQuantite.Text = "Nombre glacier"
                tbDescription.Text = "Glaciers"


                If Not IsNothing(cbOrganisation.EditValue) Then

                    Dim oOrganisationData As New OrganisationsData
                    OrganisationID = cbOrganisation.EditValue
                    dtOrganisation = oOrganisationData.SelectByID(OrganisationID)

                    If dtOrganisation.Rows.Count > 0 Then

                        For Each drOrganisation In dtOrganisation.Rows
                            tbPrix.Text = If(IsDBNull(drOrganisation("MontantGlacierLivreur")), 0, CType(drOrganisation("MontantGlacierLivreur"), Decimal?))
                            tbTotal.Text = If(IsDBNull(drOrganisation("MontantGlacierLivreur")), 0, CType(drOrganisation("MontantGlacierLivreur"), Decimal?))
                        Next

                    End If
                End If

            Case 2
                tbQuantite.Text = 1
                lblQuantite.Text = "Quantité"
                tbDescription.Text = "Crédit"
            Case 3
                tbQuantite.Text = 1
                lblQuantite.Text = "Quantité"
                tbDescription.Text = "Débit"

            Case 4
                tbQuantite.Text = 1
                lblQuantite.Text = "Quantité"
                tbDescription.Text = "Extras"


        End Select
    End Sub

    Private Sub bCancel_Click(sender As Object, e As EventArgs) Handles bCancel.Click
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

        tabData.SelectedTabPage = tabData.TabPages(0)
        tabData.TabPages(1).PageVisible = False
        tabData.TabPages(0).PageVisible = True

        bCancel.Visible = False
        bNouvelleCommission.Visible = True
        bAjouterLaCommission.Visible = False
        PAGEVISIBLEINDEX = 0

        Me.Cursor = System.Windows.Forms.Cursors.Default
    End Sub

    Private Sub bAjouterLaCommission_Click(sender As Object, e As EventArgs) Handles bAjouterLaCommission.Click

        tbIDCommission.Text = IDCommission
        tbCommissionDate.Text = CommissionDate
        tbCommissionNumber.Text = CommissionNumber.ToString.Trim
        cbPeriodes.EditValue = IDPeriode
        tbDateFrom.Text = DateFrom
        tbDateAu.Text = DateTo
        cbLivreur.EditValue = IDLivreur
        tbDescription.Text = vbNullString
        tbQuantite.EditValue = 0
        tbPrix.Text = 0
        tbTotal.Text = 0
        radChoice.SelectedIndex = -1
        chkAjout.Checked = True

        LayoutSave.Enabled = True

        bDelete.Visible = False
        DockCommission.FloatLocation = New Point(CInt((Me.Width - DockCommission.Width) / 2), CInt((Me.Height - DockCommission.Height) / 2))
        DockCommission.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Visible


    End Sub

    Private Sub bNouvelleCommission_Click(sender As Object, e As EventArgs) Handles bNouvelleCommission.Click


        Dim oPaieData As New CommissionData
        Dim oGenerationCommission As New GenerationCommissions


        IDCommission = Nothing
        CommissionNumber = Nothing
        CommissionDate = Nothing
        IDLivreur = Nothing
        Livreur = Nothing
        DateFrom = Nothing
        DateTo = Nothing
        IDPeriode = Nothing
        OrganisationID = Nothing
        Organisation = Nothing


        oCommissionLine.IDCommission = Nothing
        oCommissionLine.CommissionNumber = Nothing
        oCommissionLine.CommissionDate = Nothing
        oCommissionLine.IDLivreur = Nothing
        oCommissionLine.Livreur = Nothing
        oCommissionLine.OrganisationID = Nothing
        oCommissionLine.Organisation = Nothing
        oCommissionLine.Code = Nothing
        oCommissionLine.Km = Nothing
        oCommissionLine.Description = Nothing
        oCommissionLine.Unite = Nothing
        oCommissionLine.Prix = Nothing
        oCommissionLine.Montant = Nothing
        oCommissionLine.Credit = Nothing
        oCommissionLine.DateFrom = Nothing
        oCommissionLine.DateTo = Nothing
        oCommissionLine.NombreGlacier = Nothing
        oCommissionLine.Glacier = Nothing
        oCommissionLine.IDPeriode = Nothing

        radChoice.SelectedIndex = -1

        tbIDCommissionNP.Text = ""
        tbCommissionDateNP.Text = DateTime.Now.ToShortDateString
        tbCommissionNumberNP.Text = oGenerationCommission.generate_CommissionNumero()
        CommissionNumber = tbCommissionNumberNP.Text

        cbPeriodesNP.EditValue = go_Globals.IDPeriode
        IDPeriode = go_Globals.IDPeriode

        tbDateFromNP.Text = go_Globals.PeriodeDateFrom
        DateFrom = go_Globals.PeriodeDateFrom

        tbDateAuNP.Text = go_Globals.PeriodeDateTo
        DateTo = go_Globals.PeriodeDateFrom

        cbLivreurNP.EditValue = Nothing
        cbLivreurNP.Text = String.Empty

        tbCommissionDate.Text = tbCommissionDateNP.Text





        DockNouvelleCommission.FloatLocation = New Point(CInt((Screen.PrimaryScreen.Bounds.Width - DockNouvelleCommission.Width) / 2), CInt(Screen.PrimaryScreen.Bounds.Height - DockNouvelleCommission.Height) / 2)
        DockNouvelleCommission.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Visible


    End Sub

    Private Sub bDelete_Click(sender As Object, e As EventArgs) Handles bDelete.Click


        If PAGEVISIBLEINDEX = 0 Then
            If GridView11.FocusedRowHandle < 0 Then Exit Sub
            Dim row As System.Data.DataRow = GridView11.GetDataRow(GridView11.FocusedRowHandle)
            oCommission.IDCommission = row("IDCommission")
            oCommissionLine.IDCommission = oCommission.IDCommission

            oCommissionData.Delete(oCommission)
            oCommissionlineData.DeleteParCommission(oCommissionLine)

            grdCommission.DataSource = oCommissionData.SelectAllByDate(go_Globals.PeriodeDateFrom, go_Globals.PeriodeDateTo)
        End If


        If PAGEVISIBLEINDEX = 1 Then
            If GridView2.FocusedRowHandle < 0 Then Exit Sub
            Dim row As System.Data.DataRow = GridView2.GetDataRow(GridView2.FocusedRowHandle)
            oCommissionLine.IDCommissionLine = row("IDCommissionLine")
            oCommissionlineData.Delete(oCommissionLine)
            grdCommissionLine.DataSource = oCommissionlineData.SelectAllByID(iID)

            bDelete.Visible = False
            DockCommission.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Hidden
        End If

        bDelete.Visible = False
    End Sub

    Private Sub ToolStripButton6_Click(sender As Object, e As EventArgs) Handles ToolStripButton6.Click
        Me.Close()
    End Sub

    Private Sub grdCommissionLine_Click(sender As Object, e As EventArgs) Handles grdCommissionLine.Click

        If GridView2.FocusedRowHandle < 0 Then Exit Sub
        Dim row As System.Data.DataRow = GridView2.GetDataRow(GridView2.FocusedRowHandle)

        IDCommissionLine = row("IDCommissionLine")

        oCommissionLine.IDCommissionLine = IDCommissionLine


        LayoutSave.Enabled = False

        bDelete.Visible = True
    End Sub

    Private Sub tbPrix_EditValueChanged(sender As Object, e As EventArgs) Handles tbPrix.EditValueChanged
        If Not IsNothing(tbPrix.EditValue) And Not IsNothing(tbQuantite.EditValue) Then

            tbTotal.EditValue = tbPrix.EditValue * tbQuantite.Text

        End If
    End Sub

    Private Sub tbQuantite_EditValueChanged(sender As Object, e As EventArgs) Handles tbQuantite.EditValueChanged
        If Not IsNothing(tbPrix.EditValue) And Not IsNothing(tbQuantite.EditValue) Then

            tbTotal.EditValue = tbPrix.EditValue * tbQuantite.Text

        End If
    End Sub

    Private Sub cbOrganisation_EditValueChanged(sender As Object, e As EventArgs) Handles cbOrganisation.EditValueChanged
        Select Case radChoice.SelectedIndex
            Case 1

                lblQuantite.Text = 1
                lblQuantite.Text = "Nombre glacier"
                tbDescription.Text = "Glaciers"

                If Not IsNothing(cbOrganisation.EditValue) Then

                    Dim oOrganisationData As New OrganisationsData
                    OrganisationID = cbOrganisation.EditValue
                    dtOrganisation = oOrganisationData.SelectByID(OrganisationID)

                    If dtOrganisation.Rows.Count > 0 Then

                        For Each drOrganisation In dtOrganisation.Rows
                            tbPrix.Text = If(IsDBNull(drOrganisation("MontantGlacierLivreur")), 0, CType(drOrganisation("MontantGlacierLivreur"), Decimal?))
                            tbTotal.Text = If(IsDBNull(drOrganisation("MontantGlacierLivreur")), 0, CType(drOrganisation("MontantGlacierLivreur"), Decimal?))
                        Next

                    End If
                End If

        End Select
    End Sub
End Class