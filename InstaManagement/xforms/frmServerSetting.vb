﻿Imports DevExpress.Skins
Imports DevExpress.LookAndFeel
Imports DevExpress.UserSkins
Imports System.Threading
Imports System.Globalization

Public Class frmServerSetting
    Dim CONSTR As String

    Sub New()
        InitSkins()



        InitializeComponent()

    End Sub
    Sub InitSkins()
        DevExpress.Skins.SkinManager.EnableFormSkins()
        DevExpress.UserSkins.BonusSkins.Register()
        UserLookAndFeel.Default.SetSkinStyle("DevExpress Style")

    End Sub
    Private Sub frmServerSetting_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.LookAndFeel.ActiveLookAndFeel.SetSkinStyle("Lilian")


    End Sub

    Private Sub cmdSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSave.Click




        cnString = txtConnectionString.Text


        If txtConnectionString.Text.Trim <> "" Then

            Call writeFileStrData(txtConnectionString.Text, Application.StartupPath & "\Config.ini", , "Unicode")
        Else

            MessageBox.Show("Vous devez entrer un une chaine de connexion pour SQL Azure!")
            Exit Sub

        End If
        checkServer()
        Me.Close()
    End Sub

    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        Me.Close()
    End Sub




   
End Class