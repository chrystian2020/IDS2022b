﻿Imports DevExpress.XtraScheduler
Imports DevExpress.XtraScheduler.Localization
Imports System
Imports System.ComponentModel
Imports System.Data
Imports System.Globalization
Imports System.Threading
Imports System.Windows.Forms

Namespace dxSampleScheduler
    Partial Public Class frmCalendrier
        Inherits Form

        Public Sub New()
            InitializeComponent()

            My.Application.ChangeCulture("fr-FR")

            Threading.Thread.CurrentThread.CurrentCulture = New CultureInfo("fr-FR")
            Threading.Thread.CurrentThread.CurrentUICulture = New CultureInfo("fr-FR")

        End Sub

        Private Sub frmCalendrier_Load(sender As Object, e As EventArgs) Handles MyBase.Load
            'IDScomptabiliteDataSetBindingSource.DataSource = cnString
            'ResourcesBindingSource.DataSource = cnString

            'TODO: This line of code loads data into the 'IDScomptabiliteDataSet.Resources' table. You can move, or remove it, as needed.
            Me.ResourcesTableAdapter.Fill(Me.IDScomptabiliteDataSet.Resources)
            'TODO: This line of code loads data into the 'IDScomptabiliteDataSet.Appointments' table. You can move, or remove it, as needed.
            Me.AppointmentsTableAdapter.Fill(Me.IDScomptabiliteDataSet.Appointments)


            AddHandler Me.schedulerStorage1.AppointmentsChanged, AddressOf OnAppointmentChangedInsertedDeleted
            AddHandler Me.schedulerStorage1.AppointmentsInserted, AddressOf OnAppointmentChangedInsertedDeleted
            AddHandler Me.schedulerStorage1.AppointmentsDeleted, AddressOf OnAppointmentChangedInsertedDeleted


            schedulerControl1.Start = DateTime.Now









        End Sub

        Private Sub OnAppointmentChangedInsertedDeleted(ByVal sender As Object, ByVal e As PersistentObjectsEventArgs)
            AppointmentsTableAdapter.Update(IDScomptabiliteDataSet)
            IDScomptabiliteDataSet.AcceptChanges()
        End Sub

        Private Sub bExit_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles bExit.ItemClick
            Me.Close()

        End Sub

        Private Sub frmCalendrier_Closing(sender As Object, e As CancelEventArgs) Handles Me.Closing
            ' Create a new object, representing the German culture. 
            Dim Culture = CultureInfo.CreateSpecificCulture("en-US")

            ' The following line provides localization for the application's user interface.
            Threading.Thread.CurrentThread.CurrentUICulture = Culture

            ' The following line provides localization for data formats.
            Thread.CurrentThread.CurrentCulture = Culture

            ' Set this culture as the default culture for all threads in this application. 
            ' Note: The following properties are supported in the .NET Framework 4.5+
            CultureInfo.DefaultThreadCurrentCulture = Culture
            CultureInfo.DefaultThreadCurrentUICulture = Culture

        End Sub
    End Class
End Namespace
