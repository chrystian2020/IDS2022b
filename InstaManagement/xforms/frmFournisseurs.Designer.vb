﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmFournisseurs
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmFournisseurs))
        Me.TS = New System.Windows.Forms.ToolStrip()
        Me.Head = New System.Windows.Forms.ToolStripLabel()
        Me.bAdd = New System.Windows.Forms.ToolStripButton()
        Me.sp1 = New System.Windows.Forms.ToolStripSeparator()
        Me.bEdit = New System.Windows.Forms.ToolStripButton()
        Me.sp2 = New System.Windows.Forms.ToolStripSeparator()
        Me.bDelete = New System.Windows.Forms.ToolStripButton()
        Me.sp3 = New System.Windows.Forms.ToolStripSeparator()
        Me.bCancel = New System.Windows.Forms.ToolStripButton()
        Me.sp4 = New System.Windows.Forms.ToolStripSeparator()
        Me.bRefresh = New System.Windows.Forms.ToolStripButton()
        Me.sp5 = New System.Windows.Forms.ToolStripSeparator()
        Me.ToolStripButton6 = New System.Windows.Forms.ToolStripButton()
        Me.LayoutControl1 = New DevExpress.XtraLayout.LayoutControl()
        Me.tabData = New DevExpress.XtraTab.XtraTabControl()
        Me.XtraTabPage1 = New DevExpress.XtraTab.XtraTabPage()
        Me.LayoutControl2 = New DevExpress.XtraLayout.LayoutControl()
        Me.grdFournisseur = New DevExpress.XtraGrid.GridControl()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.IDFournisseur = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.Fournisseur = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.LayoutControlGroup1 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem7 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.XtraTabPage2 = New DevExpress.XtraTab.XtraTabPage()
        Me.LayoutControl3 = New DevExpress.XtraLayout.LayoutControl()
        Me.TLPBut = New System.Windows.Forms.TableLayoutPanel()
        Me.butApply = New System.Windows.Forms.Button()
        Me.butClose = New System.Windows.Forms.Button()
        Me.butCancel = New System.Windows.Forms.Button()
        Me.tbFournisseur = New DevExpress.XtraEditors.TextEdit()
        Me.nudIDFournisseur = New DevExpress.XtraEditors.TextEdit()
        Me.tbDescription = New DevExpress.XtraEditors.TextEdit()
        Me.LayoutControlGroup2 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem4 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem20 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem8 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem3 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.Root = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem12 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.TS.SuspendLayout()
        CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LayoutControl1.SuspendLayout()
        CType(Me.tabData, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabData.SuspendLayout()
        Me.XtraTabPage1.SuspendLayout()
        CType(Me.LayoutControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LayoutControl2.SuspendLayout()
        CType(Me.grdFournisseur, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem7, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabPage2.SuspendLayout()
        CType(Me.LayoutControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LayoutControl3.SuspendLayout()
        Me.TLPBut.SuspendLayout()
        CType(Me.tbFournisseur.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudIDFournisseur.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbDescription.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem20, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Root, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem12, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'TS
        '
        Me.TS.AutoSize = False
        Me.TS.Font = New System.Drawing.Font("Verdana", 9.75!)
        Me.TS.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Head, Me.bAdd, Me.sp1, Me.bEdit, Me.sp2, Me.bDelete, Me.sp3, Me.bCancel, Me.sp4, Me.bRefresh, Me.sp5, Me.ToolStripButton6})
        Me.TS.Location = New System.Drawing.Point(0, 0)
        Me.TS.Name = "TS"
        Me.TS.Size = New System.Drawing.Size(597, 39)
        Me.TS.TabIndex = 7
        '
        'Head
        '
        Me.Head.Name = "Head"
        Me.Head.Size = New System.Drawing.Size(0, 36)
        '
        'bAdd
        '
        Me.bAdd.Image = CType(resources.GetObject("bAdd.Image"), System.Drawing.Image)
        Me.bAdd.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.bAdd.Name = "bAdd"
        Me.bAdd.Size = New System.Drawing.Size(76, 36)
        Me.bAdd.Text = "Ajouter"
        '
        'sp1
        '
        Me.sp1.Name = "sp1"
        Me.sp1.Size = New System.Drawing.Size(6, 39)
        '
        'bEdit
        '
        Me.bEdit.Image = CType(resources.GetObject("bEdit.Image"), System.Drawing.Image)
        Me.bEdit.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.bEdit.Name = "bEdit"
        Me.bEdit.Size = New System.Drawing.Size(72, 36)
        Me.bEdit.Text = "Edition"
        '
        'sp2
        '
        Me.sp2.Name = "sp2"
        Me.sp2.Size = New System.Drawing.Size(6, 39)
        '
        'bDelete
        '
        Me.bDelete.Image = CType(resources.GetObject("bDelete.Image"), System.Drawing.Image)
        Me.bDelete.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.bDelete.Name = "bDelete"
        Me.bDelete.Size = New System.Drawing.Size(93, 36)
        Me.bDelete.Text = "Supprimer"
        '
        'sp3
        '
        Me.sp3.Name = "sp3"
        Me.sp3.Size = New System.Drawing.Size(6, 39)
        '
        'bCancel
        '
        Me.bCancel.Image = CType(resources.GetObject("bCancel.Image"), System.Drawing.Image)
        Me.bCancel.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.bCancel.Name = "bCancel"
        Me.bCancel.Size = New System.Drawing.Size(77, 36)
        Me.bCancel.Text = "Annuler"
        '
        'sp4
        '
        Me.sp4.Name = "sp4"
        Me.sp4.Size = New System.Drawing.Size(6, 39)
        '
        'bRefresh
        '
        Me.bRefresh.Image = CType(resources.GetObject("bRefresh.Image"), System.Drawing.Image)
        Me.bRefresh.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.bRefresh.Name = "bRefresh"
        Me.bRefresh.Size = New System.Drawing.Size(89, 36)
        Me.bRefresh.Text = "Rafraîchir"
        '
        'sp5
        '
        Me.sp5.Name = "sp5"
        Me.sp5.Size = New System.Drawing.Size(6, 39)
        '
        'ToolStripButton6
        '
        Me.ToolStripButton6.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.ToolStripButton6.Image = CType(resources.GetObject("ToolStripButton6.Image"), System.Drawing.Image)
        Me.ToolStripButton6.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton6.Name = "ToolStripButton6"
        Me.ToolStripButton6.Size = New System.Drawing.Size(73, 36)
        Me.ToolStripButton6.Text = "Fermer"
        '
        'LayoutControl1
        '
        Me.LayoutControl1.Controls.Add(Me.tabData)
        Me.LayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LayoutControl1.Location = New System.Drawing.Point(0, 39)
        Me.LayoutControl1.Name = "LayoutControl1"
        Me.LayoutControl1.Root = Me.Root
        Me.LayoutControl1.Size = New System.Drawing.Size(597, 408)
        Me.LayoutControl1.TabIndex = 8
        Me.LayoutControl1.Text = "LayoutControl1"
        '
        'tabData
        '
        Me.tabData.Location = New System.Drawing.Point(12, 12)
        Me.tabData.Name = "tabData"
        Me.tabData.SelectedTabPage = Me.XtraTabPage1
        Me.tabData.Size = New System.Drawing.Size(573, 384)
        Me.tabData.TabIndex = 8
        Me.tabData.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.XtraTabPage1, Me.XtraTabPage2})
        '
        'XtraTabPage1
        '
        Me.XtraTabPage1.Controls.Add(Me.LayoutControl2)
        Me.XtraTabPage1.Name = "XtraTabPage1"
        Me.XtraTabPage1.Size = New System.Drawing.Size(568, 358)
        Me.XtraTabPage1.Text = "Liste des Livreurs / vendeurs"
        '
        'LayoutControl2
        '
        Me.LayoutControl2.Controls.Add(Me.grdFournisseur)
        Me.LayoutControl2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LayoutControl2.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControl2.Name = "LayoutControl2"
        Me.LayoutControl2.Root = Me.LayoutControlGroup1
        Me.LayoutControl2.Size = New System.Drawing.Size(568, 358)
        Me.LayoutControl2.TabIndex = 1
        Me.LayoutControl2.Text = "LayoutControl2"
        '
        'grdFournisseur
        '
        Me.grdFournisseur.Location = New System.Drawing.Point(12, 12)
        Me.grdFournisseur.MainView = Me.GridView1
        Me.grdFournisseur.Name = "grdFournisseur"
        Me.grdFournisseur.Size = New System.Drawing.Size(544, 334)
        Me.grdFournisseur.TabIndex = 0
        Me.grdFournisseur.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'GridView1
        '
        Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.IDFournisseur, Me.Fournisseur})
        Me.GridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.GridView1.GridControl = Me.grdFournisseur
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsBehavior.Editable = False
        Me.GridView1.OptionsCustomization.AllowGroup = False
        Me.GridView1.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridView1.OptionsSelection.UseIndicatorForSelection = False
        Me.GridView1.OptionsView.ShowAutoFilterRow = True
        Me.GridView1.OptionsView.ShowGroupPanel = False
        Me.GridView1.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.Fournisseur, DevExpress.Data.ColumnSortOrder.Ascending)})
        '
        'IDFournisseur
        '
        Me.IDFournisseur.Caption = "IDFournisseur"
        Me.IDFournisseur.FieldName = "IDFournisseur"
        Me.IDFournisseur.Name = "IDFournisseur"
        Me.IDFournisseur.Visible = True
        Me.IDFournisseur.VisibleIndex = 0
        Me.IDFournisseur.Width = 118
        '
        'Fournisseur
        '
        Me.Fournisseur.Caption = "Fournisseur"
        Me.Fournisseur.FieldName = "Fournisseur"
        Me.Fournisseur.Name = "Fournisseur"
        Me.Fournisseur.Visible = True
        Me.Fournisseur.VisibleIndex = 1
        Me.Fournisseur.Width = 727
        '
        'LayoutControlGroup1
        '
        Me.LayoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup1.GroupBordersVisible = False
        Me.LayoutControlGroup1.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem7})
        Me.LayoutControlGroup1.Name = "Root"
        Me.LayoutControlGroup1.Size = New System.Drawing.Size(568, 358)
        Me.LayoutControlGroup1.TextVisible = False
        '
        'LayoutControlItem7
        '
        Me.LayoutControlItem7.Control = Me.grdFournisseur
        Me.LayoutControlItem7.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem7.Name = "LayoutControlItem7"
        Me.LayoutControlItem7.Size = New System.Drawing.Size(548, 338)
        Me.LayoutControlItem7.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem7.TextVisible = False
        '
        'XtraTabPage2
        '
        Me.XtraTabPage2.Controls.Add(Me.LayoutControl3)
        Me.XtraTabPage2.Name = "XtraTabPage2"
        Me.XtraTabPage2.PageVisible = False
        Me.XtraTabPage2.Size = New System.Drawing.Size(568, 358)
        Me.XtraTabPage2.Text = "Détail sur un Livreur / vendeur"
        '
        'LayoutControl3
        '
        Me.LayoutControl3.Controls.Add(Me.TLPBut)
        Me.LayoutControl3.Controls.Add(Me.tbFournisseur)
        Me.LayoutControl3.Controls.Add(Me.nudIDFournisseur)
        Me.LayoutControl3.Controls.Add(Me.tbDescription)
        Me.LayoutControl3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LayoutControl3.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControl3.Name = "LayoutControl3"
        Me.LayoutControl3.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = New System.Drawing.Rectangle(976, 223, 771, 350)
        Me.LayoutControl3.OptionsCustomizationForm.ShowPropertyGrid = True
        Me.LayoutControl3.Root = Me.LayoutControlGroup2
        Me.LayoutControl3.Size = New System.Drawing.Size(568, 358)
        Me.LayoutControl3.TabIndex = 0
        Me.LayoutControl3.Text = "LayoutControl1"
        '
        'TLPBut
        '
        Me.TLPBut.ColumnCount = 7
        Me.TLPBut.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TLPBut.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100.0!))
        Me.TLPBut.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TLPBut.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100.0!))
        Me.TLPBut.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TLPBut.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100.0!))
        Me.TLPBut.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TLPBut.Controls.Add(Me.butApply, 1, 1)
        Me.TLPBut.Controls.Add(Me.butClose, 5, 1)
        Me.TLPBut.Controls.Add(Me.butCancel, 3, 1)
        Me.TLPBut.Location = New System.Drawing.Point(12, 84)
        Me.TLPBut.Name = "TLPBut"
        Me.TLPBut.RowCount = 2
        Me.TLPBut.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26.0!))
        Me.TLPBut.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 34.0!))
        Me.TLPBut.Size = New System.Drawing.Size(544, 262)
        Me.TLPBut.TabIndex = 8
        '
        'butApply
        '
        Me.butApply.Dock = System.Windows.Forms.DockStyle.Top
        Me.butApply.Location = New System.Drawing.Point(105, 29)
        Me.butApply.Name = "butApply"
        Me.butApply.Size = New System.Drawing.Size(94, 28)
        Me.butApply.TabIndex = 20
        Me.butApply.Text = "Appliquer"
        '
        'butClose
        '
        Me.butClose.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.butClose.Dock = System.Windows.Forms.DockStyle.Top
        Me.butClose.Location = New System.Drawing.Point(345, 29)
        Me.butClose.Name = "butClose"
        Me.butClose.Size = New System.Drawing.Size(94, 28)
        Me.butClose.TabIndex = 22
        Me.butClose.Text = "Fermer"
        '
        'butCancel
        '
        Me.butCancel.Dock = System.Windows.Forms.DockStyle.Top
        Me.butCancel.Location = New System.Drawing.Point(225, 29)
        Me.butCancel.Name = "butCancel"
        Me.butCancel.Size = New System.Drawing.Size(94, 28)
        Me.butCancel.TabIndex = 21
        Me.butCancel.Text = "Retour a la grille"
        '
        'tbFournisseur
        '
        Me.tbFournisseur.Location = New System.Drawing.Point(85, 36)
        Me.tbFournisseur.Name = "tbFournisseur"
        Me.tbFournisseur.Size = New System.Drawing.Size(471, 20)
        Me.tbFournisseur.StyleController = Me.LayoutControl3
        Me.tbFournisseur.TabIndex = 7
        '
        'nudIDFournisseur
        '
        Me.nudIDFournisseur.Enabled = False
        Me.nudIDFournisseur.Location = New System.Drawing.Point(85, 12)
        Me.nudIDFournisseur.Name = "nudIDFournisseur"
        Me.nudIDFournisseur.Properties.Appearance.BackColor = System.Drawing.Color.Azure
        Me.nudIDFournisseur.Properties.Appearance.Options.UseBackColor = True
        Me.nudIDFournisseur.Size = New System.Drawing.Size(471, 20)
        Me.nudIDFournisseur.StyleController = Me.LayoutControl3
        Me.nudIDFournisseur.TabIndex = 5
        '
        'tbDescription
        '
        Me.tbDescription.Location = New System.Drawing.Point(85, 60)
        Me.tbDescription.Name = "tbDescription"
        Me.tbDescription.Size = New System.Drawing.Size(471, 20)
        Me.tbDescription.StyleController = Me.LayoutControl3
        Me.tbDescription.TabIndex = 7
        '
        'LayoutControlGroup2
        '
        Me.LayoutControlGroup2.CustomizationFormText = "LayoutControlGroup1"
        Me.LayoutControlGroup2.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup2.GroupBordersVisible = False
        Me.LayoutControlGroup2.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem4, Me.LayoutControlItem20, Me.LayoutControlItem8, Me.LayoutControlItem3})
        Me.LayoutControlGroup2.Name = "Root"
        Me.LayoutControlGroup2.Size = New System.Drawing.Size(568, 358)
        Me.LayoutControlGroup2.TextVisible = False
        '
        'LayoutControlItem4
        '
        Me.LayoutControlItem4.Control = Me.tbFournisseur
        Me.LayoutControlItem4.CustomizationFormText = "Contact #1"
        Me.LayoutControlItem4.Location = New System.Drawing.Point(0, 24)
        Me.LayoutControlItem4.Name = "LayoutControlItem4"
        Me.LayoutControlItem4.Size = New System.Drawing.Size(548, 24)
        Me.LayoutControlItem4.Text = "Fournisseur"
        Me.LayoutControlItem4.TextSize = New System.Drawing.Size(70, 13)
        '
        'LayoutControlItem20
        '
        Me.LayoutControlItem20.Control = Me.TLPBut
        Me.LayoutControlItem20.CustomizationFormText = "LayoutControlItem20"
        Me.LayoutControlItem20.Location = New System.Drawing.Point(0, 72)
        Me.LayoutControlItem20.Name = "LayoutControlItem20"
        Me.LayoutControlItem20.Size = New System.Drawing.Size(548, 266)
        Me.LayoutControlItem20.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem20.TextVisible = False
        '
        'LayoutControlItem8
        '
        Me.LayoutControlItem8.Control = Me.nudIDFournisseur
        Me.LayoutControlItem8.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem8.Name = "LayoutControlItem8"
        Me.LayoutControlItem8.Size = New System.Drawing.Size(548, 24)
        Me.LayoutControlItem8.Text = "Fournisseur ID"
        Me.LayoutControlItem8.TextSize = New System.Drawing.Size(70, 13)
        '
        'LayoutControlItem3
        '
        Me.LayoutControlItem3.Control = Me.tbDescription
        Me.LayoutControlItem3.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem3.CustomizationFormText = "Contact #1"
        Me.LayoutControlItem3.Location = New System.Drawing.Point(0, 48)
        Me.LayoutControlItem3.Name = "LayoutControlItem3"
        Me.LayoutControlItem3.Size = New System.Drawing.Size(548, 24)
        Me.LayoutControlItem3.StartNewLine = True
        Me.LayoutControlItem3.Text = "Description"
        Me.LayoutControlItem3.TextSize = New System.Drawing.Size(70, 13)
        '
        'Root
        '
        Me.Root.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.Root.GroupBordersVisible = False
        Me.Root.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem12})
        Me.Root.Name = "Root"
        Me.Root.Size = New System.Drawing.Size(597, 408)
        Me.Root.TextVisible = False
        '
        'LayoutControlItem12
        '
        Me.LayoutControlItem12.Control = Me.tabData
        Me.LayoutControlItem12.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem12.Name = "LayoutControlItem12"
        Me.LayoutControlItem12.Size = New System.Drawing.Size(577, 388)
        Me.LayoutControlItem12.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem12.TextVisible = False
        '
        'frmFournisseurs
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(597, 447)
        Me.Controls.Add(Me.LayoutControl1)
        Me.Controls.Add(Me.TS)
        Me.IconOptions.SvgImage = CType(resources.GetObject("frmFournisseurs.IconOptions.SvgImage"), DevExpress.Utils.Svg.SvgImage)
        Me.Name = "frmFournisseurs"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Gestion de Fournisseurs de services"
        Me.TS.ResumeLayout(False)
        Me.TS.PerformLayout()
        CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LayoutControl1.ResumeLayout(False)
        CType(Me.tabData, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabData.ResumeLayout(False)
        Me.XtraTabPage1.ResumeLayout(False)
        CType(Me.LayoutControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LayoutControl2.ResumeLayout(False)
        CType(Me.grdFournisseur, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem7, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabPage2.ResumeLayout(False)
        CType(Me.LayoutControl3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LayoutControl3.ResumeLayout(False)
        Me.TLPBut.ResumeLayout(False)
        CType(Me.tbFournisseur.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudIDFournisseur.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbDescription.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem20, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Root, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem12, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents TS As ToolStrip
    Friend WithEvents Head As ToolStripLabel
    Friend WithEvents bAdd As ToolStripButton
    Friend WithEvents sp1 As ToolStripSeparator
    Friend WithEvents bEdit As ToolStripButton
    Friend WithEvents sp2 As ToolStripSeparator
    Friend WithEvents bDelete As ToolStripButton
    Friend WithEvents sp3 As ToolStripSeparator
    Friend WithEvents bCancel As ToolStripButton
    Friend WithEvents sp4 As ToolStripSeparator
    Friend WithEvents bRefresh As ToolStripButton
    Friend WithEvents sp5 As ToolStripSeparator
    Friend WithEvents ToolStripButton6 As ToolStripButton
    Friend WithEvents LayoutControl1 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents Root As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents tabData As DevExpress.XtraTab.XtraTabControl
    Friend WithEvents XtraTabPage1 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents LayoutControl2 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents grdFournisseur As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents IDFournisseur As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents Fournisseur As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents LayoutControlGroup1 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem7 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents XtraTabPage2 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents LayoutControl3 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents TLPBut As TableLayoutPanel
    Friend WithEvents butApply As Button
    Friend WithEvents butClose As Button
    Friend WithEvents butCancel As Button
    Friend WithEvents tbFournisseur As DevExpress.XtraEditors.TextEdit
    Friend WithEvents nudIDFournisseur As DevExpress.XtraEditors.TextEdit
    Friend WithEvents tbDescription As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlGroup2 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem4 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem20 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem8 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem3 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem12 As DevExpress.XtraLayout.LayoutControlItem
End Class
