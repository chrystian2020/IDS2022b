﻿Imports System.Data.SqlClient
Imports System.Globalization
Imports System.Threading
Imports System.Text

Public Class frmFacture

    Dim SelectedRow As Integer
    Dim SelectedRowPaiement As Integer
    Dim oInvoiceData As New InvoiceData
    Dim oInvoiceLineData As New InvoiceLineData
    Dim oPaiementData As New PaiementsData
    Dim oImportData As New ImportData
    Dim oInvoice As New Invoice
    Dim oInvoiceLine As New InvoiceLine
    Dim dtInvoiceLine As DataTable
    Dim drInvoiceLine As DataRow
    Dim oFacturation As New Facturation
    Dim dtOrganisation As DataTable
    Dim drOrganisation As DataRow
    Private m_IDInvoice As Int32
    Private m_InvoiceDate As Nullable(Of DateTime)
    Private m_InvoiceNumber As String
    Private m_OrganisationID As Nullable(Of Int32)
    Private m_Organisation As String
    Private m_DateFrom As Nullable(Of DateTime)
    Private m_DateTo As Nullable(Of DateTime)
    Private m_IDPeriode As Nullable(Of Int32)
    Private m_Total As Nullable(Of Decimal)
    Private m_MontantPaiement As Nullable(Of Decimal)
    Private m_IDPaiement As Int32
    Dim PAGEVISIBLEINDEX As Integer

    Private m_IDInvoiceLine As Nullable(Of Int32)

    Private m_Description As String
    Private m_Unite As Nullable(Of Decimal)
    Private m_Prix As Nullable(Of Decimal)
    Private m_Montant As Nullable(Of Decimal)
    Private m_Credit As Nullable(Of Decimal)
    Dim iID As Integer
    Private m_MontantGlacierOrganisation As Nullable(Of Decimal)

    Public Property IDPaiement() As Int32
        Get
            Return m_IDPaiement
        End Get
        Set(ByVal value As Int32)
            m_IDPaiement = value
        End Set
    End Property


    Public Property MontantPaiement() As Nullable(Of Decimal)
        Get
            Return m_MontantPaiement
        End Get
        Set(ByVal value As Nullable(Of Decimal))
            m_MontantPaiement = value
        End Set
    End Property
    Public Property Total() As Nullable(Of Decimal)
        Get
            Return m_Total
        End Get
        Set(ByVal value As Nullable(Of Decimal))
            m_Total = value
        End Set
    End Property
    Public Property IDInvoiceLine() As Int32
        Get
            Return m_IDInvoiceLine
        End Get
        Set(ByVal value As Int32)
            m_IDInvoiceLine = value
        End Set
    End Property



    Public Property MontantGlacierOrganisation() As Nullable(Of Decimal)
        Get
            Return m_MontantGlacierOrganisation
        End Get
        Set(ByVal value As Nullable(Of Decimal))
            m_MontantGlacierOrganisation = value
        End Set
    End Property
    Public Property IDInvoice() As Int32
        Get
            Return m_IDInvoice
        End Get
        Set(ByVal value As Int32)
            m_IDInvoice = value
        End Set
    End Property

    Public Property InvoiceDate() As Nullable(Of DateTime)
        Get
            Return m_InvoiceDate
        End Get
        Set(ByVal value As Nullable(Of DateTime))
            m_InvoiceDate = value
        End Set
    End Property

    Public Property InvoiceNumber() As String
        Get
            Return m_InvoiceNumber
        End Get
        Set(ByVal value As String)
            m_InvoiceNumber = value
        End Set
    End Property

    Public Property OrganisationID() As Nullable(Of Int32)
        Get
            Return m_OrganisationID
        End Get
        Set(ByVal value As Nullable(Of Int32))
            m_OrganisationID = value
        End Set
    End Property

    Public Property Organisation() As String
        Get
            Return m_Organisation
        End Get
        Set(ByVal value As String)
            m_Organisation = value
        End Set
    End Property

    Public Property DateFrom() As Nullable(Of DateTime)
        Get
            Return m_DateFrom
        End Get
        Set(ByVal value As Nullable(Of DateTime))
            m_DateFrom = value
        End Set
    End Property

    Public Property DateTo() As Nullable(Of DateTime)
        Get
            Return m_DateTo
        End Get
        Set(ByVal value As Nullable(Of DateTime))
            m_DateTo = value
        End Set
    End Property

    Public Property IDPeriode() As Nullable(Of Int32)
        Get
            Return m_IDPeriode
        End Get
        Set(ByVal value As Nullable(Of Int32))
            m_IDPeriode = value
        End Set
    End Property

    Public Property Description() As String
        Get
            Return m_Description
        End Get
        Set(ByVal value As String)
            m_Description = value
        End Set
    End Property

    Public Property Unite() As Nullable(Of Decimal)
        Get
            Return m_Unite
        End Get
        Set(ByVal value As Nullable(Of Decimal))
            m_Unite = value
        End Set
    End Property

    Public Property Prix() As Nullable(Of Decimal)
        Get
            Return m_Prix
        End Get
        Set(ByVal value As Nullable(Of Decimal))
            m_Prix = value
        End Set
    End Property

    Public Property Montant() As Nullable(Of Decimal)
        Get
            Return m_Montant
        End Get
        Set(ByVal value As Nullable(Of Decimal))
            m_Montant = value
        End Set
    End Property


    Private Sub FillOrganisations()
        Dim drOrganisation As DataRow
        Dim dtOrganisation As DataTable


        Dim oOrganisationData As New OrganisationsData

        Dim table As New DataTable
        table.Columns.Add("value", Type.GetType("System.Int32"))
        table.Columns.Add("displayText", Type.GetType("System.String"))
        dtOrganisation = oOrganisationData.SelectAll()


        For Each drOrganisation In dtOrganisation.Rows
            Dim teller As Integer = 0

            teller = drOrganisation("OrganisationID")
            Dim toto As String = drOrganisation("Organisation").ToString.Trim

            table.Rows.Add(New Object() {teller, toto})
        Next

        With cbOrganisation.Properties
            .DataSource = table
            .NullText = "Selectionner un organisation"
            .DisplayMember = "displayText"
            .ValueMember = "value"
            .PopulateColumns()
            .PopupFormMinSize = New Size(50, 200)
            '.Columns("value").Visible = True
            '.Columns("displayText").Caption = "Organisation"
        End With

        With cbOrganisationI.Properties
            .DataSource = table
            .NullText = "Selectionner un organisation"
            .DisplayMember = "displayText"
            .ValueMember = "value"
            .PopulateColumns()
            .PopupFormMinSize = New Size(50, 200)
            '.Columns("value").Visible = True
            '.Columns("displayText").Caption = "Organisation"
        End With

        With cbOrganisationP.Properties
            .DataSource = table
            .NullText = "Selectionner un organisation"
            .DisplayMember = "displayText"
            .ValueMember = "value"
            .PopulateColumns()
            .PopupFormMinSize = New Size(50, 200)
            '.Columns("value").Visible = True
            '.Columns("displayText").Caption = "Organisation"
        End With


    End Sub
    Private Sub FillPeriode()

        Dim dtperiode As DataTable
        Dim drPeriode As DataRow

        Dim oPeriodeData As New PeriodesData

        Dim table As New DataTable
        table.Columns.Add("value", Type.GetType("System.Int32"))
        table.Columns.Add("displayText", Type.GetType("System.String"))
        dtperiode = oPeriodeData.SelectAllByDate(go_Globals.PeriodeDateFrom, go_Globals.PeriodeDateTo)


        For Each drPeriode In dtperiode.Rows
            Dim teller As Integer = 0

            teller = drPeriode("IDPeriode")
            Dim toto As String = drPeriode("Periode")

            table.Rows.Add(New Object() {teller, toto})
        Next



        With cbPeriodes.Properties
            .DataSource = table
            .NullText = "Selectionner une période"
            .DisplayMember = "displayText"
            .ValueMember = "value"
            .PopulateColumns()
            .PopupFormMinSize = New Size(50, 200)
            '.Columns("value").Visible = True
            '.Columns("displayText").Caption = "Périodes"
        End With

        With cbPeriodesI.Properties
            .DataSource = table
            .NullText = "Selectionner une période"
            .DisplayMember = "displayText"
            .ValueMember = "value"
            .PopulateColumns()
            .PopupFormMinSize = New Size(50, 200)
            '.Columns("value").Visible = True
            '.Columns("displayText").Caption = "Périodes"+
        End With


        With cbPeriodesP.Properties
            .DataSource = table
            .NullText = "Selectionner une période"
            .DisplayMember = "displayText"
            .ValueMember = "value"
            .PopulateColumns()
            .PopupFormMinSize = New Size(50, 200)
            '.Columns("value").Visible = True
            '.Columns("displayText").Caption = "Périodes"+
        End With

    End Sub

    Private Sub frmFacture_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        grdFactures.DataSource = oInvoiceData.SelectAllByPeriod(go_Globals.IDPeriode)
        DockLigneFacture.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Hidden
        DockNouvelleFacture.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Hidden
        DockPaiement.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Hidden
        FillOrganisations()
        FillPeriode()
        PAGEVISIBLEINDEX = 0
    End Sub

    Private Sub ToolStripButton6_Click(sender As Object, e As EventArgs) Handles ToolStripButton6.Click
        Me.Close()
    End Sub

    Private Sub grdFactures_DoubleClick(sender As Object, e As EventArgs) Handles grdFactures.DoubleClick
        Dim row As System.Data.DataRow = GridView1.GetDataRow(GridView1.FocusedRowHandle)


        InvoiceNumber = Nothing
        InvoiceDate = Nothing
        OrganisationID = Nothing
        Organisation = Nothing
        DateFrom = Nothing
        DateTo = Nothing
        IDPeriode = Nothing


        oInvoiceLine.InvoiceNumber = Nothing
        oInvoiceLine.InvoiceDate = Nothing
        oInvoiceLine.OrganisationID = Nothing
        oInvoiceLine.Organisation = Nothing
        oInvoiceLine.DateFrom = Nothing
        oInvoiceLine.DateTo = Nothing
        oInvoiceLine.IDPeriode = Nothing

        iID = row("IDInvoice")
        Dim iOrganisationID As Integer = row("OrganisationID")

        IDInvoice = row("IDInvoice")
        InvoiceNumber = row("InvoiceNumber")
        InvoiceDate = row("InvoiceDate")
        OrganisationID = iOrganisationID
        Organisation = row("Organisation")
        DateFrom = row("DateFrom")
        DateTo = row("DateTo")
        IDPeriode = row("IDPeriode")

        oInvoiceLine.IDInvoice = IDInvoice
        oInvoiceLine.InvoiceNumber = InvoiceNumber
        oInvoiceLine.InvoiceDate = InvoiceDate
        oInvoiceLine.OrganisationID = OrganisationID
        oInvoiceLine.Organisation = Organisation
        oInvoiceLine.DateFrom = DateFrom
        oInvoiceLine.DateTo = DateTo
        oInvoiceLine.IDPeriode = IDPeriode




        grdInvoiceLine.DataSource = oInvoiceLineData.SelectAllByInvoiceID(iID)
        GridView2.ClearSorting()
        GridView2.Columns("DisplayDate").SortOrder = DevExpress.Data.ColumnSortOrder.Ascending

        grdImport.DataSource = oImportData.SelectAllFromDateOrganisation(go_Globals.PeriodeDateFrom, go_Globals.PeriodeDateTo, OrganisationID)
        bCancel.Visible = True
        bAjouterLaFacture.Visible = True
        bNouvelleFacture.Visible = False
        bDelete.Visible = False
        bPaiement.Visible = False

        SelectedRow = GridView1.FocusedRowHandle
        tabData.SelectedTabPage = tabData.TabPages(1)
        tabData.TabPages(1).PageVisible = True
        tabData.TabPages(0).PageVisible = False
        PAGEVISIBLEINDEX = 1
    End Sub

    Private Sub bCancel_Click(sender As Object, e As EventArgs) Handles bCancel.Click
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        tabData.SelectedTabPage = tabData.TabPages(0)
        tabData.TabPages(1).PageVisible = False
        tabData.TabPages(0).PageVisible = True
        bCancel.Visible = False
        bNouvelleFacture.Visible = True
        bAjouterLaFacture.Visible = False
        PAGEVISIBLEINDEX = 0

        Me.Cursor = System.Windows.Forms.Cursors.Default
    End Sub


    Private Sub grdInvoiceLine_Click(sender As Object, e As EventArgs) Handles grdInvoiceLine.Click

        If GridView2.FocusedRowHandle < 0 Then Exit Sub
        Dim row As System.Data.DataRow = GridView2.GetDataRow(GridView2.FocusedRowHandle)

        IDInvoiceLine = row("IDInvoiceLine")

        oInvoiceLine.IDInvoiceLine = IDInvoiceLine
        oInvoiceLine = oInvoiceLineData.Select_Record(oInvoiceLine)

        If Not oInvoiceLine Is Nothing Then
            Try

                radChoix.SelectedIndex = If(IsNothing(oInvoiceLine.TypeAjout), Nothing, oInvoiceLine.TypeAjout.ToString.Trim)

                tbIDFactureLigneI.Text = System.Convert.ToInt32(oInvoiceLine.IDInvoiceLine)
                tbIDFactureI.Text = If(IsNothing(oInvoiceLine.IDInvoice), Nothing, oInvoiceLine.IDInvoice.ToString.Trim)
                tbDateFactureI.Text = If(IsNothing(oInvoiceLine.InvoiceDate), Nothing, oInvoiceLine.InvoiceDate.ToString.Trim)
                cbPeriodesI.EditValue = If(IsNothing(oInvoiceLine.IDPeriode), Nothing, oInvoiceLine.IDPeriode)
                tbDateFromI.Text = If(IsNothing(oInvoiceLine.DateFrom), Nothing, oInvoiceLine.DateFrom.ToString.Trim)
                tbDateAuI.Text = If(IsNothing(oInvoiceLine.DateTo), Nothing, oInvoiceLine.DateTo.ToString.Trim)
                tbInvoiceNumberI.Text = If(IsNothing(oInvoiceLine.InvoiceNumber), Nothing, oInvoiceLine.InvoiceNumber.ToString.Trim)
                cbOrganisationI.EditValue = If(IsNothing(oInvoiceLine.OrganisationID), Nothing, oInvoiceLine.OrganisationID)
                tbDescriptionI.Text = If(IsNothing(oInvoiceLine.Description), Nothing, oInvoiceLine.Description.ToString.Trim)
                tbQuantiteI.Text = If(IsNothing(oInvoiceLine.Unite), Nothing, oInvoiceLine.Unite.ToString.Trim)
                tbPrixI.Text = If(IsNothing(oInvoiceLine.Prix), Nothing, oInvoiceLine.Prix.ToString.Trim)
                tbTotalI.Text = If(IsNothing(oInvoiceLine.Montant), Nothing, oInvoiceLine.Montant.ToString.Trim)
                chkAjout.Checked = If(IsNothing(oInvoiceLine.Ajout), Nothing, oInvoiceLine.Ajout)

            Catch
            End Try
        End If


        bDelete.Visible = True
        bDelete.Enabled = True
        bUpdate.Visible = True
        LayoutSave.Enabled = False
        layoutUpdate.Enabled = True

    End Sub

    Private Function GetMonthName(d As DateTime, ci As Globalization.CultureInfo) As String

        Dim sMonth As String


        sMonth = ci.DateTimeFormat.GetMonthName(d.Month)

        Select Case sMonth
            Case "janvier"
                sMonth = "Janv"
            Case "février"
                sMonth = "Fév"
            Case "mars"
                sMonth = "Mars"
            Case "avril"
                sMonth = "Avr"
            Case "mai"
                sMonth = "Mai"
            Case "juin"
                sMonth = "Juin"
            Case "juillet"
                sMonth = "Juil"
            Case "août"
                sMonth = "Août"
            Case "septembre"
                sMonth = "Sept"
            Case "octobre"
                sMonth = "Oct"
            Case "novembre"
                sMonth = "Nov"
            Case "décembre"
                sMonth = "Déc"

        End Select

        Return sMonth
    End Function
    Private Sub bExportToExcel_Click_1(sender As Object, e As EventArgs) Handles bExportToExcel.Click
        Dim oPeriode As New Periodes
        Dim dDateFrom As Date = go_Globals.PeriodeDateFrom
        Dim dDateTo As Date = go_Globals.PeriodeDateTo
        Dim frenchCultureInfo = New Globalization.CultureInfo("fr-CA")

        Dim sFile As String = ""

        If dDateFrom.Month <> dDateTo.Month Then
            oPeriode.Periode = dDateFrom.Day & " " & GetMonthName(dDateFrom, frenchCultureInfo) & "-" & dDateTo.Day & " " & GetMonthName(dDateTo, frenchCultureInfo) & " " & dDateTo.Year.ToString
        Else
            oPeriode.Periode = dDateFrom.Day & "-" & dDateTo.Day & " " & GetMonthName(dDateTo, frenchCultureInfo) & " " & dDateTo.Year.ToString
        End If

        sFile = My.Computer.FileSystem.SpecialDirectories.Desktop & "\" & oPeriode.Periode & ".xlsx"

        grdImport.ExportToXlsx(sFile)
        System.Diagnostics.Process.Start(sFile)
    End Sub

    Private Sub bExportToPDF_Click_1(sender As Object, e As EventArgs) Handles bExportToPDF.Click
        Dim oPeriode As New Periodes
        Dim dDateFrom As Date = go_Globals.PeriodeDateFrom
        Dim dDateTo As Date = go_Globals.PeriodeDateTo
        Dim frenchCultureInfo = New Globalization.CultureInfo("fr-CA")

        Dim sFile As String = ""

        If dDateFrom.Month <> dDateTo.Month Then
            oPeriode.Periode = dDateFrom.Day & " " & GetMonthName(dDateFrom, frenchCultureInfo) & "-" & dDateTo.Day & " " & GetMonthName(dDateTo, frenchCultureInfo) & " " & dDateTo.Year.ToString
        Else
            oPeriode.Periode = dDateFrom.Day & "-" & dDateTo.Day & " " & GetMonthName(dDateTo, frenchCultureInfo) & " " & dDateTo.Year.ToString
        End If

        sFile = My.Computer.FileSystem.SpecialDirectories.Desktop & "\" & oPeriode.Periode & ".pdf"

        grdImport.ExportToPdf(sFile)
        System.Diagnostics.Process.Start(sFile)

    End Sub

    Private Sub bExportToDoc_Click_1(sender As Object, e As EventArgs) Handles bExportToDoc.Click
        Dim oPeriode As New Periodes
        Dim dDateFrom As Date = go_Globals.PeriodeDateFrom
        Dim dDateTo As Date = go_Globals.PeriodeDateTo
        Dim frenchCultureInfo = New Globalization.CultureInfo("fr-CA")

        Dim sFile As String = ""

        If dDateFrom.Month <> dDateTo.Month Then
            oPeriode.Periode = dDateFrom.Day & " " & GetMonthName(dDateFrom, frenchCultureInfo) & "-" & dDateTo.Day & " " & GetMonthName(dDateTo, frenchCultureInfo) & " " & dDateTo.Year.ToString
        Else
            oPeriode.Periode = dDateFrom.Day & "-" & dDateTo.Day & " " & GetMonthName(dDateTo, frenchCultureInfo) & " " & dDateTo.Year.ToString
        End If

        sFile = My.Computer.FileSystem.SpecialDirectories.Desktop & "\" & oPeriode.Periode & ".docx"

        grdImport.ExportToDocx(sFile)
        System.Diagnostics.Process.Start(sFile)
    End Sub

    Private Sub bExportToHTML_Click(sender As Object, e As EventArgs) Handles bExportToHTML.Click
        Dim oPeriode As New Periodes
        Dim dDateFrom As Date = go_Globals.PeriodeDateFrom
        Dim dDateTo As Date = go_Globals.PeriodeDateTo
        Dim frenchCultureInfo = New Globalization.CultureInfo("fr-CA")

        Dim sFile As String = ""

        If dDateFrom.Month <> dDateTo.Month Then
            oPeriode.Periode = dDateFrom.Day & " " & GetMonthName(dDateFrom, frenchCultureInfo) & "-" & dDateTo.Day & " " & GetMonthName(dDateTo, frenchCultureInfo) & " " & dDateTo.Year.ToString
        Else
            oPeriode.Periode = dDateFrom.Day & "-" & dDateTo.Day & " " & GetMonthName(dDateTo, frenchCultureInfo) & " " & dDateTo.Year.ToString
        End If

        sFile = My.Computer.FileSystem.SpecialDirectories.Desktop & "\" & oPeriode.Periode & ".html"

        grdImport.ExportToHtml(sFile)
        System.Diagnostics.Process.Start(sFile)
    End Sub

    Private Sub bSauvegarde_Click(sender As Object, e As EventArgs) Handles bSauvegarde.Click


        If cbPeriodes.EditValue = Nothing Then
            MsgBox("Vous devez sélectionner une période .", MsgBoxStyle.OkOnly, "Erreur")
            cbPeriodes.Focus()
            Exit Sub
        End If

        If cbOrganisation.EditValue = Nothing Then
            MsgBox("Vous devez sélectionner une Organisation .", MsgBoxStyle.OkOnly, "Erreur")
            cbOrganisation.Focus()
            Exit Sub
        End If



        Dim result As DialogResult = MessageBox.Show("Sauvegarder la fiche?", "Ajouter une nouvelle facture", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
        If result = DialogResult.No Then
            Me.Cursor = Cursors.Default
        ElseIf result = DialogResult.Yes Then
            Me.Cursor = Cursors.WaitCursor

            oInvoice.InvoiceNumber = tbInvoiceNumber.Text
            oInvoice.InvoiceDate = tbDateFacture.Text
            oInvoice.OrganisationID = cbOrganisation.EditValue
            oInvoice.Organisation = cbOrganisation.Text
            oInvoice.DateFrom = tbDateFrom.Text
            oInvoice.DateTo = tbDateAu.Text
            oInvoice.IDPeriode = cbPeriodes.EditValue
            oInvoice.IDInvoice = oInvoiceData.Add(oInvoice)

            InvoiceNumber = tbInvoiceNumber.Text
            InvoiceDate = tbDateFacture.Text
            OrganisationID = cbOrganisation.EditValue
            Organisation = cbOrganisation.Text
            DateFrom = tbDateFrom.Text
            DateTo = tbDateAu.Text
            IDPeriode = cbPeriodes.EditValue
            IDInvoice = oInvoice.IDInvoice

            oInvoiceLine.IDInvoice = IDInvoice
            oInvoiceLine.InvoiceNumber = InvoiceNumber
            oInvoiceLine.InvoiceDate = InvoiceDate
            oInvoiceLine.OrganisationID = cbOrganisation.EditValue
            oInvoiceLine.Organisation = cbOrganisation.Text
            oInvoiceLine.DateFrom = tbDateFrom.Text
            oInvoiceLine.DateTo = tbDateAu.Text
            oInvoiceLine.IDPeriode = go_Globals.IDPeriode

            tbIDFactureI.Text = IDInvoice
            tbInvoiceNumberI.Text = InvoiceNumber
            tbDateFactureI.Text = InvoiceDate
            cbPeriodesI.EditValue = IDPeriode
            tbDateFromI.Text = DateFrom
            tbDateAu.Text = DateTo
            cbOrganisationI.EditValue = OrganisationID
            tbDateFromI.Text = DateFrom
            tbDateAuI.Text = DateTo
            tbDescriptionI.Text = vbNullString
            tbQuantiteI.EditValue = 0
            tbPrixI.Text = 0
            tbTotalI.Text = 0


            DockNouvelleFacture.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Hidden

            DockLigneFacture.FloatLocation = New Point(CInt((Me.Width - DockLigneFacture.Width) / 2), CInt((Me.Height - DockLigneFacture.Height) / 2))
            DockLigneFacture.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Visible

            grdFactures.DataSource = oInvoiceData.SelectAllByDate(go_Globals.PeriodeDateFrom, go_Globals.PeriodeDateTo)
            grdInvoiceLine.DataSource = oInvoiceLineData.SelectAllByInvoiceID(IDInvoice)
            grdImport.DataSource = oImportData.SelectAllFromDateOrganisation(go_Globals.PeriodeDateFrom, go_Globals.PeriodeDateTo, 0)

            tabData.SelectedTabPage = tabData.TabPages(1)
            tabData.TabPages(1).PageVisible = True
            tabData.TabPages(0).PageVisible = False



            Me.Cursor = Cursors.Default
        End If





    End Sub

    Private Sub btnAnnuler_Click(sender As Object, e As EventArgs) Handles btnAnnuler.Click
        DockNouvelleFacture.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Hidden
    End Sub

    Private Sub tbDateFrom1_EditValueChanged(sender As Object, e As EventArgs) Handles tbDateFromI.EditValueChanged

    End Sub

    Private Sub bSauvegardeI_Click(sender As Object, e As EventArgs) Handles bSauvegardeI.Click

        Dim iNombreGlacier As Integer = 0
        Dim dblMontantGlacier As Double = 0

        If radChoix.SelectedIndex = -1 Then
            MsgBox("Vous devez sélectionner une type d'ajout .", MsgBoxStyle.OkOnly, "Erreur")
            radChoix.Focus()
            Exit Sub

        End If

        If cbPeriodesI.EditValue = Nothing Then
            MsgBox("Vous devez sélectionner une période .", MsgBoxStyle.OkOnly, "Erreur")
            cbPeriodes.Focus()
            Exit Sub
        End If

        If cbOrganisationI.EditValue = Nothing Then
            MsgBox("Vous devez sélectionner une Organisation .", MsgBoxStyle.OkOnly, "Erreur")
            cbOrganisation.Focus()
            Exit Sub
        End If
        If tbDescriptionI.Text.ToString.Trim = "" Then
            MsgBox("Vous devez entrer une description .", MsgBoxStyle.OkOnly, "Erreur")
            tbDescriptionI.Focus()
            Exit Sub
        End If


        Dim result As DialogResult = MessageBox.Show("Sauvegarder la fiche?", "Ajouter une nouvelle ligne de facture", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
        If result = DialogResult.No Then
            Me.Cursor = Cursors.Default
        ElseIf result = DialogResult.Yes Then



            Me.Cursor = Cursors.WaitCursor

            If chkAjout.Checked = True Then
                oInvoiceLine.Ajout = True
            Else
                oInvoiceLine.Ajout = False
            End If

            oInvoiceLine.IDInvoice = IDInvoice
            oInvoiceLine.InvoiceNumber = tbInvoiceNumberI.Text
            oInvoiceLine.InvoiceDate = InvoiceDate
            oInvoiceLine.DateFrom = DateFrom
            oInvoiceLine.DateTo = DateTo
            oInvoiceLine.OrganisationID = cbOrganisationI.EditValue
            oInvoiceLine.Organisation = cbOrganisationI.Text
            oInvoiceLine.IDPeriode = cbPeriodesI.EditValue
            oInvoiceLine.Description = tbDescriptionI.Text

            If radChoix.SelectedIndex = 1 Then

                iNombreGlacier = tbQuantiteI.Text
                dblMontantGlacier = Convert.ToDouble(tbPrixI.EditValue) * iNombreGlacier

                oInvoiceLine.Prix = Convert.ToDouble(dblMontantGlacier)
                oInvoiceLine.Montant = Convert.ToDouble(dblMontantGlacier)

            Else

                oInvoiceLine.Prix = Convert.ToDouble(tbPrixI.EditValue)
                oInvoiceLine.Montant = Convert.ToDouble(tbTotalI.EditValue)

            End If


            oInvoiceLine.Unite = tbQuantiteI.Text
            oInvoiceLine.Credit = 0
            oInvoiceLine.Debit = 0
            oInvoiceLine.Code = Nothing
            oInvoiceLine.DisplayDate = InvoiceDate
            oInvoiceLine.Km = 0
            oInvoiceLine.KmExtra = 0
            oInvoiceLine.NombreGlacier = tbQuantiteI.Text
            oInvoiceLine.Glacier = 0
            oInvoiceLine.TypeAjout = radChoix.SelectedIndex
            oInvoiceLine.Ajout = True
            oInvoiceLine.IDInvoiceLine = oInvoiceLineData.Add(oInvoiceLine)

            grdInvoiceLine.DataSource = oInvoiceLineData.SelectAllByInvoiceID(IDInvoice)

            tbTotalI.EditValue = 0
            tbDescriptionI.Text = vbNullString
            tbPrixI.EditValue = 0
            tbQuantiteI.EditValue = 0
            Me.Cursor = Cursors.Default

        End If

        DockLigneFacture.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Hidden
        layoutUpdate.Enabled = False
        bDelete.Visible = false



    End Sub

    Private Sub btnAnnulerI_Click(sender As Object, e As EventArgs) Handles btnAnnulerI.Click
        DockLigneFacture.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Hidden
        layoutUpdate.Enabled = False
        bDelete.Visible = False
    End Sub

    Private Sub bNouvelleFacture_Click(sender As Object, e As EventArgs) Handles bNouvelleFacture.Click

        InvoiceNumber = Nothing
        InvoiceDate = Nothing
        OrganisationID = Nothing
        Organisation = Nothing
        DateFrom = Nothing
        DateTo = Nothing
        IDPeriode = Nothing


        oInvoiceLine.InvoiceNumber = Nothing
        oInvoiceLine.InvoiceDate = Nothing
        oInvoiceLine.OrganisationID = Nothing
        oInvoiceLine.Organisation = Nothing
        oInvoiceLine.DateFrom = Nothing
        oInvoiceLine.DateTo = Nothing
        oInvoiceLine.IDPeriode = Nothing



        cbPeriodes.EditValue = Nothing
        cbPeriodes.Text = String.Empty

        InvoiceNumber = oFacturation.generate_inv()
        InvoiceDate = Now.ToShortDateString
        OrganisationID = Nothing
        Organisation = Nothing
        DateFrom = go_Globals.PeriodeDateFrom
        DateTo = go_Globals.PeriodeDateTo
        IDPeriode = go_Globals.IDPeriode
        Unite = 1

        oInvoiceLine.InvoiceNumber = InvoiceNumber
        oInvoiceLine.InvoiceDate = InvoiceDate
        oInvoiceLine.OrganisationID = OrganisationID
        oInvoiceLine.Organisation = Organisation
        oInvoiceLine.DateFrom = DateFrom
        oInvoiceLine.DateTo = DateTo
        oInvoiceLine.IDPeriode = IDPeriode
        oInvoiceLine.Unite = Unite

        tbInvoiceNumber.Text = InvoiceNumber
        cbPeriodes.EditValue = go_Globals.IDPeriode
        tbDateFrom.Text = DateFrom
        tbDateAu.Text = DateTo
        tbDateFacture.Text = InvoiceDate

        cbOrganisation.EditValue = Nothing
        cbOrganisation.Text = String.Empty

        DockNouvelleFacture.FloatLocation = New Point(CInt((Screen.PrimaryScreen.Bounds.Width - DockNouvelleFacture.Width) / 2), CInt((Screen.PrimaryScreen.Bounds.Height - DockNouvelleFacture.Height) / 2))
        DockNouvelleFacture.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Visible



    End Sub

    Private Sub bAjouterLaFacture_Click(sender As Object, e As EventArgs) Handles bAjouterLaFacture.Click


        cbPeriodesI.EditValue = Nothing
        cbPeriodesI.Text = String.Empty
        cbOrganisationI.EditValue = Nothing
        cbOrganisationI.Text = String.Empty


        tbIDFactureI.Text = IDInvoice
        tbInvoiceNumberI.Text = InvoiceNumber
        tbDateFactureI.Text = InvoiceDate
        cbPeriodesI.EditValue = IDPeriode
        tbDateFromI.Text = DateFrom
        tbDateAu.Text = DateTo
        cbOrganisationI.EditValue = OrganisationID
        tbDateFromI.Text = DateFrom
        tbDateAuI.Text = DateTo
        tbDescriptionI.Text = vbNullString
        tbQuantiteI.EditValue = 1
        tbPrixI.Text = 0
        tbTotalI.Text = 0
        radChoix.SelectedIndex = -1
        chkAjout.Checked = True
        bDelete.Visible = False
        layoutUpdate.Enabled = False
        LayoutSave.Enabled = True
        DockLigneFacture.FloatLocation = New Point(CInt((Me.Width - DockLigneFacture.Width) / 2), CInt((Me.Height - DockLigneFacture.Height) / 2))
        DockLigneFacture.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Visible


    End Sub

    Private Sub grdFactures_Click(sender As Object, e As EventArgs) Handles grdFactures.Click

        Try



            If GridView1.FocusedRowHandle < 0 Then Exit Sub

            Dim frenchCultureInfo = New Globalization.CultureInfo("fr-CA")
            Dim row As System.Data.DataRow = GridView1.GetDataRow(GridView1.FocusedRowHandle)
            SelectedRow = GridView1.FocusedRowHandle

            InvoiceNumber = Nothing
            InvoiceDate = Nothing
            OrganisationID = Nothing
            Organisation = Nothing
            DateFrom = Nothing
            DateTo = Nothing
            IDPeriode = go_Globals.IDPeriode

            iID = Convert.ToInt32(row("IDInvoice"))
            Dim iOrganisationID As Integer = Convert.ToInt32(row("OrganisationID"))

            IDInvoice = Convert.ToInt32(row("IDInvoice"))
            InvoiceNumber = row("InvoiceNumber")
            InvoiceDate = row("InvoiceDate")
            OrganisationID = iOrganisationID
            Organisation = row("Organisation")
            DateFrom = row("DateFrom")
            DateTo = row("DateTo")
            IDPeriode = Convert.ToInt32(row("IDPeriode"))

            If Not IsDBNull(row("Total")) Then
                Total = Convert.ToDouble(row("Total"))
            Else
                Total = 0
            End If


            tbIDFactureP.Text = IDInvoice
            cbPeriodesP.EditValue = IDPeriode
            tbDateFromP.Text = DateFrom
            tbDateAuP.Text = DateTo
            tbInvoiceNumberP.Text = InvoiceNumber
            cbOrganisationP.EditValue = OrganisationID
            tbMontantPaiement.Text = Total

            grdPaye.DataSource = oPaiementData.SelectAllPaiements(OrganisationID, IDPeriode)
            GridView4.ClearSorting()
            GridView4.Columns("IDPaiement").SortOrder = DevExpress.Data.ColumnSortOrder.Ascending


            bPaiement.Enabled = True
            bPaiement.Visible = True
            bDelete.Enabled = True
            bDelete.Visible = True

        Catch ex As Exception
            MessageBox.Show(ex.Message, "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try

    End Sub



    Private Sub radChoix_SelectedIndexChanged(sender As Object, e As EventArgs) Handles radChoix.SelectedIndexChanged


        Select Case radChoix.SelectedIndex
            Case 0
                lblQuantite.Text = 1
                lblQuantite.Text = "Quantité"
                tbDescriptionI.Text = vbNullString


            Case 1
                tbQuantiteI.Text = 1
                lblQuantite.Text = "Nombre glacier"
                tbDescriptionI.Text = "Glacier"

                If Not IsNothing(cbOrganisationI.EditValue) Then

                    Dim oOrganisationData As New OrganisationsData
                    OrganisationID = cbOrganisationI.EditValue
                    dtOrganisation = oOrganisationData.SelectByID(OrganisationID)

                    If dtOrganisation.Rows.Count > 0 Then

                        For Each drOrganisation In dtOrganisation.Rows
                            tbPrixI.Text = If(IsDBNull(drOrganisation("MontantGlacierOrganisation")), 0, CType(drOrganisation("MontantGlacierOrganisation"), Decimal?))
                            tbTotalI.Text = If(IsDBNull(drOrganisation("MontantGlacierOrganisation")), 0, CType(drOrganisation("MontantGlacierOrganisation"), Decimal?))
                        Next

                    End If


                End If

            Case 2
                tbQuantiteI.Text = 1
                lblQuantite.Text = "Quantité"
                tbDescriptionI.Text = "Crédit"
            Case 3
                tbQuantiteI.Text = 1
                lblQuantite.Text = "Quantité"
                tbDescriptionI.Text = "Débit"

            Case 4
                tbQuantiteI.Text = 1
                lblQuantite.Text = "Quantité"
                tbDescriptionI.Text = "Extras"


        End Select


    End Sub

    Private Sub cbOrganisationI_EditValueChanged(sender As Object, e As EventArgs) Handles cbOrganisationI.EditValueChanged

    End Sub

    Private Sub bDelete_Click(sender As Object, e As EventArgs) Handles bDelete.Click

        If PAGEVISIBLEINDEX = 0 Then
            If GridView1.FocusedRowHandle < 0 Then Exit Sub
            Dim row As System.Data.DataRow = GridView1.GetDataRow(GridView1.FocusedRowHandle)
            oInvoice.IDInvoice = row("IDInvoice")
            oInvoiceLine.IDInvoice = oInvoice.IDInvoice



            oInvoiceData.Delete(oInvoice)
            oInvoiceLineData.DeleteParInvoice(oInvoiceLine)
            grdFactures.DataSource = oInvoiceData.SelectAllByDate(go_Globals.PeriodeDateFrom, go_Globals.PeriodeDateTo)
        End If


        If PAGEVISIBLEINDEX = 1 Then
            If GridView2.FocusedRowHandle < 0 Then Exit Sub
            Dim row As System.Data.DataRow = GridView2.GetDataRow(GridView2.FocusedRowHandle)
            oInvoiceLine.IDInvoiceLine = row("IDInvoiceLine")
            oInvoiceLineData.Delete(oInvoiceLine)
            grdInvoiceLine.DataSource = oInvoiceLineData.SelectAllByInvoiceID(IDInvoice)
            DockLigneFacture.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Hidden
        End If

        bDelete.Visible = False


    End Sub

    Private Sub tbMontantTotal_KeyPress(sender As Object, e As KeyPressEventArgs)
        If e.KeyChar = "." Or e.KeyChar = "," Or e.KeyChar = Convert.ToChar(Keys.Back) Then
            e.Handled = e.KeyChar = ","
        Else
            e.Handled = Not (Char.IsDigit(e.KeyChar))
        End If
    End Sub

    Private Sub tbMontant_KeyPress(sender As Object, e As KeyPressEventArgs)
        If e.KeyChar = "." Or e.KeyChar = "," Or e.KeyChar = Convert.ToChar(Keys.Back) Then
            e.Handled = e.KeyChar = ","
        Else
            e.Handled = Not (Char.IsDigit(e.KeyChar))
        End If
    End Sub

    Private Sub tbPrixI_EditValueChanged(sender As Object, e As EventArgs) Handles tbPrixI.EditValueChanged
        If Not IsNothing(tbPrixI.EditValue) And Not IsNothing(tbQuantiteI.EditValue) Then
            tbTotalI.EditValue = tbPrixI.EditValue * tbQuantiteI.Text
        End If
    End Sub

    Private Sub bUpdate_Click(sender As Object, e As EventArgs) Handles bUpdate.Click

        Dim iNombreGlacier As Integer = 0
        Dim dblMontantGlacier As Double = 0


        If radChoix.SelectedIndex = -1 Then
            MsgBox("Vous devez sélectionner une type d'ajout .", MsgBoxStyle.OkOnly, "Erreur")
            radChoix.Focus()
            Exit Sub

        End If

        If cbPeriodesI.EditValue = Nothing Then
            MsgBox("Vous devez sélectionner une période .", MsgBoxStyle.OkOnly, "Erreur")
            cbPeriodes.Focus()
            Exit Sub
        End If

        If cbOrganisationI.EditValue = Nothing Then
            MsgBox("Vous devez sélectionner une Organisation .", MsgBoxStyle.OkOnly, "Erreur")
            cbOrganisation.Focus()
            Exit Sub
        End If
        If tbDescriptionI.Text.ToString.Trim = "" Then
            MsgBox("Vous devez entrer une description .", MsgBoxStyle.OkOnly, "Erreur")
            tbDescriptionI.Focus()
            Exit Sub
        End If


        Dim result As DialogResult = MessageBox.Show("Sauvegarder la fiche?", "Mise a jour", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
        If result = DialogResult.No Then
            Me.Cursor = Cursors.Default
        ElseIf result = DialogResult.Yes Then



            Me.Cursor = Cursors.WaitCursor

            If tbIDFactureLigneI.Text > 0 Then
                oInvoiceLine.IDInvoiceLine = tbIDFactureLigneI.Text
            Else
                MessageBox.Show("Vous devez sélectionner une ligne sur la grille")
                Exit Sub
            End If


            If chkAjout.Checked = True Then
                oInvoiceLine.Ajout = True
            Else
                oInvoiceLine.Ajout = False
            End If


            oInvoiceLine.IDInvoice = IDInvoice
            oInvoiceLine.InvoiceNumber = tbInvoiceNumberI.Text
            oInvoiceLine.InvoiceDate = InvoiceDate
            oInvoiceLine.DateFrom = DateFrom
            oInvoiceLine.DateTo = DateTo
            oInvoiceLine.OrganisationID = cbOrganisationI.EditValue
            oInvoiceLine.Organisation = cbOrganisationI.Text
            oInvoiceLine.IDPeriode = cbPeriodesI.EditValue
            oInvoiceLine.Description = tbDescriptionI.Text

            If radChoix.SelectedIndex = 1 Then

                iNombreGlacier = tbQuantiteI.Text
                dblMontantGlacier = Convert.ToDouble(tbPrixI.EditValue) * iNombreGlacier

                oInvoiceLine.Prix = Convert.ToDouble(dblMontantGlacier)
                oInvoiceLine.Montant = Convert.ToDouble(dblMontantGlacier)

            Else

                oInvoiceLine.Prix = Convert.ToDouble(tbPrixI.EditValue)
                oInvoiceLine.Montant = Convert.ToDouble(tbTotalI.EditValue)

            End If


            oInvoiceLine.Unite = tbQuantiteI.Text
            oInvoiceLine.Credit = 0
            oInvoiceLine.Debit = 0
            oInvoiceLine.Code = Nothing
            oInvoiceLine.DisplayDate = InvoiceDate
            oInvoiceLine.Km = 0
            oInvoiceLine.KmExtra = 0
            oInvoiceLine.NombreGlacier = tbQuantiteI.Text
            oInvoiceLine.Glacier = 0
            oInvoiceLine.TypeAjout = radChoix.SelectedIndex
            oInvoiceLine.Ajout = True
            oInvoiceLine.IDInvoiceLine = oInvoiceLineData.Update(oInvoiceLine, oInvoiceLine)


            grdInvoiceLine.DataSource = oInvoiceLineData.SelectAllByInvoiceID(IDInvoice)

            tbTotalI.EditValue = 0
            tbDescriptionI.Text = vbNullString
            tbPrixI.EditValue = 0
            tbQuantiteI.EditValue = 0
            Me.Cursor = Cursors.Default

        End If


        layoutUpdate.Enabled = False

        bDelete.Visible = False


        DockLigneFacture.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Hidden



    End Sub

    Private Sub bPaiement_Click(sender As Object, e As EventArgs) Handles bPaiement.Click

        DockPaiement.FloatLocation = New Point(CInt((Me.Width - DockLigneFacture.Width) / 2), CInt((Me.Height - DockLigneFacture.Height) / 2))
        DockPaiement.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Visible
        DockPaiement.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Visible

    End Sub

    Private Sub grdPaye_Click(sender As Object, e As EventArgs) Handles grdPaye.Click
        If GridView4.FocusedRowHandle < 0 Then Exit Sub
        Dim row As System.Data.DataRow = GridView4.GetDataRow(GridView4.FocusedRowHandle)

        IDPaiement = row("IDPaiement")
        MontantPaiement = row("Montant")

        bDeletePaiement.Enabled = True


    End Sub

    Private Sub bDeletePaiement_Click(sender As Object, e As EventArgs) Handles bDeletePaiement.Click

        Dim row As System.Data.DataRow = GridView4.GetDataRow(GridView4.FocusedRowHandle)
        Dim opaiement As New Paiements
        Dim oInvoice As New Invoice
        Dim dblMontant As Double = 0
        opaiement.IDPaiement = row("IDPaiement")
        dblMontant = row("Montant")

        Dim row2 As System.Data.DataRow = GridView1.GetDataRow(GridView1.FocusedRowHandle)
        Dim result As DialogResult = MessageBox.Show("Supprimer ce paiement?", "Supprimer un paiement", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
        If result = DialogResult.No Then
            Me.Cursor = Cursors.Default
            Exit Sub
        ElseIf result = DialogResult.Yes Then

            Me.Cursor = Cursors.WaitCursor
        End If


        oInvoice.IDInvoice = row2("IDInvoice")
        oInvoice.Total = row2("Total")
        oInvoice.Paye = False

        oInvoice.Total = oInvoice.Total + dblMontant

        oInvoiceData.UpdateTotal(oInvoice, oInvoice)
        oPaiementData.Delete(opaiement)

        grdFactures.DataSource = oInvoiceData.SelectAllByPeriod(go_Globals.IDPeriode)
        grdPaye.DataSource = oPaiementData.SelectAllPaiements(OrganisationID, IDPeriode)

        GridView1.FocusedRowHandle = SelectedRow
        bDeletePaiement.Enabled = False
        Me.Cursor = Cursors.Default

    End Sub

    Private Sub bFermerPaiement_Click(sender As Object, e As EventArgs) Handles bFermerPaiement.Click
        DockPaiement.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Hidden
    End Sub

    Private Sub bSauvegarderPaiement_Click(sender As Object, e As EventArgs) Handles bSauvegarderPaiement.Click
        Dim oPaiement As New Paiements
        Dim oInvoice As New Invoice

        If cbPeriodesP.EditValue = Nothing Then
            MsgBox("Vous devez sélectionner une période .", MsgBoxStyle.OkOnly, "Erreur")
            cbPeriodes.Focus()
            Exit Sub
        End If

        If cbOrganisationP.EditValue = Nothing Then
            MsgBox("Vous devez sélectionner une Organisation .", MsgBoxStyle.OkOnly, "Erreur")
            cbOrganisation.Focus()
            Exit Sub
        End If


        If tbMontantPaiement.Text = 0 Then
            MsgBox("Vous devez enter un  montant de paiement.", MsgBoxStyle.OkOnly, "Erreur")
            tbMontantPaiement.Focus()
            Exit Sub
        End If

        Dim dblMontant As Double = tbMontantPaiement.Text

        If dblMontant > Total Then
            MsgBox("Vous devez enter un  montant de paiement égale ou inférieur au montant de facture.", MsgBoxStyle.OkOnly, "Erreur")
            tbMontantPaiement.Focus()
            Exit Sub

        End If



        Dim result As DialogResult = MessageBox.Show("Appliquer un paiement a cette facture?", "Appliquer un paiement", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
        If result = DialogResult.No Then
            Me.Cursor = Cursors.Default
            Exit Sub
        ElseIf result = DialogResult.Yes Then


        End If


        Me.Cursor = Cursors.WaitCursor

        oPaiement.IDInvoice = tbIDFactureP.Text
        oPaiement.IDPeriode = cbPeriodesP.EditValue
        oPaiement.InvoiceNumber = tbInvoiceNumberP.Text
        oPaiement.OrganisationID = cbOrganisationP.EditValue
        oPaiement.Organisation = cbOrganisationP.Text
        oPaiement.DatePaiement = Now.ToShortDateString
        oPaiement.Montant = dblMontant
        oPaiementData.Add(oPaiement)

        If dblMontant = Total Then
            oInvoice.IDInvoice = oPaiement.IDInvoice
            oInvoice.Total = Total - dblMontant
            oInvoice.Paye = True
            oInvoiceData.UpdateTotal(oInvoice, oInvoice)

        Else oInvoice.IDInvoice = oPaiement.IDInvoice
            oInvoice.Total = Total - dblMontant
            oInvoiceData.UpdateTotal(oInvoice, oInvoice)
            oInvoice.Paye = False
        End If

        grdFactures.DataSource = oInvoiceData.SelectAllByPeriod(go_Globals.IDPeriode)
        grdPaye.DataSource = oPaiementData.SelectAllPaiements(OrganisationID, IDPeriode)

        GridView1.FocusedRowHandle = SelectedRow
        bPaiement.Enabled = False
        bPaiement.Visible = False
        bDelete.Visible = False
        DockPaiement.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Hidden
        Me.Cursor = Cursors.Default
    End Sub

    Private Sub tbTotalI_EditValueChanged(sender As Object, e As EventArgs) Handles tbTotalI.EditValueChanged

    End Sub

    Private Sub tbQuantiteI_EditValueChanged(sender As Object, e As EventArgs) Handles tbQuantiteI.EditValueChanged
        If Not IsNothing(tbPrixI.EditValue) And Not IsNothing(tbQuantiteI.EditValue) Then

            tbTotalI.EditValue = tbPrixI.EditValue * tbQuantiteI.Text

        End If
    End Sub
End Class