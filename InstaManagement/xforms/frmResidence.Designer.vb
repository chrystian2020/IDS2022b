﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmResidence
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmResidence))
        Me.TS = New System.Windows.Forms.ToolStrip()
        Me.Head = New System.Windows.Forms.ToolStripLabel()
        Me.bAdd = New System.Windows.Forms.ToolStripButton()
        Me.sp1 = New System.Windows.Forms.ToolStripSeparator()
        Me.bEdit = New System.Windows.Forms.ToolStripButton()
        Me.sp2 = New System.Windows.Forms.ToolStripSeparator()
        Me.bDelete = New System.Windows.Forms.ToolStripButton()
        Me.sp3 = New System.Windows.Forms.ToolStripSeparator()
        Me.bCancel = New System.Windows.Forms.ToolStripButton()
        Me.sp4 = New System.Windows.Forms.ToolStripSeparator()
        Me.bRefresh = New System.Windows.Forms.ToolStripButton()
        Me.sp5 = New System.Windows.Forms.ToolStripSeparator()
        Me.ToolStripButton6 = New System.Windows.Forms.ToolStripButton()
        Me.tabData = New DevExpress.XtraTab.XtraTabControl()
        Me.XtraTabPage1 = New DevExpress.XtraTab.XtraTabPage()
        Me.grdResidence = New DevExpress.XtraGrid.GridControl()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colIDResidence = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colResidence = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.XtraTabPage2 = New DevExpress.XtraTab.XtraTabPage()
        Me.LayoutControl1 = New DevExpress.XtraLayout.LayoutControl()
        Me.tbResidence = New DevExpress.XtraEditors.TextEdit()
        Me.TLPBut = New System.Windows.Forms.TableLayoutPanel()
        Me.butApply = New System.Windows.Forms.Button()
        Me.butClose = New System.Windows.Forms.Button()
        Me.butCancel = New System.Windows.Forms.Button()
        Me.nudIDResidence = New DevExpress.XtraEditors.TextEdit()
        Me.LayoutControlGroup1 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem20 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem8 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem1 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.EmptySpaceItem2 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlItem4 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.TS.SuspendLayout()
        CType(Me.tabData, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabData.SuspendLayout()
        Me.XtraTabPage1.SuspendLayout()
        CType(Me.grdResidence, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabPage2.SuspendLayout()
        CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LayoutControl1.SuspendLayout()
        CType(Me.tbResidence.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TLPBut.SuspendLayout()
        CType(Me.nudIDResidence.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem20, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'TS
        '
        Me.TS.AutoSize = False
        Me.TS.Font = New System.Drawing.Font("Verdana", 9.75!)
        Me.TS.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Head, Me.bAdd, Me.sp1, Me.bEdit, Me.sp2, Me.bDelete, Me.sp3, Me.bCancel, Me.sp4, Me.bRefresh, Me.sp5, Me.ToolStripButton6})
        Me.TS.Location = New System.Drawing.Point(0, 0)
        Me.TS.Name = "TS"
        Me.TS.Size = New System.Drawing.Size(448, 39)
        Me.TS.TabIndex = 8
        '
        'Head
        '
        Me.Head.Name = "Head"
        Me.Head.Size = New System.Drawing.Size(0, 36)
        '
        'bAdd
        '
        Me.bAdd.Image = CType(resources.GetObject("bAdd.Image"), System.Drawing.Image)
        Me.bAdd.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.bAdd.Name = "bAdd"
        Me.bAdd.Size = New System.Drawing.Size(76, 36)
        Me.bAdd.Text = "Ajouter"
        '
        'sp1
        '
        Me.sp1.Name = "sp1"
        Me.sp1.Size = New System.Drawing.Size(6, 39)
        '
        'bEdit
        '
        Me.bEdit.Image = CType(resources.GetObject("bEdit.Image"), System.Drawing.Image)
        Me.bEdit.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.bEdit.Name = "bEdit"
        Me.bEdit.Size = New System.Drawing.Size(72, 36)
        Me.bEdit.Text = "Edition"
        '
        'sp2
        '
        Me.sp2.Name = "sp2"
        Me.sp2.Size = New System.Drawing.Size(6, 39)
        '
        'bDelete
        '
        Me.bDelete.Image = CType(resources.GetObject("bDelete.Image"), System.Drawing.Image)
        Me.bDelete.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.bDelete.Name = "bDelete"
        Me.bDelete.Size = New System.Drawing.Size(93, 36)
        Me.bDelete.Text = "Supprimer"
        '
        'sp3
        '
        Me.sp3.Name = "sp3"
        Me.sp3.Size = New System.Drawing.Size(6, 39)
        '
        'bCancel
        '
        Me.bCancel.Image = CType(resources.GetObject("bCancel.Image"), System.Drawing.Image)
        Me.bCancel.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.bCancel.Name = "bCancel"
        Me.bCancel.Size = New System.Drawing.Size(77, 36)
        Me.bCancel.Text = "Annuler"
        '
        'sp4
        '
        Me.sp4.Name = "sp4"
        Me.sp4.Size = New System.Drawing.Size(6, 39)
        '
        'bRefresh
        '
        Me.bRefresh.Image = CType(resources.GetObject("bRefresh.Image"), System.Drawing.Image)
        Me.bRefresh.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.bRefresh.Name = "bRefresh"
        Me.bRefresh.Size = New System.Drawing.Size(89, 20)
        Me.bRefresh.Text = "Rafraîchir"
        '
        'sp5
        '
        Me.sp5.Name = "sp5"
        Me.sp5.Size = New System.Drawing.Size(6, 39)
        '
        'ToolStripButton6
        '
        Me.ToolStripButton6.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.ToolStripButton6.Image = CType(resources.GetObject("ToolStripButton6.Image"), System.Drawing.Image)
        Me.ToolStripButton6.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton6.Name = "ToolStripButton6"
        Me.ToolStripButton6.Size = New System.Drawing.Size(73, 20)
        Me.ToolStripButton6.Text = "Fermer"
        '
        'tabData
        '
        Me.tabData.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tabData.Location = New System.Drawing.Point(0, 39)
        Me.tabData.Name = "tabData"
        Me.tabData.SelectedTabPage = Me.XtraTabPage1
        Me.tabData.Size = New System.Drawing.Size(448, 386)
        Me.tabData.TabIndex = 9
        Me.tabData.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.XtraTabPage1, Me.XtraTabPage2})
        '
        'XtraTabPage1
        '
        Me.XtraTabPage1.Controls.Add(Me.grdResidence)
        Me.XtraTabPage1.Name = "XtraTabPage1"
        Me.XtraTabPage1.Size = New System.Drawing.Size(443, 360)
        Me.XtraTabPage1.Text = "Liste des Résidences"
        '
        'grdResidence
        '
        Me.grdResidence.Dock = System.Windows.Forms.DockStyle.Fill
        Me.grdResidence.Location = New System.Drawing.Point(0, 0)
        Me.grdResidence.MainView = Me.GridView1
        Me.grdResidence.Name = "grdResidence"
        Me.grdResidence.Size = New System.Drawing.Size(443, 360)
        Me.grdResidence.TabIndex = 0
        Me.grdResidence.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'GridView1
        '
        Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colIDResidence, Me.colResidence})
        Me.GridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.GridView1.GridControl = Me.grdResidence
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsBehavior.Editable = False
        Me.GridView1.OptionsCustomization.AllowGroup = False
        Me.GridView1.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridView1.OptionsSelection.UseIndicatorForSelection = False
        Me.GridView1.OptionsView.ShowAutoFilterRow = True
        Me.GridView1.OptionsView.ShowGroupPanel = False
        '
        'colIDResidence
        '
        Me.colIDResidence.Caption = "ID Residence"
        Me.colIDResidence.FieldName = "IDResidence"
        Me.colIDResidence.Name = "colIDResidence"
        Me.colIDResidence.Visible = True
        Me.colIDResidence.VisibleIndex = 0
        Me.colIDResidence.Width = 97
        '
        'colResidence
        '
        Me.colResidence.Caption = "Residence"
        Me.colResidence.FieldName = "Residence"
        Me.colResidence.Name = "colResidence"
        Me.colResidence.Visible = True
        Me.colResidence.VisibleIndex = 1
        Me.colResidence.Width = 565
        '
        'XtraTabPage2
        '
        Me.XtraTabPage2.Controls.Add(Me.LayoutControl1)
        Me.XtraTabPage2.Name = "XtraTabPage2"
        Me.XtraTabPage2.PageVisible = False
        Me.XtraTabPage2.Size = New System.Drawing.Size(405, 360)
        Me.XtraTabPage2.Text = "Détail sur une Résidence"
        '
        'LayoutControl1
        '
        Me.LayoutControl1.Controls.Add(Me.tbResidence)
        Me.LayoutControl1.Controls.Add(Me.TLPBut)
        Me.LayoutControl1.Controls.Add(Me.nudIDResidence)
        Me.LayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LayoutControl1.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControl1.Name = "LayoutControl1"
        Me.LayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = New System.Drawing.Rectangle(976, 223, 771, 350)
        Me.LayoutControl1.OptionsCustomizationForm.ShowPropertyGrid = True
        Me.LayoutControl1.Root = Me.LayoutControlGroup1
        Me.LayoutControl1.Size = New System.Drawing.Size(405, 360)
        Me.LayoutControl1.TabIndex = 0
        Me.LayoutControl1.Text = "LayoutControl1"
        '
        'tbResidence
        '
        Me.tbResidence.Location = New System.Drawing.Point(79, 36)
        Me.tbResidence.Name = "tbResidence"
        Me.tbResidence.Size = New System.Drawing.Size(128, 20)
        Me.tbResidence.StyleController = Me.LayoutControl1
        Me.tbResidence.TabIndex = 134
        '
        'TLPBut
        '
        Me.TLPBut.ColumnCount = 7
        Me.TLPBut.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TLPBut.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100.0!))
        Me.TLPBut.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TLPBut.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100.0!))
        Me.TLPBut.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TLPBut.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100.0!))
        Me.TLPBut.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TLPBut.Controls.Add(Me.butApply, 1, 1)
        Me.TLPBut.Controls.Add(Me.butClose, 5, 1)
        Me.TLPBut.Controls.Add(Me.butCancel, 3, 1)
        Me.TLPBut.Location = New System.Drawing.Point(12, 60)
        Me.TLPBut.Name = "TLPBut"
        Me.TLPBut.RowCount = 2
        Me.TLPBut.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26.0!))
        Me.TLPBut.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 34.0!))
        Me.TLPBut.Size = New System.Drawing.Size(381, 278)
        Me.TLPBut.TabIndex = 8
        '
        'butApply
        '
        Me.butApply.Dock = System.Windows.Forms.DockStyle.Top
        Me.butApply.Location = New System.Drawing.Point(23, 29)
        Me.butApply.Name = "butApply"
        Me.butApply.Size = New System.Drawing.Size(94, 28)
        Me.butApply.TabIndex = 20
        Me.butApply.Text = "Appliquer"
        '
        'butClose
        '
        Me.butClose.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.butClose.Dock = System.Windows.Forms.DockStyle.Top
        Me.butClose.Location = New System.Drawing.Point(263, 29)
        Me.butClose.Name = "butClose"
        Me.butClose.Size = New System.Drawing.Size(94, 28)
        Me.butClose.TabIndex = 22
        Me.butClose.Text = "Fermer"
        '
        'butCancel
        '
        Me.butCancel.Dock = System.Windows.Forms.DockStyle.Top
        Me.butCancel.Location = New System.Drawing.Point(143, 29)
        Me.butCancel.Name = "butCancel"
        Me.butCancel.Size = New System.Drawing.Size(94, 28)
        Me.butCancel.TabIndex = 21
        Me.butCancel.Text = "Retour a la grille"
        '
        'nudIDResidence
        '
        Me.nudIDResidence.Enabled = False
        Me.nudIDResidence.Location = New System.Drawing.Point(79, 12)
        Me.nudIDResidence.Name = "nudIDResidence"
        Me.nudIDResidence.Properties.Appearance.BackColor = System.Drawing.Color.Azure
        Me.nudIDResidence.Properties.Appearance.Options.UseBackColor = True
        Me.nudIDResidence.Size = New System.Drawing.Size(128, 20)
        Me.nudIDResidence.StyleController = Me.LayoutControl1
        Me.nudIDResidence.TabIndex = 5
        '
        'LayoutControlGroup1
        '
        Me.LayoutControlGroup1.CustomizationFormText = "LayoutControlGroup1"
        Me.LayoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup1.GroupBordersVisible = False
        Me.LayoutControlGroup1.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem20, Me.LayoutControlItem8, Me.EmptySpaceItem1, Me.EmptySpaceItem2, Me.LayoutControlItem4})
        Me.LayoutControlGroup1.Name = "Root"
        Me.LayoutControlGroup1.Size = New System.Drawing.Size(405, 360)
        Me.LayoutControlGroup1.TextVisible = False
        '
        'LayoutControlItem20
        '
        Me.LayoutControlItem20.Control = Me.TLPBut
        Me.LayoutControlItem20.CustomizationFormText = "LayoutControlItem20"
        Me.LayoutControlItem20.Location = New System.Drawing.Point(0, 48)
        Me.LayoutControlItem20.Name = "LayoutControlItem20"
        Me.LayoutControlItem20.Size = New System.Drawing.Size(385, 282)
        Me.LayoutControlItem20.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem20.TextVisible = False
        '
        'LayoutControlItem8
        '
        Me.LayoutControlItem8.Control = Me.nudIDResidence
        Me.LayoutControlItem8.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem8.Name = "LayoutControlItem8"
        Me.LayoutControlItem8.Size = New System.Drawing.Size(199, 24)
        Me.LayoutControlItem8.Text = "ID Résidence"
        Me.LayoutControlItem8.TextSize = New System.Drawing.Size(63, 13)
        '
        'EmptySpaceItem1
        '
        Me.EmptySpaceItem1.AllowHotTrack = False
        Me.EmptySpaceItem1.Location = New System.Drawing.Point(0, 330)
        Me.EmptySpaceItem1.Name = "EmptySpaceItem1"
        Me.EmptySpaceItem1.Size = New System.Drawing.Size(385, 10)
        Me.EmptySpaceItem1.TextSize = New System.Drawing.Size(0, 0)
        '
        'EmptySpaceItem2
        '
        Me.EmptySpaceItem2.AllowHotTrack = False
        Me.EmptySpaceItem2.Location = New System.Drawing.Point(199, 0)
        Me.EmptySpaceItem2.Name = "EmptySpaceItem2"
        Me.EmptySpaceItem2.Size = New System.Drawing.Size(186, 48)
        Me.EmptySpaceItem2.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlItem4
        '
        Me.LayoutControlItem4.Control = Me.tbResidence
        Me.LayoutControlItem4.Location = New System.Drawing.Point(0, 24)
        Me.LayoutControlItem4.Name = "LayoutControlItem4"
        Me.LayoutControlItem4.Size = New System.Drawing.Size(199, 24)
        Me.LayoutControlItem4.Text = "Résidence"
        Me.LayoutControlItem4.TextSize = New System.Drawing.Size(63, 13)
        '
        'frmResidence
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(448, 425)
        Me.Controls.Add(Me.tabData)
        Me.Controls.Add(Me.TS)
        Me.IconOptions.Image = CType(resources.GetObject("frmResidence.IconOptions.Image"), System.Drawing.Image)
        Me.Name = "frmResidence"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Gestion des Résidence"
        Me.TS.ResumeLayout(False)
        Me.TS.PerformLayout()
        CType(Me.tabData, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabData.ResumeLayout(False)
        Me.XtraTabPage1.ResumeLayout(False)
        CType(Me.grdResidence, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabPage2.ResumeLayout(False)
        CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LayoutControl1.ResumeLayout(False)
        CType(Me.tbResidence.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TLPBut.ResumeLayout(False)
        CType(Me.nudIDResidence.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem20, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents TS As ToolStrip
    Friend WithEvents Head As ToolStripLabel
    Friend WithEvents bAdd As ToolStripButton
    Friend WithEvents sp1 As ToolStripSeparator
    Friend WithEvents bEdit As ToolStripButton
    Friend WithEvents sp2 As ToolStripSeparator
    Friend WithEvents bDelete As ToolStripButton
    Friend WithEvents sp3 As ToolStripSeparator
    Friend WithEvents bCancel As ToolStripButton
    Friend WithEvents sp4 As ToolStripSeparator
    Friend WithEvents bRefresh As ToolStripButton
    Friend WithEvents sp5 As ToolStripSeparator
    Friend WithEvents ToolStripButton6 As ToolStripButton
    Friend WithEvents tabData As DevExpress.XtraTab.XtraTabControl
    Friend WithEvents XtraTabPage1 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents grdResidence As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colIDResidence As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colResidence As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents XtraTabPage2 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents LayoutControl1 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents tbResidence As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TLPBut As TableLayoutPanel
    Friend WithEvents butApply As Button
    Friend WithEvents butClose As Button
    Friend WithEvents butCancel As Button
    Friend WithEvents nudIDResidence As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlGroup1 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem20 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem8 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem1 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents EmptySpaceItem2 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents LayoutControlItem4 As DevExpress.XtraLayout.LayoutControlItem
End Class
