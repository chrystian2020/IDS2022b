﻿Public Class frmPaye

    Dim oPayedata As New PayeData
    Dim oPaye As New Paye
    Dim oPayeLineData As New PayeLineData
    Dim oPayeLine As New PayeLine
    Dim oImportData As New ImportData
    Dim SelectedRow As Integer
    Dim iID As Integer
    Dim oPaie As New Paye
    Dim oPaieData As New PayeData
    Dim oGenerationPaye As New GenerationPaye
    Dim dtOrganisation As DataTable
    Dim drOrganisation As DataRow
    Dim dtPayeLine As DataTable
    Dim drPayeLine As DataRow
    Dim PAGEVISIBLEINDEX As Integer



    Private m_IDPaye As Int32
    Private m_PayeNumber As String
    Private m_PayeDate As Nullable(Of DateTime)
    Private m_IDLivreur As Nullable(Of Int32)
    Private m_Livreur As String
    Private m_DateFrom As Nullable(Of DateTime)
    Private m_DateTo As Nullable(Of DateTime)
    Private m_IDPeriode As Nullable(Of Int32)
    Private m_OrganisationID As Nullable(Of Int32)
    Private m_Organisation As String
    Private m_IDPayeLine As Int32
    Public Property IDPayeLine() As Int32
        Get
            Return m_IDPayeLine
        End Get
        Set(ByVal value As Int32)
            m_IDPayeLine = value
        End Set
    End Property
    Public Property IDPaye() As Int32
        Get
            Return m_IDPaye
        End Get
        Set(ByVal value As Int32)
            m_IDPaye = value
        End Set
    End Property

    Public Property PayeNumber() As String
        Get
            Return m_PayeNumber
        End Get
        Set(ByVal value As String)
            m_PayeNumber = value
        End Set
    End Property

    Public Property PayeDate() As Nullable(Of DateTime)
        Get
            Return m_PayeDate
        End Get
        Set(ByVal value As Nullable(Of DateTime))
            m_PayeDate = value
        End Set
    End Property

    Public Property IDLivreur() As Nullable(Of Int32)
        Get
            Return m_IDLivreur
        End Get
        Set(ByVal value As Nullable(Of Int32))
            m_IDLivreur = value
        End Set
    End Property

    Public Property Livreur() As String
        Get
            Return m_Livreur
        End Get
        Set(ByVal value As String)
            m_Livreur = value
        End Set
    End Property

    Public Property DateFrom() As Nullable(Of DateTime)
        Get
            Return m_DateFrom
        End Get
        Set(ByVal value As Nullable(Of DateTime))
            m_DateFrom = value
        End Set
    End Property

    Public Property DateTo() As Nullable(Of DateTime)
        Get
            Return m_DateTo
        End Get
        Set(ByVal value As Nullable(Of DateTime))
            m_DateTo = value
        End Set
    End Property

    Public Property IDPeriode() As Nullable(Of Int32)
        Get
            Return m_IDPeriode
        End Get
        Set(ByVal value As Nullable(Of Int32))
            m_IDPeriode = value
        End Set
    End Property



    Public Property OrganisationID() As Nullable(Of Int32)
        Get
            Return m_OrganisationID
        End Get
        Set(ByVal value As Nullable(Of Int32))
            m_OrganisationID = value
        End Set
    End Property

    Public Property Organisation() As String
        Get
            Return m_Organisation
        End Get
        Set(ByVal value As String)
            m_Organisation = value
        End Set
    End Property

    Private Sub FillLivreur()
        Dim drLivreur As DataRow
        Dim dtLivreur As DataTable

        Dim oLivreurData As New LivreursData

        Dim table As New DataTable
        table.Columns.Add("value", Type.GetType("System.Int32"))
        table.Columns.Add("displayText", Type.GetType("System.String"))
        dtLivreur = oLivreurData.SelectAll()


        For Each drLivreur In dtLivreur.Rows
            Dim teller As Integer = 0

            teller = drLivreur("LivreurID")
            Dim toto As String = drLivreur("Livreur").ToString.Trim

            table.Rows.Add(New Object() {teller, toto})
        Next

        With cbLivreur.Properties
            .DataSource = table
            .NullText = "Selectionner un livreur"
            .DisplayMember = "displayText"
            .ValueMember = "value"
            .PopulateColumns()
            .PopupFormMinSize = New Size(50, 200)
            '.Columns("value").Visible = True
            '.Columns("displayText").Caption = "Livreur"

        End With


        With cbLivreurNP.Properties
            .DataSource = table
            .NullText = "Selectionner un livreur"
            .DisplayMember = "displayText"
            .ValueMember = "value"
            .PopulateColumns()
            .PopupFormMinSize = New Size(50, 200)
            '.Columns("value").Visible = True
            '.Columns("displayText").Caption = "Livreur"

        End With

    End Sub

    Private Sub FillOrganisations()
        Dim drOrganisation As DataRow
        Dim dtOrganisation As DataTable


        Dim oOrganisationData As New OrganisationsData

        Dim table As New DataTable
        table.Columns.Add("value", Type.GetType("System.Int32"))
        table.Columns.Add("displayText", Type.GetType("System.String"))
        dtOrganisation = oOrganisationData.SelectAll()


        For Each drOrganisation In dtOrganisation.Rows
            Dim teller As Integer = 0

            teller = drOrganisation("OrganisationID")
            Dim toto As String = drOrganisation("Organisation").ToString.Trim

            table.Rows.Add(New Object() {teller, toto})
        Next

        With cbOrganisation.Properties
            .DataSource = table
            .NullText = "Selectionner un organisation"
            .DisplayMember = "displayText"
            .ValueMember = "value"
            .PopulateColumns()
            .PopupFormMinSize = New Size(50, 200)
            '.Columns("value").Visible = True
            '.Columns("displayText").Caption = "Organisation"
        End With

    End Sub
    Private Sub FillPeriode()

        Dim dtperiode As DataTable
        Dim drPeriode As DataRow

        Dim oPeriodeData As New PeriodesData

        Dim table As New DataTable
        table.Columns.Add("value", Type.GetType("System.Int32"))
        table.Columns.Add("displayText", Type.GetType("System.String"))
        dtperiode = oPeriodeData.SelectAllPeriode(go_Globals.IDPeriode)


        For Each drPeriode In dtperiode.Rows
            Dim teller As Integer = 0

            teller = drPeriode("IDPeriode")
            Dim toto As String = drPeriode("Periode")

            table.Rows.Add(New Object() {teller, toto})
        Next



        With cbPeriodes.Properties
            .DataSource = table
            .NullText = "Selectionner une période"
            .DisplayMember = "displayText"
            .ValueMember = "value"
            .PopulateColumns()
            .PopupFormMinSize = New Size(50, 200)
            '.Columns("value").Visible = True
            '.Columns("displayText").Caption = "Périodes"
        End With

        With cbPeriodesNP.Properties
            .DataSource = table
            .NullText = "Selectionner une période"
            .DisplayMember = "displayText"
            .ValueMember = "value"
            .PopulateColumns()
            .PopupFormMinSize = New Size(50, 200)
            '.Columns("value").Visible = True
            '.Columns("displayText").Caption = "Périodes"
        End With

    End Sub

    Private Sub frmPaye_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        FillOrganisations()
        FillLivreur()
        FillPeriode()
        grdPaye.DataSource = oPayedata.SelectAllByDate(go_Globals.PeriodeDateFrom, go_Globals.PeriodeDateTo)

        radChoice.SelectedIndex = -1
        PAGEVISIBLEINDEX = 0


        DockPaye.FloatLocation = New Point(CInt((Screen.PrimaryScreen.Bounds.Width - DockPaye.Width) / 2), CInt(Screen.PrimaryScreen.Bounds.Height - DockPaye.Height) / 2)
        DockNouvellePaye.FloatLocation = New Point(CInt((Screen.PrimaryScreen.Bounds.Width - DockNouvellePaye.Width) / 2), CInt(Screen.PrimaryScreen.Bounds.Height - DockNouvellePaye.Height) / 2)
        DockPaye.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Hidden
        DockNouvellePaye.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Hidden

    End Sub

    Private Sub ToolStripButton6_Click(sender As Object, e As EventArgs) Handles ToolStripButton6.Click
        Me.Close()
    End Sub

    Private Sub grdPaye_DoubleClick(sender As Object, e As EventArgs) Handles grdPaye.DoubleClick


        If GridView1.FocusedRowHandle < 0 Then Exit Sub

        Dim row As System.Data.DataRow = GridView1.GetDataRow(GridView1.FocusedRowHandle)

        iID = row("IDPaye")

        IDPaye = Nothing
        PayeNumber = Nothing
        PayeDate = Nothing
        IDLivreur = Nothing
        Livreur = Nothing
        DateFrom = Nothing
        DateTo = Nothing
        IDPeriode = Nothing
        OrganisationID = Nothing
        Organisation = Nothing


        oPayeLine.IDPaye = Nothing
        oPayeLine.PayeNumber = Nothing
        oPayeLine.PayeDate = Nothing
        oPayeLine.IDLivreur = Nothing
        oPayeLine.Livreur = Nothing
        oPayeLine.OrganisationID = Nothing
        oPayeLine.Organisation = Nothing
        oPayeLine.Code = Nothing
        oPayeLine.Km = Nothing
        oPayeLine.Description = Nothing
        oPayeLine.Unite = Nothing
        oPayeLine.Prix = Nothing
        oPayeLine.Montant = Nothing
        oPayeLine.Credit = Nothing
        oPayeLine.DateFrom = Nothing
        oPayeLine.DateTo = Nothing
        oPayeLine.NombreGlacier = Nothing
        oPayeLine.Glacier = Nothing
        oPayeLine.IDPeriode = Nothing

        IDPaye = iID
        DateFrom = row("DateFrom")
        DateTo = row("DateTo")
        PayeDate = row("PayeDate")
        IDLivreur = row("IDLivreur")
        Livreur = row("Livreur")
        PayeNumber = row("PayeNumber")
        IDPeriode = go_Globals.IDPeriode

        grdPayeLine.DataSource = oPayeLineData.SelectAllByID(iID)
        grdImport.DataSource = oImportData.SelectAllFromLivreur(IDLivreur)

        bCancel.Visible = True
        bNouvellePaye.Visible = False
        bAjouterLaPaye.Visible = True
        bDelete.Visible = False

        DockPaye.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Hidden
        SelectedRow = GridView1.FocusedRowHandle
        tabData.SelectedTabPage = tabData.TabPages(1)
        tabData.TabPages(1).PageVisible = True
        tabData.TabPages(0).PageVisible = False
        PAGEVISIBLEINDEX = 1

    End Sub

    Private Sub bCancel_Click(sender As Object, e As EventArgs) Handles bCancel.Click
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

        tabData.SelectedTabPage = tabData.TabPages(0)
        tabData.TabPages(1).PageVisible = False
        tabData.TabPages(0).PageVisible = True

        bCancel.Visible = False
        bNouvellePaye.Visible = True
        bAjouterLaPaye.Visible = False
        PAGEVISIBLEINDEX = 0

        Me.Cursor = System.Windows.Forms.Cursors.Default
    End Sub



    Private Sub grdPayeLine_Click(sender As Object, e As EventArgs) Handles grdPayeLine.Click

        If GridView2.FocusedRowHandle < 0 Then Exit Sub
        Dim row As System.Data.DataRow = GridView2.GetDataRow(GridView2.FocusedRowHandle)

        IDPayeLine = row("IDPayeLine")

        oPayeLine.IDPayeLine = IDPayeLine


        LayoutSave.Enabled = False

        bDelete.Visible = True


    End Sub

    Private Function GetMonthName(d As DateTime, ci As Globalization.CultureInfo) As String

        Dim sMonth As String


        sMonth = ci.DateTimeFormat.GetMonthName(d.Month)

        Select Case sMonth
            Case "janvier"
                sMonth = "Janv"
            Case "février"
                sMonth = "Fév"
            Case "mars"
                sMonth = "Mars"
            Case "avril"
                sMonth = "Avr"
            Case "mai"
                sMonth = "Mai"
            Case "juin"
                sMonth = "Juin"
            Case "juillet"
                sMonth = "Juil"
            Case "août"
                sMonth = "Août"
            Case "septembre"
                sMonth = "Sept"
            Case "octobre"
                sMonth = "Oct"
            Case "novembre"
                sMonth = "Nov"
            Case "décembre"
                sMonth = "Déc"

        End Select

        Return sMonth
    End Function
    Private Sub bExportToExcel_Click(sender As Object, e As EventArgs) Handles bExportToExcel.Click
        Dim oPeriode As New Periodes
        Dim dDateFrom As Date = go_Globals.PeriodeDateFrom
        Dim dDateTo As Date = go_Globals.PeriodeDateTo
        Dim frenchCultureInfo = New Globalization.CultureInfo("fr-CA")

        Dim sFile As String = ""

        If dDateFrom.Month <> dDateTo.Month Then
            oPeriode.Periode = dDateFrom.Day & " " & GetMonthName(dDateFrom, frenchCultureInfo) & "-" & dDateTo.Day & " " & GetMonthName(dDateTo, frenchCultureInfo) & " " & dDateTo.Year.ToString
        Else
            oPeriode.Periode = dDateFrom.Day & "-" & dDateTo.Day & " " & GetMonthName(dDateTo, frenchCultureInfo) & " " & dDateTo.Year.ToString
        End If

        sFile = My.Computer.FileSystem.SpecialDirectories.Desktop & "\" & oPeriode.Periode & ".xlsx"

        grdImport.ExportToXlsx(sFile)
        System.Diagnostics.Process.Start(sFile)
    End Sub

    Private Sub bExportToPDF_Click(sender As Object, e As EventArgs) Handles bExportToPDF.Click
        Dim oPeriode As New Periodes
        Dim dDateFrom As Date = go_Globals.PeriodeDateFrom
        Dim dDateTo As Date = go_Globals.PeriodeDateTo
        Dim frenchCultureInfo = New Globalization.CultureInfo("fr-CA")

        Dim sFile As String = ""

        If dDateFrom.Month <> dDateTo.Month Then
            oPeriode.Periode = dDateFrom.Day & " " & GetMonthName(dDateFrom, frenchCultureInfo) & "-" & dDateTo.Day & " " & GetMonthName(dDateTo, frenchCultureInfo) & " " & dDateTo.Year.ToString
        Else
            oPeriode.Periode = dDateFrom.Day & "-" & dDateTo.Day & " " & GetMonthName(dDateTo, frenchCultureInfo) & " " & dDateTo.Year.ToString
        End If

        sFile = My.Computer.FileSystem.SpecialDirectories.Desktop & "\" & oPeriode.Periode & ".pdf"

        grdImport.ExportToPdf(sFile)
        System.Diagnostics.Process.Start(sFile)
    End Sub

    Private Sub bExportToDoc_Click(sender As Object, e As EventArgs) Handles bExportToDoc.Click
        Dim oPeriode As New Periodes
        Dim dDateFrom As Date = go_Globals.PeriodeDateFrom
        Dim dDateTo As Date = go_Globals.PeriodeDateTo
        Dim frenchCultureInfo = New Globalization.CultureInfo("fr-CA")

        Dim sFile As String = ""

        If dDateFrom.Month <> dDateTo.Month Then
            oPeriode.Periode = dDateFrom.Day & " " & GetMonthName(dDateFrom, frenchCultureInfo) & "-" & dDateTo.Day & " " & GetMonthName(dDateTo, frenchCultureInfo) & " " & dDateTo.Year.ToString
        Else
            oPeriode.Periode = dDateFrom.Day & "-" & dDateTo.Day & " " & GetMonthName(dDateTo, frenchCultureInfo) & " " & dDateTo.Year.ToString
        End If

        sFile = My.Computer.FileSystem.SpecialDirectories.Desktop & "\" & oPeriode.Periode & ".docx"

        grdImport.ExportToDocx(sFile)
        System.Diagnostics.Process.Start(sFile)
    End Sub

    Private Sub bExportToHTML_Click(sender As Object, e As EventArgs) Handles bExportToHTML.Click
        Dim oPeriode As New Periodes
        Dim dDateFrom As Date = go_Globals.PeriodeDateFrom
        Dim dDateTo As Date = go_Globals.PeriodeDateTo
        Dim frenchCultureInfo = New Globalization.CultureInfo("fr-CA")

        Dim sFile As String = ""

        If dDateFrom.Month <> dDateTo.Month Then
            oPeriode.Periode = dDateFrom.Day & " " & GetMonthName(dDateFrom, frenchCultureInfo) & "-" & dDateTo.Day & " " & GetMonthName(dDateTo, frenchCultureInfo) & " " & dDateTo.Year.ToString
        Else
            oPeriode.Periode = dDateFrom.Day & "-" & dDateTo.Day & " " & GetMonthName(dDateTo, frenchCultureInfo) & " " & dDateTo.Year.ToString
        End If

        sFile = My.Computer.FileSystem.SpecialDirectories.Desktop & "\" & oPeriode.Periode & ".html"

        grdImport.ExportToHtml(sFile)
        System.Diagnostics.Process.Start(sFile)
    End Sub

    Private Sub bAjouterLaPaye_Click(sender As Object, e As EventArgs) Handles bAjouterLaPaye.Click

        tbIDPaye.Text = IDPaye
        tbPayeDate.Text = PayeDate
        tbPayeNumber.Text = PayeNumber.ToString.Trim
        cbPeriodes.EditValue = IDPeriode
        tbDateFrom.Text = DateFrom
        tbDateAu.Text = DateTo
        cbLivreur.EditValue = IDLivreur
        tbDescription.Text = vbNullString
        tbQuantite.EditValue = 0
        tbPrix.Text = 0
        tbTotal.Text = 0
        radChoice.SelectedIndex = -1
        chkAjout.Checked = True

        LayoutSave.Enabled = True

        bDelete.Visible = False

        DockPaye.FloatLocation = New Point(CInt((Screen.PrimaryScreen.Bounds.Width - DockPaye.Width) / 2), CInt(Screen.PrimaryScreen.Bounds.Height - DockPaye.Height) / 2)
        DockNouvellePaye.FloatLocation = New Point(CInt((Screen.PrimaryScreen.Bounds.Width - DockNouvellePaye.Width) / 2), CInt(Screen.PrimaryScreen.Bounds.Height - DockNouvellePaye.Height) / 2)

        DockPaye.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Visible

        DockNouvellePaye.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Hidden


    End Sub

    Private Sub btnAnnuler_Click(sender As Object, e As EventArgs) Handles btnAnnuler.Click

        DockPaye.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Hidden

    End Sub

    Private Sub bSauvegarde_Click(sender As Object, e As EventArgs) Handles bSauvegarde.Click



        If radChoice.SelectedIndex = -1 Then
            MsgBox("Vous devez sélectionner une type d'ajout .", MsgBoxStyle.OkOnly, "Erreur")
            radChoice.Focus()
            Exit Sub
        End If

        If cbPeriodes.EditValue = Nothing Then
            MsgBox("Vous devez sélectionner une période .", MsgBoxStyle.OkOnly, "Erreur")
            cbPeriodes.Focus()
            Exit Sub
        End If

        If cbLivreur.EditValue = Nothing Then
            MsgBox("Vous devez sélectionner une Livreur.", MsgBoxStyle.OkOnly, "Erreur")
            cbLivreur.Focus()
            Exit Sub
        End If


        If cbOrganisation.EditValue = Nothing Then
            MsgBox("Vous devez sélectionner une Organisation .", MsgBoxStyle.OkOnly, "Erreur")
            cbOrganisation.Focus()
            Exit Sub
        End If

        If tbDescription.Text.ToString.Trim = "" Then
            MsgBox("Vous devez entrer une description .", MsgBoxStyle.OkOnly, "Erreur")
            tbDescription.Focus()
            Exit Sub
        End If


        Dim result As DialogResult = MessageBox.Show("Sauvegarder la fiche?", "Ajouter une ligne a la paie", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
        If result = DialogResult.No Then
            Me.Cursor = Cursors.Default
        ElseIf result = DialogResult.Yes Then


ADDGLACIER:

                oPayeLine.IDPaye = tbIDPaye.Text
                oPayeLine.PayeNumber = tbPayeNumber.Text
                oPayeLine.PayeDate = Now.ToShortDateString
                oPayeLine.IDLivreur = cbLivreur.EditValue
                oPayeLine.Livreur = cbLivreur.Text
                oPayeLine.OrganisationID = cbOrganisation.EditValue
                oPayeLine.Organisation = cbOrganisation.Text
                oPayeLine.Code = Nothing
                oPayeLine.Km = 0
                oPayeLine.Description = tbDescription.Text
                oPayeLine.Unite = tbQuantite.Text


            If chkAjout.Checked = True Then
                oPayeLine.Ajout = True
            Else
                oPayeLine.Ajout = False
            End If


            oPayeLine.Prix = Convert.ToDouble(tbPrix.EditValue)
            oPayeLine.Montant = Convert.ToDouble(tbTotal.EditValue)

            oPayeLine.Credit = 0
            oPayeLine.DateFrom = tbDateFrom.Text
            oPayeLine.DateTo = tbDateAu.Text
            oPayeLine.NombreGlacier = 0
            oPayeLine.Glacier = 0
            oPayeLine.TypeAjout = radChoice.SelectedIndex
            oPayeLine.Ajout = True
            oPayeLine.IDPeriode = go_Globals.IDPeriode

                oPayeLineData.Add(oPayeLine)
            End If

            grdPayeLine.DataSource = oPayeLineData.SelectAllByID(iID)



            bDelete.Visible = False
        DockPaye.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Hidden

    End Sub

    Private Sub tbPayeLivreur1_EditValueChanged(sender As Object, e As EventArgs) Handles tbTotal.EditValueChanged

    End Sub

    Private Sub grdPaye_Click(sender As Object, e As EventArgs) Handles grdPaye.Click
        bDelete.Visible = True
    End Sub

    Private Sub bSauvegarderNouvellePaye_Click(sender As Object, e As EventArgs) Handles bSauvegarderNouvellePaye.Click

        If cbPeriodesNP.EditValue = Nothing Then
            MsgBox("Vous devez sélectionner une période .", MsgBoxStyle.OkOnly, "Erreur")
            cbPeriodesNP.Focus()
            Exit Sub
        End If

        If cbLivreurNP.EditValue = Nothing Then
            MsgBox("Vous devez sélectionner une Livreur.", MsgBoxStyle.OkOnly, "Erreur")
            cbLivreurNP.Focus()
            Exit Sub
        End If

        IDPaye = Nothing
        DateFrom = Nothing
        DateTo = Nothing
        PayeDate = Nothing
        IDLivreur = Nothing
        Livreur = Nothing
        IDPeriode = Nothing
        OrganisationID = Nothing
        Organisation = Nothing


        Dim result As DialogResult = MessageBox.Show("Sauvegarder la fiche?", "Ajouter une nouvelle paye ? ", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
        If result = DialogResult.No Then
            Me.Cursor = Cursors.Default
        ElseIf result = DialogResult.Yes Then
            Me.Cursor = Cursors.WaitCursor

            oPaie.PayeNumber = tbPayeNumberNP.Text
            oPaie.PayeDate = Now.ToShortDateString
            oPaie.IDLivreur = cbLivreurNP.EditValue
            oPaie.Livreur = cbLivreurNP.Text
            oPaie.DateFrom = tbDateFromNP.Text
            oPaie.DateTo = tbDateAuNP.Text
            oPaie.IDPeriode = go_Globals.IDPeriode
            iID = oPaieData.Add(oPaie)



            IDPaye = iID
            PayeDate = oPaie.PayeDate
            PayeNumber = oPaie.PayeNumber
            DateFrom = oPaie.DateFrom
            DateTo = oPaie.DateTo
            IDLivreur = oPaie.IDLivreur
            Livreur = oPaie.Livreur
            IDPeriode = oPaie.IDPeriode


            tbIDPaye.Text = iID
            tbPayeNumber.Text = oPaie.PayeNumber
            cbPeriodes.EditValue = go_Globals.IDPeriode
            tbDateFrom.EditValue = go_Globals.PeriodeDateFrom
            tbDateAu.EditValue = go_Globals.PeriodeDateTo
            cbLivreur.EditValue = oPaie.IDLivreur


            grdPaye.DataSource = oPayedata.SelectAllByDate(go_Globals.PeriodeDateFrom, go_Globals.PeriodeDateTo)
            grdPayeLine.DataSource = oPayeLineData.SelectAllByID(iID)

            tabData.SelectedTabPage = tabData.TabPages(1)
            tabData.TabPages(1).PageVisible = True
            tabData.TabPages(0).PageVisible = False
            bCancel.Visible = True
            bNouvellePaye.Visible = False
            bAjouterLaPaye.Visible = True

            DockNouvellePaye.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Hidden
            DockPaye.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Visible

            Me.Cursor = Cursors.Default

        End If

    End Sub

    Private Sub bNouvellePaye_Click(sender As Object, e As EventArgs) Handles bNouvellePaye.Click



        Dim oPaieData As New PayeData
        Dim oGenerationPaye As New GenerationPaye


        IDPaye = Nothing
        PayeNumber = Nothing
        PayeDate = Nothing
        IDLivreur = Nothing
        Livreur = Nothing
        DateFrom = Nothing
        DateTo = Nothing
        IDPeriode = Nothing
        OrganisationID = Nothing
        Organisation = Nothing


        oPayeLine.IDPaye = Nothing
        oPayeLine.PayeNumber = Nothing
        oPayeLine.PayeDate = Nothing
        oPayeLine.IDLivreur = Nothing
        oPayeLine.Livreur = Nothing
        oPayeLine.OrganisationID = Nothing
        oPayeLine.Organisation = Nothing
        oPayeLine.Code = Nothing
        oPayeLine.Km = Nothing
        oPayeLine.Description = Nothing
        oPayeLine.Unite = Nothing
        oPayeLine.Prix = Nothing
        oPayeLine.Montant = Nothing
        oPayeLine.Credit = Nothing
        oPayeLine.DateFrom = Nothing
        oPayeLine.DateTo = Nothing
        oPayeLine.NombreGlacier = Nothing
        oPayeLine.Glacier = Nothing
        oPayeLine.IDPeriode = Nothing

        radChoice.SelectedIndex = -1

        tbIDPayeNP.Text = ""
        tbPayeNumberNP.Text = oGenerationPaye.generate_PayeNumero()
        PayeNumber = tbPayeNumberNP.Text

        cbPeriodesNP.EditValue = go_Globals.IDPeriode
        IDPeriode = go_Globals.IDPeriode

        tbDateFromNP.Text = go_Globals.PeriodeDateFrom
        DateFrom = go_Globals.PeriodeDateFrom

        tbDateAuNP.Text = go_Globals.PeriodeDateTo
        DateTo = go_Globals.PeriodeDateFrom

        cbLivreurNP.EditValue = Nothing
        cbLivreurNP.Text = String.Empty

        DockNouvellePaye.FloatLocation = New Point(CInt((Screen.PrimaryScreen.Bounds.Width - DockNouvellePaye.Width) / 2), CInt((Screen.PrimaryScreen.Bounds.Height - DockNouvellePaye.Height) / 2))
        DockNouvellePaye.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Visible



    End Sub

    Private Sub bAnnulerNouvellePaye_Click(sender As Object, e As EventArgs) Handles bAnnulerNouvellePaye.Click
        DockNouvellePaye.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Hidden
    End Sub

    Private Sub radChoice_SelectedIndexChanged(sender As Object, e As EventArgs) Handles radChoice.SelectedIndexChanged

        Select Case radChoice.SelectedIndex
            Case 0
                tbQuantite.Text = 1
                lblQuantite.Text = "Quantité"
                tbDescription.Text = vbNullString
            Case 1
                tbQuantite.Text = 1
                lblQuantite.Text = "Nombre glacier"
                tbDescription.Text = "Glaciers"


                If Not IsNothing(cbOrganisation.EditValue) Then

                    Dim oOrganisationData As New OrganisationsData
                    OrganisationID = cbOrganisation.EditValue
                    dtOrganisation = oOrganisationData.SelectByID(OrganisationID)

                    If dtOrganisation.Rows.Count > 0 Then

                        For Each drOrganisation In dtOrganisation.Rows
                            tbPrix.Text = If(IsDBNull(drOrganisation("MontantGlacierLivreur")), 0, CType(drOrganisation("MontantGlacierLivreur"), Decimal?))
                            tbTotal.Text = If(IsDBNull(drOrganisation("MontantGlacierLivreur")), 0, CType(drOrganisation("MontantGlacierLivreur"), Decimal?))
                        Next

                    End If
                End If

            Case 2
                tbQuantite.Text = 1
                lblQuantite.Text = "Quantité"
                tbDescription.Text = "Crédit"
            Case 3
                tbQuantite.Text = 1
                lblQuantite.Text = "Quantité"
                tbDescription.Text = "Débit"

            Case 4
                tbQuantite.Text = 1
                lblQuantite.Text = "Quantité"
                tbDescription.Text = "Extras"


        End Select

    End Sub

    Private Sub cbOrganisation_EditValueChanged(sender As Object, e As EventArgs) Handles cbOrganisation.EditValueChanged

        Select Case radChoice.SelectedIndex
            Case 1

                lblQuantite.Text = 1
                lblQuantite.Text = "Nombre glacier"
                tbDescription.Text = "Glaciers"

                If Not IsNothing(cbOrganisation.EditValue) Then

                    Dim oOrganisationData As New OrganisationsData
                    OrganisationID = cbOrganisation.EditValue
                    dtOrganisation = oOrganisationData.SelectByID(OrganisationID)

                    If dtOrganisation.Rows.Count > 0 Then

                        For Each drOrganisation In dtOrganisation.Rows
                            tbPrix.Text = If(IsDBNull(drOrganisation("MontantGlacierLivreur")), 0, CType(drOrganisation("MontantGlacierLivreur"), Decimal?))
                            tbTotal.Text = If(IsDBNull(drOrganisation("MontantGlacierLivreur")), 0, CType(drOrganisation("MontantGlacierLivreur"), Decimal?))
                        Next

                    End If
                End If

        End Select


    End Sub

    Private Sub grdImport_Click(sender As Object, e As EventArgs) Handles grdImport.Click

    End Sub

    Private Sub bDelete_Click(sender As Object, e As EventArgs) Handles bDelete.Click

        If PAGEVISIBLEINDEX = 0 Then
            If GridView1.FocusedRowHandle < 0 Then Exit Sub
            Dim row As System.Data.DataRow = GridView1.GetDataRow(GridView1.FocusedRowHandle)
            oPaye.IDPaye = row("IDPaye")
            oPayeLine.IDPaye = oPaye.IDPaye
            oPayedata.Delete(oPaye)
            oPayeLineData.DeleteParPaye(oPayeLine)
            grdPaye.DataSource = oPayedata.SelectAllByDate(go_Globals.PeriodeDateFrom, go_Globals.PeriodeDateTo)
        End If


        If PAGEVISIBLEINDEX = 1 Then
            If GridView2.FocusedRowHandle < 0 Then Exit Sub
            Dim row As System.Data.DataRow = GridView2.GetDataRow(GridView2.FocusedRowHandle)
            oPayeLine.IDPayeLine = row("IDPayeLine")
            oPayeLineData.Delete(oPayeLine)
            grdPayeLine.DataSource = oPayeLineData.SelectAllByID(iID)

            bDelete.Visible = False
            DockPaye.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Hidden
        End If

        bDelete.Visible = False
    End Sub

    Private Sub tbPrix_EditValueChanged(sender As Object, e As EventArgs) Handles tbPrix.EditValueChanged

        If Not IsNothing(tbPrix.EditValue) And Not IsNothing(tbQuantite.EditValue) Then

            tbTotal.EditValue = tbPrix.EditValue * tbQuantite.Text

        End If

    End Sub

    Private Sub bUpdate_Click(sender As Object, e As EventArgs)

        Dim iNombreGlacier As Integer
        Dim dblMontantGlacier As Double
        Dim oPayeLine2 As New PayeLine

        If radChoice.SelectedIndex = -1 Then
            MsgBox("Vous devez sélectionner une type d'ajout .", MsgBoxStyle.OkOnly, "Erreur")
            radChoice.Focus()
            Exit Sub
        End If

        If cbPeriodes.EditValue = Nothing Then
            MsgBox("Vous devez sélectionner une période .", MsgBoxStyle.OkOnly, "Erreur")
            cbPeriodes.Focus()
            Exit Sub
        End If

        If cbLivreur.EditValue = Nothing Then
            MsgBox("Vous devez sélectionner une Livreur.", MsgBoxStyle.OkOnly, "Erreur")
            cbLivreur.Focus()
            Exit Sub
        End If


        If cbOrganisation.EditValue = Nothing Then
            MsgBox("Vous devez sélectionner une Organisation .", MsgBoxStyle.OkOnly, "Erreur")
            cbOrganisation.Focus()
            Exit Sub
        End If

        If tbDescription.Text.ToString.Trim = "" Then
            MsgBox("Vous devez entrer une description .", MsgBoxStyle.OkOnly, "Erreur")
            tbDescription.Focus()
            Exit Sub
        End If


        Dim result As DialogResult = MessageBox.Show("Sauvegarder la fiche?", "Ajouter une ligne a la paie", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
        If result = DialogResult.No Then
            Me.Cursor = Cursors.Default
        ElseIf result = DialogResult.Yes Then



            If tbIDPayeLine.Text > 0 Then
                oPayeLine2.IDPayeLine = tbIDPayeLine.Text
            Else
                MessageBox.Show("Vous devez sélectionner une ligne sur la grille")
                Exit Sub
            End If




            If radChoice.SelectedIndex = 1 Then


                iNombreGlacier = tbQuantite.Text
                dblMontantGlacier = Convert.ToDouble(tbTotal.EditValue) * iNombreGlacier

                dtPayeLine = oPayeLineData.SelectAllGlacier(IDPaye)

                If dtPayeLine.Rows.Count > 0 Then
                    For Each drPayeLine In dtPayeLine.Rows
                        oPayeLine2.IDPayeLine = If(IsDBNull(drPayeLine("IDPayeLine")), 0, drPayeLine("IDPayeLine"))
                        oPayeLine2.Unite = Convert.ToInt32(drPayeLine("Unite")) + iNombreGlacier
                        oPayeLine2.Prix = dblMontantGlacier + If(IsDBNull(drPayeLine("Montant")), 0, CType(drPayeLine("Montant"), Decimal?))
                        oPayeLine2.Montant = dblMontantGlacier + If(IsDBNull(drPayeLine("Montant")), 0, CType(drPayeLine("Montant"), Decimal?))
                        oPayeLineData.UpdateGlacier(oPayeLine2, oPayeLine2)
                    Next
                Else
                    GoTo ADDGLACIER
                End If
            Else


ADDGLACIER:

                oPayeLine.IDPaye = tbIDPaye.Text
                oPayeLine.PayeNumber = tbPayeNumber.Text
                oPayeLine.PayeDate = Now.ToShortDateString
                oPayeLine.IDLivreur = cbLivreur.EditValue
                oPayeLine.Livreur = cbLivreur.Text
                oPayeLine.OrganisationID = cbOrganisation.EditValue
                oPayeLine.Organisation = cbOrganisation.Text
                oPayeLine.Code = Nothing
                oPayeLine.Km = 0
                oPayeLine.Description = tbDescription.Text
                oPayeLine.Unite = tbQuantite.Text


                If chkAjout.Checked = True Then
                    oPayeLine.Ajout = True
                Else
                    oPayeLine.Ajout = False
                End If



                If radChoice.SelectedIndex = 1 Then
                    oPayeLine.Prix = Convert.ToDouble(dblMontantGlacier)
                    oPayeLine.Montant = Convert.ToDouble(dblMontantGlacier)

                Else

                    oPayeLine.Prix = Convert.ToDouble(tbPrix.EditValue)
                    oPayeLine.Montant = Convert.ToDouble(tbTotal.EditValue)

                End If

                oPayeLine.Credit = 0
                oPayeLine.DateFrom = tbDateFrom.Text
                oPayeLine.DateTo = tbDateAu.Text
                oPayeLine.NombreGlacier = 0
                oPayeLine.Glacier = 0
                oPayeLine.TypeAjout = radChoice.SelectedIndex
                oPayeLine.IDPeriode = go_Globals.IDPeriode

                oPayeLineData.Update(oPayeLine, oPayeLine)
            End If




            grdPayeLine.DataSource = oPayeLineData.SelectAllByID(iID)


        End If


        bDelete.Visible = False
        DockPaye.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Hidden
    End Sub

    Private Sub DockPaye_Click(sender As Object, e As EventArgs) Handles DockPaye.Click

    End Sub

    Private Sub tbQuantite_EditValueChanged(sender As Object, e As EventArgs) Handles tbQuantite.EditValueChanged
        If Not IsNothing(tbPrix.EditValue) And Not IsNothing(tbQuantite.EditValue) Then

            tbTotal.EditValue = tbPrix.EditValue * tbQuantite.Text

        End If
    End Sub

    Private Sub tbDescription_EditValueChanged(sender As Object, e As EventArgs) Handles tbDescription.EditValueChanged

    End Sub
End Class