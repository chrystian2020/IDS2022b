﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEpargne
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmEpargne))
        Me.LayoutControl1 = New DevExpress.XtraLayout.LayoutControl()
        Me.tabData = New DevExpress.XtraTab.XtraTabControl()
        Me.XtraTabPage1 = New DevExpress.XtraTab.XtraTabPage()
        Me.grdEpargne = New DevExpress.XtraGrid.GridControl()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colIDAchat = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colDateFrom = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn3 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colMontant = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn8 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.XtraTabPage2 = New DevExpress.XtraTab.XtraTabPage()
        Me.LayoutControl2 = New DevExpress.XtraLayout.LayoutControl()
        Me.bSupprimerRetrait = New DevExpress.XtraEditors.SimpleButton()
        Me.bSupprimer = New DevExpress.XtraEditors.SimpleButton()
        Me.bRetirer = New DevExpress.XtraEditors.SimpleButton()
        Me.grdRetraits = New DevExpress.XtraGrid.GridControl()
        Me.GridView3 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn2 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn5 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn6 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn7 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn9 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.chkActif = New DevExpress.XtraEditors.CheckEdit()
        Me.tbPaiementParPeriode = New DevExpress.XtraEditors.TextEdit()
        Me.grdEpargnes = New DevExpress.XtraGrid.GridControl()
        Me.GridView2 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.IDEpargnePaiment = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.DateEpargne = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.Periode = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.Livreur = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.Epargne = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.IDLivreur = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.IDEpargne = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.TLPBut = New System.Windows.Forms.TableLayoutPanel()
        Me.butApply = New System.Windows.Forms.Button()
        Me.butClose = New System.Windows.Forms.Button()
        Me.butCancel = New System.Windows.Forms.Button()
        Me.nudIDEpargne = New DevExpress.XtraEditors.TextEdit()
        Me.tbDatePret = New DevExpress.XtraEditors.DateEdit()
        Me.cbLivreur = New DevExpress.XtraEditors.LookUpEdit()
        Me.tbBalance = New DevExpress.XtraEditors.TextEdit()
        Me.tbRetrait = New DevExpress.XtraEditors.TextEdit()
        Me.LayoutControlGroup1 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem20 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem8 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem18 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem7 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutAjouterPaiement = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.EmptySpaceItem2 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlItem3 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem6 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem9 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlGroup2 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem2 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem4 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem10 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem11 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem12 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem13 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.TS = New System.Windows.Forms.ToolStrip()
        Me.Head = New System.Windows.Forms.ToolStripLabel()
        Me.bAdd = New System.Windows.Forms.ToolStripButton()
        Me.sp1 = New System.Windows.Forms.ToolStripSeparator()
        Me.bEdit = New System.Windows.Forms.ToolStripButton()
        Me.sp2 = New System.Windows.Forms.ToolStripSeparator()
        Me.bDelete = New System.Windows.Forms.ToolStripButton()
        Me.sp3 = New System.Windows.Forms.ToolStripSeparator()
        Me.bCancel = New System.Windows.Forms.ToolStripButton()
        Me.sp4 = New System.Windows.Forms.ToolStripSeparator()
        Me.bRefresh = New System.Windows.Forms.ToolStripButton()
        Me.sp5 = New System.Windows.Forms.ToolStripSeparator()
        Me.ToolStripButton6 = New System.Windows.Forms.ToolStripButton()
        Me.Root = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem1 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem5 = New DevExpress.XtraLayout.LayoutControlItem()
        CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LayoutControl1.SuspendLayout()
        CType(Me.tabData, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabData.SuspendLayout()
        Me.XtraTabPage1.SuspendLayout()
        CType(Me.grdEpargne, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabPage2.SuspendLayout()
        CType(Me.LayoutControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LayoutControl2.SuspendLayout()
        CType(Me.grdRetraits, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chkActif.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbPaiementParPeriode.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grdEpargnes, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TLPBut.SuspendLayout()
        CType(Me.nudIDEpargne.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbDatePret.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbDatePret.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbLivreur.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbBalance.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbRetrait.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem20, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem18, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutAjouterPaiement, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem13, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TS.SuspendLayout()
        CType(Me.Root, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'LayoutControl1
        '
        Me.LayoutControl1.Controls.Add(Me.tabData)
        Me.LayoutControl1.Controls.Add(Me.TS)
        Me.LayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LayoutControl1.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControl1.Name = "LayoutControl1"
        Me.LayoutControl1.Root = Me.Root
        Me.LayoutControl1.Size = New System.Drawing.Size(1026, 607)
        Me.LayoutControl1.TabIndex = 0
        Me.LayoutControl1.Text = "LayoutControl1"
        '
        'tabData
        '
        Me.tabData.Location = New System.Drawing.Point(12, 36)
        Me.tabData.Name = "tabData"
        Me.tabData.SelectedTabPage = Me.XtraTabPage1
        Me.tabData.Size = New System.Drawing.Size(1002, 559)
        Me.tabData.TabIndex = 10
        Me.tabData.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.XtraTabPage1, Me.XtraTabPage2})
        '
        'XtraTabPage1
        '
        Me.XtraTabPage1.Controls.Add(Me.grdEpargne)
        Me.XtraTabPage1.Name = "XtraTabPage1"
        Me.XtraTabPage1.Size = New System.Drawing.Size(997, 533)
        Me.XtraTabPage1.Text = "Liste des épargnants"
        '
        'grdEpargne
        '
        Me.grdEpargne.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grdEpargne.Location = New System.Drawing.Point(0, 0)
        Me.grdEpargne.MainView = Me.GridView1
        Me.grdEpargne.Name = "grdEpargne"
        Me.grdEpargne.Size = New System.Drawing.Size(1001, 515)
        Me.grdEpargne.TabIndex = 0
        Me.grdEpargne.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'GridView1
        '
        Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colIDAchat, Me.colDateFrom, Me.GridColumn3, Me.colMontant, Me.GridColumn8})
        Me.GridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.GridView1.GridControl = Me.grdEpargne
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsBehavior.Editable = False
        Me.GridView1.OptionsCustomization.AllowGroup = False
        Me.GridView1.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridView1.OptionsSelection.UseIndicatorForSelection = False
        Me.GridView1.OptionsView.ShowAutoFilterRow = True
        Me.GridView1.OptionsView.ShowGroupPanel = False
        Me.GridView1.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.colDateFrom, DevExpress.Data.ColumnSortOrder.Ascending)})
        '
        'colIDAchat
        '
        Me.colIDAchat.Caption = "IDEpargne"
        Me.colIDAchat.FieldName = "IDEpargne"
        Me.colIDAchat.Name = "colIDAchat"
        Me.colIDAchat.Visible = True
        Me.colIDAchat.VisibleIndex = 0
        Me.colIDAchat.Width = 196
        '
        'colDateFrom
        '
        Me.colDateFrom.Caption = "Date de l'épargne"
        Me.colDateFrom.FieldName = "DateEpargne"
        Me.colDateFrom.Name = "colDateFrom"
        Me.colDateFrom.Visible = True
        Me.colDateFrom.VisibleIndex = 1
        Me.colDateFrom.Width = 196
        '
        'GridColumn3
        '
        Me.GridColumn3.Caption = "Livreur"
        Me.GridColumn3.FieldName = "Livreur"
        Me.GridColumn3.Name = "GridColumn3"
        Me.GridColumn3.Visible = True
        Me.GridColumn3.VisibleIndex = 2
        Me.GridColumn3.Width = 152
        '
        'colMontant
        '
        Me.colMontant.Caption = "Montant de l'épargne"
        Me.colMontant.DisplayFormat.FormatString = "c"
        Me.colMontant.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.colMontant.FieldName = "EpargneParPeriode"
        Me.colMontant.Name = "colMontant"
        Me.colMontant.Visible = True
        Me.colMontant.VisibleIndex = 3
        Me.colMontant.Width = 204
        '
        'GridColumn8
        '
        Me.GridColumn8.Caption = "IDLivreur"
        Me.GridColumn8.FieldName = "IDLivreur"
        Me.GridColumn8.Name = "GridColumn8"
        '
        'XtraTabPage2
        '
        Me.XtraTabPage2.Controls.Add(Me.LayoutControl2)
        Me.XtraTabPage2.Name = "XtraTabPage2"
        Me.XtraTabPage2.PageVisible = False
        Me.XtraTabPage2.Size = New System.Drawing.Size(997, 533)
        Me.XtraTabPage2.Text = "Détail sur un épargnant"
        '
        'LayoutControl2
        '
        Me.LayoutControl2.Controls.Add(Me.bSupprimerRetrait)
        Me.LayoutControl2.Controls.Add(Me.bSupprimer)
        Me.LayoutControl2.Controls.Add(Me.bRetirer)
        Me.LayoutControl2.Controls.Add(Me.grdRetraits)
        Me.LayoutControl2.Controls.Add(Me.chkActif)
        Me.LayoutControl2.Controls.Add(Me.tbPaiementParPeriode)
        Me.LayoutControl2.Controls.Add(Me.grdEpargnes)
        Me.LayoutControl2.Controls.Add(Me.TLPBut)
        Me.LayoutControl2.Controls.Add(Me.nudIDEpargne)
        Me.LayoutControl2.Controls.Add(Me.tbDatePret)
        Me.LayoutControl2.Controls.Add(Me.cbLivreur)
        Me.LayoutControl2.Controls.Add(Me.tbBalance)
        Me.LayoutControl2.Controls.Add(Me.tbRetrait)
        Me.LayoutControl2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LayoutControl2.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControl2.Name = "LayoutControl2"
        Me.LayoutControl2.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = New System.Drawing.Rectangle(976, 223, 771, 350)
        Me.LayoutControl2.OptionsCustomizationForm.ShowPropertyGrid = True
        Me.LayoutControl2.Root = Me.LayoutControlGroup1
        Me.LayoutControl2.Size = New System.Drawing.Size(997, 533)
        Me.LayoutControl2.TabIndex = 0
        Me.LayoutControl2.Text = "LayoutControl2"
        '
        'bSupprimerRetrait
        '
        Me.bSupprimerRetrait.Enabled = False
        Me.bSupprimerRetrait.Location = New System.Drawing.Point(487, 203)
        Me.bSupprimerRetrait.Name = "bSupprimerRetrait"
        Me.bSupprimerRetrait.Size = New System.Drawing.Size(486, 22)
        Me.bSupprimerRetrait.StyleController = Me.LayoutControl2
        Me.bSupprimerRetrait.TabIndex = 135
        Me.bSupprimerRetrait.Text = "Supprimer"
        '
        'bSupprimer
        '
        Me.bSupprimer.Enabled = False
        Me.bSupprimer.Location = New System.Drawing.Point(24, 177)
        Me.bSupprimer.Name = "bSupprimer"
        Me.bSupprimer.Size = New System.Drawing.Size(459, 48)
        Me.bSupprimer.StyleController = Me.LayoutControl2
        Me.bSupprimer.TabIndex = 134
        Me.bSupprimer.Text = "Supprimer"
        '
        'bRetirer
        '
        Me.bRetirer.Location = New System.Drawing.Point(487, 177)
        Me.bRetirer.Name = "bRetirer"
        Me.bRetirer.Size = New System.Drawing.Size(234, 22)
        Me.bRetirer.StyleController = Me.LayoutControl2
        Me.bRetirer.TabIndex = 133
        Me.bRetirer.Text = " Faire un retrait"
        '
        'grdRetraits
        '
        Me.grdRetraits.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grdRetraits.Location = New System.Drawing.Point(487, 229)
        Me.grdRetraits.MainView = Me.GridView3
        Me.grdRetraits.Name = "grdRetraits"
        Me.grdRetraits.Size = New System.Drawing.Size(486, 109)
        Me.grdRetraits.TabIndex = 132
        Me.grdRetraits.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView3})
        '
        'GridView3
        '
        Me.GridView3.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn1, Me.GridColumn2, Me.GridColumn5, Me.GridColumn6, Me.GridColumn7, Me.GridColumn9})
        Me.GridView3.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.GridView3.GridControl = Me.grdRetraits
        Me.GridView3.Name = "GridView3"
        Me.GridView3.OptionsBehavior.Editable = False
        Me.GridView3.OptionsCustomization.AllowGroup = False
        Me.GridView3.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridView3.OptionsSelection.UseIndicatorForSelection = False
        Me.GridView3.OptionsView.ShowAutoFilterRow = True
        Me.GridView3.OptionsView.ShowGroupPanel = False
        Me.GridView3.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.GridColumn2, DevExpress.Data.ColumnSortOrder.Ascending)})
        '
        'GridColumn1
        '
        Me.GridColumn1.Caption = "ID Retrait"
        Me.GridColumn1.FieldName = "IDRetrait"
        Me.GridColumn1.Name = "GridColumn1"
        Me.GridColumn1.Width = 196
        '
        'GridColumn2
        '
        Me.GridColumn2.Caption = "Date de retrait"
        Me.GridColumn2.FieldName = "DateRetrait"
        Me.GridColumn2.Name = "GridColumn2"
        Me.GridColumn2.Visible = True
        Me.GridColumn2.VisibleIndex = 0
        Me.GridColumn2.Width = 348
        '
        'GridColumn5
        '
        Me.GridColumn5.Caption = "Livreur"
        Me.GridColumn5.FieldName = "Livreur"
        Me.GridColumn5.Name = "GridColumn5"
        Me.GridColumn5.Width = 152
        '
        'GridColumn6
        '
        Me.GridColumn6.Caption = "Retrait"
        Me.GridColumn6.DisplayFormat.FormatString = "c"
        Me.GridColumn6.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.GridColumn6.FieldName = "Retrait"
        Me.GridColumn6.Name = "GridColumn6"
        Me.GridColumn6.Visible = True
        Me.GridColumn6.VisibleIndex = 1
        Me.GridColumn6.Width = 269
        '
        'GridColumn7
        '
        Me.GridColumn7.Caption = "IDLivreur"
        Me.GridColumn7.FieldName = "IDLivreur"
        Me.GridColumn7.Name = "GridColumn7"
        '
        'GridColumn9
        '
        Me.GridColumn9.Caption = "IDEpargne"
        Me.GridColumn9.FieldName = "IDEpargne"
        Me.GridColumn9.Name = "GridColumn9"
        '
        'chkActif
        '
        Me.chkActif.Location = New System.Drawing.Point(504, 44)
        Me.chkActif.Name = "chkActif"
        Me.chkActif.Properties.Caption = "Actif"
        Me.chkActif.Size = New System.Drawing.Size(469, 19)
        Me.chkActif.StyleController = Me.LayoutControl2
        Me.chkActif.TabIndex = 131
        '
        'tbPaiementParPeriode
        '
        Me.tbPaiementParPeriode.EditValue = "0"
        Me.tbPaiementParPeriode.Location = New System.Drawing.Point(610, 67)
        Me.tbPaiementParPeriode.Name = "tbPaiementParPeriode"
        Me.tbPaiementParPeriode.Properties.DisplayFormat.FormatString = "c"
        Me.tbPaiementParPeriode.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.tbPaiementParPeriode.Properties.EditFormat.FormatString = "c"
        Me.tbPaiementParPeriode.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.tbPaiementParPeriode.Properties.Mask.EditMask = "c"
        Me.tbPaiementParPeriode.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.tbPaiementParPeriode.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.tbPaiementParPeriode.Size = New System.Drawing.Size(363, 20)
        Me.tbPaiementParPeriode.StyleController = Me.LayoutControl2
        Me.tbPaiementParPeriode.TabIndex = 130
        '
        'grdEpargnes
        '
        Me.grdEpargnes.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grdEpargnes.Location = New System.Drawing.Point(24, 229)
        Me.grdEpargnes.MainView = Me.GridView2
        Me.grdEpargnes.Name = "grdEpargnes"
        Me.grdEpargnes.Size = New System.Drawing.Size(459, 109)
        Me.grdEpargnes.TabIndex = 129
        Me.grdEpargnes.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView2})
        '
        'GridView2
        '
        Me.GridView2.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.IDEpargnePaiment, Me.DateEpargne, Me.Periode, Me.Livreur, Me.Epargne, Me.IDLivreur, Me.IDEpargne})
        Me.GridView2.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.GridView2.GridControl = Me.grdEpargnes
        Me.GridView2.Name = "GridView2"
        Me.GridView2.OptionsBehavior.Editable = False
        Me.GridView2.OptionsCustomization.AllowGroup = False
        Me.GridView2.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridView2.OptionsSelection.UseIndicatorForSelection = False
        Me.GridView2.OptionsView.ShowAutoFilterRow = True
        Me.GridView2.OptionsView.ShowGroupPanel = False
        Me.GridView2.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.DateEpargne, DevExpress.Data.ColumnSortOrder.Ascending)})
        '
        'IDEpargnePaiment
        '
        Me.IDEpargnePaiment.Caption = "ID Épargne"
        Me.IDEpargnePaiment.FieldName = "IDEpargnePaiment"
        Me.IDEpargnePaiment.Name = "IDEpargnePaiment"
        Me.IDEpargnePaiment.Width = 196
        '
        'DateEpargne
        '
        Me.DateEpargne.Caption = "Date de l'épargne"
        Me.DateEpargne.FieldName = "DateEpargne"
        Me.DateEpargne.Name = "DateEpargne"
        Me.DateEpargne.Visible = True
        Me.DateEpargne.VisibleIndex = 0
        Me.DateEpargne.Width = 348
        '
        'Periode
        '
        Me.Periode.Caption = "Période"
        Me.Periode.FieldName = "Periode"
        Me.Periode.Name = "Periode"
        Me.Periode.Visible = True
        Me.Periode.VisibleIndex = 1
        Me.Periode.Width = 228
        '
        'Livreur
        '
        Me.Livreur.Caption = "Livreur"
        Me.Livreur.FieldName = "Livreur"
        Me.Livreur.Name = "Livreur"
        Me.Livreur.Width = 152
        '
        'Epargne
        '
        Me.Epargne.Caption = "Épargne"
        Me.Epargne.DisplayFormat.FormatString = "c"
        Me.Epargne.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.Epargne.FieldName = "Epargne"
        Me.Epargne.Name = "Epargne"
        Me.Epargne.Visible = True
        Me.Epargne.VisibleIndex = 2
        Me.Epargne.Width = 269
        '
        'IDLivreur
        '
        Me.IDLivreur.Caption = "IDLivreur"
        Me.IDLivreur.FieldName = "IDLivreur"
        Me.IDLivreur.Name = "IDLivreur"
        '
        'IDEpargne
        '
        Me.IDEpargne.Caption = "IDEpargne"
        Me.IDEpargne.FieldName = "IDEpargne"
        Me.IDEpargne.Name = "IDEpargne"
        '
        'TLPBut
        '
        Me.TLPBut.ColumnCount = 7
        Me.TLPBut.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TLPBut.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100.0!))
        Me.TLPBut.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TLPBut.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100.0!))
        Me.TLPBut.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TLPBut.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100.0!))
        Me.TLPBut.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TLPBut.Controls.Add(Me.butApply, 1, 1)
        Me.TLPBut.Controls.Add(Me.butClose, 5, 1)
        Me.TLPBut.Controls.Add(Me.butCancel, 3, 1)
        Me.TLPBut.Location = New System.Drawing.Point(12, 354)
        Me.TLPBut.Name = "TLPBut"
        Me.TLPBut.RowCount = 2
        Me.TLPBut.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26.0!))
        Me.TLPBut.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 34.0!))
        Me.TLPBut.Size = New System.Drawing.Size(973, 167)
        Me.TLPBut.TabIndex = 8
        '
        'butApply
        '
        Me.butApply.Dock = System.Windows.Forms.DockStyle.Top
        Me.butApply.Location = New System.Drawing.Point(319, 29)
        Me.butApply.Name = "butApply"
        Me.butApply.Size = New System.Drawing.Size(94, 28)
        Me.butApply.TabIndex = 20
        Me.butApply.Text = "Appliquer"
        '
        'butClose
        '
        Me.butClose.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.butClose.Dock = System.Windows.Forms.DockStyle.Top
        Me.butClose.Location = New System.Drawing.Point(559, 29)
        Me.butClose.Name = "butClose"
        Me.butClose.Size = New System.Drawing.Size(94, 28)
        Me.butClose.TabIndex = 22
        Me.butClose.Text = "Fermer"
        '
        'butCancel
        '
        Me.butCancel.Dock = System.Windows.Forms.DockStyle.Top
        Me.butCancel.Location = New System.Drawing.Point(439, 29)
        Me.butCancel.Name = "butCancel"
        Me.butCancel.Size = New System.Drawing.Size(94, 28)
        Me.butCancel.TabIndex = 21
        Me.butCancel.Text = "Retour a la grille"
        '
        'nudIDEpargne
        '
        Me.nudIDEpargne.Enabled = False
        Me.nudIDEpargne.Location = New System.Drawing.Point(118, 12)
        Me.nudIDEpargne.Name = "nudIDEpargne"
        Me.nudIDEpargne.Properties.Appearance.BackColor = System.Drawing.Color.Azure
        Me.nudIDEpargne.Properties.Appearance.Options.UseBackColor = True
        Me.nudIDEpargne.Size = New System.Drawing.Size(370, 20)
        Me.nudIDEpargne.StyleController = Me.LayoutControl2
        Me.nudIDEpargne.TabIndex = 5
        '
        'tbDatePret
        '
        Me.tbDatePret.EditValue = Nothing
        Me.tbDatePret.Location = New System.Drawing.Point(118, 36)
        Me.tbDatePret.Name = "tbDatePret"
        Me.tbDatePret.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.tbDatePret.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.tbDatePret.Properties.DisplayFormat.FormatString = "MM-dd-yy"
        Me.tbDatePret.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.tbDatePret.Properties.EditFormat.FormatString = "MM-dd-yy"
        Me.tbDatePret.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.tbDatePret.Properties.Mask.EditMask = "MM-dd-yy"
        Me.tbDatePret.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.tbDatePret.Size = New System.Drawing.Size(370, 20)
        Me.tbDatePret.StyleController = Me.LayoutControl2
        Me.tbDatePret.TabIndex = 22
        '
        'cbLivreur
        '
        Me.cbLivreur.Location = New System.Drawing.Point(118, 60)
        Me.cbLivreur.Name = "cbLivreur"
        Me.cbLivreur.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbLivreur.Size = New System.Drawing.Size(370, 20)
        Me.cbLivreur.StyleController = Me.LayoutControl2
        Me.cbLivreur.TabIndex = 30
        '
        'tbBalance
        '
        Me.tbBalance.EditValue = "0"
        Me.tbBalance.Location = New System.Drawing.Point(610, 91)
        Me.tbBalance.Name = "tbBalance"
        Me.tbBalance.Properties.DisplayFormat.FormatString = "c"
        Me.tbBalance.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.tbBalance.Properties.EditFormat.FormatString = "c"
        Me.tbBalance.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.tbBalance.Properties.Mask.EditMask = "c"
        Me.tbBalance.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.tbBalance.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.tbBalance.Size = New System.Drawing.Size(363, 20)
        Me.tbBalance.StyleController = Me.LayoutControl2
        Me.tbBalance.TabIndex = 130
        '
        'tbRetrait
        '
        Me.tbRetrait.EditValue = "0"
        Me.tbRetrait.Location = New System.Drawing.Point(831, 177)
        Me.tbRetrait.Name = "tbRetrait"
        Me.tbRetrait.Properties.DisplayFormat.FormatString = "c"
        Me.tbRetrait.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.tbRetrait.Properties.EditFormat.FormatString = "c"
        Me.tbRetrait.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.tbRetrait.Properties.Mask.EditMask = "c"
        Me.tbRetrait.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.tbRetrait.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.tbRetrait.Size = New System.Drawing.Size(142, 20)
        Me.tbRetrait.StyleController = Me.LayoutControl2
        Me.tbRetrait.TabIndex = 130
        '
        'LayoutControlGroup1
        '
        Me.LayoutControlGroup1.CustomizationFormText = "LayoutControlGroup1"
        Me.LayoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup1.GroupBordersVisible = False
        Me.LayoutControlGroup1.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem20, Me.LayoutControlItem8, Me.LayoutControlItem18, Me.LayoutControlItem7, Me.LayoutAjouterPaiement, Me.LayoutControlGroup2})
        Me.LayoutControlGroup1.Name = "Root"
        Me.LayoutControlGroup1.Size = New System.Drawing.Size(997, 533)
        Me.LayoutControlGroup1.TextVisible = False
        '
        'LayoutControlItem20
        '
        Me.LayoutControlItem20.Control = Me.TLPBut
        Me.LayoutControlItem20.CustomizationFormText = "LayoutControlItem20"
        Me.LayoutControlItem20.Location = New System.Drawing.Point(0, 342)
        Me.LayoutControlItem20.Name = "LayoutControlItem20"
        Me.LayoutControlItem20.Size = New System.Drawing.Size(977, 171)
        Me.LayoutControlItem20.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem20.TextVisible = False
        '
        'LayoutControlItem8
        '
        Me.LayoutControlItem8.Control = Me.nudIDEpargne
        Me.LayoutControlItem8.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem8.Name = "LayoutControlItem8"
        Me.LayoutControlItem8.Size = New System.Drawing.Size(480, 24)
        Me.LayoutControlItem8.Text = "Épargne ID"
        Me.LayoutControlItem8.TextSize = New System.Drawing.Size(102, 13)
        '
        'LayoutControlItem18
        '
        Me.LayoutControlItem18.Control = Me.tbDatePret
        Me.LayoutControlItem18.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem18.CustomizationFormText = "Date du"
        Me.LayoutControlItem18.Location = New System.Drawing.Point(0, 24)
        Me.LayoutControlItem18.Name = "LayoutControlItem18"
        Me.LayoutControlItem18.Size = New System.Drawing.Size(480, 24)
        Me.LayoutControlItem18.Text = "Date de création"
        Me.LayoutControlItem18.TextSize = New System.Drawing.Size(102, 13)
        '
        'LayoutControlItem7
        '
        Me.LayoutControlItem7.Control = Me.cbLivreur
        Me.LayoutControlItem7.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem7.CustomizationFormText = "Liste des payes"
        Me.LayoutControlItem7.Location = New System.Drawing.Point(0, 48)
        Me.LayoutControlItem7.Name = "LayoutControlItem7"
        Me.LayoutControlItem7.Size = New System.Drawing.Size(480, 77)
        Me.LayoutControlItem7.Text = "Nom du Livreur"
        Me.LayoutControlItem7.TextSize = New System.Drawing.Size(102, 13)
        '
        'LayoutAjouterPaiement
        '
        Me.LayoutAjouterPaiement.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.EmptySpaceItem2, Me.LayoutControlItem3, Me.LayoutControlItem6, Me.LayoutControlItem9})
        Me.LayoutAjouterPaiement.Location = New System.Drawing.Point(480, 0)
        Me.LayoutAjouterPaiement.Name = "LayoutAjouterPaiement"
        Me.LayoutAjouterPaiement.Size = New System.Drawing.Size(497, 125)
        Me.LayoutAjouterPaiement.Text = "Information sur l'épargne"
        '
        'EmptySpaceItem2
        '
        Me.EmptySpaceItem2.AllowHotTrack = False
        Me.EmptySpaceItem2.Location = New System.Drawing.Point(0, 71)
        Me.EmptySpaceItem2.Name = "EmptySpaceItem2"
        Me.EmptySpaceItem2.Size = New System.Drawing.Size(473, 10)
        Me.EmptySpaceItem2.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlItem3
        '
        Me.LayoutControlItem3.Control = Me.tbPaiementParPeriode
        Me.LayoutControlItem3.Location = New System.Drawing.Point(0, 23)
        Me.LayoutControlItem3.Name = "LayoutControlItem3"
        Me.LayoutControlItem3.Size = New System.Drawing.Size(473, 24)
        Me.LayoutControlItem3.Text = "Montant de l'épargne"
        Me.LayoutControlItem3.TextSize = New System.Drawing.Size(102, 13)
        '
        'LayoutControlItem6
        '
        Me.LayoutControlItem6.Control = Me.chkActif
        Me.LayoutControlItem6.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem6.Name = "LayoutControlItem6"
        Me.LayoutControlItem6.Size = New System.Drawing.Size(473, 23)
        Me.LayoutControlItem6.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem6.TextVisible = False
        '
        'LayoutControlItem9
        '
        Me.LayoutControlItem9.Control = Me.tbBalance
        Me.LayoutControlItem9.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem9.CustomizationFormText = "Montant de l'épargne"
        Me.LayoutControlItem9.Location = New System.Drawing.Point(0, 47)
        Me.LayoutControlItem9.Name = "LayoutControlItem9"
        Me.LayoutControlItem9.Size = New System.Drawing.Size(473, 24)
        Me.LayoutControlItem9.Text = "Balance"
        Me.LayoutControlItem9.TextSize = New System.Drawing.Size(102, 13)
        '
        'LayoutControlGroup2
        '
        Me.LayoutControlGroup2.AppearanceGroup.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LayoutControlGroup2.AppearanceGroup.Options.UseFont = True
        Me.LayoutControlGroup2.AppearanceGroup.Options.UseTextOptions = True
        Me.LayoutControlGroup2.AppearanceGroup.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.LayoutControlGroup2.CustomizationFormText = "Paiements d'épargnes et Retraits"
        Me.LayoutControlGroup2.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem2, Me.LayoutControlItem4, Me.LayoutControlItem10, Me.LayoutControlItem11, Me.LayoutControlItem12, Me.LayoutControlItem13})
        Me.LayoutControlGroup2.Location = New System.Drawing.Point(0, 125)
        Me.LayoutControlGroup2.Name = "LayoutControlGroup2"
        Me.LayoutControlGroup2.Size = New System.Drawing.Size(977, 217)
        Me.LayoutControlGroup2.Text = "Liste des épargnes"
        '
        'LayoutControlItem2
        '
        Me.LayoutControlItem2.Control = Me.grdEpargnes
        Me.LayoutControlItem2.Location = New System.Drawing.Point(0, 52)
        Me.LayoutControlItem2.Name = "LayoutControlItem1"
        Me.LayoutControlItem2.Size = New System.Drawing.Size(463, 113)
        Me.LayoutControlItem2.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem2.TextVisible = False
        '
        'LayoutControlItem4
        '
        Me.LayoutControlItem4.Control = Me.grdRetraits
        Me.LayoutControlItem4.Location = New System.Drawing.Point(463, 52)
        Me.LayoutControlItem4.Name = "LayoutControlItem4"
        Me.LayoutControlItem4.Size = New System.Drawing.Size(490, 113)
        Me.LayoutControlItem4.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem4.TextVisible = False
        '
        'LayoutControlItem10
        '
        Me.LayoutControlItem10.Control = Me.tbRetrait
        Me.LayoutControlItem10.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem10.CustomizationFormText = "Montant de l'épargne"
        Me.LayoutControlItem10.Location = New System.Drawing.Point(701, 0)
        Me.LayoutControlItem10.Name = "LayoutControlItem10"
        Me.LayoutControlItem10.Size = New System.Drawing.Size(252, 26)
        Me.LayoutControlItem10.Text = "Montant a retirer"
        Me.LayoutControlItem10.TextSize = New System.Drawing.Size(102, 13)
        '
        'LayoutControlItem11
        '
        Me.LayoutControlItem11.Control = Me.bRetirer
        Me.LayoutControlItem11.Location = New System.Drawing.Point(463, 0)
        Me.LayoutControlItem11.Name = "LayoutControlItem11"
        Me.LayoutControlItem11.Size = New System.Drawing.Size(238, 26)
        Me.LayoutControlItem11.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem11.TextVisible = False
        '
        'LayoutControlItem12
        '
        Me.LayoutControlItem12.Control = Me.bSupprimer
        Me.LayoutControlItem12.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem12.MinSize = New System.Drawing.Size(61, 1)
        Me.LayoutControlItem12.Name = "LayoutControlItem12"
        Me.LayoutControlItem12.Size = New System.Drawing.Size(463, 52)
        Me.LayoutControlItem12.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom
        Me.LayoutControlItem12.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem12.TextVisible = False
        '
        'LayoutControlItem13
        '
        Me.LayoutControlItem13.Control = Me.bSupprimerRetrait
        Me.LayoutControlItem13.Location = New System.Drawing.Point(463, 26)
        Me.LayoutControlItem13.Name = "LayoutControlItem13"
        Me.LayoutControlItem13.Size = New System.Drawing.Size(490, 26)
        Me.LayoutControlItem13.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem13.TextVisible = False
        '
        'TS
        '
        Me.TS.AutoSize = False
        Me.TS.Dock = System.Windows.Forms.DockStyle.None
        Me.TS.Font = New System.Drawing.Font("Verdana", 9.75!)
        Me.TS.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Head, Me.bAdd, Me.sp1, Me.bEdit, Me.sp2, Me.bDelete, Me.sp3, Me.bCancel, Me.sp4, Me.bRefresh, Me.sp5, Me.ToolStripButton6})
        Me.TS.Location = New System.Drawing.Point(12, 12)
        Me.TS.Name = "TS"
        Me.TS.Size = New System.Drawing.Size(1002, 20)
        Me.TS.TabIndex = 9
        '
        'Head
        '
        Me.Head.Name = "Head"
        Me.Head.Size = New System.Drawing.Size(0, 17)
        '
        'bAdd
        '
        Me.bAdd.Image = CType(resources.GetObject("bAdd.Image"), System.Drawing.Image)
        Me.bAdd.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.bAdd.Name = "bAdd"
        Me.bAdd.Size = New System.Drawing.Size(75, 17)
        Me.bAdd.Text = "Ajouter"
        '
        'sp1
        '
        Me.sp1.Name = "sp1"
        Me.sp1.Size = New System.Drawing.Size(6, 20)
        '
        'bEdit
        '
        Me.bEdit.Image = CType(resources.GetObject("bEdit.Image"), System.Drawing.Image)
        Me.bEdit.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.bEdit.Name = "bEdit"
        Me.bEdit.Size = New System.Drawing.Size(71, 17)
        Me.bEdit.Text = "Edition"
        '
        'sp2
        '
        Me.sp2.Name = "sp2"
        Me.sp2.Size = New System.Drawing.Size(6, 20)
        '
        'bDelete
        '
        Me.bDelete.Image = CType(resources.GetObject("bDelete.Image"), System.Drawing.Image)
        Me.bDelete.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.bDelete.Name = "bDelete"
        Me.bDelete.Size = New System.Drawing.Size(92, 17)
        Me.bDelete.Text = "Supprimer"
        '
        'sp3
        '
        Me.sp3.Name = "sp3"
        Me.sp3.Size = New System.Drawing.Size(6, 20)
        '
        'bCancel
        '
        Me.bCancel.Image = CType(resources.GetObject("bCancel.Image"), System.Drawing.Image)
        Me.bCancel.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.bCancel.Name = "bCancel"
        Me.bCancel.Size = New System.Drawing.Size(76, 17)
        Me.bCancel.Text = "Annuler"
        '
        'sp4
        '
        Me.sp4.Name = "sp4"
        Me.sp4.Size = New System.Drawing.Size(6, 20)
        '
        'bRefresh
        '
        Me.bRefresh.Image = CType(resources.GetObject("bRefresh.Image"), System.Drawing.Image)
        Me.bRefresh.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.bRefresh.Name = "bRefresh"
        Me.bRefresh.Size = New System.Drawing.Size(88, 17)
        Me.bRefresh.Text = "Rafraîchir"
        '
        'sp5
        '
        Me.sp5.Name = "sp5"
        Me.sp5.Size = New System.Drawing.Size(6, 20)
        '
        'ToolStripButton6
        '
        Me.ToolStripButton6.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.ToolStripButton6.Image = CType(resources.GetObject("ToolStripButton6.Image"), System.Drawing.Image)
        Me.ToolStripButton6.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton6.Name = "ToolStripButton6"
        Me.ToolStripButton6.Size = New System.Drawing.Size(72, 17)
        Me.ToolStripButton6.Text = "Fermer"
        '
        'Root
        '
        Me.Root.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.Root.GroupBordersVisible = False
        Me.Root.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem1, Me.LayoutControlItem5})
        Me.Root.Name = "Root"
        Me.Root.Size = New System.Drawing.Size(1026, 607)
        Me.Root.TextVisible = False
        '
        'LayoutControlItem1
        '
        Me.LayoutControlItem1.Control = Me.TS
        Me.LayoutControlItem1.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem1.Name = "LayoutControlItem1"
        Me.LayoutControlItem1.Size = New System.Drawing.Size(1006, 24)
        Me.LayoutControlItem1.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem1.TextVisible = False
        '
        'LayoutControlItem5
        '
        Me.LayoutControlItem5.Control = Me.tabData
        Me.LayoutControlItem5.Location = New System.Drawing.Point(0, 24)
        Me.LayoutControlItem5.Name = "LayoutControlItem5"
        Me.LayoutControlItem5.Size = New System.Drawing.Size(1006, 563)
        Me.LayoutControlItem5.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem5.TextVisible = False
        '
        'frmEpargne
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1026, 607)
        Me.Controls.Add(Me.LayoutControl1)
        Me.Name = "frmEpargne"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Gestion de l'épargne"
        CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LayoutControl1.ResumeLayout(False)
        CType(Me.tabData, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabData.ResumeLayout(False)
        Me.XtraTabPage1.ResumeLayout(False)
        CType(Me.grdEpargne, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabPage2.ResumeLayout(False)
        CType(Me.LayoutControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LayoutControl2.ResumeLayout(False)
        CType(Me.grdRetraits, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chkActif.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbPaiementParPeriode.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grdEpargnes, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TLPBut.ResumeLayout(False)
        CType(Me.nudIDEpargne.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbDatePret.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbDatePret.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbLivreur.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbBalance.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbRetrait.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem20, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem18, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutAjouterPaiement, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem13, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TS.ResumeLayout(False)
        Me.TS.PerformLayout()
        CType(Me.Root, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents LayoutControl1 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents Root As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents TS As ToolStrip
    Friend WithEvents Head As ToolStripLabel
    Friend WithEvents bAdd As ToolStripButton
    Friend WithEvents sp1 As ToolStripSeparator
    Friend WithEvents bEdit As ToolStripButton
    Friend WithEvents sp2 As ToolStripSeparator
    Friend WithEvents bDelete As ToolStripButton
    Friend WithEvents sp3 As ToolStripSeparator
    Friend WithEvents bCancel As ToolStripButton
    Friend WithEvents sp4 As ToolStripSeparator
    Friend WithEvents bRefresh As ToolStripButton
    Friend WithEvents sp5 As ToolStripSeparator
    Friend WithEvents ToolStripButton6 As ToolStripButton
    Friend WithEvents LayoutControlItem1 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents tabData As DevExpress.XtraTab.XtraTabControl
    Friend WithEvents XtraTabPage1 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents grdEpargne As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colIDAchat As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colDateFrom As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn3 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colMontant As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn8 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents XtraTabPage2 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents LayoutControl2 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents chkActif As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents tbPaiementParPeriode As DevExpress.XtraEditors.TextEdit
    Friend WithEvents grdEpargnes As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView2 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents IDEpargnePaiment As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents DateEpargne As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents Periode As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents Livreur As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents Epargne As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents IDLivreur As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents IDEpargne As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents TLPBut As TableLayoutPanel
    Friend WithEvents butApply As Button
    Friend WithEvents butClose As Button
    Friend WithEvents butCancel As Button
    Friend WithEvents nudIDEpargne As DevExpress.XtraEditors.TextEdit
    Friend WithEvents tbDatePret As DevExpress.XtraEditors.DateEdit
    Friend WithEvents cbLivreur As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LayoutControlGroup1 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem20 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem8 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem18 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem7 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutAjouterPaiement As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents EmptySpaceItem2 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents LayoutControlItem3 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem6 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlGroup2 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem2 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem5 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents grdRetraits As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView3 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn5 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn6 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn7 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn9 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents LayoutControlItem4 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents tbBalance As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlItem9 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents bRetirer As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents tbRetrait As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlItem10 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem11 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents bSupprimer As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LayoutControlItem12 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents bSupprimerRetrait As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LayoutControlItem13 As DevExpress.XtraLayout.LayoutControlItem
End Class
