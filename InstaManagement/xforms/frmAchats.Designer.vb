﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAchats
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmAchats))
        Me.TS = New System.Windows.Forms.ToolStrip()
        Me.Head = New System.Windows.Forms.ToolStripLabel()
        Me.bAdd = New System.Windows.Forms.ToolStripButton()
        Me.sp1 = New System.Windows.Forms.ToolStripSeparator()
        Me.bEdit = New System.Windows.Forms.ToolStripButton()
        Me.sp2 = New System.Windows.Forms.ToolStripSeparator()
        Me.bDelete = New System.Windows.Forms.ToolStripButton()
        Me.sp3 = New System.Windows.Forms.ToolStripSeparator()
        Me.bCancel = New System.Windows.Forms.ToolStripButton()
        Me.sp4 = New System.Windows.Forms.ToolStripSeparator()
        Me.bRefresh = New System.Windows.Forms.ToolStripButton()
        Me.sp5 = New System.Windows.Forms.ToolStripSeparator()
        Me.ToolStripButton6 = New System.Windows.Forms.ToolStripButton()
        Me.tabData = New DevExpress.XtraTab.XtraTabControl()
        Me.XtraTabPage1 = New DevExpress.XtraTab.XtraTabPage()
        Me.grdAchat = New DevExpress.XtraGrid.GridControl()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colIDAchat = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colDateFrom = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn3 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colMontant = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.XtraTabPage2 = New DevExpress.XtraTab.XtraTabPage()
        Me.LayoutControl1 = New DevExpress.XtraLayout.LayoutControl()
        Me.chkCalculerTaxes = New DevExpress.XtraEditors.CheckEdit()
        Me.tbDescription = New DevExpress.XtraEditors.TextEdit()
        Me.TLPBut = New System.Windows.Forms.TableLayoutPanel()
        Me.butApply = New System.Windows.Forms.Button()
        Me.butClose = New System.Windows.Forms.Button()
        Me.butCancel = New System.Windows.Forms.Button()
        Me.nudAchatID = New DevExpress.XtraEditors.TextEdit()
        Me.tbDate = New DevExpress.XtraEditors.DateEdit()
        Me.tbQuantite = New DevExpress.XtraEditors.TextEdit()
        Me.tbPrix = New DevExpress.XtraEditors.TextEdit()
        Me.tbTotal = New DevExpress.XtraEditors.TextEdit()
        Me.cbFournisseur = New DevExpress.XtraEditors.LookUpEdit()
        Me.tbTPS = New DevExpress.XtraEditors.TextEdit()
        Me.tbTVQ = New DevExpress.XtraEditors.TextEdit()
        Me.tbmontant = New DevExpress.XtraEditors.TextEdit()
        Me.LayoutControlGroup1 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem20 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem8 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem1 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.EmptySpaceItem2 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlItem18 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.lblQuantite = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutPayeAuLivreur = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem7 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem4 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutPayeAuLivreur2 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutPayeAuLivreur3 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem1 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutPayeAuLivreur1 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutPayeAuLivreur4 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.TS.SuspendLayout()
        CType(Me.tabData, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabData.SuspendLayout()
        Me.XtraTabPage1.SuspendLayout()
        CType(Me.grdAchat, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabPage2.SuspendLayout()
        CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LayoutControl1.SuspendLayout()
        CType(Me.chkCalculerTaxes.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbDescription.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TLPBut.SuspendLayout()
        CType(Me.nudAchatID.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbDate.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbQuantite.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbPrix.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbTotal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbFournisseur.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbTPS.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbTVQ.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbmontant.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem20, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem18, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblQuantite, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutPayeAuLivreur, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutPayeAuLivreur2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutPayeAuLivreur3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutPayeAuLivreur1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutPayeAuLivreur4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'TS
        '
        Me.TS.AutoSize = False
        Me.TS.Font = New System.Drawing.Font("Verdana", 9.75!)
        Me.TS.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Head, Me.bAdd, Me.sp1, Me.bEdit, Me.sp2, Me.bDelete, Me.sp3, Me.bCancel, Me.sp4, Me.bRefresh, Me.sp5, Me.ToolStripButton6})
        Me.TS.Location = New System.Drawing.Point(0, 0)
        Me.TS.Name = "TS"
        Me.TS.Size = New System.Drawing.Size(653, 39)
        Me.TS.TabIndex = 7
        '
        'Head
        '
        Me.Head.Name = "Head"
        Me.Head.Size = New System.Drawing.Size(0, 36)
        '
        'bAdd
        '
        Me.bAdd.Image = CType(resources.GetObject("bAdd.Image"), System.Drawing.Image)
        Me.bAdd.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.bAdd.Name = "bAdd"
        Me.bAdd.Size = New System.Drawing.Size(76, 36)
        Me.bAdd.Text = "Ajouter"
        '
        'sp1
        '
        Me.sp1.Name = "sp1"
        Me.sp1.Size = New System.Drawing.Size(6, 39)
        '
        'bEdit
        '
        Me.bEdit.Image = CType(resources.GetObject("bEdit.Image"), System.Drawing.Image)
        Me.bEdit.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.bEdit.Name = "bEdit"
        Me.bEdit.Size = New System.Drawing.Size(72, 36)
        Me.bEdit.Text = "Edition"
        '
        'sp2
        '
        Me.sp2.Name = "sp2"
        Me.sp2.Size = New System.Drawing.Size(6, 39)
        '
        'bDelete
        '
        Me.bDelete.Image = CType(resources.GetObject("bDelete.Image"), System.Drawing.Image)
        Me.bDelete.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.bDelete.Name = "bDelete"
        Me.bDelete.Size = New System.Drawing.Size(93, 36)
        Me.bDelete.Text = "Supprimer"
        '
        'sp3
        '
        Me.sp3.Name = "sp3"
        Me.sp3.Size = New System.Drawing.Size(6, 39)
        '
        'bCancel
        '
        Me.bCancel.Image = CType(resources.GetObject("bCancel.Image"), System.Drawing.Image)
        Me.bCancel.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.bCancel.Name = "bCancel"
        Me.bCancel.Size = New System.Drawing.Size(77, 36)
        Me.bCancel.Text = "Annuler"
        '
        'sp4
        '
        Me.sp4.Name = "sp4"
        Me.sp4.Size = New System.Drawing.Size(6, 39)
        '
        'bRefresh
        '
        Me.bRefresh.Image = CType(resources.GetObject("bRefresh.Image"), System.Drawing.Image)
        Me.bRefresh.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.bRefresh.Name = "bRefresh"
        Me.bRefresh.Size = New System.Drawing.Size(89, 36)
        Me.bRefresh.Text = "Rafraîchir"
        '
        'sp5
        '
        Me.sp5.Name = "sp5"
        Me.sp5.Size = New System.Drawing.Size(6, 39)
        '
        'ToolStripButton6
        '
        Me.ToolStripButton6.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.ToolStripButton6.Image = CType(resources.GetObject("ToolStripButton6.Image"), System.Drawing.Image)
        Me.ToolStripButton6.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton6.Name = "ToolStripButton6"
        Me.ToolStripButton6.Size = New System.Drawing.Size(73, 36)
        Me.ToolStripButton6.Text = "Fermer"
        '
        'tabData
        '
        Me.tabData.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tabData.Location = New System.Drawing.Point(0, 39)
        Me.tabData.Name = "tabData"
        Me.tabData.SelectedTabPage = Me.XtraTabPage1
        Me.tabData.Size = New System.Drawing.Size(653, 472)
        Me.tabData.TabIndex = 8
        Me.tabData.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.XtraTabPage1, Me.XtraTabPage2})
        '
        'XtraTabPage1
        '
        Me.XtraTabPage1.Controls.Add(Me.grdAchat)
        Me.XtraTabPage1.Name = "XtraTabPage1"
        Me.XtraTabPage1.Size = New System.Drawing.Size(648, 446)
        Me.XtraTabPage1.Text = "Liste des dépenses"
        '
        'grdAchat
        '
        Me.grdAchat.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grdAchat.Location = New System.Drawing.Point(0, 0)
        Me.grdAchat.MainView = Me.GridView1
        Me.grdAchat.Name = "grdAchat"
        Me.grdAchat.Size = New System.Drawing.Size(652, 428)
        Me.grdAchat.TabIndex = 0
        Me.grdAchat.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'GridView1
        '
        Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colIDAchat, Me.colDateFrom, Me.GridColumn3, Me.colMontant})
        Me.GridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.GridView1.GridControl = Me.grdAchat
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsBehavior.Editable = False
        Me.GridView1.OptionsCustomization.AllowGroup = False
        Me.GridView1.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridView1.OptionsSelection.UseIndicatorForSelection = False
        Me.GridView1.OptionsView.ShowAutoFilterRow = True
        Me.GridView1.OptionsView.ShowGroupPanel = False
        Me.GridView1.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.colDateFrom, DevExpress.Data.ColumnSortOrder.Ascending)})
        '
        'colIDAchat
        '
        Me.colIDAchat.Caption = "ID Achat"
        Me.colIDAchat.FieldName = "IDAchat"
        Me.colIDAchat.Name = "colIDAchat"
        Me.colIDAchat.Visible = True
        Me.colIDAchat.VisibleIndex = 0
        Me.colIDAchat.Width = 196
        '
        'colDateFrom
        '
        Me.colDateFrom.Caption = "Date Achat"
        Me.colDateFrom.FieldName = "DateAchat"
        Me.colDateFrom.Name = "colDateFrom"
        Me.colDateFrom.Visible = True
        Me.colDateFrom.VisibleIndex = 1
        Me.colDateFrom.Width = 196
        '
        'GridColumn3
        '
        Me.GridColumn3.Caption = "Fournisseur"
        Me.GridColumn3.FieldName = "Fournisseur"
        Me.GridColumn3.Name = "GridColumn3"
        Me.GridColumn3.Visible = True
        Me.GridColumn3.VisibleIndex = 2
        Me.GridColumn3.Width = 152
        '
        'colMontant
        '
        Me.colMontant.Caption = "Total"
        Me.colMontant.DisplayFormat.FormatString = "c"
        Me.colMontant.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.colMontant.FieldName = "Total"
        Me.colMontant.Name = "colMontant"
        Me.colMontant.Visible = True
        Me.colMontant.VisibleIndex = 3
        Me.colMontant.Width = 204
        '
        'XtraTabPage2
        '
        Me.XtraTabPage2.Controls.Add(Me.LayoutControl1)
        Me.XtraTabPage2.Name = "XtraTabPage2"
        Me.XtraTabPage2.PageVisible = False
        Me.XtraTabPage2.Size = New System.Drawing.Size(648, 446)
        Me.XtraTabPage2.Text = "Détail sur une dépense"
        '
        'LayoutControl1
        '
        Me.LayoutControl1.Controls.Add(Me.chkCalculerTaxes)
        Me.LayoutControl1.Controls.Add(Me.tbDescription)
        Me.LayoutControl1.Controls.Add(Me.TLPBut)
        Me.LayoutControl1.Controls.Add(Me.nudAchatID)
        Me.LayoutControl1.Controls.Add(Me.tbDate)
        Me.LayoutControl1.Controls.Add(Me.tbQuantite)
        Me.LayoutControl1.Controls.Add(Me.tbPrix)
        Me.LayoutControl1.Controls.Add(Me.tbTotal)
        Me.LayoutControl1.Controls.Add(Me.cbFournisseur)
        Me.LayoutControl1.Controls.Add(Me.tbTPS)
        Me.LayoutControl1.Controls.Add(Me.tbTVQ)
        Me.LayoutControl1.Controls.Add(Me.tbmontant)
        Me.LayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LayoutControl1.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControl1.Name = "LayoutControl1"
        Me.LayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = New System.Drawing.Rectangle(976, 223, 771, 350)
        Me.LayoutControl1.OptionsCustomizationForm.ShowPropertyGrid = True
        Me.LayoutControl1.Root = Me.LayoutControlGroup1
        Me.LayoutControl1.Size = New System.Drawing.Size(648, 446)
        Me.LayoutControl1.TabIndex = 0
        Me.LayoutControl1.Text = "LayoutControl1"
        '
        'chkCalculerTaxes
        '
        Me.chkCalculerTaxes.Location = New System.Drawing.Point(12, 108)
        Me.chkCalculerTaxes.Name = "chkCalculerTaxes"
        Me.chkCalculerTaxes.Properties.Caption = "Calculer Taxes ?"
        Me.chkCalculerTaxes.Size = New System.Drawing.Size(321, 19)
        Me.chkCalculerTaxes.StyleController = Me.LayoutControl1
        Me.chkCalculerTaxes.TabIndex = 135
        '
        'tbDescription
        '
        Me.tbDescription.Location = New System.Drawing.Point(97, 84)
        Me.tbDescription.Name = "tbDescription"
        Me.tbDescription.Size = New System.Drawing.Size(236, 20)
        Me.tbDescription.StyleController = Me.LayoutControl1
        Me.tbDescription.TabIndex = 134
        '
        'TLPBut
        '
        Me.TLPBut.ColumnCount = 7
        Me.TLPBut.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TLPBut.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100.0!))
        Me.TLPBut.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TLPBut.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100.0!))
        Me.TLPBut.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TLPBut.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100.0!))
        Me.TLPBut.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TLPBut.Controls.Add(Me.butApply, 1, 1)
        Me.TLPBut.Controls.Add(Me.butClose, 5, 1)
        Me.TLPBut.Controls.Add(Me.butCancel, 3, 1)
        Me.TLPBut.Location = New System.Drawing.Point(12, 275)
        Me.TLPBut.Name = "TLPBut"
        Me.TLPBut.RowCount = 2
        Me.TLPBut.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26.0!))
        Me.TLPBut.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 34.0!))
        Me.TLPBut.Size = New System.Drawing.Size(624, 149)
        Me.TLPBut.TabIndex = 8
        '
        'butApply
        '
        Me.butApply.Dock = System.Windows.Forms.DockStyle.Top
        Me.butApply.Location = New System.Drawing.Point(145, 29)
        Me.butApply.Name = "butApply"
        Me.butApply.Size = New System.Drawing.Size(94, 28)
        Me.butApply.TabIndex = 20
        Me.butApply.Text = "Appliquer"
        '
        'butClose
        '
        Me.butClose.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.butClose.Dock = System.Windows.Forms.DockStyle.Top
        Me.butClose.Location = New System.Drawing.Point(385, 29)
        Me.butClose.Name = "butClose"
        Me.butClose.Size = New System.Drawing.Size(94, 28)
        Me.butClose.TabIndex = 22
        Me.butClose.Text = "Fermer"
        '
        'butCancel
        '
        Me.butCancel.Dock = System.Windows.Forms.DockStyle.Top
        Me.butCancel.Location = New System.Drawing.Point(265, 29)
        Me.butCancel.Name = "butCancel"
        Me.butCancel.Size = New System.Drawing.Size(94, 28)
        Me.butCancel.TabIndex = 21
        Me.butCancel.Text = "Retour a la grille"
        '
        'nudAchatID
        '
        Me.nudAchatID.Enabled = False
        Me.nudAchatID.Location = New System.Drawing.Point(97, 12)
        Me.nudAchatID.Name = "nudAchatID"
        Me.nudAchatID.Properties.Appearance.BackColor = System.Drawing.Color.Azure
        Me.nudAchatID.Properties.Appearance.Options.UseBackColor = True
        Me.nudAchatID.Size = New System.Drawing.Size(236, 20)
        Me.nudAchatID.StyleController = Me.LayoutControl1
        Me.nudAchatID.TabIndex = 5
        '
        'tbDate
        '
        Me.tbDate.EditValue = Nothing
        Me.tbDate.Location = New System.Drawing.Point(97, 36)
        Me.tbDate.Name = "tbDate"
        Me.tbDate.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.tbDate.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.tbDate.Properties.DisplayFormat.FormatString = "MM-dd-yy"
        Me.tbDate.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.tbDate.Properties.EditFormat.FormatString = "MM-dd-yy"
        Me.tbDate.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.tbDate.Properties.Mask.EditMask = "MM-dd-yy"
        Me.tbDate.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.tbDate.Size = New System.Drawing.Size(236, 20)
        Me.tbDate.StyleController = Me.LayoutControl1
        Me.tbDate.TabIndex = 22
        '
        'tbQuantite
        '
        Me.tbQuantite.EditValue = "1"
        Me.tbQuantite.Location = New System.Drawing.Point(97, 131)
        Me.tbQuantite.Name = "tbQuantite"
        Me.tbQuantite.Properties.Mask.EditMask = "n0"
        Me.tbQuantite.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.tbQuantite.Size = New System.Drawing.Size(236, 20)
        Me.tbQuantite.StyleController = Me.LayoutControl1
        Me.tbQuantite.TabIndex = 132
        '
        'tbPrix
        '
        Me.tbPrix.EditValue = "0"
        Me.tbPrix.Location = New System.Drawing.Point(97, 155)
        Me.tbPrix.Name = "tbPrix"
        Me.tbPrix.Properties.DisplayFormat.FormatString = "c"
        Me.tbPrix.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.tbPrix.Properties.EditFormat.FormatString = "c"
        Me.tbPrix.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.tbPrix.Properties.Mask.EditMask = "c"
        Me.tbPrix.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.tbPrix.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.tbPrix.Size = New System.Drawing.Size(236, 20)
        Me.tbPrix.StyleController = Me.LayoutControl1
        Me.tbPrix.TabIndex = 128
        '
        'tbTotal
        '
        Me.tbTotal.EditValue = "0"
        Me.tbTotal.Enabled = False
        Me.tbTotal.Location = New System.Drawing.Point(97, 179)
        Me.tbTotal.Name = "tbTotal"
        Me.tbTotal.Properties.Appearance.BackColor = System.Drawing.SystemColors.Info
        Me.tbTotal.Properties.Appearance.Options.UseBackColor = True
        Me.tbTotal.Properties.DisplayFormat.FormatString = "c"
        Me.tbTotal.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.tbTotal.Properties.EditFormat.FormatString = "c"
        Me.tbTotal.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.tbTotal.Properties.Mask.EditMask = "c"
        Me.tbTotal.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.tbTotal.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.tbTotal.Size = New System.Drawing.Size(236, 20)
        Me.tbTotal.StyleController = Me.LayoutControl1
        Me.tbTotal.TabIndex = 128
        '
        'cbFournisseur
        '
        Me.cbFournisseur.Location = New System.Drawing.Point(97, 60)
        Me.cbFournisseur.Name = "cbFournisseur"
        Me.cbFournisseur.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbFournisseur.Size = New System.Drawing.Size(236, 20)
        Me.cbFournisseur.StyleController = Me.LayoutControl1
        Me.cbFournisseur.TabIndex = 30
        '
        'tbTPS
        '
        Me.tbTPS.EditValue = "0"
        Me.tbTPS.Enabled = False
        Me.tbTPS.Location = New System.Drawing.Point(97, 203)
        Me.tbTPS.Name = "tbTPS"
        Me.tbTPS.Properties.Appearance.BackColor = System.Drawing.SystemColors.Info
        Me.tbTPS.Properties.Appearance.Options.UseBackColor = True
        Me.tbTPS.Properties.DisplayFormat.FormatString = "c"
        Me.tbTPS.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.tbTPS.Properties.EditFormat.FormatString = "c"
        Me.tbTPS.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.tbTPS.Properties.Mask.EditMask = "c"
        Me.tbTPS.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.tbTPS.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.tbTPS.Size = New System.Drawing.Size(236, 20)
        Me.tbTPS.StyleController = Me.LayoutControl1
        Me.tbTPS.TabIndex = 128
        '
        'tbTVQ
        '
        Me.tbTVQ.EditValue = "0"
        Me.tbTVQ.Enabled = False
        Me.tbTVQ.Location = New System.Drawing.Point(97, 227)
        Me.tbTVQ.Name = "tbTVQ"
        Me.tbTVQ.Properties.Appearance.BackColor = System.Drawing.SystemColors.Info
        Me.tbTVQ.Properties.Appearance.Options.UseBackColor = True
        Me.tbTVQ.Properties.DisplayFormat.FormatString = "c"
        Me.tbTVQ.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.tbTVQ.Properties.EditFormat.FormatString = "c"
        Me.tbTVQ.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.tbTVQ.Properties.Mask.EditMask = "c"
        Me.tbTVQ.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.tbTVQ.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.tbTVQ.Size = New System.Drawing.Size(236, 20)
        Me.tbTVQ.StyleController = Me.LayoutControl1
        Me.tbTVQ.TabIndex = 128
        '
        'tbmontant
        '
        Me.tbmontant.EditValue = "0"
        Me.tbmontant.Location = New System.Drawing.Point(97, 251)
        Me.tbmontant.Name = "tbmontant"
        Me.tbmontant.Properties.DisplayFormat.FormatString = "c"
        Me.tbmontant.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.tbmontant.Properties.EditFormat.FormatString = "c"
        Me.tbmontant.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.tbmontant.Properties.Mask.EditMask = "c"
        Me.tbmontant.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.tbmontant.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.tbmontant.Size = New System.Drawing.Size(236, 20)
        Me.tbmontant.StyleController = Me.LayoutControl1
        Me.tbmontant.TabIndex = 128
        '
        'LayoutControlGroup1
        '
        Me.LayoutControlGroup1.CustomizationFormText = "LayoutControlGroup1"
        Me.LayoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup1.GroupBordersVisible = False
        Me.LayoutControlGroup1.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem20, Me.LayoutControlItem8, Me.EmptySpaceItem1, Me.EmptySpaceItem2, Me.LayoutControlItem18, Me.lblQuantite, Me.LayoutPayeAuLivreur, Me.LayoutControlItem7, Me.LayoutControlItem4, Me.LayoutPayeAuLivreur2, Me.LayoutPayeAuLivreur3, Me.LayoutControlItem1, Me.LayoutPayeAuLivreur1, Me.LayoutPayeAuLivreur4})
        Me.LayoutControlGroup1.Name = "Root"
        Me.LayoutControlGroup1.Size = New System.Drawing.Size(648, 446)
        Me.LayoutControlGroup1.TextVisible = False
        '
        'LayoutControlItem20
        '
        Me.LayoutControlItem20.Control = Me.TLPBut
        Me.LayoutControlItem20.CustomizationFormText = "LayoutControlItem20"
        Me.LayoutControlItem20.Location = New System.Drawing.Point(0, 263)
        Me.LayoutControlItem20.Name = "LayoutControlItem20"
        Me.LayoutControlItem20.Size = New System.Drawing.Size(628, 153)
        Me.LayoutControlItem20.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem20.TextVisible = False
        '
        'LayoutControlItem8
        '
        Me.LayoutControlItem8.Control = Me.nudAchatID
        Me.LayoutControlItem8.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem8.Name = "LayoutControlItem8"
        Me.LayoutControlItem8.Size = New System.Drawing.Size(325, 24)
        Me.LayoutControlItem8.Text = "Achat ID"
        Me.LayoutControlItem8.TextSize = New System.Drawing.Size(82, 13)
        '
        'EmptySpaceItem1
        '
        Me.EmptySpaceItem1.AllowHotTrack = False
        Me.EmptySpaceItem1.Location = New System.Drawing.Point(0, 416)
        Me.EmptySpaceItem1.Name = "EmptySpaceItem1"
        Me.EmptySpaceItem1.Size = New System.Drawing.Size(628, 10)
        Me.EmptySpaceItem1.TextSize = New System.Drawing.Size(0, 0)
        '
        'EmptySpaceItem2
        '
        Me.EmptySpaceItem2.AllowHotTrack = False
        Me.EmptySpaceItem2.Location = New System.Drawing.Point(325, 0)
        Me.EmptySpaceItem2.Name = "EmptySpaceItem2"
        Me.EmptySpaceItem2.Size = New System.Drawing.Size(303, 263)
        Me.EmptySpaceItem2.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlItem18
        '
        Me.LayoutControlItem18.Control = Me.tbDate
        Me.LayoutControlItem18.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem18.CustomizationFormText = "Date du"
        Me.LayoutControlItem18.Location = New System.Drawing.Point(0, 24)
        Me.LayoutControlItem18.Name = "LayoutControlItem18"
        Me.LayoutControlItem18.Size = New System.Drawing.Size(325, 24)
        Me.LayoutControlItem18.Text = "Date de dépense"
        Me.LayoutControlItem18.TextSize = New System.Drawing.Size(82, 13)
        '
        'lblQuantite
        '
        Me.lblQuantite.Control = Me.tbQuantite
        Me.lblQuantite.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.lblQuantite.CustomizationFormText = "Nombre maximum free echange (305 et 35)"
        Me.lblQuantite.Location = New System.Drawing.Point(0, 119)
        Me.lblQuantite.Name = "lblQuantite"
        Me.lblQuantite.Size = New System.Drawing.Size(325, 24)
        Me.lblQuantite.Text = "Quantité"
        Me.lblQuantite.TextSize = New System.Drawing.Size(82, 13)
        '
        'LayoutPayeAuLivreur
        '
        Me.LayoutPayeAuLivreur.Control = Me.tbPrix
        Me.LayoutPayeAuLivreur.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutPayeAuLivreur.CustomizationFormText = "Payé au livreur"
        Me.LayoutPayeAuLivreur.Location = New System.Drawing.Point(0, 143)
        Me.LayoutPayeAuLivreur.Name = "LayoutPayeAuLivreur"
        Me.LayoutPayeAuLivreur.Size = New System.Drawing.Size(325, 24)
        Me.LayoutPayeAuLivreur.Text = "Prix"
        Me.LayoutPayeAuLivreur.TextSize = New System.Drawing.Size(82, 13)
        '
        'LayoutControlItem7
        '
        Me.LayoutControlItem7.Control = Me.cbFournisseur
        Me.LayoutControlItem7.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem7.CustomizationFormText = "Liste des payes"
        Me.LayoutControlItem7.Location = New System.Drawing.Point(0, 48)
        Me.LayoutControlItem7.Name = "LayoutControlItem7"
        Me.LayoutControlItem7.Size = New System.Drawing.Size(325, 24)
        Me.LayoutControlItem7.Text = "Fournisseur"
        Me.LayoutControlItem7.TextSize = New System.Drawing.Size(82, 13)
        '
        'LayoutControlItem4
        '
        Me.LayoutControlItem4.Control = Me.tbDescription
        Me.LayoutControlItem4.Location = New System.Drawing.Point(0, 72)
        Me.LayoutControlItem4.Name = "LayoutControlItem4"
        Me.LayoutControlItem4.Size = New System.Drawing.Size(325, 24)
        Me.LayoutControlItem4.Text = "Description"
        Me.LayoutControlItem4.TextSize = New System.Drawing.Size(82, 13)
        '
        'LayoutPayeAuLivreur2
        '
        Me.LayoutPayeAuLivreur2.Control = Me.tbTPS
        Me.LayoutPayeAuLivreur2.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutPayeAuLivreur2.CustomizationFormText = "Payé au livreur"
        Me.LayoutPayeAuLivreur2.Location = New System.Drawing.Point(0, 191)
        Me.LayoutPayeAuLivreur2.Name = "LayoutPayeAuLivreur2"
        Me.LayoutPayeAuLivreur2.Size = New System.Drawing.Size(325, 24)
        Me.LayoutPayeAuLivreur2.Text = "TPS"
        Me.LayoutPayeAuLivreur2.TextSize = New System.Drawing.Size(82, 13)
        '
        'LayoutPayeAuLivreur3
        '
        Me.LayoutPayeAuLivreur3.Control = Me.tbTVQ
        Me.LayoutPayeAuLivreur3.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutPayeAuLivreur3.CustomizationFormText = "Payé au livreur"
        Me.LayoutPayeAuLivreur3.Location = New System.Drawing.Point(0, 215)
        Me.LayoutPayeAuLivreur3.Name = "LayoutPayeAuLivreur3"
        Me.LayoutPayeAuLivreur3.Size = New System.Drawing.Size(325, 24)
        Me.LayoutPayeAuLivreur3.Text = "TVQ"
        Me.LayoutPayeAuLivreur3.TextSize = New System.Drawing.Size(82, 13)
        '
        'LayoutControlItem1
        '
        Me.LayoutControlItem1.Control = Me.chkCalculerTaxes
        Me.LayoutControlItem1.Location = New System.Drawing.Point(0, 96)
        Me.LayoutControlItem1.Name = "LayoutControlItem1"
        Me.LayoutControlItem1.Size = New System.Drawing.Size(325, 23)
        Me.LayoutControlItem1.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem1.TextVisible = False
        '
        'LayoutPayeAuLivreur1
        '
        Me.LayoutPayeAuLivreur1.Control = Me.tbTotal
        Me.LayoutPayeAuLivreur1.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutPayeAuLivreur1.CustomizationFormText = "Payé au livreur"
        Me.LayoutPayeAuLivreur1.Location = New System.Drawing.Point(0, 167)
        Me.LayoutPayeAuLivreur1.Name = "LayoutPayeAuLivreur1"
        Me.LayoutPayeAuLivreur1.Size = New System.Drawing.Size(325, 24)
        Me.LayoutPayeAuLivreur1.Text = "Total"
        Me.LayoutPayeAuLivreur1.TextSize = New System.Drawing.Size(82, 13)
        '
        'LayoutPayeAuLivreur4
        '
        Me.LayoutPayeAuLivreur4.Control = Me.tbmontant
        Me.LayoutPayeAuLivreur4.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutPayeAuLivreur4.CustomizationFormText = "Payé au livreur"
        Me.LayoutPayeAuLivreur4.Location = New System.Drawing.Point(0, 239)
        Me.LayoutPayeAuLivreur4.Name = "LayoutPayeAuLivreur4"
        Me.LayoutPayeAuLivreur4.Size = New System.Drawing.Size(325, 24)
        Me.LayoutPayeAuLivreur4.Text = "Montant"
        Me.LayoutPayeAuLivreur4.TextSize = New System.Drawing.Size(82, 13)
        '
        'frmAchats
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(653, 511)
        Me.Controls.Add(Me.tabData)
        Me.Controls.Add(Me.TS)
        Me.Name = "frmAchats"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Gestion des dépenses"
        Me.TS.ResumeLayout(False)
        Me.TS.PerformLayout()
        CType(Me.tabData, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabData.ResumeLayout(False)
        Me.XtraTabPage1.ResumeLayout(False)
        CType(Me.grdAchat, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabPage2.ResumeLayout(False)
        CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LayoutControl1.ResumeLayout(False)
        CType(Me.chkCalculerTaxes.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbDescription.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TLPBut.ResumeLayout(False)
        CType(Me.nudAchatID.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbDate.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbQuantite.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbPrix.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbTotal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbFournisseur.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbTPS.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbTVQ.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbmontant.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem20, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem18, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblQuantite, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutPayeAuLivreur, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutPayeAuLivreur2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutPayeAuLivreur3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutPayeAuLivreur1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutPayeAuLivreur4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents TS As ToolStrip
    Friend WithEvents Head As ToolStripLabel
    Friend WithEvents bAdd As ToolStripButton
    Friend WithEvents sp1 As ToolStripSeparator
    Friend WithEvents bEdit As ToolStripButton
    Friend WithEvents sp2 As ToolStripSeparator
    Friend WithEvents bDelete As ToolStripButton
    Friend WithEvents sp3 As ToolStripSeparator
    Friend WithEvents bCancel As ToolStripButton
    Friend WithEvents sp4 As ToolStripSeparator
    Friend WithEvents bRefresh As ToolStripButton
    Friend WithEvents sp5 As ToolStripSeparator
    Friend WithEvents ToolStripButton6 As ToolStripButton
    Friend WithEvents tabData As DevExpress.XtraTab.XtraTabControl
    Friend WithEvents XtraTabPage1 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents grdAchat As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colIDAchat As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colDateFrom As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents XtraTabPage2 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents LayoutControl1 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents TLPBut As TableLayoutPanel
    Friend WithEvents butApply As Button
    Friend WithEvents butClose As Button
    Friend WithEvents butCancel As Button
    Friend WithEvents nudAchatID As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlGroup1 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem20 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem8 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents tbDate As DevExpress.XtraEditors.DateEdit
    Friend WithEvents tbQuantite As DevExpress.XtraEditors.TextEdit
    Friend WithEvents tbPrix As DevExpress.XtraEditors.TextEdit
    Friend WithEvents tbTotal As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlItem18 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents lblQuantite As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutPayeAuLivreur As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutPayeAuLivreur1 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem1 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents EmptySpaceItem2 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents colMontant As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents DockManager1 As DevExpress.XtraBars.Docking.DockManager
    Friend WithEvents DockFournisseurs As DevExpress.XtraBars.Docking.DockPanel
    Friend WithEvents DockPanel1_Container As DevExpress.XtraBars.Docking.ControlContainer
    Friend WithEvents LayoutControl2 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents tbFournisseur As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Root As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem5 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents grdFournisseurs As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView2 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents LayoutControlItem3 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents cbFournisseur As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LayoutControlItem7 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents btnAjouter As Button
    Friend WithEvents btnSupprimer As Button
    Friend WithEvents LayoutControlItem6 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem9 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents hideContainerLeft As DevExpress.XtraBars.Docking.AutoHideContainer
    Friend WithEvents GridColumn3 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents tbDescription As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlItem4 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents tbTPS As DevExpress.XtraEditors.TextEdit
    Friend WithEvents tbTVQ As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutPayeAuLivreur2 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutPayeAuLivreur3 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents tbmontant As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutPayeAuLivreur4 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents chkCalculerTaxes As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LayoutControlItem1 As DevExpress.XtraLayout.LayoutControlItem
End Class
