﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAdministrateur
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmAdministrateur))
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.GroupControl5 = New DevExpress.XtraEditors.GroupControl()
        Me.LayoutControl2 = New DevExpress.XtraLayout.LayoutControl()
        Me.btRapportTaxationLivreurPaye = New DevExpress.XtraEditors.SimpleButton()
        Me.tbRapportTaxationLivreurPaye = New DevExpress.XtraEditors.TextEdit()
        Me.btRapportdetaxationIDS = New DevExpress.XtraEditors.SimpleButton()
        Me.tbRapportDeTaxationIDS = New DevExpress.XtraEditors.TextEdit()
        Me.tbRapportTaxation = New DevExpress.XtraEditors.SimpleButton()
        Me.bLivraisonRestaurants = New DevExpress.XtraEditors.SimpleButton()
        Me.bLocationCommissions = New DevExpress.XtraEditors.SimpleButton()
        Me.bLocationVentes = New DevExpress.XtraEditors.SimpleButton()
        Me.bLocationConsolidation = New DevExpress.XtraEditors.SimpleButton()
        Me.bLocationPaye = New DevExpress.XtraEditors.SimpleButton()
        Me.tbLocationPaie = New DevExpress.XtraEditors.TextEdit()
        Me.bLocationFacture = New DevExpress.XtraEditors.SimpleButton()
        Me.tbLocationFacture = New DevExpress.XtraEditors.TextEdit()
        Me.tbLocationConsolidation = New DevExpress.XtraEditors.TextEdit()
        Me.tbLocationVentes = New DevExpress.XtraEditors.TextEdit()
        Me.tbLocationAchats = New DevExpress.XtraEditors.TextEdit()
        Me.bLocationAchats = New DevExpress.XtraEditors.SimpleButton()
        Me.tbLocationCommission = New DevExpress.XtraEditors.TextEdit()
        Me.tbLivraisonRestaurants = New DevExpress.XtraEditors.TextEdit()
        Me.tbRapportDeTaxation = New DevExpress.XtraEditors.TextEdit()
        Me.LayoutControlGroup2 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem2 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem14 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem6 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem19 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem24 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem25 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem26 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem28 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem27 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem29 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem30 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem31 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem33 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem35 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem32 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem34 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem36 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem37 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem38 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem39 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.GroupControl4 = New DevExpress.XtraEditors.GroupControl()
        Me.LayoutControl3 = New DevExpress.XtraLayout.LayoutControl()
        Me.tbCieAdress = New DevExpress.XtraEditors.MemoEdit()
        Me.tbCieTEl = New DevExpress.XtraEditors.TextEdit()
        Me.tbNomCompagnie = New DevExpress.XtraEditors.TextEdit()
        Me.LayoutControlGroup3 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem8 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem9 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem12 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControl1 = New DevExpress.XtraLayout.LayoutControl()
        Me.chkSSL = New DevExpress.XtraEditors.CheckEdit()
        Me.bSauvegarde = New DevExpress.XtraEditors.SimpleButton()
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl()
        Me.LayoutControl4 = New DevExpress.XtraLayout.LayoutControl()
        Me.tbNoTVQ = New DevExpress.XtraEditors.TextEdit()
        Me.tbNoTPS = New DevExpress.XtraEditors.TextEdit()
        Me.LayoutControlGroup4 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem13 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem2 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlItem16 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.GroupControl2 = New DevExpress.XtraEditors.GroupControl()
        Me.LayoutControl5 = New DevExpress.XtraLayout.LayoutControl()
        Me.tbTVQ = New DevExpress.XtraEditors.TextEdit()
        Me.tbTPS = New DevExpress.XtraEditors.TextEdit()
        Me.LayoutControlGroup5 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem17 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem3 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.LayoutControlItem18 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.tbServer = New DevExpress.XtraEditors.TextEdit()
        Me.tbUsername = New DevExpress.XtraEditors.TextEdit()
        Me.tbPassword = New DevExpress.XtraEditors.TextEdit()
        Me.tbPort = New DevExpress.XtraEditors.TextEdit()
        Me.bAnnuler = New DevExpress.XtraEditors.SimpleButton()
        Me.LayoutControlItem7 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlGroup1 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem10 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem20 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlGroup6 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem22 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem21 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem15 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem11 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem23 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem4 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem3 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem5 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem1 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.ImageList2 = New System.Windows.Forms.ImageList(Me.components)
        Me.FolderBrowserDialog1 = New System.Windows.Forms.FolderBrowserDialog()
        Me.LayoutConverter1 = New DevExpress.XtraLayout.Converter.LayoutConverter(Me.components)
        CType(Me.GroupControl5, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl5.SuspendLayout()
        CType(Me.LayoutControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LayoutControl2.SuspendLayout()
        CType(Me.tbRapportTaxationLivreurPaye.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbRapportDeTaxationIDS.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbLocationPaie.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbLocationFacture.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbLocationConsolidation.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbLocationVentes.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbLocationAchats.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbLocationCommission.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbLivraisonRestaurants.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbRapportDeTaxation.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem14, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem19, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem24, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem25, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem26, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem28, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem27, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem29, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem30, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem31, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem33, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem35, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem32, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem34, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem36, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem37, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem38, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem39, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl4.SuspendLayout()
        CType(Me.LayoutControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LayoutControl3.SuspendLayout()
        CType(Me.tbCieAdress.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbCieTEl.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbNomCompagnie.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LayoutControl1.SuspendLayout()
        CType(Me.chkSSL.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.LayoutControl4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LayoutControl4.SuspendLayout()
        CType(Me.tbNoTVQ.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbNoTPS.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem13, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem16, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl2.SuspendLayout()
        CType(Me.LayoutControl5, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LayoutControl5.SuspendLayout()
        CType(Me.tbTVQ.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbTPS.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem17, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem18, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbServer.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbUsername.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbPassword.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbPort.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem20, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem22, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem21, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem15, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem23, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ImageList1
        '
        Me.ImageList1.ImageStream = CType(resources.GetObject("ImageList1.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageList1.Images.SetKeyName(0, "organize.ico")
        Me.ImageList1.Images.SetKeyName(1, "cubes.ico")
        Me.ImageList1.Images.SetKeyName(2, "empty-shopping-cart.png")
        Me.ImageList1.Images.SetKeyName(3, "delivery.png")
        Me.ImageList1.Images.SetKeyName(4, "icon-support.png")
        Me.ImageList1.Images.SetKeyName(5, "Restaurant-Blue-2-icon.png")
        Me.ImageList1.Images.SetKeyName(6, "clients.png")
        Me.ImageList1.Images.SetKeyName(7, "select_city_icon.jpg")
        Me.ImageList1.Images.SetKeyName(8, "employee.png")
        Me.ImageList1.Images.SetKeyName(9, "driver_icon.jpg")
        Me.ImageList1.Images.SetKeyName(10, "gnome-status.png")
        Me.ImageList1.Images.SetKeyName(11, "money_icon.jpg")
        Me.ImageList1.Images.SetKeyName(12, "traffic_light.jpg")
        Me.ImageList1.Images.SetKeyName(13, "exit.png")
        Me.ImageList1.Images.SetKeyName(14, "plus_64.png")
        Me.ImageList1.Images.SetKeyName(15, "pencil_64.png")
        Me.ImageList1.Images.SetKeyName(16, "trash_64.png")
        Me.ImageList1.Images.SetKeyName(17, "tick_64.png")
        Me.ImageList1.Images.SetKeyName(18, "System-Map-icon.png")
        Me.ImageList1.Images.SetKeyName(19, "modem.png")
        '
        'GroupControl5
        '
        Me.GroupControl5.Controls.Add(Me.LayoutControl2)
        Me.GroupControl5.Location = New System.Drawing.Point(12, 281)
        Me.GroupControl5.Name = "GroupControl5"
        Me.GroupControl5.Size = New System.Drawing.Size(845, 338)
        Me.GroupControl5.TabIndex = 116
        Me.GroupControl5.Text = "Location des Rapports"
        '
        'LayoutControl2
        '
        Me.LayoutControl2.Controls.Add(Me.btRapportTaxationLivreurPaye)
        Me.LayoutControl2.Controls.Add(Me.tbRapportTaxationLivreurPaye)
        Me.LayoutControl2.Controls.Add(Me.btRapportdetaxationIDS)
        Me.LayoutControl2.Controls.Add(Me.tbRapportDeTaxationIDS)
        Me.LayoutControl2.Controls.Add(Me.tbRapportTaxation)
        Me.LayoutControl2.Controls.Add(Me.bLivraisonRestaurants)
        Me.LayoutControl2.Controls.Add(Me.bLocationCommissions)
        Me.LayoutControl2.Controls.Add(Me.bLocationVentes)
        Me.LayoutControl2.Controls.Add(Me.bLocationConsolidation)
        Me.LayoutControl2.Controls.Add(Me.bLocationPaye)
        Me.LayoutControl2.Controls.Add(Me.tbLocationPaie)
        Me.LayoutControl2.Controls.Add(Me.bLocationFacture)
        Me.LayoutControl2.Controls.Add(Me.tbLocationFacture)
        Me.LayoutControl2.Controls.Add(Me.tbLocationConsolidation)
        Me.LayoutControl2.Controls.Add(Me.tbLocationVentes)
        Me.LayoutControl2.Controls.Add(Me.tbLocationAchats)
        Me.LayoutControl2.Controls.Add(Me.bLocationAchats)
        Me.LayoutControl2.Controls.Add(Me.tbLocationCommission)
        Me.LayoutControl2.Controls.Add(Me.tbLivraisonRestaurants)
        Me.LayoutControl2.Controls.Add(Me.tbRapportDeTaxation)
        Me.LayoutControl2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LayoutControl2.Location = New System.Drawing.Point(2, 22)
        Me.LayoutControl2.Name = "LayoutControl2"
        Me.LayoutControl2.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = New System.Drawing.Rectangle(673, 40, 650, 400)
        Me.LayoutControl2.Root = Me.LayoutControlGroup2
        Me.LayoutControl2.Size = New System.Drawing.Size(841, 314)
        Me.LayoutControl2.TabIndex = 0
        Me.LayoutControl2.Text = "LayoutControl2"
        '
        'btRapportTaxationLivreurPaye
        '
        Me.btRapportTaxationLivreurPaye.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!)
        Me.btRapportTaxationLivreurPaye.Appearance.Options.UseFont = True
        Me.btRapportTaxationLivreurPaye.Location = New System.Drawing.Point(515, 255)
        Me.btRapportTaxationLivreurPaye.Name = "btRapportTaxationLivreurPaye"
        Me.btRapportTaxationLivreurPaye.Size = New System.Drawing.Size(314, 23)
        Me.btRapportTaxationLivreurPaye.StyleController = Me.LayoutControl2
        Me.btRapportTaxationLivreurPaye.TabIndex = 251
        Me.btRapportTaxationLivreurPaye.Text = "Rapport de taxation Livreur Paye"
        '
        'tbRapportTaxationLivreurPaye
        '
        Me.tbRapportTaxationLivreurPaye.Location = New System.Drawing.Point(236, 255)
        Me.tbRapportTaxationLivreurPaye.Name = "tbRapportTaxationLivreurPaye"
        Me.tbRapportTaxationLivreurPaye.Size = New System.Drawing.Size(275, 20)
        Me.tbRapportTaxationLivreurPaye.StyleController = Me.LayoutControl2
        Me.tbRapportTaxationLivreurPaye.TabIndex = 250
        '
        'btRapportdetaxationIDS
        '
        Me.btRapportdetaxationIDS.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!)
        Me.btRapportdetaxationIDS.Appearance.Options.UseFont = True
        Me.btRapportdetaxationIDS.Location = New System.Drawing.Point(515, 228)
        Me.btRapportdetaxationIDS.Name = "btRapportdetaxationIDS"
        Me.btRapportdetaxationIDS.Size = New System.Drawing.Size(314, 23)
        Me.btRapportdetaxationIDS.StyleController = Me.LayoutControl2
        Me.btRapportdetaxationIDS.TabIndex = 249
        Me.btRapportdetaxationIDS.Text = "Rapport de taxation IDS"
        '
        'tbRapportDeTaxationIDS
        '
        Me.tbRapportDeTaxationIDS.Location = New System.Drawing.Point(236, 228)
        Me.tbRapportDeTaxationIDS.Name = "tbRapportDeTaxationIDS"
        Me.tbRapportDeTaxationIDS.Size = New System.Drawing.Size(275, 20)
        Me.tbRapportDeTaxationIDS.StyleController = Me.LayoutControl2
        Me.tbRapportDeTaxationIDS.TabIndex = 248
        '
        'tbRapportTaxation
        '
        Me.tbRapportTaxation.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tbRapportTaxation.Appearance.Options.UseFont = True
        Me.tbRapportTaxation.Location = New System.Drawing.Point(515, 201)
        Me.tbRapportTaxation.Name = "tbRapportTaxation"
        Me.tbRapportTaxation.Size = New System.Drawing.Size(314, 23)
        Me.tbRapportTaxation.StyleController = Me.LayoutControl2
        Me.tbRapportTaxation.TabIndex = 247
        Me.tbRapportTaxation.Text = "Rapport de taxation Livreur"
        '
        'bLivraisonRestaurants
        '
        Me.bLivraisonRestaurants.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bLivraisonRestaurants.Appearance.Options.UseFont = True
        Me.bLivraisonRestaurants.Location = New System.Drawing.Point(515, 174)
        Me.bLivraisonRestaurants.Name = "bLivraisonRestaurants"
        Me.bLivraisonRestaurants.Size = New System.Drawing.Size(314, 23)
        Me.bLivraisonRestaurants.StyleController = Me.LayoutControl2
        Me.bLivraisonRestaurants.TabIndex = 246
        Me.bLivraisonRestaurants.Text = "Livraisons Restaurants"
        '
        'bLocationCommissions
        '
        Me.bLocationCommissions.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bLocationCommissions.Appearance.Options.UseFont = True
        Me.bLocationCommissions.Location = New System.Drawing.Point(515, 147)
        Me.bLocationCommissions.Name = "bLocationCommissions"
        Me.bLocationCommissions.Size = New System.Drawing.Size(314, 23)
        Me.bLocationCommissions.StyleController = Me.LayoutControl2
        Me.bLocationCommissions.TabIndex = 244
        Me.bLocationCommissions.Text = "Commissions"
        '
        'bLocationVentes
        '
        Me.bLocationVentes.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bLocationVentes.Appearance.Options.UseFont = True
        Me.bLocationVentes.Location = New System.Drawing.Point(515, 120)
        Me.bLocationVentes.Name = "bLocationVentes"
        Me.bLocationVentes.Size = New System.Drawing.Size(314, 23)
        Me.bLocationVentes.StyleController = Me.LayoutControl2
        Me.bLocationVentes.TabIndex = 242
        Me.bLocationVentes.Text = "Ventes"
        '
        'bLocationConsolidation
        '
        Me.bLocationConsolidation.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bLocationConsolidation.Appearance.Options.UseFont = True
        Me.bLocationConsolidation.Location = New System.Drawing.Point(515, 66)
        Me.bLocationConsolidation.Name = "bLocationConsolidation"
        Me.bLocationConsolidation.Size = New System.Drawing.Size(314, 23)
        Me.bLocationConsolidation.StyleController = Me.LayoutControl2
        Me.bLocationConsolidation.TabIndex = 241
        Me.bLocationConsolidation.Text = "Etats de comptes"
        '
        'bLocationPaye
        '
        Me.bLocationPaye.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bLocationPaye.Appearance.Options.UseFont = True
        Me.bLocationPaye.Location = New System.Drawing.Point(515, 39)
        Me.bLocationPaye.Name = "bLocationPaye"
        Me.bLocationPaye.Size = New System.Drawing.Size(314, 23)
        Me.bLocationPaye.StyleController = Me.LayoutControl2
        Me.bLocationPaye.TabIndex = 240
        Me.bLocationPaye.Text = "Payes"
        '
        'tbLocationPaie
        '
        Me.tbLocationPaie.Location = New System.Drawing.Point(236, 36)
        Me.tbLocationPaie.Name = "tbLocationPaie"
        Me.tbLocationPaie.Size = New System.Drawing.Size(275, 20)
        Me.tbLocationPaie.StyleController = Me.LayoutControl2
        Me.tbLocationPaie.TabIndex = 239
        '
        'bLocationFacture
        '
        Me.bLocationFacture.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bLocationFacture.Appearance.Options.UseFont = True
        Me.bLocationFacture.Location = New System.Drawing.Point(515, 12)
        Me.bLocationFacture.Name = "bLocationFacture"
        Me.bLocationFacture.Size = New System.Drawing.Size(314, 23)
        Me.bLocationFacture.StyleController = Me.LayoutControl2
        Me.bLocationFacture.TabIndex = 238
        Me.bLocationFacture.Text = "Factures"
        '
        'tbLocationFacture
        '
        Me.tbLocationFacture.Location = New System.Drawing.Point(236, 12)
        Me.tbLocationFacture.Name = "tbLocationFacture"
        Me.tbLocationFacture.Size = New System.Drawing.Size(275, 20)
        Me.tbLocationFacture.StyleController = Me.LayoutControl2
        Me.tbLocationFacture.TabIndex = 237
        '
        'tbLocationConsolidation
        '
        Me.tbLocationConsolidation.Location = New System.Drawing.Point(236, 66)
        Me.tbLocationConsolidation.Name = "tbLocationConsolidation"
        Me.tbLocationConsolidation.Size = New System.Drawing.Size(275, 20)
        Me.tbLocationConsolidation.StyleController = Me.LayoutControl2
        Me.tbLocationConsolidation.TabIndex = 239
        '
        'tbLocationVentes
        '
        Me.tbLocationVentes.Location = New System.Drawing.Point(236, 120)
        Me.tbLocationVentes.Name = "tbLocationVentes"
        Me.tbLocationVentes.Size = New System.Drawing.Size(275, 20)
        Me.tbLocationVentes.StyleController = Me.LayoutControl2
        Me.tbLocationVentes.TabIndex = 239
        '
        'tbLocationAchats
        '
        Me.tbLocationAchats.Location = New System.Drawing.Point(236, 93)
        Me.tbLocationAchats.Name = "tbLocationAchats"
        Me.tbLocationAchats.Size = New System.Drawing.Size(275, 20)
        Me.tbLocationAchats.StyleController = Me.LayoutControl2
        Me.tbLocationAchats.TabIndex = 239
        '
        'bLocationAchats
        '
        Me.bLocationAchats.Appearance.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bLocationAchats.Appearance.Options.UseFont = True
        Me.bLocationAchats.Location = New System.Drawing.Point(515, 93)
        Me.bLocationAchats.Name = "bLocationAchats"
        Me.bLocationAchats.Size = New System.Drawing.Size(314, 23)
        Me.bLocationAchats.StyleController = Me.LayoutControl2
        Me.bLocationAchats.TabIndex = 243
        Me.bLocationAchats.Text = "Achats"
        '
        'tbLocationCommission
        '
        Me.tbLocationCommission.Location = New System.Drawing.Point(236, 147)
        Me.tbLocationCommission.Name = "tbLocationCommission"
        Me.tbLocationCommission.Size = New System.Drawing.Size(275, 20)
        Me.tbLocationCommission.StyleController = Me.LayoutControl2
        Me.tbLocationCommission.TabIndex = 239
        '
        'tbLivraisonRestaurants
        '
        Me.tbLivraisonRestaurants.Location = New System.Drawing.Point(236, 174)
        Me.tbLivraisonRestaurants.Name = "tbLivraisonRestaurants"
        Me.tbLivraisonRestaurants.Size = New System.Drawing.Size(275, 20)
        Me.tbLivraisonRestaurants.StyleController = Me.LayoutControl2
        Me.tbLivraisonRestaurants.TabIndex = 239
        '
        'tbRapportDeTaxation
        '
        Me.tbRapportDeTaxation.Location = New System.Drawing.Point(236, 201)
        Me.tbRapportDeTaxation.Name = "tbRapportDeTaxation"
        Me.tbRapportDeTaxation.Size = New System.Drawing.Size(275, 20)
        Me.tbRapportDeTaxation.StyleController = Me.LayoutControl2
        Me.tbRapportDeTaxation.TabIndex = 239
        '
        'LayoutControlGroup2
        '
        Me.LayoutControlGroup2.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup2.GroupBordersVisible = False
        Me.LayoutControlGroup2.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem2, Me.LayoutControlItem14, Me.LayoutControlItem6, Me.LayoutControlItem19, Me.LayoutControlItem24, Me.LayoutControlItem25, Me.LayoutControlItem26, Me.LayoutControlItem28, Me.LayoutControlItem27, Me.LayoutControlItem29, Me.LayoutControlItem30, Me.LayoutControlItem31, Me.LayoutControlItem33, Me.LayoutControlItem35, Me.LayoutControlItem32, Me.LayoutControlItem34, Me.LayoutControlItem36, Me.LayoutControlItem37, Me.LayoutControlItem38, Me.LayoutControlItem39})
        Me.LayoutControlGroup2.Name = "Root"
        Me.LayoutControlGroup2.Size = New System.Drawing.Size(841, 314)
        Me.LayoutControlGroup2.TextVisible = False
        '
        'LayoutControlItem2
        '
        Me.LayoutControlItem2.Control = Me.tbLocationFacture
        Me.LayoutControlItem2.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem2.Name = "LayoutControlItem2"
        Me.LayoutControlItem2.Size = New System.Drawing.Size(503, 24)
        Me.LayoutControlItem2.Text = "Location des factures"
        Me.LayoutControlItem2.TextSize = New System.Drawing.Size(220, 13)
        '
        'LayoutControlItem14
        '
        Me.LayoutControlItem14.Control = Me.tbLocationPaie
        Me.LayoutControlItem14.Location = New System.Drawing.Point(0, 24)
        Me.LayoutControlItem14.Name = "LayoutControlItem14"
        Me.LayoutControlItem14.Size = New System.Drawing.Size(503, 30)
        Me.LayoutControlItem14.Text = "Location des paies"
        Me.LayoutControlItem14.TextSize = New System.Drawing.Size(220, 13)
        '
        'LayoutControlItem6
        '
        Me.LayoutControlItem6.Control = Me.bLocationFacture
        Me.LayoutControlItem6.Location = New System.Drawing.Point(503, 0)
        Me.LayoutControlItem6.Name = "LayoutControlItem6"
        Me.LayoutControlItem6.Size = New System.Drawing.Size(318, 27)
        Me.LayoutControlItem6.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem6.TextVisible = False
        '
        'LayoutControlItem19
        '
        Me.LayoutControlItem19.Control = Me.bLocationPaye
        Me.LayoutControlItem19.Location = New System.Drawing.Point(503, 27)
        Me.LayoutControlItem19.Name = "LayoutControlItem19"
        Me.LayoutControlItem19.Size = New System.Drawing.Size(318, 27)
        Me.LayoutControlItem19.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem19.TextVisible = False
        '
        'LayoutControlItem24
        '
        Me.LayoutControlItem24.Control = Me.tbLocationConsolidation
        Me.LayoutControlItem24.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem24.CustomizationFormText = "Location des paies"
        Me.LayoutControlItem24.Location = New System.Drawing.Point(0, 54)
        Me.LayoutControlItem24.Name = "LayoutControlItem24"
        Me.LayoutControlItem24.Size = New System.Drawing.Size(503, 27)
        Me.LayoutControlItem24.Text = "Location des Etats de comptes"
        Me.LayoutControlItem24.TextSize = New System.Drawing.Size(220, 13)
        '
        'LayoutControlItem25
        '
        Me.LayoutControlItem25.Control = Me.bLocationConsolidation
        Me.LayoutControlItem25.Location = New System.Drawing.Point(503, 54)
        Me.LayoutControlItem25.Name = "LayoutControlItem25"
        Me.LayoutControlItem25.Size = New System.Drawing.Size(318, 27)
        Me.LayoutControlItem25.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem25.TextVisible = False
        '
        'LayoutControlItem26
        '
        Me.LayoutControlItem26.Control = Me.tbLocationVentes
        Me.LayoutControlItem26.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem26.CustomizationFormText = "Location des paies"
        Me.LayoutControlItem26.Location = New System.Drawing.Point(0, 108)
        Me.LayoutControlItem26.Name = "LayoutControlItem26"
        Me.LayoutControlItem26.Size = New System.Drawing.Size(503, 27)
        Me.LayoutControlItem26.Text = "Location des Ventes"
        Me.LayoutControlItem26.TextSize = New System.Drawing.Size(220, 13)
        '
        'LayoutControlItem28
        '
        Me.LayoutControlItem28.Control = Me.bLocationVentes
        Me.LayoutControlItem28.Location = New System.Drawing.Point(503, 108)
        Me.LayoutControlItem28.Name = "LayoutControlItem28"
        Me.LayoutControlItem28.Size = New System.Drawing.Size(318, 27)
        Me.LayoutControlItem28.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem28.TextVisible = False
        '
        'LayoutControlItem27
        '
        Me.LayoutControlItem27.Control = Me.tbLocationAchats
        Me.LayoutControlItem27.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem27.CustomizationFormText = "Location des paies"
        Me.LayoutControlItem27.Location = New System.Drawing.Point(0, 81)
        Me.LayoutControlItem27.Name = "LayoutControlItem27"
        Me.LayoutControlItem27.Size = New System.Drawing.Size(503, 27)
        Me.LayoutControlItem27.Text = "Location des Achats"
        Me.LayoutControlItem27.TextSize = New System.Drawing.Size(220, 13)
        '
        'LayoutControlItem29
        '
        Me.LayoutControlItem29.Control = Me.bLocationAchats
        Me.LayoutControlItem29.Location = New System.Drawing.Point(503, 81)
        Me.LayoutControlItem29.Name = "LayoutControlItem29"
        Me.LayoutControlItem29.Size = New System.Drawing.Size(318, 27)
        Me.LayoutControlItem29.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem29.TextVisible = False
        '
        'LayoutControlItem30
        '
        Me.LayoutControlItem30.Control = Me.tbLocationCommission
        Me.LayoutControlItem30.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem30.CustomizationFormText = "Location des paies"
        Me.LayoutControlItem30.Location = New System.Drawing.Point(0, 135)
        Me.LayoutControlItem30.Name = "LayoutControlItem30"
        Me.LayoutControlItem30.Size = New System.Drawing.Size(503, 27)
        Me.LayoutControlItem30.Text = "Location des Commissions"
        Me.LayoutControlItem30.TextSize = New System.Drawing.Size(220, 13)
        '
        'LayoutControlItem31
        '
        Me.LayoutControlItem31.Control = Me.bLocationCommissions
        Me.LayoutControlItem31.Location = New System.Drawing.Point(503, 135)
        Me.LayoutControlItem31.Name = "LayoutControlItem31"
        Me.LayoutControlItem31.Size = New System.Drawing.Size(318, 27)
        Me.LayoutControlItem31.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem31.TextVisible = False
        '
        'LayoutControlItem33
        '
        Me.LayoutControlItem33.Control = Me.tbLivraisonRestaurants
        Me.LayoutControlItem33.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem33.CustomizationFormText = "Location des paies"
        Me.LayoutControlItem33.Location = New System.Drawing.Point(0, 162)
        Me.LayoutControlItem33.Name = "LayoutControlItem33"
        Me.LayoutControlItem33.Size = New System.Drawing.Size(503, 27)
        Me.LayoutControlItem33.Text = "Location des rapport de livraisons restaurants"
        Me.LayoutControlItem33.TextSize = New System.Drawing.Size(220, 13)
        '
        'LayoutControlItem35
        '
        Me.LayoutControlItem35.Control = Me.bLivraisonRestaurants
        Me.LayoutControlItem35.Location = New System.Drawing.Point(503, 162)
        Me.LayoutControlItem35.Name = "LayoutControlItem35"
        Me.LayoutControlItem35.Size = New System.Drawing.Size(318, 27)
        Me.LayoutControlItem35.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem35.TextVisible = False
        '
        'LayoutControlItem32
        '
        Me.LayoutControlItem32.Control = Me.tbRapportDeTaxation
        Me.LayoutControlItem32.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem32.CustomizationFormText = "Location des paies"
        Me.LayoutControlItem32.Location = New System.Drawing.Point(0, 189)
        Me.LayoutControlItem32.Name = "LayoutControlItem32"
        Me.LayoutControlItem32.Size = New System.Drawing.Size(503, 27)
        Me.LayoutControlItem32.Text = "Location des rapport de taxation Livreur"
        Me.LayoutControlItem32.TextSize = New System.Drawing.Size(220, 13)
        '
        'LayoutControlItem34
        '
        Me.LayoutControlItem34.Control = Me.tbRapportTaxation
        Me.LayoutControlItem34.Location = New System.Drawing.Point(503, 189)
        Me.LayoutControlItem34.Name = "LayoutControlItem34"
        Me.LayoutControlItem34.Size = New System.Drawing.Size(318, 27)
        Me.LayoutControlItem34.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem34.TextVisible = False
        '
        'LayoutControlItem36
        '
        Me.LayoutControlItem36.Control = Me.tbRapportDeTaxationIDS
        Me.LayoutControlItem36.Location = New System.Drawing.Point(0, 216)
        Me.LayoutControlItem36.Name = "LayoutControlItem36"
        Me.LayoutControlItem36.Size = New System.Drawing.Size(503, 27)
        Me.LayoutControlItem36.Text = "Location des rapport de taxation IDS"
        Me.LayoutControlItem36.TextSize = New System.Drawing.Size(220, 13)
        '
        'LayoutControlItem37
        '
        Me.LayoutControlItem37.Control = Me.btRapportdetaxationIDS
        Me.LayoutControlItem37.Location = New System.Drawing.Point(503, 216)
        Me.LayoutControlItem37.Name = "LayoutControlItem37"
        Me.LayoutControlItem37.Size = New System.Drawing.Size(318, 27)
        Me.LayoutControlItem37.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem37.TextVisible = False
        '
        'LayoutControlItem38
        '
        Me.LayoutControlItem38.Control = Me.tbRapportTaxationLivreurPaye
        Me.LayoutControlItem38.Location = New System.Drawing.Point(0, 243)
        Me.LayoutControlItem38.Name = "LayoutControlItem38"
        Me.LayoutControlItem38.Size = New System.Drawing.Size(503, 51)
        Me.LayoutControlItem38.Text = "Rapport de taxation Livreur Paye"
        Me.LayoutControlItem38.TextSize = New System.Drawing.Size(220, 13)
        '
        'LayoutControlItem39
        '
        Me.LayoutControlItem39.Control = Me.btRapportTaxationLivreurPaye
        Me.LayoutControlItem39.Location = New System.Drawing.Point(503, 243)
        Me.LayoutControlItem39.Name = "LayoutControlItem39"
        Me.LayoutControlItem39.Size = New System.Drawing.Size(318, 51)
        Me.LayoutControlItem39.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem39.TextVisible = False
        '
        'GroupControl4
        '
        Me.GroupControl4.Controls.Add(Me.LayoutControl3)
        Me.GroupControl4.Location = New System.Drawing.Point(314, 12)
        Me.GroupControl4.Name = "GroupControl4"
        Me.GroupControl4.Size = New System.Drawing.Size(543, 159)
        Me.GroupControl4.TabIndex = 115
        Me.GroupControl4.Text = "Informations sur la Compagnie"
        '
        'LayoutControl3
        '
        Me.LayoutControl3.Controls.Add(Me.tbCieAdress)
        Me.LayoutControl3.Controls.Add(Me.tbCieTEl)
        Me.LayoutControl3.Controls.Add(Me.tbNomCompagnie)
        Me.LayoutControl3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LayoutControl3.Location = New System.Drawing.Point(2, 22)
        Me.LayoutControl3.Name = "LayoutControl3"
        Me.LayoutControl3.Root = Me.LayoutControlGroup3
        Me.LayoutControl3.Size = New System.Drawing.Size(539, 135)
        Me.LayoutControl3.TabIndex = 0
        Me.LayoutControl3.Text = "aaaaaaaaaaaaa"
        '
        'tbCieAdress
        '
        Me.tbCieAdress.Location = New System.Drawing.Point(73, 60)
        Me.tbCieAdress.Name = "tbCieAdress"
        Me.tbCieAdress.Size = New System.Drawing.Size(454, 63)
        Me.tbCieAdress.StyleController = Me.LayoutControl3
        Me.tbCieAdress.TabIndex = 129
        '
        'tbCieTEl
        '
        Me.tbCieTEl.Location = New System.Drawing.Point(73, 36)
        Me.tbCieTEl.Name = "tbCieTEl"
        Me.tbCieTEl.Properties.Mask.EditMask = "(\d?\d?\d?)\d\d\d-\d\d\d\d"
        Me.tbCieTEl.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Regular
        Me.tbCieTEl.Size = New System.Drawing.Size(454, 20)
        Me.tbCieTEl.StyleController = Me.LayoutControl3
        Me.tbCieTEl.TabIndex = 124
        '
        'tbNomCompagnie
        '
        Me.tbNomCompagnie.Location = New System.Drawing.Point(73, 12)
        Me.tbNomCompagnie.Name = "tbNomCompagnie"
        Me.tbNomCompagnie.Size = New System.Drawing.Size(454, 20)
        Me.tbNomCompagnie.StyleController = Me.LayoutControl3
        Me.tbNomCompagnie.TabIndex = 123
        '
        'LayoutControlGroup3
        '
        Me.LayoutControlGroup3.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup3.GroupBordersVisible = False
        Me.LayoutControlGroup3.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem8, Me.LayoutControlItem9, Me.LayoutControlItem12})
        Me.LayoutControlGroup3.Name = "Root"
        Me.LayoutControlGroup3.Size = New System.Drawing.Size(539, 135)
        Me.LayoutControlGroup3.TextVisible = False
        '
        'LayoutControlItem8
        '
        Me.LayoutControlItem8.Control = Me.tbNomCompagnie
        Me.LayoutControlItem8.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem8.Name = "LayoutControlItem8"
        Me.LayoutControlItem8.Size = New System.Drawing.Size(519, 24)
        Me.LayoutControlItem8.Text = "Compagnie:"
        Me.LayoutControlItem8.TextSize = New System.Drawing.Size(57, 13)
        '
        'LayoutControlItem9
        '
        Me.LayoutControlItem9.Control = Me.tbCieTEl
        Me.LayoutControlItem9.Location = New System.Drawing.Point(0, 24)
        Me.LayoutControlItem9.Name = "LayoutControlItem9"
        Me.LayoutControlItem9.Size = New System.Drawing.Size(519, 24)
        Me.LayoutControlItem9.Text = "Téléphone:"
        Me.LayoutControlItem9.TextSize = New System.Drawing.Size(57, 13)
        '
        'LayoutControlItem12
        '
        Me.LayoutControlItem12.Control = Me.tbCieAdress
        Me.LayoutControlItem12.Location = New System.Drawing.Point(0, 48)
        Me.LayoutControlItem12.Name = "LayoutControlItem12"
        Me.LayoutControlItem12.Size = New System.Drawing.Size(519, 67)
        Me.LayoutControlItem12.Text = "Adresse:"
        Me.LayoutControlItem12.TextSize = New System.Drawing.Size(57, 13)
        '
        'LayoutControl1
        '
        Me.LayoutControl1.Controls.Add(Me.chkSSL)
        Me.LayoutControl1.Controls.Add(Me.bSauvegarde)
        Me.LayoutControl1.Controls.Add(Me.GroupControl5)
        Me.LayoutControl1.Controls.Add(Me.GroupControl1)
        Me.LayoutControl1.Controls.Add(Me.GroupControl2)
        Me.LayoutControl1.Controls.Add(Me.tbServer)
        Me.LayoutControl1.Controls.Add(Me.tbUsername)
        Me.LayoutControl1.Controls.Add(Me.tbPassword)
        Me.LayoutControl1.Controls.Add(Me.tbPort)
        Me.LayoutControl1.Controls.Add(Me.bAnnuler)
        Me.LayoutControl1.Controls.Add(Me.GroupControl4)
        Me.LayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LayoutControl1.HiddenItems.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem7})
        Me.LayoutControl1.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControl1.Name = "LayoutControl1"
        Me.LayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = New System.Drawing.Rectangle(593, 371, 250, 350)
        Me.LayoutControl1.Root = Me.LayoutControlGroup1
        Me.LayoutControl1.Size = New System.Drawing.Size(869, 657)
        Me.LayoutControl1.TabIndex = 118
        Me.LayoutControl1.Text = "LayoutControl1"
        '
        'chkSSL
        '
        Me.chkSSL.Location = New System.Drawing.Point(24, 140)
        Me.chkSSL.Name = "chkSSL"
        Me.chkSSL.Properties.Caption = "Utiliser  SSL"
        Me.chkSSL.Size = New System.Drawing.Size(274, 19)
        Me.chkSSL.StyleController = Me.LayoutControl1
        Me.chkSSL.TabIndex = 124
        '
        'bSauvegarde
        '
        Me.bSauvegarde.Location = New System.Drawing.Point(12, 623)
        Me.bSauvegarde.Name = "bSauvegarde"
        Me.bSauvegarde.Size = New System.Drawing.Size(385, 22)
        Me.bSauvegarde.StyleController = Me.LayoutControl1
        Me.bSauvegarde.TabIndex = 117
        Me.bSauvegarde.Text = "Sauvegarder"
        '
        'GroupControl1
        '
        Me.GroupControl1.Controls.Add(Me.LayoutControl4)
        Me.GroupControl1.Location = New System.Drawing.Point(12, 175)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(289, 102)
        Me.GroupControl1.TabIndex = 112
        Me.GroupControl1.Text = "Numéro de taxes de la compagnie"
        '
        'LayoutControl4
        '
        Me.LayoutControl4.Controls.Add(Me.tbNoTVQ)
        Me.LayoutControl4.Controls.Add(Me.tbNoTPS)
        Me.LayoutControl4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LayoutControl4.Location = New System.Drawing.Point(2, 22)
        Me.LayoutControl4.Name = "LayoutControl4"
        Me.LayoutControl4.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = New System.Drawing.Rectangle(775, 306, 650, 400)
        Me.LayoutControl4.Root = Me.LayoutControlGroup4
        Me.LayoutControl4.Size = New System.Drawing.Size(285, 78)
        Me.LayoutControl4.TabIndex = 0
        Me.LayoutControl4.Text = "LayoutControl4"
        '
        'tbNoTVQ
        '
        Me.tbNoTVQ.Location = New System.Drawing.Point(51, 36)
        Me.tbNoTVQ.Name = "tbNoTVQ"
        Me.tbNoTVQ.Size = New System.Drawing.Size(222, 20)
        Me.tbNoTVQ.StyleController = Me.LayoutControl4
        Me.tbNoTVQ.TabIndex = 236
        '
        'tbNoTPS
        '
        Me.tbNoTPS.Location = New System.Drawing.Point(51, 12)
        Me.tbNoTPS.Name = "tbNoTPS"
        Me.tbNoTPS.Size = New System.Drawing.Size(222, 20)
        Me.tbNoTPS.StyleController = Me.LayoutControl4
        Me.tbNoTPS.TabIndex = 235
        '
        'LayoutControlGroup4
        '
        Me.LayoutControlGroup4.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup4.GroupBordersVisible = False
        Me.LayoutControlGroup4.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem13, Me.EmptySpaceItem2, Me.LayoutControlItem16})
        Me.LayoutControlGroup4.Name = "Root"
        Me.LayoutControlGroup4.Size = New System.Drawing.Size(285, 78)
        Me.LayoutControlGroup4.TextVisible = False
        '
        'LayoutControlItem13
        '
        Me.LayoutControlItem13.Control = Me.tbNoTPS
        Me.LayoutControlItem13.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem13.Name = "LayoutControlItem13"
        Me.LayoutControlItem13.Size = New System.Drawing.Size(265, 24)
        Me.LayoutControlItem13.Text = "# TPS:"
        Me.LayoutControlItem13.TextSize = New System.Drawing.Size(35, 13)
        '
        'EmptySpaceItem2
        '
        Me.EmptySpaceItem2.AllowHotTrack = False
        Me.EmptySpaceItem2.Location = New System.Drawing.Point(0, 48)
        Me.EmptySpaceItem2.Name = "EmptySpaceItem2"
        Me.EmptySpaceItem2.Size = New System.Drawing.Size(265, 10)
        Me.EmptySpaceItem2.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlItem16
        '
        Me.LayoutControlItem16.Control = Me.tbNoTVQ
        Me.LayoutControlItem16.Location = New System.Drawing.Point(0, 24)
        Me.LayoutControlItem16.Name = "LayoutControlItem16"
        Me.LayoutControlItem16.Size = New System.Drawing.Size(265, 24)
        Me.LayoutControlItem16.Text = "# TVQ:"
        Me.LayoutControlItem16.TextSize = New System.Drawing.Size(35, 13)
        '
        'GroupControl2
        '
        Me.GroupControl2.Controls.Add(Me.LayoutControl5)
        Me.GroupControl2.Location = New System.Drawing.Point(305, 175)
        Me.GroupControl2.Name = "GroupControl2"
        Me.GroupControl2.Size = New System.Drawing.Size(552, 102)
        Me.GroupControl2.TabIndex = 113
        Me.GroupControl2.Text = "Taxes"
        '
        'LayoutControl5
        '
        Me.LayoutControl5.Controls.Add(Me.tbTVQ)
        Me.LayoutControl5.Controls.Add(Me.tbTPS)
        Me.LayoutControl5.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LayoutControl5.Location = New System.Drawing.Point(2, 22)
        Me.LayoutControl5.Name = "LayoutControl5"
        Me.LayoutControl5.Root = Me.LayoutControlGroup5
        Me.LayoutControl5.Size = New System.Drawing.Size(548, 78)
        Me.LayoutControl5.TabIndex = 0
        Me.LayoutControl5.Text = "TPS:"
        '
        'tbTVQ
        '
        Me.tbTVQ.Location = New System.Drawing.Point(40, 36)
        Me.tbTVQ.Name = "tbTVQ"
        Me.tbTVQ.Size = New System.Drawing.Size(496, 20)
        Me.tbTVQ.StyleController = Me.LayoutControl5
        Me.tbTVQ.TabIndex = 232
        '
        'tbTPS
        '
        Me.tbTPS.Location = New System.Drawing.Point(40, 12)
        Me.tbTPS.Name = "tbTPS"
        Me.tbTPS.Size = New System.Drawing.Size(496, 20)
        Me.tbTPS.StyleController = Me.LayoutControl5
        Me.tbTPS.TabIndex = 231
        '
        'LayoutControlGroup5
        '
        Me.LayoutControlGroup5.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup5.GroupBordersVisible = False
        Me.LayoutControlGroup5.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem17, Me.EmptySpaceItem3, Me.LayoutControlItem18})
        Me.LayoutControlGroup5.Name = "LayoutControlGroup5"
        Me.LayoutControlGroup5.Size = New System.Drawing.Size(548, 78)
        Me.LayoutControlGroup5.TextVisible = False
        '
        'LayoutControlItem17
        '
        Me.LayoutControlItem17.Control = Me.tbTPS
        Me.LayoutControlItem17.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem17.Name = "LayoutControlItem17"
        Me.LayoutControlItem17.Size = New System.Drawing.Size(528, 24)
        Me.LayoutControlItem17.Text = "TPS:"
        Me.LayoutControlItem17.TextSize = New System.Drawing.Size(24, 13)
        '
        'EmptySpaceItem3
        '
        Me.EmptySpaceItem3.AllowHotTrack = False
        Me.EmptySpaceItem3.Location = New System.Drawing.Point(0, 48)
        Me.EmptySpaceItem3.Name = "EmptySpaceItem3"
        Me.EmptySpaceItem3.Size = New System.Drawing.Size(528, 10)
        Me.EmptySpaceItem3.TextSize = New System.Drawing.Size(0, 0)
        '
        'LayoutControlItem18
        '
        Me.LayoutControlItem18.Control = Me.tbTVQ
        Me.LayoutControlItem18.Location = New System.Drawing.Point(0, 24)
        Me.LayoutControlItem18.Name = "LayoutControlItem18"
        Me.LayoutControlItem18.Size = New System.Drawing.Size(528, 24)
        Me.LayoutControlItem18.Text = "TVQ:"
        Me.LayoutControlItem18.TextSize = New System.Drawing.Size(24, 13)
        '
        'tbServer
        '
        Me.tbServer.Location = New System.Drawing.Point(159, 44)
        Me.tbServer.Name = "tbServer"
        Me.tbServer.Size = New System.Drawing.Size(139, 20)
        Me.tbServer.StyleController = Me.LayoutControl1
        Me.tbServer.TabIndex = 123
        '
        'tbUsername
        '
        Me.tbUsername.Location = New System.Drawing.Point(159, 68)
        Me.tbUsername.Name = "tbUsername"
        Me.tbUsername.Size = New System.Drawing.Size(139, 20)
        Me.tbUsername.StyleController = Me.LayoutControl1
        Me.tbUsername.TabIndex = 123
        '
        'tbPassword
        '
        Me.tbPassword.Location = New System.Drawing.Point(159, 92)
        Me.tbPassword.Name = "tbPassword"
        Me.tbPassword.Size = New System.Drawing.Size(139, 20)
        Me.tbPassword.StyleController = Me.LayoutControl1
        Me.tbPassword.TabIndex = 123
        '
        'tbPort
        '
        Me.tbPort.Location = New System.Drawing.Point(159, 116)
        Me.tbPort.Name = "tbPort"
        Me.tbPort.Size = New System.Drawing.Size(139, 20)
        Me.tbPort.StyleController = Me.LayoutControl1
        Me.tbPort.TabIndex = 123
        '
        'bAnnuler
        '
        Me.bAnnuler.Location = New System.Drawing.Point(401, 623)
        Me.bAnnuler.Name = "bAnnuler"
        Me.bAnnuler.Size = New System.Drawing.Size(456, 22)
        Me.bAnnuler.StyleController = Me.LayoutControl1
        Me.bAnnuler.TabIndex = 118
        Me.bAnnuler.Text = "Annuler"
        '
        'LayoutControlItem7
        '
        Me.LayoutControlItem7.CustomizationFormText = "LayoutControlItem7"
        Me.LayoutControlItem7.Location = New System.Drawing.Point(208, 0)
        Me.LayoutControlItem7.Name = "LayoutControlItem7"
        Me.LayoutControlItem7.Size = New System.Drawing.Size(104, 74)
        Me.LayoutControlItem7.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem7.TextVisible = False
        '
        'LayoutControlGroup1
        '
        Me.LayoutControlGroup1.CustomizationFormText = "Root"
        Me.LayoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup1.GroupBordersVisible = False
        Me.LayoutControlGroup1.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem10, Me.LayoutControlItem20, Me.LayoutControlGroup6, Me.LayoutControlItem4, Me.LayoutControlItem3, Me.LayoutControlItem5, Me.LayoutControlItem1})
        Me.LayoutControlGroup1.Name = "Root"
        Me.LayoutControlGroup1.Size = New System.Drawing.Size(869, 657)
        Me.LayoutControlGroup1.TextVisible = False
        '
        'LayoutControlItem10
        '
        Me.LayoutControlItem10.Control = Me.bSauvegarde
        Me.LayoutControlItem10.Location = New System.Drawing.Point(0, 611)
        Me.LayoutControlItem10.Name = "LayoutControlItem10"
        Me.LayoutControlItem10.Size = New System.Drawing.Size(389, 26)
        Me.LayoutControlItem10.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem10.TextVisible = False
        '
        'LayoutControlItem20
        '
        Me.LayoutControlItem20.Control = Me.bAnnuler
        Me.LayoutControlItem20.Location = New System.Drawing.Point(389, 611)
        Me.LayoutControlItem20.Name = "LayoutControlItem20"
        Me.LayoutControlItem20.Size = New System.Drawing.Size(460, 26)
        Me.LayoutControlItem20.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem20.TextVisible = False
        '
        'LayoutControlGroup6
        '
        Me.LayoutControlGroup6.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem22, Me.LayoutControlItem21, Me.LayoutControlItem15, Me.LayoutControlItem11, Me.LayoutControlItem23})
        Me.LayoutControlGroup6.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlGroup6.Name = "LayoutControlGroup6"
        Me.LayoutControlGroup6.Size = New System.Drawing.Size(302, 163)
        Me.LayoutControlGroup6.Text = "Configuration serveur de courriel"
        '
        'LayoutControlItem22
        '
        Me.LayoutControlItem22.Control = Me.tbPort
        Me.LayoutControlItem22.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem22.CustomizationFormText = "Compagnie:"
        Me.LayoutControlItem22.Location = New System.Drawing.Point(0, 72)
        Me.LayoutControlItem22.Name = "LayoutControlItem22"
        Me.LayoutControlItem22.Size = New System.Drawing.Size(278, 24)
        Me.LayoutControlItem22.Text = "Port"
        Me.LayoutControlItem22.TextSize = New System.Drawing.Size(131, 13)
        '
        'LayoutControlItem21
        '
        Me.LayoutControlItem21.Control = Me.tbPassword
        Me.LayoutControlItem21.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem21.CustomizationFormText = "Compagnie:"
        Me.LayoutControlItem21.Location = New System.Drawing.Point(0, 48)
        Me.LayoutControlItem21.Name = "LayoutControlItem21"
        Me.LayoutControlItem21.Size = New System.Drawing.Size(278, 24)
        Me.LayoutControlItem21.Text = "Mot de passe"
        Me.LayoutControlItem21.TextSize = New System.Drawing.Size(131, 13)
        '
        'LayoutControlItem15
        '
        Me.LayoutControlItem15.Control = Me.tbUsername
        Me.LayoutControlItem15.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem15.CustomizationFormText = "Compagnie:"
        Me.LayoutControlItem15.Location = New System.Drawing.Point(0, 24)
        Me.LayoutControlItem15.Name = "LayoutControlItem15"
        Me.LayoutControlItem15.Size = New System.Drawing.Size(278, 24)
        Me.LayoutControlItem15.Text = "Nom d'usager (courriel)"
        Me.LayoutControlItem15.TextSize = New System.Drawing.Size(131, 13)
        '
        'LayoutControlItem11
        '
        Me.LayoutControlItem11.Control = Me.tbServer
        Me.LayoutControlItem11.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem11.CustomizationFormText = "Compagnie:"
        Me.LayoutControlItem11.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem11.Name = "LayoutControlItem11"
        Me.LayoutControlItem11.Size = New System.Drawing.Size(278, 24)
        Me.LayoutControlItem11.Text = "Serveur de courrier sortant"
        Me.LayoutControlItem11.TextSize = New System.Drawing.Size(131, 13)
        '
        'LayoutControlItem23
        '
        Me.LayoutControlItem23.Control = Me.chkSSL
        Me.LayoutControlItem23.Location = New System.Drawing.Point(0, 96)
        Me.LayoutControlItem23.Name = "LayoutControlItem23"
        Me.LayoutControlItem23.Size = New System.Drawing.Size(278, 23)
        Me.LayoutControlItem23.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem23.TextVisible = False
        '
        'LayoutControlItem4
        '
        Me.LayoutControlItem4.Control = Me.GroupControl1
        Me.LayoutControlItem4.Location = New System.Drawing.Point(0, 163)
        Me.LayoutControlItem4.Name = "LayoutControlItem4"
        Me.LayoutControlItem4.Size = New System.Drawing.Size(293, 106)
        Me.LayoutControlItem4.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem4.TextVisible = False
        '
        'LayoutControlItem3
        '
        Me.LayoutControlItem3.Control = Me.GroupControl2
        Me.LayoutControlItem3.Location = New System.Drawing.Point(293, 163)
        Me.LayoutControlItem3.Name = "LayoutControlItem3"
        Me.LayoutControlItem3.Size = New System.Drawing.Size(556, 106)
        Me.LayoutControlItem3.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem3.TextVisible = False
        '
        'LayoutControlItem5
        '
        Me.LayoutControlItem5.Control = Me.GroupControl4
        Me.LayoutControlItem5.Location = New System.Drawing.Point(302, 0)
        Me.LayoutControlItem5.Name = "LayoutControlItem5"
        Me.LayoutControlItem5.Size = New System.Drawing.Size(547, 163)
        Me.LayoutControlItem5.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem5.TextVisible = False
        '
        'LayoutControlItem1
        '
        Me.LayoutControlItem1.Control = Me.GroupControl5
        Me.LayoutControlItem1.Location = New System.Drawing.Point(0, 269)
        Me.LayoutControlItem1.Name = "LayoutControlItem1"
        Me.LayoutControlItem1.Size = New System.Drawing.Size(849, 342)
        Me.LayoutControlItem1.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem1.TextVisible = False
        '
        'ImageList2
        '
        Me.ImageList2.ImageStream = CType(resources.GetObject("ImageList2.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList2.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageList2.Images.SetKeyName(0, "cubes.ico")
        Me.ImageList2.Images.SetKeyName(1, "shared.ico")
        Me.ImageList2.Images.SetKeyName(2, "conference.ico")
        Me.ImageList2.Images.SetKeyName(3, "people.ico")
        Me.ImageList2.Images.SetKeyName(4, "download.ico")
        Me.ImageList2.Images.SetKeyName(5, "cal.ico")
        Me.ImageList2.Images.SetKeyName(6, "statics-2.ico")
        Me.ImageList2.Images.SetKeyName(7, "chart.ico")
        Me.ImageList2.Images.SetKeyName(8, "connect.ico")
        Me.ImageList2.Images.SetKeyName(9, "payment.ico")
        Me.ImageList2.Images.SetKeyName(10, "shopping-basket.ico")
        Me.ImageList2.Images.SetKeyName(11, "stats.ico")
        Me.ImageList2.Images.SetKeyName(12, "get_info.ico")
        Me.ImageList2.Images.SetKeyName(13, "info_32.png")
        Me.ImageList2.Images.SetKeyName(14, "pie_chart_48.png")
        Me.ImageList2.Images.SetKeyName(15, "paste.ico")
        Me.ImageList2.Images.SetKeyName(16, "user group.ico")
        Me.ImageList2.Images.SetKeyName(17, "archives.png")
        Me.ImageList2.Images.SetKeyName(18, "diagram_32.png")
        Me.ImageList2.Images.SetKeyName(19, "billing.png")
        Me.ImageList2.Images.SetKeyName(20, "box_32.png")
        Me.ImageList2.Images.SetKeyName(21, "money_32.png")
        Me.ImageList2.Images.SetKeyName(22, "clients.png")
        Me.ImageList2.Images.SetKeyName(23, "Restaurant-Blue-2-icon.png")
        '
        'frmAdministrateur
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(96.0!, 96.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi
        Me.ClientSize = New System.Drawing.Size(869, 657)
        Me.Controls.Add(Me.LayoutControl1)
        Me.IconOptions.Icon = CType(resources.GetObject("frmAdministrateur.IconOptions.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.Name = "frmAdministrateur"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Configuration administrative"
        CType(Me.GroupControl5, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl5.ResumeLayout(False)
        CType(Me.LayoutControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LayoutControl2.ResumeLayout(False)
        CType(Me.tbRapportTaxationLivreurPaye.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbRapportDeTaxationIDS.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbLocationPaie.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbLocationFacture.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbLocationConsolidation.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbLocationVentes.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbLocationAchats.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbLocationCommission.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbLivraisonRestaurants.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbRapportDeTaxation.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem14, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem19, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem24, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem25, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem26, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem28, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem27, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem29, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem30, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem31, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem33, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem35, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem32, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem34, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem36, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem37, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem38, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem39, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl4.ResumeLayout(False)
        CType(Me.LayoutControl3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LayoutControl3.ResumeLayout(False)
        CType(Me.tbCieAdress.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbCieTEl.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbNomCompagnie.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LayoutControl1.ResumeLayout(False)
        CType(Me.chkSSL.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        CType(Me.LayoutControl4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LayoutControl4.ResumeLayout(False)
        CType(Me.tbNoTVQ.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbNoTPS.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem13, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem16, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl2.ResumeLayout(False)
        CType(Me.LayoutControl5, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LayoutControl5.ResumeLayout(False)
        CType(Me.tbTVQ.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbTPS.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem17, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem18, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbServer.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbUsername.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbPassword.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbPort.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem20, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem22, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem21, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem15, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem23, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents ImageList1 As System.Windows.Forms.ImageList
    Friend WithEvents GroupControl5 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents GroupControl4 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents LayoutControl1 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents GroupControl2 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents LayoutControlItem7 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlGroup1 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents ImageList2 As System.Windows.Forms.ImageList
    Friend WithEvents FolderBrowserDialog1 As System.Windows.Forms.FolderBrowserDialog
    Friend WithEvents LayoutControl2 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents LayoutControlGroup2 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutConverter1 As DevExpress.XtraLayout.Converter.LayoutConverter
    Friend WithEvents tbLocationFacture As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlItem2 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents bLocationFacture As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LayoutControlItem6 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControl3 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents LayoutControlGroup3 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents tbNomCompagnie As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlItem8 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents tbCieTEl As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlItem9 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents tbCieAdress As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents LayoutControlItem12 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControl4 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents LayoutControlGroup4 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents tbNoTPS As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlItem13 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem2 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents tbNoTVQ As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlItem16 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControl5 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents LayoutControlGroup5 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents tbTPS As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlItem17 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem3 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents tbTVQ As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlItem18 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents bLocationPaye As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents tbLocationPaie As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlItem14 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem19 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents bAnnuler As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents bSauvegarde As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LayoutControlItem10 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents chkSSL As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents tbServer As DevExpress.XtraEditors.TextEdit
    Friend WithEvents tbUsername As DevExpress.XtraEditors.TextEdit
    Friend WithEvents tbPassword As DevExpress.XtraEditors.TextEdit
    Friend WithEvents tbPort As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlItem23 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlGroup6 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem22 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem21 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem15 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem11 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents bLocationConsolidation As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents tbLocationConsolidation As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlItem24 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem25 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents tbLocationVentes As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlItem26 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents tbLocationAchats As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlItem27 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents bLocationVentes As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents bLocationAchats As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LayoutControlItem28 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem29 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents bLocationCommissions As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents tbLocationCommission As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlItem30 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem31 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents bLivraisonRestaurants As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents tbLivraisonRestaurants As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlItem33 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem35 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem20 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem4 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem3 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem5 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem1 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents tbRapportTaxation As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents tbRapportDeTaxation As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlItem32 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem34 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents btRapportdetaxationIDS As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents tbRapportDeTaxationIDS As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlItem36 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem37 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents btRapportTaxationLivreurPaye As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents tbRapportTaxationLivreurPaye As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlItem38 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem39 As DevExpress.XtraLayout.LayoutControlItem
End Class
