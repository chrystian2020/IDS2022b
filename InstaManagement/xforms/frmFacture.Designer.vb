﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmFacture
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmFacture))
        Dim GridFormatRule2 As DevExpress.XtraGrid.GridFormatRule = New DevExpress.XtraGrid.GridFormatRule()
        Dim FormatConditionRuleValue2 As DevExpress.XtraEditors.FormatConditionRuleValue = New DevExpress.XtraEditors.FormatConditionRuleValue()
        Me.colDistance = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.TS = New System.Windows.Forms.ToolStrip()
        Me.Head = New System.Windows.Forms.ToolStripLabel()
        Me.sp1 = New System.Windows.Forms.ToolStripSeparator()
        Me.bDelete = New System.Windows.Forms.ToolStripButton()
        Me.sp3 = New System.Windows.Forms.ToolStripSeparator()
        Me.bPaiement = New System.Windows.Forms.ToolStripButton()
        Me.bNouvelleFacture = New System.Windows.Forms.ToolStripButton()
        Me.bAjouterLaFacture = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.bCancel = New System.Windows.Forms.ToolStripButton()
        Me.sp4 = New System.Windows.Forms.ToolStripSeparator()
        Me.ToolStripButton6 = New System.Windows.Forms.ToolStripButton()
        Me.tabData = New DevExpress.XtraTab.XtraTabControl()
        Me.XtraTabPage1 = New DevExpress.XtraTab.XtraTabPage()
        Me.LayoutControl2 = New DevExpress.XtraLayout.LayoutControl()
        Me.bDeletePaiement = New DevExpress.XtraEditors.SimpleButton()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.grdPaye = New DevExpress.XtraGrid.GridControl()
        Me.GridView4 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn8 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn9 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn11 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn13 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn10 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.grdFactures = New DevExpress.XtraGrid.GridControl()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colIDInvoice = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn5 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colOrganisation = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.ColDateFrom = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colDateTo = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colInvoiceDate = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colOrganisationID2 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn12 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn14 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn16 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn17 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn15 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn18 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.Root = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem2 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem12 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem21 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem39 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.EmptySpaceItem1 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.EmptySpaceItem2 = New DevExpress.XtraLayout.EmptySpaceItem()
        Me.XtraTabPage2 = New DevExpress.XtraTab.XtraTabPage()
        Me.LayoutControl1 = New DevExpress.XtraLayout.LayoutControl()
        Me.bExportToHTML = New DevExpress.XtraEditors.SimpleButton()
        Me.bExportToPDF = New DevExpress.XtraEditors.SimpleButton()
        Me.bExportToDoc = New DevExpress.XtraEditors.SimpleButton()
        Me.bExportToExcel = New DevExpress.XtraEditors.SimpleButton()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.grdImport = New DevExpress.XtraGrid.GridControl()
        Me.GridView3 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colImportID = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn2 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colReference = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colClient = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemMemoEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit()
        Me.colClientAddress = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colMomentDePassage = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colPreparePar = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colHeureAppel = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colHeurePrep = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colHeureDepart = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colHeureLivraison = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colMessagePassage = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemMemoEdit3 = New DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit()
        Me.colTotalTempService = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colLivreur = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn3 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemMemoEdit2 = New DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit()
        Me.GridColumn4 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn6 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn7 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colMontantGlacierLivreur = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colMontantGlacierOrganisation = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colMontantLivreur = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.Extras = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colExtraKM = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemCalcEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit()
        Me.RepositoryItemTextEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit()
        Me.RepositoryItemTextEdit2 = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit()
        Me.RepositoryItemCalcEdit2 = New DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit()
        Me.RepositoryItemSpinEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit()
        Me.RepositoryItemCheckEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit()
        Me.grdInvoiceLine = New DevExpress.XtraGrid.GridControl()
        Me.GridView2 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colIDInvoiceLine = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colInvoiceNumber = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colInvoiceDate2 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colOrganisationID = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colOrganisation2 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colDateFrom2 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colDateTo2 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colDescription = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colUnite = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colPrix = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colMontant = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCredit = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colDebit = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colNombreGlacier = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colGlacier = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colDisplayDate = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.LayoutControlGroup1 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem1 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem3 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem4 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem8 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem6 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem7 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem5 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.DockManager1 = New DevExpress.XtraBars.Docking.DockManager(Me.components)
        Me.DockPaiement = New DevExpress.XtraBars.Docking.DockPanel()
        Me.ControlContainer2 = New DevExpress.XtraBars.Docking.ControlContainer()
        Me.LayoutControl5 = New DevExpress.XtraLayout.LayoutControl()
        Me.bFermerPaiement = New DevExpress.XtraEditors.SimpleButton()
        Me.bSauvegarderPaiement = New DevExpress.XtraEditors.SimpleButton()
        Me.tbIDFactureP = New DevExpress.XtraEditors.TextEdit()
        Me.cbPeriodesP = New DevExpress.XtraEditors.LookUpEdit()
        Me.tbInvoiceNumberP = New DevExpress.XtraEditors.TextEdit()
        Me.tbDateFromP = New DevExpress.XtraEditors.DateEdit()
        Me.tbDateAuP = New DevExpress.XtraEditors.DateEdit()
        Me.cbOrganisationP = New DevExpress.XtraEditors.LookUpEdit()
        Me.tbMontantPaiement = New DevExpress.XtraEditors.TextEdit()
        Me.LayoutControlGroup4 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem30 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem31 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem32 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem33 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem34 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem35 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem36 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem37 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem38 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.DockLigneFacture = New DevExpress.XtraBars.Docking.DockPanel()
        Me.ControlContainer1 = New DevExpress.XtraBars.Docking.ControlContainer()
        Me.LayoutControl4 = New DevExpress.XtraLayout.LayoutControl()
        Me.chkAjout = New DevExpress.XtraEditors.CheckEdit()
        Me.bUpdate = New DevExpress.XtraEditors.SimpleButton()
        Me.tbPrixI = New DevExpress.XtraEditors.TextEdit()
        Me.radChoix = New DevExpress.XtraEditors.RadioGroup()
        Me.btnAnnulerI = New DevExpress.XtraEditors.SimpleButton()
        Me.bSauvegardeI = New DevExpress.XtraEditors.SimpleButton()
        Me.cbOrganisationI = New DevExpress.XtraEditors.LookUpEdit()
        Me.tbInvoiceNumberI = New DevExpress.XtraEditors.TextEdit()
        Me.tbDateFromI = New DevExpress.XtraEditors.DateEdit()
        Me.tbDateAuI = New DevExpress.XtraEditors.DateEdit()
        Me.cbPeriodesI = New DevExpress.XtraEditors.LookUpEdit()
        Me.tbIDFactureI = New DevExpress.XtraEditors.TextEdit()
        Me.tbDescriptionI = New DevExpress.XtraEditors.TextEdit()
        Me.tbQuantiteI = New DevExpress.XtraEditors.TextEdit()
        Me.tbDateFactureI = New DevExpress.XtraEditors.DateEdit()
        Me.tbIDFactureLigneI = New DevExpress.XtraEditors.TextEdit()
        Me.tbTotalI = New DevExpress.XtraEditors.TextEdit()
        Me.tbIDFactureI1 = New DevExpress.XtraEditors.TextEdit()
        Me.LayoutControlItem29 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlGroup3 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem16 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem17 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem18 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem19 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem20 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutSave = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem22 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.lblQuantite = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem24 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem26 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem28 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.lblOrganisation = New DevExpress.XtraLayout.LayoutControlItem()
        Me.lblDescription = New DevExpress.XtraLayout.LayoutControlItem()
        Me.lblPrixI = New DevExpress.XtraLayout.LayoutControlItem()
        Me.lblTotalI = New DevExpress.XtraLayout.LayoutControlItem()
        Me.layoutUpdate = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem23 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.DockNouvelleFacture = New DevExpress.XtraBars.Docking.DockPanel()
        Me.DockPanel1_Container = New DevExpress.XtraBars.Docking.ControlContainer()
        Me.LayoutControl3 = New DevExpress.XtraLayout.LayoutControl()
        Me.btnAnnuler = New DevExpress.XtraEditors.SimpleButton()
        Me.bSauvegarde = New DevExpress.XtraEditors.SimpleButton()
        Me.cbOrganisation = New DevExpress.XtraEditors.LookUpEdit()
        Me.tbInvoiceNumber = New DevExpress.XtraEditors.TextEdit()
        Me.tbDateFrom = New DevExpress.XtraEditors.DateEdit()
        Me.tbDateAu = New DevExpress.XtraEditors.DateEdit()
        Me.cbPeriodes = New DevExpress.XtraEditors.LookUpEdit()
        Me.tbIDFacture = New DevExpress.XtraEditors.TextEdit()
        Me.tbDateFacture = New DevExpress.XtraEditors.DateEdit()
        Me.LayoutControlGroup2 = New DevExpress.XtraLayout.LayoutControlGroup()
        Me.LayoutControlItem10 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem13 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem14 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem15 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutNombreFree1 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem9 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem11 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem27 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.LayoutControlItem25 = New DevExpress.XtraLayout.LayoutControlItem()
        Me.TS.SuspendLayout()
        CType(Me.tabData, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabData.SuspendLayout()
        Me.XtraTabPage1.SuspendLayout()
        CType(Me.LayoutControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LayoutControl2.SuspendLayout()
        CType(Me.grdPaye, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grdFactures, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Root, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem21, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem39, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmptySpaceItem2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabPage2.SuspendLayout()
        CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LayoutControl1.SuspendLayout()
        CType(Me.grdImport, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemMemoEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemMemoEdit3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemMemoEdit2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemCalcEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemTextEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemTextEdit2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemCalcEdit2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemSpinEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemCheckEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grdInvoiceLine, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DockManager1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.DockPaiement.SuspendLayout()
        Me.ControlContainer2.SuspendLayout()
        CType(Me.LayoutControl5, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LayoutControl5.SuspendLayout()
        CType(Me.tbIDFactureP.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbPeriodesP.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbInvoiceNumberP.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbDateFromP.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbDateFromP.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbDateAuP.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbDateAuP.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbOrganisationP.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbMontantPaiement.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem30, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem31, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem32, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem33, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem34, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem35, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem36, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem37, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem38, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.DockLigneFacture.SuspendLayout()
        Me.ControlContainer1.SuspendLayout()
        CType(Me.LayoutControl4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LayoutControl4.SuspendLayout()
        CType(Me.chkAjout.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbPrixI.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.radChoix.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbOrganisationI.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbInvoiceNumberI.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbDateFromI.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbDateFromI.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbDateAuI.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbDateAuI.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbPeriodesI.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbIDFactureI.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbDescriptionI.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbQuantiteI.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbDateFactureI.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbDateFactureI.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbIDFactureLigneI.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbTotalI.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbIDFactureI1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem29, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem16, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem17, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem18, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem19, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem20, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutSave, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem22, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblQuantite, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem24, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem26, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem28, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblOrganisation, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblDescription, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblPrixI, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lblTotalI, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.layoutUpdate, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem23, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.DockNouvelleFacture.SuspendLayout()
        Me.DockPanel1_Container.SuspendLayout()
        CType(Me.LayoutControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.LayoutControl3.SuspendLayout()
        CType(Me.cbOrganisation.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbInvoiceNumber.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbDateFrom.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbDateFrom.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbDateAu.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbDateAu.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbPeriodes.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbIDFacture.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbDateFacture.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbDateFacture.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlGroup2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem13, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem14, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem15, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutNombreFree1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem27, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LayoutControlItem25, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'colDistance
        '
        Me.colDistance.AppearanceCell.Options.UseTextOptions = True
        Me.colDistance.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center
        Me.colDistance.Caption = "Distance"
        Me.colDistance.FieldName = "Distance"
        Me.colDistance.Name = "colDistance"
        Me.colDistance.OptionsColumn.AllowEdit = False
        Me.colDistance.OptionsColumn.AllowFocus = False
        Me.colDistance.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Distance", "SUM={0:0.##}")})
        Me.colDistance.Visible = True
        Me.colDistance.VisibleIndex = 14
        Me.colDistance.Width = 92
        '
        'TS
        '
        Me.TS.AutoSize = False
        Me.TS.Font = New System.Drawing.Font("Verdana", 9.75!)
        Me.TS.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Head, Me.sp1, Me.bDelete, Me.sp3, Me.bPaiement, Me.bNouvelleFacture, Me.bAjouterLaFacture, Me.ToolStripSeparator1, Me.bCancel, Me.sp4, Me.ToolStripButton6})
        Me.TS.Location = New System.Drawing.Point(0, 0)
        Me.TS.Name = "TS"
        Me.TS.Size = New System.Drawing.Size(1363, 39)
        Me.TS.TabIndex = 7
        '
        'Head
        '
        Me.Head.Name = "Head"
        Me.Head.Size = New System.Drawing.Size(0, 36)
        '
        'sp1
        '
        Me.sp1.Name = "sp1"
        Me.sp1.Size = New System.Drawing.Size(6, 39)
        '
        'bDelete
        '
        Me.bDelete.Image = CType(resources.GetObject("bDelete.Image"), System.Drawing.Image)
        Me.bDelete.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.bDelete.Name = "bDelete"
        Me.bDelete.Size = New System.Drawing.Size(93, 36)
        Me.bDelete.Text = "Supprimer"
        Me.bDelete.Visible = False
        '
        'sp3
        '
        Me.sp3.Name = "sp3"
        Me.sp3.Size = New System.Drawing.Size(6, 39)
        '
        'bPaiement
        '
        Me.bPaiement.Enabled = False
        Me.bPaiement.Image = CType(resources.GetObject("bPaiement.Image"), System.Drawing.Image)
        Me.bPaiement.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.bPaiement.Name = "bPaiement"
        Me.bPaiement.Size = New System.Drawing.Size(145, 36)
        Me.bPaiement.Text = "Faire un paiement"
        Me.bPaiement.Visible = False
        '
        'bNouvelleFacture
        '
        Me.bNouvelleFacture.Image = CType(resources.GetObject("bNouvelleFacture.Image"), System.Drawing.Image)
        Me.bNouvelleFacture.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.bNouvelleFacture.Name = "bNouvelleFacture"
        Me.bNouvelleFacture.Size = New System.Drawing.Size(136, 36)
        Me.bNouvelleFacture.Text = "Nouvelle facture"
        '
        'bAjouterLaFacture
        '
        Me.bAjouterLaFacture.Image = CType(resources.GetObject("bAjouterLaFacture.Image"), System.Drawing.Image)
        Me.bAjouterLaFacture.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.bAjouterLaFacture.Name = "bAjouterLaFacture"
        Me.bAjouterLaFacture.Size = New System.Drawing.Size(214, 36)
        Me.bAjouterLaFacture.Text = "Ajouter une ligne de facture"
        Me.bAjouterLaFacture.Visible = False
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(6, 39)
        '
        'bCancel
        '
        Me.bCancel.Image = CType(resources.GetObject("bCancel.Image"), System.Drawing.Image)
        Me.bCancel.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.bCancel.Name = "bCancel"
        Me.bCancel.Size = New System.Drawing.Size(220, 36)
        Me.bCancel.Text = "Retour a la liste des factures"
        Me.bCancel.Visible = False
        '
        'sp4
        '
        Me.sp4.Name = "sp4"
        Me.sp4.Size = New System.Drawing.Size(6, 39)
        '
        'ToolStripButton6
        '
        Me.ToolStripButton6.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.ToolStripButton6.Image = CType(resources.GetObject("ToolStripButton6.Image"), System.Drawing.Image)
        Me.ToolStripButton6.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton6.Name = "ToolStripButton6"
        Me.ToolStripButton6.Size = New System.Drawing.Size(73, 36)
        Me.ToolStripButton6.Text = "Fermer"
        '
        'tabData
        '
        Me.tabData.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tabData.Location = New System.Drawing.Point(0, 39)
        Me.tabData.Name = "tabData"
        Me.tabData.SelectedTabPage = Me.XtraTabPage1
        Me.tabData.Size = New System.Drawing.Size(1363, 647)
        Me.tabData.TabIndex = 8
        Me.tabData.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.XtraTabPage1, Me.XtraTabPage2})
        '
        'XtraTabPage1
        '
        Me.XtraTabPage1.Controls.Add(Me.LayoutControl2)
        Me.XtraTabPage1.Name = "XtraTabPage1"
        Me.XtraTabPage1.Size = New System.Drawing.Size(1358, 621)
        Me.XtraTabPage1.Text = "Liste des factures"
        '
        'LayoutControl2
        '
        Me.LayoutControl2.Controls.Add(Me.bDeletePaiement)
        Me.LayoutControl2.Controls.Add(Me.LabelControl2)
        Me.LayoutControl2.Controls.Add(Me.grdPaye)
        Me.LayoutControl2.Controls.Add(Me.grdFactures)
        Me.LayoutControl2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LayoutControl2.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControl2.Name = "LayoutControl2"
        Me.LayoutControl2.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = New System.Drawing.Rectangle(752, 665, 650, 400)
        Me.LayoutControl2.Root = Me.Root
        Me.LayoutControl2.Size = New System.Drawing.Size(1358, 621)
        Me.LayoutControl2.TabIndex = 0
        Me.LayoutControl2.Text = "LayoutControl2"
        '
        'bDeletePaiement
        '
        Me.bDeletePaiement.Enabled = False
        Me.bDeletePaiement.Location = New System.Drawing.Point(614, 307)
        Me.bDeletePaiement.Name = "bDeletePaiement"
        Me.bDeletePaiement.Size = New System.Drawing.Size(137, 22)
        Me.bDeletePaiement.StyleController = Me.LayoutControl2
        Me.bDeletePaiement.TabIndex = 29
        Me.bDeletePaiement.Text = "Supprimer un paiement"
        '
        'LabelControl2
        '
        Me.LabelControl2.Appearance.Font = New System.Drawing.Font("Tahoma", 15.0!)
        Me.LabelControl2.Appearance.Options.UseFont = True
        Me.LabelControl2.Appearance.Options.UseTextOptions = True
        Me.LabelControl2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.LabelControl2.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl2.Location = New System.Drawing.Point(12, 279)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(1334, 24)
        Me.LabelControl2.StyleController = Me.LayoutControl2
        Me.LabelControl2.TabIndex = 28
        Me.LabelControl2.Text = "Paiements"
        '
        'grdPaye
        '
        Me.grdPaye.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grdPaye.Location = New System.Drawing.Point(12, 333)
        Me.grdPaye.MainView = Me.GridView4
        Me.grdPaye.Name = "grdPaye"
        Me.grdPaye.Size = New System.Drawing.Size(1334, 276)
        Me.grdPaye.TabIndex = 6
        Me.grdPaye.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView4})
        '
        'GridView4
        '
        Me.GridView4.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn8, Me.GridColumn9, Me.GridColumn11, Me.GridColumn13, Me.GridColumn10})
        Me.GridView4.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.GridView4.GridControl = Me.grdPaye
        Me.GridView4.Name = "GridView4"
        Me.GridView4.OptionsBehavior.Editable = False
        Me.GridView4.OptionsCustomization.AllowGroup = False
        Me.GridView4.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridView4.OptionsSelection.UseIndicatorForSelection = False
        Me.GridView4.OptionsView.ShowAutoFilterRow = True
        Me.GridView4.OptionsView.ShowFooter = True
        Me.GridView4.OptionsView.ShowGroupPanel = False
        Me.GridView4.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.GridColumn9, DevExpress.Data.ColumnSortOrder.Ascending)})
        '
        'GridColumn8
        '
        Me.GridColumn8.Caption = "ID Paiement"
        Me.GridColumn8.FieldName = "IDPaiement"
        Me.GridColumn8.Name = "GridColumn8"
        Me.GridColumn8.Visible = True
        Me.GridColumn8.VisibleIndex = 0
        Me.GridColumn8.Width = 94
        '
        'GridColumn9
        '
        Me.GridColumn9.Caption = "Numéro de facture"
        Me.GridColumn9.FieldName = "InvoiceNumber"
        Me.GridColumn9.Name = "GridColumn9"
        Me.GridColumn9.Visible = True
        Me.GridColumn9.VisibleIndex = 1
        Me.GridColumn9.Width = 94
        '
        'GridColumn11
        '
        Me.GridColumn11.Caption = "Période"
        Me.GridColumn11.FieldName = "Periode"
        Me.GridColumn11.Name = "GridColumn11"
        Me.GridColumn11.Visible = True
        Me.GridColumn11.VisibleIndex = 3
        '
        'GridColumn13
        '
        Me.GridColumn13.Caption = "Date de paiement"
        Me.GridColumn13.FieldName = "DatePaiement"
        Me.GridColumn13.Name = "GridColumn13"
        Me.GridColumn13.Visible = True
        Me.GridColumn13.VisibleIndex = 2
        '
        'GridColumn10
        '
        Me.GridColumn10.Caption = "Montant de paiement"
        Me.GridColumn10.DisplayFormat.FormatString = "c"
        Me.GridColumn10.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.GridColumn10.FieldName = "Montant"
        Me.GridColumn10.Name = "GridColumn10"
        Me.GridColumn10.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Montant", "SUM={0:0.##}")})
        Me.GridColumn10.Visible = True
        Me.GridColumn10.VisibleIndex = 4
        '
        'grdFactures
        '
        Me.grdFactures.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grdFactures.Location = New System.Drawing.Point(12, 12)
        Me.grdFactures.MainView = Me.GridView1
        Me.grdFactures.Name = "grdFactures"
        Me.grdFactures.Size = New System.Drawing.Size(1334, 263)
        Me.grdFactures.TabIndex = 5
        Me.grdFactures.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'GridView1
        '
        Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colIDInvoice, Me.GridColumn5, Me.colOrganisation, Me.ColDateFrom, Me.colDateTo, Me.colInvoiceDate, Me.colOrganisationID2, Me.GridColumn12, Me.GridColumn14, Me.GridColumn16, Me.GridColumn17, Me.GridColumn15, Me.GridColumn18})
        Me.GridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.GridView1.GridControl = Me.grdFactures
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsBehavior.Editable = False
        Me.GridView1.OptionsCustomization.AllowGroup = False
        Me.GridView1.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridView1.OptionsSelection.UseIndicatorForSelection = False
        Me.GridView1.OptionsView.ShowAutoFilterRow = True
        Me.GridView1.OptionsView.ShowGroupPanel = False
        Me.GridView1.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.GridColumn5, DevExpress.Data.ColumnSortOrder.Ascending)})
        '
        'colIDInvoice
        '
        Me.colIDInvoice.Caption = "ID facture"
        Me.colIDInvoice.FieldName = "IDInvoice"
        Me.colIDInvoice.Name = "colIDInvoice"
        Me.colIDInvoice.Visible = True
        Me.colIDInvoice.VisibleIndex = 0
        Me.colIDInvoice.Width = 94
        '
        'GridColumn5
        '
        Me.GridColumn5.Caption = "Numéro de facture"
        Me.GridColumn5.FieldName = "Livreur"
        Me.GridColumn5.Name = "GridColumn5"
        Me.GridColumn5.Visible = True
        Me.GridColumn5.VisibleIndex = 2
        Me.GridColumn5.Width = 94
        '
        'colOrganisation
        '
        Me.colOrganisation.Caption = "Organisation"
        Me.colOrganisation.FieldName = "Organisation"
        Me.colOrganisation.Name = "colOrganisation"
        Me.colOrganisation.Visible = True
        Me.colOrganisation.VisibleIndex = 1
        '
        'ColDateFrom
        '
        Me.ColDateFrom.Caption = "Période du"
        Me.ColDateFrom.FieldName = "DateFrom"
        Me.ColDateFrom.Name = "ColDateFrom"
        Me.ColDateFrom.Visible = True
        Me.ColDateFrom.VisibleIndex = 4
        '
        'colDateTo
        '
        Me.colDateTo.Caption = "Période au"
        Me.colDateTo.FieldName = "DateTo"
        Me.colDateTo.Name = "colDateTo"
        Me.colDateTo.Visible = True
        Me.colDateTo.VisibleIndex = 5
        '
        'colInvoiceDate
        '
        Me.colInvoiceDate.Caption = "Date de facturation"
        Me.colInvoiceDate.FieldName = "InvoiceDate"
        Me.colInvoiceDate.Name = "colInvoiceDate"
        Me.colInvoiceDate.Visible = True
        Me.colInvoiceDate.VisibleIndex = 3
        '
        'colOrganisationID2
        '
        Me.colOrganisationID2.Caption = "OrganisationID"
        Me.colOrganisationID2.FieldName = "OrganisationID"
        Me.colOrganisationID2.Name = "colOrganisationID2"
        Me.colOrganisationID2.Visible = True
        Me.colOrganisationID2.VisibleIndex = 6
        '
        'GridColumn12
        '
        Me.GridColumn12.Caption = "IDPeriode"
        Me.GridColumn12.FieldName = "IDPeriode"
        Me.GridColumn12.Name = "GridColumn12"
        '
        'GridColumn14
        '
        Me.GridColumn14.Caption = "Montant"
        Me.GridColumn14.FieldName = "Amount"
        Me.GridColumn14.Name = "GridColumn14"
        Me.GridColumn14.Visible = True
        Me.GridColumn14.VisibleIndex = 7
        '
        'GridColumn16
        '
        Me.GridColumn16.Caption = "TPS"
        Me.GridColumn16.FieldName = "TPS"
        Me.GridColumn16.Name = "GridColumn16"
        Me.GridColumn16.Visible = True
        Me.GridColumn16.VisibleIndex = 8
        '
        'GridColumn17
        '
        Me.GridColumn17.Caption = "TVQ"
        Me.GridColumn17.FieldName = "TVQ"
        Me.GridColumn17.Name = "GridColumn17"
        Me.GridColumn17.Visible = True
        Me.GridColumn17.VisibleIndex = 9
        '
        'GridColumn15
        '
        Me.GridColumn15.Caption = "Balance a payer"
        Me.GridColumn15.DisplayFormat.FormatString = "c"
        Me.GridColumn15.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.GridColumn15.FieldName = "Total"
        Me.GridColumn15.Name = "GridColumn15"
        Me.GridColumn15.Visible = True
        Me.GridColumn15.VisibleIndex = 11
        '
        'GridColumn18
        '
        Me.GridColumn18.Caption = "Total"
        Me.GridColumn18.DisplayFormat.FormatString = "c"
        Me.GridColumn18.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.GridColumn18.FieldName = "MontantTotal"
        Me.GridColumn18.Name = "GridColumn18"
        Me.GridColumn18.Visible = True
        Me.GridColumn18.VisibleIndex = 10
        '
        'Root
        '
        Me.Root.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.Root.GroupBordersVisible = False
        Me.Root.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem2, Me.LayoutControlItem12, Me.LayoutControlItem21, Me.LayoutControlItem39, Me.EmptySpaceItem1, Me.EmptySpaceItem2})
        Me.Root.Name = "Root"
        Me.Root.Size = New System.Drawing.Size(1358, 621)
        Me.Root.TextVisible = False
        '
        'LayoutControlItem2
        '
        Me.LayoutControlItem2.Control = Me.grdFactures
        Me.LayoutControlItem2.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem2.Name = "LayoutControlItem2"
        Me.LayoutControlItem2.Size = New System.Drawing.Size(1338, 267)
        Me.LayoutControlItem2.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem2.TextVisible = False
        '
        'LayoutControlItem12
        '
        Me.LayoutControlItem12.Control = Me.grdPaye
        Me.LayoutControlItem12.Location = New System.Drawing.Point(0, 321)
        Me.LayoutControlItem12.Name = "LayoutControlItem12"
        Me.LayoutControlItem12.Size = New System.Drawing.Size(1338, 280)
        Me.LayoutControlItem12.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem12.TextVisible = False
        '
        'LayoutControlItem21
        '
        Me.LayoutControlItem21.Control = Me.LabelControl2
        Me.LayoutControlItem21.Location = New System.Drawing.Point(0, 267)
        Me.LayoutControlItem21.Name = "LayoutControlItem21"
        Me.LayoutControlItem21.Size = New System.Drawing.Size(1338, 28)
        Me.LayoutControlItem21.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem21.TextVisible = False
        '
        'LayoutControlItem39
        '
        Me.LayoutControlItem39.Control = Me.bDeletePaiement
        Me.LayoutControlItem39.Location = New System.Drawing.Point(602, 295)
        Me.LayoutControlItem39.Name = "LayoutControlItem39"
        Me.LayoutControlItem39.Size = New System.Drawing.Size(141, 26)
        Me.LayoutControlItem39.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem39.TextVisible = False
        '
        'EmptySpaceItem1
        '
        Me.EmptySpaceItem1.AllowHotTrack = False
        Me.EmptySpaceItem1.Location = New System.Drawing.Point(0, 295)
        Me.EmptySpaceItem1.Name = "EmptySpaceItem1"
        Me.EmptySpaceItem1.Size = New System.Drawing.Size(602, 26)
        Me.EmptySpaceItem1.TextSize = New System.Drawing.Size(0, 0)
        '
        'EmptySpaceItem2
        '
        Me.EmptySpaceItem2.AllowHotTrack = False
        Me.EmptySpaceItem2.Location = New System.Drawing.Point(743, 295)
        Me.EmptySpaceItem2.Name = "EmptySpaceItem2"
        Me.EmptySpaceItem2.Size = New System.Drawing.Size(595, 26)
        Me.EmptySpaceItem2.TextSize = New System.Drawing.Size(0, 0)
        '
        'XtraTabPage2
        '
        Me.XtraTabPage2.Controls.Add(Me.LayoutControl1)
        Me.XtraTabPage2.Name = "XtraTabPage2"
        Me.XtraTabPage2.PageVisible = False
        Me.XtraTabPage2.Size = New System.Drawing.Size(1358, 621)
        Me.XtraTabPage2.Text = "Détail sur une facture"
        '
        'LayoutControl1
        '
        Me.LayoutControl1.Controls.Add(Me.bExportToHTML)
        Me.LayoutControl1.Controls.Add(Me.bExportToPDF)
        Me.LayoutControl1.Controls.Add(Me.bExportToDoc)
        Me.LayoutControl1.Controls.Add(Me.bExportToExcel)
        Me.LayoutControl1.Controls.Add(Me.LabelControl1)
        Me.LayoutControl1.Controls.Add(Me.grdImport)
        Me.LayoutControl1.Controls.Add(Me.grdInvoiceLine)
        Me.LayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LayoutControl1.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControl1.Name = "LayoutControl1"
        Me.LayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = New System.Drawing.Rectangle(976, 223, 771, 350)
        Me.LayoutControl1.OptionsCustomizationForm.ShowPropertyGrid = True
        Me.LayoutControl1.Root = Me.LayoutControlGroup1
        Me.LayoutControl1.Size = New System.Drawing.Size(1358, 621)
        Me.LayoutControl1.TabIndex = 0
        Me.LayoutControl1.Text = "LayoutControl1"
        '
        'bExportToHTML
        '
        Me.bExportToHTML.Location = New System.Drawing.Point(1000, 244)
        Me.bExportToHTML.Name = "bExportToHTML"
        Me.bExportToHTML.Size = New System.Drawing.Size(346, 22)
        Me.bExportToHTML.StyleController = Me.LayoutControl1
        Me.bExportToHTML.TabIndex = 43
        Me.bExportToHTML.Text = "Exporter vers HTML"
        '
        'bExportToPDF
        '
        Me.bExportToPDF.Location = New System.Drawing.Point(318, 244)
        Me.bExportToPDF.Name = "bExportToPDF"
        Me.bExportToPDF.Size = New System.Drawing.Size(337, 22)
        Me.bExportToPDF.StyleController = Me.LayoutControl1
        Me.bExportToPDF.TabIndex = 42
        Me.bExportToPDF.Text = "Exporter vers PDF"
        '
        'bExportToDoc
        '
        Me.bExportToDoc.Location = New System.Drawing.Point(659, 244)
        Me.bExportToDoc.Name = "bExportToDoc"
        Me.bExportToDoc.Size = New System.Drawing.Size(337, 22)
        Me.bExportToDoc.StyleController = Me.LayoutControl1
        Me.bExportToDoc.TabIndex = 41
        Me.bExportToDoc.Text = "Exporter vers Doc"
        '
        'bExportToExcel
        '
        Me.bExportToExcel.Location = New System.Drawing.Point(12, 244)
        Me.bExportToExcel.Name = "bExportToExcel"
        Me.bExportToExcel.Size = New System.Drawing.Size(302, 22)
        Me.bExportToExcel.StyleController = Me.LayoutControl1
        Me.bExportToExcel.TabIndex = 40
        Me.bExportToExcel.Text = "Exporter vers Excel"
        '
        'LabelControl1
        '
        Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Tahoma", 15.0!)
        Me.LabelControl1.Appearance.Options.UseFont = True
        Me.LabelControl1.Appearance.Options.UseTextOptions = True
        Me.LabelControl1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.LabelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None
        Me.LabelControl1.Location = New System.Drawing.Point(12, 216)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(1334, 24)
        Me.LabelControl1.StyleController = Me.LayoutControl1
        Me.LabelControl1.TabIndex = 27
        Me.LabelControl1.Text = "Détails de la facture"
        '
        'grdImport
        '
        Me.grdImport.Location = New System.Drawing.Point(12, 270)
        Me.grdImport.MainView = Me.GridView3
        Me.grdImport.Name = "grdImport"
        Me.grdImport.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemCalcEdit1, Me.RepositoryItemTextEdit1, Me.RepositoryItemTextEdit2, Me.RepositoryItemMemoEdit1, Me.RepositoryItemMemoEdit2, Me.RepositoryItemMemoEdit3, Me.RepositoryItemCalcEdit2, Me.RepositoryItemSpinEdit1, Me.RepositoryItemCheckEdit1})
        Me.grdImport.Size = New System.Drawing.Size(1334, 339)
        Me.grdImport.TabIndex = 26
        Me.grdImport.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView3})
        '
        'GridView3
        '
        Me.GridView3.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colImportID, Me.GridColumn2, Me.colReference, Me.colClient, Me.colClientAddress, Me.colMomentDePassage, Me.colPreparePar, Me.colHeureAppel, Me.colHeurePrep, Me.colHeureDepart, Me.colHeureLivraison, Me.colMessagePassage, Me.colTotalTempService, Me.colLivreur, Me.colDistance, Me.GridColumn1, Me.GridColumn3, Me.GridColumn4, Me.GridColumn6, Me.GridColumn7, Me.colMontantGlacierLivreur, Me.colMontantGlacierOrganisation, Me.colMontantLivreur, Me.Extras, Me.colExtraKM})
        GridFormatRule2.ApplyToRow = True
        GridFormatRule2.Column = Me.colDistance
        GridFormatRule2.ColumnApplyTo = Me.colDistance
        GridFormatRule2.Name = "Format0"
        FormatConditionRuleValue2.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer))
        FormatConditionRuleValue2.Appearance.Options.UseBackColor = True
        FormatConditionRuleValue2.Condition = DevExpress.XtraEditors.FormatCondition.Equal
        FormatConditionRuleValue2.PredefinedName = "Red Fill"
        FormatConditionRuleValue2.Value1 = "0"
        GridFormatRule2.Rule = FormatConditionRuleValue2
        Me.GridView3.FormatRules.Add(GridFormatRule2)
        Me.GridView3.GridControl = Me.grdImport
        Me.GridView3.Name = "GridView3"
        Me.GridView3.OptionsCustomization.AllowGroup = False
        Me.GridView3.OptionsView.RowAutoHeight = True
        Me.GridView3.OptionsView.ShowAutoFilterRow = True
        Me.GridView3.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.ShowAlways
        Me.GridView3.OptionsView.ShowFooter = True
        Me.GridView3.OptionsView.ShowGroupPanel = False
        '
        'colImportID
        '
        Me.colImportID.AppearanceCell.Options.UseTextOptions = True
        Me.colImportID.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center
        Me.colImportID.Caption = "Import ID"
        Me.colImportID.FieldName = "ImportID"
        Me.colImportID.Name = "colImportID"
        Me.colImportID.OptionsColumn.AllowEdit = False
        Me.colImportID.OptionsColumn.AllowFocus = False
        Me.colImportID.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count, "ImportID", "{0}")})
        Me.colImportID.Visible = True
        Me.colImportID.VisibleIndex = 0
        Me.colImportID.Width = 36
        '
        'GridColumn2
        '
        Me.GridColumn2.Caption = "Code"
        Me.GridColumn2.FieldName = "Code"
        Me.GridColumn2.Name = "GridColumn2"
        Me.GridColumn2.OptionsColumn.AllowEdit = False
        Me.GridColumn2.OptionsColumn.AllowFocus = False
        Me.GridColumn2.Visible = True
        Me.GridColumn2.VisibleIndex = 1
        Me.GridColumn2.Width = 92
        '
        'colReference
        '
        Me.colReference.Caption = "Reference"
        Me.colReference.FieldName = "Reference"
        Me.colReference.Name = "colReference"
        Me.colReference.OptionsColumn.AllowFocus = False
        '
        'colClient
        '
        Me.colClient.AppearanceCell.Options.UseTextOptions = True
        Me.colClient.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center
        Me.colClient.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.colClient.Caption = "Client"
        Me.colClient.ColumnEdit = Me.RepositoryItemMemoEdit1
        Me.colClient.CustomizationCaption = "Client"
        Me.colClient.FieldName = "Client"
        Me.colClient.Name = "colClient"
        Me.colClient.OptionsColumn.AllowEdit = False
        Me.colClient.OptionsColumn.AllowFocus = False
        Me.colClient.Visible = True
        Me.colClient.VisibleIndex = 2
        Me.colClient.Width = 92
        '
        'RepositoryItemMemoEdit1
        '
        Me.RepositoryItemMemoEdit1.Name = "RepositoryItemMemoEdit1"
        '
        'colClientAddress
        '
        Me.colClientAddress.AppearanceCell.Options.UseTextOptions = True
        Me.colClientAddress.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center
        Me.colClientAddress.Caption = "ClientAddress"
        Me.colClientAddress.CustomizationCaption = "ClientAddress"
        Me.colClientAddress.FieldName = "ClientAddress"
        Me.colClientAddress.Name = "colClientAddress"
        Me.colClientAddress.OptionsColumn.AllowEdit = False
        Me.colClientAddress.OptionsColumn.AllowFocus = False
        '
        'colMomentDePassage
        '
        Me.colMomentDePassage.AppearanceCell.Options.UseTextOptions = True
        Me.colMomentDePassage.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center
        Me.colMomentDePassage.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.colMomentDePassage.Caption = "MomentDePassage"
        Me.colMomentDePassage.FieldName = "MomentDePassage"
        Me.colMomentDePassage.Name = "colMomentDePassage"
        Me.colMomentDePassage.OptionsColumn.AllowEdit = False
        Me.colMomentDePassage.OptionsColumn.AllowFocus = False
        '
        'colPreparePar
        '
        Me.colPreparePar.AppearanceCell.Options.UseTextOptions = True
        Me.colPreparePar.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center
        Me.colPreparePar.AppearanceHeader.Options.UseTextOptions = True
        Me.colPreparePar.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.colPreparePar.Caption = "Préparé par"
        Me.colPreparePar.CustomizationCaption = "Prepare Par"
        Me.colPreparePar.FieldName = "PreparePar"
        Me.colPreparePar.Name = "colPreparePar"
        Me.colPreparePar.OptionsColumn.AllowEdit = False
        Me.colPreparePar.OptionsColumn.AllowFocus = False
        '
        'colHeureAppel
        '
        Me.colHeureAppel.AppearanceCell.Options.UseTextOptions = True
        Me.colHeureAppel.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center
        Me.colHeureAppel.AppearanceHeader.Options.UseTextOptions = True
        Me.colHeureAppel.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.colHeureAppel.Caption = "HeureAppel"
        Me.colHeureAppel.DisplayFormat.FormatString = "g"
        Me.colHeureAppel.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.colHeureAppel.FieldName = "HeureAppel"
        Me.colHeureAppel.Name = "colHeureAppel"
        Me.colHeureAppel.OptionsColumn.AllowEdit = False
        Me.colHeureAppel.OptionsColumn.AllowFocus = False
        '
        'colHeurePrep
        '
        Me.colHeurePrep.AppearanceCell.Options.UseTextOptions = True
        Me.colHeurePrep.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center
        Me.colHeurePrep.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.colHeurePrep.AppearanceHeader.Options.UseTextOptions = True
        Me.colHeurePrep.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.colHeurePrep.Caption = "Préparation"
        Me.colHeurePrep.DisplayFormat.FormatString = "g"
        Me.colHeurePrep.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.colHeurePrep.FieldName = "HeurePrep"
        Me.colHeurePrep.Name = "colHeurePrep"
        Me.colHeurePrep.OptionsColumn.AllowEdit = False
        Me.colHeurePrep.OptionsColumn.AllowFocus = False
        Me.colHeurePrep.Visible = True
        Me.colHeurePrep.VisibleIndex = 3
        Me.colHeurePrep.Width = 92
        '
        'colHeureDepart
        '
        Me.colHeureDepart.AppearanceCell.Options.UseTextOptions = True
        Me.colHeureDepart.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center
        Me.colHeureDepart.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.colHeureDepart.AppearanceHeader.Options.UseTextOptions = True
        Me.colHeureDepart.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.colHeureDepart.Caption = "Départ"
        Me.colHeureDepart.DisplayFormat.FormatString = "g"
        Me.colHeureDepart.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.colHeureDepart.FieldName = "HeureDepart"
        Me.colHeureDepart.Name = "colHeureDepart"
        Me.colHeureDepart.OptionsColumn.AllowEdit = False
        Me.colHeureDepart.OptionsColumn.AllowFocus = False
        Me.colHeureDepart.Visible = True
        Me.colHeureDepart.VisibleIndex = 4
        Me.colHeureDepart.Width = 92
        '
        'colHeureLivraison
        '
        Me.colHeureLivraison.AppearanceCell.Options.UseTextOptions = True
        Me.colHeureLivraison.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center
        Me.colHeureLivraison.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.colHeureLivraison.AppearanceHeader.Options.UseTextOptions = True
        Me.colHeureLivraison.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.colHeureLivraison.Caption = "Livraison"
        Me.colHeureLivraison.DisplayFormat.FormatString = "g"
        Me.colHeureLivraison.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.colHeureLivraison.FieldName = "HeureLivraison"
        Me.colHeureLivraison.Name = "colHeureLivraison"
        Me.colHeureLivraison.OptionsColumn.AllowEdit = False
        Me.colHeureLivraison.OptionsColumn.AllowFocus = False
        Me.colHeureLivraison.Visible = True
        Me.colHeureLivraison.VisibleIndex = 5
        Me.colHeureLivraison.Width = 92
        '
        'colMessagePassage
        '
        Me.colMessagePassage.AppearanceCell.Options.UseTextOptions = True
        Me.colMessagePassage.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center
        Me.colMessagePassage.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.colMessagePassage.AppearanceHeader.Options.UseTextOptions = True
        Me.colMessagePassage.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.colMessagePassage.Caption = "Message"
        Me.colMessagePassage.ColumnEdit = Me.RepositoryItemMemoEdit3
        Me.colMessagePassage.FieldName = "MessagePassage"
        Me.colMessagePassage.Name = "colMessagePassage"
        Me.colMessagePassage.OptionsColumn.AllowEdit = False
        Me.colMessagePassage.OptionsColumn.AllowFocus = False
        Me.colMessagePassage.Visible = True
        Me.colMessagePassage.VisibleIndex = 6
        Me.colMessagePassage.Width = 92
        '
        'RepositoryItemMemoEdit3
        '
        Me.RepositoryItemMemoEdit3.Name = "RepositoryItemMemoEdit3"
        '
        'colTotalTempService
        '
        Me.colTotalTempService.AppearanceCell.Options.UseTextOptions = True
        Me.colTotalTempService.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center
        Me.colTotalTempService.Caption = "TotalTempService"
        Me.colTotalTempService.FieldName = "TotalTempService"
        Me.colTotalTempService.Name = "colTotalTempService"
        Me.colTotalTempService.OptionsColumn.AllowEdit = False
        Me.colTotalTempService.OptionsColumn.AllowFocus = False
        '
        'colLivreur
        '
        Me.colLivreur.AppearanceCell.Options.UseTextOptions = True
        Me.colLivreur.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center
        Me.colLivreur.Caption = "Livreur"
        Me.colLivreur.FieldName = "Livreur"
        Me.colLivreur.Name = "colLivreur"
        Me.colLivreur.OptionsColumn.AllowEdit = False
        Me.colLivreur.OptionsColumn.AllowFocus = False
        Me.colLivreur.Visible = True
        Me.colLivreur.VisibleIndex = 7
        Me.colLivreur.Width = 92
        '
        'GridColumn1
        '
        Me.GridColumn1.AppearanceCell.Options.UseTextOptions = True
        Me.GridColumn1.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center
        Me.GridColumn1.Caption = "Montant O."
        Me.GridColumn1.DisplayFormat.FormatString = "c"
        Me.GridColumn1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.GridColumn1.FieldName = "Montant"
        Me.GridColumn1.Name = "GridColumn1"
        Me.GridColumn1.OptionsColumn.AllowEdit = False
        Me.GridColumn1.OptionsColumn.AllowFocus = False
        Me.GridColumn1.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Montant", "SUM={0:c2}")})
        Me.GridColumn1.Visible = True
        Me.GridColumn1.VisibleIndex = 9
        Me.GridColumn1.Width = 92
        '
        'GridColumn3
        '
        Me.GridColumn3.AppearanceCell.Options.UseTextOptions = True
        Me.GridColumn3.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center
        Me.GridColumn3.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.GridColumn3.Caption = "Organisation"
        Me.GridColumn3.ColumnEdit = Me.RepositoryItemMemoEdit2
        Me.GridColumn3.FieldName = "Organisation"
        Me.GridColumn3.Name = "GridColumn3"
        Me.GridColumn3.OptionsColumn.AllowEdit = False
        Me.GridColumn3.OptionsColumn.AllowFocus = False
        Me.GridColumn3.Visible = True
        Me.GridColumn3.VisibleIndex = 8
        Me.GridColumn3.Width = 92
        '
        'RepositoryItemMemoEdit2
        '
        Me.RepositoryItemMemoEdit2.Name = "RepositoryItemMemoEdit2"
        '
        'GridColumn4
        '
        Me.GridColumn4.AppearanceCell.Options.UseTextOptions = True
        Me.GridColumn4.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center
        Me.GridColumn4.Caption = "Débit"
        Me.GridColumn4.DisplayFormat.FormatString = "c"
        Me.GridColumn4.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.GridColumn4.FieldName = "Debit"
        Me.GridColumn4.Name = "GridColumn4"
        Me.GridColumn4.OptionsColumn.AllowEdit = False
        Me.GridColumn4.OptionsColumn.AllowFocus = False
        Me.GridColumn4.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Extra", "SUM={0:c2}")})
        Me.GridColumn4.Visible = True
        Me.GridColumn4.VisibleIndex = 17
        Me.GridColumn4.Width = 92
        '
        'GridColumn6
        '
        Me.GridColumn6.AppearanceCell.Options.UseTextOptions = True
        Me.GridColumn6.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center
        Me.GridColumn6.Caption = "Crédit"
        Me.GridColumn6.DisplayFormat.FormatString = "c"
        Me.GridColumn6.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.GridColumn6.FieldName = "Credit"
        Me.GridColumn6.Name = "GridColumn6"
        Me.GridColumn6.OptionsColumn.AllowEdit = False
        Me.GridColumn6.OptionsColumn.AllowFocus = False
        Me.GridColumn6.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Credit", "SUM={0:c2}")})
        Me.GridColumn6.Visible = True
        Me.GridColumn6.VisibleIndex = 18
        Me.GridColumn6.Width = 92
        '
        'GridColumn7
        '
        Me.GridColumn7.Caption = "# Glacier"
        Me.GridColumn7.FieldName = "NombreGlacier"
        Me.GridColumn7.Name = "GridColumn7"
        Me.GridColumn7.OptionsColumn.AllowEdit = False
        Me.GridColumn7.OptionsColumn.AllowFocus = False
        Me.GridColumn7.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "NombreGlacier", "SUM={0:0.##}")})
        Me.GridColumn7.Visible = True
        Me.GridColumn7.VisibleIndex = 11
        Me.GridColumn7.Width = 92
        '
        'colMontantGlacierLivreur
        '
        Me.colMontantGlacierLivreur.Caption = "Glacier L."
        Me.colMontantGlacierLivreur.DisplayFormat.FormatString = "c"
        Me.colMontantGlacierLivreur.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.colMontantGlacierLivreur.FieldName = "MontantGlacierLivreur"
        Me.colMontantGlacierLivreur.Name = "colMontantGlacierLivreur"
        Me.colMontantGlacierLivreur.OptionsColumn.AllowEdit = False
        Me.colMontantGlacierLivreur.OptionsColumn.AllowFocus = False
        Me.colMontantGlacierLivreur.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "MontantGlacierLivreur", "SUM={0:0.##}")})
        Me.colMontantGlacierLivreur.Visible = True
        Me.colMontantGlacierLivreur.VisibleIndex = 12
        Me.colMontantGlacierLivreur.Width = 92
        '
        'colMontantGlacierOrganisation
        '
        Me.colMontantGlacierOrganisation.Caption = "Glacier O."
        Me.colMontantGlacierOrganisation.DisplayFormat.FormatString = "c"
        Me.colMontantGlacierOrganisation.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.colMontantGlacierOrganisation.FieldName = "MontantGlacierOrganisation"
        Me.colMontantGlacierOrganisation.Name = "colMontantGlacierOrganisation"
        Me.colMontantGlacierOrganisation.OptionsColumn.AllowEdit = False
        Me.colMontantGlacierOrganisation.OptionsColumn.AllowFocus = False
        Me.colMontantGlacierOrganisation.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "MontantGlacierOrganisation", "SUM={0:0.##}")})
        Me.colMontantGlacierOrganisation.Visible = True
        Me.colMontantGlacierOrganisation.VisibleIndex = 13
        Me.colMontantGlacierOrganisation.Width = 92
        '
        'colMontantLivreur
        '
        Me.colMontantLivreur.Caption = "Montant L."
        Me.colMontantLivreur.DisplayFormat.FormatString = "c"
        Me.colMontantLivreur.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.colMontantLivreur.FieldName = "MontantLivreur"
        Me.colMontantLivreur.Name = "colMontantLivreur"
        Me.colMontantLivreur.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "MontantLivreur", "SUM={0:0.##}")})
        Me.colMontantLivreur.Visible = True
        Me.colMontantLivreur.VisibleIndex = 10
        Me.colMontantLivreur.Width = 104
        '
        'Extras
        '
        Me.Extras.Caption = "Extra"
        Me.Extras.DisplayFormat.FormatString = "c"
        Me.Extras.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.Extras.FieldName = "Extra"
        Me.Extras.Name = "Extras"
        Me.Extras.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Extra", "SUM={0:0.##}")})
        Me.Extras.Visible = True
        Me.Extras.VisibleIndex = 15
        '
        'colExtraKM
        '
        Me.colExtraKM.Caption = "Extra KM"
        Me.colExtraKM.FieldName = "ExtraKM"
        Me.colExtraKM.Name = "colExtraKM"
        Me.colExtraKM.Summary.AddRange(New DevExpress.XtraGrid.GridSummaryItem() {New DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "ExtraKM", "SUM={0:0.##}")})
        Me.colExtraKM.Visible = True
        Me.colExtraKM.VisibleIndex = 16
        '
        'RepositoryItemCalcEdit1
        '
        Me.RepositoryItemCalcEdit1.AutoHeight = False
        Me.RepositoryItemCalcEdit1.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemCalcEdit1.Name = "RepositoryItemCalcEdit1"
        '
        'RepositoryItemTextEdit1
        '
        Me.RepositoryItemTextEdit1.AutoHeight = False
        Me.RepositoryItemTextEdit1.Name = "RepositoryItemTextEdit1"
        '
        'RepositoryItemTextEdit2
        '
        Me.RepositoryItemTextEdit2.AutoHeight = False
        Me.RepositoryItemTextEdit2.Name = "RepositoryItemTextEdit2"
        '
        'RepositoryItemCalcEdit2
        '
        Me.RepositoryItemCalcEdit2.AutoHeight = False
        Me.RepositoryItemCalcEdit2.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemCalcEdit2.Name = "RepositoryItemCalcEdit2"
        '
        'RepositoryItemSpinEdit1
        '
        Me.RepositoryItemSpinEdit1.AutoHeight = False
        Me.RepositoryItemSpinEdit1.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemSpinEdit1.Name = "RepositoryItemSpinEdit1"
        '
        'RepositoryItemCheckEdit1
        '
        Me.RepositoryItemCheckEdit1.AutoHeight = False
        Me.RepositoryItemCheckEdit1.Name = "RepositoryItemCheckEdit1"
        '
        'grdInvoiceLine
        '
        Me.grdInvoiceLine.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grdInvoiceLine.Location = New System.Drawing.Point(12, 12)
        Me.grdInvoiceLine.MainView = Me.GridView2
        Me.grdInvoiceLine.Name = "grdInvoiceLine"
        Me.grdInvoiceLine.Size = New System.Drawing.Size(1334, 200)
        Me.grdInvoiceLine.TabIndex = 4
        Me.grdInvoiceLine.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView2})
        '
        'GridView2
        '
        Me.GridView2.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colIDInvoiceLine, Me.colInvoiceNumber, Me.colInvoiceDate2, Me.colOrganisationID, Me.colOrganisation2, Me.colDateFrom2, Me.colDateTo2, Me.colDescription, Me.colUnite, Me.colPrix, Me.colMontant, Me.colCredit, Me.colDebit, Me.colNombreGlacier, Me.colGlacier, Me.colDisplayDate})
        Me.GridView2.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.GridView2.GridControl = Me.grdInvoiceLine
        Me.GridView2.Name = "GridView2"
        Me.GridView2.OptionsBehavior.Editable = False
        Me.GridView2.OptionsCustomization.AllowGroup = False
        Me.GridView2.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridView2.OptionsSelection.UseIndicatorForSelection = False
        Me.GridView2.OptionsView.ShowAutoFilterRow = True
        Me.GridView2.OptionsView.ShowGroupPanel = False
        Me.GridView2.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.colInvoiceNumber, DevExpress.Data.ColumnSortOrder.Ascending)})
        '
        'colIDInvoiceLine
        '
        Me.colIDInvoiceLine.Caption = "ID Facture ligne"
        Me.colIDInvoiceLine.FieldName = "IDInvoiceLine"
        Me.colIDInvoiceLine.Name = "colIDInvoiceLine"
        Me.colIDInvoiceLine.Width = 94
        '
        'colInvoiceNumber
        '
        Me.colInvoiceNumber.Caption = "Numéro de facture"
        Me.colInvoiceNumber.FieldName = "InvoiceNumber"
        Me.colInvoiceNumber.Name = "colInvoiceNumber"
        Me.colInvoiceNumber.Visible = True
        Me.colInvoiceNumber.VisibleIndex = 0
        Me.colInvoiceNumber.Width = 94
        '
        'colInvoiceDate2
        '
        Me.colInvoiceDate2.Caption = "Date de facturation"
        Me.colInvoiceDate2.FieldName = "InvoiceDate"
        Me.colInvoiceDate2.Name = "colInvoiceDate2"
        '
        'colOrganisationID
        '
        Me.colOrganisationID.Caption = "OrganisationID"
        Me.colOrganisationID.FieldName = "OrganisationID"
        Me.colOrganisationID.Name = "colOrganisationID"
        '
        'colOrganisation2
        '
        Me.colOrganisation2.Caption = "Organisation"
        Me.colOrganisation2.FieldName = "Organisation"
        Me.colOrganisation2.Name = "colOrganisation2"
        Me.colOrganisation2.Visible = True
        Me.colOrganisation2.VisibleIndex = 1
        '
        'colDateFrom2
        '
        Me.colDateFrom2.Caption = "Période du"
        Me.colDateFrom2.FieldName = "DateFrom"
        Me.colDateFrom2.Name = "colDateFrom2"
        Me.colDateFrom2.Width = 94
        '
        'colDateTo2
        '
        Me.colDateTo2.Caption = "Période au"
        Me.colDateTo2.FieldName = "DateTo"
        Me.colDateTo2.Name = "colDateTo2"
        '
        'colDescription
        '
        Me.colDescription.Caption = "Description"
        Me.colDescription.FieldName = "Description"
        Me.colDescription.Name = "colDescription"
        Me.colDescription.Visible = True
        Me.colDescription.VisibleIndex = 3
        '
        'colUnite
        '
        Me.colUnite.Caption = "Unité"
        Me.colUnite.FieldName = "Unite"
        Me.colUnite.Name = "colUnite"
        Me.colUnite.Visible = True
        Me.colUnite.VisibleIndex = 4
        '
        'colPrix
        '
        Me.colPrix.Caption = "Prix"
        Me.colPrix.DisplayFormat.FormatString = "c"
        Me.colPrix.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.colPrix.FieldName = "Prix"
        Me.colPrix.Name = "colPrix"
        Me.colPrix.Visible = True
        Me.colPrix.VisibleIndex = 5
        '
        'colMontant
        '
        Me.colMontant.Caption = "Montant"
        Me.colMontant.DisplayFormat.FormatString = "c"
        Me.colMontant.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.colMontant.FieldName = "Montant"
        Me.colMontant.Name = "colMontant"
        Me.colMontant.Visible = True
        Me.colMontant.VisibleIndex = 6
        '
        'colCredit
        '
        Me.colCredit.Caption = "Crédits"
        Me.colCredit.DisplayFormat.FormatString = "c"
        Me.colCredit.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.colCredit.FieldName = "Credit"
        Me.colCredit.Name = "colCredit"
        Me.colCredit.Visible = True
        Me.colCredit.VisibleIndex = 7
        '
        'colDebit
        '
        Me.colDebit.Caption = "Débits"
        Me.colDebit.DisplayFormat.FormatString = "c"
        Me.colDebit.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.colDebit.FieldName = "Debit"
        Me.colDebit.Name = "colDebit"
        Me.colDebit.Visible = True
        Me.colDebit.VisibleIndex = 8
        '
        'colNombreGlacier
        '
        Me.colNombreGlacier.Caption = "Nombre Glacier"
        Me.colNombreGlacier.FieldName = "NombreGlacier"
        Me.colNombreGlacier.Name = "colNombreGlacier"
        Me.colNombreGlacier.Visible = True
        Me.colNombreGlacier.VisibleIndex = 9
        '
        'colGlacier
        '
        Me.colGlacier.Caption = "Total glaciers"
        Me.colGlacier.DisplayFormat.FormatString = "c"
        Me.colGlacier.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.colGlacier.FieldName = "Glacier"
        Me.colGlacier.Name = "colGlacier"
        Me.colGlacier.Visible = True
        Me.colGlacier.VisibleIndex = 10
        '
        'colDisplayDate
        '
        Me.colDisplayDate.Caption = "Display Date"
        Me.colDisplayDate.FieldName = "DisplayDate"
        Me.colDisplayDate.Name = "colDisplayDate"
        Me.colDisplayDate.Visible = True
        Me.colDisplayDate.VisibleIndex = 2
        '
        'LayoutControlGroup1
        '
        Me.LayoutControlGroup1.CustomizationFormText = "LayoutControlGroup1"
        Me.LayoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup1.GroupBordersVisible = False
        Me.LayoutControlGroup1.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem1, Me.LayoutControlItem3, Me.LayoutControlItem4, Me.LayoutControlItem8, Me.LayoutControlItem6, Me.LayoutControlItem7, Me.LayoutControlItem5})
        Me.LayoutControlGroup1.Name = "Root"
        Me.LayoutControlGroup1.Size = New System.Drawing.Size(1358, 621)
        Me.LayoutControlGroup1.TextVisible = False
        '
        'LayoutControlItem1
        '
        Me.LayoutControlItem1.Control = Me.grdInvoiceLine
        Me.LayoutControlItem1.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem1.Name = "LayoutControlItem1"
        Me.LayoutControlItem1.Size = New System.Drawing.Size(1338, 204)
        Me.LayoutControlItem1.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem1.TextVisible = False
        '
        'LayoutControlItem3
        '
        Me.LayoutControlItem3.Control = Me.grdImport
        Me.LayoutControlItem3.Location = New System.Drawing.Point(0, 258)
        Me.LayoutControlItem3.Name = "LayoutControlItem3"
        Me.LayoutControlItem3.Size = New System.Drawing.Size(1338, 343)
        Me.LayoutControlItem3.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem3.TextVisible = False
        '
        'LayoutControlItem4
        '
        Me.LayoutControlItem4.Control = Me.LabelControl1
        Me.LayoutControlItem4.Location = New System.Drawing.Point(0, 204)
        Me.LayoutControlItem4.Name = "LayoutControlItem4"
        Me.LayoutControlItem4.Size = New System.Drawing.Size(1338, 28)
        Me.LayoutControlItem4.Text = "Détails de la facture"
        Me.LayoutControlItem4.TextLocation = DevExpress.Utils.Locations.Top
        Me.LayoutControlItem4.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem4.TextVisible = False
        '
        'LayoutControlItem8
        '
        Me.LayoutControlItem8.Control = Me.bExportToDoc
        Me.LayoutControlItem8.Location = New System.Drawing.Point(647, 232)
        Me.LayoutControlItem8.Name = "LayoutControlItem8"
        Me.LayoutControlItem8.Size = New System.Drawing.Size(341, 26)
        Me.LayoutControlItem8.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem8.TextVisible = False
        '
        'LayoutControlItem6
        '
        Me.LayoutControlItem6.Control = Me.bExportToHTML
        Me.LayoutControlItem6.Location = New System.Drawing.Point(988, 232)
        Me.LayoutControlItem6.Name = "LayoutControlItem6"
        Me.LayoutControlItem6.Size = New System.Drawing.Size(350, 26)
        Me.LayoutControlItem6.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem6.TextVisible = False
        '
        'LayoutControlItem7
        '
        Me.LayoutControlItem7.Control = Me.bExportToPDF
        Me.LayoutControlItem7.Location = New System.Drawing.Point(306, 232)
        Me.LayoutControlItem7.Name = "LayoutControlItem7"
        Me.LayoutControlItem7.Size = New System.Drawing.Size(341, 26)
        Me.LayoutControlItem7.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem7.TextVisible = False
        '
        'LayoutControlItem5
        '
        Me.LayoutControlItem5.Control = Me.bExportToExcel
        Me.LayoutControlItem5.Location = New System.Drawing.Point(0, 232)
        Me.LayoutControlItem5.Name = "LayoutControlItem5"
        Me.LayoutControlItem5.Size = New System.Drawing.Size(306, 26)
        Me.LayoutControlItem5.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem5.TextVisible = False
        '
        'DockManager1
        '
        Me.DockManager1.Form = Me
        Me.DockManager1.HiddenPanels.AddRange(New DevExpress.XtraBars.Docking.DockPanel() {Me.DockPaiement})
        Me.DockManager1.RootPanels.AddRange(New DevExpress.XtraBars.Docking.DockPanel() {Me.DockLigneFacture, Me.DockNouvelleFacture})
        Me.DockManager1.TopZIndexControls.AddRange(New String() {"DevExpress.XtraBars.BarDockControl", "DevExpress.XtraBars.StandaloneBarDockControl", "System.Windows.Forms.MenuStrip", "System.Windows.Forms.StatusStrip", "System.Windows.Forms.StatusBar", "DevExpress.XtraBars.Ribbon.RibbonStatusBar", "DevExpress.XtraBars.Ribbon.RibbonControl", "DevExpress.XtraBars.Navigation.OfficeNavigationBar", "DevExpress.XtraBars.Navigation.TileNavPane", "DevExpress.XtraBars.TabFormControl", "DevExpress.XtraBars.FluentDesignSystem.FluentDesignFormControl", "DevExpress.XtraBars.ToolbarForm.ToolbarFormControl"})
        '
        'DockPaiement
        '
        Me.DockPaiement.Controls.Add(Me.ControlContainer2)
        Me.DockPaiement.Dock = DevExpress.XtraBars.Docking.DockingStyle.Float
        Me.DockPaiement.FloatLocation = New System.Drawing.Point(540, 376)
        Me.DockPaiement.FloatSize = New System.Drawing.Size(444, 314)
        Me.DockPaiement.ID = New System.Guid("2da52160-95a2-4b56-af39-e8ac00609777")
        Me.DockPaiement.Location = New System.Drawing.Point(-32768, -32768)
        Me.DockPaiement.Name = "DockPaiement"
        Me.DockPaiement.OriginalSize = New System.Drawing.Size(315, 200)
        Me.DockPaiement.SavedIndex = 0
        Me.DockPaiement.Size = New System.Drawing.Size(444, 314)
        Me.DockPaiement.Text = "Faire un paiement"
        Me.DockPaiement.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Hidden
        '
        'ControlContainer2
        '
        Me.ControlContainer2.Controls.Add(Me.LayoutControl5)
        Me.ControlContainer2.Location = New System.Drawing.Point(4, 30)
        Me.ControlContainer2.Name = "ControlContainer2"
        Me.ControlContainer2.Size = New System.Drawing.Size(436, 280)
        Me.ControlContainer2.TabIndex = 0
        '
        'LayoutControl5
        '
        Me.LayoutControl5.Controls.Add(Me.bFermerPaiement)
        Me.LayoutControl5.Controls.Add(Me.bSauvegarderPaiement)
        Me.LayoutControl5.Controls.Add(Me.tbIDFactureP)
        Me.LayoutControl5.Controls.Add(Me.cbPeriodesP)
        Me.LayoutControl5.Controls.Add(Me.tbInvoiceNumberP)
        Me.LayoutControl5.Controls.Add(Me.tbDateFromP)
        Me.LayoutControl5.Controls.Add(Me.tbDateAuP)
        Me.LayoutControl5.Controls.Add(Me.cbOrganisationP)
        Me.LayoutControl5.Controls.Add(Me.tbMontantPaiement)
        Me.LayoutControl5.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LayoutControl5.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControl5.Name = "LayoutControl5"
        Me.LayoutControl5.Root = Me.LayoutControlGroup4
        Me.LayoutControl5.Size = New System.Drawing.Size(436, 280)
        Me.LayoutControl5.TabIndex = 0
        Me.LayoutControl5.Text = "LayoutControl5"
        '
        'bFermerPaiement
        '
        Me.bFermerPaiement.Location = New System.Drawing.Point(220, 182)
        Me.bFermerPaiement.Name = "bFermerPaiement"
        Me.bFermerPaiement.Size = New System.Drawing.Size(204, 22)
        Me.bFermerPaiement.StyleController = Me.LayoutControl5
        Me.bFermerPaiement.TabIndex = 141
        Me.bFermerPaiement.Text = "Fermer"
        '
        'bSauvegarderPaiement
        '
        Me.bSauvegarderPaiement.Location = New System.Drawing.Point(12, 182)
        Me.bSauvegarderPaiement.Name = "bSauvegarderPaiement"
        Me.bSauvegarderPaiement.Size = New System.Drawing.Size(204, 22)
        Me.bSauvegarderPaiement.StyleController = Me.LayoutControl5
        Me.bSauvegarderPaiement.TabIndex = 140
        Me.bSauvegarderPaiement.Text = "Sauvegarder"
        '
        'tbIDFactureP
        '
        Me.tbIDFactureP.EditValue = "0"
        Me.tbIDFactureP.Enabled = False
        Me.tbIDFactureP.Location = New System.Drawing.Point(105, 12)
        Me.tbIDFactureP.Name = "tbIDFactureP"
        Me.tbIDFactureP.Properties.Appearance.BackColor = System.Drawing.Color.Linen
        Me.tbIDFactureP.Properties.Appearance.Options.UseBackColor = True
        Me.tbIDFactureP.Properties.Mask.EditMask = "n0"
        Me.tbIDFactureP.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.tbIDFactureP.Size = New System.Drawing.Size(319, 20)
        Me.tbIDFactureP.StyleController = Me.LayoutControl5
        Me.tbIDFactureP.TabIndex = 132
        '
        'cbPeriodesP
        '
        Me.cbPeriodesP.Enabled = False
        Me.cbPeriodesP.Location = New System.Drawing.Point(105, 36)
        Me.cbPeriodesP.Name = "cbPeriodesP"
        Me.cbPeriodesP.Properties.Appearance.BackColor = System.Drawing.Color.Linen
        Me.cbPeriodesP.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbPeriodesP.Properties.Appearance.Options.UseBackColor = True
        Me.cbPeriodesP.Properties.Appearance.Options.UseFont = True
        Me.cbPeriodesP.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbPeriodesP.Size = New System.Drawing.Size(319, 22)
        Me.cbPeriodesP.StyleController = Me.LayoutControl5
        Me.cbPeriodesP.TabIndex = 28
        '
        'tbInvoiceNumberP
        '
        Me.tbInvoiceNumberP.Enabled = False
        Me.tbInvoiceNumberP.Location = New System.Drawing.Point(105, 110)
        Me.tbInvoiceNumberP.Name = "tbInvoiceNumberP"
        Me.tbInvoiceNumberP.Properties.Appearance.BackColor = System.Drawing.Color.Linen
        Me.tbInvoiceNumberP.Properties.Appearance.Options.UseBackColor = True
        Me.tbInvoiceNumberP.Size = New System.Drawing.Size(319, 20)
        Me.tbInvoiceNumberP.StyleController = Me.LayoutControl5
        Me.tbInvoiceNumberP.TabIndex = 4
        '
        'tbDateFromP
        '
        Me.tbDateFromP.EditValue = Nothing
        Me.tbDateFromP.Enabled = False
        Me.tbDateFromP.Location = New System.Drawing.Point(105, 62)
        Me.tbDateFromP.Name = "tbDateFromP"
        Me.tbDateFromP.Properties.Appearance.BackColor = System.Drawing.Color.Linen
        Me.tbDateFromP.Properties.Appearance.Options.UseBackColor = True
        Me.tbDateFromP.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.tbDateFromP.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.tbDateFromP.Properties.DisplayFormat.FormatString = "MM-dd-yy H:mm"
        Me.tbDateFromP.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.tbDateFromP.Properties.EditFormat.FormatString = "MM-dd-yy H:mm"
        Me.tbDateFromP.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.tbDateFromP.Properties.Mask.EditMask = "MM-dd-yy H:mm"
        Me.tbDateFromP.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.tbDateFromP.Size = New System.Drawing.Size(319, 20)
        Me.tbDateFromP.StyleController = Me.LayoutControl5
        Me.tbDateFromP.TabIndex = 22
        '
        'tbDateAuP
        '
        Me.tbDateAuP.EditValue = Nothing
        Me.tbDateAuP.Enabled = False
        Me.tbDateAuP.Location = New System.Drawing.Point(105, 86)
        Me.tbDateAuP.Name = "tbDateAuP"
        Me.tbDateAuP.Properties.Appearance.BackColor = System.Drawing.Color.Linen
        Me.tbDateAuP.Properties.Appearance.Options.UseBackColor = True
        Me.tbDateAuP.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.tbDateAuP.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.tbDateAuP.Properties.DisplayFormat.FormatString = "MM-dd-yy H:mm"
        Me.tbDateAuP.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.tbDateAuP.Properties.EditFormat.FormatString = "MM-dd-yy H:mm"
        Me.tbDateAuP.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.tbDateAuP.Properties.Mask.EditMask = "MM-dd-yy H:mm"
        Me.tbDateAuP.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.tbDateAuP.Size = New System.Drawing.Size(319, 20)
        Me.tbDateAuP.StyleController = Me.LayoutControl5
        Me.tbDateAuP.TabIndex = 22
        '
        'cbOrganisationP
        '
        Me.cbOrganisationP.Enabled = False
        Me.cbOrganisationP.Location = New System.Drawing.Point(105, 134)
        Me.cbOrganisationP.Name = "cbOrganisationP"
        Me.cbOrganisationP.Properties.Appearance.BackColor = System.Drawing.Color.Linen
        Me.cbOrganisationP.Properties.Appearance.Options.UseBackColor = True
        Me.cbOrganisationP.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbOrganisationP.Size = New System.Drawing.Size(319, 20)
        Me.cbOrganisationP.StyleController = Me.LayoutControl5
        Me.cbOrganisationP.TabIndex = 36
        '
        'tbMontantPaiement
        '
        Me.tbMontantPaiement.EditValue = "$0.00"
        Me.tbMontantPaiement.Location = New System.Drawing.Point(105, 158)
        Me.tbMontantPaiement.Name = "tbMontantPaiement"
        Me.tbMontantPaiement.Properties.DisplayFormat.FormatString = "c"
        Me.tbMontantPaiement.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.tbMontantPaiement.Properties.Mask.EditMask = "c"
        Me.tbMontantPaiement.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.tbMontantPaiement.Size = New System.Drawing.Size(319, 20)
        Me.tbMontantPaiement.StyleController = Me.LayoutControl5
        Me.tbMontantPaiement.TabIndex = 139
        '
        'LayoutControlGroup4
        '
        Me.LayoutControlGroup4.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup4.GroupBordersVisible = False
        Me.LayoutControlGroup4.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem30, Me.LayoutControlItem31, Me.LayoutControlItem32, Me.LayoutControlItem33, Me.LayoutControlItem34, Me.LayoutControlItem35, Me.LayoutControlItem36, Me.LayoutControlItem37, Me.LayoutControlItem38})
        Me.LayoutControlGroup4.Name = "LayoutControlGroup4"
        Me.LayoutControlGroup4.Size = New System.Drawing.Size(436, 280)
        Me.LayoutControlGroup4.TextVisible = False
        '
        'LayoutControlItem30
        '
        Me.LayoutControlItem30.Control = Me.tbIDFactureP
        Me.LayoutControlItem30.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem30.CustomizationFormText = "Nombre maximum free echange (305 et 35)"
        Me.LayoutControlItem30.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem30.Name = "LayoutControlItem30"
        Me.LayoutControlItem30.Size = New System.Drawing.Size(416, 24)
        Me.LayoutControlItem30.Text = "ID Facture"
        Me.LayoutControlItem30.TextSize = New System.Drawing.Size(90, 13)
        '
        'LayoutControlItem31
        '
        Me.LayoutControlItem31.Control = Me.cbPeriodesP
        Me.LayoutControlItem31.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem31.CustomizationFormText = "Périodes"
        Me.LayoutControlItem31.Location = New System.Drawing.Point(0, 24)
        Me.LayoutControlItem31.Name = "LayoutControlItem31"
        Me.LayoutControlItem31.Size = New System.Drawing.Size(416, 26)
        Me.LayoutControlItem31.Text = "Période"
        Me.LayoutControlItem31.TextSize = New System.Drawing.Size(90, 13)
        '
        'LayoutControlItem32
        '
        Me.LayoutControlItem32.Control = Me.tbInvoiceNumberP
        Me.LayoutControlItem32.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem32.CustomizationFormText = "Numéro de paye"
        Me.LayoutControlItem32.Location = New System.Drawing.Point(0, 98)
        Me.LayoutControlItem32.Name = "LayoutControlItem32"
        Me.LayoutControlItem32.Size = New System.Drawing.Size(416, 24)
        Me.LayoutControlItem32.Text = "Numéro de facture"
        Me.LayoutControlItem32.TextSize = New System.Drawing.Size(90, 13)
        '
        'LayoutControlItem33
        '
        Me.LayoutControlItem33.Control = Me.tbDateFromP
        Me.LayoutControlItem33.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem33.CustomizationFormText = "Date du"
        Me.LayoutControlItem33.Location = New System.Drawing.Point(0, 50)
        Me.LayoutControlItem33.Name = "LayoutControlItem33"
        Me.LayoutControlItem33.Size = New System.Drawing.Size(416, 24)
        Me.LayoutControlItem33.Text = "Date du"
        Me.LayoutControlItem33.TextSize = New System.Drawing.Size(90, 13)
        '
        'LayoutControlItem34
        '
        Me.LayoutControlItem34.Control = Me.tbDateAuP
        Me.LayoutControlItem34.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem34.CustomizationFormText = "Date du"
        Me.LayoutControlItem34.Location = New System.Drawing.Point(0, 74)
        Me.LayoutControlItem34.Name = "LayoutControlItem34"
        Me.LayoutControlItem34.Size = New System.Drawing.Size(416, 24)
        Me.LayoutControlItem34.Text = "Date au"
        Me.LayoutControlItem34.TextSize = New System.Drawing.Size(90, 13)
        '
        'LayoutControlItem35
        '
        Me.LayoutControlItem35.Control = Me.cbOrganisationP
        Me.LayoutControlItem35.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem35.CustomizationFormText = "Liste des factures"
        Me.LayoutControlItem35.Location = New System.Drawing.Point(0, 122)
        Me.LayoutControlItem35.Name = "LayoutControlItem35"
        Me.LayoutControlItem35.Size = New System.Drawing.Size(416, 24)
        Me.LayoutControlItem35.Text = "Organisation"
        Me.LayoutControlItem35.TextSize = New System.Drawing.Size(90, 13)
        '
        'LayoutControlItem36
        '
        Me.LayoutControlItem36.Control = Me.tbMontantPaiement
        Me.LayoutControlItem36.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem36.CustomizationFormText = "lblPrixI"
        Me.LayoutControlItem36.Location = New System.Drawing.Point(0, 146)
        Me.LayoutControlItem36.Name = "LayoutControlItem36"
        Me.LayoutControlItem36.Size = New System.Drawing.Size(416, 24)
        Me.LayoutControlItem36.Text = "Montant"
        Me.LayoutControlItem36.TextSize = New System.Drawing.Size(90, 13)
        '
        'LayoutControlItem37
        '
        Me.LayoutControlItem37.Control = Me.bSauvegarderPaiement
        Me.LayoutControlItem37.Location = New System.Drawing.Point(0, 170)
        Me.LayoutControlItem37.Name = "LayoutControlItem37"
        Me.LayoutControlItem37.Size = New System.Drawing.Size(208, 90)
        Me.LayoutControlItem37.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem37.TextVisible = False
        '
        'LayoutControlItem38
        '
        Me.LayoutControlItem38.Control = Me.bFermerPaiement
        Me.LayoutControlItem38.Location = New System.Drawing.Point(208, 170)
        Me.LayoutControlItem38.Name = "LayoutControlItem38"
        Me.LayoutControlItem38.Size = New System.Drawing.Size(208, 90)
        Me.LayoutControlItem38.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem38.TextVisible = False
        '
        'DockLigneFacture
        '
        Me.DockLigneFacture.Controls.Add(Me.ControlContainer1)
        Me.DockLigneFacture.Dock = DevExpress.XtraBars.Docking.DockingStyle.Float
        Me.DockLigneFacture.FloatLocation = New System.Drawing.Point(68, 495)
        Me.DockLigneFacture.FloatSize = New System.Drawing.Size(608, 465)
        Me.DockLigneFacture.ID = New System.Guid("48843072-d6a2-4c42-a639-6901e3079bb5")
        Me.DockLigneFacture.Location = New System.Drawing.Point(0, 0)
        Me.DockLigneFacture.Name = "DockLigneFacture"
        Me.DockLigneFacture.Options.ShowAutoHideButton = False
        Me.DockLigneFacture.Options.ShowCloseButton = False
        Me.DockLigneFacture.Options.ShowMaximizeButton = False
        Me.DockLigneFacture.OriginalSize = New System.Drawing.Size(554, 200)
        Me.DockLigneFacture.Size = New System.Drawing.Size(608, 465)
        Me.DockLigneFacture.Text = "Ajouter une ligne de facture"
        '
        'ControlContainer1
        '
        Me.ControlContainer1.Controls.Add(Me.LayoutControl4)
        Me.ControlContainer1.Location = New System.Drawing.Point(4, 30)
        Me.ControlContainer1.Name = "ControlContainer1"
        Me.ControlContainer1.Size = New System.Drawing.Size(600, 431)
        Me.ControlContainer1.TabIndex = 0
        '
        'LayoutControl4
        '
        Me.LayoutControl4.Controls.Add(Me.chkAjout)
        Me.LayoutControl4.Controls.Add(Me.bUpdate)
        Me.LayoutControl4.Controls.Add(Me.tbPrixI)
        Me.LayoutControl4.Controls.Add(Me.radChoix)
        Me.LayoutControl4.Controls.Add(Me.btnAnnulerI)
        Me.LayoutControl4.Controls.Add(Me.bSauvegardeI)
        Me.LayoutControl4.Controls.Add(Me.cbOrganisationI)
        Me.LayoutControl4.Controls.Add(Me.tbInvoiceNumberI)
        Me.LayoutControl4.Controls.Add(Me.tbDateFromI)
        Me.LayoutControl4.Controls.Add(Me.tbDateAuI)
        Me.LayoutControl4.Controls.Add(Me.cbPeriodesI)
        Me.LayoutControl4.Controls.Add(Me.tbIDFactureI)
        Me.LayoutControl4.Controls.Add(Me.tbDescriptionI)
        Me.LayoutControl4.Controls.Add(Me.tbQuantiteI)
        Me.LayoutControl4.Controls.Add(Me.tbDateFactureI)
        Me.LayoutControl4.Controls.Add(Me.tbIDFactureLigneI)
        Me.LayoutControl4.Controls.Add(Me.tbTotalI)
        Me.LayoutControl4.Controls.Add(Me.tbIDFactureI1)
        Me.LayoutControl4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LayoutControl4.HiddenItems.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem29})
        Me.LayoutControl4.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControl4.Name = "LayoutControl4"
        Me.LayoutControl4.Root = Me.LayoutControlGroup3
        Me.LayoutControl4.Size = New System.Drawing.Size(600, 431)
        Me.LayoutControl4.TabIndex = 0
        Me.LayoutControl4.Text = "LayoutControl4"
        '
        'chkAjout
        '
        Me.chkAjout.Location = New System.Drawing.Point(12, 302)
        Me.chkAjout.Name = "chkAjout"
        Me.chkAjout.Properties.Caption = "Ajout?"
        Me.chkAjout.Size = New System.Drawing.Size(382, 19)
        Me.chkAjout.StyleController = Me.LayoutControl4
        Me.chkAjout.TabIndex = 142
        '
        'bUpdate
        '
        Me.bUpdate.Location = New System.Drawing.Point(197, 325)
        Me.bUpdate.Name = "bUpdate"
        Me.bUpdate.Size = New System.Drawing.Size(197, 22)
        Me.bUpdate.StyleController = Me.LayoutControl4
        Me.bUpdate.TabIndex = 141
        Me.bUpdate.Text = "Mise a jour"
        Me.bUpdate.Visible = False
        '
        'tbPrixI
        '
        Me.tbPrixI.EditValue = "$0.00"
        Me.tbPrixI.Location = New System.Drawing.Point(105, 254)
        Me.tbPrixI.Name = "tbPrixI"
        Me.tbPrixI.Properties.DisplayFormat.FormatString = "c"
        Me.tbPrixI.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.tbPrixI.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.tbPrixI.Properties.Mask.EditMask = "c"
        Me.tbPrixI.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.tbPrixI.Size = New System.Drawing.Size(289, 20)
        Me.tbPrixI.StyleController = Me.LayoutControl4
        Me.tbPrixI.TabIndex = 139
        '
        'radChoix
        '
        Me.radChoix.Location = New System.Drawing.Point(398, 28)
        Me.radChoix.Name = "radChoix"
        Me.radChoix.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "Ajouter ligne facture"), New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "Glaciers"), New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "Ajouter un crédit"), New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "Ajouter un débit"), New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "Ajouter un extra")})
        Me.radChoix.Size = New System.Drawing.Size(190, 293)
        Me.radChoix.StyleController = Me.LayoutControl4
        Me.radChoix.TabIndex = 138
        '
        'btnAnnulerI
        '
        Me.btnAnnulerI.Location = New System.Drawing.Point(398, 325)
        Me.btnAnnulerI.Name = "btnAnnulerI"
        Me.btnAnnulerI.Size = New System.Drawing.Size(190, 22)
        Me.btnAnnulerI.StyleController = Me.LayoutControl4
        Me.btnAnnulerI.TabIndex = 137
        Me.btnAnnulerI.Text = "Fermer"
        '
        'bSauvegardeI
        '
        Me.bSauvegardeI.Location = New System.Drawing.Point(12, 325)
        Me.bSauvegardeI.Name = "bSauvegardeI"
        Me.bSauvegardeI.Size = New System.Drawing.Size(181, 22)
        Me.bSauvegardeI.StyleController = Me.LayoutControl4
        Me.bSauvegardeI.TabIndex = 136
        Me.bSauvegardeI.Text = "Sauvegarder"
        '
        'cbOrganisationI
        '
        Me.cbOrganisationI.Location = New System.Drawing.Point(105, 182)
        Me.cbOrganisationI.Name = "cbOrganisationI"
        Me.cbOrganisationI.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbOrganisationI.Size = New System.Drawing.Size(289, 20)
        Me.cbOrganisationI.StyleController = Me.LayoutControl4
        Me.cbOrganisationI.TabIndex = 36
        '
        'tbInvoiceNumberI
        '
        Me.tbInvoiceNumberI.Location = New System.Drawing.Point(105, 158)
        Me.tbInvoiceNumberI.Name = "tbInvoiceNumberI"
        Me.tbInvoiceNumberI.Size = New System.Drawing.Size(289, 20)
        Me.tbInvoiceNumberI.StyleController = Me.LayoutControl4
        Me.tbInvoiceNumberI.TabIndex = 4
        '
        'tbDateFromI
        '
        Me.tbDateFromI.EditValue = Nothing
        Me.tbDateFromI.Location = New System.Drawing.Point(105, 110)
        Me.tbDateFromI.Name = "tbDateFromI"
        Me.tbDateFromI.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.tbDateFromI.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.tbDateFromI.Properties.DisplayFormat.FormatString = "MM-dd-yy H:mm"
        Me.tbDateFromI.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.tbDateFromI.Properties.EditFormat.FormatString = "MM-dd-yy H:mm"
        Me.tbDateFromI.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.tbDateFromI.Properties.Mask.EditMask = "MM-dd-yy H:mm"
        Me.tbDateFromI.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.tbDateFromI.Size = New System.Drawing.Size(289, 20)
        Me.tbDateFromI.StyleController = Me.LayoutControl4
        Me.tbDateFromI.TabIndex = 22
        '
        'tbDateAuI
        '
        Me.tbDateAuI.EditValue = Nothing
        Me.tbDateAuI.Location = New System.Drawing.Point(105, 134)
        Me.tbDateAuI.Name = "tbDateAuI"
        Me.tbDateAuI.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.tbDateAuI.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.tbDateAuI.Properties.DisplayFormat.FormatString = "MM-dd-yy H:mm"
        Me.tbDateAuI.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.tbDateAuI.Properties.EditFormat.FormatString = "MM-dd-yy H:mm"
        Me.tbDateAuI.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.tbDateAuI.Properties.Mask.EditMask = "MM-dd-yy H:mm"
        Me.tbDateAuI.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.tbDateAuI.Size = New System.Drawing.Size(289, 20)
        Me.tbDateAuI.StyleController = Me.LayoutControl4
        Me.tbDateAuI.TabIndex = 22
        '
        'cbPeriodesI
        '
        Me.cbPeriodesI.Location = New System.Drawing.Point(105, 84)
        Me.cbPeriodesI.Name = "cbPeriodesI"
        Me.cbPeriodesI.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbPeriodesI.Properties.Appearance.Options.UseFont = True
        Me.cbPeriodesI.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbPeriodesI.Size = New System.Drawing.Size(289, 22)
        Me.cbPeriodesI.StyleController = Me.LayoutControl4
        Me.cbPeriodesI.TabIndex = 28
        '
        'tbIDFactureI
        '
        Me.tbIDFactureI.EditValue = "0"
        Me.tbIDFactureI.Enabled = False
        Me.tbIDFactureI.Location = New System.Drawing.Point(105, 36)
        Me.tbIDFactureI.Name = "tbIDFactureI"
        Me.tbIDFactureI.Properties.Appearance.BackColor = System.Drawing.Color.Linen
        Me.tbIDFactureI.Properties.Appearance.Options.UseBackColor = True
        Me.tbIDFactureI.Properties.Mask.EditMask = "n0"
        Me.tbIDFactureI.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.tbIDFactureI.Size = New System.Drawing.Size(289, 20)
        Me.tbIDFactureI.StyleController = Me.LayoutControl4
        Me.tbIDFactureI.TabIndex = 132
        '
        'tbDescriptionI
        '
        Me.tbDescriptionI.Location = New System.Drawing.Point(105, 206)
        Me.tbDescriptionI.Name = "tbDescriptionI"
        Me.tbDescriptionI.Size = New System.Drawing.Size(289, 20)
        Me.tbDescriptionI.StyleController = Me.LayoutControl4
        Me.tbDescriptionI.TabIndex = 4
        '
        'tbQuantiteI
        '
        Me.tbQuantiteI.EditValue = "1"
        Me.tbQuantiteI.Location = New System.Drawing.Point(105, 230)
        Me.tbQuantiteI.Name = "tbQuantiteI"
        Me.tbQuantiteI.Properties.Mask.EditMask = "n0"
        Me.tbQuantiteI.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.tbQuantiteI.Size = New System.Drawing.Size(289, 20)
        Me.tbQuantiteI.StyleController = Me.LayoutControl4
        Me.tbQuantiteI.TabIndex = 132
        '
        'tbDateFactureI
        '
        Me.tbDateFactureI.EditValue = Nothing
        Me.tbDateFactureI.Location = New System.Drawing.Point(105, 60)
        Me.tbDateFactureI.Name = "tbDateFactureI"
        Me.tbDateFactureI.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.tbDateFactureI.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.tbDateFactureI.Properties.DisplayFormat.FormatString = "MM-dd-yy H:mm"
        Me.tbDateFactureI.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.tbDateFactureI.Properties.EditFormat.FormatString = "MM-dd-yy H:mm"
        Me.tbDateFactureI.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.tbDateFactureI.Properties.Mask.EditMask = "MM-dd-yy H:mm"
        Me.tbDateFactureI.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.tbDateFactureI.Size = New System.Drawing.Size(289, 20)
        Me.tbDateFactureI.StyleController = Me.LayoutControl4
        Me.tbDateFactureI.TabIndex = 22
        '
        'tbIDFactureLigneI
        '
        Me.tbIDFactureLigneI.EditValue = "0"
        Me.tbIDFactureLigneI.Enabled = False
        Me.tbIDFactureLigneI.Location = New System.Drawing.Point(105, 12)
        Me.tbIDFactureLigneI.Name = "tbIDFactureLigneI"
        Me.tbIDFactureLigneI.Properties.Appearance.BackColor = System.Drawing.Color.Linen
        Me.tbIDFactureLigneI.Properties.Appearance.Options.UseBackColor = True
        Me.tbIDFactureLigneI.Properties.Mask.EditMask = "n0"
        Me.tbIDFactureLigneI.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.tbIDFactureLigneI.Size = New System.Drawing.Size(289, 20)
        Me.tbIDFactureLigneI.StyleController = Me.LayoutControl4
        Me.tbIDFactureLigneI.TabIndex = 132
        '
        'tbTotalI
        '
        Me.tbTotalI.EditValue = "$0.00"
        Me.tbTotalI.Location = New System.Drawing.Point(105, 278)
        Me.tbTotalI.Name = "tbTotalI"
        Me.tbTotalI.Properties.DisplayFormat.FormatString = "c"
        Me.tbTotalI.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.tbTotalI.Properties.Mask.EditMask = "c"
        Me.tbTotalI.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.tbTotalI.Size = New System.Drawing.Size(289, 20)
        Me.tbTotalI.StyleController = Me.LayoutControl4
        Me.tbTotalI.TabIndex = 139
        '
        'tbIDFactureI1
        '
        Me.tbIDFactureI1.EditValue = "0"
        Me.tbIDFactureI1.Enabled = False
        Me.tbIDFactureI1.Location = New System.Drawing.Point(105, 351)
        Me.tbIDFactureI1.Name = "tbIDFactureI1"
        Me.tbIDFactureI1.Properties.Appearance.BackColor = System.Drawing.Color.Linen
        Me.tbIDFactureI1.Properties.Appearance.Options.UseBackColor = True
        Me.tbIDFactureI1.Properties.Mask.EditMask = "n0"
        Me.tbIDFactureI1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.tbIDFactureI1.Size = New System.Drawing.Size(483, 20)
        Me.tbIDFactureI1.StyleController = Me.LayoutControl4
        Me.tbIDFactureI1.TabIndex = 132
        '
        'LayoutControlItem29
        '
        Me.LayoutControlItem29.Control = Me.tbIDFactureI1
        Me.LayoutControlItem29.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem29.CustomizationFormText = "Nombre maximum free echange (305 et 35)"
        Me.LayoutControlItem29.Location = New System.Drawing.Point(0, 339)
        Me.LayoutControlItem29.Name = "LayoutControlItem29"
        Me.LayoutControlItem29.Size = New System.Drawing.Size(580, 72)
        Me.LayoutControlItem29.Text = "ID Facture"
        Me.LayoutControlItem29.TextSize = New System.Drawing.Size(90, 13)
        '
        'LayoutControlGroup3
        '
        Me.LayoutControlGroup3.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup3.GroupBordersVisible = False
        Me.LayoutControlGroup3.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem16, Me.LayoutControlItem17, Me.LayoutControlItem18, Me.LayoutControlItem19, Me.LayoutControlItem20, Me.LayoutSave, Me.LayoutControlItem22, Me.lblQuantite, Me.LayoutControlItem24, Me.LayoutControlItem26, Me.LayoutControlItem28, Me.lblOrganisation, Me.lblDescription, Me.lblPrixI, Me.lblTotalI, Me.layoutUpdate, Me.LayoutControlItem23})
        Me.LayoutControlGroup3.Name = "Root"
        Me.LayoutControlGroup3.Size = New System.Drawing.Size(600, 431)
        Me.LayoutControlGroup3.TextVisible = False
        '
        'LayoutControlItem16
        '
        Me.LayoutControlItem16.Control = Me.tbInvoiceNumberI
        Me.LayoutControlItem16.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem16.CustomizationFormText = "Numéro de paye"
        Me.LayoutControlItem16.Location = New System.Drawing.Point(0, 146)
        Me.LayoutControlItem16.Name = "LayoutControlItem16"
        Me.LayoutControlItem16.Size = New System.Drawing.Size(386, 24)
        Me.LayoutControlItem16.Text = "Numéro de facture"
        Me.LayoutControlItem16.TextSize = New System.Drawing.Size(90, 13)
        '
        'LayoutControlItem17
        '
        Me.LayoutControlItem17.Control = Me.tbDateFromI
        Me.LayoutControlItem17.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem17.CustomizationFormText = "Date du"
        Me.LayoutControlItem17.Location = New System.Drawing.Point(0, 98)
        Me.LayoutControlItem17.Name = "LayoutControlItem17"
        Me.LayoutControlItem17.Size = New System.Drawing.Size(386, 24)
        Me.LayoutControlItem17.Text = "Date du"
        Me.LayoutControlItem17.TextSize = New System.Drawing.Size(90, 13)
        '
        'LayoutControlItem18
        '
        Me.LayoutControlItem18.Control = Me.tbDateAuI
        Me.LayoutControlItem18.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem18.CustomizationFormText = "Date du"
        Me.LayoutControlItem18.Location = New System.Drawing.Point(0, 122)
        Me.LayoutControlItem18.Name = "LayoutControlItem18"
        Me.LayoutControlItem18.Size = New System.Drawing.Size(386, 24)
        Me.LayoutControlItem18.Text = "Date au"
        Me.LayoutControlItem18.TextSize = New System.Drawing.Size(90, 13)
        '
        'LayoutControlItem19
        '
        Me.LayoutControlItem19.Control = Me.cbPeriodesI
        Me.LayoutControlItem19.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem19.CustomizationFormText = "Périodes"
        Me.LayoutControlItem19.Location = New System.Drawing.Point(0, 72)
        Me.LayoutControlItem19.Name = "LayoutControlItem19"
        Me.LayoutControlItem19.Size = New System.Drawing.Size(386, 26)
        Me.LayoutControlItem19.Text = "Période"
        Me.LayoutControlItem19.TextSize = New System.Drawing.Size(90, 13)
        '
        'LayoutControlItem20
        '
        Me.LayoutControlItem20.Control = Me.tbIDFactureI
        Me.LayoutControlItem20.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem20.CustomizationFormText = "Nombre maximum free echange (305 et 35)"
        Me.LayoutControlItem20.Location = New System.Drawing.Point(0, 24)
        Me.LayoutControlItem20.Name = "LayoutControlItem20"
        Me.LayoutControlItem20.Size = New System.Drawing.Size(386, 24)
        Me.LayoutControlItem20.Text = "ID Facture"
        Me.LayoutControlItem20.TextSize = New System.Drawing.Size(90, 13)
        '
        'LayoutSave
        '
        Me.LayoutSave.Control = Me.bSauvegardeI
        Me.LayoutSave.Location = New System.Drawing.Point(0, 313)
        Me.LayoutSave.Name = "LayoutSave"
        Me.LayoutSave.Size = New System.Drawing.Size(185, 98)
        Me.LayoutSave.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutSave.TextVisible = False
        '
        'LayoutControlItem22
        '
        Me.LayoutControlItem22.Control = Me.btnAnnulerI
        Me.LayoutControlItem22.Location = New System.Drawing.Point(386, 313)
        Me.LayoutControlItem22.Name = "LayoutControlItem22"
        Me.LayoutControlItem22.Size = New System.Drawing.Size(194, 98)
        Me.LayoutControlItem22.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem22.TextVisible = False
        '
        'lblQuantite
        '
        Me.lblQuantite.Control = Me.tbQuantiteI
        Me.lblQuantite.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.lblQuantite.CustomizationFormText = "Nombre maximum free echange (305 et 35)"
        Me.lblQuantite.Location = New System.Drawing.Point(0, 218)
        Me.lblQuantite.Name = "lblQuantite"
        Me.lblQuantite.Size = New System.Drawing.Size(386, 24)
        Me.lblQuantite.Text = "Quantité"
        Me.lblQuantite.TextSize = New System.Drawing.Size(90, 13)
        '
        'LayoutControlItem24
        '
        Me.LayoutControlItem24.Control = Me.tbDateFactureI
        Me.LayoutControlItem24.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem24.CustomizationFormText = "Date du"
        Me.LayoutControlItem24.Location = New System.Drawing.Point(0, 48)
        Me.LayoutControlItem24.Name = "LayoutControlItem24"
        Me.LayoutControlItem24.Size = New System.Drawing.Size(386, 24)
        Me.LayoutControlItem24.Text = "Date facture"
        Me.LayoutControlItem24.TextSize = New System.Drawing.Size(90, 13)
        '
        'LayoutControlItem26
        '
        Me.LayoutControlItem26.Control = Me.tbIDFactureLigneI
        Me.LayoutControlItem26.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem26.CustomizationFormText = "Nombre maximum free echange (305 et 35)"
        Me.LayoutControlItem26.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControlItem26.Name = "LayoutControlItem26"
        Me.LayoutControlItem26.Size = New System.Drawing.Size(386, 24)
        Me.LayoutControlItem26.Text = "ID Facture Ligne"
        Me.LayoutControlItem26.TextSize = New System.Drawing.Size(90, 13)
        '
        'LayoutControlItem28
        '
        Me.LayoutControlItem28.Control = Me.radChoix
        Me.LayoutControlItem28.Location = New System.Drawing.Point(386, 0)
        Me.LayoutControlItem28.Name = "LayoutControlItem28"
        Me.LayoutControlItem28.Size = New System.Drawing.Size(194, 313)
        Me.LayoutControlItem28.Text = "Type d'ajout"
        Me.LayoutControlItem28.TextLocation = DevExpress.Utils.Locations.Top
        Me.LayoutControlItem28.TextSize = New System.Drawing.Size(90, 13)
        '
        'lblOrganisation
        '
        Me.lblOrganisation.Control = Me.cbOrganisationI
        Me.lblOrganisation.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.lblOrganisation.CustomizationFormText = "Liste des factures"
        Me.lblOrganisation.Location = New System.Drawing.Point(0, 170)
        Me.lblOrganisation.Name = "lblOrganisation"
        Me.lblOrganisation.Size = New System.Drawing.Size(386, 24)
        Me.lblOrganisation.Text = "Organisation"
        Me.lblOrganisation.TextSize = New System.Drawing.Size(90, 13)
        '
        'lblDescription
        '
        Me.lblDescription.Control = Me.tbDescriptionI
        Me.lblDescription.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.lblDescription.CustomizationFormText = "LayoutControlItem10"
        Me.lblDescription.Location = New System.Drawing.Point(0, 194)
        Me.lblDescription.Name = "lblDescription"
        Me.lblDescription.Size = New System.Drawing.Size(386, 24)
        Me.lblDescription.Text = "Description"
        Me.lblDescription.TextSize = New System.Drawing.Size(90, 13)
        '
        'lblPrixI
        '
        Me.lblPrixI.Control = Me.tbPrixI
        Me.lblPrixI.Location = New System.Drawing.Point(0, 242)
        Me.lblPrixI.Name = "lblPrixI"
        Me.lblPrixI.Size = New System.Drawing.Size(386, 24)
        Me.lblPrixI.Text = "Prix"
        Me.lblPrixI.TextSize = New System.Drawing.Size(90, 13)
        '
        'lblTotalI
        '
        Me.lblTotalI.Control = Me.tbTotalI
        Me.lblTotalI.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.lblTotalI.CustomizationFormText = "lblPrixI"
        Me.lblTotalI.Location = New System.Drawing.Point(0, 266)
        Me.lblTotalI.Name = "lblTotalI"
        Me.lblTotalI.Size = New System.Drawing.Size(386, 24)
        Me.lblTotalI.Text = "Total"
        Me.lblTotalI.TextSize = New System.Drawing.Size(90, 13)
        '
        'layoutUpdate
        '
        Me.layoutUpdate.Control = Me.bUpdate
        Me.layoutUpdate.Location = New System.Drawing.Point(185, 313)
        Me.layoutUpdate.Name = "layoutUpdate"
        Me.layoutUpdate.Size = New System.Drawing.Size(201, 98)
        Me.layoutUpdate.TextSize = New System.Drawing.Size(0, 0)
        Me.layoutUpdate.TextVisible = False
        '
        'LayoutControlItem23
        '
        Me.LayoutControlItem23.Control = Me.chkAjout
        Me.LayoutControlItem23.Location = New System.Drawing.Point(0, 290)
        Me.LayoutControlItem23.Name = "LayoutControlItem23"
        Me.LayoutControlItem23.Size = New System.Drawing.Size(386, 23)
        Me.LayoutControlItem23.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem23.TextVisible = False
        '
        'DockNouvelleFacture
        '
        Me.DockNouvelleFacture.Controls.Add(Me.DockPanel1_Container)
        Me.DockNouvelleFacture.Dock = DevExpress.XtraBars.Docking.DockingStyle.Float
        Me.DockNouvelleFacture.FloatLocation = New System.Drawing.Point(523, 711)
        Me.DockNouvelleFacture.FloatSize = New System.Drawing.Size(338, 267)
        Me.DockNouvelleFacture.ID = New System.Guid("e0fb716c-29f8-49d1-a79c-17ceb21bdec8")
        Me.DockNouvelleFacture.Location = New System.Drawing.Point(0, 0)
        Me.DockNouvelleFacture.Name = "DockNouvelleFacture"
        Me.DockNouvelleFacture.OriginalSize = New System.Drawing.Size(313, 200)
        Me.DockNouvelleFacture.Size = New System.Drawing.Size(338, 267)
        Me.DockNouvelleFacture.Text = "Ajouter une facture"
        '
        'DockPanel1_Container
        '
        Me.DockPanel1_Container.Controls.Add(Me.LayoutControl3)
        Me.DockPanel1_Container.Location = New System.Drawing.Point(4, 30)
        Me.DockPanel1_Container.Name = "DockPanel1_Container"
        Me.DockPanel1_Container.Size = New System.Drawing.Size(330, 233)
        Me.DockPanel1_Container.TabIndex = 0
        '
        'LayoutControl3
        '
        Me.LayoutControl3.Controls.Add(Me.btnAnnuler)
        Me.LayoutControl3.Controls.Add(Me.bSauvegarde)
        Me.LayoutControl3.Controls.Add(Me.cbOrganisation)
        Me.LayoutControl3.Controls.Add(Me.tbInvoiceNumber)
        Me.LayoutControl3.Controls.Add(Me.tbDateFrom)
        Me.LayoutControl3.Controls.Add(Me.tbDateAu)
        Me.LayoutControl3.Controls.Add(Me.cbPeriodes)
        Me.LayoutControl3.Controls.Add(Me.tbIDFacture)
        Me.LayoutControl3.Controls.Add(Me.tbDateFacture)
        Me.LayoutControl3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LayoutControl3.Location = New System.Drawing.Point(0, 0)
        Me.LayoutControl3.Name = "LayoutControl3"
        Me.LayoutControl3.Root = Me.LayoutControlGroup2
        Me.LayoutControl3.Size = New System.Drawing.Size(330, 233)
        Me.LayoutControl3.TabIndex = 0
        Me.LayoutControl3.Text = "LayoutControl3"
        '
        'btnAnnuler
        '
        Me.btnAnnuler.Location = New System.Drawing.Point(167, 182)
        Me.btnAnnuler.Name = "btnAnnuler"
        Me.btnAnnuler.Size = New System.Drawing.Size(151, 22)
        Me.btnAnnuler.StyleController = Me.LayoutControl3
        Me.btnAnnuler.TabIndex = 136
        Me.btnAnnuler.Text = "Annuler"
        '
        'bSauvegarde
        '
        Me.bSauvegarde.Location = New System.Drawing.Point(12, 182)
        Me.bSauvegarde.Name = "bSauvegarde"
        Me.bSauvegarde.Size = New System.Drawing.Size(151, 22)
        Me.bSauvegarde.StyleController = Me.LayoutControl3
        Me.bSauvegarde.TabIndex = 135
        Me.bSauvegarde.Text = "Sauvegarder"
        '
        'cbOrganisation
        '
        Me.cbOrganisation.Location = New System.Drawing.Point(105, 86)
        Me.cbOrganisation.Name = "cbOrganisation"
        Me.cbOrganisation.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbOrganisation.Size = New System.Drawing.Size(213, 20)
        Me.cbOrganisation.StyleController = Me.LayoutControl3
        Me.cbOrganisation.TabIndex = 36
        '
        'tbInvoiceNumber
        '
        Me.tbInvoiceNumber.Location = New System.Drawing.Point(105, 158)
        Me.tbInvoiceNumber.Name = "tbInvoiceNumber"
        Me.tbInvoiceNumber.Size = New System.Drawing.Size(213, 20)
        Me.tbInvoiceNumber.StyleController = Me.LayoutControl3
        Me.tbInvoiceNumber.TabIndex = 4
        '
        'tbDateFrom
        '
        Me.tbDateFrom.EditValue = Nothing
        Me.tbDateFrom.Location = New System.Drawing.Point(105, 110)
        Me.tbDateFrom.Name = "tbDateFrom"
        Me.tbDateFrom.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.tbDateFrom.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.tbDateFrom.Properties.DisplayFormat.FormatString = "MM-dd-yy H:mm"
        Me.tbDateFrom.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.tbDateFrom.Properties.EditFormat.FormatString = "MM-dd-yy H:mm"
        Me.tbDateFrom.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.tbDateFrom.Properties.Mask.EditMask = "MM-dd-yy H:mm"
        Me.tbDateFrom.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.tbDateFrom.Size = New System.Drawing.Size(213, 20)
        Me.tbDateFrom.StyleController = Me.LayoutControl3
        Me.tbDateFrom.TabIndex = 22
        '
        'tbDateAu
        '
        Me.tbDateAu.EditValue = Nothing
        Me.tbDateAu.Location = New System.Drawing.Point(105, 134)
        Me.tbDateAu.Name = "tbDateAu"
        Me.tbDateAu.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.tbDateAu.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.tbDateAu.Properties.DisplayFormat.FormatString = "MM-dd-yy H:mm"
        Me.tbDateAu.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.tbDateAu.Properties.EditFormat.FormatString = "MM-dd-yy H:mm"
        Me.tbDateAu.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.tbDateAu.Properties.Mask.EditMask = "MM-dd-yy H:mm"
        Me.tbDateAu.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.tbDateAu.Size = New System.Drawing.Size(213, 20)
        Me.tbDateAu.StyleController = Me.LayoutControl3
        Me.tbDateAu.TabIndex = 22
        '
        'cbPeriodes
        '
        Me.cbPeriodes.Location = New System.Drawing.Point(105, 60)
        Me.cbPeriodes.Name = "cbPeriodes"
        Me.cbPeriodes.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbPeriodes.Properties.Appearance.Options.UseFont = True
        Me.cbPeriodes.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.cbPeriodes.Size = New System.Drawing.Size(213, 22)
        Me.cbPeriodes.StyleController = Me.LayoutControl3
        Me.cbPeriodes.TabIndex = 28
        '
        'tbIDFacture
        '
        Me.tbIDFacture.EditValue = "0"
        Me.tbIDFacture.Enabled = False
        Me.tbIDFacture.Location = New System.Drawing.Point(105, 12)
        Me.tbIDFacture.Name = "tbIDFacture"
        Me.tbIDFacture.Properties.Appearance.BackColor = System.Drawing.Color.Linen
        Me.tbIDFacture.Properties.Appearance.Options.UseBackColor = True
        Me.tbIDFacture.Properties.Mask.EditMask = "n0"
        Me.tbIDFacture.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.tbIDFacture.Size = New System.Drawing.Size(213, 20)
        Me.tbIDFacture.StyleController = Me.LayoutControl3
        Me.tbIDFacture.TabIndex = 132
        '
        'tbDateFacture
        '
        Me.tbDateFacture.EditValue = Nothing
        Me.tbDateFacture.Location = New System.Drawing.Point(105, 36)
        Me.tbDateFacture.Name = "tbDateFacture"
        Me.tbDateFacture.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.tbDateFacture.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.tbDateFacture.Properties.DisplayFormat.FormatString = "MM-dd-yy H:mm"
        Me.tbDateFacture.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.tbDateFacture.Properties.EditFormat.FormatString = "MM-dd-yy H:mm"
        Me.tbDateFacture.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.tbDateFacture.Properties.Mask.EditMask = "MM-dd-yy H:mm"
        Me.tbDateFacture.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.tbDateFacture.Size = New System.Drawing.Size(213, 20)
        Me.tbDateFacture.StyleController = Me.LayoutControl3
        Me.tbDateFacture.TabIndex = 22
        '
        'LayoutControlGroup2
        '
        Me.LayoutControlGroup2.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.[True]
        Me.LayoutControlGroup2.GroupBordersVisible = False
        Me.LayoutControlGroup2.Items.AddRange(New DevExpress.XtraLayout.BaseLayoutItem() {Me.LayoutControlItem10, Me.LayoutControlItem13, Me.LayoutControlItem14, Me.LayoutControlItem15, Me.LayoutNombreFree1, Me.LayoutControlItem9, Me.LayoutControlItem11, Me.LayoutControlItem27, Me.LayoutControlItem25})
        Me.LayoutControlGroup2.Name = "Root"
        Me.LayoutControlGroup2.Size = New System.Drawing.Size(330, 233)
        Me.LayoutControlGroup2.TextVisible = False
        '
        'LayoutControlItem10
        '
        Me.LayoutControlItem10.Control = Me.tbInvoiceNumber
        Me.LayoutControlItem10.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem10.CustomizationFormText = "Numéro de paye"
        Me.LayoutControlItem10.Location = New System.Drawing.Point(0, 146)
        Me.LayoutControlItem10.Name = "LayoutControlItem10"
        Me.LayoutControlItem10.Size = New System.Drawing.Size(310, 24)
        Me.LayoutControlItem10.Text = "Numéro de facture"
        Me.LayoutControlItem10.TextSize = New System.Drawing.Size(90, 13)
        '
        'LayoutControlItem13
        '
        Me.LayoutControlItem13.Control = Me.tbDateFrom
        Me.LayoutControlItem13.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem13.CustomizationFormText = "Date du"
        Me.LayoutControlItem13.Location = New System.Drawing.Point(0, 98)
        Me.LayoutControlItem13.Name = "LayoutControlItem13"
        Me.LayoutControlItem13.Size = New System.Drawing.Size(310, 24)
        Me.LayoutControlItem13.Text = "Date du"
        Me.LayoutControlItem13.TextSize = New System.Drawing.Size(90, 13)
        '
        'LayoutControlItem14
        '
        Me.LayoutControlItem14.Control = Me.tbDateAu
        Me.LayoutControlItem14.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem14.CustomizationFormText = "Date du"
        Me.LayoutControlItem14.Location = New System.Drawing.Point(0, 122)
        Me.LayoutControlItem14.Name = "LayoutControlItem14"
        Me.LayoutControlItem14.Size = New System.Drawing.Size(310, 24)
        Me.LayoutControlItem14.Text = "Date au"
        Me.LayoutControlItem14.TextSize = New System.Drawing.Size(90, 13)
        '
        'LayoutControlItem15
        '
        Me.LayoutControlItem15.Control = Me.cbPeriodes
        Me.LayoutControlItem15.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem15.CustomizationFormText = "Périodes"
        Me.LayoutControlItem15.Location = New System.Drawing.Point(0, 48)
        Me.LayoutControlItem15.Name = "LayoutControlItem15"
        Me.LayoutControlItem15.Size = New System.Drawing.Size(310, 26)
        Me.LayoutControlItem15.Text = "Période"
        Me.LayoutControlItem15.TextSize = New System.Drawing.Size(90, 13)
        '
        'LayoutNombreFree1
        '
        Me.LayoutNombreFree1.Control = Me.tbIDFacture
        Me.LayoutNombreFree1.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutNombreFree1.CustomizationFormText = "Nombre maximum free echange (305 et 35)"
        Me.LayoutNombreFree1.Location = New System.Drawing.Point(0, 0)
        Me.LayoutNombreFree1.Name = "LayoutNombreFree1"
        Me.LayoutNombreFree1.Size = New System.Drawing.Size(310, 24)
        Me.LayoutNombreFree1.Text = "ID Facture"
        Me.LayoutNombreFree1.TextSize = New System.Drawing.Size(90, 13)
        '
        'LayoutControlItem9
        '
        Me.LayoutControlItem9.Control = Me.bSauvegarde
        Me.LayoutControlItem9.Location = New System.Drawing.Point(0, 170)
        Me.LayoutControlItem9.Name = "LayoutControlItem9"
        Me.LayoutControlItem9.Size = New System.Drawing.Size(155, 43)
        Me.LayoutControlItem9.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem9.TextVisible = False
        '
        'LayoutControlItem11
        '
        Me.LayoutControlItem11.Control = Me.btnAnnuler
        Me.LayoutControlItem11.Location = New System.Drawing.Point(155, 170)
        Me.LayoutControlItem11.Name = "LayoutControlItem11"
        Me.LayoutControlItem11.Size = New System.Drawing.Size(155, 43)
        Me.LayoutControlItem11.TextSize = New System.Drawing.Size(0, 0)
        Me.LayoutControlItem11.TextVisible = False
        '
        'LayoutControlItem27
        '
        Me.LayoutControlItem27.Control = Me.cbOrganisation
        Me.LayoutControlItem27.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem27.CustomizationFormText = "Liste des factures"
        Me.LayoutControlItem27.Location = New System.Drawing.Point(0, 74)
        Me.LayoutControlItem27.Name = "LayoutControlItem27"
        Me.LayoutControlItem27.Size = New System.Drawing.Size(310, 24)
        Me.LayoutControlItem27.Text = "Organisation"
        Me.LayoutControlItem27.TextSize = New System.Drawing.Size(90, 13)
        '
        'LayoutControlItem25
        '
        Me.LayoutControlItem25.Control = Me.tbDateFacture
        Me.LayoutControlItem25.ControlAlignment = System.Drawing.ContentAlignment.TopLeft
        Me.LayoutControlItem25.CustomizationFormText = "Date du"
        Me.LayoutControlItem25.Location = New System.Drawing.Point(0, 24)
        Me.LayoutControlItem25.Name = "LayoutControlItem25"
        Me.LayoutControlItem25.Size = New System.Drawing.Size(310, 24)
        Me.LayoutControlItem25.Text = "Date facture"
        Me.LayoutControlItem25.TextSize = New System.Drawing.Size(90, 13)
        '
        'frmFacture
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1363, 686)
        Me.Controls.Add(Me.tabData)
        Me.Controls.Add(Me.TS)
        Me.Name = "frmFacture"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Gestion des Factures"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.TS.ResumeLayout(False)
        Me.TS.PerformLayout()
        CType(Me.tabData, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabData.ResumeLayout(False)
        Me.XtraTabPage1.ResumeLayout(False)
        CType(Me.LayoutControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LayoutControl2.ResumeLayout(False)
        CType(Me.grdPaye, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grdFactures, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Root, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem21, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem39, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmptySpaceItem2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabPage2.ResumeLayout(False)
        CType(Me.LayoutControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LayoutControl1.ResumeLayout(False)
        CType(Me.grdImport, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemMemoEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemMemoEdit3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemMemoEdit2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemCalcEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemTextEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemTextEdit2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemCalcEdit2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemSpinEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemCheckEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grdInvoiceLine, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DockManager1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.DockPaiement.ResumeLayout(False)
        Me.ControlContainer2.ResumeLayout(False)
        CType(Me.LayoutControl5, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LayoutControl5.ResumeLayout(False)
        CType(Me.tbIDFactureP.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbPeriodesP.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbInvoiceNumberP.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbDateFromP.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbDateFromP.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbDateAuP.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbDateAuP.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbOrganisationP.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbMontantPaiement.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem30, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem31, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem32, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem33, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem34, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem35, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem36, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem37, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem38, System.ComponentModel.ISupportInitialize).EndInit()
        Me.DockLigneFacture.ResumeLayout(False)
        Me.ControlContainer1.ResumeLayout(False)
        CType(Me.LayoutControl4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LayoutControl4.ResumeLayout(False)
        CType(Me.chkAjout.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbPrixI.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.radChoix.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbOrganisationI.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbInvoiceNumberI.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbDateFromI.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbDateFromI.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbDateAuI.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbDateAuI.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbPeriodesI.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbIDFactureI.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbDescriptionI.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbQuantiteI.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbDateFactureI.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbDateFactureI.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbIDFactureLigneI.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbTotalI.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbIDFactureI1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem29, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem16, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem17, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem18, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem19, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem20, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutSave, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem22, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblQuantite, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem24, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem26, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem28, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblOrganisation, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblDescription, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblPrixI, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lblTotalI, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.layoutUpdate, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem23, System.ComponentModel.ISupportInitialize).EndInit()
        Me.DockNouvelleFacture.ResumeLayout(False)
        Me.DockPanel1_Container.ResumeLayout(False)
        CType(Me.LayoutControl3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.LayoutControl3.ResumeLayout(False)
        CType(Me.cbOrganisation.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbInvoiceNumber.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbDateFrom.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbDateFrom.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbDateAu.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbDateAu.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbPeriodes.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbIDFacture.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbDateFacture.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbDateFacture.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlGroup2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem13, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem14, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem15, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutNombreFree1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem27, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LayoutControlItem25, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents TS As ToolStrip
    Friend WithEvents Head As ToolStripLabel
    Friend WithEvents sp1 As ToolStripSeparator
    Friend WithEvents bDelete As ToolStripButton
    Friend WithEvents sp3 As ToolStripSeparator
    Friend WithEvents bCancel As ToolStripButton
    Friend WithEvents sp4 As ToolStripSeparator
    Friend WithEvents ToolStripButton6 As ToolStripButton
    Friend WithEvents tabData As DevExpress.XtraTab.XtraTabControl
    Friend WithEvents XtraTabPage1 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents LayoutControl2 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents grdFactures As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colIDInvoice As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn5 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents Root As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem2 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents XtraTabPage2 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents LayoutControl1 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents grdInvoiceLine As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView2 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colIDInvoiceLine As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colInvoiceNumber As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colDateFrom2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents LayoutControlGroup1 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem1 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents colOrganisation As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents ColDateFrom As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colDateTo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colDateTo2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colOrganisationID As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colOrganisation2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colDescription As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colUnite As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colPrix As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colMontant As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCredit As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colNombreGlacier As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colGlacier As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colInvoiceDate As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colInvoiceDate2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colDebit As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents grdImport As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView3 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colImportID As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colReference As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colClient As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemMemoEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit
    Friend WithEvents colClientAddress As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colMomentDePassage As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colPreparePar As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colHeureAppel As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colHeurePrep As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colHeureDepart As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colHeureLivraison As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colMessagePassage As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemMemoEdit3 As DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit
    Friend WithEvents colTotalTempService As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colLivreur As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colDistance As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn3 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemMemoEdit2 As DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit
    Friend WithEvents GridColumn4 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn6 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn7 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colMontantGlacierLivreur As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colMontantGlacierOrganisation As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colMontantLivreur As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents Extras As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colExtraKM As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemCalcEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit
    Friend WithEvents RepositoryItemTextEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents RepositoryItemTextEdit2 As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents RepositoryItemCalcEdit2 As DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit
    Friend WithEvents RepositoryItemSpinEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit
    Friend WithEvents RepositoryItemCheckEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
    Friend WithEvents LayoutControlItem3 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem4 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents colOrganisationID2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colDisplayDate As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents bExportToExcel As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LayoutControlItem5 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents bExportToHTML As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents bExportToPDF As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents bExportToDoc As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LayoutControlItem8 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem6 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem7 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents DockManager1 As DevExpress.XtraBars.Docking.DockManager
    Friend WithEvents DockNouvelleFacture As DevExpress.XtraBars.Docking.DockPanel
    Friend WithEvents DockPanel1_Container As DevExpress.XtraBars.Docking.ControlContainer
    Friend WithEvents LayoutControl3 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents cbOrganisation As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents tbInvoiceNumber As DevExpress.XtraEditors.TextEdit
    Friend WithEvents tbDateFrom As DevExpress.XtraEditors.DateEdit
    Friend WithEvents tbDateAu As DevExpress.XtraEditors.DateEdit
    Friend WithEvents cbPeriodes As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents tbIDFacture As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlGroup2 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents LayoutControlItem27 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem10 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem13 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem14 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem15 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutNombreFree1 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents bSauvegarde As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LayoutControlItem9 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents btnAnnuler As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LayoutControlItem11 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents bNouvelleFacture As ToolStripButton
    Friend WithEvents bAjouterLaFacture As ToolStripButton
    Friend WithEvents ToolStripSeparator1 As ToolStripSeparator
    Friend WithEvents DockLigneFacture As DevExpress.XtraBars.Docking.DockPanel
    Friend WithEvents ControlContainer1 As DevExpress.XtraBars.Docking.ControlContainer
    Friend WithEvents LayoutControl4 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents LayoutControlGroup3 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents btnAnnulerI As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents bSauvegardeI As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cbOrganisationI As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents tbInvoiceNumberI As DevExpress.XtraEditors.TextEdit
    Friend WithEvents tbDateFromI As DevExpress.XtraEditors.DateEdit
    Friend WithEvents tbDateAuI As DevExpress.XtraEditors.DateEdit
    Friend WithEvents cbPeriodesI As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents tbIDFactureI As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblOrganisation As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem16 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem17 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem18 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem19 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem20 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutSave As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem22 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents tbDescriptionI As DevExpress.XtraEditors.TextEdit
    Friend WithEvents tbQuantiteI As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblDescription As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents lblQuantite As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents tbDateFactureI As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LayoutControlItem24 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents tbDateFacture As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LayoutControlItem25 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents tbIDFactureLigneI As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlItem26 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents radChoix As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents LayoutControlItem28 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents tbPrixI As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblPrixI As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents tbTotalI As DevExpress.XtraEditors.TextEdit
    Friend WithEvents lblTotalI As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents bUpdate As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents layoutUpdate As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents chkAjout As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LayoutControlItem23 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents bPaiement As ToolStripButton
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents grdPaye As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView4 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn8 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn9 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn13 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn15 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents LayoutControlItem12 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem21 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents GridColumn11 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn10 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn12 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents DockPaiement As DevExpress.XtraBars.Docking.DockPanel
    Friend WithEvents ControlContainer2 As DevExpress.XtraBars.Docking.ControlContainer
    Friend WithEvents LayoutControl5 As DevExpress.XtraLayout.LayoutControl
    Friend WithEvents LayoutControlGroup4 As DevExpress.XtraLayout.LayoutControlGroup
    Friend WithEvents tbIDFactureI1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlItem29 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents tbIDFactureP As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlItem30 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents cbPeriodesP As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LayoutControlItem31 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents tbInvoiceNumberP As DevExpress.XtraEditors.TextEdit
    Friend WithEvents tbDateFromP As DevExpress.XtraEditors.DateEdit
    Friend WithEvents tbDateAuP As DevExpress.XtraEditors.DateEdit
    Friend WithEvents cbOrganisationP As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LayoutControlItem32 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem33 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem34 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem35 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents bFermerPaiement As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents bSauvegarderPaiement As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents tbMontantPaiement As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LayoutControlItem36 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem37 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents LayoutControlItem38 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents bDeletePaiement As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LayoutControlItem39 As DevExpress.XtraLayout.LayoutControlItem
    Friend WithEvents EmptySpaceItem1 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents EmptySpaceItem2 As DevExpress.XtraLayout.EmptySpaceItem
    Friend WithEvents GridColumn14 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn16 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn17 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn18 As DevExpress.XtraGrid.Columns.GridColumn
End Class
