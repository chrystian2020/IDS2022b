﻿Imports System.Data.SqlClient
Imports System.Globalization
Imports System.Threading
Imports System.Text

Public Class frmOrganisations
    Private oOrganisationData As New OrganisationsData
    Private oOrganisation As New Organisations

    Private oCommissionKMLivreur As New CommissionKMLivreur
    Private oCommissionKMLivreurData As New CommissionKMLivreurData


    Private oMaxPayable As New MaxPayable
    Private oMaxPayableData As New MaxPayableData

    Private oCommissionKM As New CommissionKM
    Private oCommissionKMData As New CommissionKMData

    Private oCommissionParHeure As New CommissionParHeure
    Private oCommissionParHeureData As New CommissionParHeureData

    Private oCadeauLivreur As New CadeauLivreur
    Private oCadeauLivreurData As New CadeauLivreurData

    Private oCadeauOrganisation As New CadeauOrganisation
    Private oCadeauOrganisationData As New CadeauOrganisationData


    Private oHoraire As New Horaire
    Private oHoraireData As New HoraireData

    Private oHoraireJourData As New HoraireJourData

    Private oPrixFixeData As New PrixFixeData


    Private oLivreurData As New LivreursData




    Private bload As Boolean = False
    Private bAddMode As Boolean = False
    Private bEditMode As Boolean = False
    Private bDeleteMode As Boolean = False
    Public blnGridClick As Boolean = False
    Private iRowGrid As Int32 = 0
    Dim ssql As String



    Private Sub FillAllJourSemaine()

        Dim drJourSemaine As DataRow
        Dim dtJourSemaine As DataTable
        Dim oJourSemaineData As New JourSemaineData
        Dim oJourSemaine As New JourSemaine

        Dim table As New DataTable
        table.Columns.Add("value", Type.GetType("System.Int32"))
        table.Columns.Add("displayText", Type.GetType("System.String"))

        dtJourSemaine = oJourSemaineData.SelectAll()

        For Each drJourSemaine In dtJourSemaine.Rows
            Dim teller As Integer = 0
            teller = drJourSemaine("IDJourSemaine")
            Dim toto As String = drJourSemaine("Jour")

            table.Rows.Add(New Object() {teller, toto})
        Next

        'With cbJourH.Properties
        '    .DataSource = table
        '    .NullText = "Selectionner une journée"
        '    .DisplayMember = "displayText"
        '    .ValueMember = "value"
        '    .PopupFormMinSize = New Size(50, 200)
        'End With

    End Sub
    Private Sub FillAllVendeurs()


        Dim dtLivreur As DataTable
        Dim drLivreur As DataRow


        Dim table As New DataTable
        table.Columns.Add("value", Type.GetType("System.Int32"))
        table.Columns.Add("Livreur", Type.GetType("System.String"))
        dtLivreur = oLivreurData.SelectAllVendeurs



        For Each drLivreur In dtLivreur.Rows
            Dim teller As Integer = 0

            teller = drLivreur("LivreurID")
            Dim toto As String = drLivreur("Livreur")
            table.Rows.Add(New Object() {teller, toto})
        Next


        cbVendeur.Properties.DataSource = table
        cbVendeur.Properties.NullText = "Selectionner un vendeur"
        cbVendeur.Properties.DisplayMember = "Livreur"
        cbVendeur.Properties.ValueMember = "value"
        cbVendeur.Properties.PopupFormMinSize = New Size(50, 200)


    End Sub

    Private Sub FillAllLivreurs()


        Dim dtLivreur As DataTable
        Dim drLivreur As DataRow


        Dim table As New DataTable
        table.Columns.Add("value", Type.GetType("System.Int32"))
        table.Columns.Add("Livreur", Type.GetType("System.String"))
        dtLivreur = oLivreurData.SelectAll



        For Each drLivreur In dtLivreur.Rows
            Dim teller As Integer = 0

            teller = drLivreur("LivreurID")
            Dim toto As String = drLivreur("Livreur")
            table.Rows.Add(New Object() {teller, toto})
        Next


        cbLivreursCadeauL.Properties.DataSource = table
        cbLivreursCadeauL.Properties.NullText = "Selectionner un livreur"
        cbLivreursCadeauL.Properties.DisplayMember = "Livreur"
        cbLivreursCadeauL.Properties.ValueMember = "value"
        cbLivreursCadeauL.Properties.PopupFormMinSize = New Size(50, 200)
        cbLivreursCadeauL.Properties.PopulateColumns()

        cbLivreursH.Properties.DataSource = table
        cbLivreursH.Properties.NullText = "Selectionner un livreur"
        cbLivreursH.Properties.DisplayMember = "Livreur"
        cbLivreursH.Properties.ValueMember = "value"
        cbLivreursH.Properties.PopupFormMinSize = New Size(50, 200)
        cbLivreursH.Properties.PopulateColumns()

        cbLivreursP.Properties.DataSource = table
        cbLivreursP.Properties.NullText = "Selectionner un livreur"
        cbLivreursP.Properties.DisplayMember = "Livreur"
        cbLivreursP.Properties.ValueMember = "value"
        cbLivreursP.Properties.PopupFormMinSize = New Size(50, 200)
        cbLivreursP.Properties.PopulateColumns()

        cbLivreursJ.Properties.DataSource = table
        cbLivreursJ.Properties.NullText = "Selectionner un livreur"
        cbLivreursJ.Properties.DisplayMember = "Livreur"
        cbLivreursJ.Properties.ValueMember = "value"
        cbLivreursJ.Properties.PopupFormMinSize = New Size(50, 200)
        cbLivreursJ.Properties.PopulateColumns()


    End Sub

    Private Sub frmOrganisations_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        bload = True
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor


        tbDateFromCadeauO.Text = go_Globals.PeriodeDateFrom
        tbDateToCadeauO.Text = go_Globals.PeriodeDateTo
        tbDateFromCadeauL.Text = go_Globals.PeriodeDateFrom
        tbDateToCadeauL.Text = go_Globals.PeriodeDateTo

        Thread.CurrentThread.CurrentCulture = New CultureInfo("en-US", True)
        LoadGridCode()
        grdCadeauLivreur.DataSource = oCadeauLivreurData.SelectAll(go_Globals.PeriodeDateFrom, go_Globals.PeriodeDateTo)
        FillAllLivreurs()
        FillAllVendeurs()
        tbIDHoraire.Text = 0
        bload = False

        Me.Cursor = System.Windows.Forms.Cursors.Default
    End Sub
    Private Sub LoadGridCode()
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        Try

            grdOrganisations.DataSource = oOrganisationData.SelectAll
            GridView1.ClearSorting()
            GridView1.Columns("OrganisationID").SortOrder = DevExpress.Data.ColumnSortOrder.Ascending
            GridView1.FocusedRowHandle = 1
            GridView1.Columns("OrganisationID").Width = 50
            GridView1.Columns("Organisation").Width = 400

        Catch
        End Try
        Me.Cursor = System.Windows.Forms.Cursors.Default
    End Sub
    Private Sub Edit()
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        Me.AddMode = False
        Me.EditMode = True
        Me.DeleteMode = False
        ClearRecord()
        GetData()

        EnableRecord(True)


        Me.Cursor = System.Windows.Forms.Cursors.Default
    End Sub



    Private Sub TS_ItemClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolStripItemClickedEventArgs) Handles TS.ItemClicked
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

        Select Case e.ClickedItem.Text
            Case "Ajouter"

                tabData.SelectedTabPage = tabData.TabPages(1)
                tabData.TabPages(1).PageVisible = True
                tabData.TabPages(0).PageVisible = False
                Add()


            Case "Edition"
                tabData.SelectedTabPage = tabData.TabPages(1)
                tabData.TabPages(1).PageVisible = True
                tabData.TabPages(0).PageVisible = False
                Edit()

            Case "Supprimer"
                tabData.SelectedTabPage = tabData.TabPages(0)
                tabData.TabPages(0).PageVisible = True
                tabData.TabPages(1).PageVisible = False
                Delete()
            Case "Annuler"
                ShowToolStripItems("Cancel")
            Case "Rafraîchir"
                GoBack_To_Grid()
        End Select
        Me.Cursor = System.Windows.Forms.Cursors.Default
    End Sub
    Private Sub ShowToolStripItems(ByVal Item As String)
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        bAdd.Enabled = False
        bEdit.Enabled = False
        bDelete.Enabled = False
        bCancel.Enabled = False
        bRefresh.Enabled = False
        Select Case Item
            Case "Add"
                bCancel.Enabled = True
            Case "Edit"
                bCancel.Enabled = True
            Case "Delete"
                bCancel.Enabled = True
            Case "Cancel"
                bAdd.Enabled = True
                bEdit.Enabled = True
                bDelete.Enabled = True
                bRefresh.Enabled = True
                tabData.SelectedTabPage = tabData.TabPages(0)
                tabData.TabPages(1).PageVisible = False
                tabData.TabPages(0).PageVisible = True
            Case "Refresh"
                bAdd.Enabled = True
                bEdit.Enabled = True
                bDelete.Enabled = True
                bRefresh.Enabled = True
                LoadGridCode()
            Case "No Record"
                bAdd.Enabled = True
                bRefresh.Enabled = True
        End Select

        Me.Cursor = System.Windows.Forms.Cursors.Default
    End Sub
    Private Sub SetData(ByVal oOrganisation As Organisations)

        With oOrganisation

            .OrganisationID = If(String.IsNullOrEmpty(nudOrganisationID.Text), Nothing, nudOrganisationID.Text)
            .Organisation = If(String.IsNullOrEmpty(tbOrganisation.Text), Nothing, tbOrganisation.Text)
            .OrganisationImport = If(String.IsNullOrEmpty(tbOrganisationImport.Text), Nothing, tbOrganisationImport.Text)
            .Adresse = If(String.IsNullOrEmpty(tbAdresse.Text), Nothing, tbAdresse.Text)
            .Telephone = If(String.IsNullOrEmpty(tbTelephone.Text), Nothing, tbTelephone.Text)
            .KMMax = If(String.IsNullOrEmpty(tbQuantiteLivraison.Text), Nothing, tbQuantiteLivraison.Text)
            .Email = If(String.IsNullOrEmpty(tbCourriel1.Text), Nothing, tbCourriel1.Text)
            .Email1 = If(String.IsNullOrEmpty(tbCourriel2.Text), Nothing, tbCourriel2.Text)
            .Email2 = If(String.IsNullOrEmpty(tbCourriel3.Text), Nothing, tbCourriel3.Text)
            .Email3 = If(String.IsNullOrEmpty(tbCourriel4.Text), Nothing, tbCourriel4.Text)
            .MontantContrat = If(String.IsNullOrEmpty(tbMontantContrat.Text), Nothing, tbMontantContrat.Text)
            .MontantParPorteOrganisation = If(String.IsNullOrEmpty(tbMontantParPorteOrganisation.Text), Nothing, tbMontantParPorteOrganisation.Text)
            .MontantFixe = If(String.IsNullOrEmpty(tbMontantFixe.Text), Nothing, tbMontantFixe.Text)
            .PayeLivreur = If(String.IsNullOrEmpty(tbPayeLivreur.Text), Nothing, tbPayeLivreur.Text)
            .MontantParPorteLivreur = If(String.IsNullOrEmpty(tbMontantParPorteLivreur.Text), Nothing, tbMontantParPorteLivreur.Text)

            .QuantiteLivraison = If(String.IsNullOrEmpty(tbQuantiteLivraison.Text), Nothing, tbQuantiteLivraison.Text)
            .CreditParLivraison = If(String.IsNullOrEmpty(tbCreditParLivraison.Text), Nothing, tbCreditParLivraison.Text)

            If optCalculationOrganisations.SelectedIndex > -1 Then
                .MetodeCalculationOrganisation = optCalculationOrganisations.SelectedIndex
            Else
                .MetodeCalculationOrganisation = Nothing
            End If

            If optCalculationLivreur.SelectedIndex > -1 Then
                .MetodeCalculationLivreur = optCalculationLivreur.SelectedIndex
            Else
                .MetodeCalculationLivreur = Nothing
            End If
            .MontantGlacierOrganisation = If(String.IsNullOrEmpty(tbMontantGlacierOrganisation.Text), Nothing, tbMontantGlacierOrganisation.Text)
            .MontantGlacierLivreur = If(String.IsNullOrEmpty(tbMontantGlacierLivreur.Text), Nothing, tbMontantGlacierLivreur.Text)

            .MontantMobilus = If(String.IsNullOrEmpty(tbMontantMobilus.Text), Nothing, tbMontantMobilus.Text)
            .NombreFree30535 = If(String.IsNullOrEmpty(tbNombreFree30535.Text), Nothing, tbNombreFree30535.Text)

            If chkCacheEmail1.Checked = True Then
                .CacheEmail1 = True
            Else
                .CacheEmail1 = False
            End If

            If chkCacheEmail2.Checked = True Then
                .CacheEmail2 = True
            Else
                .CacheEmail2 = False
            End If

            If chkCacheEmail3.Checked = True Then
                .CacheEmail3 = True
            Else
                .CacheEmail3 = False
            End If

            If chkCacheEmail4.Checked = True Then
                .CacheEmail4 = True
            Else
                .CacheEmail4 = False
            End If

            If chkRestaurant.Checked = True Then
                .Restaurant = True
            Else
                .Restaurant = False
            End If


            If Not IsNothing(cbVendeur.EditValue) Then
                If cbVendeur.EditValue.ToString.Trim <> "" Then
                    .IDVendeur = cbVendeur.EditValue
                    .CommissionVendeur = If(String.IsNullOrEmpty(tbCommissionVendeur.Text), Nothing, tbCommissionVendeur.Text)
                End If
            Else
                .IDVendeur = Nothing
                .CommissionVendeur = Nothing

            End If

            If tbMessageFacture.Text.ToString.Trim <> "" Then
                oOrganisation.MessageFacture = tbMessageFacture.Text
            Else
                oOrganisation.MessageFacture = Nothing
            End If

            If tbMessageEtat.Text.ToString.Trim <> "" Then
                oOrganisation.MessageEtat = tbMessageEtat.Text
            Else
                oOrganisation.MessageEtat = Nothing
            End If


            If chkRestaurant.Checked = True Then
                .Restaurant = True
            Else
                .Restaurant = False
            End If

            If chkIsCommission.Checked = True Then
                .IsCommission = True
            Else
                .IsCommission = False
            End If


            If chkIsEtatDeCompte.Checked = True Then
                .isEtatDeCompte = True
            Else
                .isEtatDeCompte = False
            End If


            .MontantPrixFixeSem = If(String.IsNullOrEmpty(tbMontantPrixFixeSem.Text), Nothing, tbMontantPrixFixeSem.Text)
            .MaximumLivraison = If(String.IsNullOrEmpty(tbMaximumLivraison.Text), Nothing, tbMaximumLivraison.Text)
            .MontantMaxLivraisoneExtra = If(String.IsNullOrEmpty(tbMontantMaxLivraisoneExtra.Text), Nothing, tbMontantMaxLivraisoneExtra.Text)



        End With
    End Sub


    Private Sub UpdateRecord()
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        Dim oOrganisation As New Organisations


        Dim row As System.Data.DataRow = GridView1.GetDataRow(GridView1.FocusedRowHandle)
        oOrganisation.OrganisationID = System.Convert.ToInt32(row("OrganisationID").ToString)
        oOrganisation = oOrganisationData.Select_Record(oOrganisation)

        If VerifyData() = True Then
            SetData(oOrganisation)
            Dim bSucess As Boolean
            bSucess = oOrganisationData.Update(oOrganisation, oOrganisation)

            oOrganisationData.UpdateIDVendeur()


            If bSucess = True Then
                GoBack_To_Grid()
                GridView1.FocusedRowHandle = SelectedRow
            Else
                MsgBox("Update failed.", MsgBoxStyle.OkOnly, "Erreur")
            End If
        End If
        Me.Cursor = System.Windows.Forms.Cursors.Default
    End Sub

    Private Sub DeleteRecord()
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        Dim clsOrganisation As New Organisations

        Dim row As System.Data.DataRow = GridView1.GetDataRow(GridView1.FocusedRowHandle)
        clsOrganisation.OrganisationID = System.Convert.ToInt32(row("OrganisationID").ToString)
        Dim result As DialogResult = MessageBox.Show("Êtes-vous sur? Supprimer cet enregistrement?", "Supprimer", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
        If result = DialogResult.Yes Then
            SetData(clsOrganisation)
            Dim bSucess As Boolean
            bSucess = oOrganisationData.Delete(clsOrganisation)
            If bSucess = True Then
                GoBack_To_Grid()
            Else
                MsgBox("La supression a echouée.", MsgBoxStyle.OkOnly, "Erreur")
            End If
        End If
        Me.Cursor = System.Windows.Forms.Cursors.Default
    End Sub
    Private Sub InsertRecord()
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        Dim clsOrganisation As New Organisations
        If VerifyData() = True Then
            SetData(clsOrganisation)
            Dim bSucess As Boolean
            bSucess = oOrganisationData.Add(clsOrganisation)
            If bSucess = True Then
                oOrganisationData.UpdateIDVendeur()

                GoBack_To_Grid()
            Else
                MsgBox("L'insertion a échouée.", MsgBoxStyle.OkOnly, "Erreur")
            End If
        End If
        Me.Cursor = System.Windows.Forms.Cursors.Default
    End Sub


    Private Function VerifyData() As Boolean
        Return True
    End Function

    Public Property SelectedRow() As Int32
        Get
            Return iRowGrid
        End Get
        Set(ByVal value As Int32)
            iRowGrid = value
        End Set
    End Property

    Private Sub bAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        tabData.SelectedTabPage = tabData.TabPages(1)
        tabData.TabPages(1).PageVisible = True
        tabData.TabPages(0).PageVisible = False
    End Sub

    Private Sub bEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        tabData.SelectedTabPage = tabData.TabPages(1)
        tabData.TabPages(1).PageVisible = True
        tabData.TabPages(0).PageVisible = False
    End Sub

    Private Sub bCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        tabData.SelectedTabPage = tabData.TabPages(0)
        tabData.TabPages(1).PageVisible = False
        tabData.TabPages(0).PageVisible = True
    End Sub

    Private Sub bDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        tabData.SelectedTabPage = tabData.TabPages(0)
        tabData.TabPages(1).PageVisible = False
        tabData.TabPages(0).PageVisible = True
    End Sub

    Private Sub butCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butCancel.Click
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        ShowToolStripItems("Cancel")
        tabData.SelectedTabPage = tabData.TabPages(0)
        tabData.TabPages(1).PageVisible = False
        tabData.TabPages(0).PageVisible = True
        tabData.TabPages(2).PageVisible = False
        tabData.TabPages(3).PageVisible = False
        tabData.TabPages(4).PageVisible = False
        tabData.TabPages(5).PageVisible = False

        Me.Cursor = System.Windows.Forms.Cursors.Default

    End Sub

    Private Sub butClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butClose.Click
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        Me.Close()
        Me.Cursor = System.Windows.Forms.Cursors.Default
    End Sub
    Public Property AddMode() As Boolean
        Get
            Return bAddMode
        End Get
        Set(ByVal value As Boolean)
            bAddMode = value
        End Set
    End Property

    Public Property EditMode() As Boolean
        Get
            Return bEditMode
        End Get
        Set(ByVal value As Boolean)
            bEditMode = value
        End Set
    End Property

    Public Property DeleteMode() As Boolean
        Get
            Return bDeleteMode
        End Get
        Set(ByVal value As Boolean)
            bDeleteMode = value
        End Set
    End Property

    Private Sub EnableRecord(ByVal YesNo As Boolean)
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        Me.tbOrganisation.Enabled = YesNo
        Me.tbAdresse.Enabled = YesNo
        Me.tbTelephone.Enabled = YesNo
        Me.tbMontantContrat.Enabled = YesNo
        tbPayeLivreur.Enabled = YesNo
        Me.optCalculationOrganisations.Enabled = YesNo
        Me.tbCreditParLivraison.Enabled = YesNo
        Me.tbQuantiteLivraison.Enabled = YesNo
        tbMontantGlacierOrganisation.Enabled = YesNo
        tbMontantGlacierLivreur.Enabled = YesNo
        tbNombreFree30535.Enabled = YesNo
        tbMontantMobilus.Enabled = YesNo
        tbMessageFacture.Enabled = YesNo
        chkIsCommission.Enabled = YesNo
        chkIsEtatDeCompte.Enabled = YesNo
        chkRestaurant.Enabled = YesNo
        Me.tbMontantPrixFixeSem.Enabled = YesNo
        Me.tbMontantMaxLivraisoneExtra.Enabled = YesNo
        Me.tbMaximumLivraison.Enabled = YesNo
        tbMontantParPorteOrganisation.Enabled = YesNo
        tbMontantParPorteLivreur.Enabled = YesNo

        Me.Cursor = System.Windows.Forms.Cursors.Default
    End Sub


    Private Sub ClearRecord()
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

        Me.tbOrganisation.Text = Nothing
        Me.tbAdresse.Text = Nothing
        Me.tbTelephone.Text = Nothing
        Me.tbMontantContrat.Text = 0
        tbPayeLivreur.Text = 0
        Me.tbCreditParLivraison.Text = 0
        Me.tbQuantiteLivraison.Text = 0
        tbMontantGlacierOrganisation.Text = 0
        tbMontantGlacierLivreur.Text = 0
        tbNombreFree30535.Text = 0
        tbMontantMobilus.Text = 0
        optCalculationOrganisations.SelectedIndex = -1
        optCalculationLivreur.SelectedIndex = -1
        tbOrganisationImport.Text = Nothing
        cbVendeur.EditValue = Nothing
        chkRestaurant.Checked = False
        tbCommissionVendeur.Text = 0
        tbMontantContrat.Text = 0
        tbMontantFixe.Text = 0
        tbMessageFacture.Text = Nothing
        tbMessageEtat.Text = Nothing
        tbMontantParPorteOrganisation.Text = Nothing
        tbMontantParPorteLivreur.Text = Nothing
        tbMontantParPorteLivreur.Text = Nothing
        nudOrganisationID.Text = Nothing

        tbCourriel1.Text = Nothing
        tbCourriel2.Text = Nothing
        tbCourriel3.Text = Nothing
        tbCourriel4.Text = Nothing



        Me.tbMontantPrixFixeSem.Text = 0
        Me.tbMontantMaxLivraisoneExtra.Text = 0
        Me.tbMaximumLivraison.Text = 0


        chkIsCommission.Checked = False
        chkIsEtatDeCompte.Checked = False

        Me.tbOrganisation.Select()


        Me.Cursor = System.Windows.Forms.Cursors.Default
    End Sub

    Private Sub GoBack_To_Grid()
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        Dim gridOK As Boolean = False
        Try
            ShowToolStripItems("Cancel")
            LoadGridCode()

            gridOK = True
        Catch
            'Hide Erreur message.
        Finally
            If gridOK = False Then
                ''''
                ShowToolStripItems("Cancel")
                ''''
            End If
        End Try
        Me.Cursor = System.Windows.Forms.Cursors.Default
    End Sub


    Private Sub Delete()
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        Me.AddMode = False
        Me.EditMode = False
        Me.DeleteMode = True

        GetData()

        EnableRecord(False)
        tabData.SelectedTabPage = tabData.TabPages(1)
        tabData.TabPages(1).PageVisible = True
        tabData.TabPages(0).PageVisible = False
        ShowToolStripItems("Delete")
        Me.Cursor = System.Windows.Forms.Cursors.Default
    End Sub


    Private Sub Add()
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        Me.AddMode = True
        Me.EditMode = False
        Me.DeleteMode = False
        ResetSections()
        tabData.TabPages(2).PageVisible = False
        tabData.TabPages(3).PageVisible = False
        tabData.TabPages(4).PageVisible = False
        tabData.TabPages(5).PageVisible = False
        tabData.TabPages(8).PageVisible = False
        tabData.TabPages(9).PageVisible = False


        ClearRecord()
        EnableRecord(True)


        tabData.SelectedTabPage = tabData.TabPages(1)
        tabData.TabPages(1).PageVisible = True
        tabData.TabPages(0).PageVisible = False
        ShowToolStripItems("Add")

        Me.Cursor = System.Windows.Forms.Cursors.Default
    End Sub

    Private Sub GetData()

        Try



            Dim row As System.Data.DataRow = GridView1.GetDataRow(GridView1.FocusedRowHandle)

            Dim clsOrganisation As New Organisations
            Dim clsOrganisation2 As New Organisations
            clsOrganisation.OrganisationID = System.Convert.ToInt32(row("OrganisationID").ToString)


            grdPrixFixe.DataSource = oPrixFixeData.SelectAllByIDOrganisationIDLivreur(clsOrganisation.OrganisationID)

            LayoutMontantMobilus.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always
            tbMontantMobilus.Text = 0

            Select Case clsOrganisation.OrganisationID
                Case 4
                    LayoutNombreFree30535.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always
                    tbNombreFree30535.Text = 0
                Case 6

                    LayoutNombreFree30535.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always
                    tbNombreFree30535.Text = 0

                Case Else

                    LayoutNombreFree30535.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never
                    tbNombreFree30535.Text = 0
            End Select
            ClearRecord()


            clsOrganisation2 = oOrganisationData.Select_Record(clsOrganisation)

            If Not clsOrganisation2 Is Nothing Then

                Me.nudOrganisationID.Text = System.Convert.ToInt32(clsOrganisation2.OrganisationID)
                tbOrganisation.Text = If(IsNothing(clsOrganisation2.Organisation), Nothing, clsOrganisation2.Organisation.ToString.Trim)
                tbOrganisationImport.Text = If(IsNothing(clsOrganisation2.OrganisationImport), Nothing, clsOrganisation2.OrganisationImport.ToString.Trim)
                tbAdresse.Text = If(IsNothing(clsOrganisation2.Adresse), Nothing, clsOrganisation2.Adresse.ToString.Trim)
                tbTelephone.Text = If(IsNothing(clsOrganisation2.Telephone), Nothing, clsOrganisation2.Telephone.ToString.Trim)
                tbCourriel1.Text = If(IsNothing(clsOrganisation2.Email), Nothing, clsOrganisation2.Email.ToString.Trim)
                tbCourriel2.Text = If(IsNothing(clsOrganisation2.Email1), Nothing, clsOrganisation2.Email1.ToString.Trim)
                tbCourriel3.Text = If(IsNothing(clsOrganisation2.Email2), Nothing, clsOrganisation2.Email2.ToString.Trim)
                tbCourriel4.Text = If(IsNothing(clsOrganisation2.Email3), Nothing, clsOrganisation2.Email3.ToString.Trim)



                Me.tbMontantContrat.Text = If(clsOrganisation2.MontantContrat Is Nothing, Nothing, System.Convert.ToDecimal(clsOrganisation2.MontantContrat))
                Me.tbMontantParPorteOrganisation.Text = If(clsOrganisation2.MontantParPorteOrganisation Is Nothing, Nothing, System.Convert.ToDecimal(clsOrganisation2.MontantParPorteOrganisation))
                Me.tbPayeLivreur.Text = If(clsOrganisation2.PayeLivreur Is Nothing, Nothing, System.Convert.ToDecimal(clsOrganisation2.PayeLivreur))
                tbMontantParPorteLivreur.Text = If(clsOrganisation2.MontantParPorteLivreur Is Nothing, Nothing, System.Convert.ToDecimal(clsOrganisation2.MontantParPorteLivreur))
                Me.tbMontantFixe.Text = If(clsOrganisation2.MontantFixe Is Nothing, Nothing, System.Convert.ToDecimal(clsOrganisation2.MontantFixe))

                tbQuantiteLivraison.Text = If(clsOrganisation2.QuantiteLivraison Is Nothing, Nothing, System.Convert.ToInt32(clsOrganisation2.QuantiteLivraison))

                Me.tbQuantiteLivraison.Text = If(clsOrganisation2.QuantiteLivraison Is Nothing, Nothing, System.Convert.ToInt32(clsOrganisation2.QuantiteLivraison))

                If IsNothing(clsOrganisation2.CreditParLivraison) Then
                    tbCreditParLivraison.Text = Nothing
                Else
                    tbCreditParLivraison.Text = clsOrganisation2.CreditParLivraison
                End If

                If IsNothing(clsOrganisation2.MontantGlacierOrganisation) Then
                    tbMontantGlacierOrganisation.Text = Nothing
                Else
                    tbMontantGlacierOrganisation.Text = clsOrganisation2.MontantGlacierOrganisation
                End If

                If IsNothing(clsOrganisation2.MontantGlacierLivreur) Then
                    tbMontantGlacierLivreur.Text = Nothing
                Else
                    tbMontantGlacierLivreur.Text = clsOrganisation2.MontantGlacierLivreur
                End If






                If clsOrganisation2.MetodeCalculationOrganisation > -1 Then
                    optCalculationOrganisations.SelectedIndex = clsOrganisation2.MetodeCalculationOrganisation
                Else
                    optCalculationOrganisations.SelectedIndex = -1
                End If

                If clsOrganisation2.MetodeCalculationLivreur > -1 Then
                    optCalculationLivreur.SelectedIndex = clsOrganisation2.MetodeCalculationLivreur
                Else
                    optCalculationLivreur.SelectedIndex = -1
                End If

                If IsNothing(clsOrganisation2.NombreFree30535) Then
                    tbNombreFree30535.Text = Nothing
                Else
                    tbNombreFree30535.Text = clsOrganisation2.NombreFree30535
                End If
                If IsNothing(clsOrganisation2.MontantMobilus) Then
                    tbMontantMobilus.Text = Nothing
                Else
                    tbMontantMobilus.Text = clsOrganisation2.MontantMobilus
                End If


                If clsOrganisation2.CacheEmail1 = True Then
                    chkCacheEmail1.Checked = True
                Else
                    chkCacheEmail1.Checked = False
                End If

                If clsOrganisation2.CacheEmail2 = True Then
                    chkCacheEmail2.Checked = True
                Else
                    chkCacheEmail2.Checked = False
                End If

                If clsOrganisation2.CacheEmail3 = True Then
                    chkCacheEmail3.Checked = True
                Else
                    chkCacheEmail3.Checked = False
                End If

                If clsOrganisation2.CacheEmail4 = True Then
                    chkCacheEmail4.Checked = True
                Else
                    chkCacheEmail4.Checked = False
                End If


                If clsOrganisation2.Restaurant = True Then
                    chkRestaurant.Checked = True
                Else
                    chkRestaurant.Checked = False
                End If

                If clsOrganisation2.IDVendeur > 0 Then

                    cbVendeur.EditValue = clsOrganisation2.IDVendeur
                Else
                    cbVendeur.EditValue = Nothing
                End If


                tbMessageFacture.Text = If(IsNothing(clsOrganisation2.MessageFacture), Nothing, clsOrganisation2.MessageFacture.ToString.Trim)

                tbMessageEtat.Text = If(IsNothing(clsOrganisation2.MessageEtat), Nothing, clsOrganisation2.MessageEtat.ToString.Trim)

                Me.tbCommissionVendeur.Text = If(clsOrganisation2.CommissionVendeur Is Nothing, Nothing, System.Convert.ToDecimal(clsOrganisation2.CommissionVendeur))

                If clsOrganisation2.IsCommission = True Then
                    chkIsCommission.Checked = True
                Else
                    chkIsCommission.Checked = False
                End If

                If clsOrganisation2.isEtatDeCompte = True Then
                    chkIsEtatDeCompte.Checked = True
                Else
                    chkIsEtatDeCompte.Checked = False
                End If


                Me.tbMontantPrixFixeSem.Text = If(clsOrganisation2.MontantPrixFixeSem Is Nothing, Nothing, System.Convert.ToDecimal(clsOrganisation2.MontantPrixFixeSem))
                Me.tbMontantMaxLivraisoneExtra.Text = If(clsOrganisation2.MontantMaxLivraisoneExtra Is Nothing, Nothing, System.Convert.ToDecimal(clsOrganisation2.MontantMaxLivraisoneExtra))
                Me.tbMaximumLivraison.Text = If(clsOrganisation2.MaximumLivraison Is Nothing, Nothing, System.Convert.ToInt32(clsOrganisation2.MaximumLivraison))



            End If





        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try



    End Sub





    Private Sub butApply_Click(sender As System.Object, e As System.EventArgs) Handles LayoutControlItem82.Click, butApply.Click

        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        If Me.AddMode = True Then
            Me.InsertRecord()
        ElseIf Me.EditMode = True Then
            Me.UpdateRecord()
        ElseIf Me.DeleteMode = True Then
            Me.DeleteRecord()
        End If
        Me.Cursor = System.Windows.Forms.Cursors.Default

        tabData.SelectedTabPage = tabData.TabPages(0)
        tabData.TabPages(1).PageVisible = False
        tabData.TabPages(0).PageVisible = True
        tabData.TabPages(2).PageVisible = False
        tabData.TabPages(3).PageVisible = False
        tabData.TabPages(4).PageVisible = False
        tabData.TabPages(5).PageVisible = False
    End Sub

    Private Sub ToolStripButton6_Click(sender As System.Object, e As System.EventArgs) Handles ToolStripButton6.Click
        Me.Close()

    End Sub


    Private Sub bAjouterChargesParKm_Click(sender As Object, e As EventArgs) Handles bAjouterChargesParKm.Click


        If tbKMDebut.Text < 0 Then
            MsgBox("Vous devez entrer un Kilomètre de départ.", MsgBoxStyle.OkOnly, "Erreur")
            tbKMDebut.Focus()
            Exit Sub
        End If

        If tbKMFin.Text = 0 Then
            MsgBox("Vous devez entrer un Kilomètre de fin.", MsgBoxStyle.OkOnly, "Erreur")
            tbKMFin.Focus()
            Exit Sub
        End If

        If tbMontantCharge.Text = 0 Then
            MsgBox("Vous devez entrer un montant.", MsgBoxStyle.OkOnly, "Erreur")
            tbMontantCharge.Focus()
            Exit Sub
        End If

        If optCalculationOrganisations.SelectedIndex = -1 Then
            MsgBox("Vous devez sélectionner une méthode de calcul sur le tab Détail de l'organisation.", MsgBoxStyle.OkOnly, "Erreur")
            optCalculationOrganisations.Focus()
            Exit Sub
        End If


        With oCommissionKM
            .OrganisationID = nudOrganisationID.Text
            .Organisation = If(String.IsNullOrEmpty(tbOrganisation.Text), Nothing, tbOrganisation.Text)
            .KMFrom = If(String.IsNullOrEmpty(tbKMDebut.Text), Nothing, tbKMDebut.Text)
            .KMTo = If(String.IsNullOrEmpty(tbKMFin.Text), Nothing, tbKMFin.Text)
            .Montant = If(String.IsNullOrEmpty(tbMontantCharge.Text), Nothing, tbMontantCharge.Text)
            .Organisation = If(String.IsNullOrEmpty(tbOrganisation.Text), Nothing, tbOrganisation.Text)
            .PerKMNumber = If(String.IsNullOrEmpty(tbChargeExtraParNbKm.Text), Nothing, tbChargeExtraParNbKm.Text)
            .ExtraChargeParKM = If(String.IsNullOrEmpty(tbChargeExtra.Text), Nothing, tbChargeExtra.Text)

            If optCalculationOrganisations.SelectedIndex > -1 Then
                .MetodeCalculation = optCalculationOrganisations.SelectedIndex
            Else
                .MetodeCalculation = Nothing
            End If



        End With


        If tbHeureDepart.Text <> "00:00" Then
            oCommissionKM.HeureDepart = If(String.IsNullOrEmpty(tbHeureDepart.Text), Nothing, tbHeureDepart.Text)
            oCommissionKM.HeureDepart = oCommissionKM.HeureDepart.ToString.Replace("0001", Now.Year)
        Else
            oCommissionKM.HeureDepart = Nothing
        End If


        If tbHeureFin.Text <> "00:00" Then
            oCommissionKM.HeureFin = If(String.IsNullOrEmpty(tbHeureFin.Text), Nothing, tbHeureFin.Text)
            oCommissionKM.HeureFin = oCommissionKM.HeureFin.ToString.Replace("0001", Now.Year)
        Else
            oCommissionKM.HeureFin = Nothing
        End If



        Dim bSucess As Boolean
        bSucess = oCommissionKMData.Add(oCommissionKM)
        If bSucess = True Then
            grdParKM.DataSource = oCommissionKMData.SelectAllByName(nudOrganisationID.Text)
            GridView2.ClearSorting()
            GridView2.Columns("IDCommissionKM").SortOrder = DevExpress.Data.ColumnSortOrder.Ascending
            MessageBox.Show("l'enregistrement a été sauvegardée")

        Else
            MsgBox("L'insertion a échouée.", MsgBoxStyle.OkOnly, "Erreur")
        End If

    End Sub

    Private Sub bSupprimerChargesParKm_Click(sender As Object, e As EventArgs) Handles bSupprimerChargesParKm.Click

        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        Dim clsCommissionKM As New CommissionKM

        Dim row As System.Data.DataRow = GridView2.GetDataRow(GridView2.FocusedRowHandle)
        clsCommissionKM.IDCommissionKM = System.Convert.ToInt32(row("IDCommissionKM").ToString)
        Dim result As DialogResult = MessageBox.Show("Êtes-vous sur? Supprimer cet enregistrement?", "Supprimer", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
        If result = DialogResult.Yes Then

            Dim bSucess As Boolean
            bSucess = oCommissionKMData.Delete(clsCommissionKM)
            If bSucess = True Then
                grdParKM.DataSource = oCommissionKMData.SelectAllByName(nudOrganisationID.Text)
                GridView2.ClearSorting()
                GridView2.Columns("IDCommissionKM").SortOrder = DevExpress.Data.ColumnSortOrder.Ascending

                bSupprimerChargesParKm.Enabled = False
                bMiseaJourChargesParKm.Enabled = False
                MessageBox.Show("l'enregistrement a été supprimée")
            Else
                MsgBox("La supression a echouée.", MsgBoxStyle.OkOnly, "Erreur")
            End If
        End If
        Me.Cursor = System.Windows.Forms.Cursors.Default
    End Sub

    Private Sub bAjouterChargeParHeure_Click(sender As Object, e As EventArgs) Handles bAjouterChargeParHeure.Click


        If IsNothing(cbLivreursH.EditValue) Then
            MsgBox("Vous devez sélectionner un livreur.", MsgBoxStyle.OkOnly, "Erreur")
            cbLivreursH.Focus()
            Exit Sub
        End If



        If tbMontantParHeureLivreurH.Text < 0 Then
            MsgBox("Vous devez entrer un montant.", MsgBoxStyle.OkOnly, "Erreur")
            tbMontantParHeureLivreurH.Focus()
            Exit Sub
        End If

        If tbMontantParHeureOrganisationH.Text < 0 Then
            MsgBox("Vous devez entrer un montant.", MsgBoxStyle.OkOnly, "Erreur")
            tbMontantParHeureLivreurH.Focus()
            Exit Sub
        End If



        With oHoraire
            .IDOrganisation = nudOrganisationID.Text
            .Organisation = If(String.IsNullOrEmpty(tbOrganisation.Text), Nothing, tbOrganisation.Text)
            .IDLivreur = cbLivreursH.EditValue
            .Livreur = If(String.IsNullOrEmpty(cbLivreursH.Text), Nothing, cbLivreursH.Text)
            .MontantLivreur = If(String.IsNullOrEmpty(tbMontantParHeureLivreurH.Text), Nothing, tbMontantParHeureLivreurH.Text)
            .MontantOrganisation = If(String.IsNullOrEmpty(tbMontantParHeureOrganisationH.Text), Nothing, tbMontantParHeureOrganisationH.Text)

            .IDPeriode = go_Globals.IDPeriode

            .HeureDepart = If(String.IsNullOrEmpty(tbHeureDepartH.Text), Nothing, tbHeureDepartH.Text)

            .HeureFin = If(String.IsNullOrEmpty(tbHeureFinH.Text), Nothing, tbHeureFinH.Text)
        End With

        Dim bSucess As Boolean
        bSucess = oHoraireData.Add(oHoraire)
        If bSucess = True Then
            grdParHeure.DataSource = oHoraireData.SelectAllByIDOrganisationIDLivreurGrille(oHoraire.IDOrganisation, oHoraire.IDLivreur)
            bSupprimerChargeParHeure.Enabled = False
            bMiseAJourChargeParHeure.Enabled = False
            tbIDHoraire.Text = 0
            MessageBox.Show("l'enregistrement a été sauvegardée")
        Else
            MsgBox("L'insertion a échouée.", MsgBoxStyle.OkOnly, "Erreur")
        End If


    End Sub

    Private Sub bSupprimerChargeParHeure_Click(sender As Object, e As EventArgs) Handles bSupprimerChargeParHeure.Click
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

        Dim clsCommissionParHeure As New CommissionParHeure


        If tbIDHoraire.Text = 0 Then
            MsgBox("Vous devez sélectionner un élément dans la grille.", MsgBoxStyle.OkOnly, "Erreur")
            Exit Sub
        End If

        oHoraire.IDHoraire = tbIDHoraire.Text
        Dim result As DialogResult = MessageBox.Show("Êtes-vous sur? Supprimer cet enregistrement?", "Supprimer", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
        If result = DialogResult.Yes Then

            Dim bSucess As Boolean
            bSucess = oHoraireData.Delete(oHoraire)
            If bSucess = True Then
                grdParHeure.DataSource = oHoraireData.SelectAllByIDOrganisationIDLivreurGrille(nudOrganisationID.Text, cbLivreursH.EditValue)
                bSupprimerChargeParHeure.Enabled = False
                bMiseAJourChargeParHeure.Enabled = False
                MessageBox.Show("l'enregistrement a été supprimée")
            Else
                MsgBox("La supression a echouée.", MsgBoxStyle.OkOnly, "Erreur")
            End If
        End If
        Me.Cursor = System.Windows.Forms.Cursors.Default
    End Sub

    Private Sub tbKMFin1_EditValueChanged(sender As Object, e As EventArgs)

    End Sub

    Private Sub tbChargeExtraParNbKm1_EditValueChanged(sender As Object, e As EventArgs)

    End Sub

    Private Sub bAjouterChargesParKmCL_Click(sender As Object, e As EventArgs) Handles bAjouterChargesParKmCL.Click



        If tbKMDebutCL.Text < 0 Then
            MsgBox("Vous devez entrer un Kilomètre de départ.", MsgBoxStyle.OkOnly, "Erreur")
            tbKMDebutCL.Focus()
            Exit Sub
        End If

        If tbKMFinCL.Text < 0 Then
            MsgBox("Vous devez entrer un Kilomètre de fin.", MsgBoxStyle.OkOnly, "Erreur")
            tbKMFinCL.Focus()
            Exit Sub
        End If

        If tbMontantChargeCL.Text < 1 Then
            MsgBox("Vous devez entrer un montant.", MsgBoxStyle.OkOnly, "Erreur")
            tbMontantChargeCL.Focus()
            Exit Sub
        End If

        If optCalculationOrganisations.SelectedIndex = -1 Then
            MsgBox("Vous devez sélectionner une méthode de calcul sur le tab Détail de l'organisation.", MsgBoxStyle.OkOnly, "Erreur")
            optCalculationOrganisations.Focus()
            Exit Sub
        End If


        With oCommissionKMLivreur

            .OrganisationID = nudOrganisationID.Text
            .Organisation = If(String.IsNullOrEmpty(tbOrganisation.Text), Nothing, tbOrganisation.Text)
            .KMFrom = If(String.IsNullOrEmpty(tbKMDebutCL.Text), Nothing, tbKMDebutCL.Text)
            .KMTo = If(String.IsNullOrEmpty(tbKMFinCL.Text), Nothing, tbKMFinCL.Text)
            .Montant = If(String.IsNullOrEmpty(tbMontantChargeCL.Text), Nothing, tbMontantChargeCL.Text)
            .Organisation = If(String.IsNullOrEmpty(tbOrganisation.Text), Nothing, tbOrganisation.Text)
            .PerKMNumber = If(String.IsNullOrEmpty(tbChargeExtraParNbKmCL.Text), Nothing, tbChargeExtraParNbKmCL.Text)
            .ExtraChargeParKM = If(String.IsNullOrEmpty(tbChargeExtraCL.Text), Nothing, tbChargeExtraCL.Text)
            If optCalculationOrganisations.SelectedIndex > -1 Then
                .MetodeCalculation = optCalculationOrganisations.SelectedIndex
            Else
                .MetodeCalculation = Nothing
            End If
        End With

        If tbHeureDepartCL.Text <> "00:00" Then
            oCommissionKMLivreur.HeureDepart = If(String.IsNullOrEmpty(tbHeureDepartCL.Text), Nothing, tbHeureDepartCL.Text)
            oCommissionKMLivreur.HeureDepart = oCommissionKMLivreur.HeureDepart.ToString.Replace("0001", Now.Year)
        Else
            oCommissionKMLivreur.HeureDepart = Nothing
        End If


        If tbHeureFinCL.Text <> "00:00" Then
            oCommissionKMLivreur.HeureFin = If(String.IsNullOrEmpty(tbHeureFinCL.Text), Nothing, tbHeureFinCL.Text)
            oCommissionKMLivreur.HeureFin = oCommissionKMLivreur.HeureFin.ToString.Replace("0001", Now.Year)
        Else
            oCommissionKMLivreur.HeureFin = Nothing
        End If



        Dim bSucess As Boolean
        bSucess = oCommissionKMLivreurData.Add(oCommissionKMLivreur)
        If bSucess = True Then
            grdParKMCL.DataSource = oCommissionKMLivreurData.SelectAllByName(nudOrganisationID.Text)

            GridView5.ClearSorting()
            GridView5.Columns("IDCommissionKMLivreur").SortOrder = DevExpress.Data.ColumnSortOrder.Ascending
            MessageBox.Show("l'enregistrement a été sauvegardée")
        Else
            MsgBox("L'insertion a échouée.", MsgBoxStyle.OkOnly, "Erreur")
        End If
    End Sub

    Private Sub bSupprimerChargesParKmCL_Click(sender As Object, e As EventArgs) Handles bSupprimerChargesParKmCL.Click
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        Dim clsCommissionKM As New CommissionKMLivreur

        Dim row As System.Data.DataRow = GridView5.GetDataRow(GridView5.FocusedRowHandle)
        clsCommissionKM.IDCommissionKMLivreur = System.Convert.ToInt32(row("IDCommissionKMLivreur").ToString)
        Dim result As DialogResult = MessageBox.Show("Êtes-vous sur? Supprimer cet enregistrement?", "Supprimer", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
        If result = DialogResult.Yes Then

            Dim bSucess As Boolean
            bSucess = oCommissionKMLivreurData.Delete(clsCommissionKM)
            If bSucess = True Then
                grdParKMCL.DataSource = oCommissionKMLivreurData.SelectAllByName(nudOrganisationID.Text)

                GridView5.ClearSorting()
                GridView5.Columns("IDCommissionKMLivreur").SortOrder = DevExpress.Data.ColumnSortOrder.Ascending
                bSupprimerChargesParKmCL.Enabled = False
                bMiseAJourChargesParKmCL.Enabled = False
                MessageBox.Show("l'enregistrement a été supprimée")
            Else
                MsgBox("La supression a echouée.", MsgBoxStyle.OkOnly, "Erreur")
            End If
        End If
        Me.Cursor = System.Windows.Forms.Cursors.Default
    End Sub

    Private Sub grdParKM_Click(sender As Object, e As EventArgs) Handles grdParKM.Click
        If GridView2.FocusedRowHandle < 0 Then Exit Sub

        Dim row As System.Data.DataRow = GridView2.GetDataRow(GridView2.FocusedRowHandle)

        bSupprimerChargesParKm.Enabled = True
        bMiseaJourChargesParKm.Enabled = True

        If Not IsDBNull(row("IDCommissionKM")) Then
            tbIDCommissionKM.Text = row("IDCommissionKM")

        End If
        If Not IsDBNull(row("KMFrom")) Then
            tbKMDebut.Text = row("KMFrom")
        Else
            tbKMDebut.Text = 0
        End If
        If Not IsDBNull(row("Montant")) Then
            tbMontantCharge.Text = row("Montant")
        Else
            tbMontantCharge.Text = 0
        End If

        If Not IsDBNull(row("KMTo")) Then
            tbKMFin.Text = row("KMTo")
        Else
            tbKMFin.Text = 0
        End If

        If Not IsDBNull(row("ExtraChargeParKM")) Then
            tbChargeExtra.Text = row("ExtraChargeParKM")
        Else
            tbChargeExtra.Text = 0
        End If

        If Not IsDBNull(row("PerKMNumber")) Then
            tbChargeExtraParNbKm.Text = row("PerKMNumber")
        Else
            tbChargeExtraParNbKm.Text = 0
        End If

        If Not IsDBNull(row("HeureDepart")) Then
            tbHeureDepart.EditValue = row("HeureDepart")
        Else
            tbHeureDepart.EditValue = "00:00"
        End If

        If Not IsDBNull(row("HeureFin")) Then
            tbHeureFin.EditValue = row("HeureFin")
        Else
            tbHeureFin.EditValue = "00:00"
        End If





    End Sub

    Private Sub grdParKMCL_Click(sender As Object, e As EventArgs) Handles grdParKMCL.Click

        If GridView5.FocusedRowHandle < 0 Then Exit Sub

        Dim row As System.Data.DataRow = GridView5.GetDataRow(GridView5.FocusedRowHandle)

        bSupprimerChargesParKmCL.Enabled = True
        bMiseAJourChargesParKmCL.Enabled = True

        If Not IsDBNull(row("IDCommissionKMLivreur")) Then
            tbIDCommissionKMCL.Text = row("IDCommissionKMLivreur")

        End If
        If Not IsDBNull(row("KMFrom")) Then
            tbKMDebutCL.Text = row("KMFrom")
        Else
            tbKMDebutCL.Text = 0
        End If
        If Not IsDBNull(row("Montant")) Then
            tbMontantChargeCL.Text = row("Montant")
        Else
            tbMontantChargeCL.Text = 0
        End If

        If Not IsDBNull(row("KMTo")) Then
            tbKMFinCL.Text = row("KMTo")
        Else
            tbKMFinCL.Text = 0
        End If

        If Not IsDBNull(row("ExtraChargeParKM")) Then
            tbChargeExtraCL.Text = row("ExtraChargeParKM")
        Else
            tbChargeExtraCL.Text = 0
        End If

        If Not IsDBNull(row("PerKMNumber")) Then
            tbChargeExtraParNbKmCL.Text = row("PerKMNumber")
        Else
            tbChargeExtraParNbKmCL.Text = 0
        End If

        If Not IsDBNull(row("HeureDepart")) Then
            tbHeureDepartCL.EditValue = row("HeureDepart")
        Else
            tbHeureDepartCL.EditValue = "00:00"
        End If

        If Not IsDBNull(row("HeureFin")) Then
            tbHeureFinCL.EditValue = row("HeureFin")
        Else
            tbHeureFinCL.EditValue = "00:00"
        End If

    End Sub

    Private Sub bAjouterMP_Click(sender As Object, e As EventArgs) Handles bAjouterMP.Click
        If tbNombreLivraisons.Text = 0 Then
            MsgBox("Vous devez entrer un nombre de livraison.", MsgBoxStyle.OkOnly, "Erreur")
            tbNombreLivraisons.Focus()
            Exit Sub
        End If

        If tbMaximumPayable.Text < 1 Then
            MsgBox("Vous devez entrer un maximum payable.", MsgBoxStyle.OkOnly, "Erreur")
            tbMaximumPayable.Focus()
            Exit Sub
        End If


        With oMaxPayable
            .OrganisationID = nudOrganisationID.Text
            .Organisation = If(String.IsNullOrEmpty(tbOrganisation.Text), Nothing, tbOrganisation.Text)
            .NombreLivraison = If(String.IsNullOrEmpty(tbNombreLivraisons.Text), Nothing, tbNombreLivraisons.Text)
            .MaxPayable = If(String.IsNullOrEmpty(tbMaximumPayable.Text), Nothing, tbMaximumPayable.Text)
        End With

        Dim bSucess As Boolean
        bSucess = oMaxPayableData.Add(oMaxPayable)
        If bSucess = True Then
            grdMaxPayable.DataSource = oMaxPayableData.SelectAllByID(nudOrganisationID.Text)
            MessageBox.Show("l'enregistrement a été sauvegardée")
        Else
            MsgBox("L'insertion a échouée.", MsgBoxStyle.OkOnly, "Erreur")
        End If
    End Sub

    Private Sub bSupprimerMP_Click(sender As Object, e As EventArgs) Handles bSupprimerMP.Click
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        Dim clsMaxPayable As New MaxPayable

        Dim row As System.Data.DataRow = GridView4.GetDataRow(GridView4.FocusedRowHandle)
        clsMaxPayable.IDMaxPayable = System.Convert.ToInt32(row("IDMaxPayable").ToString)
        Dim result As DialogResult = MessageBox.Show("Êtes-vous sur? Supprimer cet enregistrement?", "Supprimer", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
        If result = DialogResult.Yes Then

            Dim bSucess As Boolean
            bSucess = oMaxPayableData.Delete(clsMaxPayable)
            If bSucess = True Then
                grdMaxPayable.DataSource = oMaxPayableData.SelectAllByID(nudOrganisationID.Text)
                bSupprimerMP.Enabled = False
                bMiseAJourMP.Enabled = False
                MessageBox.Show("l'enregistrement a été supprimée")
            Else
                MsgBox("La supression a echouée.", MsgBoxStyle.OkOnly, "Erreur")
            End If
        End If
        Me.Cursor = System.Windows.Forms.Cursors.Default
    End Sub

    Private Sub bMiseAJourChargesParKmCL_Click(sender As Object, e As EventArgs) Handles bMiseAJourChargesParKmCL.Click


        Dim SelectedRowParKm As Integer = GridView5.FocusedRowHandle

        If tbKMDebutCL.Text < 0 Then
            MsgBox("Vous devez entrer un Kilomètre de départ.", MsgBoxStyle.OkOnly, "Erreur")
            tbKMDebutCL.Focus()
            Exit Sub
        End If

        If tbKMFinCL.Text < 0 Then
            MsgBox("Vous devez entrer un Kilomètre de fin.", MsgBoxStyle.OkOnly, "Erreur")
            tbKMFinCL.Focus()
            Exit Sub
        End If

        If tbMontantChargeCL.Text < 0 Then
            MsgBox("Vous devez entrer un montant.", MsgBoxStyle.OkOnly, "Erreur")
            tbMontantChargeCL.Focus()
            Exit Sub
        End If

        If optCalculationOrganisations.SelectedIndex = -1 Then
            MsgBox("Vous devez sélectionner une méthode de calcul sur le tab Détail de l'organisation.", MsgBoxStyle.OkOnly, "Erreur")
            optCalculationOrganisations.Focus()
            Exit Sub
        End If



        oCommissionKMLivreur.OrganisationID = nudOrganisationID.Text
        oCommissionKMLivreur.Organisation = If(String.IsNullOrEmpty(tbOrganisation.Text), Nothing, tbOrganisation.Text)
        oCommissionKMLivreur.IDCommissionKMLivreur = If(String.IsNullOrEmpty(tbIDCommissionKMCL.Text), Nothing, tbIDCommissionKMCL.Text)
        oCommissionKMLivreur.KMFrom = If(String.IsNullOrEmpty(tbKMDebutCL.Text), Nothing, tbKMDebutCL.Text)
        oCommissionKMLivreur.KMTo = If(String.IsNullOrEmpty(tbKMFinCL.Text), Nothing, tbKMFinCL.Text)
        oCommissionKMLivreur.Montant = If(String.IsNullOrEmpty(tbMontantChargeCL.Text), Nothing, Convert.ToDouble(tbMontantChargeCL.EditValue))
        oCommissionKMLivreur.Organisation = If(String.IsNullOrEmpty(tbOrganisation.Text), Nothing, tbOrganisation.Text)
        oCommissionKMLivreur.PerKMNumber = If(String.IsNullOrEmpty(tbChargeExtraParNbKmCL.Text), Nothing, Convert.ToDouble(tbChargeExtraParNbKmCL.EditValue))
        oCommissionKMLivreur.ExtraChargeParKM = If(String.IsNullOrEmpty(tbChargeExtraCL.Text), Nothing, Convert.ToDouble(tbChargeExtraCL.EditValue))

        If optCalculationOrganisations.SelectedIndex > -1 Then
            oCommissionKMLivreur.MetodeCalculation = optCalculationOrganisations.SelectedIndex
        Else
            oCommissionKMLivreur.MetodeCalculation = Nothing
        End If

        If tbHeureDepartCL.Text <> "00:00" Then
            oCommissionKMLivreur.HeureDepart = If(String.IsNullOrEmpty(tbHeureDepartCL.Text), Nothing, tbHeureDepartCL.Text)
            oCommissionKMLivreur.HeureDepart = oCommissionKMLivreur.HeureDepart.ToString.Replace("0001", Now.Year)
        Else
            oCommissionKMLivreur.HeureDepart = Nothing
        End If


        If tbHeureFinCL.Text <> "00:00" Then
            oCommissionKMLivreur.HeureFin = If(String.IsNullOrEmpty(tbHeureFinCL.Text), Nothing, tbHeureFinCL.Text)
            oCommissionKMLivreur.HeureFin = oCommissionKMLivreur.HeureFin.ToString.Replace("0001", Now.Year)
        Else
            oCommissionKMLivreur.HeureFin = Nothing
        End If




        Dim bSucess As Boolean
        bSucess = oCommissionKMLivreurData.Update(oCommissionKMLivreur, oCommissionKMLivreur)
        If bSucess = True Then
            grdParKMCL.DataSource = oCommissionKMLivreurData.SelectAllByName(nudOrganisationID.Text)


            GridView5.ClearSorting()
            GridView5.Columns("IDCommissionKMLivreur").SortOrder = DevExpress.Data.ColumnSortOrder.Ascending
            GridView5.FocusedRowHandle = SelectedRowParKm
            bSupprimerChargesParKmCL.Enabled = False
            bMiseAJourChargesParKmCL.Enabled = False
            MessageBox.Show("l'enregistrement a été sauvegardée")
        Else
            MsgBox("L'insertion a échouée.", MsgBoxStyle.OkOnly, "Erreur")
        End If
    End Sub

    Private Sub bMiseAJourMP_Click(sender As Object, e As EventArgs) Handles bMiseAJourMP.Click

        Dim SelectedRowMP As Integer = GridView4.FocusedRowHandle

        If tbNombreLivraisons.Text < 1 Then
            MsgBox("Vous devez entrer un Kilomètre de départ.", MsgBoxStyle.OkOnly, "Erreur")
            tbKMDebutCL.Focus()
            Exit Sub
        End If

        If tbMaximumPayable.Text < 1 Then
            MsgBox("Vous devez entrer un Kilomètre de fin.", MsgBoxStyle.OkOnly, "Erreur")
            tbKMFinCL.Focus()
            Exit Sub
        End If

        With oMaxPayable
            .OrganisationID = nudOrganisationID.Text
            .Organisation = If(String.IsNullOrEmpty(tbOrganisation.Text), Nothing, tbOrganisation.Text)
            .IDMaxPayable = If(String.IsNullOrEmpty(tbIDMaxPayable.Text), Nothing, tbIDMaxPayable.Text)
            .NombreLivraison = If(String.IsNullOrEmpty(tbNombreLivraisons.Text), Nothing, tbNombreLivraisons.Text)
            .MaxPayable = If(String.IsNullOrEmpty(tbMaximumPayable.Text), Nothing, tbMaximumPayable.Text)
        End With

        Dim bSucess As Boolean
        bSucess = oMaxPayableData.Update(oMaxPayable, oMaxPayable)
        If bSucess = True Then
            grdMaxPayable.DataSource = oMaxPayableData.SelectAllByID(nudOrganisationID.Text)
            bSupprimerMP.Enabled = False
            bMiseAJourMP.Enabled = False
            GridView4.FocusedRowHandle = SelectedRowMP
            MessageBox.Show("l'enregistrement a été sauvegardée")
        Else
            MsgBox("La mise a jour a échouée.", MsgBoxStyle.OkOnly, "Erreur")
        End If
    End Sub

    Private Sub bMiseAJourChargeParHeure_Click(sender As Object, e As EventArgs) Handles bMiseAJourChargeParHeure.Click
        Dim SelectedRow As Integer = GridView3.FocusedRowHandle


        If tbIDHoraire.Text = 0 Then
            MsgBox("Vous devez sélectionner un élément dans la grille.", MsgBoxStyle.OkOnly, "Erreur")
            Exit Sub
        End If


        If IsNothing(cbLivreursH) Then
            MsgBox("Vous devez sélectionner un livreur.", MsgBoxStyle.OkOnly, "Erreur")
            cbLivreursH.Focus()
            Exit Sub
        End If



        If tbMontantParHeureLivreurH.Text < 0 Then
            MsgBox("Vous devez entrer un montant.", MsgBoxStyle.OkOnly, "Erreur")
            tbMontantParHeureLivreurH.Focus()
            Exit Sub
        End If

        If tbMontantParHeureOrganisationH.Text < 0 Then
            MsgBox("Vous devez entrer un montant.", MsgBoxStyle.OkOnly, "Erreur")
            tbMontantParHeureLivreurH.Focus()
            Exit Sub
        End If



        With oHoraire
            .IDHoraire = tbIDHoraire.Text
            .IDOrganisation = nudOrganisationID.Text
            .Organisation = If(String.IsNullOrEmpty(tbOrganisation.Text), Nothing, tbOrganisation.Text)
            .IDLivreur = cbLivreursH.EditValue
            .Livreur = If(String.IsNullOrEmpty(cbLivreursH.Text), Nothing, cbLivreursH.Text)
            .MontantLivreur = If(String.IsNullOrEmpty(tbMontantParHeureLivreurH.Text), Nothing, tbMontantParHeureLivreurH.Text)
            .MontantOrganisation = If(String.IsNullOrEmpty(tbMontantParHeureOrganisationH.Text), Nothing, tbMontantParHeureOrganisationH.Text)
            .IDPeriode = go_Globals.IDPeriode

            .HeureDepart = If(String.IsNullOrEmpty(tbHeureDepartH.Text), Nothing, tbHeureDepartH.Text)

            .HeureFin = If(String.IsNullOrEmpty(tbHeureFinH.Text), Nothing, tbHeureFinH.Text)


        End With

        Dim bSucess As Boolean
        bSucess = oHoraireData.Update(oHoraire, oHoraire)
        If bSucess = True Then
            grdParHeure.DataSource = oHoraireData.SelectAllByIDOrganisationIDLivreurGrille(nudOrganisationID.Text, cbLivreursH.EditValue)
            bSupprimerChargeParHeure.Enabled = False
            bMiseAJourChargeParHeure.Enabled = False
            MessageBox.Show("l'enregistrement a été sauvegardée")
        Else
            MsgBox("L'insertion a échouée.", MsgBoxStyle.OkOnly, "Erreur")
        End If

    End Sub

    Private Sub bMiseaJourChargesParKm_Click(sender As Object, e As EventArgs) Handles bMiseaJourChargesParKm.Click
        Dim SelectedRowParKm As Integer = GridView2.FocusedRowHandle


        If tbKMDebut.Text < 0 Then
            MsgBox("Vous devez entrer un Kilomètre de départ.", MsgBoxStyle.OkOnly, "Erreur")
            tbKMDebut.Focus()
            Exit Sub
        End If

        If tbKMFin.Text = 0 Then
            MsgBox("Vous devez entrer un Kilomètre de fin.", MsgBoxStyle.OkOnly, "Erreur")
            tbKMFin.Focus()
            Exit Sub
        End If

        If tbMontantCharge.Text < 0 Then
            MsgBox("Vous devez entrer un montant.", MsgBoxStyle.OkOnly, "Erreur")
            tbMontantCharge.Focus()
            Exit Sub
        End If

        If optCalculationOrganisations.SelectedIndex = -1 Then
            MsgBox("Vous devez sélectionner une méthode de calcul sur le tab Détail de l'organisation.", MsgBoxStyle.OkOnly, "Erreur")
            optCalculationOrganisations.Focus()
            Exit Sub
        End If


        With oCommissionKM
            .OrganisationID = nudOrganisationID.Text
            .Organisation = If(String.IsNullOrEmpty(tbOrganisation.Text), Nothing, tbOrganisation.Text)
            .IDCommissionKM = If(String.IsNullOrEmpty(tbIDCommissionKM.Text), Nothing, tbIDCommissionKM.Text)
            .KMFrom = If(String.IsNullOrEmpty(tbKMDebut.Text), Nothing, tbKMDebut.Text)
            .KMTo = If(String.IsNullOrEmpty(tbKMFin.Text), Nothing, tbKMFin.Text)
            .Montant = If(String.IsNullOrEmpty(tbMontantCharge.Text), Nothing, tbMontantCharge.Text)
            .Organisation = If(String.IsNullOrEmpty(tbOrganisation.Text), Nothing, tbOrganisation.Text)
            .PerKMNumber = If(String.IsNullOrEmpty(tbChargeExtraParNbKm.Text), Nothing, tbChargeExtraParNbKm.Text)
            .ExtraChargeParKM = If(String.IsNullOrEmpty(tbChargeExtra.Text), Nothing, tbChargeExtra.Text)

            If optCalculationOrganisations.SelectedIndex > -1 Then
                .MetodeCalculation = optCalculationOrganisations.SelectedIndex
            Else
                .MetodeCalculation = Nothing
            End If

            If tbHeureDepart.Text <> "00:00" Then
                oCommissionKM.HeureDepart = If(String.IsNullOrEmpty(tbHeureDepart.Text), Nothing, tbHeureDepart.Text)
                oCommissionKM.HeureDepart = oCommissionKM.HeureDepart.ToString.Replace("0001", Now.Year)
            Else
                oCommissionKM.HeureDepart = Nothing
            End If


            If tbHeureFin.Text <> "00:00" Then
                oCommissionKM.HeureFin = If(String.IsNullOrEmpty(tbHeureFin.Text), Nothing, tbHeureFin.Text)
                oCommissionKM.HeureFin = oCommissionKM.HeureFin.ToString.Replace("0001", Now.Year)
            Else
                oCommissionKM.HeureFin = Nothing
            End If


        End With

        Dim bSucess As Boolean
        bSucess = oCommissionKMData.Update(oCommissionKM, oCommissionKM)
        If bSucess = True Then
            grdParKM.DataSource = oCommissionKMData.SelectAllByName(nudOrganisationID.Text)
            GridView2.ClearSorting()
            GridView2.Columns("IDCommissionKM").SortOrder = DevExpress.Data.ColumnSortOrder.Ascending
            GridView2.FocusedRowHandle = SelectedRowParKm
            bSupprimerChargesParKm.Enabled = False
            bMiseaJourChargesParKm.Enabled = False
            MessageBox.Show("l'enregistrement a été sauvegardée")
        Else
            MsgBox("L'insertion a échouée.", MsgBoxStyle.OkOnly, "Erreur")
        End If
    End Sub

    Private Sub tbIDParHeure_EditValueChanged(sender As Object, e As EventArgs) Handles tbIDHoraire.EditValueChanged

    End Sub

    Private Sub grdParHeure_Click(sender As Object, e As EventArgs)
        If GridView3.FocusedRowHandle < 0 Then Exit Sub

        Dim row As System.Data.DataRow = GridView3.GetDataRow(GridView3.FocusedRowHandle)

        bSupprimerChargeParHeure.Enabled = True
        bMiseAJourChargeParHeure.Enabled = True

        If Not IsDBNull(row("IDCommissionParHeure")) Then
            tbIDHoraire.Text = row("IDCommissionParHeure")
        End If
        If Not IsDBNull(row("Livreur")) Then
            cbLivreursH.Text = row("Livreur")
        Else
            cbLivreursH.Text = ""
        End If
        If Not IsDBNull(row("MontantParHeure")) Then
            tbMontantParHeureLivreurH.Text = row("MontantParHeure")
        Else
            tbMontantParHeureLivreurH.Text = 0
        End If




    End Sub

    Private Sub grdMaxPayable_Click(sender As Object, e As EventArgs) Handles grdMaxPayable.Click

        If GridView4.FocusedRowHandle < 0 Then Exit Sub

        Dim row As System.Data.DataRow = GridView4.GetDataRow(GridView4.FocusedRowHandle)

        bSupprimerMP.Enabled = True
        bMiseAJourMP.Enabled = True

        If Not IsDBNull(row("IDMaxPayable")) Then
            tbIDMaxPayable.Text = row("IDMaxPayable")

        End If
        If Not IsDBNull(row("NombreLivraison")) Then
            tbNombreLivraisons.Text = row("NombreLivraison")
        Else
            tbNombreLivraisons.Text = 0
        End If
        If Not IsDBNull(row("MaxPayable")) Then
            tbMaximumPayable.Text = row("MaxPayable")
        Else
            tbMaximumPayable.Text = 0
        End If


    End Sub


    Private Sub optCalculationLivreur0()

        grdMaxPayable.DataSource = oMaxPayableData.SelectAllByID(nudOrganisationID.Text)
        GridView4.Columns("NombreLivraison").Width = 50

        grdParKMCL.DataSource = oCommissionKMLivreurData.SelectAllByName(nudOrganisationID.Text)


        GridView5.ClearSorting()
        GridView5.Columns("IDCommissionKMLivreur").SortOrder = DevExpress.Data.ColumnSortOrder.Ascending
        GridView5.Columns("IDCommissionKMLivreur").Visible = False
        GridView5.Columns("Organisation").Visible = False
        GridView5.Columns("PerKMNumber").Visible = True
        GridView5.Columns("HeureDepart").Visible = False
        GridView5.Columns("HeureFin").Visible = False
        GridView5.Columns("ExtraChargeParKM").Visible = True
        GridView5.FocusedRowHandle = 1

        LayoutChargesSupplementaires.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always
        LayoutParNombreKmCL.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always
        LayoutParHeuresCL.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never

        tabData.TabPages(4).PageVisible = True
        tabData.TabPages(5).PageVisible = True

        tabData.TabPages(4).Text = "Gestion des montants par Kilométrage / Livreur"


    End Sub

    Private Sub optCalculationLivreur1()
        LayoutContrat.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always
        LayoutPayeAuLivreur.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always
    End Sub


    Private Sub optCalculationLivreur2()

        If nudOrganisationID.Text.ToString.Trim <> "" Then
            grdParKMCL.DataSource = oCommissionKMLivreurData.SelectAllByName(nudOrganisationID.Text)

        End If



        GridView5.ClearSorting()
        GridView5.Columns("IDCommissionKMLivreur").SortOrder = DevExpress.Data.ColumnSortOrder.Ascending
        GridView5.Columns("IDCommissionKMLivreur").Visible = False
        GridView5.Columns("Organisation").Visible = False
        GridView5.Columns("PerKMNumber").Visible = False
        GridView5.Columns("HeureDepart").Visible = False
        GridView5.Columns("HeureFin").Visible = False
        GridView5.Columns("ExtraChargeParKM").Visible = False
        GridView5.FocusedRowHandle = 1

        LayoutChargesSupplementairesCL.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never
        LayoutParHeuresCL.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never

        tabData.TabPages(4).PageVisible = True
        tabData.TabPages(4).Text = "Gestion des montants par Kilométrage / Livreur"

    End Sub

    Private Sub optCalculationLivreur3()

        If nudOrganisationID.Text.ToString.Trim <> "" Then
            grdParKMCL.DataSource = oCommissionKMLivreurData.SelectAllByName(nudOrganisationID.Text)
        End If

        GridView5.ClearSorting()
        GridView5.Columns("IDCommissionKMLivreur").SortOrder = DevExpress.Data.ColumnSortOrder.Ascending
        GridView5.Columns("IDCommissionKMLivreur").Visible = False
        GridView5.Columns("Organisation").Visible = False
        GridView5.Columns("PerKMNumber").Visible = True
        GridView5.Columns("HeureDepart").Visible = False
        GridView5.Columns("HeureFin").Visible = False
        GridView5.Columns("ExtraChargeParKM").Visible = True
        GridView5.FocusedRowHandle = 1

        LayoutChargesSupplementairesCL.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always
        LayoutParNombreKmCL.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never
        LayoutParHeuresCL.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never
        LayoutParNombreKmCL.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always

        tabData.TabPages(4).PageVisible = True
        tabData.TabPages(4).Text = "Gestion des montants par Kilométrage + Extras / Livreur"

    End Sub

    Private Sub optCalculationLivreur4()

        grdParKMCL.DataSource = oCommissionKMLivreurData.SelectAllByName(nudOrganisationID.Text)

        GridView5.ClearSorting()
        GridView5.Columns("IDCommissionKMLivreur").SortOrder = DevExpress.Data.ColumnSortOrder.Ascending
        GridView5.Columns("IDCommissionKMLivreur").Visible = False
        GridView5.Columns("Organisation").Visible = False
        GridView5.Columns("ExtraChargeParKM").Visible = True
        GridView5.Columns("PerKMNumber").Visible = True
        GridView5.Columns("HeureDepart").Visible = True
        GridView5.Columns("HeureFin").Visible = True



        GridView5.Columns("KMFrom").VisibleIndex = 1
        GridView5.Columns("KMTo").VisibleIndex = 2
        GridView5.Columns("Montant").VisibleIndex = 3
        GridView5.Columns("HeureDepart").VisibleIndex = 4
        GridView5.Columns("HeureFin").VisibleIndex = 5
        GridView5.Columns("ExtraChargeParKM").VisibleIndex = 6
        GridView5.Columns("PerKMNumber").VisibleIndex = 7

        GridView5.FocusedRowHandle = 1

        LayoutChargesSupplementairesCL.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always
        LayoutParNombreKmCL.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always
        LayoutParHeuresCL.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always

        tabData.TabPages(4).PageVisible = True

        tabData.TabPages(4).Text = "Gestion des montants par Kilométrage + Xtra(Heure) / Livreur"

        tbHeureDepartCL.EditValue = "00:00"
        tbHeureFinCL.EditValue = "00:00"


    End Sub

    Private Sub optCalculationLivreur5()

        grdParKMCL.DataSource = oCommissionKMLivreurData.SelectAllByName(nudOrganisationID.Text)
        GridView5.ClearSorting()
        GridView5.FocusedRowHandle = 1

        LayoutChargesSupplementairesCL.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always
        LayoutParNombreKmCL.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always
        LayoutParHeuresCL.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never

        tabData.TabPages(4).PageVisible = True

        tabData.TabPages(4).Text = "Gestion des montants par Kilométrage fixe + Xtra / Livreur"

        tbHeureDepartCL.EditValue = "00:00"
        tbHeureFinCL.EditValue = "00:00"

        GridView5.Columns("ExtraChargeParKM").Visible = True
        GridView5.Columns("PerKMNumber").Visible = True
        GridView5.Columns("HeureDepart").Visible = False
        GridView5.Columns("HeureFin").Visible = False

    End Sub


    Private Sub optCalculationLivreur6()


        tabData.TabPages(9).PageVisible = True
        tbDateJ.Text = go_Globals.PeriodeDateFrom.ToShortDateString



    End Sub
    Private Sub optCalculationLivreur8()

        LayoutContrat.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always
        LayoutPayeAuLivreur.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always
        LayoutMontantParPorteL.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always


    End Sub
    Private Sub optCalculationLivreur7()

        tabData.TabPages(3).Text = "Gestion des paiements par heure"
        'grdParHeure.DataSource = oCommissionParHeureData.SelectAllByName(nudOrganisationID.Text)
        tabData.TabPages(3).PageVisible = True
        tbHeureDepartH.Text = go_Globals.PeriodeDateFrom
        tbHeureFinH.Text = go_Globals.PeriodeDateTo

    End Sub

    Private Sub optCalculationLivreur_SelectedIndexChanged(sender As Object, e As EventArgs) Handles optCalculationLivreur.SelectedIndexChanged


        If optCalculationOrganisations.SelectedIndex = -1 Or optCalculationLivreur.SelectedIndex = -1 Then
            Exit Sub
        End If

        ResetSections()

        Select Case optCalculationLivreur.SelectedIndex

            Case 0 ' Pharmaplus/ massicotte
                optCalculationLivreur0()
            Case 1 ' prix fixe
                optCalculationLivreur1()
            Case 2 ' Par kilometrage
                optCalculationLivreur2()
            Case 3 'Par kilometrage + extras
                optCalculationLivreur3()
            Case 4 'Par kilometrage + extras(heures)
                optCalculationLivreur4()
            Case 5 'Par kilometrage fixe + extras
                optCalculationLivreur5()
            Case 6 ' par jour
                optCalculationLivreur6()
            Case 7 ' par heure
                optCalculationLivreur7()
                tbHeureDepartH.Text = go_Globals.PeriodeDateFrom
                tbHeureFinH.Text = go_Globals.PeriodeDateFrom
            Case 8 ' par heure
                optCalculationLivreur8()


        End Select

        Select Case optCalculationOrganisations.SelectedIndex
            Case 0
                optCalculationOrganisation0()
            Case 1
                optCalculationOrganisation1()
            Case 2
                optCalculationOrganisation2()
            Case 3
                optCalculationOrganisation3()
            Case 4
                optCalculationOrganisation4()
            Case 5
                optCalculationOrganisation5()
            Case 6
                optCalculationOrganisation6()
            Case 7
                optCalculationOrganisation7()
                tbHeureDepartH.Text = go_Globals.PeriodeDateFrom
                tbHeureFinH.Text = go_Globals.PeriodeDateFrom
            Case 8
                optCalculationOrganisation8()
            Case 9
                optCalculationOrganisation9()


        End Select
        tabData.TabPages(8).PageVisible = True

    End Sub
    Private Sub optCalculationOrganisation0()

        LayoutChargesSupplementaires.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always
        LayoutChargeExtra.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always
        LayoutParHeures.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never
        LayoutParNombreKm.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always
        LayoutMontantContrat.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never
        LayoutContrat.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always
        LayoutPayeAuLivreur.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always


        tabData.TabPages(2).PageVisible = True
        tabData.TabPages(2).Text = "Par Kilométrage + Xtra / Organisations"

        grdParKM.DataSource = oCommissionKMData.SelectAllByName(nudOrganisationID.Text)
        GridView2.ClearSorting()
        GridView2.Columns("IDCommissionKM").SortOrder = DevExpress.Data.ColumnSortOrder.Ascending


        GridView2.Columns("KMFrom").Visible = True
        GridView2.Columns("KMTo").Visible = True
        GridView2.Columns("IDCommissionKM").Visible = False
        GridView2.Columns("Organisation").Visible = False
        GridView2.Columns("ExtraChargeParKM").Visible = True
        GridView2.Columns("PerKMNumber").Visible = True
        GridView2.Columns("HeureDepart").Visible = False
        GridView2.Columns("HeureFin").Visible = False

    End Sub

    Private Sub optCalculationOrganisation1()


        LayoutContrat.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always
        LayoutMontantContrat.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always


    End Sub
    Private Sub optCalculationOrganisation2()

        LayoutChargesSupplementaires.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never
        LayoutParNombreKm.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never

        grdParKM.DataSource = oCommissionKMData.SelectAllByName(nudOrganisationID.Text)
        tabData.TabPages(2).Text = "Par Kilométrage / Organisation"

        GridView2.ClearSorting()
        GridView2.Columns("IDCommissionKM").SortOrder = DevExpress.Data.ColumnSortOrder.Ascending



        GridView2.Columns("KMFrom").Visible = True
        GridView2.Columns("KMTo").Visible = True
        GridView2.Columns("IDCommissionKM").Visible = False
        GridView2.Columns("Organisation").Visible = False

        GridView2.Columns("ExtraChargeParKM").Visible = False
        GridView2.Columns("PerKMNumber").Visible = False
        GridView2.Columns("HeureDepart").Visible = False
        GridView2.Columns("HeureFin").Visible = False

        tabData.TabPages(2).PageVisible = True

    End Sub

    Private Sub optCalculationOrganisation3()

        LayoutChargesSupplementaires.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always
        LayoutChargeExtra.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always
        LayoutParHeures.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never
        LayoutParNombreKm.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always


        tabData.TabPages(2).PageVisible = True
        tabData.TabPages(2).Text = "Par Kilométrage + Xtra / Organisations"

        grdParKM.DataSource = oCommissionKMData.SelectAllByName(nudOrganisationID.Text)

        GridView2.ClearSorting()
        GridView2.Columns("IDCommissionKM").SortOrder = DevExpress.Data.ColumnSortOrder.Ascending


        GridView2.Columns("KMFrom").Visible = True
        GridView2.Columns("KMTo").Visible = True
        GridView2.Columns("IDCommissionKM").Visible = False
        GridView2.Columns("Organisation").Visible = False
        GridView2.Columns("ExtraChargeParKM").Visible = True
        GridView2.Columns("PerKMNumber").Visible = True
        GridView2.Columns("HeureDepart").Visible = False
        GridView2.Columns("HeureFin").Visible = False


    End Sub

    Private Sub optCalculationOrganisation4()

        LayoutChargesSupplementaires.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always
        LayoutChargeExtra.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always
        LayoutParHeures.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always
        LayoutParNombreKm.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always


        tabData.TabPages(2).PageVisible = True
        tabData.TabPages(2).Text = "Par Kilométrage + Xtra(Heures) / Organisation"
        grdParKM.DataSource = oCommissionKMData.SelectAllByName(nudOrganisationID.Text)

        GridView2.ClearSorting()
        GridView2.Columns("IDCommissionKM").SortOrder = DevExpress.Data.ColumnSortOrder.Ascending


        GridView2.Columns("IDCommissionKM").Visible = False
        GridView2.Columns("KMFrom").Visible = True
        GridView2.Columns("KMTo").Visible = True
        GridView2.Columns("Organisation").Visible = False
        GridView2.Columns("ExtraChargeParKM").Visible = True
        GridView2.Columns("PerKMNumber").Visible = True
        GridView2.Columns("HeureDepart").Visible = True
        GridView2.Columns("HeureFin").Visible = True


        GridView2.Columns("KMFrom").VisibleIndex = 1
        GridView2.Columns("KMTo").VisibleIndex = 2
        GridView2.Columns("Montant").VisibleIndex = 3
        GridView2.Columns("HeureDepart").VisibleIndex = 4
        GridView2.Columns("HeureFin").VisibleIndex = 5
        GridView2.Columns("ExtraChargeParKM").VisibleIndex = 6
        GridView2.Columns("PerKMNumber").VisibleIndex = 7




    End Sub

    Private Sub optCalculationOrganisation5()

        LayoutChargesSupplementaires.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always
        LayoutChargeExtra.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always
        LayoutParNombreKm.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always
        LayoutParHeures.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never


        tabData.TabPages(2).PageVisible = True
        tabData.TabPages(2).Text = "Par nombre de KM fixe + extras / Organisation"
        grdParKM.DataSource = oCommissionKMData.SelectAllByName(nudOrganisationID.Text)

        tbHeureDepart.EditValue = "00:00"
        tbHeureFin.EditValue = "00:00"

        GridView2.Columns("IDCommissionKM").Visible = False
        GridView2.Columns("KMFrom").Visible = True
        GridView2.Columns("KMTo").Visible = True
        GridView2.Columns("Organisation").Visible = False
        GridView2.Columns("ExtraChargeParKM").Visible = True
        GridView2.Columns("PerKMNumber").Visible = True
        GridView2.Columns("ExtraChargeParKM").VisibleIndex = 5
        GridView2.Columns("PerKMNumber").VisibleIndex = 6
        GridView2.Columns("HeureDepart").VisibleIndex = 7
        GridView2.Columns("HeureFin").VisibleIndex = 8
        GridView2.Columns("HeureDepart").Visible = False
        GridView2.Columns("HeureFin").Visible = False

    End Sub

    Private Sub optCalculationOrganisation6()
        tabData.TabPages(9).PageVisible = True
        tbDateJ.Text = go_Globals.PeriodeDateFrom.ToShortDateString


    End Sub

    Private Sub optCalculationOrganisation9()


        LayoutContrat.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always
        LayoutMontantContrat.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always
        LayoutMontantParPorteO.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always

    End Sub
    Private Sub optCalculationOrganisation8()



        LayoutPrixFixeExtra.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always
        tbHeureDepartH.Text = go_Globals.PeriodeDateFrom
        tbHeureFinH.Text = go_Globals.PeriodeDateTo


    End Sub
    Private Sub optCalculationOrganisation7()

        'LayoutChargesSupplementaires.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never
        'LayoutParNombreKm.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never
        'LayoutKMDebut.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never
        'LayoutKMFin.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never

        'grdParKM.DataSource = oCommissionKMData.SelectAllByName(nudOrganisationID.Text)
        'tabData.TabPages(2).Text = "Par Heure / Organisation"

        'GridView2.Columns("KMFrom").Visible = False
        'GridView2.Columns("KMTo").Visible = False
        'GridView2.Columns("IDCommissionKM").Visible = False
        'GridView2.Columns("Organisation").Visible = False
        'GridView2.Columns("ExtraChargeParKM").Visible = False
        'GridView2.Columns("PerKMNumber").Visible = False
        'GridView2.Columns("HeureDepart").Visible = True
        'GridView2.Columns("HeureFin").Visible = True

        tabData.TabPages(3).PageVisible = True
        tbHeureDepartH.Text = go_Globals.PeriodeDateFrom
        tbHeureFinH.Text = go_Globals.PeriodeDateTo


    End Sub

    Private Sub ResetSections()

        tabData.TabPages(2).PageVisible = False
        tabData.TabPages(3).PageVisible = False
        tabData.TabPages(4).PageVisible = False
        tabData.TabPages(5).PageVisible = False
        tabData.TabPages(8).PageVisible = False
        tabData.TabPages(9).PageVisible = False


        LayoutPrixFixeExtra.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never
        LayoutContrat.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never
        LayoutMontantContrat.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never
        LayoutPayeAuLivreur.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never
        LayoutMontantParPorteO.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never
        LayoutMontantParPorteL.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never

        tbIDCommissionKM.Text = vbNullString
        tbKMDebut.Text = 0
        tbKMFin.Text = 0
        tbMontantCharge.Text = 0
        tbChargeExtra.Text = 0
        tbChargeExtraParNbKm.Text = 0
        tbHeureDepart.EditValue = "00:00"
        tbHeureFin.EditValue = "00:00"

        tbIDHoraire.Text = vbNullString
        cbLivreursH.EditValue = Nothing
        tbMontantParHeureLivreurH.Text = 0
        tbHeureDepartH.Text = go_Globals.PeriodeDateFrom
        tbHeureFinH.Text = go_Globals.PeriodeDateFrom

        tbIDCommissionKMCL.Text = vbNullString
        tbKMDebutCL.Text = 0
        tbKMFinCL.Text = 0
        tbMontantChargeCL.Text = 0
        tbChargeExtraCL.Text = 0
        tbChargeExtraParNbKmCL.Text = 0
        tbHeureDepartCL.EditValue = "00:00"
        tbHeureFinCL.EditValue = "00:00"


        tbIDMaxPayable.Text = vbNullString
        tbNombreLivraisons.Text = 0
        tbMaximumPayable.Text = 0

        tabData.TabPages(6).PageVisible = True
        tabData.TabPages(7).PageVisible = True


    End Sub


    Private Sub tbMontantParJourLivreur_EditValueChanged(sender As Object, e As EventArgs)

    End Sub



    Private Sub bAjouterCadeauO_Click(sender As Object, e As EventArgs) Handles bAjouterCadeauO.Click

        If tbDateFromCadeauO.Text.ToString.Trim = "" Then
            MsgBox("Vous devez entrer une période de départ.", MsgBoxStyle.OkOnly, "Erreur")
            tbDateFromCadeauO.Focus()
            Exit Sub
        End If

        If tbDateToCadeauO.Text.ToString.Trim = "" Then
            MsgBox("Vous devez entrer une période de fin.", MsgBoxStyle.OkOnly, "Erreur")
            tbDateToCadeauO.Focus()
            Exit Sub
        End If

        If tbMontantCadeauO.Text = 0 Then
            MsgBox("Vous devez entrer un montant de cadeau.", MsgBoxStyle.OkOnly, "Erreur")
            tbMontantCadeauO.Focus()
            Exit Sub
        End If

        With oCadeauOrganisation
            .IDOrganisation = If(String.IsNullOrEmpty(nudOrganisationID.Text), Nothing, nudOrganisationID.Text)
            .Organisation = If(String.IsNullOrEmpty(tbOrganisation.Text), Nothing, tbOrganisation.Text)
            .DateFrom = If(String.IsNullOrEmpty(tbDateFromCadeauO.Text), Nothing, tbDateFromCadeauO.Text)
            .DateTo = If(String.IsNullOrEmpty(tbDateToCadeauO.Text), Nothing, tbDateToCadeauO.Text)
            .Montant = If(String.IsNullOrEmpty(tbMontantCadeauO.Text), Nothing, tbMontantCadeauO.Text)
            .IDPeriode = go_Globals.IDPeriode
        End With

        Dim bSucess As Boolean
        bSucess = oCadeauOrganisationData.Add(oCadeauOrganisation)
        If bSucess = True Then
            grdCadeauOrganisation.DataSource = oCadeauOrganisationData.SelectAll()
            MessageBox.Show("l'enregistrement a été sauvegardée")
        Else
            MsgBox("L'insertion a échouée.", MsgBoxStyle.OkOnly, "Erreur")
        End If
    End Sub

    Private Sub bAjouterCadeauL_Click(sender As Object, e As EventArgs) Handles bAjouterCadeauL.Click

        If IsNothing(cbLivreursCadeauL.EditValue) Then
            MsgBox("Vous devez entrer un livreur.", MsgBoxStyle.OkOnly, "Erreur")
            cbLivreursCadeauL.Focus()
            Exit Sub

        End If

        If tbDateFromCadeauL.Text.ToString.Trim = "" Then
            MsgBox("Vous devez entrer une période de départ.", MsgBoxStyle.OkOnly, "Erreur")
            tbDateFromCadeauL.Focus()
            Exit Sub
        End If

        If tbDateToCadeauL.Text.ToString.Trim = "" Then
            MsgBox("Vous devez entrer une période de fin.", MsgBoxStyle.OkOnly, "Erreur")
            tbDateToCadeauL.Focus()
            Exit Sub
        End If

        If tbMontantCadeauL.Text = 0 Then
            MsgBox("Vous devez entrer un montant de cadeau.", MsgBoxStyle.OkOnly, "Erreur")
            tbMontantCadeauL.Focus()
            Exit Sub
        End If


        With oCadeauLivreur
            .IDLivreur = cbLivreursCadeauL.EditValue
            .Livreur = If(String.IsNullOrEmpty(cbLivreursCadeauL.Text), Nothing, cbLivreursCadeauL.Text)
            .DateFrom = If(String.IsNullOrEmpty(tbDateFromCadeauL.Text), Nothing, tbDateFromCadeauL.Text)
            .DateTo = If(String.IsNullOrEmpty(tbDateToCadeauL.Text), Nothing, tbDateToCadeauL.Text)
            .Montant = If(String.IsNullOrEmpty(tbMontantCadeauL.Text), Nothing, tbMontantCadeauL.Text)
            .IDPeriode = go_Globals.IDPeriode
        End With

        Dim bSucess As Boolean
        bSucess = oCadeauLivreurData.Add(oCadeauLivreur)
        If bSucess = True Then
            grdCadeauLivreur.DataSource = oCadeauLivreurData.SelectAll(oCadeauLivreur.DateFrom, oCadeauLivreur.DateTo)
        Else
            MsgBox("L'insertion a échouée.", MsgBoxStyle.OkOnly, "Erreur")
        End If
    End Sub

    Private Sub bSupprimerCadeauO_Click(sender As Object, e As EventArgs) Handles bSupprimerCadeauO.Click
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

        Dim row As System.Data.DataRow = GridView6.GetDataRow(GridView6.FocusedRowHandle)
        oCadeauOrganisation.IDCadeauOrganisation = System.Convert.ToInt32(row("IDCadeauOrganisation").ToString)
        Dim result As DialogResult = MessageBox.Show("Êtes-vous sur? Supprimer cet enregistrement?", "Supprimer", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
        If result = DialogResult.Yes Then

            Dim bSucess As Boolean
            bSucess = oCadeauOrganisationData.Delete(oCadeauOrganisation)
            If bSucess = True Then
                grdCadeauOrganisation.DataSource = oCadeauOrganisationData.SelectAll
                bSupprimerCadeauO.Enabled = False
                bMAJCadeauO.Enabled = False
                MessageBox.Show("l'enregistrement a été supprimée")
            Else
                MsgBox("La supression a echouée.", MsgBoxStyle.OkOnly, "Erreur")
            End If
        End If
        Me.Cursor = System.Windows.Forms.Cursors.Default
    End Sub

    Private Sub bSupprimerCadeauL_Click(sender As Object, e As EventArgs) Handles bSupprimerCadeauL.Click
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor


        Dim row As System.Data.DataRow = GridView7.GetDataRow(GridView7.FocusedRowHandle)
        oCadeauLivreur.IDCadeauLivreur = System.Convert.ToInt32(row("IDCadeauLivreur").ToString)




        With oCadeauLivreur
            .IDLivreur = cbLivreursCadeauL.EditValue
            .Livreur = If(String.IsNullOrEmpty(cbLivreursCadeauL.Text), Nothing, cbLivreursCadeauL.Text)
            .DateFrom = If(String.IsNullOrEmpty(tbDateFromCadeauL.Text), Nothing, tbDateFromCadeauL.Text)
            .DateTo = If(String.IsNullOrEmpty(tbDateToCadeauL.Text), Nothing, tbDateToCadeauL.Text)
            .Montant = If(String.IsNullOrEmpty(tbMontantCadeauL.Text), Nothing, tbMontantCadeauL.Text)
            .IDPeriode = go_Globals.IDPeriode
        End With


        Dim result As DialogResult = MessageBox.Show("Êtes-vous sur? Supprimer cet enregistrement?", "Supprimer", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
        If result = DialogResult.Yes Then

            Dim bSucess As Boolean
            bSucess = oCadeauLivreurData.Delete(oCadeauLivreur)
            If bSucess = True Then
                grdCadeauLivreur.DataSource = oCadeauLivreurData.SelectAll(oCadeauLivreur.DateFrom, oCadeauLivreur.DateTo)
                bSupprimerCadeauL.Enabled = False
                bMAJCadeauL.Enabled = False
                MessageBox.Show("l'enregistrement a été supprimée")
            Else
                MsgBox("La supression a echouée.", MsgBoxStyle.OkOnly, "Erreur")
            End If
        End If
        Me.Cursor = System.Windows.Forms.Cursors.Default
    End Sub

    Private Sub bMAJCadeauO_Click(sender As Object, e As EventArgs) Handles bMAJCadeauO.Click

        If tbIIDCadeauOrganisation.Text = 0 Then
            MsgBox("Vous devez sélectionne une ligne dans la grille.", MsgBoxStyle.OkOnly, "Erreur")
            Exit Sub
        End If


        If tbDateFromCadeauO.Text.ToString.Trim = "" Then
            MsgBox("Vous devez entrer une période de départ.", MsgBoxStyle.OkOnly, "Erreur")
            tbDateFromCadeauO.Focus()
            Exit Sub
        End If

        If tbDateToCadeauO.Text.ToString.Trim = "" Then
            MsgBox("Vous devez entrer une période de fin.", MsgBoxStyle.OkOnly, "Erreur")
            tbDateToCadeauO.Focus()
            Exit Sub
        End If

        If tbMontantCadeauO.Text = 0 Then
            MsgBox("Vous devez entrer un montant de cadeau.", MsgBoxStyle.OkOnly, "Erreur")
            tbMontantCadeauO.Focus()
            Exit Sub
        End If

        With oCadeauOrganisation
            .IDCadeauOrganisation = If(String.IsNullOrEmpty(tbIIDCadeauOrganisation.Text), Nothing, tbIIDCadeauOrganisation.Text)
            .IDOrganisation = If(String.IsNullOrEmpty(nudOrganisationID.Text), Nothing, nudOrganisationID.Text)
            .Organisation = If(String.IsNullOrEmpty(tbOrganisation.Text), Nothing, tbOrganisation.Text)
            .DateFrom = If(String.IsNullOrEmpty(tbDateFromCadeauO.Text), Nothing, tbDateFromCadeauO.Text)
            .DateTo = If(String.IsNullOrEmpty(tbDateToCadeauO.Text), Nothing, tbDateToCadeauO.Text)
            .Montant = If(String.IsNullOrEmpty(tbMontantCadeauO.Text), Nothing, tbMontantCadeauO.Text)
            .IDPeriode = go_Globals.IDPeriode
        End With

        Dim bSucess As Boolean
        bSucess = oCadeauOrganisationData.Update(oCadeauOrganisation, oCadeauOrganisation)
        If bSucess = True Then
            grdCadeauOrganisation.DataSource = oCadeauOrganisationData.SelectAll()
            bSupprimerCadeauO.Enabled = False
            bMAJCadeauO.Enabled = False
            MessageBox.Show("l'enregistrement a été sauvegardée")
        Else
            MsgBox("L'insertion a échouée.", MsgBoxStyle.OkOnly, "Erreur")
        End If

    End Sub

    Private Sub bMAJCadeauL_Click(sender As Object, e As EventArgs) Handles bMAJCadeauL.Click
        Dim SelectedRowMP As Integer = GridView4.FocusedRowHandle


        If tbIDCadeauLivreur.Text = 0 Then
            MsgBox("Vous devez sélectionne une ligne dans la grille.", MsgBoxStyle.OkOnly, "Erreur")
            Exit Sub
        End If

        If cbLivreursCadeauL.EditValue < 0 Then
            MsgBox("Vous devez entrer un livreur.", MsgBoxStyle.OkOnly, "Erreur")
            cbLivreursCadeauL.Focus()
            Exit Sub

        End If

        If tbDateFromCadeauL.Text.ToString.Trim = "" Then
            MsgBox("Vous devez entrer une période de départ.", MsgBoxStyle.OkOnly, "Erreur")
            tbDateFromCadeauL.Focus()
            Exit Sub
        End If

        If tbDateToCadeauL.Text.ToString.Trim = "" Then
            MsgBox("Vous devez entrer une période de fin.", MsgBoxStyle.OkOnly, "Erreur")
            tbDateToCadeauL.Focus()
            Exit Sub
        End If

        If tbMontantCadeauL.Text = 0 Then
            MsgBox("Vous devez entrer un montant de cadeau.", MsgBoxStyle.OkOnly, "Erreur")
            tbMontantCadeauL.Focus()
            Exit Sub
        End If


        With oCadeauLivreur
            .IDCadeauLivreur = If(String.IsNullOrEmpty(tbIDCadeauLivreur.Text), Nothing, tbIDCadeauLivreur.Text)
            .IDLivreur = If(String.IsNullOrEmpty(cbLivreursCadeauL.EditValue), Nothing, cbLivreursCadeauL.EditValue)
            .Livreur = If(String.IsNullOrEmpty(cbLivreursCadeauL.Text), Nothing, cbLivreursCadeauL.Text)
            .DateFrom = If(String.IsNullOrEmpty(tbDateFromCadeauL.Text), Nothing, tbDateFromCadeauL.Text)
            .DateTo = If(String.IsNullOrEmpty(tbDateToCadeauL.Text), Nothing, tbDateToCadeauL.Text)
            .Montant = If(String.IsNullOrEmpty(tbMontantCadeauL.Text), Nothing, tbMontantCadeauL.Text)
            .IDPeriode = go_Globals.IDPeriode
        End With

        Dim bSucess As Boolean
        bSucess = oCadeauLivreurData.Update(oCadeauLivreur, oCadeauLivreur)
        If bSucess = True Then
            grdCadeauLivreur.DataSource = oCadeauLivreurData.SelectAll(oCadeauLivreur.DateFrom, oCadeauLivreur.DateTo)
            bSupprimerCadeauL.Enabled = False
            bMAJCadeauL.Enabled = False
            MessageBox.Show("l'enregistrement a été sauvegardée")
        Else
            MsgBox("L'insertion a échouée.", MsgBoxStyle.OkOnly, "Erreur")
        End If

    End Sub

    Private Sub cbLivreursCadeauL_SelectedIndexChanged(sender As Object, e As EventArgs)

    End Sub

    Private Sub tbDateFromGiftLivreur_EditValueChanged(sender As Object, e As EventArgs) Handles tbDateFromCadeauL.EditValueChanged

    End Sub

    Private Sub tbMontantContrat_KeyPress(sender As Object, e As KeyPressEventArgs) Handles tbMontantContrat.KeyPress

        If e.KeyChar = "." Or e.KeyChar = "," Or e.KeyChar = Convert.ToChar(Keys.Back) Or e.KeyChar = Convert.ToChar(Keys.Back) Then
            e.Handled = e.KeyChar = ","
        Else
            e.Handled = Not (Char.IsDigit(e.KeyChar))
        End If

    End Sub

    Private Sub tbPayeLivreur_KeyPress(sender As Object, e As KeyPressEventArgs) Handles tbPayeLivreur.KeyPress
        If e.KeyChar = "." Or e.KeyChar = "," Or e.KeyChar = Convert.ToChar(Keys.Back) Or e.KeyChar = Convert.ToChar(Keys.Back) Then
            e.Handled = e.KeyChar = ","
        Else
            e.Handled = Not (Char.IsDigit(e.KeyChar))
        End If
    End Sub

    Private Sub tbMontantGlacierOrganisation_KeyPress(sender As Object, e As KeyPressEventArgs) Handles tbMontantGlacierOrganisation.KeyPress
        If e.KeyChar = "." Or e.KeyChar = "," Or e.KeyChar = Convert.ToChar(Keys.Back) Or e.KeyChar = Convert.ToChar(Keys.Back) Then
            e.Handled = e.KeyChar = ","
        Else
            e.Handled = Not (Char.IsDigit(e.KeyChar))
        End If

    End Sub

    Private Sub tbMontantGlacierLivreur_KeyPress(sender As Object, e As KeyPressEventArgs) Handles tbMontantGlacierLivreur.KeyPress
        If e.KeyChar = "." Or e.KeyChar = "," Or e.KeyChar = Convert.ToChar(Keys.Back) Or e.KeyChar = Convert.ToChar(Keys.Back) Then
            e.Handled = e.KeyChar = ","
        Else
            e.Handled = Not (Char.IsDigit(e.KeyChar))
        End If
    End Sub

    Private Sub tbCreditParLivraison_KeyPress(sender As Object, e As KeyPressEventArgs) Handles tbCreditParLivraison.KeyPress
        If e.KeyChar = "." Or e.KeyChar = "," Or e.KeyChar = Convert.ToChar(Keys.Back) Or e.KeyChar = Convert.ToChar(Keys.Back) Then
            e.Handled = e.KeyChar = ","
        Else
            e.Handled = Not (Char.IsDigit(e.KeyChar))
        End If
    End Sub

    Private Sub tbMontantCharge_KeyPress(sender As Object, e As KeyPressEventArgs) Handles tbMontantCharge.KeyPress
        If e.KeyChar = "." Or e.KeyChar = "," Or e.KeyChar = Convert.ToChar(Keys.Back) Or e.KeyChar = Convert.ToChar(Keys.Back) Then
            e.Handled = e.KeyChar = ","
        Else
            e.Handled = Not (Char.IsDigit(e.KeyChar))
        End If
    End Sub

    Private Sub tbChargeExtra_KeyPress(sender As Object, e As KeyPressEventArgs) Handles tbChargeExtra.KeyPress
        If e.KeyChar = "." Or e.KeyChar = "," Or e.KeyChar = Convert.ToChar(Keys.Back) Or e.KeyChar = Convert.ToChar(Keys.Back) Then
            e.Handled = e.KeyChar = ","
        Else
            e.Handled = Not (Char.IsDigit(e.KeyChar))
        End If
    End Sub

    Private Sub tbMontantParHeureLivreur_KeyPress(sender As Object, e As KeyPressEventArgs) Handles tbMontantParHeureLivreurH.KeyPress
        If e.KeyChar = "." Or e.KeyChar = "," Or e.KeyChar = Convert.ToChar(Keys.Back) Or e.KeyChar = Convert.ToChar(Keys.Back) Then
            e.Handled = e.KeyChar = ","
        Else
            e.Handled = Not (Char.IsDigit(e.KeyChar))
        End If
    End Sub

    Private Sub tbMontantChargeCL_KeyPress(sender As Object, e As KeyPressEventArgs) Handles tbMontantChargeCL.KeyPress
        If e.KeyChar = "." Or e.KeyChar = "," Or e.KeyChar = Convert.ToChar(Keys.Back) Or e.KeyChar = Convert.ToChar(Keys.Back) Then
            e.Handled = e.KeyChar = ","
        Else
            e.Handled = Not (Char.IsDigit(e.KeyChar))
        End If
    End Sub

    Private Sub tbChargeExtraCL_KeyPress(sender As Object, e As KeyPressEventArgs) Handles tbChargeExtraCL.KeyPress
        If e.KeyChar = "." Or e.KeyChar = "," Or e.KeyChar = Convert.ToChar(Keys.Back) Or e.KeyChar = Convert.ToChar(Keys.Back) Then
            e.Handled = e.KeyChar = ","
        Else
            e.Handled = Not (Char.IsDigit(e.KeyChar))
        End If
    End Sub

    Private Sub tbMaximumPayable_KeyPress(sender As Object, e As KeyPressEventArgs) Handles tbMaximumPayable.KeyPress
        If e.KeyChar = "." Or e.KeyChar = "," Or e.KeyChar = Convert.ToChar(Keys.Back) Or e.KeyChar = Convert.ToChar(Keys.Back) Then
            e.Handled = e.KeyChar = ","
        Else
            e.Handled = Not (Char.IsDigit(e.KeyChar))
        End If
    End Sub

    Private Sub tbMontantCadeauO_KeyPress(sender As Object, e As KeyPressEventArgs) Handles tbMontantCadeauO.KeyPress
        If e.KeyChar = "." Or e.KeyChar = "," Or e.KeyChar = Convert.ToChar(Keys.Back) Or e.KeyChar = Convert.ToChar(Keys.Back) Then
            e.Handled = e.KeyChar = ","
        Else
            e.Handled = Not (Char.IsDigit(e.KeyChar))
        End If
    End Sub



    Private Sub grdParHeure_DoubleClick(sender As Object, e As EventArgs)

    End Sub

    Private Sub grdOrganisations_DoubleClick(sender As Object, e As EventArgs) Handles grdOrganisations.DoubleClick
        Dim row As System.Data.DataRow = GridView1.GetDataRow(GridView1.FocusedRowHandle)


        SelectedRow = GridView1.FocusedRowHandle
        Edit()


        tabData.SelectedTabPage = tabData.TabPages(1)
        tabData.TabPages(1).PageVisible = True
        tabData.TabPages(0).PageVisible = False
        tabData.TabPages(6).PageVisible = True
        tabData.TabPages(7).PageVisible = True
    End Sub

    Private Sub grdOrganisations_Click(sender As Object, e As EventArgs) Handles grdOrganisations.Click
        Dim row As System.Data.DataRow = GridView1.GetDataRow(GridView1.FocusedRowHandle)


        SelectedRow = GridView1.FocusedRowHandle
    End Sub

    Private Sub optCalculationOrganisations_SelectedIndexChanged(sender As Object, e As EventArgs) Handles optCalculationOrganisations.SelectedIndexChanged

        If optCalculationOrganisations.SelectedIndex = -1 Or optCalculationLivreur.SelectedIndex = -1 Then
            Exit Sub
        End If

        ResetSections()


        Select Case optCalculationOrganisations.SelectedIndex
            Case 0
                optCalculationOrganisation0()
            Case 1
                optCalculationOrganisation1()
            Case 2
                optCalculationOrganisation2()
            Case 3
                optCalculationOrganisation3()
            Case 4
                optCalculationOrganisation4()
            Case 5
                optCalculationOrganisation5()
            Case 6
                optCalculationOrganisation6()
            Case 7
                optCalculationOrganisation7()
            Case 8
                optCalculationOrganisation8()
            Case 9
                optCalculationOrganisation9()

        End Select


        Select Case optCalculationLivreur.SelectedIndex

            Case 0 ' Pharmaplus/ massicotte
                optCalculationLivreur0()
            Case 1 ' prix fixe
                optCalculationLivreur1()
            Case 2 ' Par kilometrage
                optCalculationLivreur2()
            Case 3 'Par kilometrage + extras
                optCalculationLivreur3()
            Case 4 'Par kilometrage + extras(heures)
                optCalculationLivreur4()
            Case 5 'Par kilometrage fixe + extras
                optCalculationLivreur5()
            Case 6 ' par jour
                optCalculationLivreur6()
            Case 7 ' par heure
                optCalculationLivreur7()
            Case 8 ' par Residence
                optCalculationLivreur8()

        End Select


        tabData.TabPages(8).PageVisible = True

    End Sub

    Private Sub tbMontantContrat_Enter(sender As Object, e As EventArgs) Handles tbMontantContrat.Enter

        tbMontantContrat.SelectAll()

    End Sub

    Private Sub tbPayeLivreur_Enter(sender As Object, e As EventArgs) Handles tbPayeLivreur.Enter
        tbPayeLivreur.SelectAll()

    End Sub

    Private Sub tbMontantGlacierOrganisation_Enter(sender As Object, e As EventArgs) Handles tbMontantGlacierOrganisation.Enter
        tbMontantGlacierOrganisation.SelectAll()
    End Sub

    Private Sub tbMontantGlacierLivreur_Enter(sender As Object, e As EventArgs) Handles tbMontantGlacierLivreur.Enter
        tbMontantGlacierLivreur.SelectAll()
    End Sub

    Private Sub tbQuantiteLivraison_Enter(sender As Object, e As EventArgs) Handles tbQuantiteLivraison.Enter
        tbQuantiteLivraison.SelectAll()
    End Sub

    Private Sub tbCreditParLivraison_Enter(sender As Object, e As EventArgs) Handles tbCreditParLivraison.Enter
        tbCreditParLivraison.SelectAll()
    End Sub

    Private Sub tbKMDebut_Enter(sender As Object, e As EventArgs) Handles tbKMDebut.Enter
        tbKMDebut.SelectAll()
    End Sub

    Private Sub tbKMFin_Enter(sender As Object, e As EventArgs) Handles tbKMFin.Enter
        tbKMFin.SelectAll()
    End Sub

    Private Sub tbMontantCharge_Enter(sender As Object, e As EventArgs) Handles tbMontantCharge.Enter
        tbMontantCharge.SelectAll()
    End Sub

    Private Sub tbChargeExtra_Enter(sender As Object, e As EventArgs) Handles tbChargeExtra.Enter
        tbChargeExtra.SelectAll()
    End Sub

    Private Sub tbChargeExtraParNbKm_Enter(sender As Object, e As EventArgs) Handles tbChargeExtraParNbKm.Enter
        tbChargeExtraParNbKm.SelectAll()
    End Sub

    Private Sub tbMontantParHeureLivreur_Enter(sender As Object, e As EventArgs) Handles tbMontantParHeureLivreurH.Enter
        tbMontantParHeureLivreurH.SelectAll()
    End Sub

    Private Sub tbKMDebutCL_Enter(sender As Object, e As EventArgs) Handles tbKMDebutCL.Enter
        tbKMDebutCL.SelectAll()


    End Sub

    Private Sub tbKMFinCL_Enter(sender As Object, e As EventArgs) Handles tbKMFinCL.Enter
        tbKMFinCL.SelectAll()
    End Sub

    Private Sub tbMontantChargeCL_Enter(sender As Object, e As EventArgs) Handles tbMontantChargeCL.Enter
        tbMontantChargeCL.SelectAll()
    End Sub

    Private Sub tbChargeExtraCL_Enter(sender As Object, e As EventArgs) Handles tbChargeExtraCL.Enter
        tbChargeExtraCL.SelectAll()
    End Sub

    Private Sub tbChargeExtraParNbKmCL_Enter(sender As Object, e As EventArgs) Handles tbChargeExtraParNbKmCL.Enter
        tbChargeExtraParNbKmCL.SelectAll()
    End Sub

    Private Sub tbNombreLivraisons_Enter(sender As Object, e As EventArgs) Handles tbNombreLivraisons.Enter
        tbNombreLivraisons.SelectAll()
    End Sub

    Private Sub tbMaximumPayable_Enter(sender As Object, e As EventArgs) Handles tbMaximumPayable.Enter
        tbMaximumPayable.SelectAll()
    End Sub

    Private Sub tbMontantCadeauO_Enter(sender As Object, e As EventArgs) Handles tbMontantCadeauO.Enter
        tbMontantCadeauO.SelectAll()
    End Sub

    Private Sub tbMontantCadeauL_Enter(sender As Object, e As EventArgs) Handles tbMontantCadeauL.Enter
        tbMontantCadeauL.SelectAll()
    End Sub

    Private Sub bAnnulerLivreur_Click(sender As Object, e As EventArgs) Handles bAnnulerLivreur.Click
        cbLivreursH.EditValue = Nothing
        cbLivreursH.Text = String.Empty
    End Sub

    Private Sub bAnnulerLivreurL_Click(sender As Object, e As EventArgs) Handles bAnnulerLivreurL.Click
        cbLivreursCadeauL.EditValue = Nothing
        cbLivreursCadeauL.Text = String.Empty
    End Sub

    Private Sub tbMontantContrat_EditValueChanged(sender As Object, e As EventArgs) Handles tbMontantContrat.EditValueChanged

    End Sub

    Private Sub tbMontantMobilus_EditValueChanged(sender As Object, e As EventArgs) Handles tbMontantMobilus.EditValueChanged

    End Sub

    Private Sub tbMontantMobilus_KeyPress(sender As Object, e As KeyPressEventArgs) Handles tbMontantMobilus.KeyPress
        If e.KeyChar = "." Or e.KeyChar = "," Or e.KeyChar = Convert.ToChar(Keys.Back) Or e.KeyChar = Convert.ToChar(Keys.Back) Then
            e.Handled = e.KeyChar = ","
        Else
            e.Handled = Not (Char.IsDigit(e.KeyChar))
        End If

    End Sub

    Private Sub bAnnulerVendeur_Click(sender As Object, e As EventArgs) Handles bAnnulerVendeur.Click
        cbVendeur.EditValue = Nothing
        cbVendeur.SelectedText = Nothing
        cbVendeur.Text = Nothing
    End Sub

    Private Sub grdParHeure_Click_1(sender As Object, e As EventArgs) Handles grdParHeure.Click
        If GridView3.FocusedRowHandle < 0 Then Exit Sub

        Dim row As System.Data.DataRow = GridView3.GetDataRow(GridView3.FocusedRowHandle)

        bSupprimerChargeParHeure.Enabled = True
        bMiseAJourChargeParHeure.Enabled = True

        If Not IsDBNull(row("IDHoraire")) Then
            tbIDHoraire.Text = row("IDHoraire")

        End If

        If Not IsDBNull(row("IDLivreur")) Then
            cbLivreursH.EditValue = row("IDLivreur")
        Else
            cbLivreursH.EditValue = 0
        End If

        If Not IsDBNull(row("MontantLivreur")) Then
            tbMontantParHeureLivreurH.Text = row("MontantLivreur")
        Else
            tbMontantParHeureLivreurH.Text = 0
        End If

        If Not IsDBNull(row("MontantOrganisation")) Then
            tbMontantParHeureOrganisationH.Text = row("MontantOrganisation")
        Else
            tbMontantParHeureOrganisationH.Text = 0
        End If

        If Not IsDBNull(row("HeureDepart")) Then
            tbHeureDepartH.EditValue = row("HeureDepart")
        Else
            tbHeureDepartH.EditValue = "00:00"
        End If

        If Not IsDBNull(row("HeureFin")) Then
            tbHeureFinH.EditValue = row("HeureFin")
        Else
            tbHeureFinH.EditValue = "00:00"
        End If


    End Sub

    Private Sub cbLivreursH_EditValueChanged(sender As Object, e As EventArgs) Handles cbLivreursH.EditValueChanged

        If Not IsNothing(cbLivreursH.EditValue) Then
            grdParHeure.DataSource = oHoraireData.SelectAllByIDOrganisationIDLivreurGrille(nudOrganisationID.Text, cbLivreursH.EditValue)
        End If



    End Sub

    Private Sub bAjouterPrixFixe_Click(sender As Object, e As EventArgs) Handles bAjouterPrixFixe.Click

        If IsNothing(cbLivreursP.EditValue) Then
            MsgBox("Vous devez sélectionner un livreur.", MsgBoxStyle.OkOnly, "Erreur")
            cbLivreursP.Focus()
            Exit Sub
        End If



        If tbMontantPrixFixe.Text < 0 Then
            MsgBox("Vous devez entrer un montant.", MsgBoxStyle.OkOnly, "Erreur")
            tbMontantPrixFixe.Focus()
            Exit Sub
        End If

        Dim oPrixFixe As New PrixFixe


        With oPrixFixe
            .IDOrganisation = nudOrganisationID.Text
            .Organisation = If(String.IsNullOrEmpty(tbOrganisation.Text), Nothing, tbOrganisation.Text)
            .IDLivreur = cbLivreursP.EditValue
            .Livreur = If(String.IsNullOrEmpty(cbLivreursP.Text), Nothing, cbLivreursP.Text)
            .MontantLivreur = If(String.IsNullOrEmpty(tbMontantPrixFixe.Text), Nothing, tbMontantPrixFixe.Text)

            .IDPeriode = go_Globals.IDPeriode
        End With

        Dim bSucess As Boolean
        bSucess = oPrixFixeData.Add(oPrixFixe)
        If bSucess = True Then
            grdPrixFixe.DataSource = oPrixFixeData.SelectAllByIDOrganisationIDLivreur(oPrixFixe.IDOrganisation)
            bSupprimerPrixFixe.Enabled = False
            bMiseAJourPrixFixe.Enabled = False
            tbIDPrixFixe.Text = 0
            MessageBox.Show("l'enregistrement a été sauvegardée")
        Else
            MsgBox("L'insertion a échouée.", MsgBoxStyle.OkOnly, "Erreur")
        End If

    End Sub

    Private Sub bSupprimerPrixFixe_Click(sender As Object, e As EventArgs) Handles bSupprimerPrixFixe.Click
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

        Dim oPrixFixe As New PrixFixe


        If tbIDPrixFixe.Text = 0 Then
            MsgBox("Vous devez sélectionner un élément dans la grille.", MsgBoxStyle.OkOnly, "Erreur")
            Exit Sub
        End If

        oPrixFixe.IDPrixFixe = tbIDPrixFixe.Text
        oPrixFixe.IDOrganisation = nudOrganisationID.Text
        Dim result As DialogResult = MessageBox.Show("Êtes-vous sur? Supprimer cet enregistrement?", "Supprimer", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
        If result = DialogResult.Yes Then

            Dim bSucess As Boolean
            bSucess = oPrixFixeData.Delete(oPrixFixe)
            If bSucess = True Then
                grdPrixFixe.DataSource = oPrixFixeData.SelectAllByIDOrganisationIDLivreur(oPrixFixe.IDOrganisation)
                bSupprimerPrixFixe.Enabled = False
                bMiseAJourPrixFixe.Enabled = False
                MessageBox.Show("l'enregistrement a été supprimée")
            Else
                MsgBox("La supression a echouée.", MsgBoxStyle.OkOnly, "Erreur")
            End If
        End If
        Me.Cursor = System.Windows.Forms.Cursors.Default
    End Sub

    Private Sub bMiseAJourPrixFixe_Click(sender As Object, e As EventArgs) Handles bMiseAJourPrixFixe.Click
        Dim SelectedRow As Integer = GridView3.FocusedRowHandle
        Dim oPrixFixe As New PrixFixe


        If tbIDPrixFixe.Text = 0 Then
            MsgBox("Vous devez sélectionner un élément dans la grille.", MsgBoxStyle.OkOnly, "Erreur")
            Exit Sub
        End If


        If IsNothing(cbLivreursP) Then
            MsgBox("Vous devez sélectionner un livreur.", MsgBoxStyle.OkOnly, "Erreur")
            cbLivreursH.Focus()
            Exit Sub
        End If

        If tbMontantPrixFixe.Text < 0 Then
            MsgBox("Vous devez entrer un montant.", MsgBoxStyle.OkOnly, "Erreur")
            tbMontantParHeureLivreurH.Focus()
            Exit Sub
        End If


        With oPrixFixe
            .IDPrixFixe = tbIDPrixFixe.Text
            .IDOrganisation = nudOrganisationID.Text
            .Organisation = If(String.IsNullOrEmpty(tbOrganisation.Text), Nothing, tbOrganisation.Text)
            .IDLivreur = cbLivreursP.EditValue
            .Livreur = If(String.IsNullOrEmpty(cbLivreursP.Text), Nothing, cbLivreursP.Text)
            .MontantLivreur = If(String.IsNullOrEmpty(tbMontantPrixFixe.Text), Nothing, tbMontantPrixFixe.Text)
            .IDPeriode = go_Globals.IDPeriode
        End With

        Dim bSucess As Boolean
        bSucess = oPrixFixeData.Update(oPrixFixe, oPrixFixe)
        If bSucess = True Then
            grdPrixFixe.DataSource = oPrixFixeData.SelectAllByIDOrganisationIDLivreur(nudOrganisationID.Text)
            bSupprimerChargeParHeure.Enabled = False
            bMiseAJourChargeParHeure.Enabled = False
            MessageBox.Show("l'enregistrement a été sauvegardée")
        Else
            MsgBox("L'insertion a échouée.", MsgBoxStyle.OkOnly, "Erreur")
        End If
    End Sub

    Private Sub grdPrixFixe_Click(sender As Object, e As EventArgs) Handles grdPrixFixe.Click
        If GridView31.FocusedRowHandle < 0 Then Exit Sub

        Dim row As System.Data.DataRow = GridView31.GetDataRow(GridView31.FocusedRowHandle)

        bSupprimerPrixFixe.Enabled = True
        bMiseAJourPrixFixe.Enabled = True

        If Not IsDBNull(row("IDPrixFixe")) Then
            tbIDPrixFixe.Text = row("IDPrixFixe")

        End If

        If Not IsDBNull(row("IDLivreur")) Then
            cbLivreursP.EditValue = row("IDLivreur")
        Else
            cbLivreursP.EditValue = Nothing
        End If

        If Not IsDBNull(row("MontantLivreur")) Then
            tbMontantPrixFixe.Text = row("MontantLivreur")
        Else
            tbMontantPrixFixe.Text = 0
        End If



    End Sub

    Private Sub grdCadeauLivreur_Click(sender As Object, e As EventArgs) Handles grdCadeauLivreur.Click
        If GridView7.FocusedRowHandle < 0 Then Exit Sub

        Dim row As System.Data.DataRow = GridView7.GetDataRow(GridView7.FocusedRowHandle)

        bSupprimerCadeauL.Enabled = True
        bMAJCadeauL.Enabled = True

        If Not IsDBNull(row("IDCadeauLivreur")) Then
            tbIDCadeauLivreur.Text = row("IDCadeauLivreur")

        End If

        If Not IsDBNull(row("IDLivreur")) Then
            cbLivreursCadeauL.EditValue = row("IDLivreur")
        Else
            cbLivreursCadeauL.EditValue = 0
        End If

        If Not IsDBNull(row("Montant")) Then
            tbMontantCadeauL.Text = row("Montant")
        Else
            tbMontantCadeauL.Text = 0
        End If

        If Not IsDBNull(row("DateFrom")) Then
            tbDateFromCadeauL.EditValue = row("DateFrom")
        Else
            tbDateFromCadeauL.EditValue = Nothing
        End If

        If Not IsDBNull(row("DateTo")) Then
            tbDateToCadeauL.EditValue = row("DateTo")
        Else
            tbDateToCadeauL.EditValue = Nothing
        End If


    End Sub

    Private Sub grdCadeauOrganisation_Click(sender As Object, e As EventArgs) Handles grdCadeauOrganisation.Click
        If GridView6.FocusedRowHandle < 0 Then Exit Sub

        Dim row As System.Data.DataRow = GridView6.GetDataRow(GridView6.FocusedRowHandle)

        bSupprimerCadeauO.Enabled = True
        bMAJCadeauO.Enabled = True

        If Not IsDBNull(row("IDCadeauOrganisation")) Then
            tbIIDCadeauOrganisation.Text = row("IDCadeauOrganisation")

        End If

        If Not IsDBNull(row("Montant")) Then
            tbMontantCadeauO.Text = row("Montant")
        Else
            tbMontantCadeauO.Text = 0
        End If

        If Not IsDBNull(row("DateFrom")) Then
            tbDateFromCadeauO.EditValue = row("DateFrom")
        Else
            tbDateFromCadeauO.EditValue = Nothing
        End If

        If Not IsDBNull(row("DateTo")) Then
            tbDateToCadeauO.EditValue = row("DateTo")
        Else
            tbDateToCadeauO.EditValue = Nothing
        End If
    End Sub

    Private Sub bAjouterChargeParJour_Click(sender As Object, e As EventArgs) Handles bAjouterChargeParJour.Click

        If IsNothing(cbLivreursJ.EditValue) Then
            MsgBox("Vous devez sélectionner un livreur.", MsgBoxStyle.OkOnly, "Erreur")
            cbLivreursJ.Focus()
            Exit Sub
        End If

        If tbMontantParHeureLivreurJ.Text < 0 Then
            MsgBox("Vous devez entrer un montant.", MsgBoxStyle.OkOnly, "Erreur")
            tbMontantParHeureLivreurJ.Focus()
            Exit Sub
        End If

        If tbMontantParHeureOrganisationJ.Text < 0 Then
            MsgBox("Vous devez entrer un montant.", MsgBoxStyle.OkOnly, "Erreur")
            tbMontantParHeureLivreurJ.Focus()
            Exit Sub
        End If


        Dim oHoraireJour As New HoraireJour

        With oHoraireJour
            .IDOrganisation = nudOrganisationID.Text
            .Organisation = If(String.IsNullOrEmpty(tbOrganisation.Text), Nothing, tbOrganisation.Text)
            .IDLivreur = cbLivreursJ.EditValue
            .Livreur = If(String.IsNullOrEmpty(cbLivreursJ.Text), Nothing, cbLivreursJ.Text)
            .MontantLivreur = If(String.IsNullOrEmpty(tbMontantParHeureLivreurJ.Text), Nothing, tbMontantParHeureLivreurJ.Text)
            .MontantOrganisation = If(String.IsNullOrEmpty(tbMontantParHeureOrganisationJ.Text), Nothing, tbMontantParHeureOrganisationJ.Text)
            .IDPeriode = go_Globals.IDPeriode
            .DateTravail = If(String.IsNullOrEmpty(tbDateJ.Text), Nothing, tbDateJ.Text)

        End With

        Dim bSucess As Boolean
        bSucess = oHoraireJourData.Add(oHoraireJour)
        If bSucess = True Then
            grdParJour.DataSource = oHoraireJourData.SelectAllByIDOrganisationIDLivreur(oHoraire.IDOrganisation, oHoraire.IDLivreur)
            bSupprimerChargeParJour.Enabled = False
            bMiseAJourChargeParJour.Enabled = False
            tbIDHoraireJ.Text = 0
            MessageBox.Show("l'enregistrement a été sauvegardée")
        Else
            MsgBox("L'insertion a échouée.", MsgBoxStyle.OkOnly, "Erreur")
        End If


    End Sub

    Private Sub bSupprimerChargeParJour_Click(sender As Object, e As EventArgs) Handles bSupprimerChargeParJour.Click
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

        Dim oHoraireJour As New HoraireJour


        If tbIDHoraireJ.Text = 0 Then
            MsgBox("Vous devez sélectionner un élément dans la grille.", MsgBoxStyle.OkOnly, "Erreur")
            Exit Sub
        End If

        oHoraireJour.IDHoraireJour = tbIDHoraireJ.Text
        Dim result As DialogResult = MessageBox.Show("Êtes-vous sur? Supprimer cet enregistrement?", "Supprimer", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
        If result = DialogResult.Yes Then

            Dim bSucess As Boolean
            bSucess = oHoraireJourData.Delete(oHoraireJour)
            If bSucess = True Then
                grdParJour.DataSource = oHoraireJourData.SelectAllByIDOrganisationIDLivreur(nudOrganisationID.Text, cbLivreursH.EditValue)
                bSupprimerChargeParJour.Enabled = False
                bMiseAJourChargeParJour.Enabled = False
                MessageBox.Show("l'enregistrement a été supprimée")
            Else
                MsgBox("La supression a echouée.", MsgBoxStyle.OkOnly, "Erreur")
            End If
        End If
        Me.Cursor = System.Windows.Forms.Cursors.Default
    End Sub

    Private Sub bMiseAJourChargeParJour_Click(sender As Object, e As EventArgs) Handles bMiseAJourChargeParJour.Click
        Dim SelectedRow As Integer = GridView3.FocusedRowHandle
        Dim oHoraireJour As New HoraireJour


        If tbIDHoraireJ.Text = 0 Then
            MsgBox("Vous devez sélectionner un élément dans la grille.", MsgBoxStyle.OkOnly, "Erreur")
            Exit Sub
        End If


        If IsNothing(cbLivreursJ) Then
            MsgBox("Vous devez sélectionner un livreur.", MsgBoxStyle.OkOnly, "Erreur")
            cbLivreursJ.Focus()
            Exit Sub
        End If



        If tbMontantParHeureLivreurJ.Text < 0 Then
            MsgBox("Vous devez entrer un montant.", MsgBoxStyle.OkOnly, "Erreur")
            tbMontantParHeureLivreurJ.Focus()
            Exit Sub
        End If

        If tbMontantParHeureOrganisationJ.Text < 0 Then
            MsgBox("Vous devez entrer un montant.", MsgBoxStyle.OkOnly, "Erreur")
            tbMontantParHeureLivreurJ.Focus()
            Exit Sub
        End If



        With oHoraireJour
            .IDHoraireJour = tbIDHoraireJ.Text
            .IDOrganisation = nudOrganisationID.Text
            .Organisation = If(String.IsNullOrEmpty(tbOrganisation.Text), Nothing, tbOrganisation.Text)
            .IDLivreur = cbLivreursJ.EditValue
            .Livreur = If(String.IsNullOrEmpty(cbLivreursJ.Text), Nothing, cbLivreursJ.Text)
            .MontantLivreur = If(String.IsNullOrEmpty(tbMontantParHeureLivreurJ.Text), Nothing, tbMontantParHeureLivreurJ.Text)
            .MontantOrganisation = If(String.IsNullOrEmpty(tbMontantParHeureOrganisationJ.Text), Nothing, tbMontantParHeureOrganisationJ.Text)
            .IDPeriode = go_Globals.IDPeriode
            .DateTravail = If(String.IsNullOrEmpty(tbDateJ.Text), Nothing, tbDateJ.Text)

        End With

        Dim bSucess As Boolean
        bSucess = oHoraireJourData.Update(oHoraireJour, oHoraireJour)
        If bSucess = True Then

            grdParJour.DataSource = oHoraireJourData.SelectAllByIDOrganisationIDLivreurParJourGrille(nudOrganisationID.Text, cbLivreursH.EditValue)
            bSupprimerChargeParJour.Enabled = False
            bMiseAJourChargeParJour.Enabled = False
            MessageBox.Show("l'enregistrement a été sauvegardée")
        Else
            MsgBox("L'insertion a échouée.", MsgBoxStyle.OkOnly, "Erreur")
        End If
    End Sub

    Private Sub grdParJour_Click(sender As Object, e As EventArgs) Handles grdParJour.Click
        If GridView32.FocusedRowHandle < 0 Then Exit Sub

        Dim row As System.Data.DataRow = GridView32.GetDataRow(GridView32.FocusedRowHandle)

        bSupprimerChargeParJour.Enabled = True
        bMiseAJourChargeParJour.Enabled = True

        If Not IsDBNull(row("IDHoraireJour")) Then
            tbIDHoraireJ.Text = row("IDHoraireJour")

        End If

        If Not IsDBNull(row("IDLivreur")) Then
            cbLivreursJ.EditValue = row("IDLivreur")
        Else
            cbLivreursJ.EditValue = 0
        End If

        If Not IsDBNull(row("MontantLivreur")) Then
            tbMontantParHeureLivreurJ.Text = row("MontantLivreur")
        Else
            tbMontantParHeureLivreurJ.Text = 0
        End If

        If Not IsDBNull(row("MontantOrganisation")) Then
            tbMontantParHeureOrganisationH.Text = row("MontantOrganisation")
        Else
            tbMontantParHeureOrganisationH.Text = 0
        End If

        If Not IsDBNull(row("DateTravail")) Then
            tbDateJ.EditValue = row("DateTravail")
        Else
            tbDateJ.EditValue = Nothing
        End If


    End Sub

    Private Sub cbLivreursJ_EditValueChanged(sender As Object, e As EventArgs) Handles cbLivreursJ.EditValueChanged

        If Not IsNothing(cbLivreursJ.EditValue) Then
            grdParJour.DataSource = oHoraireJourData.SelectAllByIDOrganisationIDLivreur(nudOrganisationID.Text, cbLivreursJ.EditValue)
        End If
    End Sub

    Private Sub chkIsEtatDeCompte_CheckedChanged(sender As Object, e As EventArgs) Handles chkIsEtatDeCompte.CheckedChanged


    End Sub

    Private Sub tbCommissionVendeur_EditValueChanged(sender As Object, e As EventArgs) Handles tbCommissionVendeur.EditValueChanged

    End Sub

    Private Sub bAdd_Click_1(sender As Object, e As EventArgs) Handles bAdd.Click
        Add()
    End Sub
End Class