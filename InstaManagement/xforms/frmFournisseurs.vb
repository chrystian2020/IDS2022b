﻿Imports System.Data.SqlClient
Imports System.Globalization
Imports System.Threading
Imports System.Text


Public Class frmFournisseurs
    Private oFournisseurData As New FournisseursData
    Private oFournisseur As New Fournisseurs

    Private bload As Boolean = False
    Private bAddMode As Boolean = False
    Private bEditMode As Boolean = False
    Private bDeleteMode As Boolean = False
    Public blnGridClick As Boolean = False
    Private iRowGrid As Int32 = 0
    Dim ssql As String
    Private Sub nudIDFournisseur_EditValueChanged(sender As System.Object, e As System.EventArgs) Handles nudIDFournisseur.EditValueChanged

    End Sub

    Private Sub tabData_Click(sender As System.Object, e As System.EventArgs) Handles tabData.Click

    End Sub

    Private Sub frmLivreurs_Load(sender As Object, e As System.EventArgs) Handles Me.Load

        bload = True
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

        Thread.CurrentThread.CurrentCulture = New CultureInfo("en-US", True)


        LoadGridCode()

        bload = False

        Me.Cursor = System.Windows.Forms.Cursors.Default
    End Sub


    Private Sub LoadGridCode()
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        Try

            grdFournisseur.DataSource = oFournisseurData.SelectAll
            GridView1.ClearSorting()
            GridView1.Columns("IDFournisseur").SortOrder = DevExpress.Data.ColumnSortOrder.Ascending
            GridView1.BestFitColumns()
            'GridView1.OptionsView.ColumnAutoWidth = True
            'GridView1.BestFitColumns()
            Dim viewInfo As DevExpress.XtraGrid.Views.Grid.ViewInfo.GridViewInfo = TryCast(GridView1.GetViewInfo(), DevExpress.XtraGrid.Views.Grid.ViewInfo.GridViewInfo)
            If viewInfo.ViewRects.ColumnTotalWidth < viewInfo.ViewRects.ColumnPanelWidth Then
                'GridView1.OptionsView.ColumnAutoWidth = True
            End If

            GridView1.Columns("IDFournisseur").Width = 75
            GridView1.Columns("Fournisseur").Width = 400


        Catch
        End Try
        Me.Cursor = System.Windows.Forms.Cursors.Default
    End Sub


    Private Sub Delete()
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        Me.AddMode = False
        Me.EditMode = False
        Me.DeleteMode = True
        GetData()

        EnableRecord(False)
        tabData.SelectedTabPage = tabData.TabPages(1)
        tabData.TabPages(1).PageVisible = True
        tabData.TabPages(0).PageVisible = False
        ShowToolStripItems("Delete")
        Me.Cursor = System.Windows.Forms.Cursors.Default
    End Sub


    Private Sub Add()
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        Me.AddMode = True
        Me.EditMode = False
        Me.DeleteMode = False

        ClearRecord()
        EnableRecord(True)


        tabData.SelectedTabPage = tabData.TabPages(1)
        tabData.TabPages(1).PageVisible = True
        tabData.TabPages(0).PageVisible = False
        ShowToolStripItems("Add")

        Me.Cursor = System.Windows.Forms.Cursors.Default
    End Sub
    Private Sub Edit()
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        Me.AddMode = False
        Me.EditMode = True
        Me.DeleteMode = False
        ClearRecord()
        GetData()

        EnableRecord(True)


        Me.Cursor = System.Windows.Forms.Cursors.Default
    End Sub

    Private Sub ClearRecord()
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

        Me.nudIDFournisseur.Text = Nothing
        Me.tbFournisseur.Text = Nothing
        Me.tbDescription.Text = Nothing



        Me.Cursor = System.Windows.Forms.Cursors.Default
    End Sub

    Private Sub EnableRecord(ByVal YesNo As Boolean)
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        Me.nudIDFournisseur.Enabled = YesNo
        Me.tbFournisseur.Enabled = YesNo
        Me.tbDescription.Enabled = YesNo
        Me.Cursor = System.Windows.Forms.Cursors.Default
    End Sub

    Private Sub GoBack_To_Grid()
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        Dim gridOK As Boolean = False
        Try
            ShowToolStripItems("Cancel")
            LoadGridCode()

            gridOK = True
        Catch
            'Hide Erreur message.
        Finally
            If gridOK = False Then
                ''''
                ShowToolStripItems("Cancel")
                ''''
            End If
        End Try
        Me.Cursor = System.Windows.Forms.Cursors.Default
    End Sub

    Private Sub ShowToolStripItems(ByVal Item As String)
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        bAdd.Enabled = False
        bEdit.Enabled = False
        bDelete.Enabled = False
        bCancel.Enabled = False
        bRefresh.Enabled = False
        Select Case Item
            Case "Add"
                bCancel.Enabled = True
            Case "Edit"
                bCancel.Enabled = True
            Case "Delete"
                bCancel.Enabled = True
            Case "Cancel"
                bAdd.Enabled = True
                bEdit.Enabled = True
                bDelete.Enabled = True
                bRefresh.Enabled = True
                tabData.SelectedTabPage = tabData.TabPages(0)
                tabData.TabPages(1).PageVisible = False
                tabData.TabPages(0).PageVisible = True
            Case "Refresh"
                bAdd.Enabled = True
                bEdit.Enabled = True
                bDelete.Enabled = True
                bRefresh.Enabled = True
                LoadGridCode()
            Case "No Record"
                bAdd.Enabled = True
                bRefresh.Enabled = True
        End Select

        Me.Cursor = System.Windows.Forms.Cursors.Default
    End Sub

    Public Property AddMode() As Boolean
        Get
            Return bAddMode
        End Get
        Set(ByVal value As Boolean)
            bAddMode = value
        End Set
    End Property

    Public Property EditMode() As Boolean
        Get
            Return bEditMode
        End Get
        Set(ByVal value As Boolean)
            bEditMode = value
        End Set
    End Property

    Public Property DeleteMode() As Boolean
        Get
            Return bDeleteMode
        End Get
        Set(ByVal value As Boolean)
            bDeleteMode = value
        End Set
    End Property


    Public Property SelectedRow() As Int32
        Get
            Return iRowGrid
        End Get
        Set(ByVal value As Int32)
            iRowGrid = value
        End Set
    End Property

    Private Sub bAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        tabData.SelectedTabPage = tabData.TabPages(1)
        tabData.TabPages(1).PageVisible = True
        tabData.TabPages(0).PageVisible = False
    End Sub

    Private Sub bEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        tabData.SelectedTabPage = tabData.TabPages(1)
        tabData.TabPages(1).PageVisible = True
        tabData.TabPages(0).PageVisible = False
    End Sub

    Private Sub bCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        tabData.SelectedTabPage = tabData.TabPages(0)
        tabData.TabPages(1).PageVisible = False
        tabData.TabPages(0).PageVisible = True
    End Sub

    Private Sub bDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        tabData.SelectedTabPage = tabData.TabPages(0)
        tabData.TabPages(1).PageVisible = False
        tabData.TabPages(0).PageVisible = True
    End Sub

    Private Sub butCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butCancel.Click
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        ShowToolStripItems("Cancel")
        tabData.SelectedTabPage = tabData.TabPages(0)
        tabData.TabPages(1).PageVisible = False
        tabData.TabPages(0).PageVisible = True


        Me.Cursor = System.Windows.Forms.Cursors.Default

    End Sub

    Private Sub butClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butClose.Click
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        Me.Close()
        Me.Cursor = System.Windows.Forms.Cursors.Default
    End Sub


    Private Sub GetData()


        ClearRecord()

        Dim row As System.Data.DataRow = GridView1.GetDataRow(GridView1.FocusedRowHandle)

        Dim clsFournisseur As New Fournisseurs
        clsFournisseur.IDFournisseur = System.Convert.ToInt32(row("IDFournisseur").ToString)
        clsFournisseur = oFournisseurData.Select_Record(clsFournisseur)

        If Not clsFournisseur Is Nothing Then
            Try
                nudIDFournisseur.Text = System.Convert.ToInt32(clsFournisseur.IDFournisseur)
                tbFournisseur.Text = If(IsNothing(clsFournisseur.Fournisseur), Nothing, clsFournisseur.Fournisseur.ToString.Trim)
                tbDescription.Text = If(IsNothing(clsFournisseur.Description), Nothing, clsFournisseur.Description.ToString.Trim)
            Catch
            End Try
        End If


    End Sub

    Private Sub SetData(ByVal clsFournisseur As Fournisseurs)
        With clsFournisseur
            .IDFournisseur = If(String.IsNullOrEmpty(nudIDFournisseur.Text), Nothing, nudIDFournisseur.Text)
            .Fournisseur = If(String.IsNullOrEmpty(tbFournisseur.Text), Nothing, tbFournisseur.Text)
            .Description = If(String.IsNullOrEmpty(tbDescription.Text), Nothing, tbDescription.Text)


        End With
    End Sub
    Private Function VerifyData() As Boolean
        Return True
    End Function

    Private Sub UpdateRecord()
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        Dim oclsFournisseur As New Fournisseurs


        Dim row As System.Data.DataRow = GridView1.GetDataRow(GridView1.FocusedRowHandle)
        oclsFournisseur.IDFournisseur = System.Convert.ToInt32(row("IDFournisseur").ToString)
        oclsFournisseur = oFournisseurData.Select_Record(oclsFournisseur)

        If VerifyData() = True Then
            SetData(oclsFournisseur)
            Dim bSucess As Boolean
            bSucess = oFournisseurData.Update(oclsFournisseur, oclsFournisseur)
            If bSucess = True Then
                GoBack_To_Grid()
            Else
                MsgBox("Update failed.", MsgBoxStyle.OkOnly, "Erreur")
            End If
        End If
        Me.Cursor = System.Windows.Forms.Cursors.Default
    End Sub

    Private Sub DeleteRecord()
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        Dim oclsFournisseur As New Fournisseurs

        Dim row As System.Data.DataRow = GridView1.GetDataRow(GridView1.FocusedRowHandle)
        oclsFournisseur.IDFournisseur = System.Convert.ToInt32(row("IDFournisseur").ToString)
        Dim result As DialogResult = MessageBox.Show("Êtes-vous sur? Supprimer cet enregistrement?", "Supprimer", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
        If result = DialogResult.Yes Then
            SetData(oclsFournisseur)
            Dim bSucess As Boolean
            bSucess = oFournisseurData.Delete(oclsFournisseur)
            If bSucess = True Then
                GoBack_To_Grid()
            Else
                MsgBox("La supression a echouée.", MsgBoxStyle.OkOnly, "Erreur")
            End If
        End If
        Me.Cursor = System.Windows.Forms.Cursors.Default
    End Sub

    Private Sub InsertRecord()
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        Dim oclsFournisseur As New Fournisseurs
        If VerifyData() = True Then
            SetData(oclsFournisseur)
            Dim bSucess As Boolean
            bSucess = oFournisseurData.Add(oclsFournisseur)
            If bSucess = True Then
                GoBack_To_Grid()
            Else
                MsgBox("L'insertion a échouée.", MsgBoxStyle.OkOnly, "Erreur")
            End If
        End If
        Me.Cursor = System.Windows.Forms.Cursors.Default
    End Sub
    Private Sub butApply_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butApply.Click
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        If Me.AddMode = True Then
            Me.InsertRecord()
        ElseIf Me.EditMode = True Then
            Me.UpdateRecord()
        ElseIf Me.DeleteMode = True Then
            Me.DeleteRecord()
        End If
        Me.Cursor = System.Windows.Forms.Cursors.Default

        tabData.SelectedTabPage = tabData.TabPages(0)
        tabData.TabPages(1).PageVisible = False
        tabData.TabPages(0).PageVisible = True
    End Sub

    Private Sub TS_ItemClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolStripItemClickedEventArgs) Handles TS.ItemClicked
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

        Select Case e.ClickedItem.Text
            Case "Ajouter"

                tabData.SelectedTabPage = tabData.TabPages(1)
                tabData.TabPages(1).PageVisible = True
                tabData.TabPages(0).PageVisible = False
                Add()


            Case "Edition"
                tabData.SelectedTabPage = tabData.TabPages(1)
                tabData.TabPages(1).PageVisible = True
                tabData.TabPages(0).PageVisible = False
                Edit()

            Case "Supprimer"
                tabData.SelectedTabPage = tabData.TabPages(0)
                tabData.TabPages(0).PageVisible = True
                tabData.TabPages(1).PageVisible = False
                Delete()
            Case "Annuler"
                ShowToolStripItems("Cancel")
            Case "Rafraîchir"
                GoBack_To_Grid()
        End Select
        Me.Cursor = System.Windows.Forms.Cursors.Default
    End Sub


    Private Sub grdFournisseur_Click(sender As System.Object, e As System.EventArgs) Handles grdFournisseur.Click
        If GridView1.FocusedRowHandle < 0 Then Exit Sub

        Dim row As System.Data.DataRow = GridView1.GetDataRow(GridView1.FocusedRowHandle)


        SelectedRow = GridView1.FocusedRowHandle
    End Sub

    Private Sub grdFournisseur_DoubleClick(sender As Object, e As System.EventArgs) Handles grdFournisseur.DoubleClick
        Dim row As System.Data.DataRow = GridView1.GetDataRow(GridView1.FocusedRowHandle)

        SelectedRow = GridView1.FocusedRowHandle
        Edit()
        tabData.SelectedTabPage = tabData.TabPages(1)
        tabData.TabPages(1).PageVisible = True
        tabData.TabPages(0).PageVisible = False
    End Sub



    Private Sub ToolStripButton6_Click(sender As System.Object, e As System.EventArgs) Handles ToolStripButton6.Click
        Me.Close()
    End Sub
End Class