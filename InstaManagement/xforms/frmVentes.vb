﻿Imports System.Data.SqlClient
Imports System.Globalization
Imports System.Threading
Imports System.Text


Public Class frmVentes
    Private oVentesData As New VentesData
    Private oVentes As New Ventes

    Private bload As Boolean = False
    Private bAddMode As Boolean = False
    Private bEditMode As Boolean = False
    Private bDeleteMode As Boolean = False
    Public blnGridClick As Boolean = False
    Private iRowGrid As Int32 = 0
    Dim ssql As String
    Private Sub FillPeriode()

        Dim dtperiode As DataTable
        Dim drPeriode As DataRow

        Dim oPeriodeData As New PeriodesData

        Dim table As New DataTable
        table.Columns.Add("value", Type.GetType("System.Int32"))
        table.Columns.Add("displayText", Type.GetType("System.String"))
        dtperiode = oPeriodeData.SelectAllByDate(go_Globals.PeriodeDateFrom, go_Globals.PeriodeDateTo)


        For Each drPeriode In dtperiode.Rows
            Dim teller As Integer = 0

            teller = drPeriode("IDPeriode")
            Dim toto As String = drPeriode("Periode")

            table.Rows.Add(New Object() {teller, toto})
        Next



        With cbPeriodes.Properties
            .DataSource = table
            .NullText = "Selectionner une période"
            .DisplayMember = "displayText"
            .ValueMember = "value"
            .PopulateColumns()
            .PopupFormMinSize = New Size(50, 200)
            '.Columns("value").Visible = True
            '.Columns("displayText").Caption = "Périodes"
        End With

        With cbPeriodes.Properties
            .DataSource = table
            .NullText = "Selectionner une période"
            .DisplayMember = "displayText"
            .ValueMember = "value"
            .PopulateColumns()
            .PopupFormMinSize = New Size(50, 200)
            '.Columns("value").Visible = True
            '.Columns("displayText").Caption = "Périodes"
        End With

    End Sub

    Private Sub FillOrganisations()
        Dim drOrganisation As DataRow
        Dim dtOrganisation As DataTable


        Dim oOrganisationData As New OrganisationsData

        Dim table As New DataTable
        table.Columns.Add("value", Type.GetType("System.Int32"))
        table.Columns.Add("displayText", Type.GetType("System.String"))
        dtOrganisation = oOrganisationData.SelectAll()


        For Each drOrganisation In dtOrganisation.Rows
            Dim teller As Integer = 0

            teller = drOrganisation("OrganisationID")
            Dim toto As String = drOrganisation("Organisation").ToString.Trim

            table.Rows.Add(New Object() {teller, toto})
        Next

        With cbOrganisation.Properties
            .DataSource = table
            .NullText = "Selectionner un organisation"
            .DisplayMember = "displayText"
            .ValueMember = "value"
            .PopulateColumns()
            .PopupFormMinSize = New Size(50, 200)
            '.Columns("value").Visible = True
            '.Columns("displayText").Caption = "Organisation"
        End With

    End Sub
    Private Sub frmVentes_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        bload = True
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

        Thread.CurrentThread.CurrentCulture = New CultureInfo("en-US", True)
        FillPeriode()
        FillOrganisations()
        LoadGridCode()

        cbPeriodes.EditValue = go_Globals.IDPeriode

        bload = False

        Me.Cursor = System.Windows.Forms.Cursors.Default
    End Sub

    Private Sub LoadGridCode()
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        Try

            grdVentes.DataSource = oVentesData.SelectAllByDate(go_Globals.PeriodeDateFrom, go_Globals.PeriodeDateTo)
            'GridView1.BestFitColumns()
            GridView1.ClearSorting()
            GridView1.Columns("IDVente").SortOrder = DevExpress.Data.ColumnSortOrder.Ascending

        Catch
        End Try
        Me.Cursor = System.Windows.Forms.Cursors.Default
    End Sub


    Private Sub Delete()
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        Me.AddMode = False
        Me.EditMode = False
        Me.DeleteMode = True
        GetData()

        EnableRecord(False)
        tabData.SelectedTabPage = tabData.TabPages(1)
        tabData.TabPages(1).PageVisible = True
        tabData.TabPages(0).PageVisible = False
        ShowToolStripItems("Delete")
        Me.Cursor = System.Windows.Forms.Cursors.Default
    End Sub


    Private Sub Add()
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        Me.AddMode = True
        Me.EditMode = False
        Me.DeleteMode = False

        ClearRecord()
        EnableRecord(True)



        cbPeriodes.EditValue = go_Globals.IDPeriode
        tbDateFrom.Text = go_Globals.PeriodeDateFrom
        tbDateto.text = go_Globals.PeriodeDateTo
        tbDate.Text = Now.ToShortDateString

        tabData.SelectedTabPage = tabData.TabPages(1)
        tabData.TabPages(1).PageVisible = True
        tabData.TabPages(0).PageVisible = False
        ShowToolStripItems("Add")

        Me.Cursor = System.Windows.Forms.Cursors.Default
    End Sub
    Private Sub Edit()
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        Me.AddMode = False
        Me.EditMode = True
        Me.DeleteMode = False
        ClearRecord()
        GetData()

        EnableRecord(True)


        Me.Cursor = System.Windows.Forms.Cursors.Default
    End Sub

    Private Sub ClearRecord()
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor


        Me.nudVenteID.Text = Nothing
        Me.cbPeriodes.Text = Nothing
        Me.tbDate.Text = Nothing
        Me.tbDateFrom.Text = Nothing
        Me.tbDateTo.Text = Nothing
        Me.cbOrganisation.Text = Nothing
        Me.tbDescription.Text = Nothing
        Me.tbQuantite.Text = Nothing

        Me.tbTotal.Text = Nothing



        Me.Cursor = System.Windows.Forms.Cursors.Default
    End Sub

    Private Sub EnableRecord(ByVal YesNo As Boolean)
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

        Me.nudVenteID.Enabled = YesNo
        Me.cbPeriodes.Enabled = YesNo
        Me.tbDate.Enabled = YesNo
        Me.tbDateFrom.Enabled = YesNo
        Me.tbDateTo.Enabled = YesNo
        Me.cbOrganisation.Enabled = YesNo
        Me.tbDescription.Enabled = YesNo
        Me.tbQuantite.Enabled = YesNo

        Me.tbTotal.Enabled = YesNo




        Me.Cursor = System.Windows.Forms.Cursors.Default
    End Sub

    Private Sub GoBack_To_Grid()
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        Dim gridOK As Boolean = False
        Try
            ShowToolStripItems("Cancel")
            LoadGridCode()

            gridOK = True
        Catch
            'Hide Erreur message.
        Finally
            If gridOK = False Then
                ''''
                ShowToolStripItems("Cancel")
                ''''
            End If
        End Try
        Me.Cursor = System.Windows.Forms.Cursors.Default
    End Sub

    Private Sub ShowToolStripItems(ByVal Item As String)
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        bAdd.Enabled = False
        bEdit.Enabled = False
        bDelete.Enabled = False
        bCancel.Enabled = False
        bRefresh.Enabled = False
        Select Case Item
            Case "Add"
                bCancel.Enabled = True
            Case "Edit"
                bCancel.Enabled = True
            Case "Delete"
                bCancel.Enabled = True
            Case "Cancel"
                bAdd.Enabled = True
                bEdit.Enabled = True
                bDelete.Enabled = True
                bRefresh.Enabled = True
                tabData.SelectedTabPage = tabData.TabPages(0)
                tabData.TabPages(1).PageVisible = False
                tabData.TabPages(0).PageVisible = True
            Case "Refresh"
                bAdd.Enabled = True
                bEdit.Enabled = True
                bDelete.Enabled = True
                bRefresh.Enabled = True
                LoadGridCode()
            Case "No Record"
                bAdd.Enabled = True
                bRefresh.Enabled = True
        End Select

        Me.Cursor = System.Windows.Forms.Cursors.Default
    End Sub

    Public Property AddMode() As Boolean
        Get
            Return bAddMode
        End Get
        Set(ByVal value As Boolean)
            bAddMode = value
        End Set
    End Property

    Public Property EditMode() As Boolean
        Get
            Return bEditMode
        End Get
        Set(ByVal value As Boolean)
            bEditMode = value
        End Set
    End Property

    Public Property DeleteMode() As Boolean
        Get
            Return bDeleteMode
        End Get
        Set(ByVal value As Boolean)
            bDeleteMode = value
        End Set
    End Property


    Public Property SelectedRow() As Int32
        Get
            Return iRowGrid
        End Get
        Set(ByVal value As Int32)
            iRowGrid = value
        End Set
    End Property

    Private Sub bAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        tabData.SelectedTabPage = tabData.TabPages(1)
        tabData.TabPages(1).PageVisible = True
        tabData.TabPages(0).PageVisible = False
    End Sub

    Private Sub bEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        tabData.SelectedTabPage = tabData.TabPages(1)
        tabData.TabPages(1).PageVisible = True
        tabData.TabPages(0).PageVisible = False
    End Sub

    Private Sub bCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        tabData.SelectedTabPage = tabData.TabPages(0)
        tabData.TabPages(1).PageVisible = False
        tabData.TabPages(0).PageVisible = True
    End Sub

    Private Sub bDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        tabData.SelectedTabPage = tabData.TabPages(0)
        tabData.TabPages(1).PageVisible = False
        tabData.TabPages(0).PageVisible = True
    End Sub

    Private Sub butCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butCancel.Click
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        ShowToolStripItems("Cancel")
        tabData.SelectedTabPage = tabData.TabPages(0)
        tabData.TabPages(1).PageVisible = False
        tabData.TabPages(0).PageVisible = True


        Me.Cursor = System.Windows.Forms.Cursors.Default

    End Sub

    Private Sub butClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butClose.Click, ToolStripButton6.Click
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        Me.Close()
        Me.Cursor = System.Windows.Forms.Cursors.Default
    End Sub

    Private Sub GetData()


        ClearRecord()

        Dim row As System.Data.DataRow = GridView1.GetDataRow(GridView1.FocusedRowHandle)

        Dim clsVentes As New Ventes
        clsVentes.IDVente = System.Convert.ToInt32(row("IDVente").ToString)
        clsVentes = oVentesData.Select_Record(clsVentes)

        If Not clsVentes Is Nothing Then
            Try
                nudVenteID.Text = System.Convert.ToInt32(clsVentes.IDVente)
                cbPeriodes.EditValue = If(IsNothing(clsVentes.IDPeriode), Nothing, clsVentes.IDPeriode)
                tbDate.Text = If(IsNothing(clsVentes.DateVente), Nothing, clsVentes.DateVente.ToString.Trim)
                tbDateFrom.Text = If(IsNothing(clsVentes.DateFrom), Nothing, clsVentes.DateFrom.ToString.Trim)
                tbDateTo.Text = If(IsNothing(clsVentes.DateTo), Nothing, clsVentes.DateTo.ToString.Trim)
                cbOrganisation.EditValue = If(IsNothing(clsVentes.OrganisationID), Nothing, clsVentes.OrganisationID.ToString.Trim)
                tbDescription.Text = If(IsNothing(clsVentes.Description), Nothing, clsVentes.Description.ToString.Trim)
                tbQuantite.Text = If(IsNothing(clsVentes.Unite), 0, clsVentes.Unite)

                tbTotal.Text = If(IsNothing(clsVentes.Montant), 0, clsVentes.Montant)


            Catch
            End Try
        End If


    End Sub

    Private Sub SetData(ByVal clsVentes As Ventes)
        With clsVentes
            .IDVente = If(String.IsNullOrEmpty(nudVenteID.Text), Nothing, nudVenteID.Text)
            .IDPeriode = If(String.IsNullOrEmpty(cbPeriodes.EditValue), Nothing, cbPeriodes.EditValue)
            .DateVente = If(String.IsNullOrEmpty(tbDate.Text), Nothing, tbDate.Text)
            .DateFrom = If(String.IsNullOrEmpty(tbDateFrom.Text), Nothing, tbDateFrom.Text)
            .DateTo = If(String.IsNullOrEmpty(tbDateTo.Text), Nothing, tbDateTo.Text)
            .OrganisationID = If(String.IsNullOrEmpty(cbOrganisation.EditValue), Nothing, Convert.ToInt32(cbOrganisation.EditValue))
            .Organisation = If(String.IsNullOrEmpty(cbOrganisation.Text), Nothing, cbOrganisation.Text)
            .Description = If(String.IsNullOrEmpty(tbDescription.Text), Nothing, tbDescription.Text)
            .Unite = If(String.IsNullOrEmpty(tbQuantite.Text), 0, Convert.ToDouble(tbQuantite.Text))

            .Montant = If(String.IsNullOrEmpty(tbTotal.Text), 0, Convert.ToDouble(tbTotal.EditValue))

        End With
    End Sub
    Private Function VerifyData() As Boolean

        If IsNothing(cbOrganisation.EditValue) Then
            MessageBox.Show("Vous devez sélectionner une organisation")
            cbOrganisation.Focus()
            Return False
        End If

        If IsNothing(cbPeriodes.EditValue) Then
            MessageBox.Show("Vous devez sélectionner une période")
            cbPeriodes.Focus()
            Return False
        End If

        If tbDescription.Text.ToString.Trim = "" Then
            MessageBox.Show("Vous devez sélectionner une description")
            tbDescription.Focus()
            Return False
        End If

        If tbQuantite.EditValue = 0 Then
            MessageBox.Show("Vous devez sélectionner une quantité")
            tbQuantite.Focus()
            Return False
        End If



        If tbTotal.EditValue = 0 Then
            MessageBox.Show("Vous devez sélectionner un total")
            tbTotal.Focus()
            Return False
        End If


        Return True
    End Function

    Private Sub UpdateRecord()
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor



        Dim row As System.Data.DataRow = GridView1.GetDataRow(GridView1.FocusedRowHandle)
        Dim clsVentes As New Ventes
        clsVentes.IDVente = System.Convert.ToInt32(row("IDVente").ToString)
        clsVentes = oVentesData.Select_Record(clsVentes)

        If VerifyData() = True Then
            SetData(clsVentes)
            Dim bSucess As Boolean
            bSucess = oVentesData.Update(clsVentes, clsVentes)
            If bSucess = True Then
                GoBack_To_Grid()
            Else
                MsgBox("Update failed.", MsgBoxStyle.OkOnly, "Erreur")
            End If
        End If
        Me.Cursor = System.Windows.Forms.Cursors.Default
    End Sub

    Private Sub DeleteRecord()
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

        Dim row As System.Data.DataRow = GridView1.GetDataRow(GridView1.FocusedRowHandle)

        Dim clsVentes As New Ventes
        clsVentes.IDVente = System.Convert.ToInt32(row("IDVente").ToString)
        clsVentes = oVentesData.Select_Record(clsVentes)


        clsVentes.IDVente = System.Convert.ToInt32(row("IDVente").ToString)
        Dim result As DialogResult = MessageBox.Show("Êtes-vous sur? Supprimer cet enregistrement?", "Supprimer", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
        If result = DialogResult.Yes Then
            SetData(clsVentes)
            Dim bSucess As Boolean
            bSucess = oVentesData.Delete(clsVentes)
            If bSucess = True Then
                GoBack_To_Grid()
            Else
                MsgBox("La supression a echouée.", MsgBoxStyle.OkOnly, "Erreur")
            End If
        End If
        Me.Cursor = System.Windows.Forms.Cursors.Default
    End Sub

    Private Sub InsertRecord()
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        Dim clsVentes As New Ventes
        If VerifyData() = True Then
            SetData(clsVentes)
            Dim bSucess As Boolean
            bSucess = oVentesData.Add(clsVentes)
            If bSucess = True Then
                GoBack_To_Grid()
            Else
                MsgBox("L'insertion a échouée.", MsgBoxStyle.OkOnly, "Erreur")
            End If
        End If
        Me.Cursor = System.Windows.Forms.Cursors.Default
    End Sub
    Private Sub butApply_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butApply.Click
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        If Me.AddMode = True Then
            Me.InsertRecord()
        ElseIf Me.EditMode = True Then
            Me.UpdateRecord()
        ElseIf Me.DeleteMode = True Then
            Me.DeleteRecord()
        End If
        Me.Cursor = System.Windows.Forms.Cursors.Default

    End Sub

    Private Sub TS_ItemClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolStripItemClickedEventArgs) Handles TS.ItemClicked
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

        Select Case e.ClickedItem.Text
            Case "Ajouter"

                tabData.SelectedTabPage = tabData.TabPages(1)
                tabData.TabPages(1).PageVisible = True
                tabData.TabPages(0).PageVisible = False
                Add()


            Case "Edition"
                tabData.SelectedTabPage = tabData.TabPages(1)
                tabData.TabPages(1).PageVisible = True
                tabData.TabPages(0).PageVisible = False
                Edit()

            Case "Supprimer"
                tabData.SelectedTabPage = tabData.TabPages(0)
                tabData.TabPages(0).PageVisible = True
                tabData.TabPages(1).PageVisible = False
                Delete()
            Case "Annuler"
                ShowToolStripItems("Cancel")
            Case "Rafraîchir"
                GoBack_To_Grid()
        End Select
        Me.Cursor = System.Windows.Forms.Cursors.Default
    End Sub

    Private Sub grdVentes_Click(sender As Object, e As EventArgs) Handles grdVentes.Click
        If GridView1.FocusedRowHandle < 0 Then Exit Sub

        Dim row As System.Data.DataRow = GridView1.GetDataRow(GridView1.FocusedRowHandle)


        SelectedRow = GridView1.FocusedRowHandle
    End Sub

    Private Sub grdVentes_DoubleClick(sender As Object, e As EventArgs) Handles grdVentes.DoubleClick
        Dim row As System.Data.DataRow = GridView1.GetDataRow(GridView1.FocusedRowHandle)

        SelectedRow = GridView1.FocusedRowHandle
        Edit()

        tabData.SelectedTabPage = tabData.TabPages(1)
        tabData.TabPages(1).PageVisible = True
        tabData.TabPages(0).PageVisible = False
    End Sub


    Private Sub ToolStripButton6_Click(sender As System.Object, e As System.EventArgs)
        Me.Close()
    End Sub

    Private Sub bAdd_Click_1(sender As Object, e As EventArgs) Handles bAdd.Click

    End Sub




End Class