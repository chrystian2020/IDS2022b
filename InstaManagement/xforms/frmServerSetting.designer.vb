﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmServerSetting
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmServerSetting))
        Me.Label2 = New System.Windows.Forms.Label()
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.SaveFileDialog1 = New System.Windows.Forms.SaveFileDialog()
        Me.cmdSave = New DevExpress.XtraEditors.SimpleButton()
        Me.cmdCancel = New DevExpress.XtraEditors.SimpleButton()
        Me.txtConnectionString = New DevExpress.XtraEditors.MemoEdit()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.lblB = New DevExpress.XtraEditors.LabelControl()
        CType(Me.txtConnectionString.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label2
        '
        resources.ApplyResources(Me.Label2, "Label2")
        Me.Label2.Name = "Label2"
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.FileName = "OpenFileDialog1"
        resources.ApplyResources(Me.OpenFileDialog1, "OpenFileDialog1")
        '
        'Label1
        '
        resources.ApplyResources(Me.Label1, "Label1")
        Me.Label1.ForeColor = System.Drawing.SystemColors.Highlight
        Me.Label1.Name = "Label1"
        '
        'SaveFileDialog1
        '
        resources.ApplyResources(Me.SaveFileDialog1, "SaveFileDialog1")
        '
        'cmdSave
        '
        resources.ApplyResources(Me.cmdSave, "cmdSave")
        Me.cmdSave.Name = "cmdSave"
        '
        'cmdCancel
        '
        resources.ApplyResources(Me.cmdCancel, "cmdCancel")
        Me.cmdCancel.Name = "cmdCancel"
        '
        'txtConnectionString
        '
        resources.ApplyResources(Me.txtConnectionString, "txtConnectionString")
        Me.txtConnectionString.Name = "txtConnectionString"
        Me.txtConnectionString.Properties.AccessibleDescription = resources.GetString("txtConnectionString.Properties.AccessibleDescription")
        Me.txtConnectionString.Properties.AccessibleName = resources.GetString("txtConnectionString.Properties.AccessibleName")
        Me.txtConnectionString.Properties.NullValuePrompt = resources.GetString("txtConnectionString.Properties.NullValuePrompt")
        Me.txtConnectionString.Properties.NullValuePromptShowForEmptyValue = CType(resources.GetObject("txtConnectionString.Properties.NullValuePromptShowForEmptyValue"), Boolean)
        '
        'PictureBox1
        '
        resources.ApplyResources(Me.PictureBox1, "PictureBox1")
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.TabStop = False
        '
        'lblB
        '
        resources.ApplyResources(Me.lblB, "lblB")

        Me.lblB.Appearance.FontSizeDelta = CType(resources.GetObject("lblB.Appearance.FontSizeDelta"), Integer)
        Me.lblB.Appearance.FontStyleDelta = CType(resources.GetObject("lblB.Appearance.FontStyleDelta"), System.Drawing.FontStyle)
        Me.lblB.Appearance.GradientMode = CType(resources.GetObject("lblB.Appearance.GradientMode"), System.Drawing.Drawing2D.LinearGradientMode)

        Me.lblB.Appearance.Image = CType(resources.GetObject("lblB.Appearance.Image"), System.Drawing.Image)
        Me.lblB.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near
        Me.lblB.Name = "lblB"
        '
        'frmServerSetting
        '
        resources.ApplyResources(Me, "$this")
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.txtConnectionString)
        Me.Controls.Add(Me.lblB)
        Me.Controls.Add(Me.cmdCancel)
        Me.Controls.Add(Me.cmdSave)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.PictureBox1)
        Me.Name = "frmServerSetting"
        CType(Me.txtConnectionString.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents SaveFileDialog1 As System.Windows.Forms.SaveFileDialog
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents cmdSave As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents cmdCancel As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents txtConnectionString As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents lblB As DevExpress.XtraEditors.LabelControl
End Class
