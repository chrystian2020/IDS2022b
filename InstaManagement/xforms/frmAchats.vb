﻿Imports System.Data.SqlClient
Imports System.Globalization
Imports System.Threading
Imports System.Text
Imports DevExpress.XtraGrid.Views.Grid.ViewInfo

Public Class frmAchats
    Private oAchatsData As New AchatsData
    Private oFournisseursData As New FournisseursData
    Private Achats As New Achats

    Private bload As Boolean = False
    Private bAddMode As Boolean = False
    Private bEditMode As Boolean = False
    Private bDeleteMode As Boolean = False
    Public blnGridClick As Boolean = False
    Private IDFournisseur As Integer
    Private iRowGrid As Int32 = 0
    Dim ssql As String
    Private Sub FillFournisseur()
        Dim drFournisseur As DataRow
        Dim dtFournisseur As DataTable

        Dim oFournisseurData As New FournisseursData


        Dim table As New DataTable
        table.Columns.Add("value", Type.GetType("System.Int32"))
        table.Columns.Add("displayText", Type.GetType("System.String"))
        dtFournisseur = oFournisseurData.SelectAll()


        For Each drFournisseur In dtFournisseur.Rows
            Dim teller As Integer = 0

            teller = drFournisseur("IDFournisseur")
            Dim toto As String = drFournisseur("Fournisseur").ToString.Trim

            table.Rows.Add(New Object() {teller, toto})
        Next

        With cbFournisseur.Properties
            .DataSource = table
            .NullText = "Selectionner un fournisseur"
            .DisplayMember = "displayText"
            .ValueMember = "value"
            .PopulateColumns()
            .PopupFormMinSize = New Size(50, 200)


        End With



    End Sub




    Private Sub frmAchats_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        bload = True
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

        Thread.CurrentThread.CurrentCulture = New CultureInfo("en-US", True)

        FillFournisseur()


        LoadGridCode()

        bload = False

        Me.Cursor = System.Windows.Forms.Cursors.Default
    End Sub

    Private Sub LoadGridCode()
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        Try

            Dim dDateFrom As DateTime = FirstDayOfMonth(go_Globals.PeriodeDateFrom)
            Dim dDateTo As DateTime = LastDayOfMonth(go_Globals.PeriodeDateFrom)

            grdAchat.DataSource = oAchatsData.SelectAllByDate(dDateFrom, dDateTo)
            GridView1.ClearSorting()


            GridView1.Columns("IDAchat").SortOrder = DevExpress.Data.ColumnSortOrder.Ascending

        Catch
        End Try
        Me.Cursor = System.Windows.Forms.Cursors.Default
    End Sub
    Public Function FirstDayOfMonth(ByVal sourceDate As DateTime) As DateTime
        Return New DateTime(sourceDate.Year, sourceDate.Month, 1)
    End Function

    'Get the last day of the month
    Public Function LastDayOfMonth(ByVal sourceDate As DateTime) As DateTime
        Dim lastDay As DateTime = New DateTime(sourceDate.Year, sourceDate.Month, 1)
        Return lastDay.AddMonths(1).AddDays(-1)
    End Function

    Private Sub Delete()
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        Me.AddMode = False
        Me.EditMode = False
        Me.DeleteMode = True
        GetData()

        EnableRecord(False)
        tabData.SelectedTabPage = tabData.TabPages(1)
        tabData.TabPages(1).PageVisible = True
        tabData.TabPages(0).PageVisible = False
        ShowToolStripItems("Delete")
        Me.Cursor = System.Windows.Forms.Cursors.Default
    End Sub


    Private Sub Add()
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        Me.AddMode = True
        Me.EditMode = False
        Me.DeleteMode = False

        ClearRecord()
        EnableRecord(True)
        Dim dDate As DateTime = go_Globals.PeriodeDateTo

        tbDate.Text = dDate.ToShortDateString


        tabData.SelectedTabPage = tabData.TabPages(1)
        tabData.TabPages(1).PageVisible = True
        tabData.TabPages(0).PageVisible = False
        ShowToolStripItems("Add")

        Me.Cursor = System.Windows.Forms.Cursors.Default
    End Sub
    Private Sub Edit()
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        Me.AddMode = False
        Me.EditMode = True
        Me.DeleteMode = False
        ClearRecord()
        GetData()

        EnableRecord(True)


        Me.Cursor = System.Windows.Forms.Cursors.Default
    End Sub

    Private Sub ClearRecord()
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor


        Me.nudAchatID.Text = Nothing
        Me.tbDate.Text = Nothing
        Me.cbFournisseur.EditValue = Nothing
        Me.tbQuantite.Text = Nothing
        Me.tbPrix.Text = Nothing

        tbmontant.Text = Nothing
        tbTPS.Text = Nothing
        tbTVQ.Text = Nothing
        Me.tbTotal.Text = Nothing
        tbDescription.Text = Nothing

        Me.Cursor = System.Windows.Forms.Cursors.Default
    End Sub

    Private Sub EnableRecord(ByVal YesNo As Boolean)
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

        Me.nudAchatID.Enabled = YesNo
        Me.tbDate.Enabled = YesNo
        Me.cbFournisseur.Enabled = YesNo
        Me.tbQuantite.Enabled = YesNo
        Me.tbPrix.Enabled = YesNo
        Me.tbTotal.Enabled = YesNo
        tbDescription.Enabled = YesNo
        tbmontant.Enabled = YesNo
        tbTPS.Enabled = YesNo
        tbTVQ.Enabled = YesNo
        Me.tbTotal.Enabled = YesNo

        Me.Cursor = System.Windows.Forms.Cursors.Default
    End Sub

    Private Sub GoBack_To_Grid()
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        Dim gridOK As Boolean = False
        Try
            ShowToolStripItems("Cancel")
            LoadGridCode()

            gridOK = True
        Catch
            'Hide Erreur message.
        Finally
            If gridOK = False Then
                ''''
                ShowToolStripItems("Cancel")
                ''''
            End If
        End Try
        Me.Cursor = System.Windows.Forms.Cursors.Default
    End Sub

    Private Sub ShowToolStripItems(ByVal Item As String)
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        bAdd.Enabled = False
        bEdit.Enabled = False
        bDelete.Enabled = False
        bCancel.Enabled = False
        bRefresh.Enabled = False
        Select Case Item
            Case "Add"
                bCancel.Enabled = True
            Case "Edit"
                bCancel.Enabled = True
            Case "Delete"
                bCancel.Enabled = True
            Case "Cancel"
                bAdd.Enabled = True
                bEdit.Enabled = True
                bDelete.Enabled = True
                bRefresh.Enabled = True
                tabData.SelectedTabPage = tabData.TabPages(0)
                tabData.TabPages(1).PageVisible = False
                tabData.TabPages(0).PageVisible = True
            Case "Refresh"
                bAdd.Enabled = True
                bEdit.Enabled = True
                bDelete.Enabled = True
                bRefresh.Enabled = True
                LoadGridCode()
            Case "No Record"
                bAdd.Enabled = True
                bRefresh.Enabled = True
        End Select

        Me.Cursor = System.Windows.Forms.Cursors.Default
    End Sub

    Public Property AddMode() As Boolean
        Get
            Return bAddMode
        End Get
        Set(ByVal value As Boolean)
            bAddMode = value
        End Set
    End Property

    Public Property EditMode() As Boolean
        Get
            Return bEditMode
        End Get
        Set(ByVal value As Boolean)
            bEditMode = value
        End Set
    End Property

    Public Property DeleteMode() As Boolean
        Get
            Return bDeleteMode
        End Get
        Set(ByVal value As Boolean)
            bDeleteMode = value
        End Set
    End Property

    Public Property SelectedRowFournisseur() As Int32
        Get
            Return iRowGrid
        End Get
        Set(ByVal value As Int32)
            iRowGrid = value
        End Set
    End Property

    Public Property SelectedRow() As Int32
        Get
            Return iRowGrid
        End Get
        Set(ByVal value As Int32)
            iRowGrid = value
        End Set
    End Property

    Private Sub bAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        tabData.SelectedTabPage = tabData.TabPages(1)
        tabData.TabPages(1).PageVisible = True
        tabData.TabPages(0).PageVisible = False
    End Sub

    Private Sub bEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        tabData.SelectedTabPage = tabData.TabPages(1)
        tabData.TabPages(1).PageVisible = True
        tabData.TabPages(0).PageVisible = False
    End Sub

    Private Sub bCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        tabData.SelectedTabPage = tabData.TabPages(0)
        tabData.TabPages(1).PageVisible = False
        tabData.TabPages(0).PageVisible = True
    End Sub

    Private Sub bDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        tabData.SelectedTabPage = tabData.TabPages(0)
        tabData.TabPages(1).PageVisible = False
        tabData.TabPages(0).PageVisible = True
    End Sub

    Private Sub butCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butCancel.Click
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        ShowToolStripItems("Cancel")
        tabData.SelectedTabPage = tabData.TabPages(0)
        tabData.TabPages(1).PageVisible = False
        LoadGridCode()
        tabData.TabPages(0).PageVisible = True


        Me.Cursor = System.Windows.Forms.Cursors.Default

    End Sub

    Private Sub butClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butClose.Click, ToolStripButton6.Click
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        Me.Close()
        Me.Cursor = System.Windows.Forms.Cursors.Default
    End Sub

    Private Sub GetData()


        ClearRecord()

        Dim row As System.Data.DataRow = GridView1.GetDataRow(GridView1.FocusedRowHandle)

        Dim clsAchats As New Achats
        clsAchats.IDAchat = System.Convert.ToInt32(row("IDAchat").ToString)
        clsAchats = oAchatsData.Select_Record(clsAchats)

        If Not clsAchats Is Nothing Then
            Try
                nudAchatID.Text = System.Convert.ToInt32(clsAchats.IDAchat)
                tbDate.Text = If(IsNothing(clsAchats.DateAchat), Nothing, clsAchats.DateAchat.ToString.Trim)
                cbFournisseur.EditValue = If(IsNothing(clsAchats.IDFournisseur), Nothing, clsAchats.IDFournisseur)
                tbDescription.Text = If(IsNothing(clsAchats.Description), Nothing, clsAchats.Description)
                tbQuantite.Text = If(IsNothing(clsAchats.Unite), 0, clsAchats.Unite)
                tbPrix.Text = If(IsNothing(clsAchats.Prix), 0, clsAchats.Prix)
                tbmontant.Text = If(IsNothing(clsAchats.Montant), 0, clsAchats.Montant)
                tbTPS.Text = If(IsNothing(clsAchats.TPS), 0, clsAchats.TPS)
                tbTVQ.Text = If(IsNothing(clsAchats.TVQ), 0, clsAchats.TVQ)
                tbTotal.Text = If(IsNothing(clsAchats.Total), 0, clsAchats.Total)


            Catch
            End Try
        End If


    End Sub

    Private Sub SetData(ByVal clsAchats As Achats)
        With clsAchats
            .IDAchat = If(String.IsNullOrEmpty(nudAchatID.Text), Nothing, nudAchatID.Text)
            .DateAchat = If(String.IsNullOrEmpty(tbDate.Text), Nothing, tbDate.Text)

            If IsNothing(cbFournisseur.EditValue) Then
                .IDFournisseur = Nothing
                .Fournisseur = Nothing
            Else
                .IDFournisseur = If(String.IsNullOrEmpty(cbFournisseur.EditValue), Nothing, cbFournisseur.EditValue)
                .Fournisseur = If(String.IsNullOrEmpty(cbFournisseur.Text), Nothing, cbFournisseur.Text)
            End If

            .Description = If(String.IsNullOrEmpty(tbDescription.Text), Nothing, tbDescription.Text)
            .Unite = If(String.IsNullOrEmpty(tbQuantite.Text), 0, Convert.ToDouble(tbQuantite.Text))
            .Prix = If(String.IsNullOrEmpty(tbPrix.EditValue), 0, Convert.ToDouble(tbPrix.EditValue))
            .Montant = If(String.IsNullOrEmpty(tbTotal.EditValue), 0, Convert.ToDouble(tbTotal.EditValue))
            .TPS = If(String.IsNullOrEmpty(tbTPS.EditValue), 0, Convert.ToDouble(tbTPS.EditValue))
            .TVQ = If(String.IsNullOrEmpty(tbTVQ.EditValue), 0, Convert.ToDouble(tbTVQ.EditValue))
            .Total = If(String.IsNullOrEmpty(tbTotal.EditValue), 0, Convert.ToDouble(tbTotal.EditValue))


        End With
    End Sub
    Private Function VerifyData() As Boolean




        If cbFournisseur.Text.ToString.Trim = "" Then
            MessageBox.Show("Vous devez sélectionner une description")
            cbFournisseur.Focus()
            Return False
        End If




        If tbTotal.EditValue = 0 Then
            MessageBox.Show("Vous devez sélectionner un total")
            tbTotal.Focus()
            Return False
        End If




        Return True
    End Function

    Private Sub UpdateRecord()
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor



        Dim row As System.Data.DataRow = GridView1.GetDataRow(GridView1.FocusedRowHandle)
        Dim clsAchats As New Achats
        clsAchats.IDAchat = System.Convert.ToInt32(row("IDAchat").ToString)
        clsAchats = oAchatsData.Select_Record(clsAchats)

        If VerifyData() = True Then
            SetData(clsAchats)
            Dim bSucess As Boolean
            bSucess = oAchatsData.Update(clsAchats, clsAchats)
            If bSucess = True Then
                GoBack_To_Grid()
            Else
                MsgBox("Update failed.", MsgBoxStyle.OkOnly, "Erreur")
            End If
        End If
        Me.Cursor = System.Windows.Forms.Cursors.Default
    End Sub

    Private Sub DeleteRecord()
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

        Dim row As System.Data.DataRow = GridView1.GetDataRow(GridView1.FocusedRowHandle)

        Dim clsAchats As New Achats
        clsAchats.IDAchat = System.Convert.ToInt32(row("IDAchat").ToString)
        clsAchats = oAchatsData.Select_Record(clsAchats)


        clsAchats.IDAchat = System.Convert.ToInt32(row("IDAchat").ToString)
        Dim result As DialogResult = MessageBox.Show("Êtes-vous sur? Supprimer cet enregistrement?", "Supprimer", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
        If result = DialogResult.Yes Then
            SetData(clsAchats)
            Dim bSucess As Boolean
            bSucess = oAchatsData.Delete(clsAchats)
            If bSucess = True Then
                GoBack_To_Grid()
            Else
                MsgBox("La supression a echouée.", MsgBoxStyle.OkOnly, "Erreur")
            End If
        End If
        Me.Cursor = System.Windows.Forms.Cursors.Default
    End Sub

    Private Sub InsertRecord()
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        Dim clsAchats As New Achats
        If VerifyData() = True Then
            SetData(clsAchats)
            Dim bSucess As Boolean
            bSucess = oAchatsData.Add(clsAchats)
            If bSucess = True Then
                GoBack_To_Grid()

            Else
                MsgBox("L'insertion a échouée.", MsgBoxStyle.OkOnly, "Erreur")
            End If
        End If
        Me.Cursor = System.Windows.Forms.Cursors.Default
    End Sub
    Private Sub butApply_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butApply.Click
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        If Me.AddMode = True Then
            Me.InsertRecord()
        ElseIf Me.EditMode = True Then
            Me.UpdateRecord()
        ElseIf Me.DeleteMode = True Then
            Me.DeleteRecord()
        End If
        Me.Cursor = System.Windows.Forms.Cursors.Default


    End Sub

    Private Sub TS_ItemClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolStripItemClickedEventArgs) Handles TS.ItemClicked
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

        Select Case e.ClickedItem.Text
            Case "Ajouter"

                tabData.SelectedTabPage = tabData.TabPages(1)
                tabData.TabPages(1).PageVisible = True
                tabData.TabPages(0).PageVisible = False
                Add()


            Case "Edition"
                tabData.SelectedTabPage = tabData.TabPages(1)
                tabData.TabPages(1).PageVisible = True
                tabData.TabPages(0).PageVisible = False
                Edit()

            Case "Supprimer"
                tabData.SelectedTabPage = tabData.TabPages(0)
                tabData.TabPages(0).PageVisible = True
                tabData.TabPages(1).PageVisible = False
                Delete()
            Case "Annuler"
                ShowToolStripItems("Cancel")
            Case "Rafraîchir"
                GoBack_To_Grid()
        End Select
        Me.Cursor = System.Windows.Forms.Cursors.Default
    End Sub

    Private Sub grdAchat_Click(sender As Object, e As EventArgs) Handles grdAchat.Click
        If GridView1.FocusedRowHandle < 0 Then Exit Sub

        Dim row As System.Data.DataRow = GridView1.GetDataRow(GridView1.FocusedRowHandle)


        SelectedRow = GridView1.FocusedRowHandle
    End Sub

    Private Sub grdAchat_DoubleClick(sender As Object, e As EventArgs) Handles grdAchat.DoubleClick
        Dim row As System.Data.DataRow = GridView1.GetDataRow(GridView1.FocusedRowHandle)

        SelectedRow = GridView1.FocusedRowHandle
        Edit()

        tabData.SelectedTabPage = tabData.TabPages(1)
        tabData.TabPages(1).PageVisible = True
        tabData.TabPages(0).PageVisible = False
    End Sub

    Private Sub bAdd_Click_1(sender As Object, e As EventArgs) Handles bAdd.Click

    End Sub

    Private Sub tbPrix_EditValueChanged(sender As Object, e As EventArgs) Handles tbPrix.EditValueChanged
        If Not IsNothing(tbPrix.EditValue) And Not IsNothing(tbQuantite.EditValue) Then

            tbTotal.EditValue = tbPrix.EditValue * tbQuantite.Text

        End If
    End Sub

    Private Sub bAjouter_Click(sender As Object, e As EventArgs)


    End Sub

    Private Sub bAddFournisseur_Click(sender As Object, e As EventArgs)


        grdFournisseurs.DataSource = oFournisseursData.SelectAll()

        DockFournisseurs.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Visible
    End Sub

    Private Sub bFermer_ItemClicked(sender As Object, e As ToolStripItemClickedEventArgs)
        DockFournisseurs.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Hidden
    End Sub

    Private Sub grdFournisseurs_Click(sender As Object, e As EventArgs)
        If GridView2.FocusedRowHandle < 0 Then Exit Sub

        Dim row As System.Data.DataRow = GridView2.GetDataRow(GridView2.FocusedRowHandle)

        IDFournisseur = Convert.ToInt32(row("IDFournisseur"))

        SelectedRowFournisseur = GridView2.FocusedRowHandle
        btnSupprimer.Enabled = True
    End Sub



    Private Sub btnAjouter_Click(sender As Object, e As EventArgs)


        If tbFournisseur.Text.ToString.Trim = "" Then
            MessageBox.Show("Vous dever entrer un fournisseur")
            tbFournisseur.Focus()
            Exit Sub
        End If

        Dim oFournisseur As New Fournisseurs
        Dim oFournisseurData As New FournisseursData
        oFournisseur.Fournisseur = tbFournisseur.Text.ToString.Trim
        oFournisseurData.Add(oFournisseur)
        grdFournisseurs.DataSource = oFournisseurData.SelectAll()
        FillFournisseur()
        tbFournisseur.Text = ""

    End Sub

    Private Sub btnSupprimer_Click(sender As Object, e As EventArgs)
        Dim oFournisseurData As New FournisseursData
        Dim oFournisseur As New Fournisseurs

        oFournisseur.IDFournisseur = IDFournisseur
        oFournisseurData.Delete(oFournisseur)
        grdFournisseurs.DataSource = oFournisseurData.SelectAll()
        FillFournisseur()
        btnSupprimer.Enabled = False
    End Sub

    Private Sub tbmontant_EditValueChanged(sender As Object, e As EventArgs) Handles tbmontant.EditValueChanged

    End Sub

    Private Sub tbQuantite_EditValueChanged(sender As Object, e As EventArgs) Handles tbQuantite.EditValueChanged
        If Not IsNothing(tbPrix.EditValue) And Not IsNothing(tbQuantite.EditValue) Then

            tbTotal.EditValue = tbPrix.EditValue * tbQuantite.Text

        End If
    End Sub


    Private Sub cbFournisseur_EditValueChanged(sender As Object, e As EventArgs) Handles cbFournisseur.EditValueChanged

        If Not IsNothing(cbFournisseur.EditValue) Then
            tbDescription.Text = cbFournisseur.Text
        Else
            tbDescription.Text = Nothing
        End If

    End Sub

    Private Sub tbTotalEditValueChanged()


        Dim dblCombinedRate As Double = 1 + (go_Globals.TPS + go_Globals.TVQ)

        If chkCalculerTaxes.Checked = True Then

            tbmontant.EditValue = Math.Round(Convert.ToDouble(tbTotal.EditValue) / dblCombinedRate, 2)

            tbTPS.Text = Math.Round(Convert.ToDouble(tbmontant.EditValue) * Convert.ToDouble(go_Globals.TPS), 2)
            tbTVQ.Text = Math.Round(Convert.ToDouble(tbmontant.EditValue) * Convert.ToDouble(go_Globals.TVQ), 2)



        Else

            tbTPS.Text = 0
            tbTVQ.Text = 0
            tbmontant.EditValue = Convert.ToDouble(tbTotal.EditValue)


        End If

    End Sub

    Private Sub tbTotal_EditValueChanged(sender As Object, e As EventArgs) Handles tbTotal.EditValueChanged

        Dim dblCombinedRate As Double = 1 + (go_Globals.TPS + go_Globals.TVQ)

        If chkCalculerTaxes.Checked = True Then

            tbmontant.EditValue = Math.Round(Convert.ToDouble(tbTotal.EditValue) / dblCombinedRate, 2)

            tbTPS.Text = Math.Round(Convert.ToDouble(tbmontant.EditValue) * Convert.ToDouble(go_Globals.TPS), 2)
            tbTVQ.Text = Math.Round(Convert.ToDouble(tbmontant.EditValue) * Convert.ToDouble(go_Globals.TVQ), 2)



        Else

            tbTPS.Text = 0
            tbTVQ.Text = 0
            tbmontant.EditValue = Convert.ToDouble(tbTotal.EditValue)


        End If


    End Sub


    Private Sub chkCalculerTaxes_CheckedChanged(sender As Object, e As EventArgs) Handles chkCalculerTaxes.CheckedChanged

        If chkCalculerTaxes.Checked = True Then
            tbTotalEditValueChanged()

        Else

            tbTPS.Text = 0
            tbTVQ.Text = 0
            tbmontant.EditValue = Convert.ToDouble(tbTotal.EditValue)


        End If
    End Sub
End Class