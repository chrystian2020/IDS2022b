﻿Imports System.Data.SqlClient
Imports System.Globalization
Imports System.Threading
Imports System.Text
Imports GemBox.Spreadsheet

Public Class frmLivreurs

    Private oLivreursData As New LivreursData
    Private oLivreurs As New livreurs

    Private bload As Boolean = False
    Private bAddMode As Boolean = False
    Private bEditMode As Boolean = False
    Private bDeleteMode As Boolean = False
    Public blnGridClick As Boolean = False
    Private iRowGrid As Int32 = 0
    Dim ssql As String



    Private Sub FillAllLivreur()

        Dim dtLivreur As DataTable
        Dim drLivreur As DataRow
        Dim oLivreurData As New LivreursData


        Dim table As New DataTable
        table.Columns.Add("value", Type.GetType("System.Int32"))
        table.Columns.Add("displayText", Type.GetType("System.String"))
        dtLivreur = oLivreurData.SelectAll()



        For Each drLivreur In dtLivreur.Rows
            Dim teller As Integer = 0

            teller = drLivreur("LivreurID")
            Dim toto As String = drLivreur("Livreur")

            table.Rows.Add(New Object() {teller, toto})
        Next



        With tbIDLivreurChangerDestination.Properties
            .DataSource = table
            .NullText = "Selectionner un livreur"
            .DisplayMember = "displayText"
            .ValueMember = "value"
            .PopulateColumns()
            .Columns(0).Visible = True
            .Columns(0).Width = 200
            .Columns(1).Visible = True
            .PopupFormMinSize = New Size(50, 200)
        End With
    End Sub


    Private Sub nudLivreurID_EditValueChanged(sender As System.Object, e As System.EventArgs) Handles nudLivreurID.EditValueChanged

    End Sub

    Private Sub tabData_Click(sender As System.Object, e As System.EventArgs) Handles tabData.Click

    End Sub

    Private Sub frmLivreurs_Load(sender As Object, e As System.EventArgs) Handles Me.Load

        bload = True
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

        Thread.CurrentThread.CurrentCulture = New CultureInfo("en-US", True)

        FillAllLivreur()
        LoadGridCode()

        bload = False

        Me.Cursor = System.Windows.Forms.Cursors.Default
    End Sub

    Private Sub FillAllVendeurs()


        Dim dtLivreur As DataTable
        Dim drLivreur As DataRow
        Dim oLivreurData As New LivreursData

        Dim table As New DataTable
        table.Columns.Add("value", Type.GetType("System.Int32"))
        table.Columns.Add("Livreur", Type.GetType("System.String"))
        dtLivreur = oLivreurData.SelectAllVendeurs



        For Each drLivreur In dtLivreur.Rows
            Dim teller As Integer = 0

            teller = drLivreur("LivreurID")
            Dim toto As String = drLivreur("Livreur")
            table.Rows.Add(New Object() {teller, toto})
        Next


        cbVendeur.Properties.DataSource = table
        cbVendeur.Properties.NullText = "Selectionner un vendeur"
        cbVendeur.Properties.DisplayMember = "Livreur"
        cbVendeur.Properties.ValueMember = "value"
        cbVendeur.Properties.PopupFormMinSize = New Size(50, 200)


    End Sub
    Private Sub LoadGridCode()
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        Try

            grdLivreurs.DataSource = oLivreursData.SelectAllLivreurs
            GridView1.ClearSorting()
            GridView1.Columns("Livreur").SortOrder = DevExpress.Data.ColumnSortOrder.Ascending
            'Dim viewInfo As DevExpress.XtraGrid.Views.Grid.ViewInfo.GridViewInfo = TryCast(GridView1.GetViewInfo(), DevExpress.XtraGrid.Views.Grid.ViewInfo.GridViewInfo)
            'If viewInfo.ViewRects.ColumnTotalWidth < viewInfo.ViewRects.ColumnPanelWidth Then
            '    GridView1.OptionsView.ColumnAutoWidth = True
            'End If


            'GridView1.Columns("LivreurID").Width = 400
            'GridView1.Columns("Livreur").Width = 2000
            'GridView1.Columns("Telephone").Width = 400
            'GridView1.Columns("IsVendeur").Width = 400
            'GridView1.Columns("Actif").Width = 100
            'GridView1.Columns("CalculerTaxes").Width = 100
            'GridView1.Columns("Email").Width = 100
            'GridView1.BestFitColumns()
            GridView1.OptionsView.ColumnAutoWidth = True
            GridView1.BestFitColumns()



        Catch
        End Try
        Me.Cursor = System.Windows.Forms.Cursors.Default
    End Sub


    Private Sub Delete()
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        Me.AddMode = False
        Me.EditMode = False
        Me.DeleteMode = True
        GetData()

        EnableRecord(False)
        tabData.SelectedTabPage = tabData.TabPages(1)
        tabData.TabPages(1).PageVisible = True
        tabData.TabPages(0).PageVisible = False
        ShowToolStripItems("Delete")
        Me.Cursor = System.Windows.Forms.Cursors.Default
    End Sub


    Private Sub Add()
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        Me.AddMode = True
        Me.EditMode = False
        Me.DeleteMode = False

        ClearRecord()
        EnableRecord(True)


        tabData.SelectedTabPage = tabData.TabPages(1)
        tabData.TabPages(1).PageVisible = True
        tabData.TabPages(0).PageVisible = False
        ShowToolStripItems("Add")

        Me.Cursor = System.Windows.Forms.Cursors.Default
    End Sub
    Private Sub Edit()
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        Me.AddMode = False
        Me.EditMode = True
        Me.DeleteMode = False
        ClearRecord()
        GetData()

        EnableRecord(True)


        Me.Cursor = System.Windows.Forms.Cursors.Default
    End Sub

    Private Sub ClearRecord()
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

        Me.nudLivreurID.Text = Nothing
        Me.tbLivreur.Text = Nothing
        Me.tbLivreurCache.Text = Nothing
        Me.tbTelephone.Text = Nothing
        Me.tbAdresse.Text = Nothing
        Me.ckCalculerTaxes.Checked = False
        tbEmail.Text = Nothing
        tbNomCompagnie.Text = Nothing
        tbNoTPS.Text = Nothing
        tbNoTVQ.Text = Nothing

        cbVendeur.EditValue = 0
        tbCommissionVendeur.Text = 0


        Me.Cursor = System.Windows.Forms.Cursors.Default
    End Sub

    Private Sub EnableRecord(ByVal YesNo As Boolean)
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

        Me.tbLivreur.Enabled = YesNo
        Me.tbLivreurCache.Enabled = YesNo
        Me.tbTelephone.Enabled = YesNo
        Me.tbAdresse.Enabled = YesNo
        Me.ckCalculerTaxes.Enabled = YesNo
        tbEmail.Enabled = YesNo
        cbVendeur.Enabled = YesNo
        tbCommissionVendeur.Enabled = YesNo
        tbNomCompagnie.Enabled = YesNo
        tbNoTPS.Enabled = YesNo
        tbNoTVQ.Enabled = YesNo


        Me.Cursor = System.Windows.Forms.Cursors.Default
    End Sub

    Private Sub GoBack_To_Grid()
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        Dim gridOK As Boolean = False
        Try
            ShowToolStripItems("Cancel")
            LoadGridCode()

            gridOK = True
        Catch
            'Hide Erreur message.
        Finally
            If gridOK = False Then
                ''''
                ShowToolStripItems("Cancel")
                ''''
            End If
        End Try
        Me.Cursor = System.Windows.Forms.Cursors.Default
    End Sub

    Private Sub ShowToolStripItems(ByVal Item As String)
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        bAdd.Enabled = False
        bEdit.Enabled = False
        bCancel.Enabled = False
        bRefresh.Enabled = False
        Select Case Item
            Case "Add"
                bCancel.Enabled = True
            Case "Edit"
                bCancel.Enabled = True
            Case "Delete"
                bCancel.Enabled = True
            Case "Cancel"
                bAdd.Enabled = True
                bEdit.Enabled = True
                bRefresh.Enabled = True
                tabData.SelectedTabPage = tabData.TabPages(0)
                tabData.TabPages(1).PageVisible = False
                tabData.TabPages(0).PageVisible = True
            Case "Refresh"
                bAdd.Enabled = True
                bEdit.Enabled = True
                bRefresh.Enabled = True
                LoadGridCode()
            Case "No Record"
                bAdd.Enabled = True
                bRefresh.Enabled = True
        End Select

        Me.Cursor = System.Windows.Forms.Cursors.Default
    End Sub

    Public Property AddMode() As Boolean
        Get
            Return bAddMode
        End Get
        Set(ByVal value As Boolean)
            bAddMode = value
        End Set
    End Property

    Public Property EditMode() As Boolean
        Get
            Return bEditMode
        End Get
        Set(ByVal value As Boolean)
            bEditMode = value
        End Set
    End Property

    Public Property DeleteMode() As Boolean
        Get
            Return bDeleteMode
        End Get
        Set(ByVal value As Boolean)
            bDeleteMode = value
        End Set
    End Property


    Public Property SelectedRow() As Int32
        Get
            Return iRowGrid
        End Get
        Set(ByVal value As Int32)
            iRowGrid = value
        End Set
    End Property

    Private Sub bAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        tabData.SelectedTabPage = tabData.TabPages(1)
        tabData.TabPages(1).PageVisible = True
        tabData.TabPages(0).PageVisible = False
    End Sub

    Private Sub bEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        tabData.SelectedTabPage = tabData.TabPages(1)
        tabData.TabPages(1).PageVisible = True
        tabData.TabPages(0).PageVisible = False
    End Sub

    Private Sub bCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        tabData.SelectedTabPage = tabData.TabPages(0)
        tabData.TabPages(1).PageVisible = False
        tabData.TabPages(0).PageVisible = True
    End Sub

    Private Sub bDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        tabData.SelectedTabPage = tabData.TabPages(0)
        tabData.TabPages(1).PageVisible = False
        tabData.TabPages(0).PageVisible = True
    End Sub

    Private Sub butCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butCancel.Click
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        ShowToolStripItems("Cancel")
        tabData.SelectedTabPage = tabData.TabPages(0)
        tabData.TabPages(1).PageVisible = False
        tabData.TabPages(0).PageVisible = True


        Me.Cursor = System.Windows.Forms.Cursors.Default

    End Sub

    Private Sub butClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butClose.Click
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        Me.Close()
        Me.Cursor = System.Windows.Forms.Cursors.Default
    End Sub


    Private Sub GetData()


        ClearRecord()

        Dim row As System.Data.DataRow = GridView1.GetDataRow(GridView1.FocusedRowHandle)

        Dim clsLivreur As New livreurs
        clsLivreur.LivreurID = System.Convert.ToInt32(row("LivreurID").ToString)
        clsLivreur = oLivreursData.Select_Record(clsLivreur)

        If Not clsLivreur Is Nothing Then
            Try
                nudLivreurID.Text = System.Convert.ToInt32(clsLivreur.LivreurID)
                tbLivreur.Text = If(IsNothing(clsLivreur.Livreur), Nothing, clsLivreur.Livreur.ToString.Trim)
                tbLivreurCache.Text = If(IsNothing(clsLivreur.LivreurCache), Nothing, clsLivreur.LivreurCache.ToString.Trim)
                tbAdresse.Text = If(IsNothing(clsLivreur.Adresse), Nothing, clsLivreur.Adresse.ToString.Trim)
                tbTelephone.Text = If(IsNothing(clsLivreur.Telephone), Nothing, clsLivreur.Telephone.ToString.Trim)
                tbEmail.Text = If(IsNothing(clsLivreur.Email), Nothing, clsLivreur.Email.ToString.Trim)
                tbEmail1.Text = If(IsNothing(clsLivreur.Email1), Nothing, clsLivreur.Email1.ToString.Trim)
                tbEmail2.Text = If(IsNothing(clsLivreur.Email2), Nothing, clsLivreur.Email2.ToString.Trim)

                If clsLivreur.CalculerTaxes = True Then
                    ckCalculerTaxes.Checked = True
                Else
                    ckCalculerTaxes.Checked = False
                End If

                If clsLivreur.IsVendeur = True Then
                    ckVendeur.Checked = True
                Else
                    ckVendeur.Checked = False
                End If

                If clsLivreur.Actif = True Then
                    ckActif.Checked = True
                Else
                    ckActif.Checked = False
                End If

                If clsLivreur.IDVendeur > 0 Then

                    cbVendeur.EditValue = clsLivreur.IDVendeur
                Else
                    cbVendeur.EditValue = Nothing
                End If

                Me.tbCommissionVendeur.Text = If(clsLivreur.CommissionVendeur Is Nothing, Nothing, System.Convert.ToDecimal(clsLivreur.CommissionVendeur))


                tbNomCompagnie.Text = If(IsNothing(clsLivreur.NomCompagnie), Nothing, clsLivreur.NomCompagnie.ToString.Trim)
                tbNoTPS.Text = If(IsNothing(clsLivreur.NoTPS), Nothing, clsLivreur.NoTPS.ToString.Trim)
                tbNoTVQ.Text = If(IsNothing(clsLivreur.NoTVQ), Nothing, clsLivreur.NoTVQ.ToString.Trim)



            Catch
            End Try
        End If


    End Sub

    Private Sub SetData(ByVal clsLivreurs As livreurs)
        With clsLivreurs
            .LivreurID = If(String.IsNullOrEmpty(nudLivreurID.Text), Nothing, nudLivreurID.Text)
            .Livreur = If(String.IsNullOrEmpty(tbLivreur.Text), Nothing, tbLivreur.Text)
            .LivreurCache = If(String.IsNullOrEmpty(tbLivreurCache.Text), Nothing, tbLivreurCache.Text)
            .Adresse = If(String.IsNullOrEmpty(tbAdresse.Text), Nothing, tbAdresse.Text)
            .Telephone = If(String.IsNullOrEmpty(tbTelephone.Text), Nothing, tbTelephone.Text)
            .Email = If(String.IsNullOrEmpty(tbEmail.Text), Nothing, tbEmail.Text)
            .Email1 = If(String.IsNullOrEmpty(tbEmail1.Text), Nothing, tbEmail1.Text)
            .Email2 = If(String.IsNullOrEmpty(tbEmail2.Text), Nothing, tbEmail2.Text)
            If ckCalculerTaxes.Checked = True Then
                .CalculerTaxes = 1
            Else
                .CalculerTaxes = 0
            End If

            If ckVendeur.Checked = True Then
                .IsVendeur = 1
            Else
                .IsVendeur = 0
            End If

            If ckActif.Checked = True Then
                .Actif = 1
            Else
                .Actif = 0
            End If

            If Not IsNothing(cbVendeur.EditValue) Then
                If cbVendeur.EditValue.ToString.Trim <> "" Then
                    .IDVendeur = cbVendeur.EditValue
                    .CommissionVendeur = If(String.IsNullOrEmpty(tbCommissionVendeur.Text), Nothing, tbCommissionVendeur.Text)
                End If
            Else
                .IDVendeur = Nothing
                .CommissionVendeur = Nothing
            End If

            .NomCompagnie = If(String.IsNullOrEmpty(tbNomCompagnie.Text), Nothing, tbNomCompagnie.Text.ToString.Trim)
            .NoTPS = If(String.IsNullOrEmpty(tbNoTPS.Text), Nothing, tbNoTPS.Text.ToString.Trim)
            .NoTVQ = If(String.IsNullOrEmpty(tbNoTVQ.Text), Nothing, tbNoTVQ.Text.ToString.Trim)


        End With
    End Sub
    Private Function VerifyData() As Boolean
        Return True
    End Function

    Private Sub UpdateRecord()
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        Dim oclsLivreur As New livreurs


        Dim row As System.Data.DataRow = GridView1.GetDataRow(GridView1.FocusedRowHandle)
        oclsLivreur.LivreurID = System.Convert.ToInt32(row("LivreurID").ToString)
        oclsLivreur = oLivreursData.Select_Record(oclsLivreur)

        If VerifyData() = True Then
            SetData(oclsLivreur)
            Dim bSucess As Boolean
            bSucess = oLivreursData.Update(oclsLivreur, oclsLivreur)
            If bSucess = True Then
                GoBack_To_Grid()
            Else
                MsgBox("Update failed.", MsgBoxStyle.OkOnly, "Erreur")
            End If
        End If
        Me.Cursor = System.Windows.Forms.Cursors.Default
    End Sub

    Private Sub DeleteRecord()
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        Dim oclsLivreur As New livreurs

        Dim row As System.Data.DataRow = GridView1.GetDataRow(GridView1.FocusedRowHandle)
        oclsLivreur.LivreurID = System.Convert.ToInt32(row("LivreurID").ToString)
        Dim result As DialogResult = MessageBox.Show("Êtes-vous sur? Supprimer cet enregistrement?", "Supprimer", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
        If result = DialogResult.Yes Then
            SetData(oclsLivreur)
            Dim bSucess As Boolean
            bSucess = oLivreursData.Delete(oclsLivreur)
            Dim strSQL As String

            strSQL = "DELETE FROM Horaire WHERE IDPeriode = " & go_Globals.IDPeriode & " AND IDLivreur=" & oclsLivreur.LivreurID
            SysTemGlobals.ExecuteSQL(strSQL)

            strSQL = "DELETE FROM HoraireJour WHERE IDPeriode = " & go_Globals.IDPeriode & " AND IDLivreur=" & oclsLivreur.LivreurID
            SysTemGlobals.ExecuteSQL(strSQL)


            If bSucess = True Then
                GoBack_To_Grid()
            Else
                MsgBox("La supression a echouée.", MsgBoxStyle.OkOnly, "Erreur")
            End If
        End If
        Me.Cursor = System.Windows.Forms.Cursors.Default
    End Sub

    Private Sub InsertRecord()
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        Dim oclsLivreur As New livreurs
        If VerifyData() = True Then
            SetData(oclsLivreur)
            Dim bSucess As Boolean
            bSucess = oLivreursData.Add(oclsLivreur)
            If bSucess = True Then
                GoBack_To_Grid()
            Else
                MsgBox("L'insertion a échouée.", MsgBoxStyle.OkOnly, "Erreur")
            End If
        End If
        Me.Cursor = System.Windows.Forms.Cursors.Default
    End Sub
    Private Sub butApply_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butApply.Click
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        If Me.AddMode = True Then
            Me.InsertRecord()
        ElseIf Me.EditMode = True Then
            Me.UpdateRecord()
        ElseIf Me.DeleteMode = True Then
            Me.DeleteRecord()
        End If
        Me.Cursor = System.Windows.Forms.Cursors.Default

        tabData.SelectedTabPage = tabData.TabPages(0)
        tabData.TabPages(1).PageVisible = False
        tabData.TabPages(0).PageVisible = True
    End Sub

    Private Sub TS_ItemClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolStripItemClickedEventArgs) Handles TS.ItemClicked
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

        Select Case e.ClickedItem.Text
            Case "Ajouter"

                tabData.SelectedTabPage = tabData.TabPages(1)
                tabData.TabPages(1).PageVisible = True
                tabData.TabPages(0).PageVisible = False
                Add()


            Case "Edition"
                tabData.SelectedTabPage = tabData.TabPages(1)
                tabData.TabPages(1).PageVisible = True
                tabData.TabPages(0).PageVisible = False
                Edit()

            Case "Supprimer"
                tabData.SelectedTabPage = tabData.TabPages(0)
                tabData.TabPages(0).PageVisible = True
                tabData.TabPages(1).PageVisible = False
                Delete()
            Case "Annuler"
                ShowToolStripItems("Cancel")
            Case "Rafraîchir"
                GoBack_To_Grid()
        End Select
        Me.Cursor = System.Windows.Forms.Cursors.Default
    End Sub


    Private Sub grdLivreurs_Click(sender As System.Object, e As System.EventArgs) Handles grdLivreurs.Click
        If GridView1.FocusedRowHandle < 0 Then Exit Sub

        Dim row As System.Data.DataRow = GridView1.GetDataRow(GridView1.FocusedRowHandle)


        SelectedRow = GridView1.FocusedRowHandle
    End Sub

    Private Sub grdLivreurs_DoubleClick(sender As Object, e As System.EventArgs) Handles grdLivreurs.DoubleClick
        Dim row As System.Data.DataRow = GridView1.GetDataRow(GridView1.FocusedRowHandle)

        SelectedRow = GridView1.FocusedRowHandle
        Edit()
        FillAllVendeurs()
        tabData.SelectedTabPage = tabData.TabPages(1)
        tabData.TabPages(1).PageVisible = True
        tabData.TabPages(0).PageVisible = False
    End Sub



    Private Sub ToolStripButton6_Click(sender As System.Object, e As System.EventArgs) Handles ToolStripButton6.Click
        Me.Close()
    End Sub


    Private Function GetMonthName(d As DateTime, ci As Globalization.CultureInfo) As String

        Dim sMonth As String = ""


        sMonth = ci.DateTimeFormat.GetMonthName(d.Month)

        Select Case sMonth
            Case "janvier"
                sMonth = "Janvier"
            Case "février"
                sMonth = "Février"
            Case "mars"
                sMonth = "Mars"
            Case "avril"
                sMonth = "Avril"
            Case "mai"
                sMonth = "Mai"
            Case "juin"
                sMonth = "Juin"
            Case "juillet"
                sMonth = "Juillet"
            Case "août"
                sMonth = "Août"
            Case "septembre"
                sMonth = "Septembre"
            Case "octobre"
                sMonth = "Octobre"
            Case "novembre"
                sMonth = "Novembre"
            Case "décembre"
                sMonth = "Décembre"

        End Select

        Return sMonth
    End Function

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click


        If IsNothing(tbIDLivreurDepart.EditValue) Then
            MessageBox.Show("Vous devez entrer un Livreur ID Source")
            Exit Sub
        End If

        If IsNothing(tbIDLivreurChangerDestination.EditValue) Then
            MessageBox.Show("Vous devez entrer le livreur ID destination")
            Exit Sub
        End If



        Dim result As DialogResult = MessageBox.Show("Attention, le Livreur ID " & tbIDLivreurDepart.EditValue.ToString.ToString & " Va devenir le Livreur ID " & tbIDLivreurChangerDestination.EditValue.ToString.ToString & vbLf & " Cette modification va affecter toutes les tables", "Remplacer un Livreur ID dans la BD.", MessageBoxButtons.YesNo)


        If result = DialogResult.No Then
            Exit Sub
        ElseIf result = DialogResult.Yes Then

        End If

        Cursor = Cursors.WaitCursor

        SysTemGlobals.ExecuteSQL("Update Achats set IDLivreur =" & tbIDLivreurChangerDestination.EditValue.ToString & ", Livreur = '" & tbIDLivreurChangerDestination.Text.ToString.Trim & "' WHERE IDlivreur = " & tbIDLivreurDepart.Text)
        SysTemGlobals.ExecuteSQL("Update CadeauLivreur set IDLivreur =" & tbIDLivreurChangerDestination.EditValue.ToString & ", Livreur = '" & tbIDLivreurChangerDestination.Text.ToString.Trim & "' WHERE IDlivreur = " & tbIDLivreurDepart.Text)
        SysTemGlobals.ExecuteSQL("Update Commission set IDLivreur =" & tbIDLivreurChangerDestination.EditValue.ToString & ", Livreur = '" & tbIDLivreurChangerDestination.Text.ToString.Trim & "' WHERE IDlivreur = " & tbIDLivreurDepart.Text)
        SysTemGlobals.ExecuteSQL("Update CommissionKMLivreur set LivreurID =" & tbIDLivreurChangerDestination.EditValue.ToString & ", Livreur = '" & tbIDLivreurChangerDestination.Text.ToString.Trim & "' WHERE LivreurID = " & tbIDLivreurDepart.Text)
        SysTemGlobals.ExecuteSQL("Update CommissionLine set IDLivreur =" & tbIDLivreurChangerDestination.EditValue.ToString & ", Livreur = '" & tbIDLivreurChangerDestination.Text.ToString.Trim & "' WHERE IDlivreur = " & tbIDLivreurDepart.Text)
        SysTemGlobals.ExecuteSQL("Update CommissionParHeure set LivreurID =" & tbIDLivreurChangerDestination.EditValue.ToString & ", Livreur = '" & tbIDLivreurChangerDestination.Text.ToString.Trim & "' WHERE LivreurID = " & tbIDLivreurDepart.Text)

        SysTemGlobals.ExecuteSQL("Update Epargnes set IDLivreur =" & tbIDLivreurChangerDestination.EditValue.ToString & ", Livreur = '" & tbIDLivreurChangerDestination.Text.ToString.Trim & "' WHERE IDlivreur = " & tbIDLivreurDepart.Text)
        SysTemGlobals.ExecuteSQL("Update EpargnesPaiements set IDLivreur =" & tbIDLivreurChangerDestination.EditValue.ToString & ", Livreur = '" & tbIDLivreurChangerDestination.Text.ToString.Trim & "' WHERE IDlivreur = " & tbIDLivreurDepart.Text)
        SysTemGlobals.ExecuteSQL("Update EpargnesRetraits set IDLivreur =" & tbIDLivreurChangerDestination.EditValue.ToString & ", Livreur = '" & tbIDLivreurChangerDestination.Text.ToString.Trim & "' WHERE IDlivreur = " & tbIDLivreurDepart.Text)
        SysTemGlobals.ExecuteSQL("Update Horaire set IDLivreur =" & tbIDLivreurChangerDestination.EditValue.ToString & ", Livreur = '" & tbIDLivreurChangerDestination.Text.ToString.Trim & "' WHERE IDlivreur = " & tbIDLivreurDepart.Text)
        SysTemGlobals.ExecuteSQL("Update HoraireJour set IDLivreur =" & tbIDLivreurChangerDestination.EditValue.ToString & ", Livreur = '" & tbIDLivreurChangerDestination.Text.ToString.Trim & "' WHERE IDlivreur = " & tbIDLivreurDepart.Text)
        SysTemGlobals.ExecuteSQL("Update Import set LivreurID =" & tbIDLivreurChangerDestination.EditValue.ToString & ", Livreur = '" & tbIDLivreurChangerDestination.Text.ToString.Trim & "' WHERE LivreurID = " & tbIDLivreurDepart.Text)
        SysTemGlobals.ExecuteSQL("Update ImportBackup set LivreurID =" & tbIDLivreurChangerDestination.EditValue.ToString & ", Livreur = '" & tbIDLivreurChangerDestination.Text.ToString.Trim & "' WHERE LivreurID = " & tbIDLivreurDepart.Text)
        SysTemGlobals.ExecuteSQL("Update Paye set IDLivreur =" & tbIDLivreurChangerDestination.EditValue.ToString & ", Livreur = '" & tbIDLivreurChangerDestination.Text.ToString.Trim & "' WHERE IDlivreur = " & tbIDLivreurDepart.Text)
        SysTemGlobals.ExecuteSQL("Update PayeLine set IDLivreur =" & tbIDLivreurChangerDestination.EditValue.ToString & ", Livreur = '" & tbIDLivreurChangerDestination.Text.ToString.Trim & "' WHERE IDlivreur = " & tbIDLivreurDepart.Text)
        SysTemGlobals.ExecuteSQL("Update Prets set IDLivreur =" & tbIDLivreurChangerDestination.EditValue.ToString & ", Livreur = '" & tbIDLivreurChangerDestination.Text.ToString.Trim & "' WHERE IDlivreur = " & tbIDLivreurDepart.Text)
        SysTemGlobals.ExecuteSQL("Update PretsPaiements set IDLivreur =" & tbIDLivreurChangerDestination.EditValue.ToString & ", Livreur = '" & tbIDLivreurChangerDestination.Text.ToString.Trim & "' WHERE IDlivreur = " & tbIDLivreurDepart.Text)
        SysTemGlobals.ExecuteSQL("Update PrixFixe set IDLivreur =" & tbIDLivreurChangerDestination.EditValue.ToString & ", Livreur = '" & tbIDLivreurChangerDestination.Text.ToString.Trim & "' WHERE IDlivreur = " & tbIDLivreurDepart.Text)
        SysTemGlobals.ExecuteSQL("Update RapportCommission set IDLivreur =" & tbIDLivreurChangerDestination.EditValue.ToString & ", Livreur = '" & tbIDLivreurChangerDestination.Text.ToString.Trim & "' WHERE IDlivreur = " & tbIDLivreurDepart.Text)
        SysTemGlobals.ExecuteSQL("Update RapportFacturePaye set IDLivreur =" & tbIDLivreurChangerDestination.EditValue.ToString & ", Livreur = '" & tbIDLivreurChangerDestination.Text.ToString.Trim & "' WHERE IDlivreur = " & tbIDLivreurDepart.Text)
        SysTemGlobals.ExecuteSQL("Update RapportLivraison set IDLivreur =" & tbIDLivreurChangerDestination.EditValue.ToString & ", Livreur = '" & tbIDLivreurChangerDestination.Text.ToString.Trim & "' WHERE IDlivreur = " & tbIDLivreurDepart.Text)
        SysTemGlobals.ExecuteSQL("Update RapportTaxationPayeLivreur set IDLivreur =" & tbIDLivreurChangerDestination.EditValue.ToString & ", Livreur = '" & tbIDLivreurChangerDestination.Text.ToString.Trim & "' WHERE IDlivreur = " & tbIDLivreurDepart.Text)
        SysTemGlobals.ExecuteSQL("Update RapportTaxation set IDLivreur =" & tbIDLivreurChangerDestination.EditValue.ToString & ", Livreur = '" & tbIDLivreurChangerDestination.Text.ToString.Trim & "' WHERE IDlivreur = " & tbIDLivreurDepart.Text)
        SysTemGlobals.ExecuteSQL("DELETE FROM Livreurs WHERE LivreurID =" & tbIDLivreurDepart.Text)

        Cursor = Cursors.Default

        MessageBox.Show("Toutes les tables ont étés modifiées!")


    End Sub



    Private Sub tbNomCompagnie_Leave(sender As Object, e As EventArgs) Handles tbNomCompagnie.Leave
        tbLivreur.Text = tbNomCompagnie.Text & " - (" & tbLivreur.Text & ")"
    End Sub
End Class