Imports System.Data.SqlClient

Public Class CadeauOrganisationData

    Public Function SelectTotalAmountByOrganisation(pdDateFrom As DateTime, pdDateTo As DateTime, pIDorganisation As Integer) As Double

        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "    SUM(Montant) as TotalMontant " _
            & "FROM " _
            & "     [CadeauOrganisation] " _
            & " WHERE (DateFrom  >= '" & pdDateFrom & "' AND DateTo <= '" & pdDateTo & "')" _
            & " AND IDOrganisation ='" & pIDorganisation & "'"

        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Dim drImport As DataRow
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            Dim dTotalAmount As Double = 0
            If reader.HasRows Then
                dt.Load(reader)
                If dt.Rows.Count > 0 Then
                    For Each drImport In dt.Rows
                        dTotalAmount = dTotalAmount + If(IsDBNull(drImport("TotalMontant")), 0, drImport("TotalMontant"))
                    Next
                    Return dTotalAmount
                Else
                    Return 0
                End If

            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Erreur")
        Finally
            connection.Close()
        End Try

    End Function
    Public Function SelectAll() As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDCadeauOrganisation] " _
            & "    ,[IDOrganisation] " _
            & "    ,[Organisation] " _
            & "    ,[DateFrom] " _
            & "    ,[DateTo] " _
            & "    ,[Montant] " _
            & "    ,[IDPeriode] " _
            & "FROM " _
            & "     [CadeauOrganisation] " _
            & ""
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function

    Public Function Select_Record(ByVal clsCadeauOrganisationPara As CadeauOrganisation) As CadeauOrganisation
        Dim clsCadeauOrganisation As New CadeauOrganisation
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDCadeauOrganisation] " _
            & "    ,[IDOrganisation] " _
            & "    ,[Organisation] " _
            & "    ,[DateFrom] " _
            & "    ,[DateTo] " _
            & "    ,[Montant] " _
            & "    ,[IDPeriode] " _
            & "FROM " _
            & "     [CadeauOrganisation] " _
            & "WHERE " _
            & "     [IDCadeauOrganisation] = @IDCadeauOrganisation " _
            & ""
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        selectCommand.Parameters.AddWithValue("@IDCadeauOrganisation", clsCadeauOrganisationPara.IDCadeauOrganisation)
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader(CommandBehavior.SingleRow)
            If reader.Read Then
                With clsCadeauOrganisation
                    .IDCadeauOrganisation = System.Convert.ToInt32(reader("IDCadeauOrganisation"))
                    .IDOrganisation = If(IsDBNull(reader("IDOrganisation")), Nothing, CType(reader("IDOrganisation"), Int32?))
                    .Organisation = If(IsDBNull(reader("Organisation")), Nothing, reader("Organisation").ToString)
                    .DateFrom = If(IsDBNull(reader("DateFrom")), Nothing, CType(reader("DateFrom"), DateTime?))
                    .DateTo = If(IsDBNull(reader("DateTo")), Nothing, CType(reader("DateTo"), DateTime?))
                    .Montant = If(IsDBNull(reader("Montant")), Nothing, CType(reader("Montant"), Decimal?))
                    .IDPeriode = If(IsDBNull(reader("IDPeriode")), Nothing, CType(reader("IDPeriode"), Int32?))
                End With
            Else
                clsCadeauOrganisation = Nothing
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return clsCadeauOrganisation
    End Function

    Public Function Add(ByVal clsCadeauOrganisation As CadeauOrganisation) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim insertStatement As String _
            = "INSERT " _
            & "     [CadeauOrganisation] " _
            & "     ( " _
            & "     [IDOrganisation] " _
            & "    ,[Organisation] " _
            & "    ,[DateFrom] " _
            & "    ,[DateTo] " _
            & "    ,[Montant] " _
            & "    ,[IDPeriode] " _
            & "     ) " _
            & "VALUES " _
            & "     ( " _
            & "     @IDOrganisation " _
            & "    ,@Organisation " _
            & "    ,@DateFrom " _
            & "    ,@DateTo " _
            & "    ,@Montant " _
            & "    ,@IDPeriode " _
            & "     ) " _
            & ""
        Dim insertCommand As New SqlCommand(insertStatement, connection)
        insertCommand.CommandType = CommandType.Text
        insertCommand.Parameters.AddWithValue("@IDOrganisation", IIf(clsCadeauOrganisation.IDOrganisation.HasValue, clsCadeauOrganisation.IDOrganisation, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Organisation", IIf(Not IsNothing(clsCadeauOrganisation.Organisation), clsCadeauOrganisation.Organisation, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@DateFrom", IIf(clsCadeauOrganisation.DateFrom.HasValue, clsCadeauOrganisation.DateFrom, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@DateTo", IIf(clsCadeauOrganisation.DateTo.HasValue, clsCadeauOrganisation.DateTo, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Montant", IIf(clsCadeauOrganisation.Montant.HasValue, clsCadeauOrganisation.Montant, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@IDPeriode", IIf(clsCadeauOrganisation.IDPeriode.HasValue, clsCadeauOrganisation.IDPeriode, DBNull.Value))
        Try
            connection.Open()
            Dim count As Integer = insertCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

    Public Function Update(ByVal clsCadeauOrganisationOld As CadeauOrganisation,
           ByVal clsCadeauOrganisationNew As CadeauOrganisation) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim updateStatement As String _
            = "UPDATE " _
            & "     [CadeauOrganisation] " _
            & "SET " _
            & "     [IDOrganisation] = @NewIDOrganisation " _
            & "    ,[Organisation] = @NewOrganisation " _
            & "    ,[DateFrom] = @NewDateFrom " _
            & "    ,[DateTo] = @NewDateTo " _
            & "    ,[Montant] = @NewMontant " _
            & "    ,[IDPeriode] = @NewIDPeriode " _
            & "WHERE " _
            & "     [IDCadeauOrganisation] = @OldIDCadeauOrganisation " _
            & ""
        Dim updateCommand As New SqlCommand(updateStatement, connection)
        updateCommand.CommandType = CommandType.Text
        updateCommand.Parameters.AddWithValue("@NewIDOrganisation", IIf(clsCadeauOrganisationNew.IDOrganisation.HasValue, clsCadeauOrganisationNew.IDOrganisation, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewOrganisation", IIf(Not IsNothing(clsCadeauOrganisationNew.Organisation), clsCadeauOrganisationNew.Organisation, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewDateFrom", IIf(clsCadeauOrganisationNew.DateFrom.HasValue, clsCadeauOrganisationNew.DateFrom, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewDateTo", IIf(clsCadeauOrganisationNew.DateTo.HasValue, clsCadeauOrganisationNew.DateTo, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewMontant", IIf(clsCadeauOrganisationNew.Montant.HasValue, clsCadeauOrganisationNew.Montant, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewIDPeriode", IIf(clsCadeauOrganisationNew.IDPeriode.HasValue, clsCadeauOrganisationNew.IDPeriode, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@OldIDCadeauOrganisation", clsCadeauOrganisationOld.IDCadeauOrganisation)
        Try
            connection.Open()
            Dim count As Integer = updateCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function
    Public Function Delete(ByVal clsCadeauOrganisation As CadeauOrganisation) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim deleteStatement As String _
            = "DELETE FROM " _
            & "     [CadeauOrganisation] " _
            & "WHERE " _
            & "     [IDCadeauOrganisation] = @OldIDCadeauOrganisation " _
            & ""
        Dim deleteCommand As New SqlCommand(deleteStatement, connection)
        deleteCommand.CommandType = CommandType.Text
        deleteCommand.Parameters.AddWithValue("@OldIDCadeauOrganisation", clsCadeauOrganisation.IDCadeauOrganisation)
        Try
            connection.Open()
            Dim count As Integer = deleteCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

End Class
 
