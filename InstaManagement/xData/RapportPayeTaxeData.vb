Imports System.Data.SqlClient

Public Class RapportPayeTaxeData

    Public Function SelectAll() As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDRapport] " _
            & "    ,[IDPeriode] " _
            & "    ,[IDOrganisation] " _
            & "    ,[IDLivreur] " _
            & "    ,[CheminRapport] " _
            & "    ,[NomFichier] " _
            & "    ,[Email] " _
            & "    ,[Email1] " _
            & "    ,[Email2] " _
            & "    ,[Organisation] " _
            & "    ,[Livreur] " _
            & "    ,[DateFrom] " _
            & "    ,[DateTo] " _
            & "    ,[IsCommissionVendeur] " _
            & "FROM " _
            & "     [RapportPayeTaxe] " _
            & ""
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function

    Public Function Select_Record(ByVal clsRapportPayeTaxePara As RapportPayeTaxe) As RapportPayeTaxe
        Dim clsRapportPayeTaxe As New RapportPayeTaxe
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDRapport] " _
            & "    ,[IDPeriode] " _
            & "    ,[IDOrganisation] " _
            & "    ,[IDLivreur] " _
            & "    ,[CheminRapport] " _
            & "    ,[NomFichier] " _
            & "    ,[Email] " _
            & "    ,[Email1] " _
            & "    ,[Email2] " _
            & "    ,[Organisation] " _
            & "    ,[Livreur] " _
            & "    ,[DateFrom] " _
            & "    ,[DateTo] " _
            & "    ,[IsCommissionVendeur] " _
            & "FROM " _
            & "     [RapportPayeTaxe] " _
            & "WHERE " _
            & "     [IDRapport] = @IDRapport " _
            & ""
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        selectCommand.Parameters.AddWithValue("@IDRapport", clsRapportPayeTaxePara.IDRapport)
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader(CommandBehavior.SingleRow)
            If reader.Read Then
                With clsRapportPayeTaxe
                    .IDRapport = System.Convert.ToInt32(reader("IDRapport"))
                    .IDPeriode = If(IsDBNull(reader("IDPeriode")), Nothing, CType(reader("IDPeriode"), Int32?))
                    .IDOrganisation = If(IsDBNull(reader("IDOrganisation")), Nothing, CType(reader("IDOrganisation"), Int32?))
                    .IDLivreur = If(IsDBNull(reader("IDLivreur")), Nothing, CType(reader("IDLivreur"), Int32?))
                    .CheminRapport = System.Convert.ToString(reader("CheminRapport"))
                    .NomFichier = If(IsDBNull(reader("NomFichier")), Nothing, reader("NomFichier").ToString)
                    .Email = If(IsDBNull(reader("Email")), Nothing, reader("Email").ToString)
                    .Email1 = If(IsDBNull(reader("Email1")), Nothing, reader("Email1").ToString)
                    .Email2 = If(IsDBNull(reader("Email2")), Nothing, reader("Email2").ToString)
                    .Organisation = If(IsDBNull(reader("Organisation")), Nothing, reader("Organisation").ToString)
                    .Livreur = If(IsDBNull(reader("Livreur")), Nothing, reader("Livreur").ToString)
                    .DateFrom = If(IsDBNull(reader("DateFrom")), Nothing, CType(reader("DateFrom"), DateTime?))
                    .DateTo = If(IsDBNull(reader("DateTo")), Nothing, CType(reader("DateTo"), DateTime?))
                    .IsCommissionVendeur = If(IsDBNull(reader("IsCommissionVendeur")), Nothing, CType(reader("IsCommissionVendeur"), Boolean?))
                End With
            Else
                clsRapportPayeTaxe = Nothing
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return clsRapportPayeTaxe
    End Function

    Public Function Add(ByVal clsRapportPayeTaxe As RapportPayeTaxe) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim insertStatement As String _
            = "INSERT " _
            & "     [RapportPayeTaxe] " _
            & "     ( " _
            & "     [IDPeriode] " _
            & "    ,[IDOrganisation] " _
            & "    ,[IDLivreur] " _
            & "    ,[CheminRapport] " _
            & "    ,[NomFichier] " _
            & "    ,[Email] " _
            & "    ,[Email1] " _
            & "    ,[Email2] " _
            & "    ,[Organisation] " _
            & "    ,[Livreur] " _
            & "    ,[DateFrom] " _
            & "    ,[DateTo] " _
            & "    ,[IsCommissionVendeur] " _
            & "     ) " _
            & "VALUES " _
            & "     ( " _
            & "     @IDPeriode " _
            & "    ,@IDOrganisation " _
            & "    ,@IDLivreur " _
            & "    ,@CheminRapport " _
            & "    ,@NomFichier " _
            & "    ,@Email " _
            & "    ,@Email1 " _
            & "    ,@Email2 " _
            & "    ,@Organisation " _
            & "    ,@Livreur " _
            & "    ,@DateFrom " _
            & "    ,@DateTo " _
            & "    ,@IsCommissionVendeur " _
            & "     ) " _
            & ""
        Dim insertCommand As New SqlCommand(insertStatement, connection)
        insertCommand.CommandType = CommandType.Text
        insertCommand.Parameters.AddWithValue("@IDPeriode", IIf(clsRapportPayeTaxe.IDPeriode.HasValue, clsRapportPayeTaxe.IDPeriode, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@IDOrganisation", IIf(clsRapportPayeTaxe.IDOrganisation.HasValue, clsRapportPayeTaxe.IDOrganisation, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@IDLivreur", IIf(clsRapportPayeTaxe.IDLivreur.HasValue, clsRapportPayeTaxe.IDLivreur, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@CheminRapport", clsRapportPayeTaxe.CheminRapport)
        insertCommand.Parameters.AddWithValue("@NomFichier", IIf(Not IsNothing(clsRapportPayeTaxe.NomFichier), clsRapportPayeTaxe.NomFichier, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Email", IIf(Not IsNothing(clsRapportPayeTaxe.Email), clsRapportPayeTaxe.Email, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Email1", IIf(Not IsNothing(clsRapportPayeTaxe.Email1), clsRapportPayeTaxe.Email1, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Email2", IIf(Not IsNothing(clsRapportPayeTaxe.Email2), clsRapportPayeTaxe.Email2, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Organisation", IIf(Not IsNothing(clsRapportPayeTaxe.Organisation), clsRapportPayeTaxe.Organisation, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Livreur", IIf(Not IsNothing(clsRapportPayeTaxe.Livreur), clsRapportPayeTaxe.Livreur, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@DateFrom", IIf(clsRapportPayeTaxe.DateFrom.HasValue, clsRapportPayeTaxe.DateFrom, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@DateTo", IIf(clsRapportPayeTaxe.DateTo.HasValue, clsRapportPayeTaxe.DateTo, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@IsCommissionVendeur", IIf(clsRapportPayeTaxe.IsCommissionVendeur.HasValue, clsRapportPayeTaxe.IsCommissionVendeur, DBNull.Value))
        Try
            connection.Open()
            Dim count As Integer = insertCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

    Public Function Update(ByVal clsRapportPayeTaxeOld As RapportPayeTaxe,
           ByVal clsRapportPayeTaxeNew As RapportPayeTaxe) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim updateStatement As String _
            = "UPDATE " _
            & "     [RapportPayeTaxe] " _
            & "SET " _
            & "     [IDPeriode] = @NewIDPeriode " _
            & "    ,[IDOrganisation] = @NewIDOrganisation " _
            & "    ,[IDLivreur] = @NewIDLivreur " _
            & "    ,[CheminRapport] = @NewCheminRapport " _
            & "    ,[NomFichier] = @NewNomFichier " _
            & "    ,[Email] = @NewEmail " _
            & "    ,[Email1] = @NewEmail1 " _
            & "    ,[Email2] = @NewEmail2 " _
            & "    ,[Organisation] = @NewOrganisation " _
            & "    ,[Livreur] = @NewLivreur " _
            & "    ,[DateFrom] = @NewDateFrom " _
            & "    ,[DateTo] = @NewDateTo " _
            & "    ,[IsCommissionVendeur] = @NewIsCommissionVendeur " _
            & "WHERE " _
            & "     [IDRapport] = @OldIDRapport " _
            & ""
        Dim updateCommand As New SqlCommand(updateStatement, connection)
        updateCommand.CommandType = CommandType.Text
        updateCommand.Parameters.AddWithValue("@NewIDPeriode", IIf(clsRapportPayeTaxeNew.IDPeriode.HasValue, clsRapportPayeTaxeNew.IDPeriode, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewIDOrganisation", IIf(clsRapportPayeTaxeNew.IDOrganisation.HasValue, clsRapportPayeTaxeNew.IDOrganisation, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewIDLivreur", IIf(clsRapportPayeTaxeNew.IDLivreur.HasValue, clsRapportPayeTaxeNew.IDLivreur, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewCheminRapport", clsRapportPayeTaxeNew.CheminRapport)
        updateCommand.Parameters.AddWithValue("@NewNomFichier", IIf(Not IsNothing(clsRapportPayeTaxeNew.NomFichier), clsRapportPayeTaxeNew.NomFichier, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewEmail", IIf(Not IsNothing(clsRapportPayeTaxeNew.Email), clsRapportPayeTaxeNew.Email, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewEmail1", IIf(Not IsNothing(clsRapportPayeTaxeNew.Email1), clsRapportPayeTaxeNew.Email1, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewEmail2", IIf(Not IsNothing(clsRapportPayeTaxeNew.Email2), clsRapportPayeTaxeNew.Email2, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewOrganisation", IIf(Not IsNothing(clsRapportPayeTaxeNew.Organisation), clsRapportPayeTaxeNew.Organisation, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewLivreur", IIf(Not IsNothing(clsRapportPayeTaxeNew.Livreur), clsRapportPayeTaxeNew.Livreur, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewDateFrom", IIf(clsRapportPayeTaxeNew.DateFrom.HasValue, clsRapportPayeTaxeNew.DateFrom, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewDateTo", IIf(clsRapportPayeTaxeNew.DateTo.HasValue, clsRapportPayeTaxeNew.DateTo, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewIsCommissionVendeur", IIf(clsRapportPayeTaxeNew.IsCommissionVendeur.HasValue, clsRapportPayeTaxeNew.IsCommissionVendeur, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@OldIDRapport", clsRapportPayeTaxeOld.IDRapport)
        Try
            connection.Open()
            Dim count As Integer = updateCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

    Public Function Delete(ByVal clsRapportPayeTaxe As RapportPayeTaxe) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim deleteStatement As String _
            = "DELETE FROM " _
            & "     [RapportPayeTaxe] " _
            & "WHERE " _
            & "     [IDRapport] = @OldIDRapport " _
            & ""
        Dim deleteCommand As New SqlCommand(deleteStatement, connection)
        deleteCommand.CommandType = CommandType.Text
        deleteCommand.Parameters.AddWithValue("@OldIDRapport", clsRapportPayeTaxe.IDRapport)
        Try
            connection.Open()
            Dim count As Integer = deleteCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

End Class
 
