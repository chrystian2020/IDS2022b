Imports System.Data.SqlClient

Public Class CommissionData


    Public Function SelectAllLivreurCommission(pdDateFrom As DateTime, pdDateTo As DateTime) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "Select DISTINCT IDLivreur,Livreur " &
            " FROM   dbo.Commission " &
            " WHERE (dbo.Commission.DateFrom  >= '" & pdDateFrom & "' AND dbo.Commission.DateTo <= '" & pdDateTo & "')"




        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function


    Public Function UpdateAmount(ByVal clsCommissionOld As Commission,
           ByVal clsCommissionNew As Commission) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim updateStatement As String _
            = "UPDATE " _
            & "     [Commission] " _
            & "SET " _
            & "    [Amount] = @NewAmount " _
            & "    ,[TPS] = @NewTPS " _
            & "    ,[TVQ] = @NewTVQ " _
            & "WHERE " _
            & "     [IDCommission] = @OldIDCommission " _
            & ""
        Dim updateCommand As New SqlCommand(updateStatement, connection)
        updateCommand.CommandType = CommandType.Text
        updateCommand.Parameters.AddWithValue("@NewAmount", IIf(clsCommissionNew.Amount.HasValue, clsCommissionNew.Amount, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewTPS", IIf(clsCommissionNew.TPS.HasValue, clsCommissionNew.TPS, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewTVQ", IIf(clsCommissionNew.TVQ.HasValue, clsCommissionNew.TVQ, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@OldIDCommission", clsCommissionOld.IDCommission)


        Try
            connection.Open()
            Dim count As Integer = updateCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

    Public Function SelectPayeByDateCommission(piLivreurID As Integer) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDCommission] " _
            & "    ,[CommissionNumber] " _
            & "    ,[CommissionDate] " _
            & "    ,[IDLivreur] " _
            & "    ,[Livreur] " _
            & "    ,[DateFrom] " _
            & "    ,[DateTo] " _
            & "    ,[IDPeriode] " _
            & "    ,[Amount] " _
            & "    ,[TPS] " _
            & "    ,[TVQ] " _
            & "FROM " _
            & "     [Commission] " _
            & " WHERE  IDLivreur = " & piLivreurID & " And IDPeriode = " & go_Globals.IDPeriode

        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function



    Public Function SelectLivreurAvecPayeStringCommission(pdDateFrom As DateTime, pdDateTo As DateTime, piIDLivreur As String) As DataTable
        Dim connection As SqlConnection = DB.GetConnection

        Dim selectStatement As String _
            = "Select DISTINCT dbo.Commission.IDLivreur" _
            & " From dbo.Commission LEFT OUTER Join" _
            & " dbo.livreurs ON dbo.Commission.IDLivreur = dbo.livreurs.LivreurID " _
            & " WHERE  (dbo.Commission.DateFrom  >= '" & pdDateFrom & "' AND dbo.Commission.DateTo <= '" & pdDateTo & "')  AND (dbo.livreurs.IsVendeur = 1) AND dbo.Commission.IDPeriode = " & go_Globals.IDPeriode

        If piIDLivreur.ToString.Trim <> "" Then
            selectStatement = selectStatement & " AND dbo.Commission.IDLivreur IN (" & piIDLivreur & ")"
        End If


        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function

    Public Function SelectAllByDate(pdDateFrom As DateTime, pdDateTo As DateTime) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "Select " _
            & "     [IDCommission] " _
            & "    ,[CommissionNumber] " _
            & "    ,[CommissionDate] " _
            & "    ,[IDLivreur] " _
            & "    ,[Livreur] " _
            & "    ,[DateFrom] " _
            & "    ,[DateTo] " _
            & "    ,[IDPeriode] " _
            & "    ,[Amount] " _
            & "    ,[TPS] " _
            & "    ,[TVQ] " _
            & "    ,[Total] " _
            & "FROM " _
            & "     [Commission] " _
             & " WHERE (DateFrom  >= '" & pdDateFrom & "' AND DateTo <= '" & pdDateTo & "')"




        selectStatement = selectStatement & " ORDER BY IDCommission DESC"

        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function

    Public Function SelectAllByPeriodIDOrganisation(IDPeriod As Integer, pIDOrganisation As Integer) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDCommission] " _
            & "    ,[CommissionNumber] " _
            & "    ,[CommissionDate] " _
            & "    ,[OrganisationID] " _
            & "    ,[Organisation] " _
            & "    ,[DateFrom] " _
            & "    ,[DateTo] " _
            & "    ,[IDPeriode] " _
            & "    ,[Amount] " _
            & "    ,[TPS] " _
            & "    ,[TVQ] " _
            & "FROM " _
            & "     [Commission] " _
            & " WHERE IsRestaurant = 1 AND IDPeriode=" & IDPeriod & "AND  OrganisationID = " & pIDOrganisation
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function
    Public Function SelectAllByPeriodIDLivreur(IDPeriod As Integer, pIDLivreur As Integer) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDCommission] " _
            & "    ,[CommissionNumber] " _
            & "    ,[CommissionDate] " _
            & "    ,[IDLivreur] " _
            & "    ,[Livreur] " _
            & "    ,[DateFrom] " _
            & "    ,[DateTo] " _
            & "    ,[IDPeriode] " _
            & "    ,[Amount] " _
            & "    ,[TPS] " _
            & "    ,[TVQ] " _
            & "FROM " _
            & "     [Commission] " _
            & " WHERE IDPeriode=" & IDPeriod & "AND  IDLivreur = " & pIDLivreur
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function
    Public Function SelectAllByPeriodCommission(IDPeriod As Integer) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
           & "     [IDCommission] " _
            & "    ,[CommissionNumber] " _
            & "    ,[CommissionDate] " _
            & "    ,[IDLivreur] " _
            & "    ,[Livreur] " _
            & "    ,[DateFrom] " _
            & "    ,[DateTo] " _
            & "    ,[IDPeriode] " _
            & "    ,[Amount] " _
            & "    ,[TPS] " _
            & "    ,[TVQ] " _
            & " FROM " _
            & " Commission " _
            & " WHERE IDPeriode=" & IDPeriod

        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function
    Public Function SelectAll() As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDCommission] " _
            & "    ,[CommissionNumber] " _
            & "    ,[CommissionDate] " _
            & "    ,[IDLivreur] " _
            & "    ,[Livreur] " _
            & "    ,[DateFrom] " _
            & "    ,[DateTo] " _
            & "    ,[IDPeriode] " _
            & "    ,[Amount] " _
            & "    ,[TPS] " _
            & "    ,[TVQ] " _
            & "FROM " _
            & "     [Commission] " _
            & ""
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function

    Public Function Select_Record(ByVal clsCommissionPara As Commission) As Commission
        Dim clsCommission As New Commission
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDCommission] " _
            & "    ,[CommissionNumber] " _
            & "    ,[CommissionDate] " _
            & "    ,[IDLivreur] " _
            & "    ,[Livreur] " _
            & "    ,[DateFrom] " _
            & "    ,[DateTo] " _
            & "    ,[IDPeriode] " _
            & "    ,[Amount] " _
            & "    ,[TPS] " _
            & "    ,[TVQ] " _
            & "FROM " _
            & "     [Commission] " _
            & "WHERE " _
            & "     [IDCommission] = @IDCommission " _
            & ""
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        selectCommand.Parameters.AddWithValue("@IDCommission", clsCommissionPara.IDCommission)
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader(CommandBehavior.SingleRow)
            If reader.Read Then
                With clsCommission
                    .IDCommission = System.Convert.ToInt32(reader("IDCommission"))
                    .CommissionNumber = If(IsDBNull(reader("CommissionNumber")), Nothing, reader("CommissionNumber").ToString)
                    .CommissionDate = If(IsDBNull(reader("CommissionDate")), Nothing, CType(reader("CommissionDate"), DateTime?))
                    .IDLivreur = If(IsDBNull(reader("IDLivreur")), Nothing, CType(reader("IDLivreur"), Int32?))
                    .Livreur = If(IsDBNull(reader("Livreur")), Nothing, reader("Livreur").ToString)
                    .DateFrom = If(IsDBNull(reader("DateFrom")), Nothing, CType(reader("DateFrom"), DateTime?))
                    .DateTo = If(IsDBNull(reader("DateTo")), Nothing, CType(reader("DateTo"), DateTime?))
                    .IDPeriode = If(IsDBNull(reader("IDPeriode")), Nothing, CType(reader("IDPeriode"), Int32?))
                    .Amount = If(IsDBNull(reader("Amount")), Nothing, CType(reader("Amount"), Decimal?))
                    .TPS = If(IsDBNull(reader("TPS")), Nothing, CType(reader("TPS"), Decimal?))
                    .TVQ = If(IsDBNull(reader("TVQ")), Nothing, CType(reader("TVQ"), Decimal?))
                End With
            Else
                clsCommission = Nothing
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return clsCommission
    End Function

    Public Function Add(ByVal clsCommission As Commission) As Integer
        Dim connection As SqlConnection = DB.GetConnection
        Dim insertStatement As String _
            = "INSERT " _
            & "     [Commission] " _
            & "     ( " _
            & "     [CommissionNumber] " _
            & "    ,[CommissionDate] " _
            & "    ,[IDLivreur] " _
            & "    ,[Livreur] " _
            & "    ,[DateFrom] " _
            & "    ,[DateTo] " _
            & "    ,[IDPeriode] " _
            & "    ,[Amount] " _
            & "    ,[TPS] " _
            & "    ,[TVQ] " _
            & "     ) " _
            & "VALUES " _
            & "     ( " _
            & "     @CommissionNumber " _
            & "    ,@CommissionDate " _
            & "    ,@IDLivreur " _
            & "    ,@Livreur " _
            & "    ,@DateFrom " _
            & "    ,@DateTo " _
            & "    ,@IDPeriode " _
            & "    ,@Amount " _
            & "    ,@TPS " _
            & "    ,@TVQ " _
            & "     ) ;SELECT SCOPE_IDENTITY() " _
            & ""
        Dim insertCommand As New SqlCommand(insertStatement, connection)
        insertCommand.CommandType = CommandType.Text
        insertCommand.Parameters.AddWithValue("@CommissionNumber", IIf(Not IsNothing(clsCommission.CommissionNumber), clsCommission.CommissionNumber, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@CommissionDate", IIf(clsCommission.CommissionDate.HasValue, clsCommission.CommissionDate, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@IDLivreur", IIf(clsCommission.IDLivreur.HasValue, clsCommission.IDLivreur, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Livreur", IIf(Not IsNothing(clsCommission.Livreur), clsCommission.Livreur, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@DateFrom", IIf(clsCommission.DateFrom.HasValue, clsCommission.DateFrom, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@DateTo", IIf(clsCommission.DateTo.HasValue, clsCommission.DateTo, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@IDPeriode", IIf(clsCommission.IDPeriode.HasValue, clsCommission.IDPeriode, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Amount", IIf(clsCommission.Amount.HasValue, clsCommission.Amount, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@TPS", IIf(clsCommission.TPS.HasValue, clsCommission.TPS, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@TVQ", IIf(clsCommission.TVQ.HasValue, clsCommission.TVQ, DBNull.Value))
        Try
            connection.Open()
            Dim count As Integer = insertCommand.ExecuteScalar()
            If count > 0 Then
                Return count
            Else
                Return count
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

    Public Function Update(ByVal clsCommissionOld As Commission,
           ByVal clsCommissionNew As Commission) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim updateStatement As String _
            = "UPDATE " _
            & "     [Commission] " _
            & "SET " _
            & "     [CommissionNumber] = @NewCommissionNumber " _
            & "    ,[CommissionDate] = @NewCommissionDate " _
            & "    ,[IDLivreur] = @NewIDLivreur " _
            & "    ,[Livreur] = @NewLivreur " _
            & "    ,[DateFrom] = @NewDateFrom " _
            & "    ,[DateTo] = @NewDateTo " _
            & "    ,[IDPeriode] = @NewIDPeriode " _
            & "    ,[Amount] = @NewAmount " _
            & "    ,[TPS] = @NewTPS " _
            & "    ,[TVQ] = @NewTVQ " _
            & "WHERE " _
            & "     [IDCommission] = @OldIDCommission " _
            & ""
        Dim updateCommand As New SqlCommand(updateStatement, connection)
        updateCommand.CommandType = CommandType.Text
        updateCommand.Parameters.AddWithValue("@NewCommissionNumber", IIf(Not IsNothing(clsCommissionNew.CommissionNumber), clsCommissionNew.CommissionNumber, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewCommissionDate", IIf(clsCommissionNew.CommissionDate.HasValue, clsCommissionNew.CommissionDate, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewIDLivreur", IIf(clsCommissionNew.IDLivreur.HasValue, clsCommissionNew.IDLivreur, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewLivreur", IIf(Not IsNothing(clsCommissionNew.Livreur), clsCommissionNew.Livreur, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewDateFrom", IIf(clsCommissionNew.DateFrom.HasValue, clsCommissionNew.DateFrom, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewDateTo", IIf(clsCommissionNew.DateTo.HasValue, clsCommissionNew.DateTo, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewIDPeriode", IIf(clsCommissionNew.IDPeriode.HasValue, clsCommissionNew.IDPeriode, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewAmount", IIf(clsCommissionNew.Amount.HasValue, clsCommissionNew.Amount, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewTPS", IIf(clsCommissionNew.TPS.HasValue, clsCommissionNew.TPS, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewTVQ", IIf(clsCommissionNew.TVQ.HasValue, clsCommissionNew.TVQ, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@OldIDCommission", clsCommissionOld.IDCommission)
        Try
            connection.Open()
            Dim count As Integer = updateCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

    Public Function Delete(ByVal clsCommission As Commission) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim deleteStatement As String _
            = "DELETE FROM " _
            & "     [Commission] " _
            & "WHERE " _
            & "     [IDCommission] = @OldIDCommission " _
            & ""
        Dim deleteCommand As New SqlCommand(deleteStatement, connection)
        deleteCommand.CommandType = CommandType.Text
        deleteCommand.Parameters.AddWithValue("@OldIDCommission", clsCommission.IDCommission)
        Try
            connection.Open()
            Dim count As Integer = deleteCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

End Class

