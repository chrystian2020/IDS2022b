Imports System.Data.SqlClient

Public Class PaieLivreurData

    Public Function SelectAll() As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [PaieLivreur].[IDPaie] " _
            & "    ,[PaieLivreur].[Livreur] " _
            & "    ,[PaieLivreur].[Organisation] " _
            & "    ,[PaieLivreur].[MontantPaieNormal] " _
            & "    ,[PaieLivreur].[MontantReajuste] " _
            & "    ,[PaieLivreur].[Paie] " _
            & "    ,[PaieLivreur].[PayDate] " _
            & "    ,[PaieLivreur].[IDPeriode] " _
            & "FROM " _
            & "     [PaieLivreur] " _
            & ""
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Erreur")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function

    Public Function Select_Record(ByVal clsPaieLivreurPara As PaieLivreur) As PaieLivreur
        Dim clsPaieLivreur As New PaieLivreur
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDPaie] " _
            & "    ,[Livreur] " _
            & "    ,[Organisation] " _
            & "    ,[MontantPaieNormal] " _
            & "    ,[MontantReajuste] " _
            & "    ,[Paie] " _
            & "    ,[PayDate] " _
            & "    ,[IDPeriode] " _
            & "FROM " _
            & "     [PaieLivreur] " _
            & "WHERE " _
            & "     [IDPaie] = @IDPaie " _
            & ""
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        selectCommand.Parameters.AddWithValue("@IDPaie", clsPaieLivreurPara.IDPaie)
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader(CommandBehavior.SingleRow)
            If reader.Read Then
                With clsPaieLivreur
                    .IDPaie = System.Convert.ToInt32(reader("IDPaie"))
                    .Livreur = If(IsDBNull(reader("Livreur")), Nothing, reader("Livreur").ToString)
                    .Organisation = If(IsDBNull(reader("Organisation")), Nothing, reader("Organisation").ToString)
                    .MontantPaieNormal = If(IsDBNull(reader("MontantPaieNormal")), Nothing, CType(reader("MontantPaieNormal"), Decimal?))
                    .MontantReajuste = If(IsDBNull(reader("MontantReajuste")), Nothing, CType(reader("MontantReajuste"), Decimal?))
                    .Paie = If(IsDBNull(reader("Paie")), Nothing, CType(reader("Paie"), Decimal?))
                    .PayDate = If(IsDBNull(reader("PayDate")), Nothing, CType(reader("PayDate"), Date?))
                    .IDPeriode = System.Convert.ToInt32(reader("IDPeriode"))
                End With
            Else
                clsPaieLivreur = Nothing
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Erreur")
        Finally
            connection.Close()
        End Try
        Return clsPaieLivreur
    End Function

    Public Function Add(ByVal clsPaieLivreur As PaieLivreur) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim insertStatement As String _
            = "INSERT " _
            & "     [PaieLivreur] " _
            & "     ( " _
            & "     [Livreur] " _
            & "    ,[Organisation] " _
            & "    ,[MontantPaieNormal] " _
            & "    ,[MontantReajuste] " _
            & "    ,[Paie] " _
            & "    ,[PayDate] " _
            & "    ,[IDPeriode] " _
            & "     ) " _
            & "VALUES " _
            & "     ( " _
            & "     @Livreur " _
            & "    ,@Organisation " _
            & "    ,@MontantPaieNormal " _
            & "    ,@MontantReajuste " _
            & "    ,@Paie " _
            & "    ,@PayDate " _
            & "    ,@IDPeriode " _
            & "     ) " _
            & ""
        Dim insertCommand As New SqlCommand(insertStatement, connection)
        insertCommand.CommandType = CommandType.Text
        insertCommand.Parameters.AddWithValue("@Livreur", IIf(Not IsNothing(clsPaieLivreur.Livreur), clsPaieLivreur.Livreur, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Organisation", IIf(Not IsNothing(clsPaieLivreur.Organisation), clsPaieLivreur.Organisation, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@MontantPaieNormal", IIf(clsPaieLivreur.MontantPaieNormal.HasValue, clsPaieLivreur.MontantPaieNormal, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@MontantReajuste", IIf(clsPaieLivreur.MontantReajuste.HasValue, clsPaieLivreur.MontantReajuste, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Paie", IIf(clsPaieLivreur.Paie.HasValue, clsPaieLivreur.Paie, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@PayDate", IIf(clsPaieLivreur.PayDate.HasValue, clsPaieLivreur.PayDate, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@IDPeriode", IIf(clsPaieLivreur.IDPeriode.HasValue, clsPaieLivreur.IDPeriode, DBNull.Value))
        Try
            connection.Open()
            Dim count As Integer = insertCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Erreur")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

    Public Function Update(ByVal clsPaieLivreurOld As PaieLivreur,
           ByVal clsPaieLivreurNew As PaieLivreur) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim updateStatement As String _
            = "UPDATE " _
            & "     [PaieLivreur] " _
            & "SET " _
            & "     [Livreur] = @NewLivreur " _
            & "    ,[Organisation] = @NewOrganisation " _
            & "    ,[MontantPaieNormal] = @NewMontantPaieNormal " _
            & "    ,[MontantReajuste] = @NewMontantReajuste " _
            & "    ,[Paie] = @NewPaie " _
            & "    ,[PayDate] = @PayDate " _
            & "    ,[IDPeriode] = @IDPeriode " _
            & "WHERE " _
            & "     [IDPaie] = @OldIDPaie " _
            & ""
        Dim updateCommand As New SqlCommand(updateStatement, connection)
        updateCommand.CommandType = CommandType.Text
        updateCommand.Parameters.AddWithValue("@NewLivreur", IIf(Not IsNothing(clsPaieLivreurNew.Livreur), clsPaieLivreurNew.Livreur, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewOrganisation", IIf(Not IsNothing(clsPaieLivreurNew.Organisation), clsPaieLivreurNew.Organisation, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewMontantPaieNormal", IIf(clsPaieLivreurNew.MontantPaieNormal.HasValue, clsPaieLivreurNew.MontantPaieNormal, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewMontantReajuste", IIf(clsPaieLivreurNew.MontantReajuste.HasValue, clsPaieLivreurNew.MontantReajuste, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewPaie", IIf(clsPaieLivreurNew.Paie.HasValue, clsPaieLivreurNew.Paie, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewPayDate", IIf(clsPaieLivreurNew.PayDate.HasValue, clsPaieLivreurNew.PayDate, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@IDPeriode", IIf(clsPaieLivreurNew.IDPeriode.HasValue, clsPaieLivreurNew.IDPeriode, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@OldIDPaie", clsPaieLivreurOld.IDPaie)
        Try
            connection.Open()
            Dim count As Integer = updateCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Erreur")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

    Public Function Delete(ByVal clsPaieLivreur As PaieLivreur) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim deleteStatement As String _
            = "DELETE FROM " _
            & "     [PaieLivreur] " _
            & "WHERE " _
            & "     [IDPaie] = @OldIDPaie " _
            & ""
        Dim deleteCommand As New SqlCommand(deleteStatement, connection)
        deleteCommand.CommandType = CommandType.Text
        deleteCommand.Parameters.AddWithValue("@OldIDPaie", clsPaieLivreur.IDPaie)
        Try
            connection.Open()
            Dim count As Integer = deleteCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Erreur")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

End Class
 
