Imports System.Data.SqlClient

Public Class ResourcesData

    Public Function SelectAll() As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [UniqueID] " _
            & "    ,[ResourceID] " _
            & "    ,[ResourceName] " _
            & "    ,[Color] " _
            & "    ,[CustomField1] " _
            & "FROM " _
            & "     [Resources] " _
            & ""
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function

    Public Function Select_Record(ByVal clsResourcesPara As Resources) As Resources
        Dim clsResources As New Resources
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [UniqueID] " _
            & "    ,[ResourceID] " _
            & "    ,[ResourceName] " _
            & "    ,[Color] " _
            & "    ,[CustomField1] " _
            & "FROM " _
            & "     [Resources] " _
            & "WHERE " _
            & "     [UniqueID] = @UniqueID " _
            & ""
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        selectCommand.Parameters.AddWithValue("@UniqueID", clsResourcesPara.UniqueID)
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader(CommandBehavior.SingleRow)
            If reader.Read Then
                With clsResources
                    .UniqueID = System.Convert.ToInt32(reader("UniqueID"))
                    .ResourceID = System.Convert.ToInt32(reader("ResourceID"))
                    .ResourceName = System.Convert.ToString(reader("ResourceName"))
                    .Color = If(IsDBNull(reader("Color")), Nothing, CType(reader("Color"), Int32?))
                    .CustomField1 = If(IsDBNull(reader("CustomField1")), Nothing, reader("CustomField1").ToString)
                End With
            Else
                clsResources = Nothing
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return clsResources
    End Function

    Public Function Add(ByVal clsResources As Resources) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim insertStatement As String _
            = "INSERT " _
            & "     [Resources] " _
            & "     ( " _
            & "     [ResourceID] " _
            & "    ,[ResourceName] " _
            & "    ,[Color] " _
            & "    ,[CustomField1] " _
            & "     ) " _
            & "VALUES " _
            & "     ( " _
            & "     @ResourceID " _
            & "    ,@ResourceName " _
            & "    ,@Color " _
            & "    ,@CustomField1 " _
            & "     ) " _
            & ""
        Dim insertCommand As New SqlCommand(insertStatement, connection)
        insertCommand.CommandType = CommandType.Text
        insertCommand.Parameters.AddWithValue("@ResourceID", clsResources.ResourceID)
        insertCommand.Parameters.AddWithValue("@ResourceName", clsResources.ResourceName)
        insertCommand.Parameters.AddWithValue("@Color", IIf(clsResources.Color.HasValue, clsResources.Color, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@CustomField1", IIf(Not IsNothing(clsResources.CustomField1), clsResources.CustomField1, DBNull.Value))
        Try
            connection.Open()
            Dim count As Integer = insertCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

    Public Function Update(ByVal clsResourcesOld As Resources,
           ByVal clsResourcesNew As Resources) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim updateStatement As String _
            = "UPDATE " _
            & "     [Resources] " _
            & "SET " _
            & "     [ResourceID] = @NewResourceID " _
            & "    ,[ResourceName] = @NewResourceName " _
            & "    ,[Color] = @NewColor " _
            & "    ,[CustomField1] = @NewCustomField1 " _
            & "WHERE " _
            & "     [UniqueID] = @OldUniqueID " _
            & ""
        Dim updateCommand As New SqlCommand(updateStatement, connection)
        updateCommand.CommandType = CommandType.Text
        updateCommand.Parameters.AddWithValue("@NewResourceID", clsResourcesNew.ResourceID)
        updateCommand.Parameters.AddWithValue("@NewResourceName", clsResourcesNew.ResourceName)
        updateCommand.Parameters.AddWithValue("@NewColor", IIf(clsResourcesNew.Color.HasValue, clsResourcesNew.Color, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewCustomField1", IIf(Not IsNothing(clsResourcesNew.CustomField1), clsResourcesNew.CustomField1, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@OldUniqueID", clsResourcesOld.UniqueID)

        Try
            connection.Open()
            Dim count As Integer = updateCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

    Public Function Delete(ByVal clsResources As Resources) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim deleteStatement As String _
            = "DELETE FROM " _
            & "     [Resources] " _
            & "WHERE " _
            & "     [UniqueID] = @OldUniqueID " _
            & ""
        Dim deleteCommand As New SqlCommand(deleteStatement, connection)
        deleteCommand.CommandType = CommandType.Text
        deleteCommand.Parameters.AddWithValue("@OldUniqueID", clsResources.UniqueID)
        Try
            connection.Open()
            Dim count As Integer = deleteCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

End Class
 
