Imports System.Data.SqlClient

Public Class LivreursData


    Public Function SelectByIDVendeur(piIDVendeur As Integer) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "Select " _
            & "     [LivreurID] " _
            & "    ,[LivreurCache] " _
            & "    ,[Livreur] " _
            & "    ,[Telephone] " _
            & "    ,[Adresse] " _
            & "    ,[CalculerTaxes] " _
            & "    ,[Email] " _
            & "    ,[Email1] " _
            & "    ,[Email2] " _
            & "    ,[IsVendeur] " _
            & "    ,[Actif] " _
            & "    ,[IDVendeur] " _
            & "    ,[CommissionVendeur] " _
            & "    ,[NomCompagnie] " _
            & "    ,[NoTPS] " _
            & "    ,[NoTVQ] " _
            & " FROM " _
            & "     [livreurs] " _
            & "WHERE IDVendeur= " & piIDVendeur

        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Erreur")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function
    Public Function SelectAllByVendeur(piLivreurID As Integer) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [LivreurID] " _
            & "    ,[LivreurCache] " _
            & "    ,[Livreur] " _
            & "    ,[Telephone] " _
            & "    ,[Adresse] " _
            & "    ,[CalculerTaxes] " _
            & "    ,[Email] " _
            & "    ,[Email1] " _
            & "    ,[Email2] " _
            & "    ,[IsVendeur] " _
            & "    ,[Actif] " _
            & "    ,[IDVendeur] " _
            & "    ,[CommissionVendeur] " _
            & "    ,[NomCompagnie] " _
            & "    ,[NoTPS] " _
            & "    ,[NoTVQ] " _
            & "FROM " _
            & "     [Livreurs] " _
            & " WHERE IDVendeur=" & piLivreurID

        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Erreur")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function
    Public Function SelectAllByVendeurID(piIDVendeur As Integer) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [LivreurID] " _
            & "    ,[LivreurCache] " _
            & "    ,[Livreur] " _
            & "    ,[Telephone] " _
            & "    ,[Adresse] " _
            & "    ,[CalculerTaxes] " _
            & "    ,[Email] " _
            & "    ,[Email1] " _
            & "    ,[Email2] " _
            & "    ,[IsVendeur] " _
            & "    ,[Actif] " _
            & "    ,[IDVendeur] " _
            & "    ,[CommissionVendeur] " _
            & "    ,[NomCompagnie] " _
            & "    ,[NoTPS] " _
            & "    ,[NoTVQ] " _
            & "FROM " _
            & "     [Livreurs] " _
            & " WHERE IDVendeur=" & piIDVendeur

        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Erreur")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function
    Public Function SelectAllByIDVendeur(piLivreurID As Integer) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [LivreurID] " _
            & "    ,[LivreurCache] " _
            & "    ,[Livreur] " _
            & "    ,[Telephone] " _
            & "    ,[Adresse] " _
            & "    ,[CalculerTaxes] " _
            & "    ,[Email] " _
            & "    ,[Email1] " _
            & "    ,[Email2] " _
            & "    ,[IsVendeur] " _
            & "    ,[Actif] " _
            & "    ,[IDVendeur] " _
            & "    ,[CommissionVendeur] " _
            & "    ,[NomCompagnie] " _
            & "    ,[NoTPS] " _
            & "    ,[NoTVQ] " _
            & "FROM livreurs" _
            & " WHERE IDVendeur=" & piLivreurID

        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Erreur")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function
    Public Function SelectLivreurByID(piLivreurID As Integer) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [LivreurID] " _
            & "    ,[LivreurCache] " _
            & "    ,[Livreur] " _
            & "    ,[Telephone] " _
            & "    ,[Adresse] " _
            & "    ,[CalculerTaxes] " _
            & "    ,[Email] " _
            & "    ,[Email1] " _
            & "    ,[Email2] " _
            & "    ,[IsVendeur] " _
            & "    ,[Actif] " _
            & "    ,[IDVendeur] " _
            & "    ,[CommissionVendeur] " _
            & "    ,[NomCompagnie] " _
            & "    ,[NoTPS] " _
            & "    ,[NoTVQ] " _
            & "FROM livreurs" _
            & " WHERE LivreurID=" & piLivreurID

        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Erreur")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function


    Public Function SelectAllVendeurs() As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [LivreurID] " _
            & "    ,[LivreurCache] " _
            & "    ,[Livreur] " _
            & "    ,[Telephone] " _
            & "    ,[Adresse] " _
            & "    ,[CalculerTaxes] " _
            & "    ,[Email] " _
            & "    ,[Email1] " _
            & "    ,[Email2] " _
            & "    ,[IsVendeur] " _
            & "    ,[Actif] " _
            & "    ,[IDVendeur] " _
            & "    ,[CommissionVendeur] " _
            & "    ,[NomCompagnie] " _
            & "    ,[NoTPS] " _
            & "    ,[NoTVQ] " _
            & " FROM " _
            & "     [Livreurs] " _
            & " WHERE IsVendeur = 1"


        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Erreur")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function

    Public Function SelectAllTaxedLivreurs() As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [LivreurID] " _
            & "    ,[LivreurCache] " _
            & "    ,[Livreur] " _
            & "    ,[Telephone] " _
            & "    ,[Adresse] " _
            & "    ,[CalculerTaxes] " _
            & "    ,[Email] " _
            & "    ,[Email1] " _
            & "    ,[Email2] " _
            & "    ,[IsVendeur] " _
            & "    ,[Actif] " _
            & "    ,[IDVendeur] " _
            & "    ,[CommissionVendeur] " _
            & "    ,[NomCompagnie] " _
            & "    ,[NoTPS] " _
            & "    ,[NoTVQ] " _
            & " FROM " _
            & "     [Livreurs]" _
            & " WHERE CalculerTaxes = 1"
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Erreur")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function

    Public Function SelectAllLivreurs() As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [LivreurID] " _
            & "    ,[LivreurCache] " _
            & "    ,[Livreur] " _
            & "    ,[Telephone] " _
            & "    ,[Adresse] " _
            & "    ,[CalculerTaxes] " _
            & "    ,[Email] " _
            & "    ,[Email1] " _
            & "    ,[Email2] " _
            & "    ,[IsVendeur] " _
            & "    ,[Actif] " _
            & "    ,[IDVendeur] " _
            & "    ,[CommissionVendeur] " _
            & " FROM " _
            & "     [Livreurs] ORDER BY Livreur ASC" _
            & ""
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Erreur")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function

    Public Function SelectAll() As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [LivreurID] " _
            & "    ,[LivreurCache] " _
            & "    ,[Livreur] " _
            & "    ,[Telephone] " _
            & "    ,[Adresse] " _
            & "    ,[CalculerTaxes] " _
            & "    ,[Email] " _
            & "    ,[Email1] " _
            & "    ,[Email2] " _
            & "    ,[IsVendeur] " _
            & "    ,[Actif] " _
            & "    ,[IDVendeur] " _
            & "    ,[CommissionVendeur] " _
            & "    ,[NomCompagnie] " _
            & "    ,[NoTPS] " _
            & "    ,[NoTVQ] " _
            & " FROM " _
            & "     [Livreurs] WHERE IsVendeur = 0 and Actif = 1  ORDER BY Livreur ASC" _
            & ""
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Erreur")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function
    Public Function SelectByName(psName As String) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [LivreurID] " _
            & "    ,[LivreurCache] " _
            & "    ,[Livreur] " _
            & "    ,[Telephone] " _
            & "    ,[Adresse] " _
            & "    ,[CalculerTaxes] " _
            & "    ,[Email] " _
            & "    ,[Email1] " _
            & "    ,[Email2] " _
            & "    ,[IsVendeur] " _
            & "    ,[Actif] " _
            & "    ,[IDVendeur] " _
            & "    ,[CommissionVendeur] " _
            & "    ,[NomCompagnie] " _
            & "    ,[NoTPS] " _
            & "    ,[NoTVQ] " _
            & "FROM " _
            & "     [Livreurs] " _
            & "Where LivreurCache ='" & psName & "'"

        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Erreur")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function

    Public Function SelectAllByID(piLivreurID As Integer) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [LivreurID] " _
            & "    ,[LivreurCache] " _
            & "    ,[Livreur] " _
            & "    ,[Telephone] " _
            & "    ,[Adresse] " _
            & "    ,[CalculerTaxes] " _
            & "    ,[Email] " _
            & "    ,[Email1] " _
            & "    ,[Email2] " _
            & "    ,[IsVendeur] " _
            & "    ,[Actif] " _
            & "    ,[IDVendeur] " _
            & "    ,[CommissionVendeur] " _
            & "    ,[NomCompagnie] " _
            & "    ,[NoTPS] " _
            & "    ,[NoTVQ] " _
            & " FROM " _
            & "     [Livreurs] " _
            & " Where LivreurID = " & piLivreurID

        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Erreur")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function


    Public Function SelectAllByOrganisation(psName As String) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [LivreurID] " _
            & "    ,[LivreurCache] " _
            & "    ,[Livreur] " _
            & "    ,[Telephone] " _
            & "    ,[Adresse] " _
            & "    ,[CalculerTaxes] " _
            & "    ,[Email] " _
            & "    ,[Email1] " _
            & "    ,[Email2] " _
            & "    ,[IsVendeur] " _
            & "    ,[Actif] " _
            & "    ,[IDVendeur] " _
            & "    ,[CommissionVendeur] " _
            & "    ,[NomCompagnie] " _
            & "    ,[NoTPS] " _
            & "    ,[NoTVQ] " _
            & "FROM " _
            & "     [Livreurs] " _
            & "Where Livreur ='" & psName & "'"

        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Erreur")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function
    Public Function Select_Record(ByVal clsLivreursPara As livreurs) As livreurs
        Dim clsLivreurs As New livreurs
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [LivreurID] " _
            & "    ,[LivreurCache] " _
            & "    ,[Livreur] " _
            & "    ,[Telephone] " _
            & "    ,[Adresse] " _
            & "    ,[CalculerTaxes] " _
            & "    ,[Email] " _
            & "    ,[Email1] " _
            & "    ,[Email2] " _
            & "    ,[IsVendeur] " _
            & "    ,[Actif] " _
            & "    ,[IDVendeur] " _
            & "    ,[CommissionVendeur] " _
            & "    ,[NomCompagnie] " _
            & "    ,[NoTPS] " _
            & "    ,[NoTVQ] " _
            & "FROM " _
            & "     [Livreurs] " _
            & "WHERE " _
            & "     [LivreurID] = @LivreurID " _
            & ""
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        selectCommand.Parameters.AddWithValue("@LivreurID", clsLivreursPara.LivreurID)
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader(CommandBehavior.SingleRow)
            If reader.Read Then
                With clsLivreurs
                    .LivreurID = System.Convert.ToInt32(reader("LivreurID"))
                    .Livreur = If(IsDBNull(reader("Livreur")), Nothing, reader("Livreur").ToString)
                    .LivreurCache = If(IsDBNull(reader("LivreurCache")), Nothing, reader("LivreurCache").ToString)
                    .Telephone = If(IsDBNull(reader("Telephone")), Nothing, reader("Telephone").ToString)
                    .Adresse = If(IsDBNull(reader("Adresse")), Nothing, reader("Adresse").ToString)

                    If Not IsDBNull(reader("CalculerTaxes")) Then

                        If reader("CalculerTaxes") = True Then
                            .CalculerTaxes = 1
                        Else
                            .CalculerTaxes = 0
                        End If
                    Else
                        .CalculerTaxes = 0
                    End If


                    .Email = If(IsDBNull(reader("Email")), Nothing, reader("Email").ToString)
                    .Email1 = If(IsDBNull(reader("Email1")), Nothing, reader("Email1").ToString)
                    .Email2 = If(IsDBNull(reader("Email2")), Nothing, reader("Email2").ToString)

                    If Not IsDBNull(reader("IsVendeur")) Then

                        If reader("IsVendeur") = True Then
                            .IsVendeur = 1
                        Else
                            .IsVendeur = 0
                        End If
                    Else
                        .IsVendeur = 0
                    End If

                    If Not IsDBNull(reader("Actif")) Then

                        If reader("Actif") = True Then
                            .Actif = 1
                        Else
                            .Actif = 0
                        End If
                    Else
                        .Actif = 0
                    End If

                    .IDVendeur = If(IsDBNull(reader("IDVendeur")), Nothing, reader("IDVendeur"))
                    .CommissionVendeur = If(IsDBNull(reader("CommissionVendeur")), Nothing, CType(CType(reader("CommissionVendeur"), Double?), Decimal?))

                    .NomCompagnie = If(IsDBNull(reader("NomCompagnie")), Nothing, reader("NomCompagnie").ToString)
                    .NoTPS = If(IsDBNull(reader("NoTPS")), Nothing, reader("NoTPS").ToString)
                    .NoTVQ = If(IsDBNull(reader("NoTVQ")), Nothing, reader("NoTVQ").ToString)

                End With
            Else
                clsLivreurs = Nothing
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Erreur")
        Finally
            connection.Close()
        End Try
        Return clsLivreurs
    End Function

    Public Function Add(ByVal clslivreurs As livreurs) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim insertStatement As String _
            = "INSERT " _
            & "     [dbo].[livreurs] " _
            & "     ( " _
            & "     [LivreurCache] " _
            & "    ,[Livreur] " _
            & "    ,[Telephone] " _
            & "    ,[Adresse] " _
            & "    ,[CalculerTaxes] " _
            & "    ,[Email] " _
            & "    ,[Email1] " _
            & "    ,[Email2] " _
            & "    ,[IsVendeur] " _
            & "    ,[Actif] " _
            & "    ,[IDVendeur] " _
            & "    ,[CommissionVendeur] " _
            & "    ,[NomCompagnie] " _
            & "    ,[NoTPS] " _
            & "    ,[NoTVQ] " _
            & "     ) " _
            & "VALUES " _
            & "     ( " _
            & "     @LivreurCache " _
            & "    ,@Livreur " _
            & "    ,@Telephone " _
            & "    ,@Adresse " _
            & "    ,@CalculerTaxes " _
            & "    ,@Email " _
            & "    ,@Email1 " _
            & "    ,@Email2 " _
            & "    ,@IsVendeur " _
            & "    ,@Actif " _
            & "    ,@IDVendeur " _
            & "    ,@CommissionVendeur " _
            & "    ,@NomCompagnie " _
            & "    ,@NoTPS " _
            & "    ,@NoTVQ " _
            & "     ) " _
            & ""
        Dim insertCommand As New SqlCommand(insertStatement, connection)
        insertCommand.CommandType = CommandType.Text
        insertCommand.Parameters.AddWithValue("@LivreurCache", IIf(Not IsNothing(clslivreurs.LivreurCache), clslivreurs.LivreurCache, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Livreur", IIf(Not IsNothing(clslivreurs.Livreur), clslivreurs.Livreur, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Telephone", IIf(Not IsNothing(clslivreurs.Telephone), clslivreurs.Telephone, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Adresse", IIf(Not IsNothing(clslivreurs.Adresse), clslivreurs.Adresse, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@CalculerTaxes", IIf(clslivreurs.CalculerTaxes.HasValue, clslivreurs.CalculerTaxes, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Email", IIf(Not IsNothing(clslivreurs.Email), clslivreurs.Email, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Email1", IIf(Not IsNothing(clslivreurs.Email1), clslivreurs.Email1, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Email2", IIf(Not IsNothing(clslivreurs.Email2), clslivreurs.Email2, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@IsVendeur", IIf(clslivreurs.IsVendeur.HasValue, clslivreurs.IsVendeur, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Actif", IIf(clslivreurs.Actif.HasValue, clslivreurs.Actif, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@IDVendeur", IIf(clslivreurs.IDVendeur.HasValue, clslivreurs.IDVendeur, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@CommissionVendeur", IIf(clslivreurs.CommissionVendeur.HasValue, clslivreurs.CommissionVendeur, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@NomCompagnie", IIf(Not IsNothing(clslivreurs.NomCompagnie), clslivreurs.NomCompagnie, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@NoTPS", IIf(Not IsNothing(clslivreurs.NoTPS), clslivreurs.NoTPS, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@NoTVQ", IIf(Not IsNothing(clslivreurs.NoTVQ), clslivreurs.NoTVQ, DBNull.Value))

        Try
            connection.Open()
            Dim count As Integer = insertCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

    Public Function Update(ByVal clslivreursOld As livreurs,
           ByVal clslivreursNew As livreurs) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim updateStatement As String _
            = "UPDATE " _
            & "     [livreurs] " _
            & "SET " _
            & "     [LivreurCache] = @NewLivreurCache " _
            & "    ,[Livreur] = @NewLivreur " _
            & "    ,[Telephone] = @NewTelephone " _
            & "    ,[Adresse] = @NewAdresse " _
            & "    ,[CalculerTaxes] = @NewCalculerTaxes " _
            & "    ,[Email] = @NewEmail " _
            & "    ,[Email1] = @NewEmail1 " _
            & "    ,[Email2] = @NewEmail2 " _
            & "    ,[IsVendeur] = @NewIsVendeur " _
            & "    ,[Actif] = @NewActif " _
            & "    ,[IDVendeur] = @NewIDVendeur " _
            & "    ,[CommissionVendeur] = @NewCommissionVendeur " _
            & "    ,[NomCompagnie] = @NewNomCompagnie " _
            & "    ,[NoTPS] = @NewNoTPS " _
            & "    ,[NoTVQ] = @NewNoTVQ " _
            & "WHERE " _
            & "     [LivreurID] = @OldLivreurID " _
            & ""
        Dim updateCommand As New SqlCommand(updateStatement, connection)
        updateCommand.CommandType = CommandType.Text
        updateCommand.Parameters.AddWithValue("@NewLivreurCache", IIf(Not IsNothing(clslivreursNew.LivreurCache), clslivreursNew.LivreurCache, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewLivreur", IIf(Not IsNothing(clslivreursNew.Livreur), clslivreursNew.Livreur, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewTelephone", IIf(Not IsNothing(clslivreursNew.Telephone), clslivreursNew.Telephone, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewAdresse", IIf(Not IsNothing(clslivreursNew.Adresse), clslivreursNew.Adresse, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewCalculerTaxes", IIf(clslivreursNew.CalculerTaxes.HasValue, clslivreursNew.CalculerTaxes, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewEmail", IIf(Not IsNothing(clslivreursNew.Email), clslivreursNew.Email, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewEmail1", IIf(Not IsNothing(clslivreursNew.Email1), clslivreursNew.Email1, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewEmail2", IIf(Not IsNothing(clslivreursNew.Email2), clslivreursNew.Email2, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewIsVendeur", IIf(clslivreursNew.IsVendeur.HasValue, clslivreursNew.IsVendeur, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewActif", IIf(clslivreursNew.Actif.HasValue, clslivreursNew.Actif, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewIDVendeur", IIf(clslivreursNew.IDVendeur.HasValue, clslivreursNew.IDVendeur, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewCommissionVendeur", IIf(clslivreursNew.CommissionVendeur.HasValue, clslivreursNew.CommissionVendeur, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewNomCompagnie", IIf(Not IsNothing(clslivreursNew.NomCompagnie), clslivreursNew.NomCompagnie, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewNoTPS", IIf(Not IsNothing(clslivreursNew.NoTPS), clslivreursNew.NoTPS, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewNoTVQ", IIf(Not IsNothing(clslivreursNew.NoTVQ), clslivreursNew.NoTVQ, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@OldLivreurID", clslivreursNew.LivreurID)
        Try
            connection.Open()
            Dim count As Integer = updateCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

    Public Function Delete(ByVal clsLivreurs As livreurs) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim deleteStatement As String _
            = "DELETE FROM " _
            & "     [Livreurs] " _
            & "WHERE " _
            & "     [LivreurID] = @OldLivreurID " _
            & ""
        Dim deleteCommand As New SqlCommand(deleteStatement, connection)
        deleteCommand.CommandType = CommandType.Text
        deleteCommand.Parameters.AddWithValue("@OldLivreurID", clsLivreurs.LivreurID)
        Try
            connection.Open()
            Dim count As Integer = deleteCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Erreur")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

End Class

