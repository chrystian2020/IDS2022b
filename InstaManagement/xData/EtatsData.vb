Imports System.Data.SqlClient

Public Class EtatsData

    Public Function SelectAllDataByPeriodes(PIDPeriode As Integer, piIDRapport As String) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDEtats] " _
            & "    ,[IDPeriode] " _
            & "    ,[IDOrganisation] " _
            & "    ,[IDVendeur] " _
            & "    ,[CheminRapport] " _
            & "    ,[NomFichier] " _
            & "    ,[Email] " _
            & "    ,[Email1] " _
            & "    ,[Email2] " _
            & "    ,[Organisation] " _
            & "    ,[Vendeur] " _
            & "    ,[DateFrom] " _
            & "    ,[DateTo] " _
            & " FROM " _
            & "     [Etats] " _
            & " WHERE IDOrganisation IS NOT NULL AND IDPeriode =" & PIDPeriode

        If piIDRapport.ToString.Trim <> "" Then
            selectStatement = selectStatement & " AND IDEtats IN (" & piIDRapport & ")"
        End If

        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function


    Public Function SelectAllByDates(pdDateFrom As DateTime, pdDateTo As DateTime) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDEtats] " _
            & "    ,[IDPeriode] " _
            & "    ,[IDOrganisation] " _
            & "    ,[IDVendeur] " _
            & "    ,[CheminRapport] " _
            & "    ,[NomFichier] " _
            & "    ,[Email] " _
            & "    ,[Email1] " _
            & "    ,[Email2] " _
            & "    ,[Organisation] " _
            & "    ,[Vendeur] " _
            & "    ,[DateFrom] " _
            & "    ,[DateTo] " _
            & " FROM " _
            & "     [Etats] " _
            & " WHERE (DateFrom  >= '" & pdDateFrom & "' AND DateTo <= '" & pdDateTo & "')"


        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function

    Public Function SelectAllByPeriode(pIDPeriode As Integer) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDEtats] " _
            & "    ,[IDPeriode] " _
            & "    ,[IDOrganisation] " _
            & "    ,[IDVendeur] " _
            & "    ,[CheminRapport] " _
            & "    ,[NomFichier] " _
            & "    ,[Email] " _
            & "    ,[Email1] " _
            & "    ,[Email2] " _
            & "    ,[Organisation] " _
            & "    ,[Vendeur] " _
            & "    ,[DateFrom] " _
            & "    ,[DateTo] " _
            & "FROM " _
            & "     [Etats] " _
            & " WHERE IDPeriode =" & pIDPeriode
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function
    Public Function SelectAll() As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDEtats] " _
            & "    ,[IDPeriode] " _
            & "    ,[IDOrganisation] " _
            & "    ,[IDVendeur] " _
            & "    ,[CheminRapport] " _
            & "    ,[NomFichier] " _
            & "    ,[Email] " _
            & "    ,[Email1] " _
            & "    ,[Email2] " _
            & "    ,[Organisation] " _
            & "    ,[Vendeur] " _
            & "    ,[DateFrom] " _
            & "    ,[DateTo] " _
            & "FROM " _
            & "     [Etats] " _
            & ""
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function

    Public Function Select_Record(ByVal clsEtatsPara As Etats) As Etats
        Dim clsEtats As New Etats
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDEtats] " _
            & "    ,[IDPeriode] " _
            & "    ,[IDOrganisation] " _
            & "    ,[IDVendeur] " _
            & "    ,[CheminRapport] " _
            & "    ,[NomFichier] " _
            & "    ,[Email] " _
            & "    ,[Email1] " _
            & "    ,[Email2] " _
            & "    ,[Organisation] " _
            & "    ,[Vendeur] " _
            & "    ,[DateFrom] " _
            & "    ,[DateTo] " _
            & "FROM " _
            & "     [Etats] " _
            & "WHERE " _
            & "     [IDEtats] = @IDEtats " _
            & ""
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        selectCommand.Parameters.AddWithValue("@IDEtats", clsEtatsPara.IDEtats)
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader(CommandBehavior.SingleRow)
            If reader.Read Then
                With clsEtats
                    .IDEtats = System.Convert.ToInt32(reader("IDEtats"))
                    .IDPeriode = If(IsDBNull(reader("IDPeriode")), Nothing, CType(reader("IDPeriode"), Int32?))
                    .IDOrganisation = If(IsDBNull(reader("IDOrganisation")), Nothing, CType(reader("IDOrganisation"), Int32?))
                    .IDVendeur = If(IsDBNull(reader("IDVendeur")), Nothing, CType(reader("IDVendeur"), Int32?))
                    .CheminRapport = System.Convert.ToString(reader("CheminRapport"))
                    .NomFichier = If(IsDBNull(reader("NomFichier")), Nothing, reader("NomFichier").ToString)
                    .Email = If(IsDBNull(reader("Email")), Nothing, reader("Email").ToString)
                    .Email1 = If(IsDBNull(reader("Email1")), Nothing, reader("Email1").ToString)
                    .Email2 = If(IsDBNull(reader("Email2")), Nothing, reader("Email2").ToString)
                    .Organisation = If(IsDBNull(reader("Organisation")), Nothing, reader("Organisation").ToString)
                    .Vendeur = If(IsDBNull(reader("Vendeur")), Nothing, reader("Vendeur").ToString)
                    .DateFrom = If(IsDBNull(reader("DateFrom")), Nothing, CType(reader("DateFrom"), DateTime?))
                    .DateTo = If(IsDBNull(reader("DateTo")), Nothing, CType(reader("DateTo"), DateTime?))
                End With
            Else
                clsEtats = Nothing
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return clsEtats
    End Function

    Public Function Add(ByVal clsEtats As Etats) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim insertStatement As String _
            = "INSERT " _
            & "     [Etats] " _
            & "     ( " _
            & "     [IDPeriode] " _
            & "    ,[IDOrganisation] " _
            & "    ,[IDVendeur] " _
            & "    ,[CheminRapport] " _
            & "    ,[NomFichier] " _
            & "    ,[Email] " _
            & "    ,[Email1] " _
            & "    ,[Email2] " _
            & "    ,[Organisation] " _
            & "    ,[Vendeur] " _
            & "    ,[DateFrom] " _
            & "    ,[DateTo] " _
            & "     ) " _
            & "VALUES " _
            & "     ( " _
            & "     @IDPeriode " _
            & "    ,@IDOrganisation " _
            & "    ,@IDVendeur " _
            & "    ,@CheminRapport " _
            & "    ,@NomFichier " _
            & "    ,@Email " _
            & "    ,@Email1 " _
            & "    ,@Email2 " _
            & "    ,@Organisation " _
            & "    ,@Vendeur " _
            & "    ,@DateFrom " _
            & "    ,@DateTo " _
            & "     ) " _
            & ""
        Dim insertCommand As New SqlCommand(insertStatement, connection)
        insertCommand.CommandType = CommandType.Text
        insertCommand.Parameters.AddWithValue("@IDPeriode", IIf(clsEtats.IDPeriode.HasValue, clsEtats.IDPeriode, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@IDOrganisation", IIf(clsEtats.IDOrganisation.HasValue, clsEtats.IDOrganisation, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@IDVendeur", IIf(clsEtats.IDVendeur.HasValue, clsEtats.IDVendeur, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@CheminRapport", clsEtats.CheminRapport)
        insertCommand.Parameters.AddWithValue("@NomFichier", IIf(Not IsNothing(clsEtats.NomFichier), clsEtats.NomFichier, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Email", IIf(Not IsNothing(clsEtats.Email), clsEtats.Email, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Email1", IIf(Not IsNothing(clsEtats.Email1), clsEtats.Email1, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Email2", IIf(Not IsNothing(clsEtats.Email2), clsEtats.Email2, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Organisation", IIf(Not IsNothing(clsEtats.Organisation), clsEtats.Organisation, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Vendeur", IIf(Not IsNothing(clsEtats.Vendeur), clsEtats.Vendeur, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@DateFrom", IIf(clsEtats.DateFrom.HasValue, clsEtats.DateFrom, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@DateTo", IIf(clsEtats.DateTo.HasValue, clsEtats.DateTo, DBNull.Value))
        Try
            connection.Open()
            Dim count As Integer = insertCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

    Public Function Update(ByVal clsEtatsOld As Etats,
           ByVal clsEtatsNew As Etats) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim updateStatement As String _
            = "UPDATE " _
            & "     [Etats] " _
            & "SET " _
            & "     [IDPeriode] = @NewIDPeriode " _
            & "    ,[IDOrganisation] = @NewIDOrganisation " _
            & "    ,[IDVendeur] = @NewIDVendeur " _
            & "    ,[CheminRapport] = @NewCheminRapport " _
            & "    ,[NomFichier] = @NewNomFichier " _
            & "    ,[Email] = @NewEmail " _
            & "    ,[Email1] = @NewEmail1 " _
            & "    ,[Email2] = @NewEmail2 " _
            & "    ,[Organisation] = @NewOrganisation " _
            & "    ,[Vendeur] = @NewVendeur " _
            & "    ,[DateFrom] = @NewDateFrom " _
            & "    ,[DateTo] = @NewDateTo " _
            & "WHERE " _
            & "     [IDEtats] = @OldIDEtats " _
            & ""
        Dim updateCommand As New SqlCommand(updateStatement, connection)
        updateCommand.CommandType = CommandType.Text
        updateCommand.Parameters.AddWithValue("@NewIDPeriode", IIf(clsEtatsNew.IDPeriode.HasValue, clsEtatsNew.IDPeriode, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewIDOrganisation", IIf(clsEtatsNew.IDOrganisation.HasValue, clsEtatsNew.IDOrganisation, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewIDVendeur", IIf(clsEtatsNew.IDVendeur.HasValue, clsEtatsNew.IDVendeur, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewCheminRapport", clsEtatsNew.CheminRapport)
        updateCommand.Parameters.AddWithValue("@NewNomFichier", IIf(Not IsNothing(clsEtatsNew.NomFichier), clsEtatsNew.NomFichier, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewEmail", IIf(Not IsNothing(clsEtatsNew.Email), clsEtatsNew.Email, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewEmail1", IIf(Not IsNothing(clsEtatsNew.Email1), clsEtatsNew.Email1, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewEmail2", IIf(Not IsNothing(clsEtatsNew.Email2), clsEtatsNew.Email2, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewOrganisation", IIf(Not IsNothing(clsEtatsNew.Organisation), clsEtatsNew.Organisation, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewVendeur", IIf(Not IsNothing(clsEtatsNew.Vendeur), clsEtatsNew.Vendeur, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewDateFrom", IIf(clsEtatsNew.DateFrom.HasValue, clsEtatsNew.DateFrom, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewDateTo", IIf(clsEtatsNew.DateTo.HasValue, clsEtatsNew.DateTo, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@OldIDEtats", clsEtatsOld.IDEtats)

        Try
            connection.Open()
            Dim count As Integer = updateCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function
    Public Function DeleteByPeriode(pIDPeriode As Integer) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim deleteStatement As String _
            = "DELETE FROM " _
            & "     [Etats] " _
            & "WHERE " _
            & "     [IDPeriode] = " & pIDPeriode

        Dim deleteCommand As New SqlCommand(deleteStatement, connection)
        deleteCommand.CommandType = CommandType.Text

        Try
            connection.Open()
            Dim count As Integer = deleteCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function
    Public Function Delete(ByVal clsEtats As Etats) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim deleteStatement As String _
            = "DELETE FROM " _
            & "     [Etats] " _
            & "WHERE " _
            & "     [IDEtats] = @OldIDEtats " _
            & ""
        Dim deleteCommand As New SqlCommand(deleteStatement, connection)
        deleteCommand.CommandType = CommandType.Text
        deleteCommand.Parameters.AddWithValue("@OldIDEtats", clsEtats.IDEtats)

        Try
            connection.Open()
            Dim count As Integer = deleteCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

End Class

