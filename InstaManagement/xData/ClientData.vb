Imports System.Data.SqlClient

Public Class ClientData
    Public Function SelectAllClient() As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDClient] " _
            & "    ,[Nom] " _
            & "FROM " _
            & "     [Client] " _
            & ""
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function
    Public Function SelectAll() As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDClient] " _
            & "    ,[Nom] " _
            & "    ,[Contact] " _
            & "    ,[Telephone] " _
            & "    ,[Mobile] " _
            & "    ,[Courriel] " _
            & "FROM " _
            & "     [Client] " _
            & ""
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function

    Public Function Select_Record(ByVal clsClientPara As Client) As Client
        Dim clsClient As New Client
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDClient] " _
            & "    ,[Nom] " _
            & "    ,[Contact] " _
            & "    ,[Telephone] " _
            & "    ,[Mobile] " _
            & "    ,[Courriel] " _
            & "FROM " _
            & "     [Client] " _
            & "WHERE " _
            & "     [IDClient] = @IDClient " _
            & ""
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        selectCommand.Parameters.AddWithValue("@IDClient", clsClientPara.IDClient)
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader(CommandBehavior.SingleRow)
            If reader.Read Then
                With clsClient
                    .IDClient = System.Convert.ToInt32(reader("IDClient"))
                    .Nom = If(IsDBNull(reader("Nom")), Nothing, reader("Nom").ToString)
                    .Contact = If(IsDBNull(reader("Contact")), Nothing, reader("Contact").ToString)
                    .Telephone = If(IsDBNull(reader("Telephone")), Nothing, reader("Telephone").ToString)
                    .Mobile = If(IsDBNull(reader("Mobile")), Nothing, reader("Mobile").ToString)
                    .Courriel = If(IsDBNull(reader("Courriel")), Nothing, reader("Courriel").ToString)
                End With
            Else
                clsClient = Nothing
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return clsClient
    End Function

    Public Function Add(ByVal clsClient As Client) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim insertStatement As String _
            = "INSERT " _
            & "     [Client] " _
            & "     ( " _
            & "     [Nom] " _
            & "    ,[Contact] " _
            & "    ,[Telephone] " _
            & "    ,[Mobile] " _
            & "    ,[Courriel] " _
            & "     ) " _
            & "VALUES " _
            & "     ( " _
            & "     @Nom " _
            & "    ,@Contact " _
            & "    ,@Telephone " _
            & "    ,@Mobile " _
            & "    ,@Courriel " _
            & "     ) " _
            & ""
        Dim insertCommand As New SqlCommand(insertStatement, connection)
        insertCommand.CommandType = CommandType.Text
        insertCommand.Parameters.AddWithValue("@Nom", IIf(Not IsNothing(clsClient.Nom), clsClient.Nom, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Contact", IIf(Not IsNothing(clsClient.Contact), clsClient.Contact, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Telephone", IIf(Not IsNothing(clsClient.Telephone), clsClient.Telephone, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Mobile", IIf(Not IsNothing(clsClient.Mobile), clsClient.Mobile, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Courriel", IIf(Not IsNothing(clsClient.Courriel), clsClient.Courriel, DBNull.Value))
        Try
            connection.Open()
            Dim count As Integer = insertCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

    Public Function Update(ByVal clsClientOld As Client,
           ByVal clsClientNew As Client) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim updateStatement As String _
            = "UPDATE " _
            & "     [Client] " _
            & "SET " _
            & "     [Nom] = @NewNom " _
            & "    ,[Contact] = @NewContact " _
            & "    ,[Telephone] = @NewTelephone " _
            & "    ,[Mobile] = @NewMobile " _
            & "    ,[Courriel] = @NewCourriel " _
            & "WHERE " _
            & "     [IDClient] = @OldIDClient " _
            & ""
        Dim updateCommand As New SqlCommand(updateStatement, connection)
        updateCommand.CommandType = CommandType.Text
        updateCommand.Parameters.AddWithValue("@NewNom", IIf(Not IsNothing(clsClientNew.Nom), clsClientNew.Nom, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewContact", IIf(Not IsNothing(clsClientNew.Contact), clsClientNew.Contact, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewTelephone", IIf(Not IsNothing(clsClientNew.Telephone), clsClientNew.Telephone, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewMobile", IIf(Not IsNothing(clsClientNew.Mobile), clsClientNew.Mobile, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewCourriel", IIf(Not IsNothing(clsClientNew.Courriel), clsClientNew.Courriel, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@OldIDClient", clsClientOld.IDClient)

        Try
            connection.Open()
            Dim count As Integer = updateCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

    Public Function Delete(ByVal clsClient As Client) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim deleteStatement As String _
            = "DELETE FROM " _
            & "     [Client] " _
            & "WHERE " _
            & "     [IDClient] = @OldIDClient " _
            & ""
        Dim deleteCommand As New SqlCommand(deleteStatement, connection)
        deleteCommand.CommandType = CommandType.Text
        deleteCommand.Parameters.AddWithValue("@OldIDClient", clsClient.IDClient)

        Try
            connection.Open()
            Dim count As Integer = deleteCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

End Class
 
