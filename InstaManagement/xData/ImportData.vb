Imports System.Data.SqlClient

Public Class ImportData
    Public Sub New()

    End Sub
    Public Function CopyBackupToImport(pIDPeriode As Integer) As Boolean

        Dim clsImportBackup As New ImportBackup
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectProcedure As String = "[CopyBackupToImport]"
        Dim ExecuteCommand As New SqlCommand(selectProcedure, connection)
        ExecuteCommand.CommandType = CommandType.StoredProcedure
        ExecuteCommand.Parameters.AddWithValue("@IDPeriode", pIDPeriode)
        ExecuteCommand.Parameters.Add("@ReturnValue", System.Data.SqlDbType.Int)
        ExecuteCommand.Parameters("@ReturnValue").Direction = ParameterDirection.Output
        Try
            connection.Open()
            ExecuteCommand.ExecuteNonQuery()
            Dim count As Integer = System.Convert.ToInt32(ExecuteCommand.Parameters("@ReturnValue").Value)
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True





    End Function

    Public Function CopyImportToBackupByImportID(pImportID As Integer) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "INSERT INTO [ImportBackup] " &
            "            ([reference], " &
            "             [code], " &
            "             [status], " &
            "             [client], " &
            "             [clientaddress], " &
            "             [momentdepassage], " &
            "             [preparepar], " &
            "             [heureappel], " &
            "             [heureprep], " &
            "             [heuredepart], " &
            "             [heurelivraison], " &
            "             [messagepassage], " &
            "             [totaltempservice], " &
            "             [livreur], " &
            "             [distance], " &
            "             [montant], " &
            "             [extra], " &
            "             [organisation], " &
            "             [organisationaddress], " &
            "             [idnomfichierimport], " &
            "             [debit], " &
            "             [credit], " &
            "             [glacier], " &
            "             [nombreglacier], " &
            "             [montantcontrat], " &
            "             [organisationid], " &
            "             [livreurid], " &
            "             [montantlivreur], " &
            "             [montantglacierorganisation], " &
            "             [montantglacierlivreur], " &
            "             [montantlivreurfinal], " &
            "             [montantlivreurpharmaplus], " &
            "             [extrakm], " &
            "             [echange], " &
            "             [idperiode], " &
            "             [extralivreur], " &
            "             [extrakmlivreur], " &
            "             [idsamount], " &
            "             [dateimport],IsMobilus,IDClientAddresse,IDResidence,Residence) " &
            " SELECT [reference], " &
            "       [code], " &
            "       [status], " &
            "       [client], " &
            "       [clientaddress], " &
            "       [momentdepassage], " &
            "       [preparepar], " &
            "       [heureappel], " &
            "       [heureprep], " &
            "       [heuredepart], " &
            "       [heurelivraison], " &
            "       [messagepassage], " &
            "       [totaltempservice], " &
            "       [livreur], " &
            "       [distance], " &
            "       [montant], " &
            "       [extra], " &
            "       [organisation], " &
            "       [organisationaddress], " &
            "       [idnomfichierimport], " &
            "       [debit], " &
            "       [credit], " &
            "       [glacier], " &
            "       [nombreglacier], " &
            "       [montantcontrat], " &
            "       [organisationid], " &
            "       [livreurid], " &
            "       [montantlivreur], " &
            "       [montantglacierorganisation], " &
            "       [montantglacierlivreur], " &
            "       [montantlivreurfinal], " &
            "       [montantlivreurpharmaplus], " &
            "       [extrakm], " &
            "       [echange], " &
            "       [idperiode], " &
            "       [extralivreur], " &
            "       [extrakmlivreur], " &
            "       [idsamount], " &
            "       [dateimport],IsMobilus,IDClientAddresse,IDResidence,Residence " &
            " FROM   [dbo].[Import] " &
            " WHERE ImportID =" & pImportID


        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim count As Integer = selectCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try

    End Function

    Public Function CopyImportToBackupByIDFile(pidnomfichierimport As Integer) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "INSERT INTO [ImportBackup] " &
            "            ([reference], " &
            "             [code], " &
            "             [status], " &
            "             [client], " &
            "             [clientaddress], " &
            "             [momentdepassage], " &
            "             [preparepar], " &
            "             [heureappel], " &
            "             [heureprep], " &
            "             [heuredepart], " &
            "             [heurelivraison], " &
            "             [messagepassage], " &
            "             [totaltempservice], " &
            "             [livreur], " &
            "             [distance], " &
            "             [montant], " &
            "             [extra], " &
            "             [organisation], " &
            "             [organisationaddress], " &
            "             [idnomfichierimport], " &
            "             [debit], " &
            "             [credit], " &
            "             [glacier], " &
            "             [nombreglacier], " &
            "             [montantcontrat], " &
            "             [organisationid], " &
            "             [livreurid], " &
            "             [montantlivreur], " &
            "             [montantglacierorganisation], " &
            "             [montantglacierlivreur], " &
            "             [montantlivreurfinal], " &
            "             [montantlivreurpharmaplus], " &
            "             [extrakm], " &
            "             [echange], " &
            "             [idperiode], " &
            "             [extralivreur], " &
            "             [extrakmlivreur], " &
            "             [idsamount], " &
            "             [dateimport],IsMobilus,IDClientAddresse,IDResidence,Residence) " &
            " SELECT [reference], " &
            "       [code], " &
            "       [status], " &
            "       [client], " &
            "       [clientaddress], " &
            "       [momentdepassage], " &
            "       [preparepar], " &
            "       [heureappel], " &
            "       [heureprep], " &
            "       [heuredepart], " &
            "       [heurelivraison], " &
            "       [messagepassage], " &
            "       [totaltempservice], " &
            "       [livreur], " &
            "       [distance], " &
            "       [montant], " &
            "       [extra], " &
            "       [organisation], " &
            "       [organisationaddress], " &
            "       [idnomfichierimport], " &
            "       [debit], " &
            "       [credit], " &
            "       [glacier], " &
            "       [nombreglacier], " &
            "       [montantcontrat], " &
            "       [organisationid], " &
            "       [livreurid], " &
            "       [montantlivreur], " &
            "       [montantglacierorganisation], " &
            "       [montantglacierlivreur], " &
            "       [montantlivreurfinal], " &
            "       [montantlivreurpharmaplus], " &
            "       [extrakm], " &
            "       [echange], " &
            "       [idperiode], " &
            "       [extralivreur], " &
            "       [extrakmlivreur], " &
            "       [idsamount], " &
            "       [dateimport],IsMobilus,IDClientAddresse,IDResidence,Residence " &
            " FROM   [dbo].[Import] " &
            " WHERE idnomfichierimport =" & pidnomfichierimport


        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim count As Integer = selectCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try

    End Function
    Public Function CopyImportToBackup() As Boolean
        Dim clsImportBackup As New ImportBackup
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectProcedure As String = "[CopyImportToBackup]"
        Dim ExecuteCommand As New SqlCommand(selectProcedure, connection)
        ExecuteCommand.CommandType = CommandType.StoredProcedure
        ExecuteCommand.Parameters.Add("@ReturnValue", System.Data.SqlDbType.Int)
        ExecuteCommand.Parameters("@ReturnValue").Direction = ParameterDirection.Output
        Try
            connection.Open()
            ExecuteCommand.ExecuteNonQuery()
            Dim count As Integer = System.Convert.ToInt32(ExecuteCommand.Parameters("@ReturnValue").Value)
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function



    Public Function UpdateImportGlaciersOrganisationLivreur(pdDateFrom As DateTime, pdDateTo As DateTime, piOrganisationID As Integer, piIDLivreur As Integer, pblnPrixGlacierL As Double) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim updateStatement As String _
            = "UPDATE " _
            & "     [Import] " _
            & " Set " _
            & "    [MontantGlacierLivreur] = NombreGlacier *  " & pblnPrixGlacierL _
            & " WHERE " _
            & " NombreGlacier > 0" _
            & " And (DateImport  >= '" & pdDateFrom.ToString & "' AND DateImport <= '" & pdDateTo.ToString & "') AND IDPeriode = " & go_Globals.IDPeriode _
            & " AND OrganisationID = " & piOrganisationID _
            & " AND LivreurID = " & piIDLivreur
        Dim updateCommand As New SqlCommand(updateStatement, connection)
        updateCommand.CommandType = CommandType.Text

        Try
            connection.Open()
            Dim count As Integer = updateCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Erreur")
        Finally
            connection.Close()
        End Try
        Return True


    End Function

    Public Function SelectOrganisationsAvecFactureString(pdDateFrom As DateTime, pdDateTo As DateTime, piIDOrganisation As String) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT DISTINCT" _
            & "    [OrganisationID] " _
            & "FROM " _
            & "     [Import] " _
            & "     WHERE (DateImport  >= '" & pdDateFrom.ToString & "' AND DateImport <= '" & pdDateTo.ToString & "') AND IDPeriode = " & go_Globals.IDPeriode

        If piIDOrganisation.ToString.Trim <> "" Then
            selectStatement = selectStatement & " AND OrganisationID IN (" & piIDOrganisation & ")"
        End If





        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function

    Public Function SelectLivreursAvecFactureRestaurant(pdDateFrom As DateTime, pdDateTo As DateTime, piIDLivreurs As String) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT DISTINCT" _
            & "    [LivreurID] " _
            & "FROM " _
            & "     [Import] " _
            & "     WHERE IsMobilus = 0 AND (DateImport  >= '" & pdDateFrom.ToString & "' AND DateImport <= '" & pdDateTo.ToString & "') AND IDPeriode = " & go_Globals.IDPeriode


        If piIDLivreurs.ToString.Trim <> "" Then
            selectStatement = selectStatement & " AND OrganisationID IN (" & piIDLivreurs & ")"
        End If



        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function

    Public Function SelectOrganisationsAvecFactureRestaurant(pdDateFrom As DateTime, pdDateTo As DateTime, piIDOrganisation As String) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT DISTINCT" _
            & "    dbo.Invoice.OrganisationID " _
            & "FROM " _
            & "     dbo.Invoice LEFT OUTER JOIN " _
            & "     dbo.Organisations ON dbo.Invoice.OrganisationID = dbo.Organisations.OrganisationID " _
            & "     WHERE (dbo.Organisations.Restaurant = 1) AND (dbo.Invoice.DateFrom  >= '" & pdDateFrom & "' AND dbo.Invoice.DateTo <= '" & pdDateTo & "')"


        If piIDOrganisation.ToString.Trim <> "" Then
            selectStatement = selectStatement & " AND OrganisationID IN (" & piIDOrganisation & ")"
        End If



        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function

    Public Function SelectOrganisationsAvecFacture(pdDateFrom As DateTime, pdDateTo As DateTime, piIDOrganisation As String) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT DISTINCT" _
            & "    [OrganisationID] " _
            & "FROM " _
            & "     [Import] " _
            & "     WHERE (DateImport  >= '" & pdDateFrom.ToString & "' AND DateImport <= '" & pdDateTo.ToString & "') AND IDPeriode = " & go_Globals.IDPeriode


        If piIDOrganisation.ToString.Trim <> "" Then
            selectStatement = selectStatement & " AND OrganisationID IN (" & piIDOrganisation & ")"
        End If



        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function

    Public Function SelectOrganisationsAvecFactures(pdDateFrom As DateTime, pdDateTo As DateTime, piIDOrganisation As Integer) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT DISTINCT" _
            & "    [OrganisationID] " _
            & "FROM " _
            & "     [Import] " _
            & "     WHERE (DateImport  >= '" & pdDateFrom.ToString & "' AND DateImport <= '" & pdDateTo.ToString & "') AND IDPeriode = " & go_Globals.IDPeriode
        If piIDOrganisation > 0 Then
            selectStatement = selectStatement & " AND OrganisationID = " & piIDOrganisation
        End If



        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function

    Public Function SelectLivreurAvecPayeStringVendeurs(pdDateFrom As DateTime, pdDateTo As DateTime, piIDlivreur As String) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT DISTINCT" _
            & "    [LivreurID] " _
            & "FROM " _
            & "     [Import] " _
            & "     WHERE IsVendeur = 1 (DateImport  >= '" & pdDateFrom.ToString & "' AND DateImport <= '" & pdDateTo.ToString & "') AND IDPeriode = " & go_Globals.IDPeriode

        If piIDlivreur.ToString.Trim <> "" Then
            selectStatement = selectStatement & " AND LivreurID IN (" & piIDlivreur & ")"
        End If


        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function
    Public Function SelectLivreurAvecPayeString(pdDateFrom As DateTime, pdDateTo As DateTime, piIDlivreur As String) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT DISTINCT dbo.Import.LivreurID" _
            & " FROM dbo.Import LEFT OUTER JOIN " _
            & " dbo.livreurs ON dbo.Import.LivreurID = dbo.livreurs.LivreurID " _
            & " WHERE (dbo.Import.DateImport  >= '" & pdDateFrom.ToString & "' AND dbo.Import.DateImport <= '" & pdDateTo.ToString & "') AND dbo.Import.IDPeriode = " & go_Globals.IDPeriode _
            & " AND (dbo.livreurs.IsVendeur = 0)"

        If piIDlivreur.ToString.Trim <> "" Then
            selectStatement = selectStatement & " AND dbo.Import.LivreurID IN (" & piIDlivreur & ")"
        End If


        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function
    Public Function SelectLivreurAvecPaye(pdDateFrom As DateTime, pdDateTo As DateTime, piIDlivreur As Integer) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT DISTINCT" _
            & "    [LivreurID] " _
            & "FROM " _
            & "     [Import] " _
            & "     WHERE (DateImport  >= '" & pdDateFrom.ToString & "' AND DateImport <= '" & pdDateTo.ToString & "') AND IDPeriode = " & go_Globals.IDPeriode

        If piIDlivreur > 0 Then
            selectStatement = selectStatement & " AND LivreurID =" & piIDlivreur

        End If


        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function
    Public Function SelectAllImportbyOrganisationLivreur(pdDateFrom As DateTime, pdDateTo As DateTime, piLivreurID As Integer) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "Select count(importID) AS QuantiteCommande, OrganisationID,Organisation " _
            & "     FROM " _
            & "     [Import] " _
            & "     WHERE (DateImport  >= '" & pdDateFrom.ToString & "' AND DateImport <= '" & pdDateTo.ToString & "') AND IDPeriode = " & go_Globals.IDPeriode & " AND LivreurID=" & piLivreurID _
            & "     group by OrganisationID , Organisation "



        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable

        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            Dim dTotalAmount As Double = 0
            If reader.HasRows Then
                dt.Load(reader)
                If dt.Rows.Count > 0 Then
                Else
                End If

            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Erreur")
        Finally
            connection.Close()
        End Try
        Return dt

    End Function






    Public Function SelectTotalAmountByCode(pdDateFrom As DateTime, pdDateTo As DateTime, piOrganisationID As Integer) As DataTable

        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "Select " _
            & "     [ImportID] " _
            & "    ,[Reference] " _
            & "    ,[Code] " _
            & "    ,[Status] " _
            & "    ,[Client] " _
            & "    ,[ClientAddress] " _
            & "    ,[MomentDePassage] " _
            & "    ,[PreparePar] " _
            & "    ,[HeureAppel] " _
            & "    ,[HeurePrep] " _
            & "    ,[HeureDepart] " _
            & "    ,[HeureLivraison] " _
            & "    ,[MessagePassage] " _
            & "    ,[TotalTempService] " _
            & "    ,[Livreur] " _
            & "    ,[Distance] " _
            & "    ,[Montant] " _
            & "    ,[Extra] " _
            & "    ,[Organisation] " _
            & "    ,[OrganisationAddress] " _
            & "    ,[IDNomFichierImport] " _
            & "    ,[Debit] " _
            & "    ,[Credit] " _
            & "    ,[Glacier] " _
            & "    ,[NombreGlacier] " _
            & "    ,[MontantContrat] " _
            & "    ,[OrganisationID] " _
            & "    ,[LivreurID] " _
            & "    ,[MontantLivreur] " _
            & "    ,[MontantGlacierOrganisation] " _
            & "    ,[MontantGlacierLivreur] " _
            & "    ,[MontantLivreurFinal] " _
            & "    ,[MontantLivreurPharmaplus] " _
            & "    ,[ExtraKM] " _
            & "    ,[Echange] " _
            & "    ,[IDPeriode] " _
            & "    ,[ExtraLivreur] " _
            & "    ,[ExtraKMLivreur] " _
            & "    , SUM([Import].[Amount]) as TotalMontant " _
            & "     FROM " _
            & "     [Import] " _
            & "     WHERE (DateImport  >= '" & pdDateFrom.ToString & "' AND DateImport <= '" & pdDateTo.ToString & "')  AND IDPeriode = " & go_Globals.IDPeriode & " AND OrganisationID=" & piOrganisationID


        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable

        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            Dim dTotalAmount As Double = 0
            If reader.HasRows Then
                dt.Load(reader)
                If dt.Rows.Count > 0 Then
                Else
                End If

            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Erreur")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function
    Public Function SelectGlacierByDateLivreur(pdDateFrom As DateTime, pdDateTo As DateTime, piOrganisationID As Integer, ILivreurID As Integer) As DataTable


        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String = "SELECT " _
            & " SUM(MontantGlacierLivreur)             As MontantGlacierLivreur, " _
            & " SUM(NombreGlacier)             As TotalNombreGlacier, " _
            & " Cast(DateImport As Date)         As PreparationDate " _
            & " FROM " _
            & " Import " _
            & " WHERE (DateImport  >= '" & pdDateFrom.ToString & "' AND DateImport <= '" & pdDateTo.ToString & "') AND IDPeriode = " & go_Globals.IDPeriode & " AND MontantGlacierLivreur > 0" _
            & " AND LivreurID=" & ILivreurID _
            & " GROUP BY CAST(DateImport AS DATE), Organisation "

        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()

        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Erreur")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function
    Public Function SelectExtraByDateLivreur(pdDateFrom As DateTime, pdDateTo As DateTime, piOrganisationID As Integer, ILivreurID As Integer) As DataTable


        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String = "SELECT " _
            & " Count(*)             As TotalExtraCount, " _
            & " SUM(ExtraLivreur)             As TotalExtra, " _
            & " SUM(ExtraKMLivreur)             As TotalExtraKM, " _
            & " Cast(DateImport As Date)         As PreparationDate " _
            & " FROM " _
            & " Import " _
            & " WHERE (DateImport  >= '" & pdDateFrom.ToString & "' AND DateImport <= '" & pdDateTo.ToString & "') AND IDPeriode = " & go_Globals.IDPeriode & "  AND ExtraLivreur > 0" _
            & " And LivreurID = " & ILivreurID _
            & " GROUP BY CAST(DateImport As Date), Organisation "

        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()

        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Erreur")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function

    Public Function SelectDeliveryByDateLivreur(pdDateFrom As DateTime, pdDateTo As DateTime, piOrganisationID As Integer, ILivreurID As Integer) As DataTable


        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String = "Select " _
            & " Count(*)                        As TotalLivraison, " _
            & " Round(SUM(distance), 1)         As TotalKM, " _
            & " SUM(MontantLivreur)             As TotalCommandeLivreur, " _
            & " Cast(DateImport As Date)         As PreparationDate " _
            & " FROM " _
            & " Import " _
            & " WHERE (DateImport  >= '" & pdDateFrom.ToString & "' AND DateImport <= '" & pdDateTo.ToString & "') AND IDPeriode = " & go_Globals.IDPeriode & " AND OrganisationID=" & piOrganisationID _
            & " AND LivreurID=" & ILivreurID _
            & " GROUP BY CAST(DateImport AS DATE), Organisation "





        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()

        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Erreur")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function

    Public Function SelectDeliveryByDateLivreur(pdDateFrom As DateTime, pdDateTo As DateTime, piOrganisationID As Integer) As DataTable


        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "  count(Extra) as totalExtraNumber,Sum(Extra) as totalExtra,Sum(Debit) as totalDebit, Sum(Credit) as TotalCredit,Sum(NombreGlacier) as TotalGlacier,Sum(MontantGlacierOrganisation) as TotalMontantGlacierOrganisation, Sum(Distance) as TotalKM, SUM(Montant ) as TotalCommande,Count(*) as TotalLivraison,CAST(DateImport AS DATE) as PreparationDate, Organisation " _
            & " FROM " _
            & " Import " _
            & " WHERE (DateImport  >= '" & pdDateFrom.ToString & "' AND DateImport <= '" & pdDateTo.ToString & "') AND IDPeriode = " & go_Globals.IDPeriode & " AND OrganisationID=" & piOrganisationID _
            & " GROUP BY CAST(DateImport AS DATE), Organisation "


        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()

        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Erreur")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function

    Public Function SelectTotalExtra(pdDateFrom As DateTime, pdDateTo As DateTime, piOrganisationID As Integer) As DataTable


        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "  count(Extra) as totalExtraNumber,Sum(Extra) as TotalExtra " _
            & " FROM " _
            & " Import " _
            & " WHERE (DateImport  >= '" & pdDateFrom.ToString & "' AND DateImport <= '" & pdDateTo.ToString & "') AND IDPeriode = " & go_Globals.IDPeriode & " AND OrganisationID=" & piOrganisationID _
            & " AND Extra > 0 " _
            & " GROUP BY CAST(DateImport AS DATE), Organisation "


        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()

        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Erreur")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function
    Public Function SelectDeliveryByDateExtra(pdPreparationDate As Date, piOrganisationID As Integer) As DataTable


        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & " Count(extra)                    As TotalExtraNumber, " _
            & " SUM(extra)                      As totalExtra, " _
            & " Cast(DateImport As Date)         As PreparationDate " _
            & " FROM " _
            & " Import " _
            & " WHERE extra > 0 AND  Cast(DateImport As Date) = '" & pdPreparationDate & "' AND IDPeriode = " & go_Globals.IDPeriode & " AND OrganisationID=" & piOrganisationID _
            & " GROUP BY Cast(DateImport As Date) , OrganisationID "


        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()

        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Erreur")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function


    Public Function SelectDeliveryByDatePrixFixe(pdDateFrom As DateTime, pdDateTo As DateTime, piOrganisationID As Integer) As DataTable


        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & " AVG(Montant) as Montant, " _
            & " Count(ImportID)                        As TotalLivraison, " _
            & " SUM(extra)                      As totalExtra, " _
            & " SUM(debit)                      As totalDebit, " _
            & " SUM(credit)                     As TotalCredit, " _
            & " SUM(nombreglacier)              As TotalGlacier, " _
            & " SUM(montantglacierorganisation) As TotalMontantGlacierOrganisation, " _
            & " Round(SUM(distance), 1)         As TotalKM, " _
            & " SUM(montant)                    As TotalCommande, " _
            & " SUM(ExtraKM)                    As TOTALExtraKM, " _
            & " Cast(DateImport As Date)         As PreparationDate " _
            & " FROM " _
            & " Import " _
            & " WHERE (DateImport  >= '" & pdDateFrom.ToString & "' AND DateImport <= '" & pdDateTo.ToString & "') AND IDPeriode = " & go_Globals.IDPeriode & " AND OrganisationID=" & piOrganisationID _
            & " GROUP BY CAST(DateImport AS DATE), Organisation "


        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()

        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Erreur")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function

    Public Function SelectDeliveryByDate(pdDateFrom As DateTime, pdDateTo As DateTime, piOrganisationID As Integer) As DataTable


        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & " AVG(Montant) as Montant, " _
            & " Count(ImportID)                        As TotalLivraison, " _
            & " SUM(extra)                      As totalExtra, " _
            & " SUM(debit)                      As totalDebit, " _
            & " SUM(credit)                     As TotalCredit, " _
            & " SUM(nombreglacier)              As TotalGlacier, " _
            & " SUM(montantglacierorganisation) As TotalMontantGlacierOrganisation, " _
            & " Round(SUM(distance), 1)         As TotalKM, " _
            & " SUM(montant)                    As TotalCommande, " _
            & " SUM(ExtraKM)                    As TOTALExtraKM, " _
            & " Cast(DateImport As Date)         As PreparationDate " _
            & " FROM " _
            & " Import " _
            & " WHERE (DateImport  >= '" & pdDateFrom.ToString & "' AND DateImport <= '" & pdDateTo.ToString & "') AND IDPeriode = " & go_Globals.IDPeriode & " AND OrganisationID=" & piOrganisationID _
            & " GROUP BY CAST(DateImport AS DATE), Organisation "


        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()

        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Erreur")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function

    Public Function SelectTotalAmount(pdDateFrom As DateTime, pdDateTo As DateTime, piOrganisationID As Integer) As Double

        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "    SUM([Import].[Amount]) as TotalMontant " _
            & "FROM " _
            & "     [Import] " _
            & "WHERE (DateImport  >= '" & pdDateFrom.ToString & "' AND DateImport <= '" & pdDateTo.ToString & "') AND IDPeriode = " & go_Globals.IDPeriode & " AND OrganisationID=" & piOrganisationID

        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Dim drImport As DataRow
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            Dim dTotalAmount As Double = 0
            If reader.HasRows Then
                dt.Load(reader)
                If dt.Rows.Count > 0 Then
                    For Each drImport In dt.Rows
                        dTotalAmount = dTotalAmount + If(IsDBNull(drImport("TotalMontant")), 0, drImport("TotalMontant"))
                    Next
                    Return dTotalAmount
                Else
                    Return 0
                End If

            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Erreur")
        Finally
            connection.Close()
        End Try

    End Function


    Public Function SelectTotalCredit(pdDateFrom As DateTime, pdDateTo As DateTime, piOrganisationID As Integer) As Double


        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "    SUM([Credit]) as TotalMontant " _
            & "FROM " _
            & "     [Import] " _
            & "WHERE  IDPeriode = " & go_Globals.IDPeriode & " AND OrganisationID=" & piOrganisationID & " And Echange = 1"


        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Dim drImport As DataRow
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            Dim dTotalAmount As Double = 0
            If reader.HasRows Then
                dt.Load(reader)
                If dt.Rows.Count > 0 Then
                    For Each drImport In dt.Rows
                        dTotalAmount = dTotalAmount + If(IsDBNull(drImport("TotalMontant")), 0, drImport("TotalMontant"))
                    Next
                    Return dTotalAmount
                Else
                    Return 0
                End If

            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Erreur")
        Finally
            connection.Close()
        End Try

    End Function

    Public Function SelectQteeLivraison(pdDateFrom As DateTime, pdDateTo As DateTime, piOrganisationID As Integer) As Integer

        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "Select " _
            & "    count(*) As TotalLivraisons " _
            & "FROM " _
            & "     [Import] " _
            & "WHERE (DateImport  >= '" & pdDateFrom.ToString & "' AND DateImport <= '" & pdDateTo.ToString & "') AND IDPeriode = " & go_Globals.IDPeriode & " AND OrganisationID=" & piOrganisationID

        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Dim drImport As DataRow
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            Dim dTotalAmount As Double = 0
            If reader.HasRows Then
                dt.Load(reader)
                If dt.Rows.Count > 0 Then
                    For Each drImport In dt.Rows
                        dTotalAmount = dTotalAmount + If(IsDBNull(drImport("TotalLivraisons")), 0, drImport("TotalLivraisons"))
                    Next
                    Return dTotalAmount
                Else
                    Return 0
                End If

            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Erreur")
        Finally
            connection.Close()
        End Try

    End Function


    Public Function SelectTotalDebit(pdDateFrom As DateTime, pdDateTo As DateTime, piOrganisationID As Integer) As Double

        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "    SUM([Import].[Debit]) as TotalMontant " _
            & "FROM " _
            & "     [Import] " _
            & "WHERE (DateImport  >= '" & pdDateFrom.ToString & "' AND DateImport <= '" & pdDateTo.ToString & "') AND IDPeriode = " & go_Globals.IDPeriode & " AND OrganisationID=" & piOrganisationID _
            & " AND Debit > 0"

        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Dim drImport As DataRow
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            Dim dTotalAmount As Double = 0
            If reader.HasRows Then
                dt.Load(reader)
                If dt.Rows.Count > 0 Then
                    For Each drImport In dt.Rows
                        dTotalAmount = dTotalAmount + If(IsDBNull(drImport("TotalMontant")), 0, drImport("TotalMontant"))
                    Next
                    Return dTotalAmount
                Else
                    Return 0
                End If

            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Erreur")
        Finally
            connection.Close()
        End Try

    End Function





    Public Function SelectTotalGlacier(pdDateFrom As DateTime, pdDateTo As DateTime, piOrganisationID As Integer) As Integer

        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "    count([Import].[NombreGlacier]) as TotalGlacier " _
            & "FROM " _
            & "     [Import] " _
            & "WHERE (DateImport  >= '" & pdDateFrom.ToString & "' AND DateImport <= '" & pdDateTo.ToString & "') AND IDPeriode = " & go_Globals.IDPeriode & " AND OrganisationID=" & piOrganisationID

        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Dim drImport As DataRow
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            Dim dTotalAmount As Double = 0
            If reader.HasRows Then
                dt.Load(reader)
                If dt.Rows.Count > 0 Then
                    For Each drImport In dt.Rows
                        dTotalAmount = dTotalAmount + If(IsDBNull(drImport("TotalGlacier")), 0, drImport("TotalGlacier"))
                    Next
                    Return dTotalAmount
                Else
                    Return 0
                End If

            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Erreur")
        Finally
            connection.Close()
        End Try

    End Function
    Public Function SelectLivreurFromImport(pdDateFrom As DateTime, pdDateTo As DateTime, piOrganisationID As Integer) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT  DISTINCT" _
            & "    [Import].[Livreur], Organisation " _
            & "FROM " _
            & "     [Import]  WHERE (DateImport  >= '" & pdDateFrom.ToString & "' AND DateImport <= '" & pdDateTo.ToString & "') AND IDPeriode = " & go_Globals.IDPeriode & " AND  OrganisationID=" & piOrganisationID

        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Erreur")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function


    Public Function SelectAllImportFromPeriode(pIDPeriode As Integer) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectProcedure As String = "[SelectAllImportFromPeriode]"
        Dim selectCommand As New SqlCommand(selectProcedure, connection)
        selectCommand.CommandType = CommandType.StoredProcedure
        selectCommand.CommandType = CommandType.StoredProcedure
        selectCommand.Parameters.AddWithValue("@IDPeriode", pIDPeriode)
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function

    Public Function RapportdeLivraisonParLivreur(pdDateFrom As DateTime, pdDateTo As DateTime, piLivreurID As Integer) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT dbo.livreurs.Livreur, " _
            & " dbo.import.heurelivraison, " _
            & " dbo.import.IDClientAddresse,  " _
            & " dbo.import.ImportID,  " _
            & " dbo.import.client,  " _
            & " dbo.import.clientaddress, " _
            & " dbo.import.DateImport " _
            & " FROM  dbo.import " _
            & " LEFT OUTER JOIN dbo.livreurs  " _
            & " ON dbo.import.LivreurID =  " _
            & " dbo.livreurs.LivreurID  " _
            & " WHERE dbo.import.LivreurID = " & piLivreurID & " And (dbo.import.DateImport  >= '" & pdDateFrom & "' AND dbo.import.DateImport <= '" & pdDateTo & "')  AND (dbo.import.IsMobilus = 0) AND (dbo.import.IDPeriode = " & go_Globals.IDPeriode & ")" _
            & " ORDER  BY dbo.import.heurelivraison  "




        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Erreur")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function
    Public Function RapportdeLivraisonParRestaurant(pdDateFrom As DateTime, pdDateTo As DateTime, piOrganisationID As Integer) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT dbo.organisations.organisation, " _
            & " dbo.import.heurelivraison, " _
            & " dbo.import.IDClientAddresse,  " _
            & " dbo.import.ImportID,  " _
            & " dbo.import.client,  " _
            & " dbo.import.clientaddress, " _
            & " dbo.import.DateImport, " _
            & " dbo.import.livreur  " _
            & " FROM  import " _
            & " LEFT OUTER JOIN dbo.organisations  " _
            & " ON dbo.import.organisationid =  " _
            & " dbo.organisations.organisationid  " _
            & " WHERE dbo.import.OrganisationID = " & piOrganisationID & " And (dbo.import.DateImport  >= '" & pdDateFrom & "' AND dbo.import.DateImport <= '" & pdDateTo & "')  AND dbo.import.IsMobilus = 0 ORDER BY dbo.import.DateImport"


        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Erreur")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function

    Public Function SelectAll() As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "Select " _
            & "     [ImportID] " _
            & "    ,[Reference] " _
            & "    ,[Code] " _
            & "    ,[Status] " _
            & "    ,[Client] " _
            & "    ,[ClientAddress] " _
            & "    ,[MomentDePassage] " _
            & "    ,[PreparePar] " _
            & "    ,[HeureAppel] " _
            & "    ,[HeurePrep] " _
            & "    ,[HeureDepart] " _
            & "    ,[HeureLivraison] " _
            & "    ,[MessagePassage] " _
            & "    ,[TotalTempService] " _
            & "    ,[Livreur] " _
            & "    ,[Distance] " _
            & "    ,[Montant] " _
            & "    ,[Extra] " _
            & "    ,[Organisation] " _
            & "    ,[OrganisationAddress] " _
            & "    ,[IDNomFichierImport] " _
            & "    ,[Debit] " _
            & "    ,[Credit] " _
            & "    ,[Glacier] " _
            & "    ,[NombreGlacier] " _
            & "    ,[MontantContrat] " _
            & "    ,[OrganisationID] " _
            & "    ,[LivreurID] " _
            & "    ,[MontantLivreur] " _
            & "    ,[MontantGlacierOrganisation] " _
            & "    ,[MontantGlacierLivreur] " _
            & "    ,[MontantLivreurFinal] " _
            & "    ,[MontantLivreurPharmaplus] " _
            & "    ,[ExtraKM] " _
            & "    ,[Echange] " _
            & "    ,[IDPeriode] " _
            & "    ,[ExtraLivreur] " _
            & "    ,[ExtraKMLivreur] " _
            & "FROM " _
            & "     [Import] " _
            & ""

        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Erreur")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function
    Public Function SelectAllBackupFromPeriode(pIDPeriode As Integer) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "Select " _
            & "     [ImportID] " _
            & "    ,[Reference] " _
            & "    ,[Code] " _
            & "    ,[Status] " _
            & "    ,[Client] " _
            & "    ,[ClientAddress] " _
            & "    ,[MomentDePassage] " _
            & "    ,[PreparePar] " _
            & "    ,[HeureAppel] " _
            & "    ,[HeurePrep] " _
            & "    ,[HeureDepart] " _
            & "    ,[HeureLivraison] " _
            & "    ,[MessagePassage] " _
            & "    ,[TotalTempService] " _
            & "    ,[Livreur] " _
            & "    ,[Distance] " _
            & "    ,[Montant] " _
            & "    ,[Extra] " _
            & "    ,[Organisation] " _
            & "    ,[OrganisationAddress] " _
            & "    ,[IDNomFichierImport] " _
            & "    ,[Debit] " _
            & "    ,[Credit] " _
            & "    ,[Glacier] " _
            & "    ,[NombreGlacier] " _
            & "    ,[MontantContrat] " _
            & "    ,[OrganisationID] " _
            & "    ,[LivreurID] " _
            & "    ,[MontantLivreur] " _
            & "    ,[MontantGlacierOrganisation] " _
            & "    ,[MontantGlacierLivreur] " _
            & "    ,[MontantLivreurFinal] " _
            & "    ,[MontantLivreurPharmaplus] " _
            & "    ,[ExtraKM] " _
            & "    ,[Echange] " _
            & "    ,[IDPeriode] " _
            & "    ,[ExtraLivreur] " _
            & "    ,[ExtraKMLivreur] " _
            & "    ,[IDSAmount] " _
            & "    ,[DateImport] " _
            & "    ,[IsMobilus] " _
            & "    ,[IDClientAddresse] " _
            & "FROM " _
            & "     [ImportBackup] " _
            & " WHERE IDPeriode =" & pIDPeriode

        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Erreur")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function

    Public Function SelectAllParjourLivreurPharmaplus(pdDateFrom As DateTime, pdDateTo As DateTime, piOrganisationID As Integer, piLivreurID As Integer) As DataTable

        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "Select " _
            & " AVG(Montant) As Montant, " _
            & " Count(*)                        As TotalLivraison, " _
            & " SUM(extra)                      As totalExtra, " _
            & " SUM(debit)                      As totalDebit, " _
            & " SUM(credit)                     As TotalCredit, " _
            & " SUM(nombreglacier)              As TotalGlacier, " _
            & " SUM(montantglacierorganisation) As TotalMontantGlacierOrganisation, " _
            & " Round(SUM(distance), 1)         As TotalKM, " _
            & " SUM(montant)                    As TotalCommande, " _
            & " SUM(ExtraKM)                    As TOTALExtraKM, " _
            & " Cast(DateImport As Date)         As PreparationDate " _
            & " FROM " _
            & " Import " _
            & " WHERE (DateImport  >= '" & pdDateFrom.ToString & "' AND DateImport <= '" & pdDateTo.ToString & "') AND IDPeriode = " & go_Globals.IDPeriode & " AND OrganisationID=" & piOrganisationID _
            & " GROUP BY CAST(DateImport AS DATE), Organisation "
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Erreur")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function



    Public Function SelectAllFromDateNameLivreurPharmaplus(pdDateFrom As DateTime, pdDateTo As DateTime, piOrganisationID As Integer, piLivreurID As Integer) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [ImportID] " _
            & "    ,[Reference] " _
            & "    ,[Code] " _
            & "    ,[Status] " _
            & "    ,[Client] " _
            & "    ,[ClientAddress] " _
            & "    ,[MomentDePassage] " _
            & "    ,[PreparePar] " _
            & "    ,[HeureAppel] " _
            & "    ,[HeurePrep] " _
            & "    ,[HeureDepart] " _
            & "    ,[HeureLivraison] " _
            & "    ,[MessagePassage] " _
            & "    ,[TotalTempService] " _
            & "    ,[Livreur] " _
            & "    ,[Distance] " _
            & "    ,[Montant] " _
            & "    ,[Extra] " _
            & "    ,[Organisation] " _
            & "    ,[OrganisationAddress] " _
            & "    ,[IDNomFichierImport] " _
            & "    ,[Debit] " _
            & "    ,[Credit] " _
            & "    ,[Glacier] " _
            & "    ,[NombreGlacier] " _
            & "    ,[MontantContrat] " _
            & "    ,[OrganisationID] " _
            & "    ,[LivreurID] " _
            & "    ,[MontantLivreur] " _
            & "    ,[MontantGlacierOrganisation] " _
            & "    ,[MontantGlacierLivreur] " _
            & "    ,[MontantLivreurFinal] " _
            & "    ,[MontantLivreurPharmaplus] " _
            & "    ,[ExtraKM] " _
            & "    ,[Echange] " _
            & "    ,[IDPeriode] " _
            & "    ,[ExtraLivreur] " _
            & "    ,[ExtraKMLivreur] " _
            & "    ,[IDSAmount] " _
            & "    ,[DateImport] " _
            & "    ,[IsMobilus] " _
            & "    ,[IDClientAddresse] " _
            & "FROM " _
            & "     [Import] " _
            & "WHERE (DateImport  >= '" & pdDateFrom.ToString & "' AND DateImport <= '" & pdDateTo.ToString & "') AND IDPeriode = " & go_Globals.IDPeriode & " AND OrganisationID=" & piOrganisationID & " AND LivreurID=" & piLivreurID _
            & " ORDER BY MontantLivreurFinal DESC"
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Erreur")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function

    Public Function SelectAllFromLivreur(piLivreurID As Integer) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [ImportID] " _
            & "    ,[Reference] " _
            & "    ,[Code] " _
            & "    ,[Status] " _
            & "    ,[Client] " _
            & "    ,[ClientAddress] " _
            & "    ,[MomentDePassage] " _
            & "    ,[PreparePar] " _
            & "    ,[HeureAppel] " _
            & "    ,[HeurePrep] " _
            & "    ,[HeureDepart] " _
            & "    ,[HeureLivraison] " _
            & "    ,[MessagePassage] " _
            & "    ,[TotalTempService] " _
            & "    ,[Livreur] " _
            & "    ,[Distance] " _
            & "    ,[Montant] " _
            & "    ,[Extra] " _
            & "    ,[Organisation] " _
            & "    ,[OrganisationAddress] " _
            & "    ,[IDNomFichierImport] " _
            & "    ,[Debit] " _
            & "    ,[Credit] " _
            & "    ,[Glacier] " _
            & "    ,[NombreGlacier] " _
            & "    ,[MontantContrat] " _
            & "    ,[OrganisationID] " _
            & "    ,[LivreurID] " _
            & "    ,[MontantLivreur] " _
            & "    ,[MontantGlacierOrganisation] " _
            & "    ,[MontantGlacierLivreur] " _
            & "    ,[MontantLivreurFinal] " _
            & "    ,[MontantLivreurPharmaplus] " _
            & "    ,[ExtraKM] " _
            & "    ,[Echange] " _
            & "    ,[IDPeriode] " _
            & "    ,[ExtraLivreur] " _
            & "    ,[ExtraKMLivreur] " _
            & "    ,[IDSAmount] " _
            & "    ,[DateImport] " _
            & "    ,[IsMobilus] " _
            & "    ,[IDClientAddresse] " _
            & "FROM " _
            & "     [Import] " _
            & "WHERE  IDPeriode = " & go_Globals.IDPeriode & " AND LivreurID=" & piLivreurID & " AND Distance > 0"
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Erreur")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function


    Public Function SelectAllFromDateNameLivreur(pdDateFrom As DateTime, pdDateTo As DateTime, piOrganisationID As Integer, piLivreurID As Integer) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [ImportID] " _
            & "    ,[Reference] " _
            & "    ,[Code] " _
            & "    ,[Status] " _
            & "    ,[Client] " _
            & "    ,[ClientAddress] " _
            & "    ,[MomentDePassage] " _
            & "    ,[PreparePar] " _
            & "    ,[HeureAppel] " _
            & "    ,[HeurePrep] " _
            & "    ,[HeureDepart] " _
            & "    ,[HeureLivraison] " _
            & "    ,[MessagePassage] " _
            & "    ,[TotalTempService] " _
            & "    ,[Livreur] " _
            & "    ,[Distance] " _
            & "    ,[Montant] " _
            & "    ,[Extra] " _
            & "    ,[Organisation] " _
            & "    ,[OrganisationAddress] " _
            & "    ,[IDNomFichierImport] " _
            & "    ,[Debit] " _
            & "    ,[Credit] " _
            & "    ,[Glacier] " _
            & "    ,[NombreGlacier] " _
            & "    ,[MontantContrat] " _
            & "    ,[OrganisationID] " _
            & "    ,[LivreurID] " _
            & "    ,[MontantLivreur] " _
            & "    ,[MontantGlacierOrganisation] " _
            & "    ,[MontantGlacierLivreur] " _
            & "    ,[MontantLivreurFinal] " _
            & "    ,[MontantLivreurPharmaplus] " _
            & "    ,[ExtraKM] " _
            & "    ,[Echange] " _
            & "    ,[IDPeriode] " _
            & "    ,[ExtraLivreur] " _
            & "    ,[ExtraKMLivreur] " _
            & "    ,[IDSAmount] " _
            & "    ,[DateImport] " _
            & "    ,[IsMobilus] " _
            & "    ,[IDClientAddresse] " _
            & "FROM " _
            & "     [Import] " _
            & "WHERE (DateImport  >= '" & pdDateFrom.ToString & "' AND DateImport <= '" & pdDateTo.ToString & "') AND IDPeriode = " & go_Globals.IDPeriode & " AND OrganisationID=" & piOrganisationID & " AND LivreurID=" & piLivreurID
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Erreur")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function
    Public Function SelectAllFromDateName(pdDateFrom As DateTime, pdDateTo As DateTime, piOrganisationID As Integer) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
             & "     [ImportID] " _
            & "    ,[Reference] " _
            & "    ,[Code] " _
            & "    ,[Status] " _
            & "    ,[Client] " _
            & "    ,[ClientAddress] " _
            & "    ,[MomentDePassage] " _
            & "    ,[PreparePar] " _
            & "    ,[HeureAppel] " _
            & "    ,[HeurePrep] " _
            & "    ,[HeureDepart] " _
            & "    ,[HeureLivraison] " _
            & "    ,[MessagePassage] " _
            & "    ,[TotalTempService] " _
            & "    ,[Livreur] " _
            & "    ,[Distance] " _
            & "    ,[Montant] " _
            & "    ,[Extra] " _
            & "    ,[Organisation] " _
            & "    ,[OrganisationAddress] " _
            & "    ,[IDNomFichierImport] " _
            & "    ,[Debit] " _
            & "    ,[Credit] " _
            & "    ,[Glacier] " _
            & "    ,[NombreGlacier] " _
            & "    ,[MontantContrat] " _
            & "    ,[OrganisationID] " _
            & "    ,[LivreurID] " _
            & "    ,[MontantLivreur] " _
            & "    ,[MontantGlacierOrganisation] " _
            & "    ,[MontantGlacierLivreur] " _
            & "    ,[MontantLivreurFinal] " _
            & "    ,[MontantLivreurPharmaplus] " _
            & "    ,[ExtraKM] " _
            & "    ,[Echange] " _
            & "    ,[IDPeriode] " _
            & "    ,[ExtraLivreur] " _
            & "    ,[ExtraKMLivreur] " _
            & "    ,[IDSAmount] " _
            & "    ,[DateImport] " _
            & "    ,[IsMobilus] " _
            & "    ,[IDClientAddresse] " _
            & "FROM " _
            & "     [Import] " _
            & "WHERE (DateImport  >= '" & pdDateFrom.ToString & "' AND DateImport <= '" & pdDateTo.ToString & "') AND IDPeriode = " & go_Globals.IDPeriode & " AND OrganisationID=" & piOrganisationID & ""
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Erreur")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function
    Public Function SelectAllFromDateOrganisationLivreur(pdDateFrom As DateTime, pdDateTo As DateTime, piOrganisationID As Integer, piLivreurID As Integer) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [ImportID] " _
            & "    ,[Reference] " _
            & "    ,[Code] " _
            & "    ,[Status] " _
            & "    ,[Client] " _
            & "    ,[ClientAddress] " _
            & "    ,[MomentDePassage] " _
            & "    ,[PreparePar] " _
            & "    ,[HeureAppel] " _
            & "    ,[HeurePrep] " _
            & "    ,[HeureDepart] " _
            & "    ,[HeureLivraison] " _
            & "    ,[MessagePassage] " _
            & "    ,[TotalTempService] " _
            & "    ,[Livreur] " _
            & "    ,[Distance] " _
            & "    ,[Montant] " _
            & "    ,[Extra] " _
            & "    ,[Organisation] " _
            & "    ,[OrganisationAddress] " _
            & "    ,[IDNomFichierImport] " _
            & "    ,[Debit] " _
            & "    ,[Credit] " _
            & "    ,[Glacier] " _
            & "    ,[NombreGlacier] " _
            & "    ,[MontantContrat] " _
            & "    ,[OrganisationID] " _
            & "    ,[LivreurID] " _
            & "    ,[MontantLivreur] " _
            & "    ,[MontantGlacierOrganisation] " _
            & "    ,[MontantGlacierLivreur] " _
            & "    ,[MontantLivreurFinal] " _
            & "    ,[MontantLivreurPharmaplus] " _
            & "    ,[ExtraKM] " _
            & "    ,[Echange] " _
            & "    ,[IDPeriode] " _
            & "    ,[ExtraLivreur] " _
            & "    ,[ExtraKMLivreur] " _
            & "    ,[IDSAmount] " _
            & "    ,[DateImport] " _
            & "    ,[IsMobilus] " _
            & "    ,[IDClientAddresse] " _
            & "FROM " _
            & "     [Import] " _
            & "WHERE (DateImport  >= '" & pdDateFrom.ToString & "' AND DateImport <= '" & pdDateTo.ToString & "') AND IDPeriode = " & go_Globals.IDPeriode _
            & " AND OrganisationID=" & piOrganisationID _
            & " AND LivreurID = " & piLivreurID

        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Erreur")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function


    Public Function SelectDistincResidences(pdDateFrom As DateTime, piOrganisationID As Integer) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT DISTINCT " _
            & "    [IDResidence] " _
            & "    ,[Residence] " _
            & "FROM " _
            & "     [Import] " _
            & "WHERE (DateImport  = '" & pdDateFrom & "') " & " AND IDPeriode = " & go_Globals.IDPeriode _
            & " AND OrganisationID=" & piOrganisationID & " AND IDResidence IS NOT NULL"

        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Erreur")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function

    Public Function SelectAllFromDateOrganisationNOResidenceLivreur(pdDateFrom As DateTime, piOrganisationID As Integer, piLivreurID As Integer) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [ImportID] " _
            & "    ,[Reference] " _
            & "    ,[Code] " _
            & "    ,[Status] " _
            & "    ,[Client] " _
            & "    ,[ClientAddress] " _
            & "    ,[MomentDePassage] " _
            & "    ,[PreparePar] " _
            & "    ,[HeureAppel] " _
            & "    ,[HeurePrep] " _
            & "    ,[HeureDepart] " _
            & "    ,[HeureLivraison] " _
            & "    ,[MessagePassage] " _
            & "    ,[TotalTempService] " _
            & "    ,[Livreur] " _
            & "    ,[Distance] " _
            & "    ,[Montant] " _
            & "    ,[Extra] " _
            & "    ,[Organisation] " _
            & "    ,[OrganisationAddress] " _
            & "    ,[IDNomFichierImport] " _
            & "    ,[Debit] " _
            & "    ,[Credit] " _
            & "    ,[Glacier] " _
            & "    ,[NombreGlacier] " _
            & "    ,[MontantContrat] " _
            & "    ,[OrganisationID] " _
            & "    ,[LivreurID] " _
            & "    ,[MontantLivreur] " _
            & "    ,[MontantGlacierOrganisation] " _
            & "    ,[MontantGlacierLivreur] " _
            & "    ,[MontantLivreurFinal] " _
            & "    ,[MontantLivreurPharmaplus] " _
            & "    ,[ExtraKM] " _
            & "    ,[Echange] " _
            & "    ,[IDPeriode] " _
            & "    ,[ExtraLivreur] " _
            & "    ,[ExtraKMLivreur] " _
            & "    ,[IDSAmount] " _
            & "    ,[DateImport] " _
            & "    ,[IsMobilus] " _
            & "    ,[IDClientAddresse] " _
            & "    ,[IDResidence] " _
            & "    ,[Residence] " _
            & "FROM " _
            & "     [Import] " _
            & "WHERE (DateImport  = '" & pdDateFrom.ToString & "') AND IDPeriode = " & go_Globals.IDPeriode _
            & " AND OrganisationID=" & piOrganisationID & " AND IDResidence IS NULL AND Residence IS NULL AND LivreurID=" & piLivreurID


        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Erreur")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function

    Public Function SelectAllFromDateOrganisationNOResidence(pdDateFrom As DateTime, piOrganisationID As Integer) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [ImportID] " _
            & "    ,[Reference] " _
            & "    ,[Code] " _
            & "    ,[Status] " _
            & "    ,[Client] " _
            & "    ,[ClientAddress] " _
            & "    ,[MomentDePassage] " _
            & "    ,[PreparePar] " _
            & "    ,[HeureAppel] " _
            & "    ,[HeurePrep] " _
            & "    ,[HeureDepart] " _
            & "    ,[HeureLivraison] " _
            & "    ,[MessagePassage] " _
            & "    ,[TotalTempService] " _
            & "    ,[Livreur] " _
            & "    ,[Distance] " _
            & "    ,[Montant] " _
            & "    ,[Extra] " _
            & "    ,[Organisation] " _
            & "    ,[OrganisationAddress] " _
            & "    ,[IDNomFichierImport] " _
            & "    ,[Debit] " _
            & "    ,[Credit] " _
            & "    ,[Glacier] " _
            & "    ,[NombreGlacier] " _
            & "    ,[MontantContrat] " _
            & "    ,[OrganisationID] " _
            & "    ,[LivreurID] " _
            & "    ,[MontantLivreur] " _
            & "    ,[MontantGlacierOrganisation] " _
            & "    ,[MontantGlacierLivreur] " _
            & "    ,[MontantLivreurFinal] " _
            & "    ,[MontantLivreurPharmaplus] " _
            & "    ,[ExtraKM] " _
            & "    ,[Echange] " _
            & "    ,[IDPeriode] " _
            & "    ,[ExtraLivreur] " _
            & "    ,[ExtraKMLivreur] " _
            & "    ,[IDSAmount] " _
            & "    ,[DateImport] " _
            & "    ,[IsMobilus] " _
            & "    ,[IDClientAddresse] " _
            & "    ,[IDResidence] " _
            & "    ,[Residence] " _
            & "FROM " _
            & "     [Import] " _
            & "WHERE (DateImport  = '" & pdDateFrom.ToString & "') AND IDPeriode = " & go_Globals.IDPeriode _
            & " AND OrganisationID=" & piOrganisationID & " AND IDResidence IS NULL AND Residence IS NULL"



        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Erreur")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function

    Public Function SelectAllFromDateOrganisationResidenceLivreur(pdDateFrom As DateTime, piOrganisationID As Integer, piIDResidence As Integer, piLivreurID As Integer) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [ImportID] " _
            & "    ,[Reference] " _
            & "    ,[Code] " _
            & "    ,[Status] " _
            & "    ,[Client] " _
            & "    ,[ClientAddress] " _
            & "    ,[MomentDePassage] " _
            & "    ,[PreparePar] " _
            & "    ,[HeureAppel] " _
            & "    ,[HeurePrep] " _
            & "    ,[HeureDepart] " _
            & "    ,[HeureLivraison] " _
            & "    ,[MessagePassage] " _
            & "    ,[TotalTempService] " _
            & "    ,[Livreur] " _
            & "    ,[Distance] " _
            & "    ,[Montant] " _
            & "    ,[Extra] " _
            & "    ,[Organisation] " _
            & "    ,[OrganisationAddress] " _
            & "    ,[IDNomFichierImport] " _
            & "    ,[Debit] " _
            & "    ,[Credit] " _
            & "    ,[Glacier] " _
            & "    ,[NombreGlacier] " _
            & "    ,[MontantContrat] " _
            & "    ,[OrganisationID] " _
            & "    ,[LivreurID] " _
            & "    ,[MontantLivreur] " _
            & "    ,[MontantGlacierOrganisation] " _
            & "    ,[MontantGlacierLivreur] " _
            & "    ,[MontantLivreurFinal] " _
            & "    ,[MontantLivreurPharmaplus] " _
            & "    ,[ExtraKM] " _
            & "    ,[Echange] " _
            & "    ,[IDPeriode] " _
            & "    ,[ExtraLivreur] " _
            & "    ,[ExtraKMLivreur] " _
            & "    ,[IDSAmount] " _
            & "    ,[DateImport] " _
            & "    ,[IsMobilus] " _
            & "    ,[IDClientAddresse] " _
            & "    ,[IDResidence] " _
            & "    ,[Residence] " _
            & "FROM " _
            & "     [Import] " _
            & "WHERE (DateImport  = '" & pdDateFrom.ToString & "') AND IDPeriode = " & go_Globals.IDPeriode _
            & " AND OrganisationID=" & piOrganisationID & " AND IDResidence =" & piIDResidence & " AND LivreurID=" & piLivreurID


        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Erreur")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function
    Public Function SelectAllFromDateOrganisationResidence(pdDateFrom As DateTime, piOrganisationID As Integer, piIDResidence As Integer) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [ImportID] " _
            & "    ,[Reference] " _
            & "    ,[Code] " _
            & "    ,[Status] " _
            & "    ,[Client] " _
            & "    ,[ClientAddress] " _
            & "    ,[MomentDePassage] " _
            & "    ,[PreparePar] " _
            & "    ,[HeureAppel] " _
            & "    ,[HeurePrep] " _
            & "    ,[HeureDepart] " _
            & "    ,[HeureLivraison] " _
            & "    ,[MessagePassage] " _
            & "    ,[TotalTempService] " _
            & "    ,[Livreur] " _
            & "    ,[Distance] " _
            & "    ,[Montant] " _
            & "    ,[Extra] " _
            & "    ,[Organisation] " _
            & "    ,[OrganisationAddress] " _
            & "    ,[IDNomFichierImport] " _
            & "    ,[Debit] " _
            & "    ,[Credit] " _
            & "    ,[Glacier] " _
            & "    ,[NombreGlacier] " _
            & "    ,[MontantContrat] " _
            & "    ,[OrganisationID] " _
            & "    ,[LivreurID] " _
            & "    ,[MontantLivreur] " _
            & "    ,[MontantGlacierOrganisation] " _
            & "    ,[MontantGlacierLivreur] " _
            & "    ,[MontantLivreurFinal] " _
            & "    ,[MontantLivreurPharmaplus] " _
            & "    ,[ExtraKM] " _
            & "    ,[Echange] " _
            & "    ,[IDPeriode] " _
            & "    ,[ExtraLivreur] " _
            & "    ,[ExtraKMLivreur] " _
            & "    ,[IDSAmount] " _
            & "    ,[DateImport] " _
            & "    ,[IsMobilus] " _
            & "    ,[IDClientAddresse] " _
            & "    ,[IDResidence] " _
            & "    ,[Residence] " _
            & "FROM " _
            & "     [Import] " _
            & "WHERE (DateImport  = '" & pdDateFrom.ToString & "') AND IDPeriode = " & go_Globals.IDPeriode _
            & " AND OrganisationID=" & piOrganisationID & " AND IDResidence =" & piIDResidence

        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Erreur")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function
    Public Function SelectAllFromDateOrganisation(pdDateFrom As DateTime, pdDateTo As DateTime, piOrganisationID As Integer) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [ImportID] " _
            & "    ,[Reference] " _
            & "    ,[Code] " _
            & "    ,[Status] " _
            & "    ,[Client] " _
            & "    ,[ClientAddress] " _
            & "    ,[MomentDePassage] " _
            & "    ,[PreparePar] " _
            & "    ,[HeureAppel] " _
            & "    ,[HeurePrep] " _
            & "    ,[HeureDepart] " _
            & "    ,[HeureLivraison] " _
            & "    ,[MessagePassage] " _
            & "    ,[TotalTempService] " _
            & "    ,[Livreur] " _
            & "    ,[Distance] " _
            & "    ,[Montant] " _
            & "    ,[Extra] " _
            & "    ,[Organisation] " _
            & "    ,[OrganisationAddress] " _
            & "    ,[IDNomFichierImport] " _
            & "    ,[Debit] " _
            & "    ,[Credit] " _
            & "    ,[Glacier] " _
            & "    ,[NombreGlacier] " _
            & "    ,[MontantContrat] " _
            & "    ,[OrganisationID] " _
            & "    ,[LivreurID] " _
            & "    ,[MontantLivreur] " _
            & "    ,[MontantGlacierOrganisation] " _
            & "    ,[MontantGlacierLivreur] " _
            & "    ,[MontantLivreurFinal] " _
            & "    ,[MontantLivreurPharmaplus] " _
            & "    ,[ExtraKM] " _
            & "    ,[Echange] " _
            & "    ,[IDPeriode] " _
            & "    ,[ExtraLivreur] " _
            & "    ,[ExtraKMLivreur] " _
            & "    ,[IDSAmount] " _
            & "    ,[DateImport] " _
            & "    ,[IsMobilus] " _
            & "    ,[IDClientAddresse] " _
            & "    ,[IDResidence] " _
            & "    ,[Residence] " _
            & "FROM " _
            & "     [Import] " _
            & "WHERE (DateImport  >= '" & pdDateFrom.ToString & "' AND DateImport <= '" & pdDateTo.ToString & "') AND IDPeriode = " & go_Globals.IDPeriode _
            & " AND OrganisationID=" & piOrganisationID

        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Erreur")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function
    Public Function SelectAllFromDate(pdDateFrom As DateTime, pdDateTo As DateTime) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     Import.ImportID " _
            & "    ,Import.Reference " _
            & "    ,Import.Code " _
            & "    ,Import.Status " _
            & "    ,Import.Client " _
            & "    ,Import.ClientAddress " _
            & "    ,Import.MomentDePassage " _
            & "    ,Import.PreparePar " _
            & "    ,Import.HeureAppel " _
            & "    ,Import.HeurePrep " _
            & "    ,Import.HeureDepart " _
            & "    ,Import.HeureLivraison " _
            & "    ,Import.MessagePassage " _
            & "    ,Import.TotalTempService " _
            & "    ,Livreurs.Livreur " _
            & "    ,Import.Distance " _
            & "    ,Import.Montant " _
            & "    ,Import.Extra " _
            & "    ,Import.Organisation " _
            & "    ,Import.OrganisationAddress " _
            & "    ,Import.IDNomFichierImport " _
            & "    ,Import.Debit " _
            & "    ,Import.Credit " _
            & "    ,Import.Glacier " _
            & "    ,Import.NombreGlacier " _
            & "    ,Import.MontantContrat " _
            & "    ,Import.OrganisationID " _
            & "    ,Livreurs.LivreurID " _
            & "    ,Import.MontantLivreur " _
            & "    ,Import.MontantGlacierOrganisation " _
            & "    ,Import.MontantGlacierLivreur " _
            & "    ,Import.MontantLivreurFinal " _
            & "    ,Import.MontantLivreurPharmaplus " _
            & "    ,Import.ExtraKM " _
            & "    ,Import.Echange " _
            & "    ,Import.IDPeriode " _
            & "    ,Import.ExtraLivreur " _
            & "    ,Import.ExtraKMLivreur " _
            & "    ,Import.IDSAmount " _
            & "    ,Import.DateImport " _
            & "    ,Import.IsMobilus " _
            & "    ,Import.IDClientAddresse " _
            & " FROM " _
            & "dbo.Import INNER JOIN " _
            & " dbo.livreurs On dbo.Import.LivreurID = dbo.livreurs.LivreurID " _
            & " WHERE (Import.DateImport  >= '" & pdDateFrom.ToString & "' AND Import.DateImport <= '" & pdDateTo.ToString & "')"

        ''(DateImport  >= '" & pdDateFrom.ToString & "' AND DateImport <= '" & pdDateTo.ToString & "') "

        If Not IsNothing(go_Globals.IDPeriode) Then
            selectStatement = selectStatement & " AND IDPeriode = " & go_Globals.IDPeriode
        End If


        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Erreur")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function
    Public Function SelectAllFromDateName(pdDateFrom As DateTime, pdDateTo As DateTime, piLivreurID As Integer, piOrganisationID As Integer) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [ImportID] " _
            & "    ,[Reference] " _
            & "    ,[Code] " _
            & "    ,[Status] " _
            & "    ,[Client] " _
            & "    ,[ClientAddress] " _
            & "    ,[MomentDePassage] " _
            & "    ,[PreparePar] " _
            & "    ,[HeureAppel] " _
            & "    ,[HeurePrep] " _
            & "    ,[HeureDepart] " _
            & "    ,[HeureLivraison] " _
            & "    ,[MessagePassage] " _
            & "    ,[TotalTempService] " _
            & "    ,[Livreur] " _
            & "    ,[Distance] " _
            & "    ,[Montant] " _
            & "    ,[Extra] " _
            & "    ,[Organisation] " _
            & "    ,[OrganisationAddress] " _
            & "    ,[IDNomFichierImport] " _
            & "    ,[Debit] " _
            & "    ,[Credit] " _
            & "    ,[Glacier] " _
            & "    ,[NombreGlacier] " _
            & "    ,[MontantContrat] " _
            & "    ,[OrganisationID] " _
            & "    ,[LivreurID] " _
            & "    ,[MontantLivreur] " _
            & "    ,[MontantGlacierOrganisation] " _
            & "    ,[MontantGlacierLivreur] " _
            & "    ,[MontantLivreurFinal] " _
            & "    ,[MontantLivreurPharmaplus] " _
            & "    ,[ExtraKM] " _
            & "    ,[Echange] " _
            & "    ,[IDPeriode] " _
            & "    ,[ExtraLivreur] " _
            & "    ,[ExtraKMLivreur] " _
            & "    ,[IDSAmount] " _
            & "    ,[DateImport] " _
            & "    ,[IsMobilus] " _
            & "    ,[IDClientAddresse] " _
            & "FROM " _
            & "     [Import] " _
            & "WHERE (DateImport  >= '" & pdDateFrom.ToString & "' AND DateImport <= '" & pdDateTo.ToString & "') AND IDPeriode = " & go_Globals.IDPeriode & " AND LivreurID=" & piLivreurID & " AND  OrganisationID=" & piOrganisationID & " ORDER BY Montant DESC"

        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Erreur")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function

    Public Function FixLivreursID() As Boolean

        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "UPDATE a " &
            " SET    a.livreurid = b.livreurid " &
            " FROM   import a " &
            " INNER JOIN livreurs b " &
            " ON a.livreur = b.livreurcache " &
            " WHERE  a.livreur = b.livreurcache "
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text

        Try
            connection.Open()
            Dim count As Integer = selectCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Erreur")
        Finally
            connection.Close()
        End Try

    End Function


    Public Function SelectAmount(pdDateFrom As DateTime, pdDateTo As DateTime, piLivreurID As Integer, piOrganisationID As Integer) As Double

        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "    SUM([Import].[Montant]) as TotalMontant " _
            & "FROM " _
            & "     [Import] " _
            & "WHERE (DateImport  >= '" & pdDateFrom.ToString & "' AND DateImport <= '" & pdDateTo.ToString & "') AND IDPeriode = " & go_Globals.IDPeriode & " AND LivreurID=" & piLivreurID & " AND  OrganisationID=" & piOrganisationID
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Dim drImport As DataRow
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            Dim dTotalAmount As Double = 0
            If reader.HasRows Then
                dt.Load(reader)
                If dt.Rows.Count > 0 Then
                    For Each drImport In dt.Rows
                        dTotalAmount = dTotalAmount + drImport("TotalMontant")
                    Next
                    Return dTotalAmount
                Else
                    Return 0
                End If

            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Erreur")
        Finally
            connection.Close()
        End Try

    End Function

    Public Function Select_LastID() As Integer
        Dim clsImport As New Import
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT max(ImportID) as last_id from Import WHERE ismobilus = 0" _
            & ""
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader(CommandBehavior.SingleRow)
            If reader.Read Then
                With clsImport
                    .ImportID = If(IsDBNull(reader("last_id")), 0, CType(reader("last_id"), Int32?))
                End With
            Else
                clsImport = Nothing
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return clsImport.ImportID
    End Function

    Public Function Select_Record(ByVal clsImportPara As Import) As Import
        Dim clsImport As New Import
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [ImportID] " _
            & "    ,[Reference] " _
            & "    ,[Code] " _
            & "    ,[Status] " _
            & "    ,[Client] " _
            & "    ,[ClientAddress] " _
            & "    ,[MomentDePassage] " _
            & "    ,[PreparePar] " _
            & "    ,[HeureAppel] " _
            & "    ,[HeurePrep] " _
            & "    ,[HeureDepart] " _
            & "    ,[HeureLivraison] " _
            & "    ,[MessagePassage] " _
            & "    ,[TotalTempService] " _
            & "    ,[Livreur] " _
            & "    ,[Distance] " _
            & "    ,[Montant] " _
            & "    ,[Extra] " _
            & "    ,[Organisation] " _
            & "    ,[OrganisationAddress] " _
            & "    ,[IDNomFichierImport] " _
            & "    ,[Debit] " _
            & "    ,[Credit] " _
            & "    ,[Glacier] " _
            & "    ,[NombreGlacier] " _
            & "    ,[MontantContrat] " _
            & "    ,[OrganisationID] " _
            & "    ,[LivreurID] " _
            & "    ,[MontantLivreur] " _
            & "    ,[MontantGlacierOrganisation] " _
            & "    ,[MontantGlacierLivreur] " _
            & "    ,[MontantLivreurFinal] " _
            & "    ,[MontantLivreurPharmaplus] " _
            & "    ,[ExtraKM] " _
            & "    ,[Echange] " _
            & "    ,[IDPeriode] " _
            & "    ,[ExtraLivreur] " _
            & "    ,[ExtraKMLivreur] " _
            & "    ,[IDSAmount] " _
            & "    ,[DateImport] " _
            & "    ,[IsMobilus] " _
            & "    ,[IDClientAddresse] " _
            & "FROM " _
            & "     [Import] " _
            & "WHERE " _
            & "    [ImportID] = @ImportID " _
            & ""
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        selectCommand.Parameters.AddWithValue("@ImportID", clsImportPara.ImportID)
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader(CommandBehavior.SingleRow)
            If reader.Read Then
                With clsImport
                    .ImportID = System.Convert.ToInt32(reader("ImportID"))
                    .Reference = If(IsDBNull(reader("Reference")), Nothing, reader("Reference").ToString)
                    .Code = If(IsDBNull(reader("Code")), Nothing, reader("Code").ToString)
                    .Status = If(IsDBNull(reader("Status")), Nothing, reader("Status").ToString)
                    .Client = If(IsDBNull(reader("Client")), Nothing, reader("Client").ToString)
                    .ClientAddress = If(IsDBNull(reader("ClientAddress")), Nothing, reader("ClientAddress").ToString)
                    .MomentDePassage = If(IsDBNull(reader("MomentDePassage")), Nothing, reader("MomentDePassage").ToString)
                    .PreparePar = If(IsDBNull(reader("PreparePar")), Nothing, reader("PreparePar").ToString)
                    .HeureAppel = If(IsDBNull(reader("HeureAppel")), Nothing, CType(reader("HeureAppel"), DateTime?))
                    .HeurePrep = If(IsDBNull(reader("HeurePrep")), Nothing, CType(reader("HeurePrep"), DateTime?))
                    .HeureDepart = If(IsDBNull(reader("HeureDepart")), Nothing, CType(reader("HeureDepart"), DateTime?))
                    .HeureLivraison = If(IsDBNull(reader("HeureLivraison")), Nothing, CType(reader("HeureLivraison"), DateTime?))
                    .MessagePassage = If(IsDBNull(reader("MessagePassage")), Nothing, reader("MessagePassage").ToString)
                    .TotalTempService = If(IsDBNull(reader("TotalTempService")), Nothing, reader("TotalTempService").ToString)
                    .Livreur = If(IsDBNull(reader("Livreur")), Nothing, reader("Livreur").ToString)
                    .Distance = If(IsDBNull(reader("Distance")), Nothing, CType(CType(reader("Distance"), Double?), Decimal?))
                    .Montant = If(IsDBNull(reader("Montant")), Nothing, CType(reader("Montant"), Decimal?))
                    .Extra = If(IsDBNull(reader("Extra")), Nothing, CType(reader("Extra"), Decimal?))
                    .Organisation = If(IsDBNull(reader("Organisation")), Nothing, reader("Organisation").ToString)
                    .OrganisationAddress = If(IsDBNull(reader("OrganisationAddress")), Nothing, reader("OrganisationAddress").ToString)
                    .IDNomFichierImport = If(IsDBNull(reader("IDNomFichierImport")), Nothing, CType(reader("IDNomFichierImport"), Int32?))
                    .Debit = If(IsDBNull(reader("Debit")), Nothing, CType(reader("Debit"), Decimal?))
                    .Credit = If(IsDBNull(reader("Credit")), Nothing, CType(reader("Credit"), Decimal?))
                    .Glacier = If(IsDBNull(reader("Glacier")), Nothing, CType(reader("Glacier"), Boolean?))
                    .NombreGlacier = If(IsDBNull(reader("NombreGlacier")), Nothing, CType(reader("NombreGlacier"), Int32?))
                    .MontantContrat = If(IsDBNull(reader("MontantContrat")), Nothing, CType(reader("MontantContrat"), Decimal?))
                    .OrganisationID = If(IsDBNull(reader("OrganisationID")), Nothing, CType(reader("OrganisationID"), Int32?))
                    .LivreurID = If(IsDBNull(reader("LivreurID")), Nothing, CType(reader("LivreurID"), Int32?))
                    .MontantLivreur = If(IsDBNull(reader("MontantLivreur")), Nothing, CType(reader("MontantLivreur"), Decimal?))
                    .MontantGlacierOrganisation = If(IsDBNull(reader("MontantGlacierOrganisation")), Nothing, CType(reader("MontantGlacierOrganisation"), Decimal?))
                    .MontantGlacierLivreur = If(IsDBNull(reader("MontantGlacierLivreur")), Nothing, CType(reader("MontantGlacierLivreur"), Decimal?))
                    .MontantLivreurFinal = If(IsDBNull(reader("MontantLivreurFinal")), Nothing, CType(reader("MontantLivreurFinal"), Decimal?))
                    .MontantLivreurPharmaplus = If(IsDBNull(reader("MontantLivreurPharmaplus")), Nothing, CType(reader("MontantLivreurPharmaplus"), Decimal?))
                    .ExtraKM = If(IsDBNull(reader("ExtraKM")), Nothing, CType(CType(reader("ExtraKM"), Double?), Decimal?))
                    .Echange = If(IsDBNull(reader("Echange")), Nothing, CType(reader("Echange"), Boolean?))
                    .IDPeriode = If(IsDBNull(reader("IDPeriode")), Nothing, CType(reader("IDPeriode"), Int32?))
                    .ExtraLivreur = If(IsDBNull(reader("ExtraLivreur")), Nothing, CType(reader("ExtraLivreur"), Decimal?))
                    .ExtraKMLivreur = If(IsDBNull(reader("ExtraKMLivreur")), Nothing, CType(CType(reader("ExtraKMLivreur"), Double?), Decimal?))
                    .IDSAmount = If(IsDBNull(reader("IDSAmount")), Nothing, CType(reader("IDSAmount"), Decimal?))
                    .DateImport = If(IsDBNull(reader("DateImport")), Nothing, CType(reader("DateImport"), DateTime?))
                    .IsMobilus = If(IsDBNull(reader("IsMobilus")), Nothing, CType(reader("IsMobilus"), Boolean?))
                    .IDClientAddresse = If(IsDBNull(reader("IDClientAddresse")), Nothing, CType(reader("IDClientAddresse"), Int32?))
                End With
            Else
                clsImport = Nothing
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return clsImport
    End Function

    Public Function AddLivraison(ByVal clsImport As Import) As Integer
        Dim connection As SqlConnection = DB.GetConnection
        Dim insertStatement As String _
            = "INSERT " _
            & "     [Import] " _
            & "     ( " _
            & "     [Reference] " _
            & "    ,[Code] " _
            & "    ,[Status] " _
            & "    ,[Client] " _
            & "    ,[ClientAddress] " _
            & "    ,[MomentDePassage] " _
            & "    ,[PreparePar] " _
            & "    ,[HeureAppel] " _
            & "    ,[HeurePrep] " _
            & "    ,[HeureDepart] " _
            & "    ,[HeureLivraison] " _
            & "    ,[MessagePassage] " _
            & "    ,[TotalTempService] " _
            & "    ,[Livreur] " _
            & "    ,[Distance] " _
            & "    ,[Montant] " _
            & "    ,[Extra] " _
            & "    ,[Organisation] " _
            & "    ,[OrganisationAddress] " _
            & "    ,[IDNomFichierImport] " _
            & "    ,[Debit] " _
            & "    ,[Credit] " _
            & "    ,[Glacier] " _
            & "    ,[NombreGlacier] " _
            & "    ,[MontantContrat] " _
            & "    ,[OrganisationID] " _
            & "    ,[LivreurID] " _
            & "    ,[MontantLivreur] " _
            & "    ,[MontantGlacierOrganisation] " _
            & "    ,[MontantGlacierLivreur] " _
            & "    ,[MontantLivreurFinal] " _
            & "    ,[MontantLivreurPharmaplus] " _
            & "    ,[ExtraKM] " _
            & "    ,[Echange] " _
            & "    ,[IDPeriode] " _
            & "    ,[ExtraLivreur] " _
            & "    ,[ExtraKMLivreur] " _
            & "    ,[IDSAmount] " _
            & "    ,[DateImport] " _
            & "    ,[IsMobilus] " _
            & "    ,[IDClientAddresse] " _
            & "     ) " _
            & "VALUES " _
            & "     ( " _
            & "     @Reference " _
            & "    ,@Code " _
            & "    ,@Status " _
            & "    ,@Client " _
            & "    ,@ClientAddress " _
            & "    ,@MomentDePassage " _
            & "    ,@PreparePar " _
            & "    ,@HeureAppel " _
            & "    ,@HeurePrep " _
            & "    ,@HeureDepart " _
            & "    ,@HeureLivraison " _
            & "    ,@MessagePassage " _
            & "    ,@TotalTempService " _
            & "    ,@Livreur " _
            & "    ,@Distance " _
            & "    ,@Montant " _
            & "    ,@Extra " _
            & "    ,@Organisation " _
            & "    ,@OrganisationAddress " _
            & "    ,@IDNomFichierImport " _
            & "    ,@Debit " _
            & "    ,@Credit " _
            & "    ,@Glacier " _
            & "    ,@NombreGlacier " _
            & "    ,@MontantContrat " _
            & "    ,@OrganisationID " _
            & "    ,@LivreurID " _
            & "    ,@MontantLivreur " _
            & "    ,@MontantGlacierOrganisation " _
            & "    ,@MontantGlacierLivreur " _
            & "    ,@MontantLivreurFinal " _
            & "    ,@MontantLivreurPharmaplus " _
            & "    ,@ExtraKM " _
            & "    ,@Echange " _
            & "    ,@IDPeriode " _
            & "    ,@ExtraLivreur " _
            & "    ,@ExtraKMLivreur " _
            & "    ,@IDSAmount " _
            & "    ,@DateImport " _
            & "    ,@IsMobilus " _
            & "    ,@IDClientAddresse " _
            & "     );SELECT SCOPE_IDENTITY() " _
            & ""
        Dim insertCommand As New SqlCommand(insertStatement, connection)
        insertCommand.CommandType = CommandType.Text
        insertCommand.Parameters.AddWithValue("@Reference", IIf(Not IsNothing(clsImport.Reference), clsImport.Reference, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Code", IIf(Not IsNothing(clsImport.Code), clsImport.Code, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Status", IIf(Not IsNothing(clsImport.Status), clsImport.Status, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Client", IIf(Not IsNothing(clsImport.Client), clsImport.Client, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@ClientAddress", IIf(Not IsNothing(clsImport.ClientAddress), clsImport.ClientAddress, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@MomentDePassage", IIf(Not IsNothing(clsImport.MomentDePassage), clsImport.MomentDePassage, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@PreparePar", IIf(Not IsNothing(clsImport.PreparePar), clsImport.PreparePar, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@HeureAppel", IIf(clsImport.HeureAppel.HasValue, clsImport.HeureAppel, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@HeurePrep", IIf(clsImport.HeurePrep.HasValue, clsImport.HeurePrep, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@HeureDepart", IIf(clsImport.HeureDepart.HasValue, clsImport.HeureDepart, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@HeureLivraison", IIf(clsImport.HeureLivraison.HasValue, clsImport.HeureLivraison, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@MessagePassage", IIf(Not IsNothing(clsImport.MessagePassage), clsImport.MessagePassage, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@TotalTempService", IIf(Not IsNothing(clsImport.TotalTempService), clsImport.TotalTempService, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Livreur", IIf(Not IsNothing(clsImport.Livreur), clsImport.Livreur, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Distance", IIf(clsImport.Distance.HasValue, clsImport.Distance, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Montant", IIf(clsImport.Montant.HasValue, clsImport.Montant, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Extra", IIf(clsImport.Extra.HasValue, clsImport.Extra, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Organisation", IIf(Not IsNothing(clsImport.Organisation), clsImport.Organisation, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@OrganisationAddress", IIf(Not IsNothing(clsImport.OrganisationAddress), clsImport.OrganisationAddress, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@IDNomFichierImport", IIf(clsImport.IDNomFichierImport.HasValue, clsImport.IDNomFichierImport, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Debit", IIf(clsImport.Debit.HasValue, clsImport.Debit, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Credit", IIf(clsImport.Credit.HasValue, clsImport.Credit, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Glacier", IIf(clsImport.Glacier.HasValue, clsImport.Glacier, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@NombreGlacier", IIf(clsImport.NombreGlacier.HasValue, clsImport.NombreGlacier, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@MontantContrat", IIf(clsImport.MontantContrat.HasValue, clsImport.MontantContrat, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@OrganisationID", IIf(clsImport.OrganisationID.HasValue, clsImport.OrganisationID, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@LivreurID", IIf(clsImport.LivreurID.HasValue, clsImport.LivreurID, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@MontantLivreur", IIf(clsImport.MontantLivreur.HasValue, clsImport.MontantLivreur, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@MontantGlacierOrganisation", IIf(clsImport.MontantGlacierOrganisation.HasValue, clsImport.MontantGlacierOrganisation, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@MontantGlacierLivreur", IIf(clsImport.MontantGlacierLivreur.HasValue, clsImport.MontantGlacierLivreur, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@MontantLivreurFinal", IIf(clsImport.MontantLivreurFinal.HasValue, clsImport.MontantLivreurFinal, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@MontantLivreurPharmaplus", IIf(clsImport.MontantLivreurPharmaplus.HasValue, clsImport.MontantLivreurPharmaplus, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@ExtraKM", IIf(clsImport.ExtraKM.HasValue, clsImport.ExtraKM, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Echange", IIf(clsImport.Echange.HasValue, clsImport.Echange, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@IDPeriode", IIf(clsImport.IDPeriode.HasValue, clsImport.IDPeriode, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@ExtraLivreur", IIf(clsImport.ExtraLivreur.HasValue, clsImport.ExtraLivreur, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@ExtraKMLivreur", IIf(clsImport.ExtraKMLivreur.HasValue, clsImport.ExtraKMLivreur, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@IDSAmount", IIf(clsImport.IDSAmount.HasValue, clsImport.IDSAmount, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@DateImport", IIf(clsImport.DateImport.HasValue, clsImport.DateImport, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@IsMobilus", IIf(clsImport.IsMobilus.HasValue, clsImport.IsMobilus, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@IDClientAddresse", IIf(clsImport.IDClientAddresse.HasValue, clsImport.IDClientAddresse, DBNull.Value))
        Try
            connection.Open()
            Dim count As Integer = insertCommand.ExecuteScalar()
            If count > 0 Then
                Return count
            Else
                Return count
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function
    Public Function Add(ByVal clsImportBackup As Import) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim insertProcedure As String = "[ImportInsert]"
        Dim insertCommand As New SqlCommand(insertProcedure, connection)
        insertCommand.CommandType = CommandType.StoredProcedure
        insertCommand.Parameters.AddWithValue("@Reference", IIf(Not IsNothing(clsImportBackup.Reference), clsImportBackup.Reference, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Code", IIf(Not IsNothing(clsImportBackup.Code), clsImportBackup.Code, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Status", IIf(Not IsNothing(clsImportBackup.Status), clsImportBackup.Status, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Client", IIf(Not IsNothing(clsImportBackup.Client), clsImportBackup.Client, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@ClientAddress", IIf(Not IsNothing(clsImportBackup.ClientAddress), clsImportBackup.ClientAddress, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@MomentDePassage", IIf(Not IsNothing(clsImportBackup.MomentDePassage), clsImportBackup.MomentDePassage, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@PreparePar", IIf(Not IsNothing(clsImportBackup.PreparePar), clsImportBackup.PreparePar, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@HeureAppel", IIf(clsImportBackup.HeureAppel.HasValue, clsImportBackup.HeureAppel, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@HeurePrep", IIf(clsImportBackup.HeurePrep.HasValue, clsImportBackup.HeurePrep, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@HeureDepart", IIf(clsImportBackup.HeureDepart.HasValue, clsImportBackup.HeureDepart, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@HeureLivraison", IIf(clsImportBackup.HeureLivraison.HasValue, clsImportBackup.HeureLivraison, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@MessagePassage", IIf(Not IsNothing(clsImportBackup.MessagePassage), clsImportBackup.MessagePassage, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@TotalTempService", IIf(Not IsNothing(clsImportBackup.TotalTempService), clsImportBackup.TotalTempService, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Livreur", IIf(Not IsNothing(clsImportBackup.Livreur), clsImportBackup.Livreur, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Distance", IIf(clsImportBackup.Distance.HasValue, clsImportBackup.Distance, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Montant", IIf(clsImportBackup.Montant.HasValue, clsImportBackup.Montant, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Extra", IIf(clsImportBackup.Extra.HasValue, clsImportBackup.Extra, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Organisation", IIf(Not IsNothing(clsImportBackup.Organisation), clsImportBackup.Organisation, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@OrganisationAddress", IIf(Not IsNothing(clsImportBackup.OrganisationAddress), clsImportBackup.OrganisationAddress, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@IDNomFichierImport", IIf(clsImportBackup.IDNomFichierImport.HasValue, clsImportBackup.IDNomFichierImport, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Debit", IIf(clsImportBackup.Debit.HasValue, clsImportBackup.Debit, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Credit", IIf(clsImportBackup.Credit.HasValue, clsImportBackup.Credit, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Glacier", IIf(clsImportBackup.Glacier.HasValue, clsImportBackup.Glacier, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@NombreGlacier", IIf(clsImportBackup.NombreGlacier.HasValue, clsImportBackup.NombreGlacier, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@MontantContrat", IIf(clsImportBackup.MontantContrat.HasValue, clsImportBackup.MontantContrat, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@OrganisationID", IIf(clsImportBackup.OrganisationID.HasValue, clsImportBackup.OrganisationID, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@LivreurID", IIf(clsImportBackup.LivreurID.HasValue, clsImportBackup.LivreurID, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@MontantLivreur", IIf(clsImportBackup.MontantLivreur.HasValue, clsImportBackup.MontantLivreur, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@MontantGlacierOrganisation", IIf(clsImportBackup.MontantGlacierOrganisation.HasValue, clsImportBackup.MontantGlacierOrganisation, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@MontantGlacierLivreur", IIf(clsImportBackup.MontantGlacierLivreur.HasValue, clsImportBackup.MontantGlacierLivreur, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@MontantLivreurFinal", IIf(clsImportBackup.MontantLivreurFinal.HasValue, clsImportBackup.MontantLivreurFinal, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@MontantLivreurPharmaplus", IIf(clsImportBackup.MontantLivreurPharmaplus.HasValue, clsImportBackup.MontantLivreurPharmaplus, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@ExtraKM", IIf(clsImportBackup.ExtraKM.HasValue, clsImportBackup.ExtraKM, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Echange", IIf(clsImportBackup.Echange.HasValue, clsImportBackup.Echange, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@IDPeriode", IIf(clsImportBackup.IDPeriode.HasValue, clsImportBackup.IDPeriode, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@ExtraLivreur", IIf(clsImportBackup.ExtraLivreur.HasValue, clsImportBackup.ExtraLivreur, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@ExtraKMLivreur", IIf(clsImportBackup.ExtraKMLivreur.HasValue, clsImportBackup.ExtraKMLivreur, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@IDSAmount", IIf(clsImportBackup.IDSAmount.HasValue, clsImportBackup.IDSAmount, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@DateImport", IIf(clsImportBackup.DateImport.HasValue, clsImportBackup.DateImport, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@IsMobilus", IIf(clsImportBackup.IsMobilus.HasValue, clsImportBackup.IsMobilus, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@IDClientAddresse", IIf(clsImportBackup.IDClientAddresse.HasValue, clsImportBackup.IDClientAddresse, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@IDResidence", IIf(clsImportBackup.IDResidence.HasValue, clsImportBackup.IDResidence, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Residence", IIf(Not IsNothing(clsImportBackup.Residence), clsImportBackup.Residence, DBNull.Value))
        insertCommand.Parameters.Add("@ReturnValue", System.Data.SqlDbType.Int)
        insertCommand.Parameters("@ReturnValue").Direction = ParameterDirection.Output
        Try
            connection.Open()
            insertCommand.ExecuteNonQuery()
            Dim count As Integer = System.Convert.ToInt32(insertCommand.Parameters("@ReturnValue").Value)
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function
    Public Function UpdateAmountByNameLivreur(pdDateFrom As DateTime, pdDateTo As DateTime, ByVal clsImportNew As Import, piLivreurID As Integer) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim updateStatement As String _
            = "UPDATE " _
            & "     [Import] " _
            & "SET " _
            & "    [MontantLivreur] = @NewAmount " _
            & "WHERE " _
            & "     [OrganisationID] = @OrganisationID  AND LivreurID = @LivreurID " _
            & " AND  (DateImport  >= '" & pdDateFrom.ToString & "' AND DateImport <= '" & pdDateTo.ToString & "') AND IDPeriode = " & go_Globals.IDPeriode
        Dim updateCommand As New SqlCommand(updateStatement, connection)
        updateCommand.CommandType = CommandType.Text
        updateCommand.Parameters.AddWithValue("@NewAmount", IIf(clsImportNew.Montant.HasValue, clsImportNew.Montant, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@OrganisationID", IIf(Not IsNothing(clsImportNew.OrganisationID), clsImportNew.OrganisationID, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@LivreurID", IIf(Not IsNothing(piLivreurID), piLivreurID, DBNull.Value))



        Try
            connection.Open()
            Dim count As Integer = updateCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Erreur")
        Finally
            connection.Close()
        End Try
        Return True
    End Function
    Public Function UpdateAmountByName(pdDateFrom As DateTime, pdDateTo As DateTime, ByVal clsImportNew As Import) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim updateStatement As String _
            = "UPDATE " _
            & "     [Import] " _
            & "SET " _
            & "    [Montant] = @NewAmount " _
            & "WHERE " _
            & "     [OrganisationID] = @OrganisationID " _
            & " AND  (DateImport  >= '" & pdDateFrom.ToString & "' AND DateImport <= '" & pdDateTo.ToString & "') AND IDPeriode = " & go_Globals.IDPeriode
        Dim updateCommand As New SqlCommand(updateStatement, connection)
        updateCommand.CommandType = CommandType.Text
        updateCommand.Parameters.AddWithValue("@NewAmount", IIf(clsImportNew.Montant.HasValue, clsImportNew.Montant, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@OrganisationID", IIf(Not IsNothing(clsImportNew.OrganisationID), clsImportNew.OrganisationID, DBNull.Value))



        Try
            connection.Open()
            Dim count As Integer = updateCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Erreur")
        Finally
            connection.Close()
        End Try
        Return True
    End Function
    Public Function UpdateLivreurAmountByID(ByVal clsImportNew As Import) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim updateStatement As String _
            = "UPDATE " _
            & "     [Import] " _
            & "SET " _
            & "    [MontantLivreur] = @NewAmount " _
            & "WHERE " _
            & "     [ImportID] = @ImportID " _
            & ""
        Dim updateCommand As New SqlCommand(updateStatement, connection)
        updateCommand.CommandType = CommandType.Text
        updateCommand.Parameters.AddWithValue("@NewAmount", IIf(clsImportNew.Montant.HasValue, clsImportNew.Montant, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@ImportID", IIf(Not IsNothing(clsImportNew.ImportID), clsImportNew.ImportID, DBNull.Value))



        Try
            connection.Open()
            Dim count As Integer = updateCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Erreur")
        Finally
            connection.Close()
        End Try
        Return True
    End Function
    Public Function UpdateAmountByIDLivreurPharmaplusApresCalcul(ByVal clsImportNew As Import) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim updateStatement As String _
            = "UPDATE " _
            & "     [Import] " _
            & "SET " _
            & "    [MontantLivreurPharmaplus] = @NewAmount " _
            & "WHERE " _
            & "     [ImportID] = @ImportID " _
            & ""
        Dim updateCommand As New SqlCommand(updateStatement, connection)
        updateCommand.CommandType = CommandType.Text
        updateCommand.Parameters.AddWithValue("@NewAmount", IIf(clsImportNew.Montant.HasValue, clsImportNew.Montant, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@ImportID", IIf(Not IsNothing(clsImportNew.ImportID), clsImportNew.ImportID, DBNull.Value))



        Try
            connection.Open()
            Dim count As Integer = updateCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Erreur")
        Finally
            connection.Close()
        End Try
        Return True
    End Function
    Public Function UpdateAmountByIDLivreurPharmaplus(ByVal clsImportNew As Import) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim updateStatement As String _
            = "UPDATE " _
            & "     [Import] " _
            & "SET " _
            & "    [MontantLivreurFinal] = @NewAmount " _
            & "    ,[ExtraLivreur] = @NewExtraLivreur " _
            & "    ,[ExtraKMLivreur] = @NewExtraKMLivreur " _
            & "WHERE " _
            & "     [ImportID] = @ImportID " _
            & ""
        Dim updateCommand As New SqlCommand(updateStatement, connection)
        updateCommand.CommandType = CommandType.Text
        updateCommand.Parameters.AddWithValue("@NewAmount", IIf(clsImportNew.Montant.HasValue, clsImportNew.Montant, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewExtraLivreur", IIf(clsImportNew.ExtraLivreur.HasValue, clsImportNew.ExtraLivreur, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewExtraKMLivreur", IIf(clsImportNew.ExtraKMLivreur.HasValue, clsImportNew.ExtraKMLivreur, DBNull.Value))


        updateCommand.Parameters.AddWithValue("@ImportID", IIf(Not IsNothing(clsImportNew.ImportID), clsImportNew.ImportID, DBNull.Value))



        Try
            connection.Open()
            Dim count As Integer = updateCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Erreur")
        Finally
            connection.Close()
        End Try
        Return True
    End Function


    Public Function UpdateAmountXtraExtraKMByIDLivreur(ByVal clsImportNew As Import) As Boolean

        Dim connection As SqlConnection = DB.GetConnection
        Dim updateStatement As String _
            = "UPDATE " _
            & "     [Import] " _
            & "SET " _
            & "     [MontantLivreur] = @NewAmount " _
            & "    ,[ExtraLivreur] = @NewExtra " _
            & "    ,[ExtraKMLivreur] = @NewExtraKM " _
            & "    ,[MontantGlacierLivreur] = @NewMontantGlacierLivreur " _
            & "WHERE " _
            & "     [ImportID] = @ImportID " _
            & ""
        Dim updateCommand As New SqlCommand(updateStatement, connection)
        updateCommand.CommandType = CommandType.Text
        updateCommand.Parameters.AddWithValue("@NewAmount", IIf(clsImportNew.MontantLivreur.HasValue, clsImportNew.MontantLivreur, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewExtra", IIf(clsImportNew.ExtraLivreur.HasValue, clsImportNew.ExtraLivreur, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewMontantGlacierLivreur", IIf(clsImportNew.MontantGlacierLivreur.HasValue, clsImportNew.MontantGlacierLivreur, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewExtraKM", IIf(clsImportNew.ExtraKMLivreur.HasValue, clsImportNew.ExtraKMLivreur, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@ImportID", IIf(Not IsNothing(clsImportNew.ImportID), clsImportNew.ImportID, DBNull.Value))


        Try
            connection.Open()
            Dim count As Integer = updateCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Erreur")
        Finally
            connection.Close()
        End Try
        Return True
    End Function


    Public Function UpdateCredit(ByVal clsImportNew As Import) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim updateStatement As String _
            = "UPDATE " _
            & "     [Import] " _
            & "SET " _
            & "    [Credit] = @NewCredit " _
             & " WHERE " _
            & "     [ImportID] = @ImportID " _
            & ""
        Dim updateCommand As New SqlCommand(updateStatement, connection)
        updateCommand.CommandType = CommandType.Text
        updateCommand.Parameters.AddWithValue("@NewCredit", IIf(clsImportNew.Credit.HasValue, clsImportNew.Credit, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@ImportID", IIf(Not IsNothing(clsImportNew.ImportID), clsImportNew.ImportID, DBNull.Value))

        Try
            connection.Open()
            Dim count As Integer = updateCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Erreur")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

    Public Function UpdateAmountExtraGlacierKMByID(ByVal clsImportNew As Import) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim updateStatement As String _
            = "UPDATE " _
            & "     [Import] " _
            & "SET " _
            & "    [Montant] = @NewAmount " _
            & "    ,[Extra] = @NewExtra " _
            & "    ,[MontantGlacierOrganisation] = @NewMontantGlacierOrganisation " _
            & "    ,[ExtraKM] = @NewExtraKM " _
             & "WHERE " _
            & "     [ImportID] = @ImportID " _
            & ""
        Dim updateCommand As New SqlCommand(updateStatement, connection)
        updateCommand.CommandType = CommandType.Text
        updateCommand.Parameters.AddWithValue("@NewAmount", IIf(clsImportNew.Montant.HasValue, clsImportNew.Montant, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewExtra", IIf(clsImportNew.Extra.HasValue, clsImportNew.Extra, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewMontantGlacierOrganisation", IIf(clsImportNew.MontantGlacierOrganisation.HasValue, clsImportNew.MontantGlacierOrganisation, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewExtraKM", IIf(clsImportNew.ExtraKM.HasValue, clsImportNew.ExtraKM, DBNull.Value))
        'updateCommand.Parameters.AddWithValue("@NewExtraKMLivreur", IIf(clsImportNew.ExtraKMLivreur.HasValue, clsImportNew.ExtraKMLivreur, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@ImportID", IIf(Not IsNothing(clsImportNew.ImportID), clsImportNew.ImportID, DBNull.Value))



        Try
            connection.Open()
            Dim count As Integer = updateCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Erreur")
        Finally
            connection.Close()
        End Try
        Return True
    End Function
    Public Function UpdateAmountByID(ByVal clsImportNew As Import) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim updateStatement As String _
            = "UPDATE " _
            & "     [Import] " _
            & "SET " _
            & "    [Montant] = @NewAmount " _
            & "WHERE " _
            & "     [ImportID] = @ImportID " _
            & ""
        Dim updateCommand As New SqlCommand(updateStatement, connection)
        updateCommand.CommandType = CommandType.Text
        updateCommand.Parameters.AddWithValue("@NewAmount", IIf(clsImportNew.Montant.HasValue, clsImportNew.Montant, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@ImportID", IIf(Not IsNothing(clsImportNew.ImportID), clsImportNew.ImportID, DBNull.Value))



        Try
            connection.Open()
            Dim count As Integer = updateCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Erreur")
        Finally
            connection.Close()
        End Try
        Return True
    End Function
    Public Function UpdateDebit(ByVal clsImportOld As Import,
           ByVal clsImportNew As Import) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim updateStatement As String _
            = "UPDATE " _
            & "     [Import] " _
            & "SET " _
            & "    [Debit] = @NewDebit " _
            & "WHERE " _
            & "     [ImportID] = @OldImportID " _
            & ""
        Dim updateCommand As New SqlCommand(updateStatement, connection)
        updateCommand.CommandType = CommandType.Text
        updateCommand.Parameters.AddWithValue("@NewDebit", IIf(clsImportNew.Debit.HasValue, clsImportNew.Debit, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@OldImportID", clsImportOld.ImportID)
        Try
            connection.Open()
            Dim count As Integer = updateCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Erreur")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

    Public Function UpdateKM(ByVal clsImportOld As Import,
           ByVal clsImportNew As Import) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim updateStatement As String _
            = "UPDATE " _
            & "     [Import] " _
            & "SET " _
            & "    [Distance] = @NewDistance " _
            & "WHERE " _
            & "     [ImportID] = @OldImportID " _
            & ""
        Dim updateCommand As New SqlCommand(updateStatement, connection)
        updateCommand.CommandType = CommandType.Text
        updateCommand.Parameters.AddWithValue("@NewDistance", IIf(clsImportNew.Distance.HasValue, clsImportNew.Distance, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@OldImportID", clsImportOld.ImportID)
        Try
            connection.Open()
            Dim count As Integer = updateCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Erreur")
        Finally
            connection.Close()
        End Try
        Return True
    End Function
    Public Function UpdateMontantGlacierLivreur(ByVal clsImportOld As Import,
           ByVal clsImportNew As Import) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim updateStatement As String _
            = "UPDATE " _
            & "     [Import] " _
            & "SET " _
            & "    [MontantGlacierLivreur] = @NewMontantGlacierLivreur " _
            & "WHERE " _
            & "     [ImportID] = @OldImportID " _
            & ""
        Dim updateCommand As New SqlCommand(updateStatement, connection)
        updateCommand.CommandType = CommandType.Text
        updateCommand.Parameters.AddWithValue("@NewMontantGlacierLivreur", IIf(clsImportNew.MontantGlacierLivreur.HasValue, clsImportNew.MontantGlacierLivreur, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@OldImportID", clsImportOld.ImportID)
        Try
            connection.Open()
            Dim count As Integer = updateCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Erreur")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

    Public Function UpdateMontantGlacierOrganisation(ByVal clsImportOld As Import,
           ByVal clsImportNew As Import) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim updateStatement As String _
            = "UPDATE " _
            & "     [Import] " _
            & "SET " _
            & "    [MontantGlacierOrganisation] = @NewMontantGlacierOrganisation " _
            & "WHERE " _
            & "     [ImportID] = @OldImportID " _
            & ""
        Dim updateCommand As New SqlCommand(updateStatement, connection)
        updateCommand.CommandType = CommandType.Text
        updateCommand.Parameters.AddWithValue("@NewMontantGlacierOrganisation", IIf(clsImportNew.MontantGlacierOrganisation.HasValue, clsImportNew.MontantGlacierOrganisation, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@OldImportID", clsImportOld.ImportID)
        Try
            connection.Open()
            Dim count As Integer = updateCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Erreur")
        Finally
            connection.Close()
        End Try
        Return True
    End Function
    Public Function UpdateCredit(ByVal clsImportOld As Import,
           ByVal clsImportNew As Import) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim updateStatement As String _
            = "UPDATE " _
            & "     [Import] " _
            & "SET " _
            & "    [Credit] = @NewCredit " _
            & "WHERE " _
            & "     [ImportID] = @OldImportID " _
            & ""
        Dim updateCommand As New SqlCommand(updateStatement, connection)
        updateCommand.CommandType = CommandType.Text
        updateCommand.Parameters.AddWithValue("@NewCredit", IIf(clsImportNew.Credit.HasValue, clsImportNew.Credit, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@OldImportID", clsImportOld.ImportID)
        Try
            connection.Open()
            Dim count As Integer = updateCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Erreur")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

    Public Function UpdateMessage(ByVal clsImportOld As Import,
           ByVal clsImportNew As Import) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim updateStatement As String _
            = "UPDATE " _
            & "     [Import] " _
            & "SET " _
            & "    [Credit] = @NewCredit " _
            & "WHERE " _
            & "     [ImportID] = @OldImportID " _
            & ""
        Dim updateCommand As New SqlCommand(updateStatement, connection)
        updateCommand.CommandType = CommandType.Text
        updateCommand.Parameters.AddWithValue("@NewCredit", clsImportNew.MessagePassage.ToString.Trim)
        updateCommand.Parameters.AddWithValue("@OldImportID", clsImportOld.ImportID)
        Try
            connection.Open()
            Dim count As Integer = updateCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Erreur")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

    Public Function UpdateGlacier(ByVal clsImportOld As Import,
           ByVal clsImportNew As Import) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim updateStatement As String _
            = "UPDATE " _
            & "     [Import] " _
            & "SET " _
            & "    [NombreGlacier] = @NewNombreGlacier " _
            & "WHERE " _
            & "     [ImportID] = @OldImportID " _
            & ""
        Dim updateCommand As New SqlCommand(updateStatement, connection)
        updateCommand.CommandType = CommandType.Text

        updateCommand.Parameters.AddWithValue("@NewNombreGlacier", clsImportOld.NombreGlacier)
        updateCommand.Parameters.AddWithValue("@OldImportID", clsImportOld.ImportID)


        Try
            connection.Open()
            Dim count As Integer = updateCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Erreur")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

    Public Function UpdateResidence(ByVal clsImportOld As Import,
           ByVal clsImportNew As Import) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim updateStatement As String _
            = "UPDATE " _
            & "     [Import] " _
            & "SET " _
            & "    [IDResidence] = @NewIDResidence " _
            & "    ,[Residence] = @NewResidence " _
            & "WHERE " _
            & "     [ImportID] = @OldImportID " _
            & ""
        Dim updateCommand As New SqlCommand(updateStatement, connection)
        updateCommand.CommandType = CommandType.Text
        updateCommand.Parameters.AddWithValue("@NewIDResidence", IIf(clsImportNew.IDResidence.HasValue, clsImportNew.IDResidence, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewResidence", IIf(Not IsNothing(clsImportNew.Residence), clsImportNew.Residence, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@OldImportID", clsImportOld.ImportID)

        Try
            connection.Open()
            Dim count As Integer = updateCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

    Public Function Update(ByVal clsImportBackupOld As Import,
           ByVal clsImportBackupNew As Import) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim updateProcedure As String = "[ImportUpdate]"
        Dim updateCommand As New SqlCommand(updateProcedure, connection)
        updateCommand.CommandType = CommandType.StoredProcedure
        updateCommand.Parameters.AddWithValue("@NewReference", IIf(Not IsNothing(clsImportBackupNew.Reference), clsImportBackupNew.Reference, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewCode", IIf(Not IsNothing(clsImportBackupNew.Code), clsImportBackupNew.Code, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewStatus", IIf(Not IsNothing(clsImportBackupNew.Status), clsImportBackupNew.Status, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewClient", IIf(Not IsNothing(clsImportBackupNew.Client), clsImportBackupNew.Client, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewClientAddress", IIf(Not IsNothing(clsImportBackupNew.ClientAddress), clsImportBackupNew.ClientAddress, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewMomentDePassage", IIf(Not IsNothing(clsImportBackupNew.MomentDePassage), clsImportBackupNew.MomentDePassage, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewPreparePar", IIf(Not IsNothing(clsImportBackupNew.PreparePar), clsImportBackupNew.PreparePar, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewHeureAppel", IIf(clsImportBackupNew.HeureAppel.HasValue, clsImportBackupNew.HeureAppel, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewHeurePrep", IIf(clsImportBackupNew.HeurePrep.HasValue, clsImportBackupNew.HeurePrep, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewHeureDepart", IIf(clsImportBackupNew.HeureDepart.HasValue, clsImportBackupNew.HeureDepart, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewHeureLivraison", IIf(clsImportBackupNew.HeureLivraison.HasValue, clsImportBackupNew.HeureLivraison, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewMessagePassage", IIf(Not IsNothing(clsImportBackupNew.MessagePassage), clsImportBackupNew.MessagePassage, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewTotalTempService", IIf(Not IsNothing(clsImportBackupNew.TotalTempService), clsImportBackupNew.TotalTempService, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewLivreur", IIf(Not IsNothing(clsImportBackupNew.Livreur), clsImportBackupNew.Livreur, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewDistance", IIf(clsImportBackupNew.Distance.HasValue, clsImportBackupNew.Distance, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewMontant", IIf(clsImportBackupNew.Montant.HasValue, clsImportBackupNew.Montant, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewExtra", IIf(clsImportBackupNew.Extra.HasValue, clsImportBackupNew.Extra, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewOrganisation", IIf(Not IsNothing(clsImportBackupNew.Organisation), clsImportBackupNew.Organisation, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewOrganisationAddress", IIf(Not IsNothing(clsImportBackupNew.OrganisationAddress), clsImportBackupNew.OrganisationAddress, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewIDNomFichierImport", IIf(clsImportBackupNew.IDNomFichierImport.HasValue, clsImportBackupNew.IDNomFichierImport, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewDebit", IIf(clsImportBackupNew.Debit.HasValue, clsImportBackupNew.Debit, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewCredit", IIf(clsImportBackupNew.Credit.HasValue, clsImportBackupNew.Credit, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewGlacier", IIf(clsImportBackupNew.Glacier.HasValue, clsImportBackupNew.Glacier, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewNombreGlacier", IIf(clsImportBackupNew.NombreGlacier.HasValue, clsImportBackupNew.NombreGlacier, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewMontantContrat", IIf(clsImportBackupNew.MontantContrat.HasValue, clsImportBackupNew.MontantContrat, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewOrganisationID", IIf(clsImportBackupNew.OrganisationID.HasValue, clsImportBackupNew.OrganisationID, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewLivreurID", IIf(clsImportBackupNew.LivreurID.HasValue, clsImportBackupNew.LivreurID, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewMontantLivreur", IIf(clsImportBackupNew.MontantLivreur.HasValue, clsImportBackupNew.MontantLivreur, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewMontantGlacierOrganisation", IIf(clsImportBackupNew.MontantGlacierOrganisation.HasValue, clsImportBackupNew.MontantGlacierOrganisation, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewMontantGlacierLivreur", IIf(clsImportBackupNew.MontantGlacierLivreur.HasValue, clsImportBackupNew.MontantGlacierLivreur, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewMontantLivreurFinal", IIf(clsImportBackupNew.MontantLivreurFinal.HasValue, clsImportBackupNew.MontantLivreurFinal, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewMontantLivreurPharmaplus", IIf(clsImportBackupNew.MontantLivreurPharmaplus.HasValue, clsImportBackupNew.MontantLivreurPharmaplus, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewExtraKM", IIf(clsImportBackupNew.ExtraKM.HasValue, clsImportBackupNew.ExtraKM, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewEchange", IIf(clsImportBackupNew.Echange.HasValue, clsImportBackupNew.Echange, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewIDPeriode", IIf(clsImportBackupNew.IDPeriode.HasValue, clsImportBackupNew.IDPeriode, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewExtraLivreur", IIf(clsImportBackupNew.ExtraLivreur.HasValue, clsImportBackupNew.ExtraLivreur, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewExtraKMLivreur", IIf(clsImportBackupNew.ExtraKMLivreur.HasValue, clsImportBackupNew.ExtraKMLivreur, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewIDSAmount", IIf(clsImportBackupNew.IDSAmount.HasValue, clsImportBackupNew.IDSAmount, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewDateImport", IIf(clsImportBackupNew.DateImport.HasValue, clsImportBackupNew.DateImport, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewIsMobilus", IIf(clsImportBackupNew.IsMobilus.HasValue, clsImportBackupNew.IsMobilus, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewIDClientAddresse", IIf(clsImportBackupNew.IDClientAddresse.HasValue, clsImportBackupNew.IDClientAddresse, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewIDResidence", IIf(clsImportBackupNew.IDResidence.HasValue, clsImportBackupNew.IDResidence, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewResidence", IIf(Not IsNothing(clsImportBackupNew.Residence), clsImportBackupNew.Residence, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@OldImportID", clsImportBackupOld.ImportID)
        updateCommand.Parameters.Add("@ReturnValue", System.Data.SqlDbType.Int)
        updateCommand.Parameters("@ReturnValue").Direction = ParameterDirection.Output
        Try
            connection.Open()
            updateCommand.ExecuteNonQuery()
            Dim count As Integer = System.Convert.ToInt32(updateCommand.Parameters("@ReturnValue").Value)
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function


    Public Function DeleteImportBackup(ByVal clsNomFichierImport As NomFichierImport) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim deleteStatement As String _
            = "DELETE FROM " _
            & "     [ImportBackup] " _
            & "WHERE " _
            & "     [IDNomFichierImport] = @IDNomFichierImport " _
            & ""
        Dim deleteCommand As New SqlCommand(deleteStatement, connection)
        deleteCommand.CommandType = CommandType.Text
        deleteCommand.Parameters.AddWithValue("@IDNomFichierImport", clsNomFichierImport.IDNomFichierImport)
        Try
            connection.Open()
            Dim count As Integer = deleteCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Erreur")
        Finally
            connection.Close()
        End Try
        Return True
    End Function
    Public Function DeleteImport(ByVal clsNomFichierImport As NomFichierImport) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim deleteStatement As String _
            = "DELETE FROM " _
            & "     [Import] " _
            & "WHERE " _
            & "     [IDNomFichierImport] = @IDNomFichierImport " _
            & ""
        Dim deleteCommand As New SqlCommand(deleteStatement, connection)
        deleteCommand.CommandType = CommandType.Text
        deleteCommand.Parameters.AddWithValue("@IDNomFichierImport", clsNomFichierImport.IDNomFichierImport)
        Try
            connection.Open()
            Dim count As Integer = deleteCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Erreur")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

    Public Function DeleteAll() As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim deleteProcedure As String = "[dbo].[ImportDeleteAll]"
        Dim deleteCommand As New SqlCommand(deleteProcedure, connection)
        deleteCommand.CommandType = CommandType.StoredProcedure
        deleteCommand.Parameters.Add("@ReturnValue", System.Data.SqlDbType.Int)
        deleteCommand.Parameters("@ReturnValue").Direction = ParameterDirection.Output
        Try
            connection.Open()
            deleteCommand.ExecuteNonQuery()
            Dim count As Integer = System.Convert.ToInt32(deleteCommand.Parameters("@ReturnValue").Value)
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

    Public Function DeleteFromBackup(pIDPeriode As Integer) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim deleteStatement As String _
            = "DELETE FROM " _
            & "     [ImportBackup] " _
            & "WHERE " _
            & "     [IDPeriode] = " & pIDPeriode

        Dim deleteCommand As New SqlCommand(deleteStatement, connection)
        deleteCommand.CommandType = CommandType.Text
        Try
            connection.Open()
            Dim count As Integer = deleteCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Erreur")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

    Public Function Delete(ByVal clsImportBackup As Import) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim deleteProcedure As String = "[ImportDelete]"
        Dim deleteCommand As New SqlCommand(deleteProcedure, connection)
        deleteCommand.CommandType = CommandType.StoredProcedure
        deleteCommand.Parameters.AddWithValue("@OldImportID", clsImportBackup.ImportID)
        deleteCommand.Parameters.Add("@ReturnValue", System.Data.SqlDbType.Int)
        deleteCommand.Parameters("@ReturnValue").Direction = ParameterDirection.Output
        Try
            connection.Open()
            deleteCommand.ExecuteNonQuery()
            Dim count As Integer = System.Convert.ToInt32(deleteCommand.Parameters("@ReturnValue").Value)
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

End Class




