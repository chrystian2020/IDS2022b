Imports System.Data.SqlClient

Public Class RapportFacturePayeData
    Public Function SelectAllPByDate(pdDateFrom As DateTime, pdDateTo As DateTime) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDRapport] " _
            & "    ,[IDPeriode] " _
            & "    ,[IDOrganisation] " _
            & "    ,[IDLivreur] " _
            & "    ,[CheminRapport] " _
            & "    ,[NomFichier] " _
            & "    ,[Email] " _
            & "    ,[Email1] " _
            & "    ,[Email2] " _
            & "    ,[Organisation] " _
            & "    ,[Livreur] " _
            & "    ,[DateFrom] " _
            & "    ,[DateTo] " _
            & " FROM " _
            & "     [RapportFacturePaye] " _
            & " WHERE (DateFrom  >= '" & pdDateFrom & "' AND DateTo <= '" & pdDateTo & "') AND IDPeriode = " & go_Globals.IDPeriode & " and IDLivreur IS NOT NULL"
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function
    Public Function SelectAllFByDate(pdDateFrom As DateTime, pdDateTo As DateTime) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDRapport] " _
            & "    ,[IDPeriode] " _
            & "    ,[IDOrganisation] " _
            & "    ,[IDLivreur] " _
            & "    ,[CheminRapport] " _
            & "    ,[NomFichier] " _
            & "    ,[Email] " _
            & "    ,[Email1] " _
            & "    ,[Email2] " _
            & "    ,[Organisation] " _
            & "    ,[Livreur] " _
            & "    ,[DateFrom] " _
            & "    ,[DateTo] " _
            & "FROM " _
            & "     [RapportFacturePaye] " _
            & " WHERE (DateFrom  >= '" & pdDateFrom & "' AND DateTo <= '" & pdDateTo & "') AND IDPeriode = " & go_Globals.IDPeriode & " and IDOrganisation IS NOT NULL"
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function


    Public Function SelectAllP() As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDRapport] " _
            & "    ,[NomFichier] " _
            & "FROM " _
            & "     [RapportFacturePaye] " _
            & " WHERE IDLivreur IS NOT NULL"
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function
    Public Function SelectAllPaye(PIDPeriode As Integer) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDRapport] " _
            & "    ,[NomFichier] " _
            & "FROM " _
            & "     [RapportFacturePaye] " _
            & " WHERE IDLivreur IS NOT NULL AND IDPeriode =" & PIDPeriode
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function
    Public Function SelectAllF() As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDRapport] " _
            & "    ,[NomFichier] " _
            & "FROM " _
            & "     [RapportFacturePaye] " _
            & " WHERE IDOrganisation IS NOT NULL"

        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function
    Public Function SelectAllFactures(PIDPeriode As Integer) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDRapport] " _
            & "    ,[NomFichier] " _
            & "FROM " _
            & "     [RapportFacturePaye] " _
            & " WHERE IDOrganisation IS NOT NULL AND IDPeriode =" & PIDPeriode
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function

    Public Function SelectAllDataByPeriodesP(PIDPeriode As Integer, piIDRapport As String) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
           & "     [IDRapport] " _
            & "    ,[IDPeriode] " _
            & "    ,[IDOrganisation] " _
            & "    ,[IDLivreur] " _
            & "    ,[CheminRapport] " _
            & "    ,[NomFichier] " _
            & "    ,[Email] " _
            & "    ,[Email1] " _
            & "    ,[Email2] " _
            & "    ,[Organisation] " _
            & "    ,[Livreur] " _
            & "FROM " _
            & "     [RapportFacturePaye] " _
            & " WHERE IDLivreur IS NOT NULL AND IDPeriode =" & PIDPeriode

        If piIDRapport.ToString.Trim <> "" Then
            selectStatement = selectStatement & " AND IDRapport IN (" & piIDRapport & ")"
        End If


        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function


    Public Function SelectAllDataByPeriodeP(PIDPeriode As Integer, piIDRapport As Integer) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
           & "     [IDRapport] " _
            & "    ,[IDPeriode] " _
            & "    ,[IDOrganisation] " _
            & "    ,[IDLivreur] " _
            & "    ,[CheminRapport] " _
            & "    ,[NomFichier] " _
            & "    ,[Email] " _
            & "    ,[Email1] " _
            & "    ,[Email2] " _
            & "    ,[Organisation] " _
            & "    ,[Livreur] " _
            & "FROM " _
            & "     [RapportFacturePaye] " _
            & " WHERE IDLivreur IS NOT NULL AND IDPeriode =" & PIDPeriode

        If piIDRapport > 0 Then
            selectStatement = selectStatement & " AND IDRapport= " & piIDRapport
        End If


        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function
    Public Function SelectAllDataPeriodeF(PIDPeriode As Integer, piIDRapport As Integer) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
           & "     [IDRapport] " _
            & "    ,[IDPeriode] " _
            & "    ,[IDOrganisation] " _
            & "    ,[IDLivreur] " _
            & "    ,[CheminRapport] " _
            & "    ,[NomFichier] " _
            & "    ,[Email] " _
            & "    ,[Email1] " _
            & "    ,[Email2] " _
            & "    ,[Organisation] " _
            & "    ,[Livreur] " _
            & "FROM " _
            & "     [RapportFacturePaye] " _
            & " WHERE IDOrganisation IS NOT NULL AND IDPeriode =" & PIDPeriode

        If piIDRapport > 0 Then
            selectStatement = selectStatement & " AND IDRapport= " & piIDRapport
        End If

        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function

    Public Function SelectAllDataByPeriodesF(PIDPeriode As Integer, piIDRapport As String) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
           & "     [IDRapport] " _
            & "    ,[IDPeriode] " _
            & "    ,[IDOrganisation] " _
            & "    ,[IDLivreur] " _
            & "    ,[CheminRapport] " _
            & "    ,[NomFichier] " _
            & "    ,[Email] " _
            & "    ,[Email1] " _
            & "    ,[Email2] " _
            & "    ,[Organisation] " _
            & "    ,[Livreur] " _
            & "FROM " _
            & "     [RapportFacturePaye] " _
            & " WHERE IDOrganisation IS NOT NULL AND IDPeriode =" & PIDPeriode

        If piIDRapport.ToString.Trim <> "" Then
            selectStatement = selectStatement & " AND IDRapport IN (" & piIDRapport & ")"
        End If

        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function
    Public Function SelectAllDataByPeriodeFNormal(PIDPeriode As Integer, piIDRapport As Integer) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
           & "     [IDRapport] " _
            & "    ,[IDPeriode] " _
            & "    ,[IDOrganisation] " _
            & "    ,[IDLivreur] " _
            & "    ,[CheminRapport] " _
            & "    ,[NomFichier] " _
            & "    ,[Email] " _
            & "    ,[Email1] " _
            & "    ,[Email2] " _
            & "    ,[Organisation] " _
            & "    ,[Livreur] " _
            & "FROM " _
            & "     [RapportFacturePaye] " _
            & " WHERE IDOrganisation IS NOT NULL AND IDPeriode =" & PIDPeriode

        If piIDRapport > 0 Then
            selectStatement = selectStatement & " AND IDRapport IN (" & piIDRapport & ")"
        End If

        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function


    Public Function SelectAllDataByPeriodeF(PIDPeriode As Integer, piIDRapport As String) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
           & "     [IDRapport] " _
            & "    ,[IDPeriode] " _
            & "    ,[IDOrganisation] " _
            & "    ,[IDLivreur] " _
            & "    ,[CheminRapport] " _
            & "    ,[NomFichier] " _
            & "    ,[Email] " _
            & "    ,[Email1] " _
            & "    ,[Email2] " _
            & "    ,[Organisation] " _
            & "    ,[Livreur] " _
            & "FROM " _
            & "     [RapportFacturePaye] " _
            & " WHERE IDOrganisation IS NOT NULL AND IDPeriode =" & PIDPeriode

        If piIDRapport.ToString.Trim <> "" Then
            selectStatement = selectStatement & " AND IDRapport IN (" & piIDRapport & ")"
        End If

        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function
    Public Function SelectAllByID(piIDRapport As String) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDRapport] " _
            & "    ,[IDPeriode] " _
            & "    ,[IDOrganisation] " _
            & "    ,[IDLivreur] " _
            & "    ,[CheminRapport] " _
            & "    ,[NomFichier] " _
            & "    ,[Email] " _
            & "    ,[Email1] " _
            & "    ,[Email2] " _
            & "    ,[Organisation] " _
            & "    ,[Livreur] " _
            & "    ,[DateFrom] " _
            & "    ,[DateTo] " _
            & "FROM " _
            & "     [RapportFacturePaye] " _
            & " WHERE IDRapport IN (" & piIDRapport & ")"
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function

    Public Function SelectAllByIDsCommission(piIDRapport As String) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDRapport] " _
            & "    ,[IDPeriode] " _
            & "    ,[IDOrganisation] " _
            & "    ,[IDLivreur] " _
            & "    ,[CheminRapport] " _
            & "    ,[NomFichier] " _
            & "    ,[Email] " _
            & "    ,[Email1] " _
            & "    ,[Email2] " _
            & "    ,[Organisation] " _
            & "    ,[Livreur] " _
            & "    ,[DateFrom] " _
            & "    ,[DateTo] " _
            & "FROM " _
            & "     [RapportFacturePaye] " _
            & " WHERE IsCommissionVendeur = 1 AND  IDRapport IN (" & piIDRapport & ")"
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function
    Public Function SelectAllByIDs(piIDRapport As String) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDRapport] " _
            & "    ,[IDPeriode] " _
            & "    ,[IDOrganisation] " _
            & "    ,[IDLivreur] " _
            & "    ,[CheminRapport] " _
            & "    ,[NomFichier] " _
            & "    ,[Email] " _
            & "    ,[Email1] " _
            & "    ,[Email2] " _
            & "    ,[Organisation] " _
            & "    ,[Livreur] " _
            & "    ,[DateFrom] " _
            & "    ,[DateTo] " _
            & "FROM " _
            & "     [RapportFacturePaye] " _
            & " WHERE IDRapport IN (" & piIDRapport & ") AND IsCommissionVendeur = 0"
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function
    Public Function SelectAllByID(piIDRapport As Integer) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDRapport] " _
            & "    ,[IDPeriode] " _
            & "    ,[IDOrganisation] " _
            & "    ,[IDLivreur] " _
            & "    ,[CheminRapport] " _
            & "    ,[NomFichier] " _
            & "    ,[Email] " _
            & "    ,[Email1] " _
            & "    ,[Email2] " _
            & "    ,[Organisation] " _
            & "    ,[Livreur] " _
            & "    ,[DateFrom] " _
            & "    ,[DateTo] " _
            & "FROM " _
            & "     [RapportFacturePaye] " _
            & " WHERE IDRapport =" & piIDRapport
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function

    Public Function SelectAll() As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDRapport] " _
            & "    ,[IDPeriode] " _
            & "    ,[IDOrganisation] " _
            & "    ,[IDLivreur] " _
            & "    ,[CheminRapport] " _
            & "    ,[NomFichier] " _
            & "    ,[Email] " _
            & "    ,[Email1] " _
            & "    ,[Email2] " _
            & "    ,[Organisation] " _
            & "    ,[Livreur] " _
            & "    ,[DateFrom] " _
            & "    ,[DateTo] " _
            & "FROM " _
            & "     [RapportFacturePaye] " _
            & ""
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function

    Public Function Select_Record(ByVal clsRapportFacturePayePara As RapportFacturePaye) As RapportFacturePaye
        Dim clsRapportFacturePaye As New RapportFacturePaye
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDRapport] " _
            & "    ,[IDPeriode] " _
            & "    ,[IDOrganisation] " _
            & "    ,[IDLivreur] " _
            & "    ,[CheminRapport] " _
            & "    ,[NomFichier] " _
            & "    ,[Email] " _
            & "    ,[Email1] " _
            & "    ,[Email2] " _
            & "    ,[Organisation] " _
            & "    ,[Livreur] " _
            & "    ,[DateFrom] " _
            & "    ,[DateTo] " _
            & "FROM " _
            & "     [RapportFacturePaye] " _
            & "WHERE " _
            & "     [IDRapport] = @IDRapport " _
            & ""
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        selectCommand.Parameters.AddWithValue("@IDRapport", clsRapportFacturePayePara.IDRapport)
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader(CommandBehavior.SingleRow)
            If reader.Read Then
                With clsRapportFacturePaye
                    .IDRapport = System.Convert.ToInt32(reader("IDRapport"))
                    .IDPeriode = If(IsDBNull(reader("IDPeriode")), Nothing, CType(reader("IDPeriode"), Int32?))
                    .IDOrganisation = If(IsDBNull(reader("IDOrganisation")), Nothing, CType(reader("IDOrganisation"), Int32?))
                    .IDLivreur = If(IsDBNull(reader("IDLivreur")), Nothing, CType(reader("IDLivreur"), Int32?))
                    .CheminRapport = If(IsDBNull(reader("CheminRapport")), Nothing, reader("CheminRapport").ToString)
                    .NomFichier = If(IsDBNull(reader("NomFichier")), Nothing, reader("NomFichier").ToString)
                    .Email = If(IsDBNull(reader("Email")), Nothing, reader("Email").ToString)
                    .Email1 = If(IsDBNull(reader("Email1")), Nothing, reader("Email1").ToString)
                    .Email2 = If(IsDBNull(reader("Email2")), Nothing, reader("Email2").ToString)
                    .Organisation = If(IsDBNull(reader("Organisation")), Nothing, reader("Organisation").ToString)
                    .Livreur = If(IsDBNull(reader("Livreur")), Nothing, reader("Livreur").ToString)
                    .DateFrom = If(IsDBNull(reader("DateFrom")), Nothing, CType(reader("DateFrom"), DateTime?))
                    .DateTo = If(IsDBNull(reader("DateTo")), Nothing, CType(reader("DateTo"), DateTime?))
                End With
            Else
                clsRapportFacturePaye = Nothing
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return clsRapportFacturePaye
    End Function

    Public Function Add(ByVal clsRapportFacturePaye As RapportFacturePaye) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim insertStatement As String _
            = "INSERT " _
            & "     [RapportFacturePaye] " _
            & "     ( " _
            & "     [IDPeriode] " _
            & "    ,[IDOrganisation] " _
            & "    ,[IDLivreur] " _
            & "    ,[CheminRapport] " _
            & "    ,[NomFichier] " _
            & "    ,[Email] " _
            & "    ,[Email1] " _
            & "    ,[Email2] " _
            & "    ,[Organisation] " _
            & "    ,[Livreur] " _
            & "    ,[DateFrom] " _
            & "    ,[DateTo] " _
            & "    ,[IsCommissionVendeur] " _
            & "     ) " _
            & "VALUES " _
            & "     ( " _
            & "     @IDPeriode " _
            & "    ,@IDOrganisation " _
            & "    ,@IDLivreur " _
            & "    ,@CheminRapport " _
            & "    ,@NomFichier " _
            & "    ,@Email " _
            & "    ,@Email1 " _
            & "    ,@Email2 " _
            & "    ,@Organisation " _
            & "    ,@Livreur " _
            & "    ,@DateFrom " _
            & "    ,@DateTo " _
            & "    ,@IsCommissionVendeur " _
            & "     ) " _
            & ""
        Dim insertCommand As New SqlCommand(insertStatement, connection)
        insertCommand.CommandType = CommandType.Text
        insertCommand.Parameters.AddWithValue("@IDPeriode", IIf(clsRapportFacturePaye.IDPeriode.HasValue, clsRapportFacturePaye.IDPeriode, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@IDOrganisation", IIf(clsRapportFacturePaye.IDOrganisation.HasValue, clsRapportFacturePaye.IDOrganisation, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@IDLivreur", IIf(clsRapportFacturePaye.IDLivreur.HasValue, clsRapportFacturePaye.IDLivreur, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@CheminRapport", IIf(Not IsNothing(clsRapportFacturePaye.CheminRapport), clsRapportFacturePaye.CheminRapport, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@NomFichier", IIf(Not IsNothing(clsRapportFacturePaye.NomFichier), clsRapportFacturePaye.NomFichier, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Email", IIf(Not IsNothing(clsRapportFacturePaye.Email), clsRapportFacturePaye.Email, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Email1", IIf(Not IsNothing(clsRapportFacturePaye.Email1), clsRapportFacturePaye.Email1, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Email2", IIf(Not IsNothing(clsRapportFacturePaye.Email2), clsRapportFacturePaye.Email2, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Organisation", IIf(Not IsNothing(clsRapportFacturePaye.Organisation), clsRapportFacturePaye.Organisation, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Livreur", IIf(Not IsNothing(clsRapportFacturePaye.Livreur), clsRapportFacturePaye.Livreur, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@DateFrom", IIf(clsRapportFacturePaye.DateFrom.HasValue, clsRapportFacturePaye.DateFrom, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@DateTo", IIf(clsRapportFacturePaye.DateTo.HasValue, clsRapportFacturePaye.DateTo, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@IsCommissionVendeur", IIf(clsRapportFacturePaye.IsCommissionVendeur.HasValue, clsRapportFacturePaye.IsCommissionVendeur, DBNull.Value))
        Try
            connection.Open()
            Dim count As Integer = insertCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

    Public Function Update(ByVal clsRapportFacturePayeOld As RapportFacturePaye,
           ByVal clsRapportFacturePayeNew As RapportFacturePaye) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim updateStatement As String _
            = "UPDATE " _
            & "     [RapportFacturePaye] " _
            & "SET " _
            & "     [IDPeriode] = @NewIDPeriode " _
            & "    ,[IDOrganisation] = @NewIDOrganisation " _
            & "    ,[IDLivreur] = @NewIDLivreur " _
            & "    ,[CheminRapport] = @NewCheminRapport " _
            & "    ,[NomFichier] = @NewNomFichier " _
            & "    ,[Email] = @NewEmail " _
            & "    ,[Email1] = @NewEmail1 " _
            & "    ,[Email2] = @NewEmail2 " _
            & "    ,[Organisation] = @NewOrganisation " _
            & "    ,[Livreur] = @NewLivreur " _
            & "    ,[DateFrom] = @NewDateFrom " _
            & "    ,[DateTo] = @NewDateTo " _
            & "WHERE " _
            & "     [IDRapport] = @OldIDRapport " _
            & " AND ((@OldIDPeriode IS NULL AND [IDPeriode] IS NULL) OR [IDPeriode] = @OldIDPeriode) " _
            & " AND ((@OldIDOrganisation IS NULL AND [IDOrganisation] IS NULL) OR [IDOrganisation] = @OldIDOrganisation) " _
            & " AND ((@OldIDLivreur IS NULL AND [IDLivreur] IS NULL) OR [IDLivreur] = @OldIDLivreur) " _
            & " AND ((@OldCheminRapport IS NULL AND [CheminRapport] IS NULL) OR [CheminRapport] = @OldCheminRapport) " _
            & " AND ((@OldNomFichier IS NULL AND [NomFichier] IS NULL) OR [NomFichier] = @OldNomFichier) " _
            & " AND ((@OldEmail IS NULL AND [Email] IS NULL) OR [Email] = @OldEmail) " _
            & " AND ((@OldEmail1 IS NULL AND [Email1] IS NULL) OR [Email1] = @OldEmail1) " _
            & " AND ((@OldEmail2 IS NULL AND [Email2] IS NULL) OR [Email2] = @OldEmail2) " _
            & " AND ((@OldOrganisation IS NULL AND [Organisation] IS NULL) OR [Organisation] = @OldOrganisation) " _
            & " AND ((@OldLivreur IS NULL AND [Livreur] IS NULL) OR [Livreur] = @OldLivreur) " _
            & " AND ((@OldDateFrom IS NULL AND [DateFrom] IS NULL) OR [DateFrom] = @OldDateFrom) " _
            & " AND ((@OldDateTo IS NULL AND [DateTo] IS NULL) OR [DateTo] = @OldDateTo) " _
            & ""
        Dim updateCommand As New SqlCommand(updateStatement, connection)
        updateCommand.CommandType = CommandType.Text
        updateCommand.Parameters.AddWithValue("@NewIDPeriode", IIf(clsRapportFacturePayeNew.IDPeriode.HasValue, clsRapportFacturePayeNew.IDPeriode, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewIDOrganisation", IIf(clsRapportFacturePayeNew.IDOrganisation.HasValue, clsRapportFacturePayeNew.IDOrganisation, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewIDLivreur", IIf(clsRapportFacturePayeNew.IDLivreur.HasValue, clsRapportFacturePayeNew.IDLivreur, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewCheminRapport", IIf(Not IsNothing(clsRapportFacturePayeNew.CheminRapport), clsRapportFacturePayeNew.CheminRapport, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewNomFichier", IIf(Not IsNothing(clsRapportFacturePayeNew.NomFichier), clsRapportFacturePayeNew.NomFichier, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewEmail", IIf(Not IsNothing(clsRapportFacturePayeNew.Email), clsRapportFacturePayeNew.Email, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewEmail1", IIf(Not IsNothing(clsRapportFacturePayeNew.Email1), clsRapportFacturePayeNew.Email1, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewEmail2", IIf(Not IsNothing(clsRapportFacturePayeNew.Email2), clsRapportFacturePayeNew.Email2, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewOrganisation", IIf(Not IsNothing(clsRapportFacturePayeNew.Organisation), clsRapportFacturePayeNew.Organisation, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewLivreur", IIf(Not IsNothing(clsRapportFacturePayeNew.Livreur), clsRapportFacturePayeNew.Livreur, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewDateFrom", IIf(clsRapportFacturePayeNew.DateFrom.HasValue, clsRapportFacturePayeNew.DateFrom, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewDateTo", IIf(clsRapportFacturePayeNew.DateTo.HasValue, clsRapportFacturePayeNew.DateTo, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@OldIDRapport", clsRapportFacturePayeOld.IDRapport)
        updateCommand.Parameters.AddWithValue("@OldIDPeriode", IIf(clsRapportFacturePayeOld.IDPeriode.HasValue, clsRapportFacturePayeOld.IDPeriode, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@OldIDOrganisation", IIf(clsRapportFacturePayeOld.IDOrganisation.HasValue, clsRapportFacturePayeOld.IDOrganisation, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@OldIDLivreur", IIf(clsRapportFacturePayeOld.IDLivreur.HasValue, clsRapportFacturePayeOld.IDLivreur, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@OldCheminRapport", IIf(Not IsNothing(clsRapportFacturePayeOld.CheminRapport), clsRapportFacturePayeOld.CheminRapport, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@OldNomFichier", IIf(Not IsNothing(clsRapportFacturePayeOld.NomFichier), clsRapportFacturePayeOld.NomFichier, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@OldEmail", IIf(Not IsNothing(clsRapportFacturePayeOld.Email), clsRapportFacturePayeOld.Email, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@OldEmail1", IIf(Not IsNothing(clsRapportFacturePayeOld.Email1), clsRapportFacturePayeOld.Email1, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@OldEmail2", IIf(Not IsNothing(clsRapportFacturePayeOld.Email2), clsRapportFacturePayeOld.Email2, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@OldOrganisation", IIf(Not IsNothing(clsRapportFacturePayeOld.Organisation), clsRapportFacturePayeOld.Organisation, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@OldLivreur", IIf(Not IsNothing(clsRapportFacturePayeOld.Livreur), clsRapportFacturePayeOld.Livreur, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@OldDateFrom", IIf(clsRapportFacturePayeOld.DateFrom.HasValue, clsRapportFacturePayeOld.DateFrom, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@OldDateTo", IIf(clsRapportFacturePayeOld.DateTo.HasValue, clsRapportFacturePayeOld.DateTo, DBNull.Value))
        Try
            connection.Open()
            Dim count As Integer = updateCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

    Public Function DeletePeriod(clsRapportFacturePaye As RapportFacturePaye) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim deleteStatement As String _
            = "DELETE FROM " _
            & "     [RapportFacturePaye] " _
            & "WHERE " _
            & "     [IDPeriode] = @OldIDPeriode "


        Dim deleteCommand As New SqlCommand(deleteStatement, connection)
        deleteCommand.CommandType = CommandType.Text
        deleteCommand.Parameters.AddWithValue("@OldIDPeriode", clsRapportFacturePaye.IDPeriode)


        Try
            connection.Open()
            Dim count As Integer = deleteCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function
    Public Function DeletePeriodLCommission(clsRapportFacturePaye As RapportFacturePaye) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim deleteStatement As String _
            = "DELETE FROM " _
            & "     [RapportFacturePaye] " _
            & "WHERE " _
            & "     [IDPeriode] = @OldIDPeriode " _
            & " AND    [IDLivreur] IS NOT NULL AND IsCommissionVendeur = 1"

        Dim deleteCommand As New SqlCommand(deleteStatement, connection)
        deleteCommand.CommandType = CommandType.Text
        deleteCommand.Parameters.AddWithValue("@OldIDPeriode", clsRapportFacturePaye.IDPeriode)


        Try
            connection.Open()
            Dim count As Integer = deleteCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function
    Public Function DeletePeriodL(clsRapportFacturePaye As RapportFacturePaye) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim deleteStatement As String _
            = "DELETE FROM " _
            & "     [RapportFacturePaye] " _
            & "WHERE " _
            & "     [IDPeriode] = @OldIDPeriode " _
            & " AND    [IDLivreur] IS NOT NULL "

        Dim deleteCommand As New SqlCommand(deleteStatement, connection)
        deleteCommand.CommandType = CommandType.Text
        deleteCommand.Parameters.AddWithValue("@OldIDPeriode", clsRapportFacturePaye.IDPeriode)


        Try
            connection.Open()
            Dim count As Integer = deleteCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

    Public Function DeletePeriodO(clsRapportFacturePaye As RapportFacturePaye) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim deleteStatement As String _
            = "DELETE FROM " _
            & "     [RapportFacturePaye] " _
            & "WHERE " _
            & "     [IDPeriode] = @OldIDPeriode " _
            & " AND    [IDOrganisation] IS NOT NULL "

        Dim deleteCommand As New SqlCommand(deleteStatement, connection)
        deleteCommand.CommandType = CommandType.Text
        deleteCommand.Parameters.AddWithValue("@OldIDPeriode", clsRapportFacturePaye.IDPeriode)


        Try
            connection.Open()
            Dim count As Integer = deleteCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

    Public Function DeletePeriodOrganisation(ByVal clsRapportFacturePaye As RapportFacturePaye) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim deleteStatement As String _
            = "DELETE FROM " _
            & "     [RapportFacturePaye] " _
            & "WHERE " _
            & "     [IDPeriode] = @OldIDPeriode " _
            & " AND IDOrganisation = @OldIDOrganisation"
        Dim deleteCommand As New SqlCommand(deleteStatement, connection)
        deleteCommand.CommandType = CommandType.Text
        deleteCommand.Parameters.AddWithValue("@OldIDPeriode", clsRapportFacturePaye.IDPeriode)
        deleteCommand.Parameters.AddWithValue("@OldIDOrganisation", clsRapportFacturePaye.IDOrganisation)

        Try
            connection.Open()
            Dim count As Integer = deleteCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

    Public Function DeletePeriodLivreur(ByVal clsRapportFacturePaye As RapportFacturePaye) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim deleteStatement As String _
            = "DELETE FROM " _
            & "     [RapportFacturePaye] " _
            & "WHERE " _
            & "     [IDPeriode] = @OldIDPeriode " _
            & " AND IDLivreur = @OldIDLivreur"
        Dim deleteCommand As New SqlCommand(deleteStatement, connection)
        deleteCommand.CommandType = CommandType.Text
        deleteCommand.Parameters.AddWithValue("@OldIDPeriode", clsRapportFacturePaye.IDPeriode)
        deleteCommand.Parameters.AddWithValue("@OldIDLivreur", clsRapportFacturePaye.IDOrganisation)

        Try
            connection.Open()
            Dim count As Integer = deleteCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function


    Public Function Delete(ByVal clsRapportFacturePaye As RapportFacturePaye) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim deleteStatement As String _
            = "DELETE FROM " _
            & "     [RapportFacturePaye] " _
            & "WHERE " _
            & "     [IDRapport] = @OldIDRapport " _
            & ""
        Dim deleteCommand As New SqlCommand(deleteStatement, connection)
        deleteCommand.CommandType = CommandType.Text
        deleteCommand.Parameters.AddWithValue("@OldIDRapport", clsRapportFacturePaye.IDRapport)

        Try
            connection.Open()
            Dim count As Integer = deleteCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

End Class

