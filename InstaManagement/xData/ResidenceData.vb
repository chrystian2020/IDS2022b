Imports System.Data.SqlClient

Public Class ResidenceData

    Public Function SelectAll() As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
           & "     [IDResidence] " _
            & "    ,[Residence] " _
            & "FROM " _
            & "     [Residence] " _
            & ""
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function

    Public Function Select_Record(ByVal clsResidencePara As Residence) As Residence
        Dim clsResidence As New Residence
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDResidence] " _
            & "    ,[Residence] " _
            & "FROM " _
            & "     [Residence] " _
            & "WHERE " _
            & "     [IDResidence] = @IDResidence " _
            & ""
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        selectCommand.Parameters.AddWithValue("@IDResidence", clsResidencePara.IDResidence)
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader(CommandBehavior.SingleRow)
            If reader.Read Then
                With clsResidence
                    .IDResidence = System.Convert.ToInt32(reader("IDResidence"))
                    .Residence = If(IsDBNull(reader("Residence")), Nothing, reader("Residence").ToString)
                End With
            Else
                clsResidence = Nothing
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return clsResidence
    End Function

    Public Function Add(ByVal clsResidence As Residence) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim insertStatement As String _
            = "INSERT " _
            & "     [Residence] " _
            & "     ( " _
            & "     [Residence] " _
            & "     ) " _
            & "VALUES " _
            & "     ( " _
            & "     @Residence " _
            & "     ) " _
            & ""
        Dim insertCommand As New SqlCommand(insertStatement, connection)
        insertCommand.CommandType = CommandType.Text
        insertCommand.Parameters.AddWithValue("@Residence", IIf(Not IsNothing(clsResidence.Residence), clsResidence.Residence, DBNull.Value))
        Try
            connection.Open()
            Dim count As Integer = insertCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

    Public Function Update(ByVal clsResidenceOld As Residence,
           ByVal clsResidenceNew As Residence) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim updateStatement As String _
            = "UPDATE " _
            & "     [Residence] " _
            & "SET " _
            & "     [Residence] = @NewResidence " _
            & "WHERE " _
            & "     [IDResidence] = @OldIDResidence " _
            & ""
        Dim updateCommand As New SqlCommand(updateStatement, connection)
        updateCommand.CommandType = CommandType.Text
        updateCommand.Parameters.AddWithValue("@NewResidence", IIf(Not IsNothing(clsResidenceNew.Residence), clsResidenceNew.Residence, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@OldIDResidence", clsResidenceOld.IDResidence)

        Try
            connection.Open()
            Dim count As Integer = updateCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

    Public Function Delete(ByVal clsResidence As Residence) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim deleteStatement As String _
            = "DELETE FROM " _
            & "     [Residence] " _
            & "WHERE " _
            & "     [IDResidence] = @OldIDResidence " _
            & ""
        Dim deleteCommand As New SqlCommand(deleteStatement, connection)
        deleteCommand.CommandType = CommandType.Text
        deleteCommand.Parameters.AddWithValue("@OldIDResidence", clsResidence.IDResidence)

        Try
            connection.Open()
            Dim count As Integer = deleteCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

End Class
 
