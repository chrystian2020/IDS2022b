Imports System.Data.SqlClient

Public Class FournisseursData

    Public Function SelectAll() As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDFournisseur] " _
            & "    ,[Fournisseur] " _
            & "    ,[Description] " _
            & "FROM " _
            & "     [Fournisseurs] " _
            & ""
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function

    Public Function Select_Record(ByVal clsFournisseursPara As Fournisseurs) As Fournisseurs
        Dim clsFournisseurs As New Fournisseurs
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDFournisseur] " _
            & "    ,[Fournisseur] " _
            & "    ,[Description] " _
            & "FROM " _
            & "     [Fournisseurs] " _
            & "WHERE " _
            & "     [IDFournisseur] = @IDFournisseur " _
            & ""
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        selectCommand.Parameters.AddWithValue("@IDFournisseur", clsFournisseursPara.IDFournisseur)
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader(CommandBehavior.SingleRow)
            If reader.Read Then
                With clsFournisseurs
                    .IDFournisseur = System.Convert.ToInt32(reader("IDFournisseur"))
                    .Fournisseur = If(IsDBNull(reader("Fournisseur")), Nothing, reader("Fournisseur").ToString)
                    .Description = If(IsDBNull(reader("Description")), Nothing, reader("Description").ToString)
                End With
            Else
                clsFournisseurs = Nothing
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return clsFournisseurs
    End Function

    Public Function Add(ByVal clsFournisseurs As Fournisseurs) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim insertStatement As String _
            = "INSERT " _
            & "     [Fournisseurs] " _
            & "     ( " _
            & "     [Fournisseur] " _
            & "    ,[Description] " _
            & "     ) " _
            & "VALUES " _
            & "     ( " _
            & "     @Fournisseur " _
            & "    ,@Description " _
            & "     ) " _
            & ""
        Dim insertCommand As New SqlCommand(insertStatement, connection)
        insertCommand.CommandType = CommandType.Text
        insertCommand.Parameters.AddWithValue("@Fournisseur", IIf(Not IsNothing(clsFournisseurs.Fournisseur), clsFournisseurs.Fournisseur, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Description", IIf(Not IsNothing(clsFournisseurs.Description), clsFournisseurs.Description, DBNull.Value))
        Try
            connection.Open()
            Dim count As Integer = insertCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

    Public Function Update(ByVal clsFournisseursOld As Fournisseurs,
           ByVal clsFournisseursNew As Fournisseurs) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim updateStatement As String _
            = "UPDATE " _
            & "     [Fournisseurs] " _
            & "SET " _
            & "     [Fournisseur] = @NewFournisseur " _
            & "    ,[Description] = @NewDescription " _
            & "WHERE " _
            & "     [IDFournisseur] = @OldIDFournisseur " _
            & " AND ((@OldFournisseur IS NULL AND [Fournisseur] IS NULL) OR [Fournisseur] = @OldFournisseur) " _
            & " AND ((@OldDescription IS NULL AND [Description] IS NULL) OR [Description] = @OldDescription) " _
            & ""
        Dim updateCommand As New SqlCommand(updateStatement, connection)
        updateCommand.CommandType = CommandType.Text
        updateCommand.Parameters.AddWithValue("@NewFournisseur", IIf(Not IsNothing(clsFournisseursNew.Fournisseur), clsFournisseursNew.Fournisseur, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewDescription", IIf(Not IsNothing(clsFournisseursNew.Description), clsFournisseursNew.Description, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@OldIDFournisseur", clsFournisseursOld.IDFournisseur)
        updateCommand.Parameters.AddWithValue("@OldFournisseur", IIf(Not IsNothing(clsFournisseursOld.Fournisseur), clsFournisseursOld.Fournisseur, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@OldDescription", IIf(Not IsNothing(clsFournisseursOld.Description), clsFournisseursOld.Description, DBNull.Value))
        Try
            connection.Open()
            Dim count As Integer = updateCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

    Public Function Delete(ByVal clsFournisseurs As Fournisseurs) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim deleteStatement As String _
            = "DELETE FROM " _  
            & "     [Fournisseurs] " _
            & "WHERE " _
            & "     [IDFournisseur] = @OldIDFournisseur " _
            & " AND ((@OldFournisseur IS NULL AND [Fournisseur] IS NULL) OR [Fournisseur] = @OldFournisseur) " _
            & " AND ((@OldDescription IS NULL AND [Description] IS NULL) OR [Description] = @OldDescription) " _
            & ""
        Dim deleteCommand As New SqlCommand(deleteStatement, connection)
        deleteCommand.CommandType = CommandType.Text
        deleteCommand.Parameters.AddWithValue("@OldIDFournisseur", clsFournisseurs.IDFournisseur)
        deleteCommand.Parameters.AddWithValue("@OldFournisseur", IIf(Not IsNothing(clsFournisseurs.Fournisseur), clsFournisseurs.Fournisseur, DBNull.Value))
        deleteCommand.Parameters.AddWithValue("@OldDescription", IIf(Not IsNothing(clsFournisseurs.Description), clsFournisseurs.Description, DBNull.Value))
        Try
            connection.Open()
            Dim count As Integer = deleteCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

End Class
 
