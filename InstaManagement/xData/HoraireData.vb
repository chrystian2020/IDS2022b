Imports System.Data.SqlClient

Public Class HoraireData
    Public Function SelectAlldistint(pIDOrganisation As Integer) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT DISTINCT" _
        & "    ,[IDOrganisation] " _
        & " FROM  Horaire " _
        & " WHERE  IDOrganisation=" & pIDOrganisation _
        & " AND IDPeriode=" & go_Globals.IDPeriode _
        & " ORDER BY dbo.Horaire.IDHoraire,dbo.Horaire.IDLivreur"


        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function

    Public Function SelectAllByIDOrganisationIDLivreurGrille(pIDOrganisation As Integer, pIDLivreur As Integer) As DataTable
        Dim connection As SqlConnection = DB.GetConnection

        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDHoraire] " _
            & "    ,[IDOrganisation] " _
            & "    ,[IDLivreur] " _
            & "    ,[HeureDepart] " _
            & "    ,[HeureFin] " _
            & "    ,[Livreur] " _
            & "    ,[Organisation] " _
            & "    ,[MontantLivreur] " _
            & "    ,[MontantOrganisation] " _
            & "    ,[IDPeriode] " _
            & " FROM  Horaire " _
            & " WHERE  IDOrganisation=" & pIDOrganisation _
            & " AND IDLivreur=" & pIDLivreur _
            & " AND IDPeriode=" & go_Globals.IDPeriode




        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function
    Public Function SelectAllByIDOrganisationIDLivreurParHeure(pIDOrganisation As Integer, pIDLivreur As Integer) As DataTable
        Dim connection As SqlConnection = DB.GetConnection

        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDLivreur] " _
            & "    ,[MontantLivreur] " _
            & "    ,(SUM(CAST(DATEDIFF(MINUTE,HeureDepart, HeureFin) AS FLOAT)) / 60) AS TotalHour " _
            & "    ,MontantLivreur * (SUM(CAST(DATEDIFF(MINUTE,HeureDepart, HeureFin) AS FLOAT)) / 60) as MontantTotal " _
            & "    ,[Livreur] " _
            & " FROM  Horaire " _
            & " WHERE  IDOrganisation=" & pIDOrganisation _
            & " AND IDLivreur=" & pIDLivreur _
            & " AND IDPeriode=" & go_Globals.IDPeriode _
            & " GROUP BY [IDLivreur],[Livreur],[MontantLivreur]"



        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function
    Public Function SelectAllByIDOrganisationParHeure(pIDOrganisation As Integer) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDLivreur] " _
            & "    ,[MontantOrganisation] " _
            & "    ,(SUM(CAST(DATEDIFF(MINUTE,HeureDepart, HeureFin) AS FLOAT)) / 60) AS TotalHour " _
            & "    ,MontantOrganisation * (SUM(CAST(DATEDIFF(MINUTE,HeureDepart, HeureFin) AS FLOAT)) / 60) as MontantTotal " _
            & "    ,[Livreur] " _
            & " FROM  Horaire " _
            & " WHERE  IDOrganisation=" & pIDOrganisation _
            & " AND IDPeriode=" & go_Globals.IDPeriode _
            & " AND IDLivreur is not NULL GROUP BY [IDLivreur],[Livreur],[MontantOrganisation]" _
            & " ORDER BY [IDLivreur]"


        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function
    Public Function Select_Record(ByVal clsHorairePara As Horaire) As Horaire
        Dim clsHoraire As New Horaire
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDHoraire] " _
            & "    ,[IDOrganisation] " _
            & "    ,[IDLivreur] " _
            & "    ,[HeureDepart] " _
            & "    ,[HeureFin] " _
            & "    ,[Livreur] " _
            & "    ,[Organisation] " _
            & "    ,[MontantLivreur] " _
            & "    ,[MontantOrganisation] " _
            & "    ,[IDPeriode] " _
            & "FROM " _
            & "     [Horaire] " _
            & "WHERE " _
            & "     [IDHoraire] = @IDHoraire " _
            & ""
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        selectCommand.Parameters.AddWithValue("@IDHoraire", clsHorairePara.IDHoraire)
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader(CommandBehavior.SingleRow)
            If reader.Read Then
                With clsHoraire
                    .IDHoraire = System.Convert.ToInt32(reader("IDHoraire"))
                    .IDOrganisation = If(IsDBNull(reader("IDOrganisation")), Nothing, CType(reader("IDOrganisation"), Int32?))
                    .IDLivreur = If(IsDBNull(reader("IDLivreur")), Nothing, CType(reader("IDLivreur"), Int32?))
                    .HeureDepart = If(IsDBNull(reader("HeureDepart")), Nothing, CType(reader("HeureDepart"), DateTime?))
                    .HeureFin = If(IsDBNull(reader("HeureFin")), Nothing, CType(reader("HeureFin"), DateTime?))
                    .Livreur = If(IsDBNull(reader("Livreur")), Nothing, reader("Livreur").ToString)
                    .Organisation = If(IsDBNull(reader("Organisation")), Nothing, reader("Organisation").ToString)
                    .MontantLivreur = If(IsDBNull(reader("MontantLivreur")), Nothing, CType(reader("MontantLivreur"), Decimal?))
                    .MontantOrganisation = If(IsDBNull(reader("MontantOrganisation")), Nothing, CType(reader("MontantOrganisation"), Decimal?))
                    .IDPeriode = System.Convert.ToInt32(reader("IDPeriode"))

                End With
            Else
                clsHoraire = Nothing
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return clsHoraire
    End Function

    Public Function Add(ByVal clsHoraire As Horaire) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim insertStatement As String _
            = "INSERT " _
            & "     [Horaire] " _
            & "     ( " _
            & "     [IDOrganisation] " _
            & "    ,[IDLivreur] " _
            & "    ,[HeureDepart] " _
            & "    ,[HeureFin] " _
            & "    ,[Livreur] " _
            & "    ,[Organisation] " _
            & "    ,[MontantLivreur] " _
            & "    ,[MontantOrganisation] " _
            & "    ,[IDPeriode] " _
            & "     ) " _
            & "VALUES " _
            & "     ( " _
            & "     @IDOrganisation " _
            & "    ,@IDLivreur " _
            & "    ,@HeureDepart " _
            & "    ,@HeureFin " _
            & "    ,@Livreur " _
            & "    ,@Organisation " _
            & "    ,@MontantLivreur " _
            & "    ,@MontantOrganisation " _
            & "    ,@IDPeriode " _
            & "     ) " _
            & ""
        Dim insertCommand As New SqlCommand(insertStatement, connection)
        insertCommand.CommandType = CommandType.Text
        insertCommand.Parameters.AddWithValue("@IDOrganisation", IIf(clsHoraire.IDOrganisation.HasValue, clsHoraire.IDOrganisation, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@IDLivreur", IIf(clsHoraire.IDLivreur.HasValue, clsHoraire.IDLivreur, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@HeureDepart", IIf(clsHoraire.HeureDepart.HasValue, clsHoraire.HeureDepart, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@HeureFin", IIf(clsHoraire.HeureFin.HasValue, clsHoraire.HeureFin, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Livreur", IIf(Not IsNothing(clsHoraire.Livreur), clsHoraire.Livreur, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Organisation", IIf(Not IsNothing(clsHoraire.Organisation), clsHoraire.Organisation, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@MontantLivreur", IIf(clsHoraire.MontantLivreur.HasValue, clsHoraire.MontantLivreur, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@MontantOrganisation", IIf(clsHoraire.MontantOrganisation.HasValue, clsHoraire.MontantOrganisation, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@IDPeriode", IIf(clsHoraire.IDPeriode.HasValue, clsHoraire.IDPeriode, DBNull.Value))

        Try
            connection.Open()
            Dim count As Integer = insertCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

    Public Function Update(ByVal clsHoraireOld As Horaire,
           ByVal clsHoraireNew As Horaire) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim updateStatement As String _
            = "UPDATE " _
            & "     [Horaire] " _
            & "SET " _
            & "     [IDOrganisation] = @NewIDOrganisation " _
            & "    ,[IDLivreur] = @NewIDLivreur " _
            & "    ,[HeureDepart] = @NewHeureDepart " _
            & "    ,[HeureFin] = @NewHeureFin " _
            & "    ,[Livreur] = @NewLivreur " _
            & "    ,[Organisation] = @NewOrganisation " _
            & "    ,[MontantLivreur] = @NewMontantLivreur " _
            & "    ,[MontantOrganisation] = @NewMontantOrganisation " _
            & "    ,[IDPeriode] = @NewIDPeriode " _
            & " WHERE " _
            & "     [IDHoraire] = @OldIDHoraire " _
            & ""
        Dim updateCommand As New SqlCommand(updateStatement, connection)
        updateCommand.CommandType = CommandType.Text
        updateCommand.Parameters.AddWithValue("@NewIDOrganisation", IIf(clsHoraireNew.IDOrganisation.HasValue, clsHoraireNew.IDOrganisation, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewIDLivreur", IIf(clsHoraireNew.IDLivreur.HasValue, clsHoraireNew.IDLivreur, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewHeureDepart", IIf(clsHoraireNew.HeureDepart.HasValue, clsHoraireNew.HeureDepart, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewHeureFin", IIf(clsHoraireNew.HeureFin.HasValue, clsHoraireNew.HeureFin, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewLivreur", IIf(Not IsNothing(clsHoraireNew.Livreur), clsHoraireNew.Livreur, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewOrganisation", IIf(Not IsNothing(clsHoraireNew.Organisation), clsHoraireNew.Organisation, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewMontantLivreur", IIf(clsHoraireNew.MontantLivreur.HasValue, clsHoraireNew.MontantLivreur, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewMontantOrganisation", IIf(clsHoraireNew.MontantOrganisation.HasValue, clsHoraireNew.MontantOrganisation, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewIDPeriode", IIf(clsHoraireNew.IDPeriode.HasValue, clsHoraireNew.IDPeriode, DBNull.Value))

        updateCommand.Parameters.AddWithValue("@OldIDHoraire", clsHoraireOld.IDHoraire)

        Try
            connection.Open()
            Dim count As Integer = updateCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

    Public Function Delete(ByVal clsHoraire As Horaire) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim deleteStatement As String _
            = "DELETE FROM " _
            & "     [Horaire] " _
            & "WHERE " _
            & "     [IDHoraire] = @OldIDHoraire " _
            & ""
        Dim deleteCommand As New SqlCommand(deleteStatement, connection)
        deleteCommand.CommandType = CommandType.Text
        deleteCommand.Parameters.AddWithValue("@OldIDHoraire", clsHoraire.IDHoraire)

        Try
            connection.Open()
            Dim count As Integer = deleteCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

End Class
 
