Imports System.Data.SqlClient

Public Class GlobalQuery




    Public Function NettoyerBD() As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim CMD As New SqlCommand("NettoyerBD")
        CMD.Connection = connection
        CMD.CommandType = CommandType.StoredProcedure

        Dim adapter As New SqlDataAdapter(CMD)
        adapter.SelectCommand.CommandTimeout = 300


        Try
            connection.Open()
            Dim count As Integer = CMD.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

    Public Function Update(ByVal clsAdminOld As Admin,
           ByVal clsAdminNew As Admin) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim updateStatement As String _
            = "UPDATE " _
            & "     [Admin] " _
            & "SET " _
            & "     [TPS] = @NewTPS " _
            & "    ,[TVQ] = @NewTVQ " _
            & "    ,[TPSNumero] = @NewTPSNumero " _
            & "    ,[TVQNumero] = @NewTVQNumero " _
            & "    ,[CompanyName] = @NewCompanyName " _
            & "    ,[CompanyAdresse] = @NewCompanyAdresse " _
            & "    ,[CompanyContact] = @NewCompanyContact " _
            & "    ,[CompanyContactTelephone] = @NewCompanyContactTelephone " _
            & "    ,[LocationPaie] = @NewLocationPaie " _
            & "    ,[LocationFacture] = @NewLocationFacture " _
            & "    ,[Server] = @NewServer " _
            & "    ,[Username] = @NewUsername " _
            & "    ,[Password] = @NewPassword " _
            & "    ,[Port] = @NewPort " _
            & "    ,[UseSSL] = @NewUseSSL " _
            & ""
        Dim updateCommand As New SqlCommand(updateStatement, connection)
        updateCommand.CommandType = CommandType.Text
        updateCommand.Parameters.AddWithValue("@NewTPS", IIf(clsAdminNew.TPS.HasValue, clsAdminNew.TPS, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewTVQ", IIf(clsAdminNew.TVQ.HasValue, clsAdminNew.TVQ, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewTPSNumero", IIf(Not IsNothing(clsAdminNew.TPSNumero), clsAdminNew.TPSNumero, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewTVQNumero", IIf(Not IsNothing(clsAdminNew.TVQNumero), clsAdminNew.TVQNumero, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewCompanyName", IIf(Not IsNothing(clsAdminNew.CompanyName), clsAdminNew.CompanyName, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewCompanyAdresse", IIf(Not IsNothing(clsAdminNew.CompanyAdresse), clsAdminNew.CompanyAdresse, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewCompanyContact", IIf(Not IsNothing(clsAdminNew.CompanyContact), clsAdminNew.CompanyContact, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewCompanyContactTelephone", IIf(Not IsNothing(clsAdminNew.CompanyContactTelephone), clsAdminNew.CompanyContactTelephone, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewLocationPaie", IIf(Not IsNothing(clsAdminNew.LocationPaie), clsAdminNew.LocationPaie, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewLocationFacture", IIf(Not IsNothing(clsAdminNew.LocationFacture), clsAdminNew.LocationFacture, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewServer", IIf(Not IsNothing(clsAdminNew.Server), clsAdminNew.Server, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewUsername", IIf(Not IsNothing(clsAdminNew.Username), clsAdminNew.Username, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewPassword", IIf(Not IsNothing(clsAdminNew.Password), clsAdminNew.Password, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewPort", IIf(clsAdminNew.Port.HasValue, clsAdminNew.Port, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewUseSSL", IIf(clsAdminNew.UseSSL.HasValue, clsAdminNew.UseSSL, DBNull.Value))

        Try
            connection.Open()
            Dim count As Integer = updateCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function



End Class

