Imports System.Data.SqlClient

Public Class HoraireJourData


    Public Function SelectAlldistint(pIDOrganisation As Integer) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT DISTINCT" _
        & "    ,[IDOrganisation] " _
        & " FROM  Horaire " _
        & " WHERE  IDOrganisation=" & pIDOrganisation _
        & " AND IDPeriode=" & go_Globals.IDPeriode _
        & " ORDER BY dbo.Horaire.IDHoraire,dbo.Horaire.IDLivreur"


        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function

    Public Function SelectAllByIDOrganisationParJour(pIDOrganisation As Integer) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDLivreur] " _
            & "    ,[MontantOrganisation] " _
            & "    ,Count(*) AS TotalDay " _
            & "    ,CAST(SUM(MontantOrganisation) AS Float) as MontantTotal " _
            & "    ,[Livreur] " _
            & " FROM  Horaire " _
            & " WHERE  IDOrganisation=" & pIDOrganisation _
            & " AND IDPeriode=" & go_Globals.IDPeriode _
            & " GROUP BY [IDLivreur],[Livreur],[MontantOrganisation]" _
            & " ORDER BY [IDLivreur]"


        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function

    Public Function SelectAllByIDOrganisationIDLivreurParJourGrille(pIDOrganisation As Integer, pIDLivreur As Integer) As DataTable
        Dim connection As SqlConnection = DB.GetConnection

        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDHoraireJour] " _
            & "    ,[IDOrganisation] " _
            & "    ,[IDLivreur] " _
            & "    ,[DateTravail] " _
            & "    ,[Livreur] " _
            & "    ,[Organisation] " _
            & "    ,[MontantLivreur] " _
            & "    ,[MontantOrganisation] " _
            & "    ,[IDPeriode] " _
            & " FROM  HoraireJour " _
            & " WHERE  IDOrganisation=" & pIDOrganisation _
            & " AND IDLivreur=" & pIDLivreur _
            & " AND IDPeriode=" & go_Globals.IDPeriode _
            & " GROUP BY [IDLivreur],[Livreur],[MontantLivreur]"



        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function
    Public Function SelectAllByIDOrganisationIDLivreurParJour(pIDOrganisation As Integer, pIDLivreur As Integer) As DataTable
        Dim connection As SqlConnection = DB.GetConnection

        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDLivreur] " _
            & "    ,[MontantLivreur] " _
            & "    ,Count(*) AS TotalDay " _
            & "    ,CAST(SUM(MontantLivreur) AS Float) AS MontantTotal " _
            & "    ,[Livreur] " _
            & " FROM  HoraireJour " _
            & " WHERE  IDOrganisation=" & pIDOrganisation _
            & " AND IDLivreur=" & pIDLivreur _
            & " AND IDPeriode=" & go_Globals.IDPeriode _
            & " GROUP BY [IDLivreur],[Livreur],[MontantLivreur]"



        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function

    Public Function SelectAllByIDOrganisationIDLivreur2(pIDOrganisation As Integer, pIDLivreur As Integer) As DataTable
        Dim connection As SqlConnection = DB.GetConnection

        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDLivreur] " _
            & "    ,[MontantLivreur] " _
            & "    ,(DATEDIFF(DAY,HeureDepart, HeureFin)) AS TotalDay " _
            & "    ,MontantLivreur * (DATEDIFF(DAY,HeureDepart, HeureFin)) as MontantTotal " _
            & "    ,[Livreur] " _
            & " FROM  Horaire " _
            & " WHERE  IDOrganisation=" & pIDOrganisation _
            & " AND IDLivreur=" & pIDLivreur _
            & " AND IDPeriode=" & go_Globals.IDPeriode _
            & " GROUP BY [IDLivreur],[Livreur],[MontantLivreur]"



        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function



    Public Function SelectAllByIDOrganisation(pIDOrganisation As Integer) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDHoraireJour] " _
            & "    ,[IDOrganisation] " _
            & "    ,[IDLivreur] " _
            & "    ,[DateTravail] " _
            & "    ,[Livreur] " _
            & "    ,[Organisation] " _
            & "    ,[MontantLivreur] " _
            & "    ,[MontantOrganisation] " _
            & "    ,[IDPeriode] " _
        & " FROM  Horaire " _
        & " WHERE  IDOrganisation=" & pIDOrganisation _
        & " AND IDPeriode=" & go_Globals.IDPeriode _
        & " ORDER BY dbo.Horaire.IDHoraireJour,dbo.Horaire.IDLivreur"


        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function
    Public Function SelectAll() As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDHoraireJour] " _
            & "    ,[IDOrganisation] " _
            & "    ,[IDLivreur] " _
            & "    ,[DateTravail] " _
            & "    ,[Livreur] " _
            & "    ,[Organisation] " _
            & "    ,[MontantLivreur] " _
            & "    ,[MontantOrganisation] " _
            & "    ,[IDPeriode] " _
            & "FROM " _
            & "     [HoraireJour] " _
            & ""
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function


    Public Function SelectAllByIDOrganisationIDLivreur(pIDOrganisation As Integer, pIDLivreur As Integer) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDHoraireJour] " _
            & "    ,[IDOrganisation] " _
            & "    ,[IDLivreur] " _
            & "    ,[DateTravail] " _
            & "    ,[Livreur] " _
            & "    ,[Organisation] " _
            & "    ,[MontantLivreur] " _
            & "    ,[MontantOrganisation] " _
            & "    ,[IDPeriode] " _
            & " FROM HoraireJour " _
        & " WHERE  IDOrganisation=" & pIDOrganisation _
        & " AND IDLivreur=" & pIDLivreur _
        & " AND IDPeriode =" & go_Globals.IDPeriode _
        & " ORDER BY dbo.Horaire.IDHoraireJour"


        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function


    Public Function Select_Record(ByVal clsHoraireJourPara As HoraireJour) As HoraireJour
        Dim clsHoraireJour As New HoraireJour
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDHoraireJour] " _
            & "    ,[IDOrganisation] " _
            & "    ,[IDLivreur] " _
            & "    ,[DateTravail] " _
            & "    ,[Livreur] " _
            & "    ,[Organisation] " _
            & "    ,[MontantLivreur] " _
            & "    ,[MontantOrganisation] " _
            & "    ,[IDPeriode] " _
            & "FROM " _
            & "     [HoraireJour] " _
            & "WHERE " _
            & "     [IDHoraireJour] = @IDHoraireJour " _
            & ""
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        selectCommand.Parameters.AddWithValue("@IDHoraireJour", clsHoraireJourPara.IDHoraireJour)
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader(CommandBehavior.SingleRow)
            If reader.Read Then
                With clsHoraireJour
                    .IDHoraireJour = System.Convert.ToInt32(reader("IDHoraireJour"))
                    .IDOrganisation = If(IsDBNull(reader("IDOrganisation")), Nothing, CType(reader("IDOrganisation"), Int32?))
                    .IDLivreur = If(IsDBNull(reader("IDLivreur")), Nothing, CType(reader("IDLivreur"), Int32?))
                    .DateTravail = If(IsDBNull(reader("DateTravail")), Nothing, CType(reader("DateTravail"), DateTime?))
                    .Livreur = If(IsDBNull(reader("Livreur")), Nothing, reader("Livreur").ToString)
                    .Organisation = If(IsDBNull(reader("Organisation")), Nothing, reader("Organisation").ToString)
                    .MontantLivreur = If(IsDBNull(reader("MontantLivreur")), Nothing, CType(reader("MontantLivreur"), Decimal?))
                    .MontantOrganisation = If(IsDBNull(reader("MontantOrganisation")), Nothing, CType(reader("MontantOrganisation"), Decimal?))
                    .IDPeriode = If(IsDBNull(reader("IDPeriode")), Nothing, CType(reader("IDPeriode"), Int32?))
                End With
            Else
                clsHoraireJour = Nothing
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return clsHoraireJour
    End Function

    Public Function Add(ByVal clsHoraireJour As HoraireJour) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim insertStatement As String _
            = "INSERT " _
            & "     [HoraireJour] " _
            & "     ( " _
            & "     [IDOrganisation] " _
            & "    ,[IDLivreur] " _
            & "    ,[DateTravail] " _
            & "    ,[Livreur] " _
            & "    ,[Organisation] " _
            & "    ,[MontantLivreur] " _
            & "    ,[MontantOrganisation] " _
            & "    ,[IDPeriode] " _
            & "     ) " _
            & "VALUES " _
            & "     ( " _
            & "     @IDOrganisation " _
            & "    ,@IDLivreur " _
            & "    ,@DateTravail " _
            & "    ,@Livreur " _
            & "    ,@Organisation " _
            & "    ,@MontantLivreur " _
            & "    ,@MontantOrganisation " _
            & "    ,@IDPeriode " _
            & "     ) " _
            & ""
        Dim insertCommand As New SqlCommand(insertStatement, connection)
        insertCommand.CommandType = CommandType.Text
        insertCommand.Parameters.AddWithValue("@IDOrganisation", IIf(clsHoraireJour.IDOrganisation.HasValue, clsHoraireJour.IDOrganisation, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@IDLivreur", IIf(clsHoraireJour.IDLivreur.HasValue, clsHoraireJour.IDLivreur, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@DateTravail", IIf(clsHoraireJour.DateTravail.HasValue, clsHoraireJour.DateTravail, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Livreur", IIf(Not IsNothing(clsHoraireJour.Livreur), clsHoraireJour.Livreur, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Organisation", IIf(Not IsNothing(clsHoraireJour.Organisation), clsHoraireJour.Organisation, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@MontantLivreur", IIf(clsHoraireJour.MontantLivreur.HasValue, clsHoraireJour.MontantLivreur, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@MontantOrganisation", IIf(clsHoraireJour.MontantOrganisation.HasValue, clsHoraireJour.MontantOrganisation, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@IDPeriode", IIf(clsHoraireJour.IDPeriode.HasValue, clsHoraireJour.IDPeriode, DBNull.Value))
        Try
            connection.Open()
            Dim count As Integer = insertCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

    Public Function Update(ByVal clsHoraireJourOld As HoraireJour,
           ByVal clsHoraireJourNew As HoraireJour) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim updateStatement As String _
            = "UPDATE " _
            & "     [HoraireJour] " _
            & "SET " _
            & "     [IDOrganisation] = @NewIDOrganisation " _
            & "    ,[IDLivreur] = @NewIDLivreur " _
            & "    ,[DateTravail] = @NewDateTravail " _
            & "    ,[Livreur] = @NewLivreur " _
            & "    ,[Organisation] = @NewOrganisation " _
            & "    ,[MontantLivreur] = @NewMontantLivreur " _
            & "    ,[MontantOrganisation] = @NewMontantOrganisation " _
            & "    ,[IDPeriode] = @NewIDPeriode " _
            & "WHERE " _
            & "     [IDHoraireJour] = @OldIDHoraireJour " _
            & ""
        Dim updateCommand As New SqlCommand(updateStatement, connection)
        updateCommand.CommandType = CommandType.Text
        updateCommand.Parameters.AddWithValue("@NewIDOrganisation", IIf(clsHoraireJourNew.IDOrganisation.HasValue, clsHoraireJourNew.IDOrganisation, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewIDLivreur", IIf(clsHoraireJourNew.IDLivreur.HasValue, clsHoraireJourNew.IDLivreur, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewDateTravail", IIf(clsHoraireJourNew.DateTravail.HasValue, clsHoraireJourNew.DateTravail, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewLivreur", IIf(Not IsNothing(clsHoraireJourNew.Livreur), clsHoraireJourNew.Livreur, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewOrganisation", IIf(Not IsNothing(clsHoraireJourNew.Organisation), clsHoraireJourNew.Organisation, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewMontantLivreur", IIf(clsHoraireJourNew.MontantLivreur.HasValue, clsHoraireJourNew.MontantLivreur, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewMontantOrganisation", IIf(clsHoraireJourNew.MontantOrganisation.HasValue, clsHoraireJourNew.MontantOrganisation, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewIDPeriode", IIf(clsHoraireJourNew.IDPeriode.HasValue, clsHoraireJourNew.IDPeriode, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@OldIDHoraireJour", clsHoraireJourOld.IDHoraireJour)

        Try
            connection.Open()
            Dim count As Integer = updateCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

    Public Function Delete(ByVal clsHoraireJour As HoraireJour) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim deleteStatement As String _
            = "DELETE FROM " _
            & "     [HoraireJour] " _
            & "WHERE " _
            & "     [IDHoraireJour] = @OldIDHoraireJour " _
            & ""
        Dim deleteCommand As New SqlCommand(deleteStatement, connection)
        deleteCommand.CommandType = CommandType.Text
        deleteCommand.Parameters.AddWithValue("@OldIDHoraireJour", clsHoraireJour.IDHoraireJour)
        Try
            connection.Open()
            Dim count As Integer = deleteCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

End Class

