Imports System.Data.SqlClient

Public Class CommissionParHeureData
    Public Function SelectAllByName(piOrganisationID As Integer) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDCommissionParHeure] " _
            & "    ,[OrganisationID] " _
            & "    ,[Organisation] " _
            & "    ,[LivreurID] " _
            & "    ,[Livreur] " _
            & "    ,[MontantParHeure] " _
            & "FROM " _
            & "     [CommissionParHeure] " _
            & " WHERE OrganisationID =" & piOrganisationID



        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Erreur")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function


    Public Function SelectByName(piLivreur As Integer, piOrganisationID As Integer) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDCommissionParHeure] " _
            & "    ,[OrganisationID] " _
            & "    ,[Organisation] " _
            & "    ,[LivreurID] " _
            & "    ,[Livreur] " _
            & "    ,[MontantParHeure] " _
            & "FROM " _
            & "     [CommissionParHeure] " _
            & " WHERE Livreur =" & piLivreur & " AND Organisation=" & piOrganisationID

        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Erreur")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function
    Public Function SelectAll() As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDCommissionParHeure] " _
            & "    ,[OrganisationID] " _
            & "    ,[Organisation] " _
            & "    ,[LivreurID] " _
            & "    ,[Livreur] " _
            & "    ,[MontantParHeure] " _
            & "FROM " _
            & "     [CommissionParHeure] " _
            & ""
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Erreur")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function

    Public Function Select_Record(ByVal clsCommissionParHeurePara As CommissionParHeure) As CommissionParHeure
        Dim clsCommissionParHeure As New CommissionParHeure
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDCommissionParHeure] " _
            & "    ,[OrganisationID] " _
            & "    ,[Organisation] " _
            & "    ,[LivreurID] " _
            & "    ,[Livreur] " _
            & "    ,[MontantParHeure] " _
            & "FROM " _
            & "     [CommissionParHeure] " _
            & "WHERE " _
            & "     [IDCommissionParHeure] = @IDCommissionParHeure " _
            & ""
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        selectCommand.Parameters.AddWithValue("@IDCommissionParHeure", clsCommissionParHeurePara.IDCommissionParHeure)
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader(CommandBehavior.SingleRow)
            If reader.Read Then
                With clsCommissionParHeure
                    .IDCommissionParHeure = System.Convert.ToInt32(reader("IDCommissionParHeure"))
                    .OrganisationID = If(IsDBNull(reader("OrganisationID")), Nothing, CType(reader("OrganisationID"), Int32?))
                    .Organisation = If(IsDBNull(reader("Organisation")), Nothing, reader("Organisation").ToString)
                    .LivreurID = If(IsDBNull(reader("LivreurID")), Nothing, CType(reader("LivreurID"), Int32?))
                    .Livreur = If(IsDBNull(reader("Livreur")), Nothing, reader("Livreur").ToString)
                    .MontantParHeure = If(IsDBNull(reader("MontantParHeure")), Nothing, CType(reader("MontantParHeure"), Decimal?))
                End With
            Else
                clsCommissionParHeure = Nothing
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return clsCommissionParHeure
    End Function

    Public Function Add(ByVal clsCommissionParHeure As CommissionParHeure) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim insertStatement As String _
            = "INSERT " _
            & "     [CommissionParHeure] " _
            & "     ( " _
            & "     [OrganisationID] " _
            & "    ,[Organisation] " _
            & "    ,[LivreurID] " _
            & "    ,[Livreur] " _
            & "    ,[MontantParHeure] " _
            & "     ) " _
            & "VALUES " _
            & "     ( " _
            & "     @OrganisationID " _
            & "    ,@Organisation " _
            & "    ,@LivreurID " _
            & "    ,@Livreur " _
            & "    ,@MontantParHeure " _
            & "     ) " _
            & ""
        Dim insertCommand As New SqlCommand(insertStatement, connection)
        insertCommand.CommandType = CommandType.Text
        insertCommand.Parameters.AddWithValue("@OrganisationID", IIf(clsCommissionParHeure.OrganisationID.HasValue, clsCommissionParHeure.OrganisationID, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Organisation", IIf(Not IsNothing(clsCommissionParHeure.Organisation), clsCommissionParHeure.Organisation, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@LivreurID", IIf(clsCommissionParHeure.LivreurID.HasValue, clsCommissionParHeure.LivreurID, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Livreur", IIf(Not IsNothing(clsCommissionParHeure.Livreur), clsCommissionParHeure.Livreur, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@MontantParHeure", IIf(clsCommissionParHeure.MontantParHeure.HasValue, clsCommissionParHeure.MontantParHeure, DBNull.Value))
        Try
            connection.Open()
            Dim count As Integer = insertCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

    Public Function Update(ByVal clsCommissionParHeureOld As CommissionParHeure,
           ByVal clsCommissionParHeureNew As CommissionParHeure) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim updateStatement As String _
            = "UPDATE " _
            & "     [CommissionParHeure] " _
            & "SET " _
            & "     [OrganisationID] = @NewOrganisationID " _
            & "    ,[Organisation] = @NewOrganisation " _
            & "    ,[LivreurID] = @NewLivreurID " _
            & "    ,[Livreur] = @NewLivreur " _
            & "    ,[MontantParHeure] = @NewMontantParHeure " _
            & "WHERE " _
            & "     [IDCommissionParHeure] = @OldIDCommissionParHeure " _
            & " AND ((@OldOrganisationID IS NULL AND [OrganisationID] IS NULL) OR [OrganisationID] = @OldOrganisationID) " _
            & " AND ((@OldOrganisation IS NULL AND [Organisation] IS NULL) OR [Organisation] = @OldOrganisation) " _
            & " AND ((@OldLivreurID IS NULL AND [LivreurID] IS NULL) OR [LivreurID] = @OldLivreurID) " _
            & " AND ((@OldLivreur IS NULL AND [Livreur] IS NULL) OR [Livreur] = @OldLivreur) " _
            & " AND ((@OldMontantParHeure IS NULL AND [MontantParHeure] IS NULL) OR [MontantParHeure] = @OldMontantParHeure) " _
            & ""
        Dim updateCommand As New SqlCommand(updateStatement, connection)
        updateCommand.CommandType = CommandType.Text
        updateCommand.Parameters.AddWithValue("@NewOrganisationID", IIf(clsCommissionParHeureNew.OrganisationID.HasValue, clsCommissionParHeureNew.OrganisationID, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewOrganisation", IIf(Not IsNothing(clsCommissionParHeureNew.Organisation), clsCommissionParHeureNew.Organisation, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewLivreurID", IIf(clsCommissionParHeureNew.LivreurID.HasValue, clsCommissionParHeureNew.LivreurID, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewLivreur", IIf(Not IsNothing(clsCommissionParHeureNew.Livreur), clsCommissionParHeureNew.Livreur, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewMontantParHeure", IIf(clsCommissionParHeureNew.MontantParHeure.HasValue, clsCommissionParHeureNew.MontantParHeure, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@OldIDCommissionParHeure", clsCommissionParHeureOld.IDCommissionParHeure)
        updateCommand.Parameters.AddWithValue("@OldOrganisationID", IIf(clsCommissionParHeureOld.OrganisationID.HasValue, clsCommissionParHeureOld.OrganisationID, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@OldOrganisation", IIf(Not IsNothing(clsCommissionParHeureOld.Organisation), clsCommissionParHeureOld.Organisation, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@OldLivreurID", IIf(clsCommissionParHeureOld.LivreurID.HasValue, clsCommissionParHeureOld.LivreurID, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@OldLivreur", IIf(Not IsNothing(clsCommissionParHeureOld.Livreur), clsCommissionParHeureOld.Livreur, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@OldMontantParHeure", IIf(clsCommissionParHeureOld.MontantParHeure.HasValue, clsCommissionParHeureOld.MontantParHeure, DBNull.Value))
        Try
            connection.Open()
            Dim count As Integer = updateCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

    Public Function Delete(ByVal clsCommissionParHeure As CommissionParHeure) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim deleteStatement As String _
            = "DELETE FROM " _
            & "     [CommissionParHeure] " _
            & "WHERE " _
            & "     [IDCommissionParHeure] = @OldIDCommissionParHeure " _
            & ""
        Dim deleteCommand As New SqlCommand(deleteStatement, connection)
        deleteCommand.CommandType = CommandType.Text
        deleteCommand.Parameters.AddWithValue("@OldIDCommissionParHeure", clsCommissionParHeure.IDCommissionParHeure)
        Try
            connection.Open()
            Dim count As Integer = deleteCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Erreur")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

End Class
 
