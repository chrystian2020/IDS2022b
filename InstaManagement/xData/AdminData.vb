Imports System.Data.SqlClient

Public Class AdminData

    Public Function SelectAll() As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [TPS] " _
            & "    ,[TVQ] " _
            & "    ,[TPSNumero] " _
            & "    ,[TVQNumero] " _
            & "    ,[CompanyName] " _
            & "    ,[CompanyAdresse] " _
            & "    ,[CompanyContact] " _
            & "    ,[CompanyContactTelephone] " _
            & "    ,[LocationPaie] " _
            & "    ,[LocationFacture] " _
            & "    ,[Server] " _
            & "    ,[Username] " _
            & "    ,[Password] " _
            & "    ,[Port] " _
            & "    ,[UseSSL] " _
            & "    ,[LocationConsolidation] " _
            & "    ,[LocationVentes] " _
            & "    ,[LocationAchats] " _
            & "    ,[LocationCommissions] " _
            & "    ,[LocationRapportLivreur] " _
            & "    ,[LocationRapportRestaurant] " _
            & "    ,[LocationRapportDeTaxation] " _
            & "    ,[LocationRapportDeTaxationIDS] " _
            & "    ,[RapportTaxationLivreurPaye] " _
            & "FROM " _
            & "     [Admin] " _
            & ""
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function

    Public Function Select_Record(ByVal clsAdminPara As Admin) As Admin
        Dim clsAdmin As New Admin
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [TPS] " _
            & "    ,[TVQ] " _
            & "    ,[TPSNumero] " _
            & "    ,[TVQNumero] " _
            & "    ,[CompanyName] " _
            & "    ,[CompanyAdresse] " _
            & "    ,[CompanyContact] " _
            & "    ,[CompanyContactTelephone] " _
            & "    ,[LocationPaie] " _
            & "    ,[LocationFacture] " _
            & "    ,[Server] " _
            & "    ,[Username] " _
            & "    ,[Password] " _
            & "    ,[Port] " _
            & "    ,[UseSSL] " _
            & "    ,[LocationConsolidation] " _
            & "    ,[LocationVentes] " _
            & "    ,[LocationAchats] " _
            & "    ,[LocationCommissions] " _
            & "    ,[LocationRapportLivreur] " _
            & "    ,[LocationRapportRestaurant] " _
            & "    ,[LocationRapportDeTaxation] " _
            & "    ,[LocationRapportDeTaxationIDS] " _
            & "    ,[RapportTaxationLivreurPaye] " _
            & "FROM " _
            & "     [Admin] " _
            & ""
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        selectCommand.Parameters.AddWithValue("@TPS", clsAdminPara.TPS)
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader(CommandBehavior.SingleRow)
            If reader.Read Then
                With clsAdmin
                    .TPS = If(IsDBNull(reader("TPS")), Nothing, CType(CType(reader("TPS"), Double?), Decimal?))
                    .TVQ = If(IsDBNull(reader("TVQ")), Nothing, CType(CType(reader("TVQ"), Double?), Decimal?))
                    .TPSNumero = If(IsDBNull(reader("TPSNumero")), Nothing, reader("TPSNumero").ToString)
                    .TVQNumero = If(IsDBNull(reader("TVQNumero")), Nothing, reader("TVQNumero").ToString)
                    .CompanyName = If(IsDBNull(reader("CompanyName")), Nothing, reader("CompanyName").ToString)
                    .CompanyAdresse = If(IsDBNull(reader("CompanyAdresse")), Nothing, reader("CompanyAdresse").ToString)
                    .CompanyContact = If(IsDBNull(reader("CompanyContact")), Nothing, reader("CompanyContact").ToString)
                    .CompanyContactTelephone = If(IsDBNull(reader("CompanyContactTelephone")), Nothing, reader("CompanyContactTelephone").ToString)
                    .LocationPaie = If(IsDBNull(reader("LocationPaie")), Nothing, reader("LocationPaie").ToString)
                    .LocationFacture = If(IsDBNull(reader("LocationFacture")), Nothing, reader("LocationFacture").ToString)
                    .Server = If(IsDBNull(reader("Server")), Nothing, reader("Server").ToString)
                    .Username = If(IsDBNull(reader("Username")), Nothing, reader("Username").ToString)
                    .Password = If(IsDBNull(reader("Password")), Nothing, reader("Password").ToString)
                    .Port = If(IsDBNull(reader("Port")), Nothing, CType(reader("Port"), Int32?))
                    .UseSSL = If(IsDBNull(reader("UseSSL")), Nothing, CType(reader("UseSSL"), Boolean?))
                    .LocationConsolidation = If(IsDBNull(reader("LocationConsolidation")), Nothing, reader("LocationConsolidation").ToString)
                    .LocationVentes = If(IsDBNull(reader("LocationVentes")), Nothing, reader("LocationVentes").ToString)
                    .LocationAchats = If(IsDBNull(reader("LocationAchats")), Nothing, reader("LocationAchats").ToString)
                    .LocationCommissions = If(IsDBNull(reader("LocationCommissions")), Nothing, reader("LocationCommissions").ToString)
                    .LocationRapportLivreur = If(IsDBNull(reader("LocationRapportLivreur")), Nothing, reader("LocationRapportLivreur").ToString)
                    .LocationRapportRestaurant = If(IsDBNull(reader("LocationRapportRestaurant")), Nothing, reader("LocationRapportRestaurant").ToString)
                    .LocationRapportDeTaxation = If(IsDBNull(reader("LocationRapportDeTaxation")), Nothing, reader("LocationRapportDeTaxation").ToString)
                    .LocationRapportDeTaxationIDS = If(IsDBNull(reader("LocationRapportDeTaxationIDS")), Nothing, reader("LocationRapportDeTaxationIDS").ToString)
                    .RapportTaxationLivreurPaye = If(IsDBNull(reader("RapportTaxationLivreurPaye")), Nothing, reader("RapportTaxationLivreurPaye").ToString)
                End With
            Else
                clsAdmin = Nothing
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return clsAdmin
    End Function

    Public Function Add(ByVal clsAdmin As Admin) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim insertStatement As String _
            = "INSERT " _
            & "     [Admin] " _
            & "     ( " _
            & "     [TPS] " _
            & "    ,[TVQ] " _
            & "    ,[TPSNumero] " _
            & "    ,[TVQNumero] " _
            & "    ,[CompanyName] " _
            & "    ,[CompanyAdresse] " _
            & "    ,[CompanyContact] " _
            & "    ,[CompanyContactTelephone] " _
            & "    ,[LocationPaie] " _
            & "    ,[LocationFacture] " _
            & "    ,[Server] " _
            & "    ,[Username] " _
            & "    ,[Password] " _
            & "    ,[Port] " _
            & "    ,[UseSSL] " _
            & "    ,[LocationConsolidation] " _
            & "    ,[LocationVentes] " _
            & "    ,[LocationAchats] " _
            & "    ,[LocationCommissions] " _
            & "    ,[LocationRapportLivreur] " _
            & "    ,[LocationRapportRestaurant] " _
            & "    ,[LocationRapportDeTaxation] " _
            & "    ,[LocationRapportDeTaxationIDS] " _
            & "    ,[RapportTaxationLivreurPaye] " _
            & "     ) " _
            & "VALUES " _
            & "     ( " _
            & "     @TPS " _
            & "    ,@TVQ " _
            & "    ,@TPSNumero " _
            & "    ,@TVQNumero " _
            & "    ,@CompanyName " _
            & "    ,@CompanyAdresse " _
            & "    ,@CompanyContact " _
            & "    ,@CompanyContactTelephone " _
            & "    ,@LocationPaie " _
            & "    ,@LocationFacture " _
            & "    ,@Server " _
            & "    ,@Username " _
            & "    ,@Password " _
            & "    ,@Port " _
            & "    ,@UseSSL " _
            & "    ,@LocationConsolidation " _
            & "    ,@LocationVentes " _
            & "    ,@LocationAchats " _
            & "    ,@LocationCommissions " _
            & "    ,@LocationRapportLivreur " _
            & "    ,@LocationRapportRestaurant " _
            & "    ,@LocationRapportDeTaxation " _
            & "    ,@LocationRapportDeTaxationIDS " _
            & "    ,@RapportTaxationLivreurPaye " _
            & "     ) " _
            & ""
        Dim insertCommand As New SqlCommand(insertStatement, connection)
        insertCommand.CommandType = CommandType.Text
        insertCommand.Parameters.AddWithValue("@TPS", IIf(clsAdmin.TPS.HasValue, clsAdmin.TPS, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@TVQ", IIf(clsAdmin.TVQ.HasValue, clsAdmin.TVQ, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@TPSNumero", IIf(Not IsNothing(clsAdmin.TPSNumero), clsAdmin.TPSNumero, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@TVQNumero", IIf(Not IsNothing(clsAdmin.TVQNumero), clsAdmin.TVQNumero, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@CompanyName", IIf(Not IsNothing(clsAdmin.CompanyName), clsAdmin.CompanyName, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@CompanyAdresse", IIf(Not IsNothing(clsAdmin.CompanyAdresse), clsAdmin.CompanyAdresse, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@CompanyContact", IIf(Not IsNothing(clsAdmin.CompanyContact), clsAdmin.CompanyContact, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@CompanyContactTelephone", IIf(Not IsNothing(clsAdmin.CompanyContactTelephone), clsAdmin.CompanyContactTelephone, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@LocationPaie", IIf(Not IsNothing(clsAdmin.LocationPaie), clsAdmin.LocationPaie, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@LocationFacture", IIf(Not IsNothing(clsAdmin.LocationFacture), clsAdmin.LocationFacture, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Server", IIf(Not IsNothing(clsAdmin.Server), clsAdmin.Server, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Username", IIf(Not IsNothing(clsAdmin.Username), clsAdmin.Username, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Password", IIf(Not IsNothing(clsAdmin.Password), clsAdmin.Password, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Port", IIf(clsAdmin.Port.HasValue, clsAdmin.Port, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@UseSSL", IIf(clsAdmin.UseSSL.HasValue, clsAdmin.UseSSL, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@LocationConsolidation", IIf(Not IsNothing(clsAdmin.LocationConsolidation), clsAdmin.LocationConsolidation, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@LocationVentes", IIf(Not IsNothing(clsAdmin.LocationVentes), clsAdmin.LocationVentes, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@LocationAchats", IIf(Not IsNothing(clsAdmin.LocationAchats), clsAdmin.LocationAchats, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@LocationCommissions", IIf(Not IsNothing(clsAdmin.LocationCommissions), clsAdmin.LocationCommissions, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@LocationRapportLivreur", IIf(Not IsNothing(clsAdmin.LocationRapportLivreur), clsAdmin.LocationRapportLivreur, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@LocationRapportRestaurant", IIf(Not IsNothing(clsAdmin.LocationRapportRestaurant), clsAdmin.LocationRapportRestaurant, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@LocationRapportDeTaxation", IIf(Not IsNothing(clsAdmin.LocationRapportDeTaxation), clsAdmin.LocationRapportDeTaxation, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@LocationRapportDeTaxationIDS", IIf(Not IsNothing(clsAdmin.LocationRapportDeTaxationIDS), clsAdmin.LocationRapportDeTaxationIDS, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@RapportTaxationLivreurPaye", IIf(Not IsNothing(clsAdmin.RapportTaxationLivreurPaye), clsAdmin.RapportTaxationLivreurPaye, DBNull.Value))

        Try
            connection.Open()
            Dim count As Integer = insertCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

    Public Function Update(ByVal clsAdminOld As Admin,
           ByVal clsAdminNew As Admin) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim updateStatement As String _
            = "UPDATE " _
            & "     [Admin] " _
            & "SET " _
            & "     [TPS] = @NewTPS " _
            & "    ,[TVQ] = @NewTVQ " _
            & "    ,[TPSNumero] = @NewTPSNumero " _
            & "    ,[TVQNumero] = @NewTVQNumero " _
            & "    ,[CompanyName] = @NewCompanyName " _
            & "    ,[CompanyAdresse] = @NewCompanyAdresse " _
            & "    ,[CompanyContact] = @NewCompanyContact " _
            & "    ,[CompanyContactTelephone] = @NewCompanyContactTelephone " _
            & "    ,[LocationPaie] = @NewLocationPaie " _
            & "    ,[LocationFacture] = @NewLocationFacture " _
            & "    ,[Server] = @NewServer " _
            & "    ,[Username] = @NewUsername " _
            & "    ,[Password] = @NewPassword " _
            & "    ,[Port] = @NewPort " _
            & "    ,[UseSSL] = @NewUseSSL " _
            & "    ,[LocationConsolidation] = @NewLocationConsolidation " _
            & "    ,[LocationVentes] = @NewLocationVentes " _
            & "    ,[LocationAchats] = @NewLocationAchats " _
            & "    ,[LocationCommissions] = @NewLocationCommissions " _
            & "    ,[LocationRapportLivreur] = @NewLocationRapportLivreur " _
            & "    ,[LocationRapportRestaurant] = @NewLocationRapportRestaurant " _
            & "    ,[LocationRapportDeTaxation] = @NewLocationRapportDeTaxation " _
            & "    ,[LocationRapportDeTaxationIDS] = @NewLocationRapportDeTaxationIDS " _
            & "    ,[RapportTaxationLivreurPaye] = @NewRapportTaxationLivreurPaye"


        Dim updateCommand As New SqlCommand(updateStatement, connection)
        updateCommand.CommandType = CommandType.Text
        updateCommand.Parameters.AddWithValue("@NewTPS", IIf(clsAdminNew.TPS.HasValue, clsAdminNew.TPS, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewTVQ", IIf(clsAdminNew.TVQ.HasValue, clsAdminNew.TVQ, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewTPSNumero", IIf(Not IsNothing(clsAdminNew.TPSNumero), clsAdminNew.TPSNumero, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewTVQNumero", IIf(Not IsNothing(clsAdminNew.TVQNumero), clsAdminNew.TVQNumero, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewCompanyName", IIf(Not IsNothing(clsAdminNew.CompanyName), clsAdminNew.CompanyName, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewCompanyAdresse", IIf(Not IsNothing(clsAdminNew.CompanyAdresse), clsAdminNew.CompanyAdresse, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewCompanyContact", IIf(Not IsNothing(clsAdminNew.CompanyContact), clsAdminNew.CompanyContact, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewCompanyContactTelephone", IIf(Not IsNothing(clsAdminNew.CompanyContactTelephone), clsAdminNew.CompanyContactTelephone, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewLocationPaie", IIf(Not IsNothing(clsAdminNew.LocationPaie), clsAdminNew.LocationPaie, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewLocationFacture", IIf(Not IsNothing(clsAdminNew.LocationFacture), clsAdminNew.LocationFacture, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewServer", IIf(Not IsNothing(clsAdminNew.Server), clsAdminNew.Server, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewUsername", IIf(Not IsNothing(clsAdminNew.Username), clsAdminNew.Username, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewPassword", IIf(Not IsNothing(clsAdminNew.Password), clsAdminNew.Password, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewPort", IIf(clsAdminNew.Port.HasValue, clsAdminNew.Port, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewUseSSL", IIf(clsAdminNew.UseSSL.HasValue, clsAdminNew.UseSSL, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewLocationConsolidation", IIf(Not IsNothing(clsAdminNew.LocationConsolidation), clsAdminNew.LocationConsolidation, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewLocationVentes", IIf(Not IsNothing(clsAdminNew.LocationVentes), clsAdminNew.LocationVentes, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewLocationAchats", IIf(Not IsNothing(clsAdminNew.LocationAchats), clsAdminNew.LocationAchats, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewLocationCommissions", IIf(Not IsNothing(clsAdminNew.LocationCommissions), clsAdminNew.LocationCommissions, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewLocationRapportLivreur", IIf(Not IsNothing(clsAdminNew.LocationRapportLivreur), clsAdminNew.LocationRapportLivreur, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewLocationRapportRestaurant", IIf(Not IsNothing(clsAdminNew.LocationRapportRestaurant), clsAdminNew.LocationRapportRestaurant, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewLocationRapportDeTaxation", IIf(Not IsNothing(clsAdminNew.LocationRapportDeTaxation), clsAdminNew.LocationRapportDeTaxation, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewLocationRapportDeTaxationIDS", IIf(Not IsNothing(clsAdminNew.LocationRapportDeTaxationIDS), clsAdminNew.LocationRapportDeTaxationIDS, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewRapportTaxationLivreurPaye", IIf(Not IsNothing(clsAdminNew.RapportTaxationLivreurPaye), clsAdminNew.RapportTaxationLivreurPaye, DBNull.Value))



        Try
            connection.Open()
            Dim count As Integer = updateCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function


End Class
 
