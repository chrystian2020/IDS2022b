Imports System.Data.SqlClient

Public Class EtatsAchatsData


    Public Function SelectAllByPeriode(pIDPeriode As Integer) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDEtatsAchats] " _
            & "    ,[IDPeriode] " _
            & "    ,[CheminRapport] " _
            & "    ,[NomFichier] " _
            & "    ,[DateFrom] " _
            & "    ,[DateTo] " _
            & "FROM " _
            & "     [EtatsAchats] " _
           & " WHERE IDPeriode =" & pIDPeriode
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function

    Public Function SelectAll() As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDEtatsAchats] " _
            & "    ,[IDPeriode] " _
            & "    ,[CheminRapport] " _
            & "    ,[NomFichier] " _
            & "    ,[DateFrom] " _
            & "    ,[DateTo] " _
            & "FROM " _
            & "     [EtatsAchats] " _
            & ""
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function

    Public Function Select_Record(ByVal clsEtatsAchatsPara As EtatsAchats) As EtatsAchats
        Dim clsEtatsAchats As New EtatsAchats
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDEtatsAchats] " _
            & "    ,[IDPeriode] " _
            & "    ,[CheminRapport] " _
            & "    ,[NomFichier] " _
            & "    ,[DateFrom] " _
            & "    ,[DateTo] " _
            & "FROM " _
            & "     [EtatsAchats] " _
            & "WHERE " _
            & "     [IDEtatsAchats] = @IDEtatsAchats " _
            & ""
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        selectCommand.Parameters.AddWithValue("@IDEtatsAchats", clsEtatsAchatsPara.IDEtatsAchats)
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader(CommandBehavior.SingleRow)
            If reader.Read Then
                With clsEtatsAchats
                    .IDEtatsAchats = System.Convert.ToInt32(reader("IDEtatsAchats"))
                    .IDPeriode = If(IsDBNull(reader("IDPeriode")), Nothing, CType(reader("IDPeriode"), Int32?))
                    .CheminRapport = System.Convert.ToString(reader("CheminRapport"))
                    .NomFichier = If(IsDBNull(reader("NomFichier")), Nothing, reader("NomFichier").ToString)
                    .DateFrom = If(IsDBNull(reader("DateFrom")), Nothing, CType(reader("DateFrom"), DateTime?))
                    .DateTo = If(IsDBNull(reader("DateTo")), Nothing, CType(reader("DateTo"), DateTime?))
                End With
            Else
                clsEtatsAchats = Nothing
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return clsEtatsAchats
    End Function

    Public Function Add(ByVal clsEtatsAchats As EtatsAchats) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim insertStatement As String _
            = "INSERT " _
            & "     [EtatsAchats] " _
            & "     ( " _
            & "     [IDPeriode] " _
            & "    ,[CheminRapport] " _
            & "    ,[NomFichier] " _
            & "    ,[DateFrom] " _
            & "    ,[DateTo] " _
            & "     ) " _
            & "VALUES " _
            & "     ( " _
            & "     @IDPeriode " _
            & "    ,@CheminRapport " _
            & "    ,@NomFichier " _
            & "    ,@DateFrom " _
            & "    ,@DateTo " _
            & "     ) " _
            & ""
        Dim insertCommand As New SqlCommand(insertStatement, connection)
        insertCommand.CommandType = CommandType.Text
        insertCommand.Parameters.AddWithValue("@IDPeriode", IIf(clsEtatsAchats.IDPeriode.HasValue, clsEtatsAchats.IDPeriode, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@CheminRapport", clsEtatsAchats.CheminRapport)
        insertCommand.Parameters.AddWithValue("@NomFichier", IIf(Not IsNothing(clsEtatsAchats.NomFichier), clsEtatsAchats.NomFichier, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@DateFrom", IIf(clsEtatsAchats.DateFrom.HasValue, clsEtatsAchats.DateFrom, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@DateTo", IIf(clsEtatsAchats.DateTo.HasValue, clsEtatsAchats.DateTo, DBNull.Value))
        Try
            connection.Open()
            Dim count As Integer = insertCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

    Public Function Update(ByVal clsEtatsAchatsOld As EtatsAchats,
           ByVal clsEtatsAchatsNew As EtatsAchats) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim updateStatement As String _
            = "UPDATE " _
            & "     [EtatsAchats] " _
            & "SET " _
            & "     [IDPeriode] = @NewIDPeriode " _
            & "    ,[CheminRapport] = @NewCheminRapport " _
            & "    ,[NomFichier] = @NewNomFichier " _
            & "    ,[DateFrom] = @NewDateFrom " _
            & "    ,[DateTo] = @NewDateTo " _
            & "WHERE " _
            & "     [IDEtatsAchats] = @OldIDEtatsAchats " _
            & ""
        Dim updateCommand As New SqlCommand(updateStatement, connection)
        updateCommand.CommandType = CommandType.Text
        updateCommand.Parameters.AddWithValue("@NewIDPeriode", IIf(clsEtatsAchatsNew.IDPeriode.HasValue, clsEtatsAchatsNew.IDPeriode, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewCheminRapport", clsEtatsAchatsNew.CheminRapport)
        updateCommand.Parameters.AddWithValue("@NewNomFichier", IIf(Not IsNothing(clsEtatsAchatsNew.NomFichier), clsEtatsAchatsNew.NomFichier, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewDateFrom", IIf(clsEtatsAchatsNew.DateFrom.HasValue, clsEtatsAchatsNew.DateFrom, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewDateTo", IIf(clsEtatsAchatsNew.DateTo.HasValue, clsEtatsAchatsNew.DateTo, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@OldIDEtatsAchats", clsEtatsAchatsOld.IDEtatsAchats)

        Try
            connection.Open()
            Dim count As Integer = updateCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

    Public Function Delete(ByVal clsEtatsAchats As EtatsAchats) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim deleteStatement As String _
            = "DELETE FROM " _
            & "     [EtatsAchats] " _
            & "WHERE " _
            & "     [IDEtatsAchats] = @OldIDEtatsAchats " _
            & ""
        Dim deleteCommand As New SqlCommand(deleteStatement, connection)
        deleteCommand.CommandType = CommandType.Text
        deleteCommand.Parameters.AddWithValue("@OldIDEtatsAchats", clsEtatsAchats.IDEtatsAchats)

        Try
            connection.Open()
            Dim count As Integer = deleteCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

End Class

