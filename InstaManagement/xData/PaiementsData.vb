Imports System.Data.SqlClient

Public Class PaiementsData

    Public Function SelectAllPaiements(pIDOrganisation As Integer, pIDPeriode As Integer) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT IDPaiement, " &
            " dbo.paiements.IDInvoice, " &
            " dbo.paiements.InvoiceNumber, " &
            " dbo.paiements.DatePaiement, " &
            " dbo.paiements.OrganisationID, " &
            " dbo.paiements.Organisation, " &
            " dbo.paiements.Montant, " &
            " dbo.paiements.IDPeriode, " &
            " dbo.periodes.Periode " &
            " FROM   dbo.paiements " &
            " LEFT OUTER JOIN dbo.periodes " &
            " ON dbo.paiements.idperiode = dbo.periodes.idperiode " &
            " WHERE dbo.paiements.OrganisationID= " & pIDOrganisation & " AND dbo.paiements.IDPeriode= " & pIDPeriode

        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function


    Public Function SelectAll() As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDPaiement] " _
            & "    ,[IDInvoice] " _
            & "    ,[InvoiceNumber] " _
            & "    ,[IDPeriode] " _
            & "    ,[DatePaiement] " _
            & "    ,[OrganisationID] " _
            & "    ,[Organisation] " _
            & "    ,[Montant] " _
            & "FROM " _
            & "     [Paiements] " _
            & ""
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function

    Public Function Select_Record(ByVal clsPaiementsPara As Paiements) As Paiements
        Dim clsPaiements As New Paiements
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDPaiement] " _
            & "    ,[IDInvoice] " _
            & "    ,[InvoiceNumber] " _
            & "    ,[IDPeriode] " _
            & "    ,[DatePaiement] " _
            & "    ,[OrganisationID] " _
            & "    ,[Organisation] " _
            & "    ,[Montant] " _
            & "FROM " _
            & "     [Paiements] " _
            & "WHERE " _
            & "     [IDPaiement] = @IDPaiement " _
            & ""
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        selectCommand.Parameters.AddWithValue("@IDPaiement", clsPaiementsPara.IDPaiement)
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader(CommandBehavior.SingleRow)
            If reader.Read Then
                With clsPaiements
                    .IDPaiement = System.Convert.ToInt32(reader("IDPaiement"))
                    .IDInvoice = System.Convert.ToInt32(reader("IDInvoice"))
                    .InvoiceNumber = If(IsDBNull(reader("InvoiceNumber")), Nothing, reader("InvoiceNumber").ToString)
                    .IDPeriode = If(IsDBNull(reader("IDPeriode")), Nothing, CType(reader("IDPeriode"), Int32?))
                    .DatePaiement = If(IsDBNull(reader("DatePaiement")), Nothing, CType(reader("DatePaiement"), DateTime?))
                    .OrganisationID = If(IsDBNull(reader("OrganisationID")), Nothing, CType(reader("OrganisationID"), Int32?))
                    .Organisation = If(IsDBNull(reader("Organisation")), Nothing, reader("Organisation").ToString)
                    .Montant = If(IsDBNull(reader("Montant")), Nothing, CType(reader("Montant"), Decimal?))
                End With
            Else
                clsPaiements = Nothing
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return clsPaiements
    End Function

    Public Function Add(ByVal clsPaiements As Paiements) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim insertStatement As String _
            = "INSERT " _
            & "     [Paiements] " _
            & "     ( " _
            & "     [IDInvoice] " _
            & "    ,[InvoiceNumber] " _
            & "    ,[IDPeriode] " _
            & "    ,[DatePaiement] " _
            & "    ,[OrganisationID] " _
            & "    ,[Organisation] " _
            & "    ,[Montant] " _
            & "     ) " _
            & "VALUES " _
            & "     ( " _
            & "     @IDInvoice " _
            & "    ,@InvoiceNumber " _
            & "    ,@IDPeriode " _
            & "    ,@DatePaiement " _
            & "    ,@OrganisationID " _
            & "    ,@Organisation " _
            & "    ,@Montant " _
            & "     ) " _
            & ""
        Dim insertCommand As New SqlCommand(insertStatement, connection)
        insertCommand.CommandType = CommandType.Text
        insertCommand.Parameters.AddWithValue("@IDInvoice", clsPaiements.IDInvoice)
        insertCommand.Parameters.AddWithValue("@InvoiceNumber", IIf(Not IsNothing(clsPaiements.InvoiceNumber), clsPaiements.InvoiceNumber, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@IDPeriode", IIf(clsPaiements.IDPeriode.HasValue, clsPaiements.IDPeriode, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@DatePaiement", IIf(clsPaiements.DatePaiement.HasValue, clsPaiements.DatePaiement, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@OrganisationID", IIf(clsPaiements.OrganisationID.HasValue, clsPaiements.OrganisationID, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Organisation", IIf(Not IsNothing(clsPaiements.Organisation), clsPaiements.Organisation, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Montant", IIf(clsPaiements.Montant.HasValue, clsPaiements.Montant, DBNull.Value))
        Try
            connection.Open()
            Dim count As Integer = insertCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

    Public Function Update(ByVal clsPaiementsOld As Paiements,
           ByVal clsPaiementsNew As Paiements) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim updateStatement As String _
            = "UPDATE " _
            & "     [Paiements] " _
            & "SET " _
            & "     [IDInvoice] = @NewIDInvoice " _
            & "    ,[InvoiceNumber] = @NewInvoiceNumber " _
            & "    ,[IDPeriode] = @NewIDPeriode " _
            & "    ,[DatePaiement] = @NewDatePaiement " _
            & "    ,[OrganisationID] = @NewOrganisationID " _
            & "    ,[Organisation] = @NewOrganisation " _
            & "    ,[Montant] = @NewMontant " _
            & "WHERE " _
            & "     [IDPaiement] = @OldIDPaiement " _
            & ""
        Dim updateCommand As New SqlCommand(updateStatement, connection)
        updateCommand.CommandType = CommandType.Text
        updateCommand.Parameters.AddWithValue("@NewIDInvoice", clsPaiementsNew.IDInvoice)
        updateCommand.Parameters.AddWithValue("@NewInvoiceNumber", IIf(Not IsNothing(clsPaiementsNew.InvoiceNumber), clsPaiementsNew.InvoiceNumber, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewIDPeriode", IIf(clsPaiementsNew.IDPeriode.HasValue, clsPaiementsNew.IDPeriode, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewDatePaiement", IIf(clsPaiementsNew.DatePaiement.HasValue, clsPaiementsNew.DatePaiement, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewOrganisationID", IIf(clsPaiementsNew.OrganisationID.HasValue, clsPaiementsNew.OrganisationID, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewOrganisation", IIf(Not IsNothing(clsPaiementsNew.Organisation), clsPaiementsNew.Organisation, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewMontant", IIf(clsPaiementsNew.Montant.HasValue, clsPaiementsNew.Montant, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@OldIDPaiement", clsPaiementsOld.IDPaiement)

        Try
            connection.Open()
            Dim count As Integer = updateCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

    Public Function Delete(ByVal clsPaiements As Paiements) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim deleteStatement As String _
            = "DELETE FROM " _
            & "     [Paiements] " _
            & "WHERE " _
            & "     [IDPaiement] = @OldIDPaiement " _
            & ""
        Dim deleteCommand As New SqlCommand(deleteStatement, connection)
        deleteCommand.CommandType = CommandType.Text
        deleteCommand.Parameters.AddWithValue("@OldIDPaiement", clsPaiements.IDPaiement)

        Try
            connection.Open()
            Dim count As Integer = deleteCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

End Class
 
