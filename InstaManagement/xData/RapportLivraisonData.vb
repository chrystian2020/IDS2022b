Imports System.Data.SqlClient

Public Class RapportLivraisonData

    Public Function SelectAll() As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDRapportLivraison] " _
            & "    ,[IDPeriode] " _
            & "    ,[IDOrganisation] " _
            & "    ,[Organisation] " _
            & "    ,[IDLivreur] " _
            & "    ,[Livreur] " _
            & "    ,[CheminRapport] " _
            & "    ,[NomFichier] " _
            & "    ,[Email] " _
            & "    ,[Email1] " _
            & "    ,[Email2] " _
            & "    ,[DateFrom] " _
            & "    ,[DateTo] " _
            & "    ,[IsRapportRestaurant] " _
            & "FROM " _
            & "     [RapportLivraison] " _
            & ""
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function

    Public Function SelectAllByIDs(piIDRapport As String) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDRapportLivraison] " _
            & "    ,[IDPeriode] " _
            & "    ,[IDOrganisation] " _
            & "    ,[Organisation] " _
            & "    ,[IDLivreur] " _
            & "    ,[Livreur] " _
            & "    ,[CheminRapport] " _
            & "    ,[NomFichier] " _
            & "    ,[Email] " _
            & "    ,[Email1] " _
            & "    ,[Email2] " _
            & "    ,[DateFrom] " _
            & "    ,[DateTo] " _
            & "    ,[IsRapportRestaurant] " _
            & "FROM " _
            & "     [RapportLivraison] " _
            & " WHERE IDRapportLivraison IN (" & piIDRapport & ")"
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function
    Public Function SelectAllByPeriodeP(pIDPeriode As Integer) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDRapportLivraison] " _
            & "    ,[IDPeriode] " _
            & "    ,[IDOrganisation] " _
            & "    ,[Organisation] " _
            & "    ,[IDLivreur] " _
            & "    ,[Livreur] " _
            & "    ,[CheminRapport] " _
            & "    ,[NomFichier] " _
            & "    ,[Email] " _
            & "    ,[Email1] " _
            & "    ,[Email2] " _
            & "    ,[DateFrom] " _
            & "    ,[DateTo] " _
            & "    ,[IsRapportRestaurant] " _
            & "FROM " _
            & "     [RapportLivraison] " _
            & " WHERE  IsRapportRestaurant = 0 AND  IDPeriode =" & pIDPeriode
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function
    Public Function SelectAllByPeriodeF(pIDPeriode As Integer) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDRapportLivraison] " _
            & "    ,[IDPeriode] " _
            & "    ,[IDOrganisation] " _
            & "    ,[Organisation] " _
            & "    ,[IDLivreur] " _
            & "    ,[Livreur] " _
            & "    ,[CheminRapport] " _
            & "    ,[NomFichier] " _
            & "    ,[Email] " _
            & "    ,[Email1] " _
            & "    ,[Email2] " _
            & "    ,[DateFrom] " _
            & "    ,[DateTo] " _
            & "    ,[IsRapportRestaurant] " _
            & "FROM " _
            & "     [RapportLivraison] " _
            & " WHERE  IsRapportRestaurant = 1 AND  IDPeriode =" & pIDPeriode
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function
    Public Function SelectAllDataByPeriodesP(PIDPeriode As Integer, piIDRapport As String) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDRapportLivraison] " _
            & "    ,[IDPeriode] " _
            & "    ,[IDOrganisation] " _
            & "    ,[Organisation] " _
            & "    ,[IDLivreur] " _
            & "    ,[Livreur] " _
            & "    ,[CheminRapport] " _
            & "    ,[NomFichier] " _
            & "    ,[Email] " _
            & "    ,[Email1] " _
            & "    ,[Email2] " _
            & "    ,[DateFrom] " _
            & "    ,[DateTo] " _
            & "    ,[IsRapportRestaurant] " _
            & "FROM " _
            & "     [RapportLivraison] " _
            & " WHERE IsRapportRestaurant = 0 AND IDPeriode =" & PIDPeriode

        If piIDRapport.ToString.Trim <> "" Then
            selectStatement = selectStatement & " AND IDRapport IN (" & piIDRapport & ")"
        End If


        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function

    Public Function SelectAllDataByPeriodesF(PIDPeriode As Integer, piIDRapport As String) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDRapportLivraison] " _
            & "    ,[IDPeriode] " _
            & "    ,[IDOrganisation] " _
            & "    ,[Organisation] " _
            & "    ,[IDLivreur] " _
            & "    ,[Livreur] " _
            & "    ,[CheminRapport] " _
            & "    ,[NomFichier] " _
            & "    ,[Email] " _
            & "    ,[Email1] " _
            & "    ,[Email2] " _
            & "    ,[DateFrom] " _
            & "    ,[DateTo] " _
            & "    ,[IsRapportRestaurant] " _
            & "FROM " _
            & "     [RapportLivraison] " _
            & " WHERE IsRapportRestaurant = 1 AND IDPeriode =" & PIDPeriode

        If piIDRapport.ToString.Trim <> "" Then
            selectStatement = selectStatement & " AND IDRapportLivraison IN (" & piIDRapport & ")"
        End If

        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function
    Public Function Select_Record(ByVal clsRapportLivraisonPara As RapportLivraison) As RapportLivraison
        Dim clsRapportLivraison As New RapportLivraison
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDRapportLivraison] " _
            & "    ,[IDPeriode] " _
            & "    ,[IDOrganisation] " _
            & "    ,[Organisation] " _
            & "    ,[IDLivreur] " _
            & "    ,[Livreur] " _
            & "    ,[CheminRapport] " _
            & "    ,[NomFichier] " _
            & "    ,[Email] " _
            & "    ,[Email1] " _
            & "    ,[Email2] " _
            & "    ,[DateFrom] " _
            & "    ,[DateTo] " _
            & "    ,[IsRapportRestaurant] " _
            & "FROM " _
            & "     [RapportLivraison] " _
            & "WHERE " _
            & "    [IDRapportLivraison] = @IDRapportLivraison " _
            & ""
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        selectCommand.Parameters.AddWithValue("@IDRapportLivraison", clsRapportLivraisonPara.IDRapportLivraison)
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader(CommandBehavior.SingleRow)
            If reader.Read Then
                With clsRapportLivraison
                    .IDRapportLivraison = System.Convert.ToInt32(reader("IDRapportLivraison"))
                    .IDPeriode = If(IsDBNull(reader("IDPeriode")), Nothing, CType(reader("IDPeriode"), Int32?))
                    .IDOrganisation = If(IsDBNull(reader("IDOrganisation")), Nothing, CType(reader("IDOrganisation"), Int32?))
                    .Organisation = If(IsDBNull(reader("Organisation")), Nothing, reader("Organisation").ToString)
                    .IDLivreur = If(IsDBNull(reader("IDLivreur")), Nothing, CType(reader("IDLivreur"), Int32?))
                    .Livreur = If(IsDBNull(reader("Livreur")), Nothing, reader("Livreur").ToString)
                    .CheminRapport = System.Convert.ToString(reader("CheminRapport"))
                    .NomFichier = If(IsDBNull(reader("NomFichier")), Nothing, reader("NomFichier").ToString)
                    .Email = If(IsDBNull(reader("Email")), Nothing, reader("Email").ToString)
                    .Email1 = If(IsDBNull(reader("Email1")), Nothing, reader("Email1").ToString)
                    .Email2 = If(IsDBNull(reader("Email2")), Nothing, reader("Email2").ToString)
                    .DateFrom = If(IsDBNull(reader("DateFrom")), Nothing, CType(reader("DateFrom"), DateTime?))
                    .DateTo = If(IsDBNull(reader("DateTo")), Nothing, CType(reader("DateTo"), DateTime?))
                    .IsRapportRestaurant = If(IsDBNull(reader("IsRapportRestaurant")), Nothing, CType(reader("IsRapportRestaurant"), Boolean?))
                End With
            Else
                clsRapportLivraison = Nothing
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return clsRapportLivraison
    End Function

    Public Function Add(ByVal clsRapportLivraison As RapportLivraison) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim insertStatement As String _
            = "INSERT " _
            & "     [RapportLivraison] " _
            & "     ( " _
            & "     [IDPeriode] " _
            & "    ,[IDOrganisation] " _
            & "    ,[Organisation] " _
            & "    ,[IDLivreur] " _
            & "    ,[Livreur] " _
            & "    ,[CheminRapport] " _
            & "    ,[NomFichier] " _
            & "    ,[Email] " _
            & "    ,[Email1] " _
            & "    ,[Email2] " _
            & "    ,[DateFrom] " _
            & "    ,[DateTo] " _
            & "    ,[IsRapportRestaurant] " _
            & "     ) " _
            & "VALUES " _
            & "     ( " _
            & "     @IDPeriode " _
            & "    ,@IDOrganisation " _
            & "    ,@Organisation " _
            & "    ,@IDLivreur " _
            & "    ,@Livreur " _
            & "    ,@CheminRapport " _
            & "    ,@NomFichier " _
            & "    ,@Email " _
            & "    ,@Email1 " _
            & "    ,@Email2 " _
            & "    ,@DateFrom " _
            & "    ,@DateTo " _
            & "    ,@IsRapportRestaurant " _
            & "     ) " _
            & ""
        Dim insertCommand As New SqlCommand(insertStatement, connection)
        insertCommand.CommandType = CommandType.Text
        insertCommand.Parameters.AddWithValue("@IDPeriode", IIf(clsRapportLivraison.IDPeriode.HasValue, clsRapportLivraison.IDPeriode, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@IDOrganisation", IIf(clsRapportLivraison.IDOrganisation.HasValue, clsRapportLivraison.IDOrganisation, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Organisation", IIf(Not IsNothing(clsRapportLivraison.Organisation), clsRapportLivraison.Organisation, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@IDLivreur", IIf(clsRapportLivraison.IDLivreur.HasValue, clsRapportLivraison.IDLivreur, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Livreur", IIf(Not IsNothing(clsRapportLivraison.Livreur), clsRapportLivraison.Livreur, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@CheminRapport", clsRapportLivraison.CheminRapport)
        insertCommand.Parameters.AddWithValue("@NomFichier", IIf(Not IsNothing(clsRapportLivraison.NomFichier), clsRapportLivraison.NomFichier, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Email", IIf(Not IsNothing(clsRapportLivraison.Email), clsRapportLivraison.Email, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Email1", IIf(Not IsNothing(clsRapportLivraison.Email1), clsRapportLivraison.Email1, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Email2", IIf(Not IsNothing(clsRapportLivraison.Email2), clsRapportLivraison.Email2, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@DateFrom", IIf(clsRapportLivraison.DateFrom.HasValue, clsRapportLivraison.DateFrom, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@DateTo", IIf(clsRapportLivraison.DateTo.HasValue, clsRapportLivraison.DateTo, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@IsRapportRestaurant", IIf(clsRapportLivraison.IsRapportRestaurant.HasValue, clsRapportLivraison.IsRapportRestaurant, DBNull.Value))
        Try
            connection.Open()
            Dim count As Integer = insertCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

    Public Function Update(ByVal clsRapportLivraisonOld As RapportLivraison,
           ByVal clsRapportLivraisonNew As RapportLivraison) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim updateStatement As String _
            = "UPDATE " _
            & "     [RapportLivraison] " _
            & "SET " _
            & "     [IDPeriode] = @NewIDPeriode " _
            & "    ,[IDOrganisation] = @NewIDOrganisation " _
            & "    ,[Organisation] = @NewOrganisation " _
            & "    ,[IDLivreur] = @NewIDLivreur " _
            & "    ,[Livreur] = @NewLivreur " _
            & "    ,[CheminRapport] = @NewCheminRapport " _
            & "    ,[NomFichier] = @NewNomFichier " _
            & "    ,[Email] = @NewEmail " _
            & "    ,[Email1] = @NewEmail1 " _
            & "    ,[Email2] = @NewEmail2 " _
            & "    ,[DateFrom] = @NewDateFrom " _
            & "    ,[DateTo] = @NewDateTo " _
            & "    ,[IsRapportRestaurant] = @NewIsRapportRestaurant " _
            & "WHERE " _
            & "     [IDRapportLivraison] = @OldIDRapportLivraison " _
            & ""
        Dim updateCommand As New SqlCommand(updateStatement, connection)
        updateCommand.CommandType = CommandType.Text
        updateCommand.Parameters.AddWithValue("@NewIDPeriode", IIf(clsRapportLivraisonNew.IDPeriode.HasValue, clsRapportLivraisonNew.IDPeriode, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewIDOrganisation", IIf(clsRapportLivraisonNew.IDOrganisation.HasValue, clsRapportLivraisonNew.IDOrganisation, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewOrganisation", IIf(Not IsNothing(clsRapportLivraisonNew.Organisation), clsRapportLivraisonNew.Organisation, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewIDLivreur", IIf(clsRapportLivraisonNew.IDLivreur.HasValue, clsRapportLivraisonNew.IDLivreur, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewLivreur", IIf(Not IsNothing(clsRapportLivraisonNew.Livreur), clsRapportLivraisonNew.Livreur, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewCheminRapport", clsRapportLivraisonNew.CheminRapport)
        updateCommand.Parameters.AddWithValue("@NewNomFichier", IIf(Not IsNothing(clsRapportLivraisonNew.NomFichier), clsRapportLivraisonNew.NomFichier, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewEmail", IIf(Not IsNothing(clsRapportLivraisonNew.Email), clsRapportLivraisonNew.Email, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewEmail1", IIf(Not IsNothing(clsRapportLivraisonNew.Email1), clsRapportLivraisonNew.Email1, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewEmail2", IIf(Not IsNothing(clsRapportLivraisonNew.Email2), clsRapportLivraisonNew.Email2, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewDateFrom", IIf(clsRapportLivraisonNew.DateFrom.HasValue, clsRapportLivraisonNew.DateFrom, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewDateTo", IIf(clsRapportLivraisonNew.DateTo.HasValue, clsRapportLivraisonNew.DateTo, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewIsRapportRestaurant", IIf(clsRapportLivraisonNew.IsRapportRestaurant.HasValue, clsRapportLivraisonNew.IsRapportRestaurant, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@OldIDRapportLivraison", clsRapportLivraisonOld.IDRapportLivraison)

        Try
            connection.Open()
            Dim count As Integer = updateCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

    Public Function Delete(ByVal clsRapportLivraison As RapportLivraison) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim deleteStatement As String _
            = "DELETE FROM " _
            & "     [RapportLivraison] " _
            & "WHERE " _
            & "     [IDRapportLivraison] = @OldIDRapportLivraison " _
            & ""
        Dim deleteCommand As New SqlCommand(deleteStatement, connection)
        deleteCommand.CommandType = CommandType.Text
        deleteCommand.Parameters.AddWithValue("@OldIDRapportLivraison", clsRapportLivraison.IDRapportLivraison)

        Try
            connection.Open()
            Dim count As Integer = deleteCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

    Public Function DeleteByPeriodP(ByVal clsRapportLivraison As RapportLivraison) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim deleteStatement As String _
            = "DELETE FROM " _
            & "     [RapportLivraison] " _
            & "WHERE " _
            & "     [IDPeriode] = @OldIDPeriode " _
            & " AND IsRapportRestaurant = 0"
        Dim deleteCommand As New SqlCommand(deleteStatement, connection)
        deleteCommand.CommandType = CommandType.Text
        deleteCommand.Parameters.AddWithValue("@OldIDPeriode", clsRapportLivraison.IDPeriode)

        Try
            connection.Open()
            Dim count As Integer = deleteCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function
    Public Function DeleteByPeriodF(ByVal clsRapportLivraison As RapportLivraison) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim deleteStatement As String _
            = "DELETE FROM " _
            & "     [RapportLivraison] " _
            & "WHERE " _
            & "     [IDPeriode] = @OldIDPeriode " _
            & " AND IsRapportRestaurant = 1"
        Dim deleteCommand As New SqlCommand(deleteStatement, connection)
        deleteCommand.CommandType = CommandType.Text
        deleteCommand.Parameters.AddWithValue("@OldIDPeriode", clsRapportLivraison.IDPeriode)

        Try
            connection.Open()
            Dim count As Integer = deleteCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function
End Class
 
