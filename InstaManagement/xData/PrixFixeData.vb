Imports System.Data.SqlClient

Public Class PrixFixeData

    Public Function SelectAll() As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDPrixFixe] " _
            & "    ,[IDOrganisation] " _
            & "    ,[IDLivreur] " _
            & "    ,[Livreur] " _
            & "    ,[Organisation] " _
            & "    ,[MontantLivreur] " _
            & "    ,[IDPeriode] " _
            & "FROM " _
            & "     [PrixFixe] " _
            & ""
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function
    Public Function SelectByIDLivreur(PIDLivreur As Integer) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDPrixFixe] " _
            & "    ,[IDOrganisation] " _
            & "    ,[IDLivreur] " _
            & "    ,[Livreur] " _
            & "    ,[Organisation] " _
            & "    ,[MontantLivreur] " _
            & "    ,[IDPeriode] " _
            & " FROM PrixFixe " _
        & " WHERE  IDLivreur=" & PIDLivreur _
        & " AND IDPeriode =" & go_Globals.IDPeriode _
        & " ORDER BY dbo.PrixFixe.IDPrixFixe"


        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function
    Public Function SelectByIDOrganisationIDLivreur(pIDOrganisation As Integer, PIDLivreur As Integer) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDPrixFixe] " _
            & "    ,[IDOrganisation] " _
            & "    ,[IDLivreur] " _
            & "    ,[Livreur] " _
            & "    ,[Organisation] " _
            & "    ,[MontantLivreur] " _
            & "    ,[IDPeriode] " _
            & " FROM PrixFixe " _
        & " WHERE  IDOrganisation=" & pIDOrganisation _
        & " AND IDLivreur =" & PIDLivreur _
        & " AND IDPeriode =" & go_Globals.IDPeriode _
        & " ORDER BY dbo.PrixFixe.IDPrixFixe"


        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function
    Public Function SelectAllByIDOrganisationIDLivreur(pIDOrganisation As Integer) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDPrixFixe] " _
            & "    ,[IDOrganisation] " _
            & "    ,[IDLivreur] " _
            & "    ,[Livreur] " _
            & "    ,[Organisation] " _
            & "    ,[MontantLivreur] " _
            & "    ,[IDPeriode] " _
            & " FROM PrixFixe " _
        & " WHERE  IDOrganisation=" & pIDOrganisation _
        & " AND IDPeriode =" & go_Globals.IDPeriode _
        & " ORDER BY dbo.PrixFixe.IDPrixFixe"


        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function
    Public Function Select_Record(ByVal clsPrixFixePara As PrixFixe) As PrixFixe
        Dim clsPrixFixe As New PrixFixe
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDPrixFixe] " _
            & "    ,[IDOrganisation] " _
            & "    ,[IDLivreur] " _
            & "    ,[Livreur] " _
            & "    ,[Organisation] " _
            & "    ,[MontantLivreur] " _
            & "    ,[IDPeriode] " _
            & "FROM " _
            & "     [PrixFixe] " _
            & "WHERE " _
            & "     [IDPrixFixe] = @IDPrixFixe " _
            & ""
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        selectCommand.Parameters.AddWithValue("@IDPrixFixe", clsPrixFixePara.IDPrixFixe)
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader(CommandBehavior.SingleRow)
            If reader.Read Then
                With clsPrixFixe
                    .IDPrixFixe = System.Convert.ToInt32(reader("IDPrixFixe"))
                    .IDOrganisation = If(IsDBNull(reader("IDOrganisation")), Nothing, CType(reader("IDOrganisation"), Int32?))
                    .IDLivreur = If(IsDBNull(reader("IDLivreur")), Nothing, CType(reader("IDLivreur"), Int32?))
                    .Livreur = If(IsDBNull(reader("Livreur")), Nothing, reader("Livreur").ToString)
                    .Organisation = If(IsDBNull(reader("Organisation")), Nothing, reader("Organisation").ToString)
                    .MontantLivreur = If(IsDBNull(reader("MontantLivreur")), Nothing, CType(reader("MontantLivreur"), Decimal?))
                    .IDPeriode = If(IsDBNull(reader("IDPeriode")), Nothing, CType(reader("IDPeriode"), Int32?))
                End With
            Else
                clsPrixFixe = Nothing
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return clsPrixFixe
    End Function

    Public Function Add(ByVal clsPrixFixe As PrixFixe) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim insertStatement As String _
            = "INSERT " _
            & "     [PrixFixe] " _
            & "     ( " _
            & "     [IDOrganisation] " _
            & "    ,[IDLivreur] " _
            & "    ,[Livreur] " _
            & "    ,[Organisation] " _
            & "    ,[MontantLivreur] " _
            & "    ,[IDPeriode] " _
            & "     ) " _
            & "VALUES " _
            & "     ( " _
            & "     @IDOrganisation " _
            & "    ,@IDLivreur " _
            & "    ,@Livreur " _
            & "    ,@Organisation " _
            & "    ,@MontantLivreur " _
            & "    ,@IDPeriode " _
            & "     ) " _
            & ""
        Dim insertCommand As New SqlCommand(insertStatement, connection)
        insertCommand.CommandType = CommandType.Text
        insertCommand.Parameters.AddWithValue("@IDOrganisation", IIf(clsPrixFixe.IDOrganisation.HasValue, clsPrixFixe.IDOrganisation, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@IDLivreur", IIf(clsPrixFixe.IDLivreur.HasValue, clsPrixFixe.IDLivreur, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Livreur", IIf(Not IsNothing(clsPrixFixe.Livreur), clsPrixFixe.Livreur, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Organisation", IIf(Not IsNothing(clsPrixFixe.Organisation), clsPrixFixe.Organisation, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@MontantLivreur", IIf(clsPrixFixe.MontantLivreur.HasValue, clsPrixFixe.MontantLivreur, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@IDPeriode", IIf(clsPrixFixe.IDPeriode.HasValue, clsPrixFixe.IDPeriode, DBNull.Value))
        Try
            connection.Open()
            Dim count As Integer = insertCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

    Public Function Update(ByVal clsPrixFixeOld As PrixFixe,
           ByVal clsPrixFixeNew As PrixFixe) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim updateStatement As String _
            = "UPDATE " _
            & "     [PrixFixe] " _
            & "SET " _
            & "     [IDOrganisation] = @NewIDOrganisation " _
            & "    ,[IDLivreur] = @NewIDLivreur " _
            & "    ,[Livreur] = @NewLivreur " _
            & "    ,[Organisation] = @NewOrganisation " _
            & "    ,[MontantLivreur] = @NewMontantLivreur " _
            & "    ,[IDPeriode] = @NewIDPeriode " _
            & "WHERE " _
            & "     [IDPrixFixe] = @OldIDPrixFixe " _
            & ""
        Dim updateCommand As New SqlCommand(updateStatement, connection)
        updateCommand.CommandType = CommandType.Text
        updateCommand.Parameters.AddWithValue("@NewIDOrganisation", IIf(clsPrixFixeNew.IDOrganisation.HasValue, clsPrixFixeNew.IDOrganisation, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewIDLivreur", IIf(clsPrixFixeNew.IDLivreur.HasValue, clsPrixFixeNew.IDLivreur, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewLivreur", IIf(Not IsNothing(clsPrixFixeNew.Livreur), clsPrixFixeNew.Livreur, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewOrganisation", IIf(Not IsNothing(clsPrixFixeNew.Organisation), clsPrixFixeNew.Organisation, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewMontantLivreur", IIf(clsPrixFixeNew.MontantLivreur.HasValue, clsPrixFixeNew.MontantLivreur, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewIDPeriode", IIf(clsPrixFixeNew.IDPeriode.HasValue, clsPrixFixeNew.IDPeriode, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@OldIDPrixFixe", clsPrixFixeOld.IDPrixFixe)

        Try
            connection.Open()
            Dim count As Integer = updateCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

    Public Function Delete(ByVal clsPrixFixe As PrixFixe) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim deleteStatement As String _
            = "DELETE FROM " _
            & "     [PrixFixe] " _
            & "WHERE " _
            & "     [IDPrixFixe] = @OldIDPrixFixe " _
            & ""
        Dim deleteCommand As New SqlCommand(deleteStatement, connection)
        deleteCommand.CommandType = CommandType.Text
        deleteCommand.Parameters.AddWithValue("@OldIDPrixFixe", clsPrixFixe.IDPrixFixe)

        Try
            connection.Open()
            Dim count As Integer = deleteCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

End Class
 
