Imports System.Data.SqlClient

Public Class JourSemaineData

    Public Function SelectAll() As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDJourSemaine] " _
            & "    ,[Jour] " _
            & "FROM " _
            & "     [JourSemaine] " _
            & ""
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function

    Public Function Select_Record(ByVal clsJourSemainePara As JourSemaine) As JourSemaine
        Dim clsJourSemaine As New JourSemaine
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDJourSemaine] " _
            & "    ,[Jour] " _
            & "FROM " _
            & "     [JourSemaine] " _
            & "WHERE " _
            & "     [IDJourSemaine] = @IDJourSemaine " _
            & ""
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        selectCommand.Parameters.AddWithValue("@IDJourSemaine", clsJourSemainePara.IDJourSemaine)
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader(CommandBehavior.SingleRow)
            If reader.Read Then
                With clsJourSemaine
                    .IDJourSemaine = System.Convert.ToInt32(reader("IDJourSemaine"))
                    .Jour = If(IsDBNull(reader("Jour")), Nothing, reader("Jour").ToString)
                End With
            Else
                clsJourSemaine = Nothing
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return clsJourSemaine
    End Function

    Public Function Add(ByVal clsJourSemaine As JourSemaine) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim insertStatement As String _
            = "INSERT " _
            & "     [JourSemaine] " _
            & "     ( " _
            & "     [Jour] " _
            & "     ) " _
            & "VALUES " _
            & "     ( " _
            & "     @Jour " _
            & "     ) " _
            & ""
        Dim insertCommand As New SqlCommand(insertStatement, connection)
        insertCommand.CommandType = CommandType.Text
        insertCommand.Parameters.AddWithValue("@Jour", IIf(Not IsNothing(clsJourSemaine.Jour), clsJourSemaine.Jour, DBNull.Value))
        Try
            connection.Open()
            Dim count As Integer = insertCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

    Public Function Update(ByVal clsJourSemaineOld As JourSemaine,
           ByVal clsJourSemaineNew As JourSemaine) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim updateStatement As String _
            = "UPDATE " _
            & "     [JourSemaine] " _
            & "SET " _
            & "     [Jour] = @NewJour " _
            & "WHERE " _
            & "     [IDJourSemaine] = @OldIDJourSemaine " _
            & ""
        Dim updateCommand As New SqlCommand(updateStatement, connection)
        updateCommand.CommandType = CommandType.Text
        updateCommand.Parameters.AddWithValue("@NewJour", IIf(Not IsNothing(clsJourSemaineNew.Jour), clsJourSemaineNew.Jour, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@OldIDJourSemaine", clsJourSemaineOld.IDJourSemaine)

        Try
            connection.Open()
            Dim count As Integer = updateCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

    Public Function Delete(ByVal clsJourSemaine As JourSemaine) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim deleteStatement As String _
            = "DELETE FROM " _
            & "     [JourSemaine] " _
            & "WHERE " _
            & "     [IDJourSemaine] = @OldIDJourSemaine " _
            & ""
        Dim deleteCommand As New SqlCommand(deleteStatement, connection)
        deleteCommand.CommandType = CommandType.Text
        deleteCommand.Parameters.AddWithValue("@OldIDJourSemaine", clsJourSemaine.IDJourSemaine)
        deleteCommand.Parameters.AddWithValue("@OldJour", IIf(Not IsNothing(clsJourSemaine.Jour), clsJourSemaine.Jour, DBNull.Value))
        Try
            connection.Open()
            Dim count As Integer = deleteCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

End Class
 
