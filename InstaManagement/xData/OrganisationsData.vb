Imports System.Data.SqlClient

Public Class OrganisationsData

    Public Function SelectAllInvoicesParOrganisationEtatDeCompte(pdDateFrom As DateTime, pdDateTo As DateTime) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT DISTINCT dbo.organisations.OrganisationID " &
            " FROM   dbo.invoice " &
            " LEFT OUTER JOIN dbo.organisations " &
            " ON dbo.invoice.organisationid = " &
            " dbo.organisations.organisationid " &
            " WHERE dbo.organisations.OrganisationID IS NOT NULL AND (dbo.invoice.DateFrom  >= '" & pdDateFrom & "' AND dbo.invoice.DateTo <= '" & pdDateTo & "')   AND dbo.invoice.Total > 0"



        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function


    Public Function SelectAllMetodeCalculationLivreur() As Integer
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "Select " _
            & "    * " _
            & " FROM " _
            & "     [Organisations] WHERE MetodeCalculationLivreur Is  NULL"
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            Dim dr As DataRow

            If reader.HasRows Then
                dt.Load(reader)
                If dt.Rows.Count > 0 Then
                    For Each dr In dt.Rows
                        MessageBox.Show("Veuillez affecter une m�thode de calcul a " & dr("Organisation").ToString.Trim)
                    Next

                    Return dt.Rows.Count


                Else
                    Return 0
                End If

                Return 0

            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Erreur")
        Finally
            connection.Close()
        End Try

    End Function


    Public Function SelectAllMetodeCalculationOrganisation() As Integer
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "Select " _
            & "    * " _
            & " FROM " _
            & "     [Organisations] WHERE MetodeCalculationOrganisation Is  NULL"
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            Dim dr As DataRow

            If reader.HasRows Then
                dt.Load(reader)
                If dt.Rows.Count > 0 Then
                    For Each dr In dt.Rows

                        MessageBox.Show("Veuillez affecter une m�thode de calcul a " & dr("Organisation").ToString.Trim)
                    Next

                    Return dt.Rows.Count

                Else
                    Return 0
                End If

            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Erreur")
        Finally
            connection.Close()
        End Try

    End Function

    Public Function SelectAllRestaurant() As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "Select " _
            & "     [OrganisationID] " _
            & "    ,[Organisation] " _
            & "    ,[Adresse] " _
            & "    ,[Telephone] " _
            & "    ,[KMMax] " _
            & "    ,[MontantBonus] " _
            & "    ,[MontantContrat] " _
            & "    ,[Livadd] " _
            & "    ,[MetodeCalculationOrganisation] " _
            & "    ,[OrganisationImport] " _
            & "    ,[MetodeCalculationLivreur] " _
            & "    ,[QuantiteLivraison] " _
            & "    ,[CreditParLivraison] " _
            & "    ,[PayeLivreur] " _
            & "    ,[MontantGlacierOrganisation] " _
            & "    ,[MontantGlacierLivreur] " _
            & "    ,[Email] " _
            & "    ,[Email1] " _
            & "    ,[Email2] " _
            & "    ,[Email3] " _
            & "    ,[MontantContraHeure] " _
            & "    ,[MontantContraJour] " _
            & "    ,[MontantContraJourLivreur] " _
            & "    ,[Code] " _
            & "    ,[MontantMobilus] " _
            & "    ,[NombreFree30535] " _
            & "    ,[CacheEmail1] " _
            & "    ,[CacheEmail2] " _
            & "    ,[CacheEmail3] " _
            & "    ,[CacheEmail4] " _
            & "    ,[IDVendeur] " _
            & "    ,[Restaurant] " _
            & "    ,[CommissionVendeur] " _
            & "    ,[MontantFixe] " _
            & "    ,[MessageFacture] " _
            & "    ,[MessageEtat] " _
            & "    ,[IsCommission] " _
            & "    ,[isEtatDeCompte] " _
            & "    ,[MontantPrixFixeSem] " _
            & "    ,[MontantMaxLivraisoneExtra] " _
            & "    ,[MaximumLivraison] " _
            & "    ,[MontantParPorteOrganisation] " _
            & "    ,[MontantParPorteLivreur] " _
            & "FROM " _
            & "     [Organisations] " _
            & " WHERE Restaurant = 1 ORDER BY Organisation ASC "
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function

    Public Function SelectAll() As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "Select " _
            & "     [OrganisationID] " _
            & "    ,[Organisation] " _
            & "    ,[Adresse] " _
            & "    ,[Telephone] " _
            & "    ,[KMMax] " _
            & "    ,[MontantBonus] " _
            & "    ,[MontantContrat] " _
            & "    ,[Livadd] " _
            & "    ,[MetodeCalculationOrganisation] " _
            & "    ,[OrganisationImport] " _
            & "    ,[MetodeCalculationLivreur] " _
            & "    ,[QuantiteLivraison] " _
            & "    ,[CreditParLivraison] " _
            & "    ,[PayeLivreur] " _
            & "    ,[MontantGlacierOrganisation] " _
            & "    ,[MontantGlacierLivreur] " _
            & "    ,[Email] " _
            & "    ,[Email1] " _
            & "    ,[Email2] " _
            & "    ,[Email3] " _
            & "    ,[MontantContraHeure] " _
            & "    ,[MontantContraJour] " _
            & "    ,[MontantContraJourLivreur] " _
            & "    ,[Code] " _
            & "    ,[MontantMobilus] " _
            & "    ,[NombreFree30535] " _
            & "    ,[CacheEmail1] " _
            & "    ,[CacheEmail2] " _
            & "    ,[CacheEmail3] " _
            & "    ,[CacheEmail4] " _
            & "    ,[IDVendeur] " _
            & "    ,[Restaurant] " _
            & "    ,[CommissionVendeur] " _
            & "    ,[MontantFixe] " _
            & "    ,[MessageFacture] " _
            & "    ,[MessageEtat] " _
            & "    ,[IsCommission] " _
            & "    ,[isEtatDeCompte] " _
            & "    ,[MontantPrixFixeSem] " _
            & "    ,[MontantMaxLivraisoneExtra] " _
            & "    ,[MaximumLivraison] " _
            & "    ,[MontantParPorteOrganisation] " _
            & "    ,[MontantParPorteLivreur] " _
            & "FROM " _
            & "     [Organisations] " _
            & ""
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function
    Public Function SelectByIDVendeur(piIDVendeur As Integer) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "Select " _
            & "     [OrganisationID] " _
            & "    ,[Organisation] " _
            & "    ,[Adresse] " _
            & "    ,[Telephone] " _
            & "    ,[KMMax] " _
            & "    ,[MontantBonus] " _
            & "    ,[MontantContrat] " _
            & "    ,[Livadd] " _
            & "    ,[MetodeCalculationOrganisation] " _
            & "    ,[OrganisationImport] " _
            & "    ,[MetodeCalculationLivreur] " _
            & "    ,[QuantiteLivraison] " _
            & "    ,[CreditParLivraison] " _
            & "    ,[PayeLivreur] " _
            & "    ,[MontantGlacierOrganisation] " _
            & "    ,[MontantGlacierLivreur] " _
            & "    ,[Email] " _
            & "    ,[Email1] " _
            & "    ,[Email2] " _
            & "    ,[Email3] " _
            & "    ,[MontantContraHeure] " _
            & "    ,[MontantContraJour] " _
            & "    ,[MontantContraJourLivreur] " _
            & "    ,[Code] " _
            & "    ,[MontantMobilus] " _
            & "    ,[NombreFree30535] " _
            & "    ,[CacheEmail1] " _
            & "    ,[CacheEmail2] " _
            & "    ,[CacheEmail3] " _
            & "    ,[CacheEmail4] " _
            & "    ,[IDVendeur] " _
            & "    ,[Restaurant] " _
            & "    ,[CommissionVendeur] " _
            & "    ,[MontantFixe] " _
            & "    ,[MessageFacture] " _
            & "    ,[MessageEtat] " _
            & "    ,[IsCommission] " _
            & "    ,[isEtatDeCompte] " _
            & "    ,[MontantPrixFixeSem] " _
            & "    ,[MontantMaxLivraisoneExtra] " _
            & "    ,[MaximumLivraison] " _
            & "    ,[MontantParPorteOrganisation] " _
            & "    ,[MontantParPorteLivreur] " _
            & " FROM " _
            & "     [Organisations] " _
            & " WHERE IDVendeur= " & piIDVendeur _
            & " AND IsCommission= 1"

        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Erreur")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function
    Public Function SelectByID(piOrganisationID As Integer) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "Select " _
            & "     [OrganisationID] " _
            & "    ,[Organisation] " _
            & "    ,[Adresse] " _
            & "    ,[Telephone] " _
            & "    ,[KMMax] " _
            & "    ,[MontantBonus] " _
            & "    ,[MontantContrat] " _
            & "    ,[Livadd] " _
            & "    ,[MetodeCalculationOrganisation] " _
            & "    ,[OrganisationImport] " _
            & "    ,[MetodeCalculationLivreur] " _
            & "    ,[QuantiteLivraison] " _
            & "    ,[CreditParLivraison] " _
            & "    ,[PayeLivreur] " _
            & "    ,[MontantGlacierOrganisation] " _
            & "    ,[MontantGlacierLivreur] " _
            & "    ,[Email] " _
            & "    ,[Email1] " _
            & "    ,[Email2] " _
            & "    ,[Email3] " _
            & "    ,[MontantContraHeure] " _
            & "    ,[MontantContraJour] " _
            & "    ,[MontantContraJourLivreur] " _
            & "    ,[Code] " _
            & "    ,[MontantMobilus] " _
            & "    ,[NombreFree30535] " _
            & "    ,[CacheEmail1] " _
            & "    ,[CacheEmail2] " _
            & "    ,[CacheEmail3] " _
            & "    ,[CacheEmail4] " _
            & "    ,[IDVendeur] " _
            & "    ,[Restaurant] " _
            & "    ,[CommissionVendeur] " _
            & "    ,[MontantFixe] " _
            & "    ,[MessageFacture] " _
            & "    ,[MessageEtat] " _
            & "    ,[IsCommission] " _
            & "    ,[isEtatDeCompte] " _
            & "    ,[MontantPrixFixeSem] " _
            & "    ,[MontantMaxLivraisoneExtra] " _
            & "    ,[MaximumLivraison] " _
            & "    ,[MontantParPorteOrganisation] " _
            & "    ,[MontantParPorteLivreur] " _
            & " FROM " _
            & "     [Organisations] " _
            & "WHERE OrganisationID=" & piOrganisationID

        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Erreur")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function


    Public Function SelectByName(psOrganisation1 As String) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "Select " _
            & "     [OrganisationID] " _
            & "    ,[Organisation] " _
            & "    ,[Adresse] " _
            & "    ,[Telephone] " _
            & "    ,[KMMax] " _
            & "    ,[MontantBonus] " _
            & "    ,[MontantContrat] " _
            & "    ,[Livadd] " _
            & "    ,[MetodeCalculationOrganisation] " _
            & "    ,[OrganisationImport] " _
            & "    ,[MetodeCalculationLivreur] " _
            & "    ,[QuantiteLivraison] " _
            & "    ,[CreditParLivraison] " _
            & "    ,[PayeLivreur] " _
            & "    ,[MontantGlacierOrganisation] " _
            & "    ,[MontantGlacierLivreur] " _
            & "    ,[Email] " _
            & "    ,[Email1] " _
            & "    ,[Email2] " _
            & "    ,[Email3] " _
            & "    ,[MontantContraHeure] " _
            & "    ,[MontantContraJour] " _
            & "    ,[MontantContraJourLivreur] " _
            & "    ,[Code] " _
            & "    ,[MontantMobilus] " _
            & "    ,[NombreFree30535] " _
            & "    ,[CacheEmail1] " _
            & "    ,[CacheEmail2] " _
            & "    ,[CacheEmail3] " _
            & "    ,[CacheEmail4] " _
            & "    ,[IDVendeur] " _
            & "    ,[Restaurant] " _
            & "    ,[CommissionVendeur] " _
            & "    ,[MontantFixe] " _
            & "    ,[MessageFacture] " _
            & "    ,[MessageEtat] " _
            & "    ,[IsCommission] " _
            & "    ,[isEtatDeCompte] " _
            & "    ,[MontantPrixFixeSem] " _
            & "    ,[MontantMaxLivraisoneExtra] " _
            & "    ,[MaximumLivraison] " _
            & "    ,[MontantParPorteOrganisation] " _
            & "    ,[MontantParPorteLivreur] " _
            & "FROM " _
            & "     [Organisations] " _
            & "WHERE OrganisationImport='" & SysTemGlobals.DoubleQuote(psOrganisation1) & "'"

        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Erreur")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function
    Public Function SelectByNamePharmaplusMassicotte() As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [OrganisationID] " _
            & "    ,[Organisation] " _
            & "    ,[Adresse] " _
            & "    ,[Telephone] " _
            & "    ,[KMMax] " _
            & "    ,[MontantBonus] " _
            & "    ,[MontantContrat] " _
            & "    ,[Livadd] " _
            & "    ,[MetodeCalculationOrganisation] " _
            & "    ,[OrganisationImport] " _
            & "    ,[MetodeCalculationLivreur] " _
            & "    ,[QuantiteLivraison] " _
            & "    ,[CreditParLivraison] " _
            & "    ,[PayeLivreur] " _
            & "    ,[MontantGlacierOrganisation] " _
            & "    ,[MontantGlacierLivreur] " _
            & "    ,[Email] " _
            & "    ,[Email1] " _
            & "    ,[Email2] " _
            & "    ,[Email3] " _
            & "    ,[MontantContraHeure] " _
            & "    ,[MontantContraJour] " _
            & "    ,[MontantContraJourLivreur] " _
            & "    ,[Code] " _
            & "    ,[MontantMobilus] " _
            & "    ,[NombreFree30535] " _
            & "    ,[CacheEmail1] " _
            & "    ,[CacheEmail2] " _
            & "    ,[CacheEmail3] " _
            & "    ,[CacheEmail4] " _
            & "    ,[IDVendeur] " _
            & "    ,[Restaurant] " _
            & "    ,[CommissionVendeur] " _
            & "    ,[MontantFixe] " _
            & "    ,[MessageFacture] " _
            & "    ,[MessageEtat] " _
            & "    ,[IsCommission] " _
            & "    ,[isEtatDeCompte] " _
            & "    ,[MontantPrixFixeSem] " _
            & "    ,[MontantMaxLivraisoneExtra] " _
            & "    ,[MaximumLivraison] " _
            & "    ,[MontantParPorteOrganisation] " _
            & "    ,[MontantParPorteLivreur] " _
            & "FROM " _
            & "     [Organisations] "


        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Erreur")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function

    Public Function Select_RecordVendeur(ByVal clsOrganisationsPara As Organisations) As Organisations
        Dim clsOrganisations As New Organisations
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     dbo.Organisations.OrganisationID " _
            & "    ,dbo.Organisations.Organisation " _
            & "    ,dbo.Organisations.Adresse " _
            & "    ,dbo.Organisations.Telephone " _
            & "    ,dbo.Organisations.KMMax " _
            & "    ,dbo.Organisations.MontantBonus " _
            & "    ,dbo.Organisations.MontantContrat " _
            & "    ,dbo.Organisations.Livadd " _
            & "    ,dbo.Organisations.MetodeCalculationOrganisation " _
            & "    ,dbo.Organisations.OrganisationImport " _
            & "    ,dbo.Organisations.MetodeCalculationLivreur " _
            & "    ,dbo.Organisations.QuantiteLivraison " _
            & "    ,dbo.Organisations.CreditParLivraison " _
            & "    ,dbo.Organisations.PayeLivreur " _
            & "    ,dbo.Organisations.MontantGlacierOrganisation " _
            & "    ,dbo.Organisations.MontantGlacierLivreur " _
            & "    ,dbo.Organisations.Email " _
            & "    ,dbo.Organisations.Email1 " _
            & "    ,dbo.Organisations.Email2 " _
            & "    ,dbo.Organisations.Email3 " _
            & "    ,dbo.Organisations.MontantContraHeure " _
            & "    ,dbo.Organisations.MontantContraJour " _
            & "    ,dbo.Organisations.MontantContraJourLivreur " _
            & "    ,dbo.Organisations.Code " _
            & "    ,dbo.Organisations.MontantMobilus " _
            & "    ,dbo.Organisations.NombreFree30535 " _
            & "    ,dbo.Organisations.CacheEmail1 " _
            & "    ,dbo.Organisations.CacheEmail2 " _
            & "    ,dbo.Organisations.CacheEmail3 " _
            & "    ,dbo.Organisations.CacheEmail4 " _
            & "    ,dbo.Organisations.IDVendeur " _
            & "    ,dbo.Organisations.Restaurant " _
            & "    ,dbo.Organisations.CommissionVendeur " _
            & "    ,dbo.Organisations.MontantFixe " _
            & "    ,dbo.Organisations.MessageFacture" _
            & "    ,dbo.Organisations.MessageEtat" _
            & "    ,dbo.Organisations.IsCommission" _
            & "    ,dbo.Organisations.isEtatDeCompte" _
            & "    ,dbo.livreurs.telephone AS VendeurTelephone " _
            & "    ,dbo.livreurs.livreur AS Vendeur" _
            & "    ,dbo.livreurs.Adresse AS addresse" _
            & "    ,dbo.livreurs.Email AS Courriel" _
            & " FROM " _
            & " dbo.Organisations LEFT OUTER JOIN " _
            & " dbo.livreurs ON dbo.Organisations.IDVendeur = dbo.livreurs.LivreurID " _
            & " WHERE " _
            & "    OrganisationID = @OrganisationID " _
            & ""
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        selectCommand.Parameters.AddWithValue("@OrganisationID", clsOrganisationsPara.OrganisationID)
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader(CommandBehavior.SingleRow)
            If reader.Read Then
                With clsOrganisations
                    .OrganisationID = System.Convert.ToInt32(reader("OrganisationID"))
                    .Organisation = If(IsDBNull(reader("Organisation")), Nothing, reader("Organisation").ToString)
                    .Adresse = If(IsDBNull(reader("Adresse")), Nothing, reader("Adresse").ToString)
                    .Telephone = If(IsDBNull(reader("Telephone")), Nothing, reader("Telephone").ToString)
                    .KMMax = If(IsDBNull(reader("KMMax")), Nothing, CType(CType(reader("KMMax"), Double?), Decimal?))
                    .MontantBonus = If(IsDBNull(reader("MontantBonus")), Nothing, CType(reader("MontantBonus"), Decimal?))
                    .MontantContrat = If(IsDBNull(reader("MontantContrat")), Nothing, CType(reader("MontantContrat"), Decimal?))
                    .Livadd = If(IsDBNull(reader("Livadd")), Nothing, CType(reader("Livadd"), Decimal?))
                    .MetodeCalculationOrganisation = If(IsDBNull(reader("MetodeCalculationOrganisation")), Nothing, CType(reader("MetodeCalculationOrganisation"), Int32?))
                    .OrganisationImport = If(IsDBNull(reader("OrganisationImport")), Nothing, reader("OrganisationImport").ToString)
                    .MetodeCalculationLivreur = If(IsDBNull(reader("MetodeCalculationLivreur")), Nothing, CType(reader("MetodeCalculationLivreur"), Int32?))
                    .QuantiteLivraison = If(IsDBNull(reader("QuantiteLivraison")), Nothing, CType(reader("QuantiteLivraison"), Int32?))
                    .CreditParLivraison = If(IsDBNull(reader("CreditParLivraison")), Nothing, CType(reader("CreditParLivraison"), Decimal?))
                    .PayeLivreur = If(IsDBNull(reader("PayeLivreur")), Nothing, CType(reader("PayeLivreur"), Decimal?))
                    .MontantGlacierOrganisation = If(IsDBNull(reader("MontantGlacierOrganisation")), Nothing, CType(reader("MontantGlacierOrganisation"), Decimal?))
                    .MontantGlacierLivreur = If(IsDBNull(reader("MontantGlacierLivreur")), Nothing, CType(reader("MontantGlacierLivreur"), Decimal?))
                    .Email = If(IsDBNull(reader("Email")), Nothing, reader("Email").ToString)
                    .Email1 = If(IsDBNull(reader("Email1")), Nothing, reader("Email1").ToString)
                    .Email2 = If(IsDBNull(reader("Email2")), Nothing, reader("Email2").ToString)
                    .Email3 = If(IsDBNull(reader("Email3")), Nothing, reader("Email3").ToString)
                    .MontantContraHeure = If(IsDBNull(reader("MontantContraHeure")), Nothing, CType(reader("MontantContraHeure"), Decimal?))
                    .MontantContraJour = If(IsDBNull(reader("MontantContraJour")), Nothing, CType(reader("MontantContraJour"), Decimal?))
                    .MontantContraJourLivreur = If(IsDBNull(reader("MontantContraJourLivreur")), Nothing, CType(reader("MontantContraJourLivreur"), Decimal?))
                    .Code = If(IsDBNull(reader("Code")), Nothing, reader("Code").ToString)
                    .MontantMobilus = If(IsDBNull(reader("MontantMobilus")), Nothing, CType(reader("MontantMobilus"), Decimal?))
                    .NombreFree30535 = If(IsDBNull(reader("NombreFree30535")), Nothing, CType(reader("NombreFree30535"), Int32?))
                    .CacheEmail1 = If(IsDBNull(reader("CacheEmail1")), Nothing, CType(reader("CacheEmail1"), Boolean?))
                    .CacheEmail2 = If(IsDBNull(reader("CacheEmail2")), Nothing, CType(reader("CacheEmail2"), Boolean?))
                    .CacheEmail3 = If(IsDBNull(reader("CacheEmail3")), Nothing, CType(reader("CacheEmail3"), Boolean?))
                    .CacheEmail4 = If(IsDBNull(reader("CacheEmail4")), Nothing, CType(reader("CacheEmail4"), Boolean?))
                    .IDVendeur = If(IsDBNull(reader("IDVendeur")), Nothing, CType(reader("IDVendeur"), Int32?))
                    .Restaurant = If(IsDBNull(reader("Restaurant")), Nothing, CType(reader("Restaurant"), Boolean?))
                    .CommissionVendeur = If(IsDBNull(reader("CommissionVendeur")), Nothing, CType(CType(reader("CommissionVendeur"), Double?), Decimal?))
                    .MontantFixe = If(IsDBNull(reader("MontantFixe")), Nothing, CType(reader("MontantFixe"), Decimal?))
                    .MessageFacture = If(IsDBNull(reader("MessageFacture")), Nothing, reader("MessageFacture").ToString)
                    .VendeurTelephone = If(IsDBNull(reader("VendeurTelephone")), Nothing, reader("VendeurTelephone").ToString.Trim)
                    .Addresse = If(IsDBNull(reader("Addresse")), Nothing, reader("Addresse").ToString.Trim)
                    .Vendeur = If(IsDBNull(reader("Vendeur")), Nothing, reader("Vendeur").ToString.Trim)
                    .Courriel = If(IsDBNull(reader("Courriel")), Nothing, reader("Courriel").ToString.Trim)
                    .MessageEtat = If(IsDBNull(reader("MessageEtat")), Nothing, reader("MessageEtat").ToString.Trim)
                    .IsCommission = If(IsDBNull(reader("IsCommission")), Nothing, CType(reader("IsCommission"), Boolean?))
                    .isEtatDeCompte = If(IsDBNull(reader("isEtatDeCompte")), Nothing, CType(reader("isEtatDeCompte"), Boolean?))



                End With
            Else
                clsOrganisations = Nothing
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return clsOrganisations
    End Function


    Public Function Select_Record(ByVal clsOrganisationsPara As Organisations) As Organisations
        Dim clsOrganisations As New Organisations
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [OrganisationID] " _
            & "    ,[Organisation] " _
            & "    ,[Adresse] " _
            & "    ,[Telephone] " _
            & "    ,[KMMax] " _
            & "    ,[MontantBonus] " _
            & "    ,[MontantContrat] " _
            & "    ,[Livadd] " _
            & "    ,[MetodeCalculationOrganisation] " _
            & "    ,[OrganisationImport] " _
            & "    ,[MetodeCalculationLivreur] " _
            & "    ,[QuantiteLivraison] " _
            & "    ,[CreditParLivraison] " _
            & "    ,[PayeLivreur] " _
            & "    ,[MontantGlacierOrganisation] " _
            & "    ,[MontantGlacierLivreur] " _
            & "    ,[Email] " _
            & "    ,[Email1] " _
            & "    ,[Email2] " _
            & "    ,[Email3] " _
            & "    ,[MontantContraHeure] " _
            & "    ,[MontantContraJour] " _
            & "    ,[MontantContraJourLivreur] " _
            & "    ,[Code] " _
            & "    ,[MontantMobilus] " _
            & "    ,[NombreFree30535] " _
            & "    ,[CacheEmail1] " _
            & "    ,[CacheEmail2] " _
            & "    ,[CacheEmail3] " _
            & "    ,[CacheEmail4] " _
            & "    ,[IDVendeur] " _
            & "    ,[Restaurant] " _
            & "    ,[CommissionVendeur] " _
            & "    ,[MontantFixe] " _
            & "    ,[MessageFacture] " _
            & "    ,[MessageEtat] " _
            & "    ,[IsCommission] " _
            & "    ,[isEtatDeCompte] " _
            & "    ,[MontantPrixFixeSem] " _
            & "    ,[MontantMaxLivraisoneExtra] " _
            & "    ,[MaximumLivraison] " _
            & "    ,[MontantParPorteOrganisation] " _
            & "    ,[MontantParPorteLivreur] " _
            & "FROM " _
            & "     [Organisations] " _
            & "WHERE " _
            & "     [OrganisationID] = @OrganisationID " _
            & ""
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        selectCommand.Parameters.AddWithValue("@OrganisationID", clsOrganisationsPara.OrganisationID)
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader(CommandBehavior.SingleRow)
            If reader.Read Then
                With clsOrganisations
                    .OrganisationID = System.Convert.ToInt32(reader("OrganisationID"))
                    .Organisation = If(IsDBNull(reader("Organisation")), Nothing, reader("Organisation").ToString)
                    .Adresse = If(IsDBNull(reader("Adresse")), Nothing, reader("Adresse").ToString)
                    .Telephone = If(IsDBNull(reader("Telephone")), Nothing, reader("Telephone").ToString)
                    .KMMax = If(IsDBNull(reader("KMMax")), Nothing, CType(CType(reader("KMMax"), Double?), Decimal?))
                    .MontantBonus = If(IsDBNull(reader("MontantBonus")), Nothing, CType(reader("MontantBonus"), Decimal?))
                    .MontantContrat = If(IsDBNull(reader("MontantContrat")), Nothing, CType(reader("MontantContrat"), Decimal?))
                    .Livadd = If(IsDBNull(reader("Livadd")), Nothing, CType(reader("Livadd"), Decimal?))
                    .MetodeCalculationOrganisation = If(IsDBNull(reader("MetodeCalculationOrganisation")), Nothing, CType(reader("MetodeCalculationOrganisation"), Int32?))
                    .OrganisationImport = If(IsDBNull(reader("OrganisationImport")), Nothing, reader("OrganisationImport").ToString)
                    .MetodeCalculationLivreur = If(IsDBNull(reader("MetodeCalculationLivreur")), Nothing, CType(reader("MetodeCalculationLivreur"), Int32?))
                    .QuantiteLivraison = If(IsDBNull(reader("QuantiteLivraison")), Nothing, CType(reader("QuantiteLivraison"), Int32?))
                    .CreditParLivraison = If(IsDBNull(reader("CreditParLivraison")), Nothing, CType(reader("CreditParLivraison"), Decimal?))
                    .PayeLivreur = If(IsDBNull(reader("PayeLivreur")), Nothing, CType(reader("PayeLivreur"), Decimal?))
                    .MontantGlacierOrganisation = If(IsDBNull(reader("MontantGlacierOrganisation")), Nothing, CType(reader("MontantGlacierOrganisation"), Decimal?))
                    .MontantGlacierLivreur = If(IsDBNull(reader("MontantGlacierLivreur")), Nothing, CType(reader("MontantGlacierLivreur"), Decimal?))
                    .Email = If(IsDBNull(reader("Email")), Nothing, reader("Email").ToString)
                    .Email1 = If(IsDBNull(reader("Email1")), Nothing, reader("Email1").ToString)
                    .Email2 = If(IsDBNull(reader("Email2")), Nothing, reader("Email2").ToString)
                    .Email3 = If(IsDBNull(reader("Email3")), Nothing, reader("Email3").ToString)
                    .MontantContraHeure = If(IsDBNull(reader("MontantContraHeure")), Nothing, CType(reader("MontantContraHeure"), Decimal?))
                    .MontantContraJour = If(IsDBNull(reader("MontantContraJour")), Nothing, CType(reader("MontantContraJour"), Decimal?))
                    .MontantContraJourLivreur = If(IsDBNull(reader("MontantContraJourLivreur")), Nothing, CType(reader("MontantContraJourLivreur"), Decimal?))
                    .Code = If(IsDBNull(reader("Code")), Nothing, reader("Code").ToString)
                    .MontantMobilus = If(IsDBNull(reader("MontantMobilus")), Nothing, CType(reader("MontantMobilus"), Decimal?))
                    .NombreFree30535 = If(IsDBNull(reader("NombreFree30535")), Nothing, CType(reader("NombreFree30535"), Int32?))
                    .CacheEmail1 = If(IsDBNull(reader("CacheEmail1")), Nothing, CType(reader("CacheEmail1"), Boolean?))
                    .CacheEmail2 = If(IsDBNull(reader("CacheEmail2")), Nothing, CType(reader("CacheEmail2"), Boolean?))
                    .CacheEmail3 = If(IsDBNull(reader("CacheEmail3")), Nothing, CType(reader("CacheEmail3"), Boolean?))
                    .CacheEmail4 = If(IsDBNull(reader("CacheEmail4")), Nothing, CType(reader("CacheEmail4"), Boolean?))
                    .IDVendeur = If(IsDBNull(reader("IDVendeur")), Nothing, CType(reader("IDVendeur"), Int32?))
                    .Restaurant = If(IsDBNull(reader("Restaurant")), Nothing, CType(reader("Restaurant"), Boolean?))
                    .CommissionVendeur = If(IsDBNull(reader("CommissionVendeur")), Nothing, CType(CType(reader("CommissionVendeur"), Double?), Decimal?))
                    .MontantFixe = If(IsDBNull(reader("MontantFixe")), Nothing, CType(reader("MontantFixe"), Decimal?))
                    .MessageFacture = If(IsDBNull(reader("MessageFacture")), Nothing, reader("MessageFacture").ToString)
                    .MessageEtat = If(IsDBNull(reader("MessageEtat")), Nothing, reader("MessageEtat").ToString)
                    .IsCommission = If(IsDBNull(reader("IsCommission")), Nothing, CType(reader("IsCommission"), Boolean?))
                    .isEtatDeCompte = If(IsDBNull(reader("isEtatDeCompte")), Nothing, CType(reader("isEtatDeCompte"), Boolean?))
                    .MontantPrixFixeSem = If(IsDBNull(reader("MontantPrixFixeSem")), Nothing, CType(reader("MontantPrixFixeSem"), Decimal?))
                    .MontantMaxLivraisoneExtra = If(IsDBNull(reader("MontantMaxLivraisoneExtra")), Nothing, CType(reader("MontantMaxLivraisoneExtra"), Decimal?))
                    .MaximumLivraison = If(IsDBNull(reader("MaximumLivraison")), Nothing, CType(reader("MaximumLivraison"), Int32?))
                    .MontantParPorteOrganisation = If(IsDBNull(reader("MontantParPorteOrganisation")), Nothing, CType(reader("MontantParPorteOrganisation"), Decimal?))
                    .MontantParPorteLivreur = If(IsDBNull(reader("MontantParPorteLivreur")), Nothing, CType(reader("MontantParPorteLivreur"), Decimal?))
                End With
            Else
                clsOrganisations = Nothing
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return clsOrganisations
    End Function

    Public Function Add(ByVal clsOrganisations As Organisations) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim insertStatement As String _
            = "INSERT " _
            & "     [Organisations] " _
            & "     ( " _
            & "     [Organisation] " _
            & "    ,[Adresse] " _
            & "    ,[Telephone] " _
            & "    ,[KMMax] " _
            & "    ,[MontantBonus] " _
            & "    ,[MontantContrat] " _
            & "    ,[Livadd] " _
            & "    ,[MetodeCalculationOrganisation] " _
            & "    ,[OrganisationImport] " _
            & "    ,[MetodeCalculationLivreur] " _
            & "    ,[QuantiteLivraison] " _
            & "    ,[CreditParLivraison] " _
            & "    ,[PayeLivreur] " _
            & "    ,[MontantGlacierOrganisation] " _
            & "    ,[MontantGlacierLivreur] " _
            & "    ,[Email] " _
            & "    ,[Email1] " _
            & "    ,[Email2] " _
            & "    ,[Email3] " _
            & "    ,[MontantContraHeure] " _
            & "    ,[MontantContraJour] " _
            & "    ,[MontantContraJourLivreur] " _
            & "    ,[Code] " _
            & "    ,[MontantMobilus] " _
            & "    ,[NombreFree30535] " _
            & "    ,[CacheEmail1] " _
            & "    ,[CacheEmail2] " _
            & "    ,[CacheEmail3] " _
            & "    ,[CacheEmail4] " _
            & "    ,[IDVendeur] " _
            & "    ,[Restaurant] " _
            & "    ,[CommissionVendeur] " _
            & "    ,[MontantFixe] " _
            & "    ,[MessageFacture] " _
            & "    ,[MessageEtat] " _
            & "    ,[IsCommission] " _
            & "    ,[isEtatDeCompte] " _
            & "    ,[MontantPrixFixeSem] " _
            & "    ,[MontantMaxLivraisoneExtra] " _
            & "    ,[MaximumLivraison] " _
            & "    ,[MontantParPorteOrganisation] " _
            & "    ,[MontantParPorteLivreur] " _
            & "     ) " _
            & "VALUES " _
            & "     ( " _
            & "     @Organisation " _
            & "    ,@Adresse " _
            & "    ,@Telephone " _
            & "    ,@KMMax " _
            & "    ,@MontantBonus " _
            & "    ,@MontantContrat " _
            & "    ,@Livadd " _
            & "    ,@MetodeCalculationOrganisation " _
            & "    ,@OrganisationImport " _
            & "    ,@MetodeCalculationLivreur " _
            & "    ,@QuantiteLivraison " _
            & "    ,@CreditParLivraison " _
            & "    ,@PayeLivreur " _
            & "    ,@MontantGlacierOrganisation " _
            & "    ,@MontantGlacierLivreur " _
            & "    ,@Email " _
            & "    ,@Email1 " _
            & "    ,@Email2 " _
            & "    ,@Email3 " _
            & "    ,@MontantContraHeure " _
            & "    ,@MontantContraJour " _
            & "    ,@MontantContraJourLivreur " _
            & "    ,@Code " _
            & "    ,@MontantMobilus " _
            & "    ,@NombreFree30535 " _
            & "    ,@CacheEmail1 " _
            & "    ,@CacheEmail2 " _
            & "    ,@CacheEmail3 " _
            & "    ,@CacheEmail4 " _
            & "    ,@IDVendeur " _
            & "    ,@Restaurant " _
            & "    ,@CommissionVendeur " _
            & "    ,@MontantFixe " _
            & "    ,@MessageFacture " _
            & "    ,@MessageEtat " _
            & "    ,@IsCommission " _
            & "    ,@isEtatDeCompte " _
            & "    ,@MontantPrixFixeSem " _
            & "    ,@MontantMaxLivraisoneExtra " _
            & "    ,@MaximumLivraison " _
            & "    ,@MontantParPorteOrganisation " _
            & "    ,@MontantParPorteLivreur " _
            & "     ) " _
            & ""
        Dim insertCommand As New SqlCommand(insertStatement, connection)
        insertCommand.CommandType = CommandType.Text
        insertCommand.Parameters.AddWithValue("@Organisation", IIf(Not IsNothing(clsOrganisations.Organisation), clsOrganisations.Organisation, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Adresse", IIf(Not IsNothing(clsOrganisations.Adresse), clsOrganisations.Adresse, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Telephone", IIf(Not IsNothing(clsOrganisations.Telephone), clsOrganisations.Telephone, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@KMMax", IIf(clsOrganisations.KMMax.HasValue, clsOrganisations.KMMax, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@MontantBonus", IIf(clsOrganisations.MontantBonus.HasValue, clsOrganisations.MontantBonus, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@MontantContrat", IIf(clsOrganisations.MontantContrat.HasValue, clsOrganisations.MontantContrat, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Livadd", IIf(clsOrganisations.Livadd.HasValue, clsOrganisations.Livadd, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@MetodeCalculationOrganisation", IIf(clsOrganisations.MetodeCalculationOrganisation.HasValue, clsOrganisations.MetodeCalculationOrganisation, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@OrganisationImport", IIf(Not IsNothing(clsOrganisations.OrganisationImport), clsOrganisations.OrganisationImport, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@MetodeCalculationLivreur", IIf(clsOrganisations.MetodeCalculationLivreur.HasValue, clsOrganisations.MetodeCalculationLivreur, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@QuantiteLivraison", IIf(clsOrganisations.QuantiteLivraison.HasValue, clsOrganisations.QuantiteLivraison, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@CreditParLivraison", IIf(clsOrganisations.CreditParLivraison.HasValue, clsOrganisations.CreditParLivraison, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@PayeLivreur", IIf(clsOrganisations.PayeLivreur.HasValue, clsOrganisations.PayeLivreur, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@MontantGlacierOrganisation", IIf(clsOrganisations.MontantGlacierOrganisation.HasValue, clsOrganisations.MontantGlacierOrganisation, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@MontantGlacierLivreur", IIf(clsOrganisations.MontantGlacierLivreur.HasValue, clsOrganisations.MontantGlacierLivreur, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Email", IIf(Not IsNothing(clsOrganisations.Email), clsOrganisations.Email, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Email1", IIf(Not IsNothing(clsOrganisations.Email1), clsOrganisations.Email1, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Email2", IIf(Not IsNothing(clsOrganisations.Email2), clsOrganisations.Email2, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Email3", IIf(Not IsNothing(clsOrganisations.Email3), clsOrganisations.Email3, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@MontantContraHeure", IIf(clsOrganisations.MontantContraHeure.HasValue, clsOrganisations.MontantContraHeure, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@MontantContraJour", IIf(clsOrganisations.MontantContraJour.HasValue, clsOrganisations.MontantContraJour, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@MontantContraJourLivreur", IIf(clsOrganisations.MontantContraJourLivreur.HasValue, clsOrganisations.MontantContraJourLivreur, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Code", IIf(Not IsNothing(clsOrganisations.Code), clsOrganisations.Code, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@MontantMobilus", IIf(clsOrganisations.MontantMobilus.HasValue, clsOrganisations.MontantMobilus, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@NombreFree30535", IIf(clsOrganisations.NombreFree30535.HasValue, clsOrganisations.NombreFree30535, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@CacheEmail1", IIf(clsOrganisations.CacheEmail1.HasValue, clsOrganisations.CacheEmail1, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@CacheEmail2", IIf(clsOrganisations.CacheEmail2.HasValue, clsOrganisations.CacheEmail2, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@CacheEmail3", IIf(clsOrganisations.CacheEmail3.HasValue, clsOrganisations.CacheEmail3, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@CacheEmail4", IIf(clsOrganisations.CacheEmail4.HasValue, clsOrganisations.CacheEmail4, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@IDVendeur", IIf(clsOrganisations.IDVendeur.HasValue, clsOrganisations.IDVendeur, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Restaurant", IIf(clsOrganisations.Restaurant.HasValue, clsOrganisations.Restaurant, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@CommissionVendeur", IIf(clsOrganisations.CommissionVendeur.HasValue, clsOrganisations.CommissionVendeur, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@MontantFixe", IIf(clsOrganisations.MontantFixe.HasValue, clsOrganisations.MontantFixe, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@MessageFacture", IIf(Not IsNothing(clsOrganisations.MessageFacture), clsOrganisations.MessageFacture, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@MessageEtat", IIf(Not IsNothing(clsOrganisations.MessageEtat), clsOrganisations.MessageEtat, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@IsCommission", IIf(clsOrganisations.IsCommission.HasValue, clsOrganisations.IsCommission, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@isEtatDeCompte", IIf(clsOrganisations.isEtatDeCompte.HasValue, clsOrganisations.isEtatDeCompte, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@MontantPrixFixeSem", IIf(clsOrganisations.MontantPrixFixeSem.HasValue, clsOrganisations.MontantPrixFixeSem, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@MontantMaxLivraisoneExtra", IIf(clsOrganisations.MontantMaxLivraisoneExtra.HasValue, clsOrganisations.MontantMaxLivraisoneExtra, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@MaximumLivraison", IIf(clsOrganisations.MaximumLivraison.HasValue, clsOrganisations.MaximumLivraison, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@MontantParPorteOrganisation", IIf(clsOrganisations.MontantParPorteOrganisation.HasValue, clsOrganisations.MontantParPorteOrganisation, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@MontantParPorteLivreur", IIf(clsOrganisations.MontantParPorteLivreur.HasValue, clsOrganisations.MontantParPorteLivreur, DBNull.Value))
        Try
            connection.Open()
            Dim count As Integer = insertCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

    Public Function UpdateMessage(ByVal clsOrganisationsNew As Organisations) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim updateStatement As String _
            = "UPDATE " _
            & "     [Organisations] " _
            & "SET " _
            & "    [MessageFacture] = @NewMessageFacture " _
            & ""
        Dim updateCommand As New SqlCommand(updateStatement, connection)
        updateCommand.CommandType = CommandType.Text
        updateCommand.Parameters.AddWithValue("@NewMessageFacture", IIf(Not IsNothing(clsOrganisationsNew.MessageFacture), clsOrganisationsNew.MessageFacture, DBNull.Value))
        Try
            connection.Open()
            Dim count As Integer = updateCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function


    Public Function UpdateIDVendeur() As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim updateStatement As String _
            = "UPDATE " _
            & "     [Organisations] " _
            & "SET " _
            & "     [IDVendeur] = NULL " _
            & "WHERE " _
            & "     [IDVendeur] =0 " _
            & ""
        Dim updateCommand As New SqlCommand(updateStatement, connection)
        updateCommand.CommandType = CommandType.Text
        Try
            connection.Open()
            Dim count As Integer = updateCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

    Public Function Update(ByVal clsOrganisationsOld As Organisations,
           ByVal clsOrganisationsNew As Organisations) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim updateStatement As String _
            = "UPDATE " _
            & "     [Organisations] " _
            & "SET " _
            & "     [Organisation] = @NewOrganisation " _
            & "    ,[Adresse] = @NewAdresse " _
            & "    ,[Telephone] = @NewTelephone " _
            & "    ,[KMMax] = @NewKMMax " _
            & "    ,[MontantBonus] = @NewMontantBonus " _
            & "    ,[MontantContrat] = @NewMontantContrat " _
            & "    ,[Livadd] = @NewLivadd " _
            & "    ,[MetodeCalculationOrganisation] = @NewMetodeCalculationOrganisation " _
            & "    ,[OrganisationImport] = @NewOrganisationImport " _
            & "    ,[MetodeCalculationLivreur] = @NewMetodeCalculationLivreur " _
            & "    ,[QuantiteLivraison] = @NewQuantiteLivraison " _
            & "    ,[CreditParLivraison] = @NewCreditParLivraison " _
            & "    ,[PayeLivreur] = @NewPayeLivreur " _
            & "    ,[MontantGlacierOrganisation] = @NewMontantGlacierOrganisation " _
            & "    ,[MontantGlacierLivreur] = @NewMontantGlacierLivreur " _
            & "    ,[Email] = @NewEmail " _
            & "    ,[Email1] = @NewEmail1 " _
            & "    ,[Email2] = @NewEmail2 " _
            & "    ,[Email3] = @NewEmail3 " _
            & "    ,[MontantContraHeure] = @NewMontantContraHeure " _
            & "    ,[MontantContraJour] = @NewMontantContraJour " _
            & "    ,[MontantContraJourLivreur] = @NewMontantContraJourLivreur " _
            & "    ,[Code] = @NewCode " _
            & "    ,[MontantMobilus] = @NewMontantMobilus " _
            & "    ,[NombreFree30535] = @NewNombreFree30535 " _
            & "    ,[CacheEmail1] = @NewCacheEmail1 " _
            & "    ,[CacheEmail2] = @NewCacheEmail2 " _
            & "    ,[CacheEmail3] = @NewCacheEmail3 " _
            & "    ,[CacheEmail4] = @NewCacheEmail4 " _
            & "    ,[IDVendeur] = @NewIDVendeur " _
            & "    ,[Restaurant] = @NewRestaurant " _
            & "    ,[CommissionVendeur] = @NewCommissionVendeur " _
            & "    ,[MontantFixe] = @NewMontantFixe " _
            & "    ,[MessageFacture] = @NewMessageFacture " _
            & "    ,[MessageEtat] = @NewMessageEtat " _
            & "    ,[IsCommission] = @NewIsCommission " _
            & "    ,[isEtatDeCompte] = @NewisEtatDeCompte " _
            & "    ,[MontantPrixFixeSem] = @NewMontantPrixFixeSem " _
            & "    ,[MontantMaxLivraisoneExtra] = @NewMontantMaxLivraisoneExtra " _
            & "    ,[MaximumLivraison] = @NewMaximumLivraison " _
            & "    ,[MontantParPorteOrganisation] = @NewMontantParPorteOrganisation " _
            & "    ,[MontantParPorteLivreur] = @NewMontantParPorteLivreur " _
            & "WHERE " _
            & "     [OrganisationID] = @OldOrganisationID " _
            & ""
        Dim updateCommand As New SqlCommand(updateStatement, connection)
        updateCommand.CommandType = CommandType.Text
        updateCommand.Parameters.AddWithValue("@NewOrganisation", IIf(Not IsNothing(clsOrganisationsNew.Organisation), clsOrganisationsNew.Organisation, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewAdresse", IIf(Not IsNothing(clsOrganisationsNew.Adresse), clsOrganisationsNew.Adresse, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewTelephone", IIf(Not IsNothing(clsOrganisationsNew.Telephone), clsOrganisationsNew.Telephone, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewKMMax", IIf(clsOrganisationsNew.KMMax.HasValue, clsOrganisationsNew.KMMax, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewMontantBonus", IIf(clsOrganisationsNew.MontantBonus.HasValue, clsOrganisationsNew.MontantBonus, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewMontantContrat", IIf(clsOrganisationsNew.MontantContrat.HasValue, clsOrganisationsNew.MontantContrat, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewLivadd", IIf(clsOrganisationsNew.Livadd.HasValue, clsOrganisationsNew.Livadd, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewMetodeCalculationOrganisation", IIf(clsOrganisationsNew.MetodeCalculationOrganisation.HasValue, clsOrganisationsNew.MetodeCalculationOrganisation, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewOrganisationImport", IIf(Not IsNothing(clsOrganisationsNew.OrganisationImport), clsOrganisationsNew.OrganisationImport, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewMetodeCalculationLivreur", IIf(clsOrganisationsNew.MetodeCalculationLivreur.HasValue, clsOrganisationsNew.MetodeCalculationLivreur, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewQuantiteLivraison", IIf(clsOrganisationsNew.QuantiteLivraison.HasValue, clsOrganisationsNew.QuantiteLivraison, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewCreditParLivraison", IIf(clsOrganisationsNew.CreditParLivraison.HasValue, clsOrganisationsNew.CreditParLivraison, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewPayeLivreur", IIf(clsOrganisationsNew.PayeLivreur.HasValue, clsOrganisationsNew.PayeLivreur, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewMontantGlacierOrganisation", IIf(clsOrganisationsNew.MontantGlacierOrganisation.HasValue, clsOrganisationsNew.MontantGlacierOrganisation, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewMontantGlacierLivreur", IIf(clsOrganisationsNew.MontantGlacierLivreur.HasValue, clsOrganisationsNew.MontantGlacierLivreur, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewEmail", IIf(Not IsNothing(clsOrganisationsNew.Email), clsOrganisationsNew.Email, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewEmail1", IIf(Not IsNothing(clsOrganisationsNew.Email1), clsOrganisationsNew.Email1, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewEmail2", IIf(Not IsNothing(clsOrganisationsNew.Email2), clsOrganisationsNew.Email2, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewEmail3", IIf(Not IsNothing(clsOrganisationsNew.Email3), clsOrganisationsNew.Email3, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewMontantContraHeure", IIf(clsOrganisationsNew.MontantContraHeure.HasValue, clsOrganisationsNew.MontantContraHeure, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewMontantContraJour", IIf(clsOrganisationsNew.MontantContraJour.HasValue, clsOrganisationsNew.MontantContraJour, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewMontantContraJourLivreur", IIf(clsOrganisationsNew.MontantContraJourLivreur.HasValue, clsOrganisationsNew.MontantContraJourLivreur, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewCode", IIf(Not IsNothing(clsOrganisationsNew.Code), clsOrganisationsNew.Code, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewMontantMobilus", IIf(clsOrganisationsNew.MontantMobilus.HasValue, clsOrganisationsNew.MontantMobilus, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewNombreFree30535", IIf(clsOrganisationsNew.NombreFree30535.HasValue, clsOrganisationsNew.NombreFree30535, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewCacheEmail1", IIf(clsOrganisationsNew.CacheEmail1.HasValue, clsOrganisationsNew.CacheEmail1, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewCacheEmail2", IIf(clsOrganisationsNew.CacheEmail2.HasValue, clsOrganisationsNew.CacheEmail2, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewCacheEmail3", IIf(clsOrganisationsNew.CacheEmail3.HasValue, clsOrganisationsNew.CacheEmail3, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewCacheEmail4", IIf(clsOrganisationsNew.CacheEmail4.HasValue, clsOrganisationsNew.CacheEmail4, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewIDVendeur", IIf(clsOrganisationsNew.IDVendeur.HasValue, clsOrganisationsNew.IDVendeur, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewRestaurant", IIf(clsOrganisationsNew.Restaurant.HasValue, clsOrganisationsNew.Restaurant, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewCommissionVendeur", IIf(clsOrganisationsNew.CommissionVendeur.HasValue, clsOrganisationsNew.CommissionVendeur, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewMontantFixe", IIf(clsOrganisationsNew.MontantFixe.HasValue, clsOrganisationsNew.MontantFixe, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewMessageFacture", IIf(Not IsNothing(clsOrganisationsNew.MessageFacture), clsOrganisationsNew.MessageFacture, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewMessageEtat", IIf(Not IsNothing(clsOrganisationsNew.MessageEtat), clsOrganisationsNew.MessageEtat, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewIsCommission", IIf(clsOrganisationsNew.IsCommission.HasValue, clsOrganisationsNew.IsCommission, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewisEtatDeCompte", IIf(clsOrganisationsNew.isEtatDeCompte.HasValue, clsOrganisationsNew.isEtatDeCompte, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewMontantPrixFixeSem", IIf(clsOrganisationsNew.MontantPrixFixeSem.HasValue, clsOrganisationsNew.MontantPrixFixeSem, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewMontantMaxLivraisoneExtra", IIf(clsOrganisationsNew.MontantMaxLivraisoneExtra.HasValue, clsOrganisationsNew.MontantMaxLivraisoneExtra, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewMaximumLivraison", IIf(clsOrganisationsNew.MaximumLivraison.HasValue, clsOrganisationsNew.MaximumLivraison, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewMontantParPorteOrganisation", IIf(clsOrganisationsNew.MontantParPorteOrganisation.HasValue, clsOrganisationsNew.MontantParPorteOrganisation, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewMontantParPorteLivreur", IIf(clsOrganisationsNew.MontantParPorteLivreur.HasValue, clsOrganisationsNew.MontantParPorteLivreur, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@OldOrganisationID", clsOrganisationsOld.OrganisationID)

        Try
            connection.Open()
            Dim count As Integer = updateCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function
    Public Function Delete(ByVal clsOrganisations As Organisations) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim deleteStatement As String _
            = "DELETE FROM " _
            & "     [Organisations] " _
            & "WHERE " _
            & "     [OrganisationID] = @OldOrganisationID " _
            & ""
        Dim deleteCommand As New SqlCommand(deleteStatement, connection)
        deleteCommand.CommandType = CommandType.Text
        deleteCommand.Parameters.AddWithValue("@OldOrganisationID", clsOrganisations.OrganisationID)

        Try
            connection.Open()
            Dim count As Integer = deleteCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

End Class

