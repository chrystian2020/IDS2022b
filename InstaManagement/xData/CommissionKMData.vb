Imports System.Data.SqlClient

Public Class CommissionKMData
    Public Function SelectMontantParKm(piOrganisation As Integer, pKM As Double) As Double
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
           = "SELECT " _
            & "    [CommissionKM].[Montant]  AS TotalMontant" _
            & " FROM " _
            & " [CommissionKM] " _
            & " WHERE OrganisationID =" & piOrganisation & " AND (KMTo > " & pKM & " AND KMFrom < " & pKM & ")"

        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Dim dTotalAmount As Double = 0

        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
                For Each drImport In dt.Rows
                    dTotalAmount = dTotalAmount + drImport("TotalMontant")
                Next
                Return dTotalAmount
            Else
                Return 0
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Erreur")
        Finally
            connection.Close()
        End Try

    End Function

    Public Function SelectAllByKM(piOrganisationID As Integer, pdblDistance As Double) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT  * " _
            & "FROM " _
            & "     [CommissionKM]  WHERE OrganisationID = @OrganisationID " _
            & " AND (@KM between KMFrom and KMTo)"


        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        selectCommand.Parameters.AddWithValue("@OrganisationID", piOrganisationID)
        selectCommand.Parameters.AddWithValue("@KM", pdblDistance)

        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)



            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Erreur")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function
    Public Function SelectAllByTimeKM(piOrganisation As Integer, pDateCompare As DateTime, pdblDistance As Double) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT  * " _
            & "FROM " _
            & "     [CommissionKM]  WHERE OrganisationID = @Organisation " _
            & " AND (CAST(@DateCompare as time) between CAST(HeureDepart as time) and CAST(HeureFin as time)) " _
            & " AND (@KM between KMFrom and KMTo)"


        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        selectCommand.Parameters.AddWithValue("@Organisation", piOrganisation)
        selectCommand.Parameters.AddWithValue("@DateCompare", pDateCompare)
        selectCommand.Parameters.AddWithValue("@KM", pdblDistance)

        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Erreur")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function
    Public Function SelectAllByTime(piOrganisation As Integer, pDateCompare As DateTime) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT  * " _
            & "FROM " _
            & "     [CommissionKM]  WHERE OrganisationID = @OrganisationID " _
            & " AND (CAST(@DateCompare as time) between CAST(HeureDepart as time) and CAST(HeureFin as time)) "



        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        selectCommand.Parameters.AddWithValue("@OrganisationID", piOrganisation)
        selectCommand.Parameters.AddWithValue("@DateCompare", pDateCompare)


        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Erreur")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function

    Public Function SelectAllByName(piOrganisation As Integer) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "Select " _
            & "     [CommissionKM].[IDCommissionKM] " _
            & "    ,[CommissionKM].[OrganisationID] " _
            & "    ,[CommissionKM].[Organisation] " _
            & "    ,[CommissionKM].[KMFrom] " _
            & "    ,[CommissionKM].[KMTo] " _
            & "    ,[CommissionKM].[Montant] " _
            & "    ,[CommissionKM].[ExtraChargeParKM] " _
            & "    ,[CommissionKM].[PerKMNumber] " _
            & "    ,[CommissionKM].[MetodeCalculation] " _
            & "    ,[HeureDepart] " _
            & "    ,[HeureFin] " _
            & "FROM " _
            & "     [CommissionKM]  WHERE OrganisationID = " & piOrganisation
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text



        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Erreur")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function

    Public Function SelectAll() As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [CommissionKM].[IDCommissionKM] " _
            & "    ,[CommissionKM].[OrganisationID] " _
            & "    ,[CommissionKM].[Organisation] " _
            & "    ,[CommissionKM].[KMFrom] " _
            & "    ,[CommissionKM].[KMTo] " _
            & "    ,[CommissionKM].[Montant] " _
            & "    ,[CommissionKM].[ExtraChargeParKM] " _
            & "    ,[CommissionKM].[PerKMNumber] " _
            & "    ,[CommissionKM].[MetodeCalculation] " _
            & "    ,[HeureDepart] " _
            & "    ,[HeureFin] " _
            & "FROM " _
            & "     [CommissionKM] " _
            & ""
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Erreur")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function

    Public Function Select_Record(ByVal clsCommissionKMPara As CommissionKM) As CommissionKM
        Dim clsCommissionKM As New CommissionKM
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDCommissionKM] " _
            & "    ,[OrganisationID] " _
            & "    ,[Organisation] " _
            & "    ,[KMFrom] " _
            & "    ,[KMTo] " _
            & "    ,[Montant] " _
            & "    ,[ExtraChargeParKM] " _
            & "    ,[PerKMNumber] " _
            & "    ,[MetodeCalculation] " _
            & "    ,[HeureDepart] " _
            & "    ,[HeureFin] " _
            & "FROM " _
            & "     [CommissionKM] " _
            & "WHERE " _
            & "     [IDCommissionKM] = @IDCommissionKM " _
            & ""
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        selectCommand.Parameters.AddWithValue("@IDCommissionKM", clsCommissionKMPara.IDCommissionKM)
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader(CommandBehavior.SingleRow)
            If reader.Read Then
                With clsCommissionKM
                    .IDCommissionKM = System.Convert.ToInt32(reader("IDCommissionKM"))
                    .OrganisationID = If(IsDBNull(reader("OrganisationID")), Nothing, CType(reader("OrganisationID"), Int32?))
                    .Organisation = If(IsDBNull(reader("Organisation")), Nothing, reader("Organisation").ToString)
                    .KMFrom = If(IsDBNull(reader("KMFrom")), Nothing, CType(CType(reader("KMFrom"), Double?), Decimal?))
                    .KMTo = If(IsDBNull(reader("KMTo")), Nothing, CType(CType(reader("KMTo"), Double?), Decimal?))
                    .Montant = If(IsDBNull(reader("Montant")), Nothing, CType(reader("Montant"), Decimal?))
                    .ExtraChargeParKM = If(IsDBNull(reader("ExtraChargeParKM")), Nothing, CType(reader("ExtraChargeParKM"), Decimal?))
                    .PerKMNumber = If(IsDBNull(reader("PerKMNumber")), Nothing, CType(reader("PerKMNumber"), Decimal?))
                    .MetodeCalculation = If(IsDBNull(reader("MetodeCalculation")), Nothing, CType(reader("MetodeCalculation"), Int32?))
                    .HeureDepart = If(IsDBNull(reader("HeureDepart")), Nothing, CType(reader("HeureDepart"), DateTime?))
                    .HeureFin = If(IsDBNull(reader("HeureFin")), Nothing, CType(reader("HeureFin"), DateTime?))
                End With
            Else
                clsCommissionKM = Nothing
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return clsCommissionKM
    End Function

    Public Function Add(ByVal clsCommissionKM As CommissionKM) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim insertStatement As String _
            = "INSERT " _
            & "     [CommissionKM] " _
            & "     ( " _
            & "     [OrganisationID] " _
            & "    ,[Organisation] " _
            & "    ,[KMFrom] " _
            & "    ,[KMTo] " _
            & "    ,[Montant] " _
            & "    ,[ExtraChargeParKM] " _
            & "    ,[PerKMNumber] " _
            & "    ,[MetodeCalculation] " _
            & "    ,[HeureDepart] " _
            & "    ,[HeureFin] " _
            & "     ) " _
            & "VALUES " _
            & "     ( " _
            & "     @OrganisationID " _
            & "    ,@Organisation " _
            & "    ,@KMFrom " _
            & "    ,@KMTo " _
            & "    ,@Montant " _
            & "    ,@ExtraChargeParKM " _
            & "    ,@PerKMNumber " _
            & "    ,@MetodeCalculation " _
            & "    ,@HeureDepart " _
            & "    ,@HeureFin " _
            & "     ) " _
            & ""
        Dim insertCommand As New SqlCommand(insertStatement, connection)
        insertCommand.CommandType = CommandType.Text
        insertCommand.Parameters.AddWithValue("@OrganisationID", IIf(clsCommissionKM.OrganisationID.HasValue, clsCommissionKM.OrganisationID, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Organisation", IIf(Not IsNothing(clsCommissionKM.Organisation), clsCommissionKM.Organisation, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@KMFrom", IIf(clsCommissionKM.KMFrom.HasValue, clsCommissionKM.KMFrom, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@KMTo", IIf(clsCommissionKM.KMTo.HasValue, clsCommissionKM.KMTo, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Montant", IIf(clsCommissionKM.Montant.HasValue, clsCommissionKM.Montant, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@ExtraChargeParKM", IIf(clsCommissionKM.ExtraChargeParKM.HasValue, clsCommissionKM.ExtraChargeParKM, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@PerKMNumber", IIf(clsCommissionKM.PerKMNumber.HasValue, clsCommissionKM.PerKMNumber, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@MetodeCalculation", IIf(clsCommissionKM.MetodeCalculation.HasValue, clsCommissionKM.MetodeCalculation, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@HeureDepart", IIf(clsCommissionKM.HeureDepart.HasValue, clsCommissionKM.HeureDepart, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@HeureFin", IIf(clsCommissionKM.HeureFin.HasValue, clsCommissionKM.HeureFin, DBNull.Value))
        Try
            connection.Open()
            Dim count As Integer = insertCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

    Public Function Update(ByVal clsCommissionKMOld As CommissionKM,
           ByVal clsCommissionKMNew As CommissionKM) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim updateStatement As String _
            = "UPDATE " _
            & "     [CommissionKM] " _
            & "SET " _
            & "     [OrganisationID] = @NewOrganisationID " _
            & "    ,[Organisation] = @NewOrganisation " _
            & "    ,[KMFrom] = @NewKMFrom " _
            & "    ,[KMTo] = @NewKMTo " _
            & "    ,[Montant] = @NewMontant " _
            & "    ,[ExtraChargeParKM] = @NewExtraChargeParKM " _
            & "    ,[PerKMNumber] = @NewPerKMNumber " _
            & "    ,[MetodeCalculation] = @NewMetodeCalculation " _
            & "    ,[HeureDepart] = @NewHeureDepart " _
            & "    ,[HeureFin] = @NewHeureFin " _
            & "WHERE " _
            & "     [IDCommissionKM] = @OldIDCommissionKM " _
            & ""
        Dim updateCommand As New SqlCommand(updateStatement, connection)
        updateCommand.CommandType = CommandType.Text
        updateCommand.Parameters.AddWithValue("@NewOrganisationID", IIf(clsCommissionKMNew.OrganisationID.HasValue, clsCommissionKMNew.OrganisationID, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewOrganisation", IIf(Not IsNothing(clsCommissionKMNew.Organisation), clsCommissionKMNew.Organisation, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewKMFrom", IIf(clsCommissionKMNew.KMFrom.HasValue, clsCommissionKMNew.KMFrom, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewKMTo", IIf(clsCommissionKMNew.KMTo.HasValue, clsCommissionKMNew.KMTo, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewMontant", IIf(clsCommissionKMNew.Montant.HasValue, clsCommissionKMNew.Montant, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewExtraChargeParKM", IIf(clsCommissionKMNew.ExtraChargeParKM.HasValue, clsCommissionKMNew.ExtraChargeParKM, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewPerKMNumber", IIf(clsCommissionKMNew.PerKMNumber.HasValue, clsCommissionKMNew.PerKMNumber, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewMetodeCalculation", IIf(clsCommissionKMNew.MetodeCalculation.HasValue, clsCommissionKMNew.MetodeCalculation, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewHeureDepart", IIf(clsCommissionKMNew.HeureDepart.HasValue, clsCommissionKMNew.HeureDepart, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewHeureFin", IIf(clsCommissionKMNew.HeureFin.HasValue, clsCommissionKMNew.HeureFin, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@OldIDCommissionKM", clsCommissionKMOld.IDCommissionKM)

        Try
            connection.Open()
            Dim count As Integer = updateCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

    Public Function Delete(ByVal clsCommissionKM As CommissionKM) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim deleteStatement As String _
            = "DELETE FROM " _
            & "     [CommissionKM] " _
            & "WHERE " _
            & "     [IDCommissionKM] = @OldIDCommissionKM " _
            & ""
        Dim deleteCommand As New SqlCommand(deleteStatement, connection)
        deleteCommand.CommandType = CommandType.Text
        deleteCommand.Parameters.AddWithValue("@OldIDCommissionKM", clsCommissionKM.IDCommissionKM)

        Try
            connection.Open()
            Dim count As Integer = deleteCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Erreur")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

End Class
 
