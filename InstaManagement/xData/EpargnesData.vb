Imports System.Data.SqlClient

Public Class EpargnesData
    Public Function SelectAllByLivreur(pIDLivreur As Integer) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
             & "     [IDEpargne] " _
            & "    ,[IDLivreur] " _
            & "    ,[Livreur] " _
            & "    ,[DateEpargne] " _
            & "    ,[EpargneParPeriode] " _
            & "    ,[EpargneActif] " _
            & "    ,[Balance] " _
            & "FROM " _
            & "     [Epargnes] " _
            & " WHERE IDLivreur= " & pIDLivreur
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function
    Public Function SelectAll() As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
             & "     [IDEpargne] " _
            & "    ,[IDLivreur] " _
            & "    ,[Livreur] " _
            & "    ,[DateEpargne] " _
            & "    ,[EpargneParPeriode] " _
            & "    ,[EpargneActif] " _
            & "    ,[Balance] " _
            & "FROM " _
            & "     [Epargnes] " _
            & ""
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function

    Public Function Select_Record(ByVal clsEpargnesPara As Epargnes) As Epargnes
        Dim clsEpargnes As New Epargnes
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
              & "     [IDEpargne] " _
            & "    ,[IDLivreur] " _
            & "    ,[Livreur] " _
            & "    ,[DateEpargne] " _
            & "    ,[EpargneParPeriode] " _
            & "    ,[EpargneActif] " _
            & "    ,[Balance] " _
            & "FROM " _
            & "     [Epargnes] " _
            & "WHERE " _
            & "     [IDEpargne] = @IDEpargne " _
            & ""
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        selectCommand.Parameters.AddWithValue("@IDEpargne", clsEpargnesPara.IDEpargne)
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader(CommandBehavior.SingleRow)
            If reader.Read Then
                With clsEpargnes
                    .IDEpargne = System.Convert.ToInt32(reader("IDEpargne"))
                    .IDLivreur = If(IsDBNull(reader("IDLivreur")), Nothing, CType(reader("IDLivreur"), Int32?))
                    .Livreur = If(IsDBNull(reader("Livreur")), Nothing, reader("Livreur").ToString)
                    .DateEpargne = If(IsDBNull(reader("DateEpargne")), Nothing, CType(reader("DateEpargne"), DateTime?))
                    .EpargneParPeriode = If(IsDBNull(reader("EpargneParPeriode")), Nothing, CType(reader("EpargneParPeriode"), Decimal?))
                    .EpargneActif = If(IsDBNull(reader("EpargneActif")), Nothing, CType(reader("EpargneActif"), Boolean?))
                    .Balance = If(IsDBNull(reader("Balance")), Nothing, CType(reader("Balance"), Decimal?))
                End With
            Else
                clsEpargnes = Nothing
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return clsEpargnes
    End Function

    Public Function Add(ByVal clsEpargnes As Epargnes) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim insertStatement As String _
            = "INSERT " _
            & "     [Epargnes] " _
            & "     ( " _
            & "     [IDLivreur] " _
            & "    ,[Livreur] " _
            & "    ,[DateEpargne] " _
            & "    ,[EpargneParPeriode] " _
            & "    ,[EpargneActif] " _
            & "    ,[Balance] " _
            & "     ) " _
            & "VALUES " _
            & "     ( " _
            & "     @IDLivreur " _
            & "    ,@Livreur " _
            & "    ,@DateEpargne " _
            & "    ,@EpargneParPeriode " _
            & "    ,@EpargneActif " _
            & "    ,@Balance " _
            & "     ) " _
            & ""
        Dim insertCommand As New SqlCommand(insertStatement, connection)
        insertCommand.CommandType = CommandType.Text
        insertCommand.Parameters.AddWithValue("@IDLivreur", IIf(clsEpargnes.IDLivreur.HasValue, clsEpargnes.IDLivreur, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Livreur", IIf(Not IsNothing(clsEpargnes.Livreur), clsEpargnes.Livreur, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@DateEpargne", IIf(clsEpargnes.DateEpargne.HasValue, clsEpargnes.DateEpargne, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@EpargneParPeriode", IIf(clsEpargnes.EpargneParPeriode.HasValue, clsEpargnes.EpargneParPeriode, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@EpargneActif", IIf(clsEpargnes.EpargneActif.HasValue, clsEpargnes.EpargneActif, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Balance", IIf(clsEpargnes.Balance.HasValue, clsEpargnes.Balance, DBNull.Value))
        Try
            connection.Open()
            Dim count As Integer = insertCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

    Public Function Update(ByVal clsEpargnesOld As Epargnes,
           ByVal clsEpargnesNew As Epargnes) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim updateStatement As String _
            = "UPDATE " _
            & "     [Epargnes] " _
            & "SET " _
            & "     [IDLivreur] = @NewIDLivreur " _
            & "    ,[Livreur] = @NewLivreur " _
            & "    ,[DateEpargne] = @NewDateEpargne " _
            & "    ,[EpargneParPeriode] = @NewEpargneParPeriode " _
            & "    ,[EpargneActif] = @NewEpargneActif " _
            & "    ,[Balance] = @NewBalance " _
            & "WHERE " _
            & "     [IDEpargne] = @OldIDEpargne " _
            & ""
        Dim updateCommand As New SqlCommand(updateStatement, connection)
        updateCommand.CommandType = CommandType.Text
        updateCommand.Parameters.AddWithValue("@NewIDLivreur", IIf(clsEpargnesNew.IDLivreur.HasValue, clsEpargnesNew.IDLivreur, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewLivreur", IIf(Not IsNothing(clsEpargnesNew.Livreur), clsEpargnesNew.Livreur, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewDateEpargne", IIf(clsEpargnesNew.DateEpargne.HasValue, clsEpargnesNew.DateEpargne, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewEpargneParPeriode", IIf(clsEpargnesNew.EpargneParPeriode.HasValue, clsEpargnesNew.EpargneParPeriode, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewEpargneActif", IIf(clsEpargnesNew.EpargneActif.HasValue, clsEpargnesNew.EpargneActif, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewBalance", IIf(clsEpargnesNew.Balance.HasValue, clsEpargnesNew.Balance, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@OldIDEpargne", clsEpargnesOld.IDEpargne)

        Try
            connection.Open()
            Dim count As Integer = updateCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function


    Public Function UpdateBalance(ByVal clsEpargnesOld As Epargnes,
           ByVal clsEpargnesNew As Epargnes) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim updateStatement As String _
            = "UPDATE " _
            & "     [Epargnes] " _
            & "SET " _
            & "    [Balance] = @NewBalance " _
            & "WHERE " _
            & "     [IDEpargne] = @OldIDEpargne " _
            & ""
        Dim updateCommand As New SqlCommand(updateStatement, connection)
        updateCommand.CommandType = CommandType.Text

        updateCommand.Parameters.AddWithValue("@NewBalance", IIf(clsEpargnesNew.Balance.HasValue, clsEpargnesNew.Balance, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@OldIDEpargne", clsEpargnesOld.IDEpargne)

        Try
            connection.Open()
            Dim count As Integer = updateCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

    Public Function Delete(ByVal clsEpargnes As Epargnes) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim deleteStatement As String _
            = "DELETE FROM " _
            & "     [Epargnes] " _
            & "WHERE " _
            & "     [IDEpargne] = @OldIDEpargne " _
            & ""
        Dim deleteCommand As New SqlCommand(deleteStatement, connection)
        deleteCommand.CommandType = CommandType.Text
        deleteCommand.Parameters.AddWithValue("@OldIDEpargne", clsEpargnes.IDEpargne)

        Try
            connection.Open()
            Dim count As Integer = deleteCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

End Class
 
