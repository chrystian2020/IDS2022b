Imports System.Data.SqlClient

Public Class PretsData
    Public Function SelectAllByLivreur(pIDLivreur As Integer) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDPret] " _
            & "    ,[IDLivreur] " _
            & "    ,[Livreur] " _
            & "    ,[DatePret] " _
            & "    ,[MontantPret] " _
            & "    ,[BalancePret] " _
            & "    ,[NombreSemaine] " _
            & "    ,[PaiementParPeriode] " _
            & "FROM " _
            & "     [Prets] " _
            & " WHERE IDLivreur= " & pIDLivreur
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function

    Public Function SelectAll() As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDPret] " _
            & "    ,[IDLivreur] " _
            & "    ,[Livreur] " _
            & "    ,[DatePret] " _
            & "    ,[MontantPret] " _
            & "    ,[BalancePret] " _
            & "    ,[NombreSemaine] " _
            & "    ,[PaiementParPeriode] " _
            & "FROM " _
            & "     [Prets] " _
            & ""
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function

    Public Function Select_Record(ByVal clsPretsPara As Prets) As Prets
        Dim clsPrets As New Prets
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDPret] " _
            & "    ,[IDLivreur] " _
            & "    ,[Livreur] " _
            & "    ,[DatePret] " _
            & "    ,[MontantPret] " _
            & "    ,[BalancePret] " _
            & "    ,[NombreSemaine] " _
            & "    ,[PaiementParPeriode] " _
            & "FROM " _
            & "     [Prets] " _
            & "WHERE " _
            & "     [IDPret] = @IDPret " _
            & ""
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        selectCommand.Parameters.AddWithValue("@IDPret", clsPretsPara.IDPret)
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader(CommandBehavior.SingleRow)
            If reader.Read Then
                With clsPrets
                    .IDPret = System.Convert.ToInt32(reader("IDPret"))
                    .IDLivreur = If(IsDBNull(reader("IDLivreur")), Nothing, CType(reader("IDLivreur"), Int32?))
                    .Livreur = If(IsDBNull(reader("Livreur")), Nothing, reader("Livreur").ToString)
                    .DatePret = If(IsDBNull(reader("DatePret")), Nothing, CType(reader("DatePret"), DateTime?))
                    .MontantPret = If(IsDBNull(reader("MontantPret")), Nothing, CType(reader("MontantPret"), Decimal?))
                    .BalancePret = If(IsDBNull(reader("BalancePret")), Nothing, CType(reader("BalancePret"), Decimal?))
                    .NombreSemaine = If(IsDBNull(reader("NombreSemaine")), Nothing, CType(reader("NombreSemaine"), Int32?))
                    .PaiementParPeriode = If(IsDBNull(reader("PaiementParPeriode")), Nothing, CType(reader("PaiementParPeriode"), Decimal?))
                End With
            Else
                clsPrets = Nothing
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return clsPrets
    End Function

    Public Function Add(ByVal clsPrets As Prets) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim insertStatement As String _
            = "INSERT " _
            & "     [Prets] " _
            & "     ( " _
            & "     [IDLivreur] " _
            & "    ,[Livreur] " _
            & "    ,[DatePret] " _
            & "    ,[MontantPret] " _
            & "    ,[BalancePret] " _
            & "    ,[NombreSemaine] " _
            & "    ,[PaiementParPeriode] " _
            & "     ) " _
            & "VALUES " _
            & "     ( " _
            & "     @IDLivreur " _
            & "    ,@Livreur " _
            & "    ,@DatePret " _
            & "    ,@MontantPret " _
            & "    ,@BalancePret " _
            & "    ,@NombreSemaine " _
            & "    ,@PaiementParPeriode " _
            & "     ) " _
            & ""
        Dim insertCommand As New SqlCommand(insertStatement, connection)
        insertCommand.CommandType = CommandType.Text
        insertCommand.Parameters.AddWithValue("@IDLivreur", IIf(clsPrets.IDLivreur.HasValue, clsPrets.IDLivreur, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Livreur", IIf(Not IsNothing(clsPrets.Livreur), clsPrets.Livreur, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@DatePret", IIf(clsPrets.DatePret.HasValue, clsPrets.DatePret, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@MontantPret", IIf(clsPrets.MontantPret.HasValue, clsPrets.MontantPret, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@BalancePret", IIf(clsPrets.BalancePret.HasValue, clsPrets.BalancePret, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@NombreSemaine", IIf(clsPrets.NombreSemaine.HasValue, clsPrets.NombreSemaine, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@PaiementParPeriode", IIf(clsPrets.PaiementParPeriode.HasValue, clsPrets.PaiementParPeriode, DBNull.Value))
        Try
            connection.Open()
            Dim count As Integer = insertCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function
    Public Function UpdateBalance(ByVal clsPretsOld As Prets,
           ByVal clsPretsNew As Prets) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim updateStatement As String _
            = "UPDATE " _
            & "     [Prets] " _
            & "SET " _
            & "    [BalancePret] = @NewBalancePret " _
            & "WHERE " _
            & "     [IDPret] = @OldIDPret " _
            & ""
        Dim updateCommand As New SqlCommand(updateStatement, connection)
        updateCommand.CommandType = CommandType.Text
        updateCommand.Parameters.AddWithValue("@NewBalancePret", IIf(clsPretsNew.BalancePret.HasValue, clsPretsNew.BalancePret, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@OldIDPret", clsPretsOld.IDPret)

        Try
            connection.Open()
            Dim count As Integer = updateCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function
    Public Function Update(ByVal clsPretsOld As Prets,
           ByVal clsPretsNew As Prets) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim updateStatement As String _
            = "UPDATE " _
            & "     [Prets] " _
            & "SET " _
            & "     [IDLivreur] = @NewIDLivreur " _
            & "    ,[Livreur] = @NewLivreur " _
            & "    ,[DatePret] = @NewDatePret " _
            & "    ,[MontantPret] = @NewMontantPret " _
            & "    ,[BalancePret] = @NewBalancePret " _
            & "    ,[NombreSemaine] = @NewNombreSemaine " _
            & "    ,[PaiementParPeriode] = @NewPaiementParPeriode " _
            & "WHERE " _
            & "     [IDPret] = @OldIDPret " _
            & ""
        Dim updateCommand As New SqlCommand(updateStatement, connection)
        updateCommand.CommandType = CommandType.Text
        updateCommand.Parameters.AddWithValue("@NewIDLivreur", IIf(clsPretsNew.IDLivreur.HasValue, clsPretsNew.IDLivreur, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewLivreur", IIf(Not IsNothing(clsPretsNew.Livreur), clsPretsNew.Livreur, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewDatePret", IIf(clsPretsNew.DatePret.HasValue, clsPretsNew.DatePret, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewMontantPret", IIf(clsPretsNew.MontantPret.HasValue, clsPretsNew.MontantPret, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewBalancePret", IIf(clsPretsNew.BalancePret.HasValue, clsPretsNew.BalancePret, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewNombreSemaine", IIf(clsPretsNew.NombreSemaine.HasValue, clsPretsNew.NombreSemaine, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewPaiementParPeriode", IIf(clsPretsNew.PaiementParPeriode.HasValue, clsPretsNew.PaiementParPeriode, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@OldIDPret", clsPretsOld.IDPret)

        Try
            connection.Open()
            Dim count As Integer = updateCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

    Public Function Delete(ByVal clsPrets As Prets) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim deleteStatement As String _
            = "DELETE FROM " _
            & "     [Prets] " _
            & "WHERE " _
            & "     [IDPret] = @OldIDPret " _
            & ""
        Dim deleteCommand As New SqlCommand(deleteStatement, connection)
        deleteCommand.CommandType = CommandType.Text
        deleteCommand.Parameters.AddWithValue("@OldIDPret", clsPrets.IDPret)

        Try
            connection.Open()
            Dim count As Integer = deleteCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

End Class
 
