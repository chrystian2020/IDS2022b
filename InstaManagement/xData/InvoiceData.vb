Imports System.Data.SqlClient

Public Class InvoiceData

    Public Function GetTotalTVQIDS(pdDateFrom As DateTime, pdDateTo As DateTime) As Double
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT SUM(TVQ) AS TotalTVQ" _
            & " FROM " _
            & "     [Invoice] " _
            & " WHERe (DateFrom  >= '" & pdDateFrom & "' AND DateTo <= '" & pdDateTo & "')"


        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Dim dr As DataRow
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
                If dt.Rows.Count > 0 Then
                    For Each dr In dt.Rows
                        GetTotalTVQIDS = If(IsDBNull(dr("TotalTVQ")), 0, CType(dr("TotalTVQ"), Decimal?))
                    Next

                End If
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return GetTotalTVQIDS

    End Function


    Public Function GetTotalTPSIDS(pdDateFrom As DateTime, pdDateTo As DateTime) As Double
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT SUM(TPS) AS TotalTPS" _
            & " FROM " _
            & "     [Invoice] " _
            & " WHERe (DateFrom  >= '" & pdDateFrom & "' AND DateTo <= '" & pdDateTo & "')"


        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Dim dr As DataRow
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
                If dt.Rows.Count > 0 Then
                    For Each dr In dt.Rows
                        GetTotalTPSIDS = If(IsDBNull(dr("TotalTPS")), 0, CType(dr("TotalTPS"), Decimal?))
                    Next

                End If
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return GetTotalTPSIDS

    End Function
    Public Function GetChiffreAffaireIDS(pdDateFrom As DateTime, pdDateTo As DateTime) As Double
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT SUM(amount) AS ChiffreAffaire" _
            & " FROM " _
            & "     [Invoice] " _
            & " WHERe (DateFrom  >= '" & pdDateFrom & "' AND DateTo <= '" & pdDateTo & "')"


        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Dim dr As DataRow
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
                If dt.Rows.Count > 0 Then
                    For Each dr In dt.Rows
                        GetChiffreAffaireIDS = If(IsDBNull(dr("ChiffreAffaire")), 0, CType(dr("ChiffreAffaire"), Decimal?))
                    Next


                End If
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return GetChiffreAffaireIDS
    End Function

    Public Function SelectDISTINCTAllByDate(pdDateFrom As DateTime, pdDateTo As DateTime) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT DISTINCT" _
            & "    [OrganisationID] " _
            & "    ,[Organisation] " _
            & " FROM " _
            & "     [Invoice] " _
            & " WHERe (DateFrom  >= '" & pdDateFrom & "' AND DateTo <= '" & pdDateTo & "')"


        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function

    Public Function SelectAllInvoicesParOrganisation(pdDateFrom As DateTime, pdDateTo As DateTime) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT DISTINCT dbo.organisations.organisationid " &
            " FROM   dbo.invoice " &
            " LEFT OUTER JOIN dbo.organisations " &
            " ON dbo.invoice.organisationid = " &
            " dbo.organisations.organisationid " &
            " WHERE (dbo.invoice.DateFrom  >= '" & pdDateFrom & "' AND dbo.invoice.DateTo <= '" & pdDateTo & "') AND dbo.organisations.organisationid IS NOT NULL"

        'If go_Globals.IDPeriode > 0 Then
        '    selectStatement = selectStatement & " AND dbo.invoice.IDPeriode = " & go_Globals.IDPeriode
        'End If



        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function

    Public Function SelectAllInvoicesParOrganisationVendeurs(pdDateFrom As DateTime, pdDateTo As DateTime, pIDVendeur As Integer) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT DISTINCT dbo.organisations.organisationid " &
            " FROM   dbo.invoice " &
            " LEFT OUTER JOIN dbo.organisations " &
            " ON dbo.invoice.organisationid = " &
            " dbo.organisations.organisationid " &
            " WHERE   (dbo.invoice.DateFrom  >= '" & pdDateFrom & "' AND dbo.invoice.DateTo <= '" & pdDateTo & "') AND dbo.invoice.Total > 0"


        selectStatement = selectStatement & " AND  dbo.organisations.IsEtatDeCompte = 1 AND  dbo.organisations.IDVendeur IS NOT NULL AND dbo.organisations.IDVendeur=" & pIDVendeur
        selectStatement = selectStatement & " ORDER  BY dbo.organisations.organisationid "



        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function



    Public Function SelectAllInvoicesParOrganisation(pdDateFrom As DateTime, pdDateTo As DateTime, pEmail As String) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT DISTINCT dbo.organisations.organisationid " &
            " FROM   dbo.invoice " &
            " LEFT OUTER JOIN dbo.organisations " &
            " ON dbo.invoice.organisationid = " &
            " dbo.organisations.organisationid " &
            " WHERE  dbo.organisations.email = '" & pEmail & "'" &
            " AND (dbo.invoice.DateFrom  >= '" & pdDateFrom & "' AND dbo.invoice.DateTo <= '" & pdDateTo & "') AND dbo.invoice.Paye = 0"

        selectStatement = selectStatement & " ORDER  BY dbo.organisations.organisationid "



        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function



    Public Function SelectAllByPeriodOrderByOrganisation(pdDateFrom As DateTime, pdDateTo As DateTime, pOrganisationID As Integer) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT dbo.invoice.idinvoice, " &
            "       dbo.invoice.invoicedate, " &
            "       dbo.invoice.invoicenumber, " &
            "       dbo.invoice.organisationid, " &
            "       dbo.invoice.datefrom, " &
            "       dbo.invoice.dateto, " &
            "       dbo.invoice.idperiode, " &
            "       dbo.invoice.amount, " &
            "       dbo.invoice.tps, " &
            "       dbo.invoice.tvq, " &
            "       dbo.invoice.total, " &
            "       dbo.organisations.organisation, " &
            "       dbo.organisations.adresse, " &
            "       dbo.organisations.telephone, " &
            "       dbo.organisations.email, " &
            "       dbo.organisations.idvendeur, " &
            "       dbo.livreurs.livreur as Vendeur, " &
            "       dbo.livreurs.telephone AS VendeurTelephone, " &
            "       dbo.livreurs.Adresse AS Addresse, " &
            "       dbo.livreurs.Email AS courriel " &
            " FROM  dbo.livreurs " &
            "       RIGHT OUTER JOIN dbo.organisations " &
            "       ON dbo.livreurs.LivreurID = dbo.organisations.idvendeur " &
            "       RIGHT OUTER JOIN dbo.invoice " &
            "       ON dbo.organisations.organisationid = " &
            "       dbo.invoice.organisationid " &
            "  WHERE (dbo.invoice.DateFrom  >= '" & pdDateFrom & "' AND dbo.invoice.DateTo <= '" & pdDateTo & "') AND dbo.invoice.organisationid =" & pOrganisationID

        'If go_Globals.IDPeriode > 0 Then
        '    selectStatement = selectStatement & " AND dbo.invoice.IDPeriode = " & go_Globals.IDPeriode
        'End If

        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function

    Public Function SelectAllByDateSumOrganisation(pdDateFrom As DateTime, pdDateTo As DateTime, pOrganisationID As Integer) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "    SUM(Total) as TotalAmount " _
            & "    ,SUM(TPS) as TotalTPS" _
            & "    ,SUM(TVQ) as TotalTVQ " _
            & "    ,OrganisationID " _
            & "    ,Organisation " _
            & "    ,InvoiceDate " _
            & "    ,DateFrom " _
            & "    ,DateTo " _
            & " FROM " _
            & "     [Invoice] " _
            & " WHERE (InvoiceDate  >= '" & pdDateFrom & "' AND InvoiceDate <= '" & pdDateTo & "') AND OrganisationID=" & pOrganisationID



        'If go_Globals.IDPeriode > 0 Then
        '    selectStatement = selectStatement & " And IDPeriode = " & go_Globals.IDPeriode
        'End If

        selectStatement = selectStatement & " GROUP BY OrganisationID,Organisation,InvoiceDate,DateFrom,DateTo "
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function
    Public Function SelectAllByDateNoGroup(pdDateFrom As DateTime, pdDateTo As DateTime) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "    SUM(Total) as TotalAmount " _
            & "    ,SUM(TPS) as TotalTPS" _
            & "    ,SUM(TVQ) as TotalTVQ " _
            & "    ,OrganisationID " _
            & "    ,Organisation " _
            & " FROM " _
            & "     [Invoice] " _
            & " WHERE (InvoiceDate  >= '" & pdDateFrom & "' AND InvoiceDate <= '" & pdDateTo & "')"



        'If go_Globals.IDPeriode > 0 Then
        '    selectStatement = selectStatement & " And IDPeriode = " & go_Globals.IDPeriode
        'End If

        selectStatement = selectStatement & " GROUP BY OrganisationID,Organisation "
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function

    Public Function SelectAllByDateSum(pdDateFrom As DateTime, pdDateTo As DateTime) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "    SUM(Total) as TotalAmount " _
            & "    ,SUM(TPS) as TotalTPS" _
            & "    ,SUM(TVQ) as TotalTVQ " _
            & "    ,OrganisationID " _
            & "    ,Organisation " _
            & " FROM " _
            & "     [Invoice] " _
            & " WHERE (InvoiceDate  >= '" & pdDateFrom & "' AND InvoiceDate <= '" & pdDateTo & "')"



        'If go_Globals.IDPeriode > 0 Then
        '    selectStatement = selectStatement & " And IDPeriode = " & go_Globals.IDPeriode
        'End If

        selectStatement = selectStatement & " GROUP BY OrganisationID,Organisation "
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function



    Public Function SelectAllByDate(pdDateFrom As DateTime, pdDateTo As DateTime) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDInvoice] " _
            & "    ,[InvoiceDate] " _
            & "    ,[InvoiceNumber] " _
            & "    ,[OrganisationID] " _
            & "    ,[Organisation] " _
            & "    ,[DateFrom] " _
            & "    ,[DateTo] " _
            & "    ,[IDPeriode] " _
            & "    ,[Amount] " _
            & "    ,[TPS] " _
            & "    ,[TVQ] " _
            & "    ,[Total] " _
            & "FROM " _
            & "     [Invoice] " _
            & " WHERE (DateFrom  >= '" & pdDateFrom & "' AND DateTo <= '" & pdDateTo & "') AND IDPeriode = " & go_Globals.IDPeriode & " ORDER BY OrganisationID, IDPeriode ASC"
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function
    Public Function SelectInvoiceByDateVendeur(pdDateFrom As DateTime, pdDateTo As DateTime, piOrganisationID As Integer) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDInvoice] " _
            & "    ,[InvoiceDate] " _
            & "    ,[InvoiceNumber] " _
            & "    ,[OrganisationID] " _
            & "    ,[Organisation] " _
            & "    ,[DateFrom] " _
            & "    ,[DateTo] " _
            & "    ,[IDPeriode] " _
            & "    ,[Amount] " _
            & "    ,[TPS] " _
            & "    ,[TVQ] " _
            & "    ,[Total] " _
            & "    ,[dbo.Organisations.IsEtatDeCompte] " _
            & "dbo.Invoice RIGHT OUTER JOIN " _
            & "dbo.Organisations ON dbo.Invoice.OrganisationID = dbo.Organisations.OrganisationID " _
            & "     [Invoice] " _
            & " WHERE OrganisationID = " & piOrganisationID & " And (DateFrom  >= '" & pdDateFrom & "' AND DateTo <= '" & pdDateTo & "') AND (dbo.Organisations.IsEtatDeCompte = 1) ORDER BY IDInvoice"

        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function


    Public Function SelectInvoiceByDateINFO(pdDateFrom As DateTime, pdDateTo As DateTime, piOrganisationID As Integer) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDInvoice] " _
            & "    ,[InvoiceDate] " _
            & "    ,[InvoiceNumber] " _
            & "    ,[OrganisationID] " _
            & "    ,[Organisation] " _
            & "    ,[DateFrom] " _
            & "    ,[DateTo] " _
            & "    ,[IDPeriode] " _
            & "    ,[Amount] " _
            & "    ,[TPS] " _
            & "    ,[TVQ] " _
            & "    ,[Total] " _
            & "FROM " _
            & "     [Invoice] " _
            & " WHERE OrganisationID = " & piOrganisationID & " And (DateFrom  >= '" & pdDateFrom & "' AND DateTo <= '" & pdDateTo & "')   ORDER BY IDInvoice"

        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function
    Public Function SelectPaidInvoiceByDate(pdDateFrom As DateTime, pdDateTo As DateTime, piOrganisationID As Integer) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDInvoice] " _
            & "    ,[InvoiceDate] " _
            & "    ,[InvoiceNumber] " _
            & "    ,[OrganisationID] " _
            & "    ,[Organisation] " _
            & "    ,[DateFrom] " _
            & "    ,[DateTo] " _
            & "    ,[IDPeriode] " _
            & "    ,[Amount] " _
            & "    ,[TPS] " _
            & "    ,[TVQ] " _
            & "    ,[Total] " _
            & " FROM " _
            & "     [Invoice] " _
            & " WHERE Total > 0 AND OrganisationID = " & piOrganisationID & " And (DateFrom  >= '" & pdDateFrom & "' AND DateTo <= '" & pdDateTo & "')   ORDER BY IDInvoice"

        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function
    Public Function SelectInvoiceByDate(pdDateFrom As DateTime, pdDateTo As DateTime, piOrganisationID As Integer) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDInvoice] " _
            & "    ,[InvoiceDate] " _
            & "    ,[InvoiceNumber] " _
            & "    ,[OrganisationID] " _
            & "    ,[Organisation] " _
            & "    ,[DateFrom] " _
            & "    ,[DateTo] " _
            & "    ,[IDPeriode] " _
            & "    ,[MontantTotal] " _
            & "    ,[Amount] " _
            & "    ,[TPS] " _
            & "    ,[TVQ] " _
            & "    ,[Total] " _
            & "FROM " _
            & "     [Invoice] " _
            & " WHERE OrganisationID = " & piOrganisationID & " And (DateFrom  >= '" & pdDateFrom & "' AND DateTo <= '" & pdDateTo & "')   ORDER BY IDInvoice"

        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function

    Public Function SelectAllInvoiceNumber(pdDateFrom As DateTime, pdDateTo As DateTime) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "    IDInvoice,InvoiceNumber " _
            & "FROM " _
            & "     Invoice " _
            & " WHERE (DateFrom  >= '" & pdDateFrom & "' AND DateTo <= '" & pdDateTo & "') AND IDPeriode = " & go_Globals.IDPeriode & " ORDER BY IDInvoice"

        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function

    Public Function SelectAllByPeriodOrganisationID(IDPeriod As Integer, pOrganisationID As Integer) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDInvoice] " _
            & "    ,[InvoiceDate] " _
            & "    ,[InvoiceNumber] " _
            & "    ,[OrganisationID] " _
            & "    ,[Organisation] " _
            & "    ,[DateFrom] " _
            & "    ,[DateTo] " _
            & "    ,[IDPeriode] " _
            & "    ,[Amount] " _
            & "    ,[TPS] " _
            & "    ,[TVQ] " _
            & "    ,[Total] " _
            & "FROM " _
            & "     [Invoice] " _
            & " WHERE IDPeriode=" & IDPeriod & " AND OrganisationID=" & pOrganisationID

        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function

    Public Function SelectAllByPeriod(IDPeriod As Integer) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDInvoice] " _
            & "    ,[InvoiceDate] " _
            & "    ,[InvoiceNumber] " _
            & "    ,[OrganisationID] " _
            & "    ,[Organisation] " _
            & "    ,[DateFrom] " _
            & "    ,[DateTo] " _
            & "    ,[IDPeriode] " _
            & "    ,[Amount] " _
            & "    ,[TPS] " _
            & "    ,[TVQ] " _
            & "    ,[Total] " _
            & "    ,[MontantTotal] " _
            & "FROM " _
            & "     [Invoice] " _
            & " WHERE IDPeriode=" & IDPeriod
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function
    Public Function SelectAllByOrganisationID(pdDateFrom As DateTime, pdDateTo As DateTime, pOrganisationID As Integer) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDInvoice] " _
            & "    ,[InvoiceDate] " _
            & "    ,[InvoiceNumber] " _
            & "    ,[OrganisationID] " _
            & "    ,[Organisation] " _
            & "    ,[DateFrom] " _
            & "    ,[DateTo] " _
            & "    ,[IDPeriode] " _
            & "    ,[Amount] " _
            & "    ,[TPS] " _
            & "    ,[TVQ] " _
            & "    ,[Total] " _
            & "FROM " _
            & "     [Invoice] " _
            & " WHERE (InvoiceDate  >= '" & pdDateFrom & "' AND InvoiceDate <= '" & pdDateTo & "') AND OrganisationID =" & pOrganisationID
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function
    Public Function SelectAll() As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDInvoice] " _
            & "    ,[InvoiceDate] " _
            & "    ,[InvoiceNumber] " _
            & "    ,[OrganisationID] " _
            & "    ,[Organisation] " _
            & "    ,[DateFrom] " _
            & "    ,[DateTo] " _
            & "    ,[IDPeriode] " _
            & "    ,[Amount] " _
            & "    ,[TPS] " _
            & "    ,[TVQ] " _
            & "    ,[Total] " _
            & "FROM " _
            & "     [Invoice] " _
            & ""
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function
    Public Function Select_RecordTotal(pdDateFrom As DateTime, pdDateTo As DateTime, pOrganisationID As Integer) As Invoice
        Dim clsInvoice As New Invoice
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "    SUM(Total) as TotalAmount " _
            & "    ,SUM(TPS) as TotalTPS" _
            & "    ,SUM(TVQ) as TotalTVQ " _
            & " FROM " _
            & "     [Invoice] " _
            & " WHERE (InvoiceDate  >= '" & pdDateFrom & "' AND InvoiceDate <= '" & pdDateTo & "') AND OrganisationID=" & pOrganisationID



        'If go_Globals.IDPeriode > 0 Then
        '    selectStatement = selectStatement & " And IDPeriode = " & go_Globals.IDPeriode
        'End If

        selectStatement = selectStatement & " GROUP BY OrganisationID,Organisation,InvoiceDate,DateFrom,DateTo "
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader(CommandBehavior.SingleRow)
            If reader.Read Then
                With clsInvoice
                    '.IDInvoice = System.Convert.ToInt32(reader("IDInvoice"))
                    '.InvoiceDate = If(IsDBNull(reader("InvoiceDate")), Nothing, CType(reader("InvoiceDate"), DateTime?))
                    '.InvoiceNumber = If(IsDBNull(reader("InvoiceNumber")), Nothing, reader("InvoiceNumber").ToString)
                    '.OrganisationID = If(IsDBNull(reader("OrganisationID")), Nothing, CType(reader("OrganisationID"), Int32?))
                    '.Organisation = If(IsDBNull(reader("Organisation")), Nothing, reader("Organisation").ToString)
                    '.DateFrom = If(IsDBNull(reader("DateFrom")), Nothing, CType(reader("DateFrom"), DateTime?))
                    '.DateTo = If(IsDBNull(reader("DateTo")), Nothing, CType(reader("DateTo"), DateTime?))
                    '.IDPeriode = If(IsDBNull(reader("IDPeriode")), Nothing, CType(reader("IDPeriode"), Int32?))
                    '.Amount = If(IsDBNull(reader("TotalAmount")), 0, CType(reader("TotalAmount"), Decimal?))
                    .TPS = If(IsDBNull(reader("TotalTPS")), 0, CType(reader("TotalTPS"), Decimal?))
                    .TVQ = If(IsDBNull(reader("TotalTVQ")), 0, CType(reader("TotalTVQ"), Decimal?))
                    .Total = If(IsDBNull(reader("TotalAmount")), Nothing, CType(reader("TotalAmount"), Decimal?))
                End With
            Else
                clsInvoice = Nothing
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return clsInvoice
    End Function
    Public Function Select_Record(ByVal clsInvoicePara As Invoice) As Invoice
        Dim clsInvoice As New Invoice
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDInvoice] " _
            & "    ,[InvoiceDate] " _
            & "    ,[InvoiceNumber] " _
            & "    ,[OrganisationID] " _
            & "    ,[Organisation] " _
            & "    ,[DateFrom] " _
            & "    ,[DateTo] " _
            & "    ,[IDPeriode] " _
            & "    ,[Amount] " _
            & "    ,[TPS] " _
            & "    ,[TVQ] " _
            & "    ,[Total] " _
            & "FROM " _
            & "     [Invoice] " _
            & "WHERE " _
            & "     [IDInvoice] = @IDInvoice " _
            & ""
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        selectCommand.Parameters.AddWithValue("@IDInvoice", clsInvoicePara.IDInvoice)
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader(CommandBehavior.SingleRow)
            If reader.Read Then
                With clsInvoice
                    .IDInvoice = System.Convert.ToInt32(reader("IDInvoice"))
                    .InvoiceDate = If(IsDBNull(reader("InvoiceDate")), Nothing, CType(reader("InvoiceDate"), DateTime?))
                    .InvoiceNumber = If(IsDBNull(reader("InvoiceNumber")), Nothing, reader("InvoiceNumber").ToString)
                    .OrganisationID = If(IsDBNull(reader("OrganisationID")), Nothing, CType(reader("OrganisationID"), Int32?))
                    .Organisation = If(IsDBNull(reader("Organisation")), Nothing, reader("Organisation").ToString)
                    .DateFrom = If(IsDBNull(reader("DateFrom")), Nothing, CType(reader("DateFrom"), DateTime?))
                    .DateTo = If(IsDBNull(reader("DateTo")), Nothing, CType(reader("DateTo"), DateTime?))
                    .IDPeriode = If(IsDBNull(reader("IDPeriode")), Nothing, CType(reader("IDPeriode"), Int32?))
                    .Amount = If(IsDBNull(reader("Amount")), 0, CType(reader("Amount"), Decimal?))
                    .TPS = If(IsDBNull(reader("TPS")), 0, CType(reader("TPS"), Decimal?))
                    .TVQ = If(IsDBNull(reader("TVQ")), 0, CType(reader("TVQ"), Decimal?))
                    .Total = If(IsDBNull(reader("Total")), Nothing, CType(reader("Total"), Decimal?))
                End With
            Else
                clsInvoice = Nothing
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return clsInvoice
    End Function


    Public Function Add(ByVal clsInvoice As Invoice) As Integer
        Dim connection As SqlConnection = DB.GetConnection
        Dim insertStatement As String _
            = "INSERT " _
            & "     [Invoice] " _
            & "     ( " _
            & "     [InvoiceDate] " _
            & "    ,[InvoiceNumber] " _
            & "    ,[OrganisationID] " _
            & "    ,[Organisation] " _
            & "    ,[DateFrom] " _
            & "    ,[DateTo] " _
            & "    ,[IDPeriode] " _
            & "    ,[Amount] " _
            & "    ,[TPS] " _
            & "    ,[TVQ] " _
            & "    ,[Paye] " _
            & "     ) " _
            & "VALUES " _
            & "     ( " _
            & "     @InvoiceDate " _
            & "    ,@InvoiceNumber " _
            & "    ,@OrganisationID " _
            & "    ,@Organisation " _
            & "    ,@DateFrom " _
            & "    ,@DateTo " _
            & "    ,@IDPeriode " _
            & "    ,@Amount " _
            & "    ,@TPS " _
            & "    ,@TVQ " _
            & "    ,@Paye " _
            & "     ) ;SELECT SCOPE_IDENTITY()" _
            & ""
        Dim insertCommand As New SqlCommand(insertStatement, connection)
        insertCommand.CommandType = CommandType.Text
        insertCommand.Parameters.AddWithValue("@InvoiceDate", IIf(clsInvoice.InvoiceDate.HasValue, clsInvoice.InvoiceDate, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@InvoiceNumber", IIf(Not IsNothing(clsInvoice.InvoiceNumber), clsInvoice.InvoiceNumber, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@OrganisationID", IIf(clsInvoice.OrganisationID.HasValue, clsInvoice.OrganisationID, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Organisation", IIf(Not IsNothing(clsInvoice.Organisation), clsInvoice.Organisation, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@DateFrom", IIf(clsInvoice.DateFrom.HasValue, clsInvoice.DateFrom, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@DateTo", IIf(clsInvoice.DateTo.HasValue, clsInvoice.DateTo, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@IDPeriode", IIf(clsInvoice.IDPeriode.HasValue, clsInvoice.IDPeriode, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Amount", IIf(clsInvoice.Amount.HasValue, clsInvoice.Amount, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@TPS", IIf(clsInvoice.TPS.HasValue, clsInvoice.TPS, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@TVQ", IIf(clsInvoice.TVQ.HasValue, clsInvoice.TVQ, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Paye", IIf(clsInvoice.Paye.HasValue, clsInvoice.Paye, DBNull.Value))
        Try
            connection.Open()
            Dim count As Integer = insertCommand.ExecuteScalar()
            If count > 0 Then
                Return count
            Else
                Return count
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

    Public Function UpdateInvoiceDate(ByVal clsInvoiceOld As Invoice,
           ByVal clsInvoiceNew As Invoice) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim updateStatement As String _
            = "UPDATE " _
            & "     [Invoice] " _
            & "SET " _
            & "    [InvoiceDate] = @NewInvoiceDate " _
            & "    ,[Paye] = @NewPaye " _
            & "WHERE " _
            & "     [IDInvoice] = @OldIDInvoice " _
            & ""
        Dim updateCommand As New SqlCommand(updateStatement, connection)
        updateCommand.CommandType = CommandType.Text
        updateCommand.Parameters.AddWithValue("@NewInvoiceDate", IIf(clsInvoiceNew.InvoiceDate.HasValue, clsInvoiceNew.InvoiceDate, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewPaye", IIf(clsInvoiceNew.Paye.HasValue, clsInvoiceNew.Paye, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@OldIDInvoice", clsInvoiceOld.IDInvoice)

        Try
            connection.Open()
            Dim count As Integer = updateCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function
    Public Function UpdateAmount(ByVal clsInvoiceOld As Invoice,
           ByVal clsInvoiceNew As Invoice) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim updateStatement As String _
            = "UPDATE " _
            & "     [Invoice] " _
            & "SET " _
            & "    [Amount] = @NewAmount " _
            & "    ,[TPS] = @NewTPS " _
            & "    ,[TVQ] = @NewTVQ " _
            & "    ,[Total] = @NewTotal " _
            & "WHERE " _
            & "     [IDInvoice] = @OldIDInvoice " _
            & ""
        Dim updateCommand As New SqlCommand(updateStatement, connection)
        updateCommand.CommandType = CommandType.Text
        updateCommand.Parameters.AddWithValue("@NewAmount", IIf(clsInvoiceNew.Amount.HasValue, clsInvoiceNew.Amount, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewTPS", IIf(clsInvoiceNew.TPS.HasValue, clsInvoiceNew.TPS, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewTVQ", IIf(clsInvoiceNew.TVQ.HasValue, clsInvoiceNew.TVQ, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewTotal", IIf(clsInvoiceNew.Total.HasValue, clsInvoiceNew.Total, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@OldIDInvoice", clsInvoiceOld.IDInvoice)

        Try
            connection.Open()
            Dim count As Integer = updateCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

    Public Function UpdateTotal(ByVal clsInvoiceOld As Invoice,
           ByVal clsInvoiceNew As Invoice) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim updateStatement As String _
            = "UPDATE " _
            & "     [Invoice] " _
            & " SET " _
            & "    [Total] = @NewTotal " _
            & "    ,[Paye] = @NewPaye " _
            & " WHERE " _
            & "     [IDInvoice] = @OldIDInvoice " _
            & ""
        Dim updateCommand As New SqlCommand(updateStatement, connection)
        updateCommand.CommandType = CommandType.Text
        updateCommand.Parameters.AddWithValue("@NewTotal", IIf(clsInvoiceNew.Total.HasValue, clsInvoiceNew.Total, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewPaye", IIf(clsInvoiceNew.Paye.HasValue, clsInvoiceNew.Paye, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@OldIDInvoice", clsInvoiceOld.IDInvoice)

        Try
            connection.Open()
            Dim count As Integer = updateCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

    Public Function Update(ByVal clsInvoiceOld As Invoice,
           ByVal clsInvoiceNew As Invoice) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim updateStatement As String _
            = "UPDATE " _
            & "     [Invoice] " _
            & "SET " _
            & "     [InvoiceDate] = @NewInvoiceDate " _
            & "    ,[InvoiceNumber] = @NewInvoiceNumber " _
            & "    ,[OrganisationID] = @NewOrganisationID " _
            & "    ,[Organisation] = @NewOrganisation " _
            & "    ,[DateFrom] = @NewDateFrom " _
            & "    ,[DateTo] = @NewDateTo " _
            & "    ,[IDPeriode] = @NewIDPeriode " _
            & "    ,[Amount] = @NewAmount " _
            & "    ,[TPS] = @NewTPS " _
            & "    ,[TVQ] = @NewTVQ " _
            & "WHERE " _
            & "     [IDInvoice] = @OldIDInvoice " _
            & ""
        Dim updateCommand As New SqlCommand(updateStatement, connection)
        updateCommand.CommandType = CommandType.Text
        updateCommand.Parameters.AddWithValue("@NewInvoiceDate", IIf(clsInvoiceNew.InvoiceDate.HasValue, clsInvoiceNew.InvoiceDate, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewInvoiceNumber", IIf(Not IsNothing(clsInvoiceNew.InvoiceNumber), clsInvoiceNew.InvoiceNumber, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewOrganisationID", IIf(clsInvoiceNew.OrganisationID.HasValue, clsInvoiceNew.OrganisationID, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewOrganisation", IIf(Not IsNothing(clsInvoiceNew.Organisation), clsInvoiceNew.Organisation, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewDateFrom", IIf(clsInvoiceNew.DateFrom.HasValue, clsInvoiceNew.DateFrom, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewDateTo", IIf(clsInvoiceNew.DateTo.HasValue, clsInvoiceNew.DateTo, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewIDPeriode", IIf(clsInvoiceNew.IDPeriode.HasValue, clsInvoiceNew.IDPeriode, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewAmount", IIf(clsInvoiceNew.Amount.HasValue, clsInvoiceNew.Amount, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewTPS", IIf(clsInvoiceNew.TPS.HasValue, clsInvoiceNew.TPS, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewTVQ", IIf(clsInvoiceNew.TVQ.HasValue, clsInvoiceNew.TVQ, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@OldIDInvoice", clsInvoiceOld.IDInvoice)

        Try
            connection.Open()
            Dim count As Integer = updateCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

    Public Function DeleteByPeriodOrganisation(ByVal clsInvoice As Invoice) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim deleteStatement As String _
            = "DELETE FROM " _
            & "     [Invoice] " _
            & " WHERE " _
            & "     [IDPeriode] = @OldIDPeriode " _
            & " AND [OrganisationID] = @OldOrganisationID "

        Dim deleteCommand As New SqlCommand(deleteStatement, connection)
        deleteCommand.CommandType = CommandType.Text
        deleteCommand.Parameters.AddWithValue("@OldIDPeriode", clsInvoice.IDPeriode)
        deleteCommand.Parameters.AddWithValue("@OldOrganisationID", clsInvoice.OrganisationID)

        Try
            connection.Open()
            Dim count As Integer = deleteCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

    Public Function DeleteByPeriod(ByVal clsInvoice As Invoice) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim deleteStatement As String _
            = "DELETE FROM " _
            & "     [Invoice] " _
            & "WHERE " _
            & "     [IDPeriode] = @OldIDPeriode "
        Dim deleteCommand As New SqlCommand(deleteStatement, connection)
        deleteCommand.CommandType = CommandType.Text
        deleteCommand.Parameters.AddWithValue("@OldIDPeriode", clsInvoice.IDPeriode)

        Try
            connection.Open()
            Dim count As Integer = deleteCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function
    Public Function Delete(ByVal clsInvoice As Invoice) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim deleteStatement As String _
            = "DELETE FROM " _
            & "     [Invoice] " _
            & "WHERE " _
            & "     [IDInvoice] = @OldIDInvoice "
        Dim deleteCommand As New SqlCommand(deleteStatement, connection)
        deleteCommand.CommandType = CommandType.Text
        deleteCommand.Parameters.AddWithValue("@OldIDInvoice", clsInvoice.IDInvoice)

        Try
            connection.Open()
            Dim count As Integer = deleteCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

    Public Function DeleteByPeriodeO(ByVal clsInvoice As Invoice) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim deleteStatement As String _
            = "DELETE FROM " _
            & "     [Invoice] " _
            & "WHERE " _
            & "     [IDPeriode] = @OldIDPeriode"
        Dim deleteCommand As New SqlCommand(deleteStatement, connection)
        deleteCommand.CommandType = CommandType.Text
        deleteCommand.Parameters.AddWithValue("@OldIDPeriode", clsInvoice.IDPeriode)

        Try
            connection.Open()
            Dim count As Integer = deleteCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

    Public Function DeleteInvoiceByOrganisationDate(ByVal clsInvoice As Invoice) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim deleteStatement As String _
            = "DELETE FROM " _
            & "     [Invoice] " _
            & "WHERE " _
            & "     [IDInvoice] = @OldIDInvoice "
        Dim deleteCommand As New SqlCommand(deleteStatement, connection)
        deleteCommand.CommandType = CommandType.Text
        deleteCommand.Parameters.AddWithValue("@OldIDInvoice", clsInvoice.IDInvoice)

        Try
            connection.Open()
            Dim count As Integer = deleteCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

End Class

