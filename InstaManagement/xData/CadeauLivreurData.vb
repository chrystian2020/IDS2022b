Imports System.Data.SqlClient

Public Class CadeauLivreurData
    Public Function SelectTotalAmount(pdDateFrom As DateTime, pdDateTo As DateTime, piLivreurID As Integer) As Double

        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "    SUM(Montant) as TotalMontant " _
            & "FROM " _
            & "     [CadeauLivreur] " _
            & "WHERE (DateFrom  >= '" & pdDateFrom & "' AND DateTo <= '" & pdDateTo & "') AND IDLivreur='" & piLivreurID & "'"

        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Dim drImport As DataRow
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            Dim dTotalAmount As Double = 0
            If reader.HasRows Then
                dt.Load(reader)
                If dt.Rows.Count > 0 Then
                    For Each drImport In dt.Rows
                        dTotalAmount = dTotalAmount + If(IsDBNull(drImport("TotalMontant")), 0, drImport("TotalMontant"))
                    Next
                    Return dTotalAmount
                Else
                    Return 0
                End If

            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Erreur")
        Finally
            connection.Close()
        End Try

    End Function
    Public Function SelectAll(pdDateFrom As DateTime, pdDateTo As DateTime) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDCadeauLivreur] " _
            & "    ,[IDLivreur] " _
            & "    ,[Livreur] " _
            & "    ,[DateFrom] " _
            & "    ,[DateTo] " _
            & "    ,[Montant] " _
            & "    ,[IDPeriode] " _
            & " FROM " _
            & "     [CadeauLivreur] " _
            & " WHERE (DateFrom  >= '" & pdDateFrom & "' AND DateTo <= '" & pdDateTo & "')"
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function

    Public Function Select_Record(ByVal clsCadeauLivreurPara As CadeauLivreur) As CadeauLivreur
        Dim clsCadeauLivreur As New CadeauLivreur
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDCadeauLivreur] " _
            & "    ,[IDLivreur] " _
            & "    ,[Livreur] " _
            & "    ,[DateFrom] " _
            & "    ,[DateTo] " _
            & "    ,[Montant] " _
            & "    ,[IDPeriode] " _
            & "FROM " _
            & "     [CadeauLivreur] " _
            & "WHERE " _
            & "     [IDCadeauLivreur] = @IDCadeauLivreur " _
            & ""
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        selectCommand.Parameters.AddWithValue("@IDCadeauLivreur", clsCadeauLivreurPara.IDCadeauLivreur)
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader(CommandBehavior.SingleRow)
            If reader.Read Then
                With clsCadeauLivreur
                    .IDCadeauLivreur = System.Convert.ToInt32(reader("IDCadeauLivreur"))
                    .IDLivreur = If(IsDBNull(reader("IDLivreur")), Nothing, CType(reader("IDLivreur"), Int32?))
                    .Livreur = If(IsDBNull(reader("Livreur")), Nothing, reader("Livreur").ToString)
                    .DateFrom = If(IsDBNull(reader("DateFrom")), Nothing, CType(reader("DateFrom"), DateTime?))
                    .DateTo = If(IsDBNull(reader("DateTo")), Nothing, CType(reader("DateTo"), DateTime?))
                    .Montant = If(IsDBNull(reader("Montant")), Nothing, CType(reader("Montant"), Decimal?))
                    .IDPeriode = If(IsDBNull(reader("IDPeriode")), Nothing, CType(reader("IDPeriode"), Int32?))
                End With
            Else
                clsCadeauLivreur = Nothing
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return clsCadeauLivreur
    End Function

    Public Function Add(ByVal clsCadeauLivreur As CadeauLivreur) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim insertStatement As String _
            = "INSERT " _
            & "     [CadeauLivreur] " _
            & "     ( " _
            & "     [IDLivreur] " _
            & "    ,[Livreur] " _
            & "    ,[DateFrom] " _
            & "    ,[DateTo] " _
            & "    ,[Montant] " _
            & "    ,[IDPeriode] " _
            & "     ) " _
            & "VALUES " _
            & "     ( " _
            & "     @IDLivreur " _
            & "    ,@Livreur " _
            & "    ,@DateFrom " _
            & "    ,@DateTo " _
            & "    ,@Montant " _
            & "    ,@IDPeriode " _
            & "     ) " _
            & ""
        Dim insertCommand As New SqlCommand(insertStatement, connection)
        insertCommand.CommandType = CommandType.Text
        insertCommand.Parameters.AddWithValue("@IDLivreur", IIf(clsCadeauLivreur.IDLivreur.HasValue, clsCadeauLivreur.IDLivreur, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Livreur", IIf(Not IsNothing(clsCadeauLivreur.Livreur), clsCadeauLivreur.Livreur, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@DateFrom", IIf(clsCadeauLivreur.DateFrom.HasValue, clsCadeauLivreur.DateFrom, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@DateTo", IIf(clsCadeauLivreur.DateTo.HasValue, clsCadeauLivreur.DateTo, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Montant", IIf(clsCadeauLivreur.Montant.HasValue, clsCadeauLivreur.Montant, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@IDPeriode", IIf(clsCadeauLivreur.IDPeriode.HasValue, clsCadeauLivreur.IDPeriode, DBNull.Value))
        Try
            connection.Open()
            Dim count As Integer = insertCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

    Public Function Update(ByVal clsCadeauLivreurOld As CadeauLivreur,
           ByVal clsCadeauLivreurNew As CadeauLivreur) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim updateStatement As String _
            = "UPDATE " _
            & "     [CadeauLivreur] " _
            & "SET " _
            & "     [IDLivreur] = @NewIDLivreur " _
            & "    ,[Livreur] = @NewLivreur " _
            & "    ,[DateFrom] = @NewDateFrom " _
            & "    ,[DateTo] = @NewDateTo " _
            & "    ,[Montant] = @NewMontant " _
            & "    ,[IDPeriode] = @NewIDPeriode " _
            & "WHERE " _
            & "     [IDCadeauLivreur] = @OldIDCadeauLivreur " _
            & ""
        Dim updateCommand As New SqlCommand(updateStatement, connection)
        updateCommand.CommandType = CommandType.Text
        updateCommand.Parameters.AddWithValue("@NewIDLivreur", IIf(clsCadeauLivreurNew.IDLivreur.HasValue, clsCadeauLivreurNew.IDLivreur, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewLivreur", IIf(Not IsNothing(clsCadeauLivreurNew.Livreur), clsCadeauLivreurNew.Livreur, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewDateFrom", IIf(clsCadeauLivreurNew.DateFrom.HasValue, clsCadeauLivreurNew.DateFrom, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewDateTo", IIf(clsCadeauLivreurNew.DateTo.HasValue, clsCadeauLivreurNew.DateTo, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewMontant", IIf(clsCadeauLivreurNew.Montant.HasValue, clsCadeauLivreurNew.Montant, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewIDPeriode", IIf(clsCadeauLivreurNew.IDPeriode.HasValue, clsCadeauLivreurNew.IDPeriode, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@OldIDCadeauLivreur", clsCadeauLivreurOld.IDCadeauLivreur)

        Try
            connection.Open()
            Dim count As Integer = updateCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

    Public Function Delete(ByVal clsCadeauLivreur As CadeauLivreur) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim deleteStatement As String _
            = "DELETE FROM " _
            & "     [CadeauLivreur] " _
            & "WHERE " _
            & "     [IDCadeauLivreur] = @OldIDCadeauLivreur " _
            & ""
        Dim deleteCommand As New SqlCommand(deleteStatement, connection)
        deleteCommand.CommandType = CommandType.Text
        deleteCommand.Parameters.AddWithValue("@OldIDCadeauLivreur", clsCadeauLivreur.IDCadeauLivreur)
        Try
            connection.Open()
            Dim count As Integer = deleteCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

End Class
 
