Imports System.Data.SqlClient

Public Class ClientAddresseData

    Public Function SelectAll() As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDClientAddress] " _
            & "    ,[Nom] " _
            & "    ,[AdresseNo] " _
            & "    ,[Adresse] " _
            & "    ,[Appartement] " _
            & "    ,[CodeEntree] " _
            & "    ,[CodePostale] " _
            & "    ,[VilleID] " _
            & "    ,[Contact] " _
            & "    ,[Telephone] " _
            & "    ,[Mobile] " _
            & "    ,[Courriel] " _
            & "    ,[Titre] " _
            & "FROM ClientAddresse"


        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function

    Public Function Select_Record(ByVal clsClientAddressePara As ClientAddresse) As ClientAddresse
        Dim clsClientAddresse As New ClientAddresse
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDClientAddress] " _
            & "    ,[Nom] " _
            & "    ,[AdresseNo] " _
            & "    ,[Adresse] " _
            & "    ,[Appartement] " _
            & "    ,[CodeEntree] " _
            & "    ,[CodePostale] " _
            & "    ,[VilleID] " _
            & "    ,[Contact] " _
            & "    ,[Telephone] " _
            & "    ,[Mobile] " _
            & "    ,[Courriel] " _
            & "    ,[Titre] " _
            & "FROM " _
            & "     [ClientAddresse] " _
            & "WHERE " _
            & "    [IDClientAddress] = @IDClientAddress " _
            & ""
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        selectCommand.Parameters.AddWithValue("@IDClientAddress", clsClientAddressePara.IDClientAddress)
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader(CommandBehavior.SingleRow)
            If reader.Read Then
                With clsClientAddresse
                    .IDClientAddress = System.Convert.ToInt32(reader("IDClientAddress"))
                    .Nom = If(IsDBNull(reader("Nom")), Nothing, reader("Nom").ToString)
                    .AdresseNo = If(IsDBNull(reader("AdresseNo")), Nothing, reader("AdresseNo").ToString)
                    .Adresse = If(IsDBNull(reader("Adresse")), Nothing, reader("Adresse").ToString)
                    .Appartement = If(IsDBNull(reader("Appartement")), Nothing, reader("Appartement").ToString)
                    .CodeEntree = If(IsDBNull(reader("CodeEntree")), Nothing, reader("CodeEntree").ToString)
                    .CodePostale = If(IsDBNull(reader("CodePostale")), Nothing, reader("CodePostale").ToString)
                    .VilleID = If(IsDBNull(reader("VilleID")), Nothing, CType(reader("VilleID"), Int32?))
                    .Contact = If(IsDBNull(reader("Contact")), Nothing, reader("Contact").ToString)
                    .Telephone = If(IsDBNull(reader("Telephone")), Nothing, reader("Telephone").ToString)
                    .Mobile = If(IsDBNull(reader("Mobile")), Nothing, reader("Mobile").ToString)
                    .Courriel = If(IsDBNull(reader("Courriel")), Nothing, reader("Courriel").ToString)
                    .Titre = If(IsDBNull(reader("Titre")), Nothing, reader("Titre").ToString)

                End With
            Else
                clsClientAddresse = Nothing
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return clsClientAddresse
    End Function

    Public Function Add(ByVal clsClientAddresse As ClientAddresse) As Integer
        Dim connection As SqlConnection = DB.GetConnection
        Dim insertStatement As String _
            = "INSERT " _
            & "     [ClientAddresse] " _
            & "     ( " _
            & "    [Nom] " _
            & "    ,[AdresseNo] " _
            & "    ,[Adresse] " _
            & "    ,[Appartement] " _
            & "    ,[CodeEntree] " _
            & "    ,[CodePostale] " _
            & "    ,[VilleID] " _
            & "    ,[Contact] " _
            & "    ,[Telephone] " _
            & "    ,[Mobile] " _
            & "    ,[Courriel] " _
            & "    ,[Titre] " _
            & "     ) " _
            & "VALUES " _
            & "     ( " _
            & "    @Nom " _
            & "    ,@AdresseNo " _
            & "    ,@Adresse " _
            & "    ,@Appartement " _
            & "    ,@CodeEntree " _
            & "    ,@CodePostale " _
            & "    ,@VilleID " _
            & "    ,@Contact " _
            & "    ,@Telephone " _
            & "    ,@Mobile " _
            & "    ,@Courriel " _
            & "    ,@Titre " _
            & "     );SELECT SCOPE_IDENTITY()" _
            & ""
        Dim insertCommand As New SqlCommand(insertStatement, connection)
        insertCommand.CommandType = CommandType.Text
        insertCommand.Parameters.AddWithValue("@Nom", IIf(Not IsNothing(clsClientAddresse.Nom), clsClientAddresse.Nom, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@AdresseNo", IIf(Not IsNothing(clsClientAddresse.AdresseNo), clsClientAddresse.AdresseNo, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Adresse", IIf(Not IsNothing(clsClientAddresse.Adresse), clsClientAddresse.Adresse, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Appartement", IIf(Not IsNothing(clsClientAddresse.Appartement), clsClientAddresse.Appartement, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@CodeEntree", IIf(Not IsNothing(clsClientAddresse.CodeEntree), clsClientAddresse.CodeEntree, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@CodePostale", IIf(Not IsNothing(clsClientAddresse.CodePostale), clsClientAddresse.CodePostale, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@VilleID", IIf(clsClientAddresse.VilleID.HasValue, clsClientAddresse.VilleID, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Contact", IIf(Not IsNothing(clsClientAddresse.Contact), clsClientAddresse.Contact, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Telephone", IIf(Not IsNothing(clsClientAddresse.Telephone), clsClientAddresse.Telephone, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Mobile", IIf(Not IsNothing(clsClientAddresse.Mobile), clsClientAddresse.Mobile, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Courriel", IIf(Not IsNothing(clsClientAddresse.Courriel), clsClientAddresse.Courriel, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Titre", IIf(Not IsNothing(clsClientAddresse.Titre), clsClientAddresse.Titre, DBNull.Value))

        Try
            connection.Open()
            Dim count As Integer = insertCommand.ExecuteScalar()
            If count > 0 Then
                Return count
            Else
                Return count
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

    Public Function Update(ByVal clsClientAddresseOld As ClientAddresse,
           ByVal clsClientAddresseNew As ClientAddresse) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim updateStatement As String _
            = "UPDATE " _
            & "     [ClientAddresse] " _
            & "SET " _
            & "    [Nom] = @NewNom " _
            & "    ,[AdresseNo] = @NewAdresseNo " _
            & "    ,[Adresse] = @NewAdresse " _
            & "    ,[Appartement] = @NewAppartement " _
            & "    ,[CodeEntree] = @NewCodeEntree " _
            & "    ,[CodePostale] = @NewCodePostale " _
            & "    ,[VilleID] = @NewVilleID " _
            & "    ,[Contact] = @NewContact " _
            & "    ,[Telephone] = @NewTelephone " _
            & "    ,[Mobile] = @NewMobile " _
            & "    ,[Courriel] = @NewCourriel " _
            & "    ,[Titre] = @NewTitre " _
            & "WHERE " _
            & "     [IDClientAddress] = @OldIDClientAddress " _
            & ""
        Dim updateCommand As New SqlCommand(updateStatement, connection)
        updateCommand.CommandType = CommandType.Text

        updateCommand.Parameters.AddWithValue("@NewNom", IIf(Not IsNothing(clsClientAddresseNew.Nom), clsClientAddresseNew.Nom, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewAdresseNo", IIf(Not IsNothing(clsClientAddresseNew.AdresseNo), clsClientAddresseNew.AdresseNo, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewAdresse", IIf(Not IsNothing(clsClientAddresseNew.Adresse), clsClientAddresseNew.Adresse, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewAppartement", IIf(Not IsNothing(clsClientAddresseNew.Appartement), clsClientAddresseNew.Appartement, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewCodeEntree", IIf(Not IsNothing(clsClientAddresseNew.CodeEntree), clsClientAddresseNew.CodeEntree, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewCodePostale", IIf(Not IsNothing(clsClientAddresseNew.CodePostale), clsClientAddresseNew.CodePostale, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewVilleID", IIf(clsClientAddresseNew.VilleID.HasValue, clsClientAddresseNew.VilleID, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewContact", IIf(Not IsNothing(clsClientAddresseNew.Contact), clsClientAddresseNew.Contact, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewTelephone", IIf(Not IsNothing(clsClientAddresseNew.Telephone), clsClientAddresseNew.Telephone, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewMobile", IIf(Not IsNothing(clsClientAddresseNew.Mobile), clsClientAddresseNew.Mobile, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewCourriel", IIf(Not IsNothing(clsClientAddresseNew.Courriel), clsClientAddresseNew.Courriel, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewTitre", IIf(Not IsNothing(clsClientAddresseNew.Titre), clsClientAddresseNew.Titre, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@OldIDClientAddress", clsClientAddresseOld.IDClientAddress)

        Try
            connection.Open()
            Dim count As Integer = updateCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

    Public Function Delete(ByVal clsClientAddresse As ClientAddresse) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim deleteStatement As String _
            = "DELETE FROM " _
            & "     [ClientAddresse] " _
            & "WHERE " _
            & "     [IDClientAddress] = @OldIDClientAddress "

        Dim deleteCommand As New SqlCommand(deleteStatement, connection)
        deleteCommand.CommandType = CommandType.Text
        deleteCommand.Parameters.AddWithValue("@OldIDClientAddress", clsClientAddresse.IDClientAddress)

        Try
            connection.Open()
            Dim count As Integer = deleteCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

End Class
 
