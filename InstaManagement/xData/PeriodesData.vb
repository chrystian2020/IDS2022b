Imports System.Data.SqlClient

Public Class PeriodesData


    Public Function SelectAllByDate(pdDateFrom As DateTime, pdDateTo As DateTime) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDPeriode] " _
            & "    ,RTRIM(LTRIM([Periode])) as Periode " _
            & "FROM " _
            & "     [Periodes] " _
            & " WHERE (DateFrom  >= '" & pdDateFrom & "' AND DateTo <= '" & pdDateTo & "')"
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function


    Public Function SelectAllByID(piPeriode As Integer) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDPeriode] " _
            & "    ,[Periode] " _
            & "    ,[DateFrom] " _
            & "    ,[DateTo] " _
            & "    ,[YearFrom] " _
            & "    ,[YearTo] " _
            & "FROM " _
            & "     [Periodes] " _
            & " WHERE IDPeriode='" & piPeriode & "'"
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function

    Public Function SelectAllByPeriodes(psPeriode As String) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDPeriode] " _
            & "    ,[Periode] " _
            & "    ,[DateFrom] " _
            & "    ,[DateTo] " _
            & "    ,[YearFrom] " _
            & "    ,[YearTo] " _
            & "    ,[IDTypePeriode] " _
            & "FROM " _
            & "     [Periodes] " _
            & " WHERE IDPeriode='" & psPeriode & "'"
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function
    Public Function SelectAllByPeriode(psPeriode As Integer) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDPeriode] " _
            & "    ,[Periode] " _
            & "    ,[DateFrom] " _
            & "    ,[DateTo] " _
            & "    ,[YearFrom] " _
            & "    ,[YearTo] " _
            & "    ,[IDTypePeriode] " _
            & "FROM " _
            & "     [Periodes] " _
            & " WHERE IDPeriode=" & psPeriode
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function

    Public Function SelectAllPeriode(piPeriode As Integer) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDPeriode] " _
            & "    ,[Periode] " _
            & "    ,[DateFrom] " _
            & "    ,[DateTo] " _
            & "    ,[YearFrom] " _
            & "    ,[YearTo] " _
            & "    ,[IDTypePeriode] " _
            & "FROM " _
            & "     [Periodes] " _
            & " WHERE IDTypePeriode= " & piPeriode _
            & " AND (YearTo = " & go_Globals.Year & ") ORDER BY DateFrom"


        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function

    Public Function SelectAll(piTypePeriode As Integer) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDPeriode] " _
            & "    ,[Periode] " _
            & "    ,[DateFrom] " _
            & "    ,[DateTo] " _
            & "    ,[YearFrom] " _
            & "    ,[YearTo] " _
            & "    ,[IDTypePeriode] " _
            & "FROM " _
            & "     [Periodes] " _
            & " WHERE IDTypePeriode= " & piTypePeriode _
            & " AND (YearTo = " & go_Globals.Year & ") ORDER BY IDPeriode ASC"


        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function

    Public Function Select_Record(ByVal clsPeriodesPara As Periodes) As Periodes
        Dim clsPeriodes As New Periodes
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDPeriode] " _
            & "    ,[Periode] " _
            & "    ,[DateFrom] " _
            & "    ,[DateTo] " _
            & "    ,[YearFrom] " _
            & "    ,[YearTo] " _
            & "    ,[IDTypePeriode] " _
            & "FROM " _
            & "     [Periodes] " _
            & "WHERE " _
            & "     [IDPeriode] = @IDPeriode " _
            & ""
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        selectCommand.Parameters.AddWithValue("@IDPeriode", clsPeriodesPara.IDPeriode)
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader(CommandBehavior.SingleRow)
            If reader.Read Then
                With clsPeriodes
                    .IDPeriode = System.Convert.ToInt32(reader("IDPeriode"))
                    .Periode = If(IsDBNull(reader("Periode")), Nothing, reader("Periode").ToString)
                    .DateFrom = If(IsDBNull(reader("DateFrom")), Nothing, CType(reader("DateFrom"), DateTime?))
                    .DateTo = If(IsDBNull(reader("DateTo")), Nothing, CType(reader("DateTo"), DateTime?))
                    .YearFrom = If(IsDBNull(reader("YearFrom")), Nothing, CType(reader("YearFrom"), Int32?))
                    .YearTo = If(IsDBNull(reader("YearTo")), Nothing, CType(reader("YearTo"), Int32?))
                    .IDTypePeriode = System.Convert.ToInt32(reader("IDTypePeriode"))
                End With
            Else
                clsPeriodes = Nothing
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return clsPeriodes
    End Function

    Public Function Add(ByVal clsPeriodes As Periodes) As Integer
        Dim connection As SqlConnection = DB.GetConnection
        Dim insertStatement As String _
            = "INSERT " _
            & "     [Periodes] " _
            & "     ( " _
            & "     [Periode] " _
            & "    ,[DateFrom] " _
            & "    ,[DateTo] " _
            & "    ,[YearFrom] " _
            & "    ,[YearTo] " _
            & "    ,[IDTypePeriode] " _
            & "     ) " _
            & "VALUES " _
            & "     ( " _
            & "     @Periode " _
            & "    ,@DateFrom " _
            & "    ,@DateTo " _
            & "    ,@YearFrom " _
            & "    ,@YearTo " _
            & "    ,@IDTypePeriode " _
            & "     ) ;SELECT SCOPE_IDENTITY()" _
            & ""
        Dim insertCommand As New SqlCommand(insertStatement, connection)
        insertCommand.CommandType = CommandType.Text
        insertCommand.Parameters.AddWithValue("@Periode", IIf(Not IsNothing(clsPeriodes.Periode), clsPeriodes.Periode, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@DateFrom", IIf(clsPeriodes.DateFrom.HasValue, clsPeriodes.DateFrom, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@DateTo", IIf(clsPeriodes.DateTo.HasValue, clsPeriodes.DateTo, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@YearFrom", IIf(clsPeriodes.YearFrom.HasValue, clsPeriodes.YearFrom, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@YearTo", IIf(clsPeriodes.YearTo.HasValue, clsPeriodes.YearTo, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@IDTypePeriode", IIf(Not IsNothing(clsPeriodes.IDTypePeriode), clsPeriodes.IDTypePeriode, DBNull.Value))
        Try
            connection.Open()
            Dim count As Integer = insertCommand.ExecuteScalar()
            If count > 0 Then
                Return count
            Else
                Return 0
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

    Public Function Update(ByVal clsPeriodesOld As Periodes,
           ByVal clsPeriodesNew As Periodes) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim updateStatement As String _
            = "UPDATE " _
            & "     [Periodes] " _
            & "SET " _
            & "     [Periode] = @NewPeriode " _
            & "    ,[DateFrom] = @NewDateFrom " _
            & "    ,[DateTo] = @NewDateTo " _
            & "    ,[YearFrom] = @NewYearFrom " _
            & "    ,[YearTo] = @NewYearTo " _
            & "    ,[IDTypePeriode] = @NewIDTypePeriode " _
            & "WHERE " _
            & "     [IDPeriode] = @OldIDPeriode " _
            & ""
        Dim updateCommand As New SqlCommand(updateStatement, connection)
        updateCommand.CommandType = CommandType.Text
        updateCommand.Parameters.AddWithValue("@NewPeriode", IIf(Not IsNothing(clsPeriodesNew.Periode), clsPeriodesNew.Periode, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewDateFrom", IIf(clsPeriodesNew.DateFrom.HasValue, clsPeriodesNew.DateFrom, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewDateTo", IIf(clsPeriodesNew.DateTo.HasValue, clsPeriodesNew.DateTo, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewYearFrom", IIf(clsPeriodesNew.YearFrom.HasValue, clsPeriodesNew.YearFrom, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewYearTo", IIf(clsPeriodesNew.YearTo.HasValue, clsPeriodesNew.YearTo, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@OldIDPeriode", clsPeriodesOld.IDPeriode)
        updateCommand.Parameters.AddWithValue("@OldPeriode", IIf(Not IsNothing(clsPeriodesOld.Periode), clsPeriodesOld.Periode, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@OldDateFrom", IIf(clsPeriodesOld.DateFrom.HasValue, clsPeriodesOld.DateFrom, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@OldDateTo", IIf(clsPeriodesOld.DateTo.HasValue, clsPeriodesOld.DateTo, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@OldYearFrom", IIf(clsPeriodesOld.YearFrom.HasValue, clsPeriodesOld.YearFrom, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@OldYearTo", IIf(clsPeriodesOld.YearTo.HasValue, clsPeriodesOld.YearTo, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewIDTypePeriode", IIf(clsPeriodesOld.IDTypePeriode.HasValue, clsPeriodesOld.IDTypePeriode, DBNull.Value))
        Try
            connection.Open()
            Dim count As Integer = updateCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

    Public Function Delete(ByVal clsPeriodes As Periodes) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim deleteStatement As String _
            = "DELETE FROM " _
            & "     [Periodes] " _
            & "WHERE " _
            & "     [IDPeriode] = @OldIDPeriode " _
            & ""
        Dim deleteCommand As New SqlCommand(deleteStatement, connection)
        deleteCommand.CommandType = CommandType.Text
        deleteCommand.Parameters.AddWithValue("@OldIDPeriode", clsPeriodes.IDPeriode)

        Try
            connection.Open()
            Dim count As Integer = deleteCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function


End Class

