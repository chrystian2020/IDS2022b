Imports System.Data.SqlClient

Public Class EpargnesPaiementsData


    Public Function SelectAllByID(pIDEpargne As Integer) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = " SELECT dbo.EpargnesPaiements.IDEpargnePaiment, dbo.EpargnesPaiements.IDEpargne, dbo.EpargnesPaiements.IDLivreur, dbo.EpargnesPaiements.Livreur, dbo.EpargnesPaiements.DateEpargne, dbo.EpargnesPaiements.Epargne, dbo.Periodes.Periode " _
            & " FROM  dbo.EpargnesPaiements LEFT OUTER JOIN " _
            & " dbo.Periodes ON dbo.EpargnesPaiements.Periode = dbo.Periodes.IDPeriode " _
            & " WHERE dbo.EpargnesPaiements.IDEpargne = " & pIDEpargne & "ORDER BY EpargnesPaiements.IDEpargne ASC"
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function
    Public Function SelectAllEpargneForPeriode(pIDEpargne As Integer, pIDLivreur As Integer, pPeriode As Integer) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDEpargnePaiment] " _
            & "    ,[IDEpargne] " _
            & "    ,[IDLivreur] " _
            & "    ,[Livreur] " _
            & "    ,[DateEpargne] " _
            & "    ,[Epargne] " _
            & "    ,[Periode] " _
            & "FROM " _
            & "     [EpargnesPaiements] " _
            & " WHERE IDLivreur= " & pIDLivreur & " AND IDEpargne = " & pIDEpargne & " AND Periode=" & pPeriode


        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function
    Public Function SelectAll() As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDEpargnePaiment] " _
            & "    ,[IDEpargne] " _
            & "    ,[IDLivreur] " _
            & "    ,[Livreur] " _
            & "    ,[DateEpargne] " _
            & "    ,[Epargne] " _
            & "    ,[Periode] " _
            & "FROM " _
            & "     [EpargnesPaiements] " _
            & ""
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function

    Public Function Select_Record(ByVal clsEpargnesPaiementsPara As EpargnesPaiements) As EpargnesPaiements
        Dim clsEpargnesPaiements As New EpargnesPaiements
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDEpargnePaiment] " _
            & "    ,[IDEpargne] " _
            & "    ,[IDLivreur] " _
            & "    ,[Livreur] " _
            & "    ,[DateEpargne] " _
            & "    ,[Epargne] " _
            & "    ,[Periode] " _
            & "FROM " _
            & "     [EpargnesPaiements] " _
            & "WHERE " _
            & "     [IDEpargnePaiment] = @IDEpargnePaiment " _
            & ""
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        selectCommand.Parameters.AddWithValue("@IDEpargnePaiment", clsEpargnesPaiementsPara.IDEpargnePaiment)
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader(CommandBehavior.SingleRow)
            If reader.Read Then
                With clsEpargnesPaiements
                    .IDEpargnePaiment = System.Convert.ToInt32(reader("IDEpargnePaiment"))
                    .IDEpargne = System.Convert.ToInt32(reader("IDEpargne"))
                    .IDLivreur = If(IsDBNull(reader("IDLivreur")), Nothing, CType(reader("IDLivreur"), Int32?))
                    .Livreur = If(IsDBNull(reader("Livreur")), Nothing, reader("Livreur").ToString)
                    .DateEpargne = If(IsDBNull(reader("DateEpargne")), Nothing, CType(reader("DateEpargne"), DateTime?))
                    .Epargne = If(IsDBNull(reader("Epargne")), Nothing, CType(reader("Epargne"), Decimal?))
                    .Periode = If(IsDBNull(reader("Periode")), Nothing, CType(reader("Periode"), Int32?))
                End With
            Else
                clsEpargnesPaiements = Nothing
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return clsEpargnesPaiements
    End Function

    Public Function Add(ByVal clsEpargnesPaiements As EpargnesPaiements) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim insertStatement As String _
            = "INSERT " _
            & "     [EpargnesPaiements] " _
            & "     ( " _
            & "     [IDEpargne] " _
            & "    ,[IDLivreur] " _
            & "    ,[Livreur] " _
            & "    ,[DateEpargne] " _
            & "    ,[Epargne] " _
            & "    ,[Periode] " _
            & "     ) " _
            & "VALUES " _
            & "     ( " _
            & "     @IDEpargne " _
            & "    ,@IDLivreur " _
            & "    ,@Livreur " _
            & "    ,@DateEpargne " _
            & "    ,@Epargne " _
            & "    ,@Periode " _
            & "     ) " _
            & ""
        Dim insertCommand As New SqlCommand(insertStatement, connection)
        insertCommand.CommandType = CommandType.Text
        insertCommand.Parameters.AddWithValue("@IDEpargne", clsEpargnesPaiements.IDEpargne)
        insertCommand.Parameters.AddWithValue("@IDLivreur", IIf(clsEpargnesPaiements.IDLivreur.HasValue, clsEpargnesPaiements.IDLivreur, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Livreur", IIf(Not IsNothing(clsEpargnesPaiements.Livreur), clsEpargnesPaiements.Livreur, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@DateEpargne", IIf(clsEpargnesPaiements.DateEpargne.HasValue, clsEpargnesPaiements.DateEpargne, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Epargne", IIf(clsEpargnesPaiements.Epargne.HasValue, clsEpargnesPaiements.Epargne, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Periode", IIf(clsEpargnesPaiements.Periode.HasValue, clsEpargnesPaiements.Periode, DBNull.Value))
        Try
            connection.Open()
            Dim count As Integer = insertCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

    Public Function Update(ByVal clsEpargnesPaiementsOld As EpargnesPaiements,
           ByVal clsEpargnesPaiementsNew As EpargnesPaiements) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim updateStatement As String _
            = "UPDATE " _
            & "     [EpargnesPaiements] " _
            & "SET " _
            & "     [IDEpargne] = @NewIDEpargne " _
            & "    ,[IDLivreur] = @NewIDLivreur " _
            & "    ,[Livreur] = @NewLivreur " _
            & "    ,[DateEpargne] = @NewDateEpargne " _
            & "    ,[Epargne] = @NewEpargne " _
            & "    ,[Periode] = @NewPeriode " _
            & "WHERE " _
            & "     [IDEpargnePaiment] = @OldIDEpargnePaiment " _
            & ""
        Dim updateCommand As New SqlCommand(updateStatement, connection)
        updateCommand.CommandType = CommandType.Text
        updateCommand.Parameters.AddWithValue("@NewIDEpargne", clsEpargnesPaiementsNew.IDEpargne)
        updateCommand.Parameters.AddWithValue("@NewIDLivreur", IIf(clsEpargnesPaiementsNew.IDLivreur.HasValue, clsEpargnesPaiementsNew.IDLivreur, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewLivreur", IIf(Not IsNothing(clsEpargnesPaiementsNew.Livreur), clsEpargnesPaiementsNew.Livreur, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewDateEpargne", IIf(clsEpargnesPaiementsNew.DateEpargne.HasValue, clsEpargnesPaiementsNew.DateEpargne, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewEpargne", IIf(clsEpargnesPaiementsNew.Epargne.HasValue, clsEpargnesPaiementsNew.Epargne, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewPeriode", IIf(clsEpargnesPaiementsNew.Periode.HasValue, clsEpargnesPaiementsNew.Periode, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@OldIDEpargnePaiment", clsEpargnesPaiementsOld.IDEpargnePaiment)

        Try
            connection.Open()
            Dim count As Integer = updateCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

    Public Function Delete(ByVal clsEpargnesPaiements As EpargnesPaiements) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim deleteStatement As String _
            = "DELETE FROM " _
            & "     [EpargnesPaiements] " _
            & "WHERE " _
            & "     [IDEpargnePaiment] = @OldIDEpargnePaiment " _
            & ""
        Dim deleteCommand As New SqlCommand(deleteStatement, connection)
        deleteCommand.CommandType = CommandType.Text
        deleteCommand.Parameters.AddWithValue("@OldIDEpargnePaiment", clsEpargnesPaiements.IDEpargnePaiment)

        Try
            connection.Open()
            Dim count As Integer = deleteCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

    Public Function DeleteParPeriode(ByVal clsEpargnesPaiements As EpargnesPaiements) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim deleteStatement As String _
            = "DELETE FROM " _
            & "     [EpargnesPaiements] " _
            & "WHERE " _
            & "     [Periode] = @OldIDEpargnePaiment " _
            & ""
        Dim deleteCommand As New SqlCommand(deleteStatement, connection)
        deleteCommand.CommandType = CommandType.Text
        deleteCommand.Parameters.AddWithValue("@OldIDEpargnePaiment", clsEpargnesPaiements.Periode)

        Try
            connection.Open()
            Dim count As Integer = deleteCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

    Public Function DeleteByID(PIDEpargne As Integer) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim deleteStatement As String _
            = "DELETE FROM " _
            & "     [EpargnesPaiements] " _
            & " WHERE IDEpargne =" & PIDEpargne
        Dim deleteCommand As New SqlCommand(deleteStatement, connection)
        deleteCommand.CommandType = CommandType.Text


        Try
            connection.Open()
            Dim count As Integer = deleteCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

End Class

