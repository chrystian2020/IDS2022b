Imports System.Data.SqlClient

Public Class AchatsData

    Public Function GetTotalTVQIDS(pdDateFrom As DateTime, pdDateTo As DateTime) As Double
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT SUM(TVQ) AS TotalTVQ" _
            & " FROM " _
            & "     [Achats] " _
            & " WHERe (DateAchat  >= '" & pdDateFrom & "' AND DateAchat <= '" & pdDateTo & "')"


        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Dim dr As DataRow
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
                If dt.Rows.Count > 0 Then
                    For Each dr In dt.Rows
                        GetTotalTVQIDS = If(IsDBNull(dr("TotalTVQ")), 0, CType(dr("TotalTVQ"), Decimal?))
                    Next

                End If
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return GetTotalTVQIDS

    End Function


    Public Function GetTotalTPSIDS(pdDateFrom As DateTime, pdDateTo As DateTime) As Double
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT SUM(TPS) AS TotalTPS" _
            & " FROM " _
            & "     [Achats] " _
            & " WHERe (DateAchat  >= '" & pdDateFrom & "' AND DateAchat <= '" & pdDateTo & "')"


        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Dim dr As DataRow
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
                If dt.Rows.Count > 0 Then
                    For Each dr In dt.Rows
                        GetTotalTPSIDS = If(IsDBNull(dr("TotalTPS")), 0, CType(dr("TotalTPS"), Decimal?))
                    Next

                End If
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return GetTotalTPSIDS

    End Function
    Public Function GetTotalAchat(pdDateFrom As DateTime, pdDateTo As DateTime) As Double
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT SUM(Montant) AS TotalAchat" _
            & " FROM " _
            & "     [Achats] " _
            & " WHERe (DateFrom  >= '" & pdDateFrom & "' AND DateTo <= '" & pdDateTo & "')"


        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Dim dr As DataRow
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
                If dt.Rows.Count > 0 Then
                    For Each dr In dt.Rows
                        GetTotalAchat = If(IsDBNull(dr("TotalAchat")), 0, CType(dr("TotalAchat"), Decimal?))
                    Next


                End If
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return GetTotalAchat
    End Function

    Public Function SelectDISTINCTAllByDate(pdDateFrom As DateTime, pdDateTo As DateTime) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT DISTINCT" _
             & "    [Fournisseur] " _
            & "FROM " _
            & "     [Achats] " _
            & " WHERe (DateAchat  BETWEEN '" & pdDateFrom & "' AND '" & pdDateTo & "') AND Fournisseur IS NOT NULL and Fournisseur <> ''"


        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function


    Public Function SelectAllByDate(pdDateFrom As DateTime, pdDateTo As DateTime) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDAchat] " _
            & "    ,[DateAchat] " _
            & "    ,[IDFournisseur] " _
            & "    ,[Fournisseur] " _
            & "    ,[Unite] " _
            & "    ,[Prix] " _
            & "    ,[Montant] " _
            & "    ,[TPS] " _
            & "    ,[TVQ] " _
            & "    ,[Total] " _
            & "    ,[Description] " _
            & "FROM " _
            & "     [Achats] " _
            & " WHERe (DateAchat  BETWEEN '" & pdDateFrom & "' AND '" & pdDateTo & "') ORDER BY IDFournisseur"



        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")



        Finally
            connection.Close()
        End Try
        Return dt
    End Function
    Public Function SelectAllByDateParLivreur(pdDateFrom As DateTime, pdDateTo As DateTime, pIDLivreur As Integer) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "Select " _
            & "     [IDAchat] " _
            & "    ,[DateAchat] " _
            & "    ,[IDFournisseur] " _
            & "    ,[Fournisseur] " _
            & "    ,[Unite] " _
            & "    ,[Prix] " _
            & "    ,[Montant] " _
            & "    ,[TPS] " _
            & "    ,[TVQ] " _
            & "    ,[Total] " _
            & "    ,[Description] " _
            & "FROM " _
            & "     [Achats] " _
            & " WHERe (DateFrom  >= '" & pdDateFrom & "' AND DateTo <= '" & pdDateTo & "')"

        If go_Globals.IDPeriode > 0 Then
            selectStatement = selectStatement & " AND IDLivreur = " & pIDLivreur
        End If


        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function
    Public Function SelectAllByDateSUMLivreur(pdDateFrom As DateTime, pdDateTo As DateTime) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "    SUM(Montant) as TotalAmount " _
            & "    ,IDLivreur " _
            & "    ,Livreur " _
            & " FROM " _
            & "     [Achats] " _
            & " WHERE (DateAchat  >= '" & pdDateFrom & "' AND DateAchat <= '" & pdDateTo & "') AND IDLivreur IS NOT NULL"

        'If go_Globals.IDPeriode > 0 Then
        '    selectStatement = selectStatement & " And IDPeriode = " & go_Globals.IDPeriode
        'End If

        selectStatement = selectStatement & " GROUP BY IDLivreur,Livreur ORDER BY IDLivreur"

        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function

    Public Function SelectAllByDateSUMFournisseur(pdDateFrom As DateTime, pdDateTo As DateTime) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "    SUM(Montant) as TotalAmount " _
            & "    ,IDFournisseur " _
            & "    ,Fournisseur " _
            & " FROM " _
            & "     [Achats] " _
            & " WHERE (DateAchat  >= '" & pdDateFrom & "' AND DateAchat <= '" & pdDateTo & "') AND IDFournisseur IS NOT NULL"



        'If go_Globals.IDPeriode > 0 Then
        '    selectStatement = selectStatement & " And IDPeriode = " & go_Globals.IDPeriode
        'End If

        selectStatement = selectStatement & " GROUP BY IDFournisseur,Fournisseur ORDER BY  Fournisseur "

        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function
    Public Function SelectAllByPeriod() As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDAchat] " _
            & "    ,[DateAchat] " _
            & "    ,[IDFournisseur] " _
            & "    ,[Fournisseur] " _
            & "    ,[Unite] " _
            & "    ,[Prix] " _
            & "    ,[Montant] " _
            & "    ,[TPS] " _
            & "    ,[TVQ] " _
            & "    ,[Total] " _
            & "    ,[Description] " _
            & " FROM " _
            & "     [Achats] " _
            & " WHERE IDPeriode=" & go_Globals.IDPeriode



        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function
    Public Function SelectAll() As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDAchat] " _
            & "    ,[DateAchat] " _
            & "    ,[IDFournisseur] " _
            & "    ,[Fournisseur] " _
            & "    ,[Unite] " _
            & "    ,[Prix] " _
            & "    ,[Montant] " _
            & "    ,[TPS] " _
            & "    ,[TVQ] " _
            & "    ,[Total] " _
            & "    ,[Description] " _
            & "FROM " _
            & "     [Achats] " _
            & ""
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function


    Public Function Select_Record(ByVal clsAchatsPara As Achats) As Achats
        Dim clsAchats As New Achats
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDAchat] " _
            & "    ,[DateAchat] " _
            & "    ,[IDFournisseur] " _
            & "    ,[Fournisseur] " _
            & "    ,[Unite] " _
            & "    ,[Prix] " _
            & "    ,[Montant] " _
            & "    ,[TPS] " _
            & "    ,[TVQ] " _
            & "    ,[Total] " _
            & "    ,[Description] " _
            & "FROM " _
            & "     [Achats] " _
            & "WHERE " _
            & "     [IDAchat] = @IDAchat " _
            & ""
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        selectCommand.Parameters.AddWithValue("@IDAchat", clsAchatsPara.IDAchat)
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader(CommandBehavior.SingleRow)
            If reader.Read Then
                With clsAchats
                    .IDAchat = System.Convert.ToInt32(reader("IDAchat"))
                    .DateAchat = If(IsDBNull(reader("DateAchat")), Nothing, CType(reader("DateAchat"), DateTime?))
                    .IDFournisseur = If(IsDBNull(reader("IDFournisseur")), Nothing, CType(reader("IDFournisseur"), Int32?))
                    .Fournisseur = If(IsDBNull(reader("Fournisseur")), Nothing, reader("Fournisseur").ToString)
                    .Unite = If(IsDBNull(reader("Unite")), Nothing, CType(CType(reader("Unite"), Double?), Decimal?))
                    .Prix = If(IsDBNull(reader("Prix")), Nothing, CType(reader("Prix"), Decimal?))
                    .Montant = If(IsDBNull(reader("Montant")), Nothing, CType(reader("Montant"), Decimal?))
                    .TPS = If(IsDBNull(reader("TPS")), Nothing, CType(reader("TPS"), Decimal?))
                    .TVQ = If(IsDBNull(reader("TVQ")), Nothing, CType(reader("TVQ"), Decimal?))
                    .Total = If(IsDBNull(reader("Total")), Nothing, CType(reader("Total"), Decimal?))
                    .Description = If(IsDBNull(reader("Description")), Nothing, reader("Description").ToString)
                End With
            Else
                clsAchats = Nothing
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return clsAchats
    End Function

    Public Function Add(ByVal clsAchats As Achats) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim insertStatement As String _
            = "INSERT " _
            & "     [Achats] " _
            & "     ( " _
            & "     [DateAchat] " _
            & "    ,[IDFournisseur] " _
            & "    ,[Fournisseur] " _
            & "    ,[Unite] " _
            & "    ,[Prix] " _
            & "    ,[Montant] " _
            & "    ,[TPS] " _
            & "    ,[TVQ] " _
            & "    ,[Total] " _
            & "    ,[Description] " _
            & "     ) " _
            & "VALUES " _
            & "     ( " _
            & "     @DateAchat " _
            & "    ,@IDFournisseur " _
            & "    ,@Fournisseur " _
            & "    ,@Unite " _
            & "    ,@Prix " _
            & "    ,@Montant " _
            & "    ,@TPS " _
            & "    ,@TVQ " _
            & "    ,@Total " _
            & "    ,@Description " _
            & "     ) " _
            & ""
        Dim insertCommand As New SqlCommand(insertStatement, connection)
        insertCommand.CommandType = CommandType.Text
        insertCommand.Parameters.AddWithValue("@DateAchat", IIf(clsAchats.DateAchat.HasValue, clsAchats.DateAchat, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@IDFournisseur", IIf(clsAchats.IDFournisseur.HasValue, clsAchats.IDFournisseur, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Fournisseur", IIf(Not IsNothing(clsAchats.Fournisseur), clsAchats.Fournisseur, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Unite", IIf(clsAchats.Unite.HasValue, clsAchats.Unite, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Prix", IIf(clsAchats.Prix.HasValue, clsAchats.Prix, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Montant", IIf(clsAchats.Montant.HasValue, clsAchats.Montant, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@TPS", IIf(clsAchats.TPS.HasValue, clsAchats.TPS, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@TVQ", IIf(clsAchats.TVQ.HasValue, clsAchats.TVQ, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Total", IIf(clsAchats.Total.HasValue, clsAchats.Total, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Description", IIf(Not IsNothing(clsAchats.Description), clsAchats.Description, DBNull.Value))
        Try
            connection.Open()
            Dim count As Integer = insertCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

    Public Function Update(ByVal clsAchatsOld As Achats,
           ByVal clsAchatsNew As Achats) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim updateStatement As String _
            = "UPDATE " _
            & "     [Achats] " _
            & "SET " _
            & "     [DateAchat] = @NewDateAchat " _
            & "    ,[IDFournisseur] = @NewIDFournisseur " _
            & "    ,[Fournisseur] = @NewFournisseur " _
            & "    ,[Unite] = @NewUnite " _
            & "    ,[Prix] = @NewPrix " _
            & "    ,[Montant] = @NewMontant " _
            & "    ,[TPS] = @NewTPS " _
            & "    ,[TVQ] = @NewTVQ " _
            & "    ,[Total] = @NewTotal " _
            & "    ,[Description] = @NewDescription " _
            & "WHERE " _
            & "     [IDAchat] = @OldIDAchat " _
            & ""
        Dim updateCommand As New SqlCommand(updateStatement, connection)
        updateCommand.CommandType = CommandType.Text
        updateCommand.Parameters.AddWithValue("@NewDateAchat", IIf(clsAchatsNew.DateAchat.HasValue, clsAchatsNew.DateAchat, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewIDFournisseur", IIf(clsAchatsNew.IDFournisseur.HasValue, clsAchatsNew.IDFournisseur, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewFournisseur", IIf(Not IsNothing(clsAchatsNew.Fournisseur), clsAchatsNew.Fournisseur, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewUnite", IIf(clsAchatsNew.Unite.HasValue, clsAchatsNew.Unite, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewPrix", IIf(clsAchatsNew.Prix.HasValue, clsAchatsNew.Prix, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewMontant", IIf(clsAchatsNew.Montant.HasValue, clsAchatsNew.Montant, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewTPS", IIf(clsAchatsNew.TPS.HasValue, clsAchatsNew.TPS, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewTVQ", IIf(clsAchatsNew.TVQ.HasValue, clsAchatsNew.TVQ, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewTotal", IIf(clsAchatsNew.Total.HasValue, clsAchatsNew.Total, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewDescription", IIf(Not IsNothing(clsAchatsNew.Description), clsAchatsNew.Description, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@OldIDAchat", clsAchatsOld.IDAchat)

        Try
            connection.Open()
            Dim count As Integer = updateCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

    Public Function Delete(ByVal clsAchats As Achats) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim deleteStatement As String _
            = "DELETE FROM " _
            & "     [Achats] " _
            & "WHERE " _
            & "     [IDAchat] = @OldIDAchat " _
            & ""
        Dim deleteCommand As New SqlCommand(deleteStatement, connection)
        deleteCommand.CommandType = CommandType.Text
        deleteCommand.Parameters.AddWithValue("@OldIDAchat", clsAchats.IDAchat)

        Try
            connection.Open()
            Dim count As Integer = deleteCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

End Class

