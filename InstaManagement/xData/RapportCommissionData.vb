Imports System.Data.SqlClient

Public Class RapportCommissionData

    Public Function SelectAllByIDsCommission(piIDRapport As String) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDRapportCommission] " _
            & "    ,[IDPeriode] " _
            & "    ,[IDLivreur] " _
            & "    ,[Livreur] " _
            & "    ,[CheminRapport] " _
            & "    ,[NomFichier] " _
            & "    ,[Email] " _
            & "    ,[DateFrom] " _
            & "    ,[DateTo] " _
            & "FROM " _
            & "     [RapportCommission] " _
            & "  IN (" & piIDRapport & ")"
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function
    Public Function SelectAllDataByPeriodesP(PIDPeriode As Integer, piIDRapport As String) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDRapportCommission] " _
            & "    ,[IDPeriode] " _
            & "    ,[IDLivreur] " _
            & "    ,[Livreur] " _
            & "    ,[CheminRapport] " _
            & "    ,[NomFichier] " _
            & "    ,[Email] " _
            & "    ,[DateFrom] " _
            & "    ,[DateTo] " _
            & "FROM " _
            & "     [RapportCommission] " _
            & " WHERE IDPeriode =" & PIDPeriode

        If piIDRapport.ToString.Trim <> "" Then
            selectStatement = selectStatement & " AND IDRapportCommission IN (" & piIDRapport & ")"
        End If


        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function



    Public Function SelectAllPByDate(pdDateFrom As DateTime, pdDateTo As DateTime) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDRapportCommission] " _
            & "    ,[IDPeriode] " _
            & "    ,[IDLivreur] " _
            & "    ,[Livreur] " _
            & "    ,[CheminRapport] " _
            & "    ,[NomFichier] " _
            & "    ,[Email] " _
            & "    ,[DateFrom] " _
            & "    ,[DateTo] " _
            & " FROM " _
            & "     [RapportCommission] " _
            & " WHERE (DateFrom  >= '" & pdDateFrom & "' AND DateTo <= '" & pdDateTo & "') AND IDPeriode = " & go_Globals.IDPeriode
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function
    Public Function SelectAll() As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDRapportCommission] " _
            & "    ,[IDPeriode] " _
            & "    ,[IDLivreur] " _
            & "    ,[Livreur] " _
            & "    ,[CheminRapport] " _
            & "    ,[NomFichier] " _
            & "    ,[Email] " _
            & "    ,[DateFrom] " _
            & "    ,[DateTo] " _
            & "FROM " _
            & "     [RapportCommission] " _
            & ""
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function
    Public Function SelectAllByID(piIDRapport As String) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDRapportCommission] " _
            & "    ,[IDPeriode] " _
            & "    ,[IDLivreur] " _
            & "    ,[Livreur] " _
            & "    ,[CheminRapport] " _
            & "    ,[NomFichier] " _
            & "    ,[Email] " _
            & "    ,[DateFrom] " _
            & "    ,[DateTo] " _
            & "FROM " _
            & "     [RapportCommission] " _
            & " WHERE IDRapportCommission IN (" & piIDRapport & ")"
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function

    Public Function DeletePeriodLCommission(clsRapportCommission As RapportCommission) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim deleteStatement As String _
            = "DELETE FROM " _
            & "     [RapportCommission] " _
            & "WHERE " _
            & "     [IDPeriode] = @OldIDPeriode "

        Dim deleteCommand As New SqlCommand(deleteStatement, connection)
        deleteCommand.CommandType = CommandType.Text
        deleteCommand.Parameters.AddWithValue("@OldIDPeriode", clsRapportCommission.IDPeriode)


        Try
            connection.Open()
            Dim count As Integer = deleteCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

    Public Function Select_Record(ByVal clsRapportCommissionPara As RapportCommission) As RapportCommission
        Dim clsRapportCommission As New RapportCommission
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDRapportCommission] " _
            & "    ,[IDPeriode] " _
            & "    ,[IDLivreur] " _
            & "    ,[Livreur] " _
            & "    ,[CheminRapport] " _
            & "    ,[NomFichier] " _
            & "    ,[Email] " _
            & "    ,[DateFrom] " _
            & "    ,[DateTo] " _
            & "FROM " _
            & "     [RapportCommission] " _
            & "WHERE " _
            & "     [IDRapportCommission] = @IDRapportCommission " _
            & ""
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        selectCommand.Parameters.AddWithValue("@IDRapportCommission", clsRapportCommissionPara.IDRapportCommission)
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader(CommandBehavior.SingleRow)
            If reader.Read Then
                With clsRapportCommission
                    .IDRapportCommission = System.Convert.ToInt32(reader("IDRapportCommission"))
                    .IDPeriode = If(IsDBNull(reader("IDPeriode")), Nothing, CType(reader("IDPeriode"), Int32?))
                    .IDLivreur = If(IsDBNull(reader("IDLivreur")), Nothing, CType(reader("IDLivreur"), Int32?))
                    .Livreur = If(IsDBNull(reader("Livreur")), Nothing, reader("Livreur").ToString)
                    .CheminRapport = If(IsDBNull(reader("CheminRapport")), Nothing, reader("CheminRapport").ToString)
                    .NomFichier = If(IsDBNull(reader("NomFichier")), Nothing, reader("NomFichier").ToString)
                    .Email = If(IsDBNull(reader("Email")), Nothing, reader("Email").ToString)
                    .DateFrom = If(IsDBNull(reader("DateFrom")), Nothing, CType(reader("DateFrom"), DateTime?))
                    .DateTo = If(IsDBNull(reader("DateTo")), Nothing, CType(reader("DateTo"), DateTime?))
                End With
            Else
                clsRapportCommission = Nothing
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return clsRapportCommission
    End Function

    Public Function Add(ByVal clsRapportCommission As RapportCommission) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim insertStatement As String _
            = "INSERT " _
            & "     [RapportCommission] " _
            & "     ( " _
            & "     [IDPeriode] " _
            & "    ,[IDLivreur] " _
            & "    ,[Livreur] " _
            & "    ,[CheminRapport] " _
            & "    ,[NomFichier] " _
            & "    ,[Email] " _
            & "    ,[DateFrom] " _
            & "    ,[DateTo] " _
            & "     ) " _
            & "VALUES " _
            & "     ( " _
            & "     @IDPeriode " _
            & "    ,@IDLivreur " _
            & "    ,@Livreur " _
            & "    ,@CheminRapport " _
            & "    ,@NomFichier " _
            & "    ,@Email " _
            & "    ,@DateFrom " _
            & "    ,@DateTo " _
            & "     ) " _
            & ""
        Dim insertCommand As New SqlCommand(insertStatement, connection)
        insertCommand.CommandType = CommandType.Text
        insertCommand.Parameters.AddWithValue("@IDPeriode", IIf(clsRapportCommission.IDPeriode.HasValue, clsRapportCommission.IDPeriode, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@IDLivreur", IIf(clsRapportCommission.IDLivreur.HasValue, clsRapportCommission.IDLivreur, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Livreur", IIf(Not IsNothing(clsRapportCommission.Livreur), clsRapportCommission.Livreur, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@CheminRapport", IIf(Not IsNothing(clsRapportCommission.CheminRapport), clsRapportCommission.CheminRapport, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@NomFichier", IIf(Not IsNothing(clsRapportCommission.NomFichier), clsRapportCommission.NomFichier, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Email", IIf(Not IsNothing(clsRapportCommission.Email), clsRapportCommission.Email, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@DateFrom", IIf(clsRapportCommission.DateFrom.HasValue, clsRapportCommission.DateFrom, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@DateTo", IIf(clsRapportCommission.DateTo.HasValue, clsRapportCommission.DateTo, DBNull.Value))

        Try
            connection.Open()
            Dim count As Integer = insertCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

    Public Function Update(ByVal clsRapportCommissionOld As RapportCommission,
           ByVal clsRapportCommissionNew As RapportCommission) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim updateStatement As String _
            = "UPDATE " _
            & "     [RapportCommission] " _
            & "SET " _
            & "     [IDPeriode] = @NewIDPeriode " _
            & "    ,[IDLivreur] = @NewIDLivreur " _
            & "    ,[Livreur] = @NewLivreur " _
            & "    ,[CheminRapport] = @NewCheminRapport " _
            & "    ,[NomFichier] = @NewNomFichier " _
            & "    ,[Email] = @NewEmail " _
            & "    ,[DateFrom] = @NewDateFrom " _
            & "    ,[DateTo] = @NewDateTo " _
            & "WHERE " _
            & "     [IDRapportCommission] = @OldIDRapportCommission " _
            & ""
        Dim updateCommand As New SqlCommand(updateStatement, connection)
        updateCommand.CommandType = CommandType.Text
        updateCommand.Parameters.AddWithValue("@NewIDPeriode", IIf(clsRapportCommissionNew.IDPeriode.HasValue, clsRapportCommissionNew.IDPeriode, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewIDLivreur", IIf(clsRapportCommissionNew.IDLivreur.HasValue, clsRapportCommissionNew.IDLivreur, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewLivreur", IIf(Not IsNothing(clsRapportCommissionNew.Livreur), clsRapportCommissionNew.Livreur, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewCheminRapport", IIf(Not IsNothing(clsRapportCommissionNew.CheminRapport), clsRapportCommissionNew.CheminRapport, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewNomFichier", IIf(Not IsNothing(clsRapportCommissionNew.NomFichier), clsRapportCommissionNew.NomFichier, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewEmail", IIf(Not IsNothing(clsRapportCommissionNew.Email), clsRapportCommissionNew.Email, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewDateFrom", IIf(clsRapportCommissionNew.DateFrom.HasValue, clsRapportCommissionNew.DateFrom, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewDateTo", IIf(clsRapportCommissionNew.DateTo.HasValue, clsRapportCommissionNew.DateTo, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@OldIDRapportCommission", clsRapportCommissionOld.IDRapportCommission)
        Try
            connection.Open()
            Dim count As Integer = updateCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

    Public Function Delete(ByVal clsRapportCommission As RapportCommission) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim deleteStatement As String _
            = "DELETE FROM " _
            & "     [RapportCommission] " _
            & "WHERE " _
            & "     [IDRapportCommission] = @OldIDRapportCommission " _
            & ""
        Dim deleteCommand As New SqlCommand(deleteStatement, connection)
        deleteCommand.CommandType = CommandType.Text
        deleteCommand.Parameters.AddWithValue("@OldIDRapportCommission", clsRapportCommission.IDRapportCommission)

        Try
            connection.Open()
            Dim count As Integer = deleteCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

End Class

