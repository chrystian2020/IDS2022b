Imports System.Data.SqlClient

Public Class PayeData
    Public Function GetTotalTVQLivreur(pdDateFrom As DateTime, pdDateTo As DateTime, pIDLivreur As Integer) As Double
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT SUM(TVQ) AS TotalTVQ" _
            & " FROM " _
            & "     [paye] " _
            & " WHERe (DateFrom  >= '" & pdDateFrom & "' AND DateTo <= '" & pdDateTo & "')" _
            & " AND IDLivreur=" & pIDLivreur


        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Dim dr As DataRow
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
                If dt.Rows.Count > 0 Then
                    For Each dr In dt.Rows
                        GetTotalTVQLivreur = If(IsDBNull(dr("TotalTVQ")), 0, CType(dr("TotalTVQ"), Decimal?))
                    Next

                End If
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try

    End Function

    Public Function GetTotalTVQIDS(pdDateFrom As DateTime, pdDateTo As DateTime) As Double
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT SUM(TVQ) AS TotalTVQ" _
            & " FROM " _
            & "     [paye] " _
            & " WHERe (DateFrom  >= '" & pdDateFrom & "' AND DateTo <= '" & pdDateTo & "')"


        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Dim dr As DataRow
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
                If dt.Rows.Count > 0 Then
                    For Each dr In dt.Rows
                        GetTotalTVQIDS = If(IsDBNull(dr("TotalTVQ")), 0, CType(dr("TotalTVQ"), Decimal?))
                    Next

                End If
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return GetTotalTVQIDS

    End Function


    Public Function GetTotalTPSIDS(pdDateFrom As DateTime, pdDateTo As DateTime) As Double

        Dim dTotal As Double = 0

        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT SUM(TPS) AS TotalTPS" _
            & " FROM " _
            & "     [paye] " _
            & " WHERe (DateFrom  >= '" & pdDateFrom & "' AND DateTo <= '" & pdDateTo & "')"


        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Dim dr As DataRow
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
                If dt.Rows.Count > 0 Then
                    For Each dr In dt.Rows
                        dTotal = If(IsDBNull(dr("TotalTPS")), 0, CType(dr("TotalTPS"), Decimal?))
                    Next

                End If
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dTotal

    End Function


    Public Function GetTotalTPSLivreur(pdDateFrom As DateTime, pdDateTo As DateTime, pIDLivreurID As Integer) As Double
        Dim connection As SqlConnection = DB.GetConnection
        Dim dTotal As Double = 0
        Dim selectStatement As String _
            = "SELECT SUM(TPS) AS TotalTPS" _
            & " FROM " _
            & "     [paye] " _
            & " WHERe (DateFrom  >= '" & pdDateFrom & "' AND DateTo <= '" & pdDateTo & "')" _
            & " AND IDLivreur = " & pIDLivreurID




        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Dim dr As DataRow
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
                If dt.Rows.Count > 0 Then
                    For Each dr In dt.Rows
                        dTotal = If(IsDBNull(dr("TotalTPS")), 0, CType(dr("TotalTPS"), Decimal?))
                    Next

                End If
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dTotal

    End Function

    Public Function GetTotalPayeLivreur(pdDateFrom As DateTime, pdDateTo As DateTime, piIDLivreur As Integer) As DataTable

        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT SUM(Amount) AS TotalPaye" _
            & " FROM " _
            & "     [paye] " _
            & " WHERe (DateFrom  >= '" & pdDateFrom & "' AND DateTo <= '" & pdDateTo & "')" _
            & " AND IDLivreur = " & piIDLivreur


        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt

    End Function

    Public Function GetTotalPaye(pdDateFrom As DateTime, pdDateTo As DateTime, piIDLivreur As Integer) As Double
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT SUM(Amount) AS TotalPaye" _
            & "     ,[IDPaye] " _
            & "    ,[PayeNumber] " _
            & "    ,[PayeDate] " _
            & "    ,[IDLivreur] " _
            & "    ,[Livreur] " _
            & "    ,[DateFrom] " _
            & "    ,[DateTo] " _
            & "    ,[IDPeriode] " _
            & "    ,[Amount] " _
            & "    ,[TPS] " _
            & "    ,[TVQ] " _
            & "    ,[Total] " _
            & " FROM " _
            & "     [paye] " _
            & " WHERe (DateFrom  >= '" & pdDateFrom & "' AND DateTo <= '" & pdDateTo & "')" _
            & " AND IDLivreur = " & piIDLivreur


        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Dim dr As DataRow
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
                If dt.Rows.Count > 0 Then
                    For Each dr In dt.Rows
                        GetTotalPaye = If(IsDBNull(dr("TotalPaye")), 0, CType(dr("TotalPaye"), Decimal?))
                    Next


                End If
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return GetTotalPaye
    End Function
    Public Function SelectPayeParLivreur(pdDateFrom As DateTime, pdDateTo As DateTime, piIDLivreur As Integer) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT DISTINCT" _
            & "    [IDLivreur] " _
            & " FROM " _
            & "     [Paye] " _
            & " WHERE  (DateFrom  >= '" & pdDateFrom & "' AND DateTo <= '" & pdDateTo & "')"

        If piIDLivreur > 0 Then
            selectStatement = selectStatement & "AND LivreurID IS NOT NULL AND IDLivreur = " & piIDLivreur

        End If


        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function


    Public Function SelectAllPayeParLivreur(pdDateFrom As DateTime, pdDateTo As DateTime, piIDLivreur As Integer, pLivreur As String) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT DISTINCT" _
         & "     [IDPaye] " _
            & "    ,[PayeNumber] " _
            & "    ,[PayeDate] " _
            & "    ,[IDLivreur] " _
            & "    ,[Livreur] " _
            & "    ,[DateFrom] " _
            & "    ,[DateTo] " _
            & "    ,[IDPeriode] " _
            & "    ,[Amount] " _
            & "    ,[TPS] " _
            & "    ,[TVQ] " _
            & "    ,[Total] " _
            & "FROM " _
            & "     [Paye] " _
            & " WHERE  (DateFrom  >= '" & pdDateFrom & "' AND DateTo <= '" & pdDateTo & "')"

        If piIDLivreur > 0 Then
            selectStatement = selectStatement & " AND IDLivreur = " & piIDLivreur & " AND livreur ='" & pLivreur.ToString.Trim & "'"
        End If


        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function

    Public Function SelectAllLivreurs(pdDateFrom As DateTime, pdDateTo As DateTime) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT DISTINCT" _
            & "    [IDLivreur] " _
            & "    ,[Livreur] " _
            & "FROM " _
            & "     [Paye] " _
            & " WHERE  (DateFrom  >= '" & pdDateFrom & "' AND DateTo <= '" & pdDateTo & "')"




        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function
    Public Function SelectAllLivreursRevenu(pdDateFrom As DateTime, pdDateTo As DateTime) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT DISTINCT" _
            & "    dbo.Paye.IDLivreur " _
            & "FROM dbo.Paye LEFT JOIN dbo.livreurs ON dbo.Paye.IDLivreur = dbo.livreurs.LivreurID " _
            & " WHERE  (dbo.Paye.DateFrom  >= '" & pdDateFrom & "' AND dbo.Paye.DateTo <= '" & pdDateTo & "') AND Paye.IDLivreur IS NOT NULL"




        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function

    Public Function SelectAllLivreursTaxable(pdDateFrom As DateTime, pdDateTo As DateTime) As DataTable


        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT DISTINCT" _
            & "    dbo.Paye.IDLivreur " _
            & "FROM dbo.Paye LEFT JOIN dbo.livreurs ON dbo.Paye.IDLivreur = dbo.livreurs.LivreurID " _
            & " WHERE  (dbo.Paye.DateFrom  >= '" & pdDateFrom & "' AND dbo.Paye.DateTo <= '" & pdDateTo & "') AND Paye.IDLivreur IS NOT NULL"

        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function


    Public Function SelectAllLivreur(pdDateFrom As DateTime, pdDateTo As DateTime) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "Select DISTINCT IDLivreur " &
            " FROM   dbo.Paye " &
            " WHERE (dbo.Paye.DateFrom  >= '" & pdDateFrom & "' AND dbo.Paye.DateTo <= '" & pdDateTo & "')"

        'If go_Globals.IDPeriode > 0 Then
        '    selectStatement = selectStatement & " AND dbo.invoice.IDPeriode = " & go_Globals.IDPeriode
        'End If



        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function
    Public Function SelectAllByPeriodLivreurID(IDPeriod As Integer, pIDLivreur As Integer) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDPaye] " _
            & "    ,[PayeNumber] " _
            & "    ,[PayeDate] " _
            & "    ,[IDLivreur] " _
            & "    ,[Livreur] " _
            & "    ,[DateFrom] " _
            & "    ,[DateTo] " _
            & "    ,[IDPeriode] " _
            & "    ,[Amount] " _
            & "    ,[TPS] " _
            & "    ,[TVQ] " _
            & "    ,[Total] " _
            & "FROM " _
            & "     [Paye] " _
            & " WHERE IDPeriode=" & IDPeriod & " AND IDLivreur=" & pIDLivreur

        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function

    Public Function SelectAllByDateivreur(pdDateFrom As DateTime, pdDateTo As DateTime, pLivreurID As Integer) As DataTable

        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDPaye] " _
            & "    ,[PayeNumber] " _
            & "    ,[PayeDate] " _
            & "    ,[IDLivreur] " _
            & "    ,[Livreur] " _
            & "    ,[DateFrom] " _
            & "    ,[DateTo] " _
            & "    ,[IDPeriode] " _
            & "    ,[Amount] " _
            & "    ,[TPS] " _
            & "    ,[TVQ] " _
            & "    ,[Total] " _
            & " FROM " _
            & "     [Paye] " _
            & " WHERE (DateFrom  >= '" & pdDateFrom & "' AND DateTo <= '" & pdDateTo & "') AND IDLivreur=" & pLivreurID





        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt

    End Function

    Public Function SelectAllByDateSUMLivreur(pdDateFrom As DateTime, pdDateTo As DateTime, pLivreurID As Integer) As DataTable

        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "    SUM(Amount) as TotalAmount " _
            & "    ,SUM(TPS) as TotalTPS" _
            & "    ,SUM(TVQ) as TotalTVQ " _
            & "    ,IDLivreur " _
            & "    ,Livreur " _
            & " FROM " _
            & "     [Paye] " _
            & " WHERE (DateFrom  >= '" & pdDateFrom & "' AND DateTo <= '" & pdDateTo & "') AND IDLivreur=" & pLivreurID _
            & " GROUP BY IDLivreur,Livreur"




        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt

    End Function



    Public Function SelectAllByDateSUM(pdDateFrom As DateTime, pdDateTo As DateTime) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "    SUM(Total) as TotalAmount " _
            & "    ,SUM(TPS) as TotalTPS" _
            & "    ,SUM(TVQ) as TotalTVQ " _
            & "    ,IDLivreur " _
            & "    ,Livreur " _
            & " FROM " _
            & "     [Paye] " _
            & " WHERE (PayeDate  >= '" & pdDateFrom & "' AND PayeDate <= '" & pdDateTo & "')"



        'If go_Globals.IDPeriode > 0 Then
        '    selectStatement = selectStatement & " And IDPeriode = " & go_Globals.IDPeriode
        'End If

        selectStatement = selectStatement & " GROUP BY IDLivreur,Livreur "

        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function

    Public Function SelectAllByDate(pdDateFrom As DateTime, pdDateTo As DateTime) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "Select " _
            & "     [IDPaye] " _
            & "    ,[PayeNumber] " _
            & "    ,[PayeDate] " _
            & "    ,[IDLivreur] " _
            & "    ,[Livreur] " _
            & "    ,[DateFrom] " _
            & "    ,[DateTo] " _
            & "    ,[IDPeriode] " _
            & "    ,[Amount] " _
            & "    ,[TPS] " _
            & "    ,[TVQ] " _
            & "    ,[Total] " _
            & "FROM " _
            & "     [Paye] " _
             & " WHERE (DateFrom  >= '" & pdDateFrom & "' AND DateTo <= '" & pdDateTo & "')"

        If go_Globals.IDPeriode > 0 Then
            selectStatement = selectStatement & " AND IDPeriode = " & go_Globals.IDPeriode
        End If


        selectStatement = selectStatement & " ORDER BY IDPaye DESC"

        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function

    Public Function SelectLivreurAvecPayeStringCommission(pdDateFrom As DateTime, pdDateTo As DateTime, piIDLivreur As String) As DataTable
        Dim connection As SqlConnection = DB.GetConnection

        'Select Case DISTINCT dbo.Paye.IDLivreur
        'From dbo.Paye LEFT OUTER Join
        'dbo.livreurs ON dbo.Paye.IDLivreur = dbo.livreurs.LivreurID
        Dim selectStatement As String _
            = "Select DISTINCT dbo.Paye.IDLivreur" _
            & " From dbo.Paye LEFT OUTER Join" _
            & " dbo.livreurs ON dbo.Paye.IDLivreur = dbo.livreurs.LivreurID " _
            & " WHERE  (dbo.Paye.DateFrom  >= '" & pdDateFrom & "' AND dbo.Paye.DateTo <= '" & pdDateTo & "')  AND (dbo.livreurs.IsVendeur = 1) AND dbo.Paye.IDPeriode = " & go_Globals.IDPeriode

        If piIDLivreur.ToString.Trim <> "" Then
            selectStatement = selectStatement & " AND dbo.Paye.IDLivreur IN (" & piIDLivreur & ")"
        End If


        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function

    Public Function SelectLivreurAvecPayeString(pdDateFrom As DateTime, pdDateTo As DateTime, piIDLivreur As String) As DataTable
        Dim connection As SqlConnection = DB.GetConnection

        'Select Case DISTINCT dbo.Paye.IDLivreur
        'From dbo.Paye LEFT OUTER Join
        'dbo.livreurs ON dbo.Paye.IDLivreur = dbo.livreurs.LivreurID
        Dim selectStatement As String _
            = "Select DISTINCT dbo.Paye.IDLivreur" _
            & " From dbo.Paye LEFT OUTER Join" _
            & " dbo.livreurs ON dbo.Paye.IDLivreur = dbo.livreurs.LivreurID " _
            & " WHERE  (dbo.Paye.DateFrom  >= '" & pdDateFrom & "' AND dbo.Paye.DateTo <= '" & pdDateTo & "')  AND (dbo.livreurs.IsVendeur = 0) AND dbo.Paye.IDPeriode = " & go_Globals.IDPeriode

        If piIDLivreur.ToString.Trim <> "" Then
            selectStatement = selectStatement & " AND dbo.Paye.IDLivreur IN (" & piIDLivreur & ")"
        End If


        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function
    Public Function SelectLivreurAvecPaye(pdDateFrom As DateTime, pdDateTo As DateTime, piIDLivreur As Integer) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT DISTINCT" _
            & "    [IDLivreur] " _
            & "FROM " _
            & "     [Paye] " _
            & " WHERE  (DateFrom  >= '" & pdDateFrom & "' AND DateTo <= '" & pdDateTo & "') AND IDPeriode = " & go_Globals.IDPeriode

        If piIDLivreur > 0 Then
            selectStatement = selectStatement & " AND IDLivreur=" & piIDLivreur

        End If


        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function
    Public Function SelectPayeByDateCommission(piLivreurID As Integer) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDPaye] " _
            & "    ,[PayeNumber] " _
            & "    ,[PayeDate] " _
            & "    ,[IDLivreur] " _
            & "    ,[Livreur] " _
            & "    ,[DateFrom] " _
            & "    ,[DateTo] " _
            & "    ,[IDPeriode] " _
            & "    ,[Amount] " _
            & "    ,[TPS] " _
            & "    ,[TVQ] " _
            & "    ,[Total] " _
            & "FROM " _
            & "     [Paye] " _
            & " WHERE IsCommissionVendeur = 1 AND IDLivreur = " & piLivreurID & " And IDPeriode = " & go_Globals.IDPeriode

        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function
    Public Function SelectPayeByDate(piLivreurID As Integer) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDPaye] " _
            & "    ,[PayeNumber] " _
            & "    ,[PayeDate] " _
            & "    ,[IDLivreur] " _
            & "    ,[Livreur] " _
            & "    ,[DateFrom] " _
            & "    ,[DateTo] " _
            & "    ,[IDPeriode] " _
            & "    ,[Amount] " _
            & "    ,[TPS] " _
            & "    ,[TVQ] " _
            & "    ,[Total] " _
            & "FROM " _
            & "     [Paye] " _
            & " WHERE IDLivreur = " & piLivreurID & " And IDPeriode = " & go_Globals.IDPeriode

        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function
    Public Function SelectAllPayeNumber(pdDateFrom As DateTime, pdDateTo As DateTime) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "    IDPaye,PayeNumber " _
            & "FROM " _
            & "     [Paye] " _
            & " WHERE (DateFrom  >= '" & pdDateFrom & "' AND DateTo <= '" & pdDateTo & "')  AND IDPeriode = " & go_Globals.IDPeriode & " ORDER BY IDPaye"

        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function
    Public Function SelectAllByPeriodIDLivreur(IDPeriod As Integer, pIDLivreur As Integer) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDPaye] " _
            & "    ,[PayeNumber] " _
            & "    ,[PayeDate] " _
            & "    ,[IDLivreur] " _
            & "    ,[Livreur] " _
            & "    ,[DateFrom] " _
            & "    ,[DateTo] " _
            & "    ,[IDPeriode] " _
            & "    ,[Amount] " _
            & "    ,[TPS] " _
            & "    ,[TVQ] " _
            & "    ,[Total] " _
            & "FROM " _
            & "     [Paye] " _
            & " WHERE IDPeriode=" & IDPeriod & "AND  IDLivreur = " & pIDLivreur
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function
    Public Function SelectAllByPeriodCommission(IDPeriod As Integer) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDPaye] " _
            & "    ,[PayeNumber] " _
            & "    ,[PayeDate] " _
            & "    ,[IDLivreur] " _
            & "    ,[Livreur] " _
            & "    ,[DateFrom] " _
            & "    ,[DateTo] " _
            & "    ,[IDPeriode] " _
            & "    ,[Amount] " _
            & "    ,[TPS] " _
            & "    ,[TVQ] " _
            & "    ,[IsCommissionVendeur] " _
            & "    ,[Total] " _
            & "FROM " _
            & "     [Paye] " _
            & " WHERE IDPeriode=" & IDPeriod _
            & " AND IsCommissionVendeur = 1"
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function
    Public Function SelectAllByPeriod(IDPeriod As Integer) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDPaye] " _
            & "    ,[PayeNumber] " _
            & "    ,[PayeDate] " _
            & "    ,[IDLivreur] " _
            & "    ,[Livreur] " _
            & "    ,[DateFrom] " _
            & "    ,[DateTo] " _
            & "    ,[IDPeriode] " _
            & "    ,[Amount] " _
            & "    ,[TPS] " _
            & "    ,[TVQ] " _
            & "    ,[Total] " _
            & "FROM " _
            & "     [Paye] " _
            & " WHERE IDPeriode=" & IDPeriod _
            & " AND IsCommissionVendeur = 0"
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function
    Public Function SelectAll() As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDPaye] " _
            & "    ,[PayeNumber] " _
            & "    ,[PayeDate] " _
            & "    ,[IDLivreur] " _
            & "    ,[Livreur] " _
            & "    ,[DateFrom] " _
            & "    ,[DateTo] " _
            & "    ,[IDPeriode] " _
            & "    ,[Amount] " _
            & "    ,[TPS] " _
            & "    ,[TVQ] " _
            & "    ,[Total] " _
            & "FROM " _
            & "     [Paye] " _
            & ""
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function

    Public Function Select_Record(ByVal clsPayePara As Paye) As Paye
        Dim clsPaye As New Paye
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDPaye] " _
            & "    ,[PayeNumber] " _
            & "    ,[PayeDate] " _
            & "    ,[IDLivreur] " _
            & "    ,[Livreur] " _
            & "    ,[DateFrom] " _
            & "    ,[DateTo] " _
            & "    ,[IDPeriode] " _
            & "    ,[Amount] " _
            & "    ,[TPS] " _
            & "    ,[TVQ] " _
            & "FROM " _
            & "     [Paye] " _
            & "WHERE " _
            & "     [IDPaye] = @IDPaye " _
            & ""
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        selectCommand.Parameters.AddWithValue("@IDPaye", clsPayePara.IDPaye)
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader(CommandBehavior.SingleRow)
            If reader.Read Then
                With clsPaye
                    .IDPaye = System.Convert.ToInt32(reader("IDPaye"))
                    .PayeNumber = If(IsDBNull(reader("PayeNumber")), Nothing, reader("PayeNumber").ToString)
                    .PayeDate = If(IsDBNull(reader("PayeDate")), Nothing, CType(reader("PayeDate"), DateTime?))
                    .IDLivreur = If(IsDBNull(reader("IDLivreur")), Nothing, CType(reader("IDLivreur"), Int32?))
                    .Livreur = If(IsDBNull(reader("Livreur")), Nothing, reader("Livreur").ToString)
                    .DateFrom = If(IsDBNull(reader("DateFrom")), Nothing, CType(reader("DateFrom"), DateTime?))
                    .DateTo = If(IsDBNull(reader("DateTo")), Nothing, CType(reader("DateTo"), DateTime?))
                    .IDPeriode = If(IsDBNull(reader("IDPeriode")), Nothing, CType(reader("IDPeriode"), Int32?))
                    .Amount = If(IsDBNull(reader("Amount")), Nothing, CType(reader("Amount"), Decimal?))
                    .TPS = If(IsDBNull(reader("TPS")), Nothing, CType(reader("TPS"), Decimal?))
                    .TVQ = If(IsDBNull(reader("TVQ")), Nothing, CType(reader("TVQ"), Decimal?))
                End With
            Else
                clsPaye = Nothing
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return clsPaye
    End Function

    Public Function Add(ByVal clsPaye As Paye) As Integer
        Dim connection As SqlConnection = DB.GetConnection
        Dim insertStatement As String _
            = "INSERT " _
            & "     [Paye] " _
            & "     ( " _
            & "     [PayeNumber] " _
            & "    ,[PayeDate] " _
            & "    ,[IDLivreur] " _
            & "    ,[Livreur] " _
            & "    ,[DateFrom] " _
            & "    ,[DateTo] " _
            & "    ,[IDPeriode] " _
            & "    ,[Amount] " _
            & "    ,[TPS] " _
            & "    ,[TVQ] " _
            & "    ,[IsCommissionVendeur] " _
            & "     ) " _
            & "VALUES " _
            & "     ( " _
            & "     @PayeNumber " _
            & "    ,@PayeDate " _
            & "    ,@IDLivreur " _
            & "    ,@Livreur " _
            & "    ,@DateFrom " _
            & "    ,@DateTo " _
            & "    ,@IDPeriode " _
            & "    ,@Amount " _
            & "    ,@TPS " _
            & "    ,@TVQ " _
            & "    ,@IsCommissionVendeur " _
            & "     );SELECT SCOPE_IDENTITY() " _
            & ""
        Dim insertCommand As New SqlCommand(insertStatement, connection)
        insertCommand.CommandType = CommandType.Text
        insertCommand.Parameters.AddWithValue("@PayeNumber", IIf(Not IsNothing(clsPaye.PayeNumber), clsPaye.PayeNumber, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@PayeDate", IIf(clsPaye.PayeDate.HasValue, clsPaye.PayeDate, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@IDLivreur", IIf(clsPaye.IDLivreur.HasValue, clsPaye.IDLivreur, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Livreur", IIf(Not IsNothing(clsPaye.Livreur), clsPaye.Livreur, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@DateFrom", IIf(clsPaye.DateFrom.HasValue, clsPaye.DateFrom, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@DateTo", IIf(clsPaye.DateTo.HasValue, clsPaye.DateTo, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@IDPeriode", IIf(clsPaye.IDPeriode.HasValue, clsPaye.IDPeriode, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Amount", IIf(clsPaye.Amount.HasValue, clsPaye.Amount, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@TPS", IIf(clsPaye.TPS.HasValue, clsPaye.TPS, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@TVQ", IIf(clsPaye.TVQ.HasValue, clsPaye.TVQ, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@IsCommissionVendeur", IIf(clsPaye.IsCommissionVendeur.HasValue, clsPaye.IsCommissionVendeur, DBNull.Value))
        Try
            connection.Open()
            Dim count As Integer = insertCommand.ExecuteScalar()
            If count > 0 Then
                Return count
            Else
                Return count
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

    Public Function UpdatePayeDate(ByVal clsPayeOld As Paye,
           ByVal clsPayeNew As Paye) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim updateStatement As String _
            = "UPDATE " _
            & "     [Paye] " _
            & "SET " _
            & "    [PayeDate] = @NewPayeDate " _
            & "WHERE " _
            & "     [IDPaye] = @OldIDPaye " _
            & ""
        Dim updateCommand As New SqlCommand(updateStatement, connection)
        updateCommand.CommandType = CommandType.Text
        updateCommand.Parameters.AddWithValue("@NewPayeDate", IIf(clsPayeNew.PayeDate.HasValue, clsPayeNew.PayeDate, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@OldIDPaye", clsPayeOld.IDPaye)


        Try
            connection.Open()
            Dim count As Integer = updateCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function


    Public Function UpdateAmount(ByVal clsPayeOld As Paye,
           ByVal clsPayeNew As Paye) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim updateStatement As String _
            = "UPDATE " _
            & "     [Paye] " _
            & "SET " _
            & "    [Amount] = @NewAmount " _
            & "    ,[TPS] = @NewTPS " _
            & "    ,[TVQ] = @NewTVQ " _
            & "WHERE " _
            & "     [IDPaye] = @OldIDPaye " _
            & ""
        Dim updateCommand As New SqlCommand(updateStatement, connection)
        updateCommand.CommandType = CommandType.Text
        updateCommand.Parameters.AddWithValue("@NewAmount", IIf(clsPayeNew.Amount.HasValue, clsPayeNew.Amount, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewTPS", IIf(clsPayeNew.TPS.HasValue, clsPayeNew.TPS, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewTVQ", IIf(clsPayeNew.TVQ.HasValue, clsPayeNew.TVQ, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@OldIDPaye", clsPayeOld.IDPaye)


        Try
            connection.Open()
            Dim count As Integer = updateCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

    Public Function Update(ByVal clsPayeOld As Paye,
           ByVal clsPayeNew As Paye) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim updateStatement As String _
            = "UPDATE " _
            & "     [Paye] " _
            & "SET " _
            & "     [PayeNumber] = @NewPayeNumber " _
            & "    ,[PayeDate] = @NewPayeDate " _
            & "    ,[IDLivreur] = @NewIDLivreur " _
            & "    ,[Livreur] = @NewLivreur " _
            & "    ,[DateFrom] = @NewDateFrom " _
            & "    ,[DateTo] = @NewDateTo " _
            & "    ,[IDPeriode] = @NewIDPeriode " _
            & "    ,[Amount] = @NewAmount " _
            & "    ,[TPS] = @NewTPS " _
            & "    ,[TVQ] = @NewTVQ " _
            & "WHERE " _
            & "     [IDPaye] = @OldIDPaye " _
            & ""
        Dim updateCommand As New SqlCommand(updateStatement, connection)
        updateCommand.CommandType = CommandType.Text
        updateCommand.Parameters.AddWithValue("@NewPayeNumber", IIf(Not IsNothing(clsPayeNew.PayeNumber), clsPayeNew.PayeNumber, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewPayeDate", IIf(clsPayeNew.PayeDate.HasValue, clsPayeNew.PayeDate, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewIDLivreur", IIf(clsPayeNew.IDLivreur.HasValue, clsPayeNew.IDLivreur, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewLivreur", IIf(Not IsNothing(clsPayeNew.Livreur), clsPayeNew.Livreur, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewDateFrom", IIf(clsPayeNew.DateFrom.HasValue, clsPayeNew.DateFrom, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewDateTo", IIf(clsPayeNew.DateTo.HasValue, clsPayeNew.DateTo, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewIDPeriode", IIf(clsPayeNew.IDPeriode.HasValue, clsPayeNew.IDPeriode, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewAmount", IIf(clsPayeNew.Amount.HasValue, clsPayeNew.Amount, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewTPS", IIf(clsPayeNew.TPS.HasValue, clsPayeNew.TPS, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewTVQ", IIf(clsPayeNew.TVQ.HasValue, clsPayeNew.TVQ, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@OldIDPaye", clsPayeOld.IDPaye)


        Try
            connection.Open()
            Dim count As Integer = updateCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

    Public Function DeleteByPeriodLivreur(ByVal clsPaye As Paye) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim deleteStatement As String _
            = "DELETE FROM " _
            & "     [Paye] " _
            & "WHERE " _
            & "     [IDPeriode] = @OldIDPeriode " _
            & " AND [IDLivreur] = @OldIDLivreur "

        Dim deleteCommand As New SqlCommand(deleteStatement, connection)
        deleteCommand.CommandType = CommandType.Text
        deleteCommand.Parameters.AddWithValue("@OldIDPeriode", clsPaye.IDPeriode)
        deleteCommand.Parameters.AddWithValue("@OldIDLivreur", clsPaye.IDLivreur)

        Try
            connection.Open()
            Dim count As Integer = deleteCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

    Public Function DeleteByPeriod(ByVal clsPaye As Paye) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim deleteStatement As String _
            = "DELETE FROM " _
            & "     [Paye] " _
            & "WHERE " _
            & "     [IDPeriode] = @OldIDPeriode " _
            & ""
        Dim deleteCommand As New SqlCommand(deleteStatement, connection)
        deleteCommand.CommandType = CommandType.Text
        deleteCommand.Parameters.AddWithValue("@OldIDPeriode", clsPaye.IDPeriode)

        Try
            connection.Open()
            Dim count As Integer = deleteCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function
    Public Function Delete(ByVal clsPaye As Paye) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim deleteStatement As String _
            = "DELETE FROM " _
            & "     [Paye] " _
            & "WHERE " _
            & "     [IDPaye] = @OldIDPaye " _
            & ""
        Dim deleteCommand As New SqlCommand(deleteStatement, connection)
        deleteCommand.CommandType = CommandType.Text
        deleteCommand.Parameters.AddWithValue("@OldIDPaye", clsPaye.IDPaye)

        Try
            connection.Open()
            Dim count As Integer = deleteCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

End Class

