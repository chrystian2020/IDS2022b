Imports System.Data.SqlClient

Public Class PretsPaiementsData


    Public Function SelectAllByID(pIDPret As Integer) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = " SELECT dbo.PretsPaiements.IDPretPaiment, dbo.PretsPaiements.IDPret, dbo.PretsPaiements.IDLivreur, dbo.PretsPaiements.Livreur, dbo.PretsPaiements.DatePaiement, dbo.PretsPaiements.Paiement, dbo.Periodes.Periode " _
            & " FROM  dbo.PretsPaiements LEFT OUTER JOIN " _
            & " dbo.Periodes ON dbo.PretsPaiements.Periode = dbo.Periodes.IDPeriode " _
            & " WHERE dbo.PretsPaiements.IDPret = " & pIDPret & "ORDER BY PretsPaiements.IDPret ASC"
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function

    Public Function SelectAllPaymentsCount(pIDPret As Integer, pIDLivreur As Integer) As Integer
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDPretPaiment] " _
            & "    ,[IDPret] " _
            & "    ,[IDLivreur] " _
            & "    ,[Livreur] " _
            & "    ,[DatePaiement] " _
            & "    ,[Paiement] " _
            & "    ,[Periode] " _
            & "FROM " _
            & "     [PretsPaiements] " _
            & " WHERE IDLivreur= " & pIDLivreur & " AND IDPret = " & pIDPret


        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt.Rows.Count
    End Function

    Public Function SelectAllPaymentsForPeriode(pIDPret As Integer, pIDLivreur As Integer, pPeriode As Integer) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDPretPaiment] " _
            & "    ,[IDPret] " _
            & "    ,[IDLivreur] " _
            & "    ,[Livreur] " _
            & "    ,[DatePaiement] " _
            & "    ,[Paiement] " _
            & "    ,[Periode] " _
            & "FROM " _
            & "     [PretsPaiements] " _
            & " WHERE IDLivreur= " & pIDLivreur & " AND IDPret = " & pIDPret & " AND Periode=" & pPeriode


        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function
    Public Function SelectAll() As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDPretPaiment] " _
            & "    ,[IDPret] " _
            & "    ,[IDLivreur] " _
            & "    ,[Livreur] " _
            & "    ,[DatePaiement] " _
            & "    ,[Paiement] " _
            & "    ,[Periode] " _
            & "FROM " _
            & "     [PretsPaiements] " _
            & ""
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function

    Public Function Select_Record(ByVal clsPretsPaiementsPara As PretsPaiements) As PretsPaiements
        Dim clsPretsPaiements As New PretsPaiements
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDPretPaiment] " _
            & "    ,[IDPret] " _
            & "    ,[IDLivreur] " _
            & "    ,[Livreur] " _
            & "    ,[DatePaiement] " _
            & "    ,[Paiement] " _
            & "    ,[Periode] " _
            & "FROM " _
            & "     [PretsPaiements] " _
            & "WHERE " _
            & "     [IDPretPaiment] = @IDPretPaiment " _
            & ""
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        selectCommand.Parameters.AddWithValue("@IDPretPaiment", clsPretsPaiementsPara.IDPretPaiment)
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader(CommandBehavior.SingleRow)
            If reader.Read Then
                With clsPretsPaiements
                    .IDPretPaiment = System.Convert.ToInt32(reader("IDPretPaiment"))
                    .IDPret = System.Convert.ToInt32(reader("IDPret"))
                    .IDLivreur = If(IsDBNull(reader("IDLivreur")), Nothing, CType(reader("IDLivreur"), Int32?))
                    .Livreur = If(IsDBNull(reader("Livreur")), Nothing, reader("Livreur").ToString)
                    .DatePaiement = If(IsDBNull(reader("DatePaiement")), Nothing, CType(reader("DatePaiement"), DateTime?))
                    .Paiement = If(IsDBNull(reader("Paiement")), Nothing, CType(reader("Paiement"), Decimal?))
                    .Periode = If(IsDBNull(reader("Periode")), Nothing, CType(reader("Periode"), Int32?))
                End With
            Else
                clsPretsPaiements = Nothing
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return clsPretsPaiements
    End Function

    Public Function Add(ByVal clsPretsPaiements As PretsPaiements) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim insertStatement As String _
            = "INSERT " _
            & "     [PretsPaiements] " _
            & "     ( " _
            & "     [IDPret] " _
            & "    ,[IDLivreur] " _
            & "    ,[Livreur] " _
            & "    ,[DatePaiement] " _
            & "    ,[Paiement] " _
            & "    ,[Periode] " _
            & "     ) " _
            & "VALUES " _
            & "     ( " _
            & "     @IDPret " _
            & "    ,@IDLivreur " _
            & "    ,@Livreur " _
            & "    ,@DatePaiement " _
            & "    ,@Paiement " _
            & "    ,@Periode " _
            & "     ) " _
            & ""
        Dim insertCommand As New SqlCommand(insertStatement, connection)
        insertCommand.CommandType = CommandType.Text
        insertCommand.Parameters.AddWithValue("@IDPret", clsPretsPaiements.IDPret)
        insertCommand.Parameters.AddWithValue("@IDLivreur", IIf(clsPretsPaiements.IDLivreur.HasValue, clsPretsPaiements.IDLivreur, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Livreur", IIf(Not IsNothing(clsPretsPaiements.Livreur), clsPretsPaiements.Livreur, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@DatePaiement", IIf(clsPretsPaiements.DatePaiement.HasValue, clsPretsPaiements.DatePaiement, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Paiement", IIf(clsPretsPaiements.Paiement.HasValue, clsPretsPaiements.Paiement, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Periode", IIf(clsPretsPaiements.Periode.HasValue, clsPretsPaiements.Periode, DBNull.Value))
        Try
            connection.Open()
            Dim count As Integer = insertCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

    Public Function Update(ByVal clsPretsPaiementsOld As PretsPaiements,
           ByVal clsPretsPaiementsNew As PretsPaiements) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim updateStatement As String _
            = "UPDATE " _
            & "     [PretsPaiements] " _
            & "SET " _
            & "     [IDPret] = @NewIDPret " _
            & "    ,[IDLivreur] = @NewIDLivreur " _
            & "    ,[Livreur] = @NewLivreur " _
            & "    ,[DatePaiement] = @NewDatePaiement " _
            & "    ,[Paiement] = @NewPaiement " _
            & "    ,[Periode] = @NewPeriode " _
            & "WHERE " _
            & "     [IDPretPaiment] = @OldIDPretPaiment " _
            & ""
        Dim updateCommand As New SqlCommand(updateStatement, connection)
        updateCommand.CommandType = CommandType.Text
        updateCommand.Parameters.AddWithValue("@NewIDPret", clsPretsPaiementsNew.IDPret)
        updateCommand.Parameters.AddWithValue("@NewIDLivreur", IIf(clsPretsPaiementsNew.IDLivreur.HasValue, clsPretsPaiementsNew.IDLivreur, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewLivreur", IIf(Not IsNothing(clsPretsPaiementsNew.Livreur), clsPretsPaiementsNew.Livreur, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewDatePaiement", IIf(clsPretsPaiementsNew.DatePaiement.HasValue, clsPretsPaiementsNew.DatePaiement, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewPaiement", IIf(clsPretsPaiementsNew.Paiement.HasValue, clsPretsPaiementsNew.Paiement, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewPeriode", IIf(clsPretsPaiementsNew.Periode.HasValue, clsPretsPaiementsNew.Periode, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@OldIDPretPaiment", clsPretsPaiementsOld.IDPretPaiment)

        Try
            connection.Open()
            Dim count As Integer = updateCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

    Public Function Delete(ByVal clsPretsPaiements As PretsPaiements) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim deleteStatement As String _
            = "DELETE FROM " _
            & "     [PretsPaiements] " _
            & "WHERE " _
            & "     [IDPretPaiment] = @OldIDPretPaiment " _
            & ""
        Dim deleteCommand As New SqlCommand(deleteStatement, connection)
        deleteCommand.CommandType = CommandType.Text
        deleteCommand.Parameters.AddWithValue("@OldIDPretPaiment", clsPretsPaiements.IDPretPaiment)

        Try
            connection.Open()
            Dim count As Integer = deleteCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function
    Public Function DeleteByID(PIDPret As Integer) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim deleteStatement As String _
            = "DELETE FROM " _
            & "     [PretsPaiements] " _
            & " WHERE IDPret =" & PIDPret
        Dim deleteCommand As New SqlCommand(deleteStatement, connection)
        deleteCommand.CommandType = CommandType.Text


        Try
            connection.Open()
            Dim count As Integer = deleteCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function
End Class
 
