Imports System.Data.SqlClient

Public Class VillesData

    Public Function SelectAll() As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [VilleID] " _
            & "    ,[Ville] " _
            & "FROM " _
            & "     [Villes] " _
            & " ORDER BY Ville ASC"
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function

    Public Function Select_Record(ByVal clsVillesPara As Villes) As Villes
        Dim clsVilles As New Villes
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [VilleID] " _
            & "    ,[Ville] " _
            & "FROM " _
            & "     [Villes] " _
            & "WHERE " _
            & "    [VilleID] = @VilleID " _
            & ""
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        selectCommand.Parameters.AddWithValue("@VilleID", clsVillesPara.VilleID)
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader(CommandBehavior.SingleRow)
            If reader.Read Then
                With clsVilles
                    .VilleID = System.Convert.ToInt32(reader("VilleID"))
                    .Ville = If(IsDBNull(reader("Ville")), Nothing, reader("Ville").ToString)
                End With
            Else
                clsVilles = Nothing
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return clsVilles
    End Function

    Public Function Add(ByVal clsVilles As Villes) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim insertStatement As String _
            = "INSERT " _
            & "     [Villes] " _
            & "     ( " _
            & "     [VilleID] " _
            & "    ,[Ville] " _
            & "     ) " _
            & "VALUES " _
            & "     ( " _
            & "     @VilleID " _
            & "    ,@Ville " _
            & "     ) " _
            & ""
        Dim insertCommand As New SqlCommand(insertStatement, connection)
        insertCommand.CommandType = CommandType.Text
        insertCommand.Parameters.AddWithValue("@VilleID", clsVilles.VilleID)
        insertCommand.Parameters.AddWithValue("@Ville", IIf(Not IsNothing(clsVilles.Ville), clsVilles.Ville, DBNull.Value))
        Try
            connection.Open()
            Dim count As Integer = insertCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

    Public Function Update(ByVal clsVillesOld As Villes,
           ByVal clsVillesNew As Villes) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim updateStatement As String _
            = "UPDATE " _
            & "     [Villes] " _
            & "SET " _
            & "     [VilleID] = @NewVilleID " _
            & "    ,[Ville] = @NewVille " _
            & "WHERE " _
            & "     [VilleID] = @OldVilleID " _
            & " AND ((@OldVille IS NULL AND [Ville] IS NULL) OR [Ville] = @OldVille) " _
            & ""
        Dim updateCommand As New SqlCommand(updateStatement, connection)
        updateCommand.CommandType = CommandType.Text
        updateCommand.Parameters.AddWithValue("@NewVilleID", clsVillesNew.VilleID)
        updateCommand.Parameters.AddWithValue("@NewVille", IIf(Not IsNothing(clsVillesNew.Ville), clsVillesNew.Ville, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@OldVilleID", clsVillesOld.VilleID)
        updateCommand.Parameters.AddWithValue("@OldVille", IIf(Not IsNothing(clsVillesOld.Ville), clsVillesOld.Ville, DBNull.Value))
        Try
            connection.Open()
            Dim count As Integer = updateCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

    Public Function Delete(ByVal clsVilles As Villes) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim deleteStatement As String _
            = "DELETE FROM " _  
            & "     [Villes] " _
            & "WHERE " _
            & "     [VilleID] = @OldVilleID " _
            & " AND ((@OldVille IS NULL AND [Ville] IS NULL) OR [Ville] = @OldVille) " _
            & ""
        Dim deleteCommand As New SqlCommand(deleteStatement, connection)
        deleteCommand.CommandType = CommandType.Text
        deleteCommand.Parameters.AddWithValue("@OldVilleID", clsVilles.VilleID)
        deleteCommand.Parameters.AddWithValue("@OldVille", IIf(Not IsNothing(clsVilles.Ville), clsVilles.Ville, DBNull.Value))
        Try
            connection.Open()
            Dim count As Integer = deleteCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

End Class
 
