Imports System.Data.SqlClient

Public Class VentesData


    Public Function SelectAllByDateSum(pdDateFrom As DateTime, pdDateTo As DateTime) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "    SUM(Montant) as TotalAmount " _
            & "    ,SUM(TPS) as TotalTPS" _
            & "    ,SUM(TVQ) as TotalTVQ " _
            & "    ,OrganisationID " _
            & "    ,Organisation " _
            & "    ,DateVente " _
            & "    ,DateFrom " _
            & "    ,DateTo " _
            & " FROM " _
            & "     [Ventes] " _
            & " WHERE (DateVente  >= '" & pdDateFrom & "' AND DateVente <= '" & pdDateTo & "')"

        'If go_Globals.IDPeriode > 0 Then
        '    selectStatement = selectStatement & " And IDPeriode = " & go_Globals.IDPeriode
        'End If

        selectStatement = selectStatement & " GROUP BY OrganisationID,Organisation,DateVente,DateFrom,DateTo "
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function



    Public Function SelectAllByDate(pdDateFrom As DateTime, pdDateTo As DateTime) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDVente] " _
            & "    ,[DateVente] " _
            & "    ,[DateFrom] " _
            & "    ,[DateTo] " _
            & "    ,[IDPeriode] " _
            & "    ,[OrganisationID] " _
            & "    ,[Organisation] " _
            & "    ,[Description] " _
            & "    ,[Unite] " _
            & "    ,[Prix] " _
            & "    ,[Montant] " _
            & "    ,[MontantTotal] " _
            & "FROM " _
            & "     [Ventes] " _
            & " WHERE (DateFrom  >= '" & pdDateFrom & "' AND DateTo <= '" & pdDateTo & "')"


        'If go_Globals.IDPeriode > 0 Then
        '    selectStatement = selectStatement & " AND IDPeriode = " & go_Globals.IDPeriode
        'End If


        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function

    Public Function SelectAll() As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDVente] " _
            & "    ,[DateVente] " _
            & "    ,[DateFrom] " _
            & "    ,[DateTo] " _
            & "    ,[IDPeriode] " _
            & "    ,[OrganisationID] " _
            & "    ,[Organisation] " _
            & "    ,[Description] " _
            & "    ,[Unite] " _
            & "    ,[Prix] " _
            & "    ,[Montant] " _
            & "FROM " _
            & "     [Ventes] " _
            & ""
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function

    Public Function Select_Record(ByVal clsVentesPara As Ventes) As Ventes
        Dim clsVentes As New Ventes
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDVente] " _
            & "    ,[DateVente] " _
            & "    ,[DateFrom] " _
            & "    ,[DateTo] " _
            & "    ,[IDPeriode] " _
            & "    ,[OrganisationID] " _
            & "    ,[Organisation] " _
            & "    ,[Description] " _
            & "    ,[Unite] " _
            & "    ,[Prix] " _
            & "    ,[Montant] " _
            & "FROM " _
            & "     [Ventes] " _
            & "WHERE " _
            & "     [IDVente] = @IDVente " _
            & ""
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        selectCommand.Parameters.AddWithValue("@IDVente", clsVentesPara.IDVente)
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader(CommandBehavior.SingleRow)
            If reader.Read Then
                With clsVentes
                    .IDVente = System.Convert.ToInt32(reader("IDVente"))
                    .DateVente = If(IsDBNull(reader("DateVente")), Nothing, CType(reader("DateVente"), Date?))
                    .DateFrom = If(IsDBNull(reader("DateFrom")), Nothing, CType(reader("DateFrom"), DateTime?))
                    .DateTo = If(IsDBNull(reader("DateTo")), Nothing, CType(reader("DateTo"), DateTime?))
                    .IDPeriode = If(IsDBNull(reader("IDPeriode")), Nothing, CType(reader("IDPeriode"), Int32?))
                    .OrganisationID = If(IsDBNull(reader("OrganisationID")), Nothing, CType(reader("OrganisationID"), Int32?))
                    .Organisation = If(IsDBNull(reader("Organisation")), Nothing, reader("Organisation").ToString)
                    .Description = If(IsDBNull(reader("Description")), Nothing, reader("Description").ToString)
                    .Unite = If(IsDBNull(reader("Unite")), Nothing, CType(CType(reader("Unite"), Double?), Decimal?))

                    .Montant = If(IsDBNull(reader("Montant")), Nothing, CType(reader("Montant"), Decimal?))

                End With
            Else
                clsVentes = Nothing
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return clsVentes
    End Function


    Public Function Add(ByVal clsVentes As Ventes) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim insertStatement As String _
            = "INSERT " _
            & "     [Ventes] " _
            & "     ( " _
            & "     [DateVente] " _
            & "    ,[DateFrom] " _
            & "    ,[DateTo] " _
            & "    ,[IDPeriode] " _
            & "    ,[OrganisationID] " _
            & "    ,[Organisation] " _
            & "    ,[Description] " _
            & "    ,[Unite] " _
            & "    ,[Prix] " _
            & "    ,[Montant] " _
            & "     ) " _
            & "VALUES " _
            & "     ( " _
            & "     @DateVente " _
            & "    ,@DateFrom " _
            & "    ,@DateTo " _
            & "    ,@IDPeriode " _
            & "    ,@OrganisationID " _
            & "    ,@Organisation " _
            & "    ,@Description " _
            & "    ,@Unite " _
            & "    ,@Prix " _
            & "    ,@Montant " _
            & "     ) " _
            & ""
        Dim insertCommand As New SqlCommand(insertStatement, connection)
        insertCommand.CommandType = CommandType.Text
        insertCommand.Parameters.AddWithValue("@DateVente", IIf(clsVentes.DateVente.HasValue, clsVentes.DateVente, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@DateFrom", IIf(clsVentes.DateFrom.HasValue, clsVentes.DateFrom, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@DateTo", IIf(clsVentes.DateTo.HasValue, clsVentes.DateTo, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@IDPeriode", IIf(clsVentes.IDPeriode.HasValue, clsVentes.IDPeriode, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@OrganisationID", IIf(clsVentes.OrganisationID.HasValue, clsVentes.OrganisationID, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Organisation", IIf(Not IsNothing(clsVentes.Organisation), clsVentes.Organisation, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Description", IIf(Not IsNothing(clsVentes.Description), clsVentes.Description, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Unite", IIf(clsVentes.Unite.HasValue, clsVentes.Unite, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Prix", IIf(clsVentes.Montant.HasValue, clsVentes.Montant, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Montant", IIf(clsVentes.Montant.HasValue, clsVentes.Montant, DBNull.Value))

        Try
            connection.Open()
            Dim count As Integer = insertCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function


    Public Function Update(ByVal clsVentesOld As Ventes,
           ByVal clsVentesNew As Ventes) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim updateStatement As String _
            = "UPDATE " _
            & "     [Ventes] " _
            & "SET " _
            & "     [DateVente] = @NewDateVente " _
            & "    ,[DateFrom] = @NewDateFrom " _
            & "    ,[DateTo] = @NewDateTo " _
            & "    ,[IDPeriode] = @NewIDPeriode " _
            & "    ,[OrganisationID] = @NewOrganisationID " _
            & "    ,[Organisation] = @NewOrganisation " _
            & "    ,[Description] = @NewDescription " _
            & "    ,[Unite] = @NewUnite " _
            & "    ,[Prix] = @NewPrix " _
            & "    ,[Montant] = @NewMontant " _
            & "WHERE " _
            & "     [IDVente] = @OldIDVente " _
            & ""
        Dim updateCommand As New SqlCommand(updateStatement, connection)
        updateCommand.CommandType = CommandType.Text
        updateCommand.Parameters.AddWithValue("@NewDateVente", IIf(clsVentesNew.DateVente.HasValue, clsVentesNew.DateVente, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewDateFrom", IIf(clsVentesNew.DateFrom.HasValue, clsVentesNew.DateFrom, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewDateTo", IIf(clsVentesNew.DateTo.HasValue, clsVentesNew.DateTo, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewIDPeriode", IIf(clsVentesNew.IDPeriode.HasValue, clsVentesNew.IDPeriode, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewOrganisationID", IIf(clsVentesNew.OrganisationID.HasValue, clsVentesNew.OrganisationID, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewOrganisation", IIf(Not IsNothing(clsVentesNew.Organisation), clsVentesNew.Organisation, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewDescription", IIf(Not IsNothing(clsVentesNew.Description), clsVentesNew.Description, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewUnite", IIf(clsVentesNew.Unite.HasValue, clsVentesNew.Unite, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewPrix", IIf(clsVentesNew.Montant.HasValue, clsVentesNew.Montant, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewMontant", IIf(clsVentesNew.Montant.HasValue, clsVentesNew.Montant, DBNull.Value))

        updateCommand.Parameters.AddWithValue("@OldIDVente", clsVentesOld.IDVente)

        Try
            connection.Open()
            Dim count As Integer = updateCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

    Public Function Delete(ByVal clsVentes As Ventes) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim deleteStatement As String _
            = "DELETE FROM " _
            & "     [Ventes] " _
            & "WHERE " _
            & "     [IDVente] = @OldIDVente " _
            & ""
        Dim deleteCommand As New SqlCommand(deleteStatement, connection)
        deleteCommand.CommandType = CommandType.Text
        deleteCommand.Parameters.AddWithValue("@OldIDVente", clsVentes.IDVente)

        Try
            connection.Open()
            Dim count As Integer = deleteCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

End Class
 
