Imports System.Data.SqlClient

Public Class PayeLineData

    Public Function SelectAllGlacier(piIDPaye As Integer) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDPayeLine] " _
            & "    ,[IDPaye] " _
            & "    ,[PayeDate] " _
            & "    ,[PayeNumber] " _
            & "    ,[IDLivreur] " _
            & "    ,[Livreur] " _
            & "    ,[OrganisationID] " _
            & "    ,[Organisation] " _
            & "    ,[Code] " _
            & "    ,[Km] " _
            & "    ,[Description] " _
            & "    ,[Unite] " _
            & "    ,[Prix] " _
            & "    ,[Montant] " _
            & "    ,[Credit] " _
            & "    ,[DateFrom] " _
            & "    ,[DateTo] " _
            & "    ,[NombreGlacier] " _
            & "    ,[Glacier] " _
            & "    ,[IDPeriode] " _
            & "    ,[TypeAjout] " _
            & "    ,[Ajout] " _
            & "FROM " _
            & "     [PayeLine] " _
            & " WHERE IDPaye =" & piIDPaye & "AND TypeAjout = 1"
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function


    Public Function SelectAllByID(piIDPaye As Integer) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDPayeLine] " _
            & "    ,[IDPaye] " _
            & "    ,[PayeDate] " _
            & "    ,[PayeNumber] " _
            & "    ,[IDLivreur] " _
            & "    ,[Livreur] " _
            & "    ,[OrganisationID] " _
            & "    ,[Organisation] " _
            & "    ,[Code] " _
            & "    ,[Km] " _
            & "    ,[Description] " _
            & "    ,[Unite] " _
            & "    ,[Prix] " _
            & "    ,[Montant] " _
            & "    ,[Credit] " _
            & "    ,[DateFrom] " _
            & "    ,[DateTo] " _
            & "    ,[NombreGlacier] " _
            & "    ,[Glacier] " _
            & "    ,[IDPeriode] " _
            & "    ,[TypeAjout] " _
            & "    ,[Ajout] " _
            & "FROM " _
            & "     [PayeLine] " _
            & " WHERE IDPaye =" & piIDPaye
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function
    Public Function SelectAllByDateCommission(piLivreurID As Integer) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDPayeLine] " _
            & "    ,[IDPaye] " _
            & "    ,[PayeDate] " _
            & "    ,[PayeNumber] " _
            & "    ,[IDLivreur] " _
            & "    ,[Livreur] " _
            & "    ,[OrganisationID] " _
            & "    ,[Organisation] " _
            & "    ,[Code] " _
            & "    ,[Km] " _
            & "    ,[Description] " _
            & "    ,[Unite] " _
            & "    ,[Prix] " _
            & "    ,[Montant] " _
            & "    ,[Credit] " _
            & "    ,[DateFrom] " _
            & "    ,[DateTo] " _
            & "    ,[NombreGlacier] " _
            & "    ,[Glacier] " _
            & "    ,[IDPeriode] " _
            & "    ,[TypeAjout] " _
            & "    ,[Ajout] " _
            & "FROM " _
            & "     [PayeLine] " _
            & " WHERE  IDLivreur = " & piLivreurID & " And IDPeriode = " & go_Globals.IDPeriode
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function
    Public Function SelectAllByDate(piLivreurID As Integer) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDPayeLine] " _
            & "    ,[IDPaye] " _
            & "    ,[PayeDate] " _
            & "    ,[PayeNumber] " _
            & "    ,[IDLivreur] " _
            & "    ,[Livreur] " _
            & "    ,[OrganisationID] " _
            & "    ,[Organisation] " _
            & "    ,[Code] " _
            & "    ,[Km] " _
            & "    ,[Description] " _
            & "    ,[Unite] " _
            & "    ,[Prix] " _
            & "    ,[Montant] " _
            & "    ,[Credit] " _
            & "    ,[DateFrom] " _
            & "    ,[DateTo] " _
            & "    ,[NombreGlacier] " _
            & "    ,[Glacier] " _
            & "    ,[IDPeriode] " _
            & "    ,[TypeAjout] " _
            & "    ,[Ajout] " _
            & "FROM " _
            & "     [PayeLine] " _
            & " WHERE IDLivreur = " & piLivreurID & " And IDPeriode = " & go_Globals.IDPeriode
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function

    Public Function SelectAll() As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDPayeLine] " _
            & "    ,[IDPaye] " _
            & "    ,[PayeDate] " _
            & "    ,[PayeNumber] " _
            & "    ,[IDLivreur] " _
            & "    ,[Livreur] " _
            & "    ,[OrganisationID] " _
            & "    ,[Organisation] " _
            & "    ,[Code] " _
            & "    ,[Km] " _
            & "    ,[Description] " _
            & "    ,[Unite] " _
            & "    ,[Prix] " _
            & "    ,[Montant] " _
            & "    ,[Credit] " _
            & "    ,[DateFrom] " _
            & "    ,[DateTo] " _
            & "    ,[NombreGlacier] " _
            & "    ,[Glacier] " _
            & "    ,[IDPeriode] " _
            & "    ,[TypeAjout] " _
            & "    ,[Ajout] " _
            & "FROM " _
            & "     [PayeLine] " _
            & ""
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function

    Public Function Select_Record(ByVal clsPayeLinePara As PayeLine) As PayeLine
        Dim clsPayeLine As New PayeLine
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDPayeLine] " _
            & "    ,[IDPaye] " _
            & "    ,[PayeDate] " _
            & "    ,[PayeNumber] " _
            & "    ,[IDLivreur] " _
            & "    ,[Livreur] " _
            & "    ,[OrganisationID] " _
            & "    ,[Organisation] " _
            & "    ,[Code] " _
            & "    ,[Km] " _
            & "    ,[Description] " _
            & "    ,[Unite] " _
            & "    ,[Prix] " _
            & "    ,[Montant] " _
            & "    ,[Credit] " _
            & "    ,[DateFrom] " _
            & "    ,[DateTo] " _
            & "    ,[NombreGlacier] " _
            & "    ,[Glacier] " _
            & "    ,[IDPeriode] " _
            & "    ,[TypeAjout] " _
            & "    ,[Ajout] " _
            & "FROM " _
            & "     [PayeLine] " _
            & "WHERE " _
            & "     [IDPayeLine] = @IDPayeLine " _
            & ""
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        selectCommand.Parameters.AddWithValue("@IDPayeLine", clsPayeLinePara.IDPayeLine)
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader(CommandBehavior.SingleRow)
            If reader.Read Then
                With clsPayeLine
                    .IDPayeLine = System.Convert.ToInt32(reader("IDPayeLine"))
                    .IDPaye = If(IsDBNull(reader("IDPaye")), Nothing, CType(reader("IDPaye"), Int32?))
                    .PayeDate = If(IsDBNull(reader("PayeDate")), Nothing, CType(reader("PayeDate"), DateTime?))
                    .PayeNumber = If(IsDBNull(reader("PayeNumber")), Nothing, reader("PayeNumber").ToString)
                    .IDLivreur = If(IsDBNull(reader("IDLivreur")), Nothing, CType(reader("IDLivreur"), Int32?))
                    .Livreur = If(IsDBNull(reader("Livreur")), Nothing, reader("Livreur").ToString)
                    .OrganisationID = If(IsDBNull(reader("OrganisationID")), Nothing, CType(reader("OrganisationID"), Int32?))
                    .Organisation = If(IsDBNull(reader("Organisation")), Nothing, reader("Organisation").ToString)
                    .Code = If(IsDBNull(reader("Code")), Nothing, reader("Code").ToString)
                    .Km = If(IsDBNull(reader("Km")), Nothing, CType(CType(reader("Km"), Double?), Decimal?))
                    .Description = If(IsDBNull(reader("Description")), Nothing, reader("Description").ToString)
                    .Unite = If(IsDBNull(reader("Unite")), Nothing, CType(CType(reader("Unite"), Double?), Decimal?))
                    .Prix = If(IsDBNull(reader("Prix")), Nothing, CType(reader("Prix"), Decimal?))
                    .Montant = If(IsDBNull(reader("Montant")), Nothing, CType(reader("Montant"), Decimal?))
                    .Credit = If(IsDBNull(reader("Credit")), Nothing, CType(reader("Credit"), Decimal?))
                    .DateFrom = If(IsDBNull(reader("DateFrom")), Nothing, CType(reader("DateFrom"), DateTime?))
                    .DateTo = If(IsDBNull(reader("DateTo")), Nothing, CType(reader("DateTo"), DateTime?))
                    .NombreGlacier = If(IsDBNull(reader("NombreGlacier")), Nothing, CType(reader("NombreGlacier"), Int32?))
                    .Glacier = If(IsDBNull(reader("Glacier")), Nothing, CType(reader("Glacier"), Decimal?))
                    .IDPeriode = If(IsDBNull(reader("IDPeriode")), Nothing, CType(reader("IDPeriode"), Int32?))
                    .TypeAjout = If(IsDBNull(reader("TypeAjout")), Nothing, CType(reader("TypeAjout"), Int32?))
                    .Ajout = If(IsDBNull(reader("Ajout")), Nothing, CType(reader("Ajout"), Boolean?))
                End With
            Else
                clsPayeLine = Nothing
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return clsPayeLine
    End Function


    Public Function Add(ByVal clsPayeLine As PayeLine) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim insertStatement As String _
            = "INSERT " _
            & "     [PayeLine] " _
            & "     ( " _
            & "     [IDPaye] " _
            & "    ,[PayeDate] " _
            & "    ,[PayeNumber] " _
            & "    ,[IDLivreur] " _
            & "    ,[Livreur] " _
            & "    ,[OrganisationID] " _
            & "    ,[Organisation] " _
            & "    ,[Code] " _
            & "    ,[Km] " _
            & "    ,[Description] " _
            & "    ,[Unite] " _
            & "    ,[Prix] " _
            & "    ,[Montant] " _
            & "    ,[Credit] " _
            & "    ,[DateFrom] " _
            & "    ,[DateTo] " _
            & "    ,[NombreGlacier] " _
            & "    ,[Glacier] " _
            & "    ,[IDPeriode] " _
            & "    ,[TypeAjout] " _
            & "    ,[Ajout] " _
            & "    ,[IsCommissionVendeur] " _
            & "     ) " _
            & "VALUES " _
            & "     ( " _
            & "     @IDPaye " _
            & "    ,@PayeDate " _
            & "    ,@PayeNumber " _
            & "    ,@IDLivreur " _
            & "    ,@Livreur " _
            & "    ,@OrganisationID " _
            & "    ,@Organisation " _
            & "    ,@Code " _
            & "    ,@Km " _
            & "    ,@Description " _
            & "    ,@Unite " _
            & "    ,@Prix " _
            & "    ,@Montant " _
            & "    ,@Credit " _
            & "    ,@DateFrom " _
            & "    ,@DateTo " _
            & "    ,@NombreGlacier " _
            & "    ,@Glacier " _
            & "    ,@IDPeriode " _
            & "    ,@TypeAjout " _
            & "    ,@Ajout " _
            & "    ,@IsCommissionVendeur " _
            & "     ) " _
            & ""
        Dim insertCommand As New SqlCommand(insertStatement, connection)
        insertCommand.CommandType = CommandType.Text
        insertCommand.Parameters.AddWithValue("@IDPaye", IIf(clsPayeLine.IDPaye.HasValue, clsPayeLine.IDPaye, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@PayeDate", IIf(clsPayeLine.PayeDate.HasValue, clsPayeLine.PayeDate, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@PayeNumber", IIf(Not IsNothing(clsPayeLine.PayeNumber), clsPayeLine.PayeNumber, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@IDLivreur", IIf(clsPayeLine.IDLivreur.HasValue, clsPayeLine.IDLivreur, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Livreur", IIf(Not IsNothing(clsPayeLine.Livreur), clsPayeLine.Livreur, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@OrganisationID", IIf(clsPayeLine.OrganisationID.HasValue, clsPayeLine.OrganisationID, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Organisation", IIf(Not IsNothing(clsPayeLine.Organisation), clsPayeLine.Organisation, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Code", IIf(Not IsNothing(clsPayeLine.Code), clsPayeLine.Code, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Km", IIf(clsPayeLine.Km.HasValue, clsPayeLine.Km, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Description", IIf(Not IsNothing(clsPayeLine.Description), clsPayeLine.Description, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Unite", IIf(clsPayeLine.Unite.HasValue, clsPayeLine.Unite, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Prix", IIf(clsPayeLine.Prix.HasValue, clsPayeLine.Prix, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Montant", IIf(clsPayeLine.Montant.HasValue, clsPayeLine.Montant, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Credit", IIf(clsPayeLine.Credit.HasValue, clsPayeLine.Credit, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@DateFrom", IIf(clsPayeLine.DateFrom.HasValue, clsPayeLine.DateFrom, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@DateTo", IIf(clsPayeLine.DateTo.HasValue, clsPayeLine.DateTo, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@NombreGlacier", IIf(clsPayeLine.NombreGlacier.HasValue, clsPayeLine.NombreGlacier, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Glacier", IIf(clsPayeLine.Glacier.HasValue, clsPayeLine.Glacier, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@IDPeriode", IIf(clsPayeLine.IDPeriode.HasValue, clsPayeLine.IDPeriode, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@TypeAjout", IIf(clsPayeLine.TypeAjout.HasValue, clsPayeLine.TypeAjout, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Ajout", IIf(clsPayeLine.Ajout.HasValue, clsPayeLine.Ajout, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@IsCommissionVendeur", IIf(clsPayeLine.IsCommissionVendeur.HasValue, clsPayeLine.IsCommissionVendeur, DBNull.Value))
        Try
            connection.Open()
            Dim count As Integer = insertCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function


    Public Function UpdateGlacier(ByVal clsPayeLineOld As PayeLine,
           ByVal clsPayeLineNew As PayeLine) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim updateStatement As String _
            = "UPDATE " _
            & "     [PayeLine] " _
            & "SET " _
            & "    [Unite] = @NewNombreGlacier " _
            & "    ,[Prix] = @NewPrix " _
            & "    ,[Montant] = @NewMontant " _
            & "WHERE " _
            & "     [IDPayeLine] = @OldIDPayeLine " _
            & ""
        Dim updateCommand As New SqlCommand(updateStatement, connection)
        updateCommand.CommandType = CommandType.Text
        updateCommand.Parameters.AddWithValue("@NewNombreGlacier", IIf(clsPayeLineNew.Unite.HasValue, clsPayeLineNew.Unite, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewPrix", IIf(clsPayeLineNew.Prix.HasValue, clsPayeLineNew.Prix, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewMontant", IIf(clsPayeLineNew.Montant.HasValue, clsPayeLineNew.Montant, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@OldIDPayeLine", clsPayeLineOld.IDPayeLine)

        Try
            connection.Open()
            Dim count As Integer = updateCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function
    Public Function Update(ByVal clsPayeLineOld As PayeLine,
           ByVal clsPayeLineNew As PayeLine) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim updateStatement As String _
            = "UPDATE " _
            & "     [PayeLine] " _
            & "SET " _
            & "     [IDPaye] = @NewIDPaye " _
            & "    ,[PayeDate] = @NewPayeDate " _
            & "    ,[PayeNumber] = @NewPayeNumber " _
            & "    ,[IDLivreur] = @NewIDLivreur " _
            & "    ,[Livreur] = @NewLivreur " _
            & "    ,[OrganisationID] = @NewOrganisationID " _
            & "    ,[Organisation] = @NewOrganisation " _
            & "    ,[Code] = @NewCode " _
            & "    ,[Km] = @NewKm " _
            & "    ,[Description] = @NewDescription " _
            & "    ,[Unite] = @NewUnite " _
            & "    ,[Prix] = @NewPrix " _
            & "    ,[Montant] = @NewMontant " _
            & "    ,[Credit] = @NewCredit " _
            & "    ,[DateFrom] = @NewDateFrom " _
            & "    ,[DateTo] = @NewDateTo " _
            & "    ,[NombreGlacier] = @NewNombreGlacier " _
            & "    ,[Glacier] = @NewGlacier " _
            & "    ,[IDPeriode] = @NewIDPeriode " _
            & "    ,[TypeAjout] = @NewTypeAjout " _
            & "    ,[Ajout] = @NewAjout " _
            & "WHERE " _
            & "     [IDPayeLine] = @OldIDPayeLine " _
            & ""
        Dim updateCommand As New SqlCommand(updateStatement, connection)
        updateCommand.CommandType = CommandType.Text
        updateCommand.Parameters.AddWithValue("@NewIDPaye", IIf(clsPayeLineNew.IDPaye.HasValue, clsPayeLineNew.IDPaye, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewPayeDate", IIf(clsPayeLineNew.PayeDate.HasValue, clsPayeLineNew.PayeDate, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewPayeNumber", IIf(Not IsNothing(clsPayeLineNew.PayeNumber), clsPayeLineNew.PayeNumber, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewIDLivreur", IIf(clsPayeLineNew.IDLivreur.HasValue, clsPayeLineNew.IDLivreur, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewLivreur", IIf(Not IsNothing(clsPayeLineNew.Livreur), clsPayeLineNew.Livreur, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewOrganisationID", IIf(clsPayeLineNew.OrganisationID.HasValue, clsPayeLineNew.OrganisationID, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewOrganisation", IIf(Not IsNothing(clsPayeLineNew.Organisation), clsPayeLineNew.Organisation, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewCode", IIf(Not IsNothing(clsPayeLineNew.Code), clsPayeLineNew.Code, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewKm", IIf(clsPayeLineNew.Km.HasValue, clsPayeLineNew.Km, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewDescription", IIf(Not IsNothing(clsPayeLineNew.Description), clsPayeLineNew.Description, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewUnite", IIf(clsPayeLineNew.Unite.HasValue, clsPayeLineNew.Unite, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewPrix", IIf(clsPayeLineNew.Prix.HasValue, clsPayeLineNew.Prix, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewMontant", IIf(clsPayeLineNew.Montant.HasValue, clsPayeLineNew.Montant, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewCredit", IIf(clsPayeLineNew.Credit.HasValue, clsPayeLineNew.Credit, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewDateFrom", IIf(clsPayeLineNew.DateFrom.HasValue, clsPayeLineNew.DateFrom, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewDateTo", IIf(clsPayeLineNew.DateTo.HasValue, clsPayeLineNew.DateTo, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewNombreGlacier", IIf(clsPayeLineNew.NombreGlacier.HasValue, clsPayeLineNew.NombreGlacier, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewGlacier", IIf(clsPayeLineNew.Glacier.HasValue, clsPayeLineNew.Glacier, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewIDPeriode", IIf(clsPayeLineNew.IDPeriode.HasValue, clsPayeLineNew.IDPeriode, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewTypeAjout", IIf(clsPayeLineNew.TypeAjout.HasValue, clsPayeLineNew.TypeAjout, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewAjout", IIf(clsPayeLineNew.Ajout.HasValue, clsPayeLineNew.Ajout, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@OldIDPayeLine", clsPayeLineOld.IDPayeLine)

        Try
            connection.Open()
            Dim count As Integer = updateCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

    Public Function DeleteByPeriodLivreurCommission(ByVal clsCommissionLine As CommissionLine) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim deleteStatement As String _
            = " DELETE FROM " _
            & "     [PayeLine] " _
            & " WHERE " _
            & " IDPeriode = @OldIDPeriode " _
            & " AND IDLivreur = @OldIDLivreur AND Ajout <> 1"

        Dim deleteCommand As New SqlCommand(deleteStatement, connection)
        deleteCommand.CommandType = CommandType.Text
        deleteCommand.Parameters.AddWithValue("@OldIDPeriode", clsCommissionLine.IDPeriode)
        deleteCommand.Parameters.AddWithValue("@OldIDLivreur", clsCommissionLine.IDLivreur)

        Try
            connection.Open()
            Dim count As Integer = deleteCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function


    Public Function DeleteByPeriodLivreur(ByVal clsPayeLine As PayeLine) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim deleteStatement As String _
            = "DELETE FROM " _
            & "     [PayeLine] " _
            & "WHERE " _
            & "     [IDPeriode] = @OldIDPeriode " _
            & " AND [IDLivreur] = @OldIDLivreur AND Ajout <> 1"

        Dim deleteCommand As New SqlCommand(deleteStatement, connection)
        deleteCommand.CommandType = CommandType.Text
        deleteCommand.Parameters.AddWithValue("@OldIDPeriode", clsPayeLine.IDPeriode)
        deleteCommand.Parameters.AddWithValue("@OldIDLivreur", clsPayeLine.IDLivreur)

        Try
            connection.Open()
            Dim count As Integer = deleteCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

    Public Function DeleteByPeriodCommission(ByVal clsPayeLine As PayeLine) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim deleteStatement As String _
            = "DELETE FROM " _
            & "     [PayeLine] " _
            & "WHERE " _
            & "     [IDPeriode] = @OldIDPeriode AND Ajout <> 1 AND IsCommissionVendeur = 1"

        Dim deleteCommand As New SqlCommand(deleteStatement, connection)
        deleteCommand.CommandType = CommandType.Text
        deleteCommand.Parameters.AddWithValue("@OldIDPeriode", clsPayeLine.IDPeriode)

        Try
            connection.Open()
            Dim count As Integer = deleteCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

    Public Function DeleteByPeriod(ByVal clsPayeLine As PayeLine) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim deleteStatement As String _
            = "DELETE FROM " _
            & "     [PayeLine] " _
            & "WHERE " _
            & "     [IDPeriode] = @OldIDPeriode AND Ajout <> 1 AND IsCommissionVendeur=0"

        Dim deleteCommand As New SqlCommand(deleteStatement, connection)
        deleteCommand.CommandType = CommandType.Text
        deleteCommand.Parameters.AddWithValue("@OldIDPeriode", clsPayeLine.IDPeriode)

        Try
            connection.Open()
            Dim count As Integer = deleteCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function
    Public Function DeleteParPaye(ByVal clsPayeLine As PayeLine) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim deleteStatement As String _
            = "DELETE FROM " _
            & "     [PayeLine] " _
            & "WHERE " _
            & "     [IDPaye] = @OldIDPaye "

        Dim deleteCommand As New SqlCommand(deleteStatement, connection)
        deleteCommand.CommandType = CommandType.Text
        deleteCommand.Parameters.AddWithValue("@OldIDPaye", clsPayeLine.IDPaye)

        Try
            connection.Open()
            Dim count As Integer = deleteCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

    Public Function Delete(ByVal clsPayeLine As PayeLine) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim deleteStatement As String _
            = "DELETE FROM " _
            & "     [PayeLine] " _
            & "WHERE " _
            & "     [IDPayeLine] = @OldIDPayeLine "

        Dim deleteCommand As New SqlCommand(deleteStatement, connection)
        deleteCommand.CommandType = CommandType.Text
        deleteCommand.Parameters.AddWithValue("@OldIDPayeLine", clsPayeLine.IDPayeLine)

        Try
            connection.Open()
            Dim count As Integer = deleteCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

End Class

