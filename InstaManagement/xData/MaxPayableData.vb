Imports System.Data.SqlClient

Public Class MaxPayableData


    Public Function SelectAllByQuantiteLivraison(piQTY As Integer, piOrganisationID As Integer) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDMaxPayable] " _
            & "    ,[NombreLivraison] " _
            & "    ,[MaxPayable] " _
            & "    ,[OrganisationID] " _
            & "    ,[Organisation] " _
            & "FROM " _
            & "     [MaxPayable] " _
            & " WHERE NombreLivraison=" & piQTY & " AND  OrganisationID =" & piOrganisationID
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function
    Public Function SelectAllByID(piOrganisationID As Integer) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDMaxPayable] " _
            & "    ,[NombreLivraison] " _
            & "    ,[MaxPayable] " _
            & "    ,[OrganisationID] " _
            & "    ,[Organisation] " _
            & "FROM " _
            & "     [MaxPayable] " _
            & " WHERe OrganisationID=" & piOrganisationID
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function
    Public Function SelectAll() As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDMaxPayable] " _
            & "    ,[NombreLivraison] " _
            & "    ,[MaxPayable] " _
            & "    ,[OrganisationID] " _
            & "    ,[Organisation] " _
            & "FROM " _
            & "     [MaxPayable] " _
            & ""
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function

    Public Function Select_Record(ByVal clsMaxPayablePara As MaxPayable) As MaxPayable
        Dim clsMaxPayable As New MaxPayable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDMaxPayable] " _
            & "    ,[NombreLivraison] " _
            & "    ,[MaxPayable] " _
            & "    ,[OrganisationID] " _
            & "    ,[Organisation] " _
            & "FROM " _
            & "     [MaxPayable] " _
            & "WHERE " _
            & "     [IDMaxPayable] = @IDMaxPayable " _
            & ""
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        selectCommand.Parameters.AddWithValue("@IDMaxPayable", clsMaxPayablePara.IDMaxPayable)
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader(CommandBehavior.SingleRow)
            If reader.Read Then
                With clsMaxPayable
                    .IDMaxPayable = System.Convert.ToInt32(reader("IDMaxPayable"))
                    .NombreLivraison = If(IsDBNull(reader("NombreLivraison")), Nothing, CType(reader("NombreLivraison"), Int32?))
                    .MaxPayable = If(IsDBNull(reader("MaxPayable")), Nothing, CType(reader("MaxPayable"), Decimal?))
                    .OrganisationID = If(IsDBNull(reader("OrganisationID")), Nothing, CType(reader("OrganisationID"), Int32?))
                    .Organisation = If(IsDBNull(reader("Organisation")), Nothing, reader("Organisation").ToString)
                End With
            Else
                clsMaxPayable = Nothing
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return clsMaxPayable
    End Function

    Public Function Add(ByVal clsMaxPayable As MaxPayable) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim insertStatement As String _
            = "INSERT " _
            & "     [MaxPayable] " _
            & "     ( " _
            & "     [NombreLivraison] " _
            & "    ,[MaxPayable] " _
            & "    ,[OrganisationID] " _
            & "    ,[Organisation] " _
            & "     ) " _
            & "VALUES " _
            & "     ( " _
            & "     @NombreLivraison " _
            & "    ,@MaxPayable " _
            & "    ,@OrganisationID " _
            & "    ,@Organisation " _
            & "     ) " _
            & ""
        Dim insertCommand As New SqlCommand(insertStatement, connection)
        insertCommand.CommandType = CommandType.Text
        insertCommand.Parameters.AddWithValue("@NombreLivraison", IIf(clsMaxPayable.NombreLivraison.HasValue, clsMaxPayable.NombreLivraison, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@MaxPayable", IIf(clsMaxPayable.MaxPayable.HasValue, clsMaxPayable.MaxPayable, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@OrganisationID", IIf(clsMaxPayable.OrganisationID.HasValue, clsMaxPayable.OrganisationID, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Organisation", IIf(Not IsNothing(clsMaxPayable.Organisation), clsMaxPayable.Organisation, DBNull.Value))
        Try
            connection.Open()
            Dim count As Integer = insertCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

    Public Function Update(ByVal clsMaxPayableOld As MaxPayable,
           ByVal clsMaxPayableNew As MaxPayable) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim updateStatement As String _
            = "UPDATE " _
            & "     [MaxPayable] " _
            & "SET " _
            & "     [NombreLivraison] = @NewNombreLivraison " _
            & "    ,[MaxPayable] = @NewMaxPayable " _
            & "    ,[OrganisationID] = @NewOrganisationID " _
            & "    ,[Organisation] = @NewOrganisation " _
            & "WHERE " _
            & "     [IDMaxPayable] = @OldIDMaxPayable " _
            & ""
        Dim updateCommand As New SqlCommand(updateStatement, connection)
        updateCommand.CommandType = CommandType.Text
        updateCommand.Parameters.AddWithValue("@NewNombreLivraison", IIf(clsMaxPayableNew.NombreLivraison.HasValue, clsMaxPayableNew.NombreLivraison, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewMaxPayable", IIf(clsMaxPayableNew.MaxPayable.HasValue, clsMaxPayableNew.MaxPayable, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewOrganisationID", IIf(clsMaxPayableNew.OrganisationID.HasValue, clsMaxPayableNew.OrganisationID, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewOrganisation", IIf(Not IsNothing(clsMaxPayableNew.Organisation), clsMaxPayableNew.Organisation, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@OldIDMaxPayable", clsMaxPayableOld.IDMaxPayable)

        Try
            connection.Open()
            Dim count As Integer = updateCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

    Public Function Delete(ByVal clsMaxPayable As MaxPayable) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim deleteStatement As String _
            = "DELETE FROM " _
            & "     [MaxPayable] " _
            & "WHERE " _
            & "     [IDMaxPayable] = @OldIDMaxPayable " _
            & ""
        Dim deleteCommand As New SqlCommand(deleteStatement, connection)
        deleteCommand.CommandType = CommandType.Text
        deleteCommand.Parameters.AddWithValue("@OldIDMaxPayable", clsMaxPayable.IDMaxPayable)

        Try
            connection.Open()
            Dim count As Integer = deleteCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

End Class

