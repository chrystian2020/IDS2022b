Imports System.Data.SqlClient

Public Class InvoiceLineData
    Public Function DeleteParInvoice(ByVal clsInvoiceLine As InvoiceLine) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim deleteStatement As String _
            = "DELETE FROM " _
            & "     [InvoiceLine] " _
            & "WHERE " _
            & "     [IDInvoice] = @OldIDInvoice "

        Dim deleteCommand As New SqlCommand(deleteStatement, connection)
        deleteCommand.CommandType = CommandType.Text
        deleteCommand.Parameters.AddWithValue("@OldIDInvoice", clsInvoiceLine.IDInvoice)

        Try
            connection.Open()
            Dim count As Integer = deleteCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function
    Public Function SelectAllGlacier(piIDInvoiceID As Integer) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDInvoiceLine] " _
            & "    ,[IDInvoice] " _
            & "    ,[InvoiceNumber] " _
            & "    ,[InvoiceDate] " _
            & "    ,[DisplayDate] " _
            & "    ,[OrganisationID] " _
            & "    ,[Organisation] " _
            & "    ,[DateFrom] " _
            & "    ,[DateTo] " _
            & "    ,[Code] " _
            & "    ,[Km] " _
            & "    ,[Description] " _
            & "    ,[Unite] " _
            & "    ,[Prix] " _
            & "    ,[Montant] " _
            & "    ,[Credit] " _
            & "    ,[Debit] " _
            & "    ,[NombreGlacier] " _
            & "    ,[Glacier] " _
            & "    ,[KmExtra] " _
            & "    ,[LivreurID] " _
            & "    ,[IDPeriode] " _
            & "    ,[TypeAjout] " _
            & "    ,[Ajout] " _
            & "FROM " _
            & "     [InvoiceLine] " _
            & " WHERE IDInvoice = " & piIDInvoiceID & "AND TypeAjout = 1"
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function


    Public Function SelectAllByInvoiceID(piIDInvoiceLine As Integer) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDInvoiceLine] " _
            & "    ,[IDInvoice] " _
            & "    ,[InvoiceNumber] " _
            & "    ,[InvoiceDate] " _
            & "    ,[DisplayDate] " _
            & "    ,[OrganisationID] " _
            & "    ,[Organisation] " _
            & "    ,[DateFrom] " _
            & "    ,[DateTo] " _
            & "    ,[Code] " _
            & "    ,[Km] " _
            & "    ,[Description] " _
            & "    ,[Unite] " _
            & "    ,[Prix] " _
            & "    ,[Montant] " _
            & "    ,[Credit] " _
            & "    ,[Debit] " _
            & "    ,[NombreGlacier] " _
            & "    ,[Glacier] " _
            & "    ,[KmExtra] " _
            & "    ,[LivreurID] " _
            & "    ,[IDPeriode] " _
            & "    ,[TypeAjout] " _
            & "    ,[Ajout] " _
            & "FROM " _
            & "     [InvoiceLine] " _
            & " WHERE IDInvoice = " & piIDInvoiceLine & " ORDER BY IDInvoiceLine, InvoiceDate, Ajout"
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function

    Public Function SelectAllByID(piIDInvoiceLine As Integer) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDInvoiceLine] " _
            & "    ,[IDInvoice] " _
            & "    ,[InvoiceNumber] " _
            & "    ,[InvoiceDate] " _
            & "    ,[DisplayDate] " _
            & "    ,[OrganisationID] " _
            & "    ,[Organisation] " _
            & "    ,[DateFrom] " _
            & "    ,[DateTo] " _
            & "    ,[Code] " _
            & "    ,[Km] " _
            & "    ,[Description] " _
            & "    ,[Unite] " _
            & "    ,[Prix] " _
            & "    ,[Montant] " _
            & "    ,[Credit] " _
            & "    ,[Debit] " _
            & "    ,[NombreGlacier] " _
            & "    ,[Glacier] " _
            & "    ,[KmExtra] " _
            & "    ,[LivreurID] " _
            & "    ,[IDPeriode] " _
            & "    ,[TypeAjout] " _
            & "    ,[Ajout] " _
            & "FROM " _
            & "     [InvoiceLine] " _
            & " WHERE IDInvoice = " & piIDInvoiceLine
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function

    Public Function SelectAllByDate(pdDateFrom As DateTime, pdDateTo As DateTime, piOrganisationID As Integer) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDInvoiceLine] " _
            & "    ,[IDInvoice] " _
            & "    ,[InvoiceNumber] " _
            & "    ,[InvoiceDate] " _
            & "    ,[DisplayDate] " _
            & "    ,[OrganisationID] " _
            & "    ,[Organisation] " _
            & "    ,[DateFrom] " _
            & "    ,[DateTo] " _
            & "    ,[Code] " _
            & "    ,[Km] " _
            & "    ,[Description] " _
            & "    ,[Unite] " _
            & "    ,[Prix] " _
            & "    ,[Montant] " _
            & "    ,[Credit] " _
            & "    ,[Debit] " _
            & "    ,[NombreGlacier] " _
            & "    ,[Glacier] " _
            & "    ,[KmExtra] " _
            & "    ,[LivreurID] " _
            & "    ,[IDPeriode] " _
            & "    ,[TypeAjout] " _
            & "    ,[Ajout] " _
            & " FROM " _
            & "     [InvoiceLine] " _
            & " WHERE (DateFrom  >= '" & pdDateFrom & "' AND DateTo <= '" & pdDateTo & "')  AND IDPeriode = " & go_Globals.IDPeriode & " AND OrganisationID=  " & piOrganisationID & " ORDER BY IDInvoiceLine ASC"



        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function
    Public Function SelectAll() As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDInvoiceLine] " _
            & "    ,[IDInvoice] " _
            & "    ,[InvoiceNumber] " _
            & "    ,[InvoiceDate] " _
            & "    ,[DisplayDate] " _
            & "    ,[OrganisationID] " _
            & "    ,[Organisation] " _
            & "    ,[DateFrom] " _
            & "    ,[DateTo] " _
            & "    ,[Code] " _
            & "    ,[Km] " _
            & "    ,[Description] " _
            & "    ,[Unite] " _
            & "    ,[Prix] " _
            & "    ,[Montant] " _
            & "    ,[Credit] " _
            & "    ,[Debit] " _
            & "    ,[NombreGlacier] " _
            & "    ,[Glacier] " _
            & "    ,[KmExtra] " _
            & "    ,[LivreurID] " _
            & "    ,[IDPeriode] " _
            & "    ,[TypeAjout] " _
            & "    ,[Ajout] " _
            & "FROM " _
            & "     [InvoiceLine] " _
            & ""
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function

    Public Function Select_Record(ByVal clsInvoiceLinePara As InvoiceLine) As InvoiceLine
        Dim clsInvoiceLine As New InvoiceLine
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDInvoiceLine] " _
            & "    ,[IDInvoice] " _
            & "    ,[InvoiceNumber] " _
            & "    ,[InvoiceDate] " _
            & "    ,[DisplayDate] " _
            & "    ,[OrganisationID] " _
            & "    ,[Organisation] " _
            & "    ,[DateFrom] " _
            & "    ,[DateTo] " _
            & "    ,[Code] " _
            & "    ,[Km] " _
            & "    ,[Description] " _
            & "    ,[Unite] " _
            & "    ,[Prix] " _
            & "    ,[Montant] " _
            & "    ,[Credit] " _
            & "    ,[Debit] " _
            & "    ,[NombreGlacier] " _
            & "    ,[Glacier] " _
            & "    ,[KmExtra] " _
            & "    ,[LivreurID] " _
            & "    ,[IDPeriode] " _
            & "    ,[TypeAjout] " _
            & "    ,[Ajout] " _
            & "FROM " _
            & "     [InvoiceLine] " _
            & "WHERE " _
            & "     [IDInvoiceLine] = @IDInvoiceLine " _
            & ""
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        selectCommand.Parameters.AddWithValue("@IDInvoiceLine", clsInvoiceLinePara.IDInvoiceLine)
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader(CommandBehavior.SingleRow)
            If reader.Read Then
                With clsInvoiceLine
                    .IDInvoiceLine = System.Convert.ToInt32(reader("IDInvoiceLine"))
                    .IDInvoice = If(IsDBNull(reader("IDInvoice")), Nothing, CType(reader("IDInvoice"), Int32?))
                    .InvoiceNumber = If(IsDBNull(reader("InvoiceNumber")), Nothing, reader("InvoiceNumber").ToString)
                    .InvoiceDate = If(IsDBNull(reader("InvoiceDate")), Nothing, CType(reader("InvoiceDate"), DateTime?))
                    .DisplayDate = If(IsDBNull(reader("DisplayDate")), Nothing, CType(reader("DisplayDate"), DateTime?))
                    .OrganisationID = If(IsDBNull(reader("OrganisationID")), Nothing, CType(reader("OrganisationID"), Int32?))
                    .Organisation = If(IsDBNull(reader("Organisation")), Nothing, reader("Organisation").ToString)
                    .DateFrom = If(IsDBNull(reader("DateFrom")), Nothing, CType(reader("DateFrom"), DateTime?))
                    .DateTo = If(IsDBNull(reader("DateTo")), Nothing, CType(reader("DateTo"), DateTime?))
                    .Code = If(IsDBNull(reader("Code")), Nothing, reader("Code").ToString)
                    .Km = If(IsDBNull(reader("Km")), Nothing, CType(CType(reader("Km"), Double?), Decimal?))
                    .Description = If(IsDBNull(reader("Description")), Nothing, reader("Description").ToString)
                    .Unite = If(IsDBNull(reader("Unite")), Nothing, CType(CType(reader("Unite"), Double?), Decimal?))
                    .Prix = If(IsDBNull(reader("Prix")), Nothing, CType(reader("Prix"), Decimal?))
                    .Montant = If(IsDBNull(reader("Montant")), Nothing, CType(reader("Montant"), Decimal?))
                    .Credit = If(IsDBNull(reader("Credit")), Nothing, CType(reader("Credit"), Decimal?))
                    .Debit = If(IsDBNull(reader("Debit")), Nothing, CType(reader("Debit"), Decimal?))
                    .NombreGlacier = If(IsDBNull(reader("NombreGlacier")), Nothing, CType(reader("NombreGlacier"), Int32?))
                    .Glacier = If(IsDBNull(reader("Glacier")), Nothing, CType(reader("Glacier"), Decimal?))
                    .KmExtra = If(IsDBNull(reader("KmExtra")), Nothing, CType(CType(reader("KmExtra"), Double?), Decimal?))
                    .LivreurID = If(IsDBNull(reader("LivreurID")), Nothing, CType(reader("LivreurID"), Int32?))
                    .IDPeriode = If(IsDBNull(reader("IDPeriode")), Nothing, CType(reader("IDPeriode"), Int32?))
                    .TypeAjout = If(IsDBNull(reader("TypeAjout")), Nothing, CType(reader("TypeAjout"), Int32?))
                    .Ajout = If(IsDBNull(reader("Ajout")), Nothing, CType(reader("Ajout"), Boolean?))
                End With
            Else
                clsInvoiceLine = Nothing
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return clsInvoiceLine
    End Function


    Public Function Add(ByVal clsInvoiceLine As InvoiceLine) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim insertStatement As String _
            = "INSERT " _
            & "     [InvoiceLine] " _
            & "     ( " _
            & "     [IDInvoice] " _
            & "    ,[InvoiceNumber] " _
            & "    ,[InvoiceDate] " _
            & "    ,[DisplayDate] " _
            & "    ,[OrganisationID] " _
            & "    ,[Organisation] " _
            & "    ,[DateFrom] " _
            & "    ,[DateTo] " _
            & "    ,[Code] " _
            & "    ,[Km] " _
            & "    ,[Description] " _
            & "    ,[Unite] " _
            & "    ,[Prix] " _
            & "    ,[Montant] " _
            & "    ,[Credit] " _
            & "    ,[Debit] " _
            & "    ,[NombreGlacier] " _
            & "    ,[Glacier] " _
            & "    ,[KmExtra] " _
            & "    ,[LivreurID] " _
            & "    ,[IDPeriode] " _
            & "    ,[TypeAjout] " _
            & "    ,[Ajout] " _
            & "     ) " _
            & "VALUES " _
            & "     ( " _
            & "     @IDInvoice " _
            & "    ,@InvoiceNumber " _
            & "    ,@InvoiceDate " _
            & "    ,@DisplayDate " _
            & "    ,@OrganisationID " _
            & "    ,@Organisation " _
            & "    ,@DateFrom " _
            & "    ,@DateTo " _
            & "    ,@Code " _
            & "    ,@Km " _
            & "    ,@Description " _
            & "    ,@Unite " _
            & "    ,@Prix " _
            & "    ,@Montant " _
            & "    ,@Credit " _
            & "    ,@Debit " _
            & "    ,@NombreGlacier " _
            & "    ,@Glacier " _
            & "    ,@KmExtra " _
            & "    ,@LivreurID " _
            & "    ,@IDPeriode " _
            & "    ,@TypeAjout " _
            & "    ,@Ajout " _
            & "     ) " _
            & ""
        Dim insertCommand As New SqlCommand(insertStatement, connection)
        insertCommand.CommandType = CommandType.Text
        insertCommand.Parameters.AddWithValue("@IDInvoice", IIf(clsInvoiceLine.IDInvoice.HasValue, clsInvoiceLine.IDInvoice, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@InvoiceNumber", IIf(Not IsNothing(clsInvoiceLine.InvoiceNumber), clsInvoiceLine.InvoiceNumber, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@InvoiceDate", IIf(clsInvoiceLine.InvoiceDate.HasValue, clsInvoiceLine.InvoiceDate, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@DisplayDate", IIf(clsInvoiceLine.DisplayDate.HasValue, clsInvoiceLine.DisplayDate, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@OrganisationID", IIf(clsInvoiceLine.OrganisationID.HasValue, clsInvoiceLine.OrganisationID, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Organisation", IIf(Not IsNothing(clsInvoiceLine.Organisation), clsInvoiceLine.Organisation, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@DateFrom", IIf(clsInvoiceLine.DateFrom.HasValue, clsInvoiceLine.DateFrom, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@DateTo", IIf(clsInvoiceLine.DateTo.HasValue, clsInvoiceLine.DateTo, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Code", IIf(Not IsNothing(clsInvoiceLine.Code), clsInvoiceLine.Code, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Km", IIf(clsInvoiceLine.Km.HasValue, clsInvoiceLine.Km, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Description", IIf(Not IsNothing(clsInvoiceLine.Description), clsInvoiceLine.Description, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Unite", IIf(clsInvoiceLine.Unite.HasValue, clsInvoiceLine.Unite, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Prix", IIf(clsInvoiceLine.Prix.HasValue, clsInvoiceLine.Prix, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Montant", IIf(clsInvoiceLine.Montant.HasValue, clsInvoiceLine.Montant, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Credit", IIf(clsInvoiceLine.Credit.HasValue, clsInvoiceLine.Credit, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Debit", IIf(clsInvoiceLine.Debit.HasValue, clsInvoiceLine.Debit, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@NombreGlacier", IIf(clsInvoiceLine.NombreGlacier.HasValue, clsInvoiceLine.NombreGlacier, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Glacier", IIf(clsInvoiceLine.Glacier.HasValue, clsInvoiceLine.Glacier, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@KmExtra", IIf(clsInvoiceLine.KmExtra.HasValue, clsInvoiceLine.KmExtra, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@LivreurID", IIf(clsInvoiceLine.LivreurID.HasValue, clsInvoiceLine.LivreurID, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@IDPeriode", IIf(clsInvoiceLine.IDPeriode.HasValue, clsInvoiceLine.IDPeriode, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@TypeAjout", IIf(clsInvoiceLine.TypeAjout.HasValue, clsInvoiceLine.TypeAjout, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Ajout", IIf(clsInvoiceLine.Ajout.HasValue, clsInvoiceLine.Ajout, DBNull.Value))
        Try
            connection.Open()
            Dim count As Integer = insertCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

    Public Function Update(ByVal clsInvoiceLineOld As InvoiceLine,
           ByVal clsInvoiceLineNew As InvoiceLine) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim updateStatement As String _
            = "UPDATE " _
            & "     [InvoiceLine] " _
            & "SET " _
            & "     [IDInvoice] = @NewIDInvoice " _
            & "    ,[InvoiceNumber] = @NewInvoiceNumber " _
            & "    ,[InvoiceDate] = @NewInvoiceDate " _
            & "    ,[DisplayDate] = @NewDisplayDate " _
            & "    ,[OrganisationID] = @NewOrganisationID " _
            & "    ,[Organisation] = @NewOrganisation " _
            & "    ,[DateFrom] = @NewDateFrom " _
            & "    ,[DateTo] = @NewDateTo " _
            & "    ,[Code] = @NewCode " _
            & "    ,[Km] = @NewKm " _
            & "    ,[Description] = @NewDescription " _
            & "    ,[Unite] = @NewUnite " _
            & "    ,[Prix] = @NewPrix " _
            & "    ,[Montant] = @NewMontant " _
            & "    ,[Credit] = @NewCredit " _
            & "    ,[Debit] = @NewDebit " _
            & "    ,[NombreGlacier] = @NewNombreGlacier " _
            & "    ,[Glacier] = @NewGlacier " _
            & "    ,[KmExtra] = @NewKmExtra " _
            & "    ,[LivreurID] = @NewLivreurID " _
            & "    ,[IDPeriode] = @NewIDPeriode " _
            & "    ,[TypeAjout] = @NewTypeAjout " _
            & "    ,[Ajout] = @NewAjout " _
            & "WHERE " _
            & "     [IDInvoiceLine] = @OldIDInvoiceLine " _
            & ""
        Dim updateCommand As New SqlCommand(updateStatement, connection)
        updateCommand.CommandType = CommandType.Text
        updateCommand.Parameters.AddWithValue("@NewIDInvoice", IIf(clsInvoiceLineNew.IDInvoice.HasValue, clsInvoiceLineNew.IDInvoice, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewInvoiceNumber", IIf(Not IsNothing(clsInvoiceLineNew.InvoiceNumber), clsInvoiceLineNew.InvoiceNumber, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewInvoiceDate", IIf(clsInvoiceLineNew.InvoiceDate.HasValue, clsInvoiceLineNew.InvoiceDate, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewDisplayDate", IIf(clsInvoiceLineNew.DisplayDate.HasValue, clsInvoiceLineNew.DisplayDate, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewOrganisationID", IIf(clsInvoiceLineNew.OrganisationID.HasValue, clsInvoiceLineNew.OrganisationID, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewOrganisation", IIf(Not IsNothing(clsInvoiceLineNew.Organisation), clsInvoiceLineNew.Organisation, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewDateFrom", IIf(clsInvoiceLineNew.DateFrom.HasValue, clsInvoiceLineNew.DateFrom, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewDateTo", IIf(clsInvoiceLineNew.DateTo.HasValue, clsInvoiceLineNew.DateTo, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewCode", IIf(Not IsNothing(clsInvoiceLineNew.Code), clsInvoiceLineNew.Code, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewKm", IIf(clsInvoiceLineNew.Km.HasValue, clsInvoiceLineNew.Km, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewDescription", IIf(Not IsNothing(clsInvoiceLineNew.Description), clsInvoiceLineNew.Description, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewUnite", IIf(clsInvoiceLineNew.Unite.HasValue, clsInvoiceLineNew.Unite, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewPrix", IIf(clsInvoiceLineNew.Prix.HasValue, clsInvoiceLineNew.Prix, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewMontant", IIf(clsInvoiceLineNew.Montant.HasValue, clsInvoiceLineNew.Montant, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewCredit", IIf(clsInvoiceLineNew.Credit.HasValue, clsInvoiceLineNew.Credit, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewDebit", IIf(clsInvoiceLineNew.Debit.HasValue, clsInvoiceLineNew.Debit, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewNombreGlacier", IIf(clsInvoiceLineNew.NombreGlacier.HasValue, clsInvoiceLineNew.NombreGlacier, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewGlacier", IIf(clsInvoiceLineNew.Glacier.HasValue, clsInvoiceLineNew.Glacier, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewKmExtra", IIf(clsInvoiceLineNew.KmExtra.HasValue, clsInvoiceLineNew.KmExtra, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewLivreurID", IIf(clsInvoiceLineNew.LivreurID.HasValue, clsInvoiceLineNew.LivreurID, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewIDPeriode", IIf(clsInvoiceLineNew.IDPeriode.HasValue, clsInvoiceLineNew.IDPeriode, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewTypeAjout", IIf(clsInvoiceLineNew.TypeAjout.HasValue, clsInvoiceLineNew.TypeAjout, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewAjout", IIf(clsInvoiceLineNew.Ajout.HasValue, clsInvoiceLineNew.Ajout, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@OldIDInvoiceLine", clsInvoiceLineOld.IDInvoiceLine)

        Try
            connection.Open()
            Dim count As Integer = updateCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

    Public Function DeleteByPeriodO(ByVal clsInvoiceLine As InvoiceLine) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim deleteStatement As String _
            = "DELETE FROM " _
            & "     [InvoiceLine] " _
            & "WHERE " _
            & "     [IDPeriode] = @OldIDPeriode " _
            & " AND [OrganisationID] IS NOT NULL AND (Ajout <> 1 OR Ajout IS NULL)"
        Dim deleteCommand As New SqlCommand(deleteStatement, connection)
        deleteCommand.CommandType = CommandType.Text
        deleteCommand.Parameters.AddWithValue("@OldIDPeriode", clsInvoiceLine.IDPeriode)

        Try
            connection.Open()
            Dim count As Integer = deleteCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

    Public Function DeleteByPeriodOrganisation(ByVal clsInvoiceLine As InvoiceLine) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim deleteStatement As String _
            = "DELETE FROM " _
            & "     [InvoiceLine] " _
            & "WHERE " _
            & "     [IDPeriode] = @OldIDPeriode " _
            & " AND [OrganisationID] = @OldOrganisationID AND Ajout <> 1"
        Dim deleteCommand As New SqlCommand(deleteStatement, connection)
        deleteCommand.CommandType = CommandType.Text
        deleteCommand.Parameters.AddWithValue("@OldIDPeriode", clsInvoiceLine.IDPeriode)
        deleteCommand.Parameters.AddWithValue("@OldOrganisationID", clsInvoiceLine.OrganisationID)
        Try
            connection.Open()
            Dim count As Integer = deleteCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function
    Public Function DeleteByPeriod(ByVal clsInvoiceLine As InvoiceLine) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim deleteStatement As String _
            = "DELETE FROM " _
            & "     [InvoiceLine] " _
            & "WHERE " _
            & "     [IDPeriode] = @OldIDPeriode " _
            & ""
        Dim deleteCommand As New SqlCommand(deleteStatement, connection)
        deleteCommand.CommandType = CommandType.Text
        deleteCommand.Parameters.AddWithValue("@OldIDPeriode", clsInvoiceLine.IDPeriode)

        Try
            connection.Open()
            Dim count As Integer = deleteCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function
    Public Function Delete(ByVal clsInvoiceLine As InvoiceLine) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim deleteStatement As String _
            = "DELETE FROM " _
            & "     [InvoiceLine] " _
            & "WHERE " _
            & "     [IDInvoiceLine] = @OldIDInvoiceLine " _
            & ""
        Dim deleteCommand As New SqlCommand(deleteStatement, connection)
        deleteCommand.CommandType = CommandType.Text
        deleteCommand.Parameters.AddWithValue("@OldIDInvoiceLine", clsInvoiceLine.IDInvoiceLine)

        Try
            connection.Open()
            Dim count As Integer = deleteCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

End Class

