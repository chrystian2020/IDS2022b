Imports System.Data.SqlClient

Public Class EtatsVentesData


    Public Function SelectAllByPeriode(pIDPeriode As Integer) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDEtatsVentes] " _
            & "    ,[IDPeriode] " _
            & "    ,[CheminRapport] " _
            & "    ,[NomFichier] " _
            & "    ,[DateFrom] " _
            & "    ,[DateTo] " _
            & "FROM " _
            & "     [EtatsVentes] " _
            & " WHERE IDPeriode =" & pIDPeriode
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function


    Public Function SelectAll() As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDEtatsVentes] " _
            & "    ,[IDPeriode] " _
            & "    ,[CheminRapport] " _
            & "    ,[NomFichier] " _
            & "    ,[DateFrom] " _
            & "    ,[DateTo] " _
            & "FROM " _
            & "     [EtatsVentes] " _
            & ""
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function

    Public Function Select_Record(ByVal clsEtatsVentesPara As EtatsVentes) As EtatsVentes
        Dim clsEtatsVentes As New EtatsVentes
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDEtatsVentes] " _
            & "    ,[IDPeriode] " _
            & "    ,[CheminRapport] " _
            & "    ,[NomFichier] " _
            & "    ,[DateFrom] " _
            & "    ,[DateTo] " _
            & "FROM " _
            & "     [EtatsVentes] " _
            & "WHERE " _
            & "     [IDEtatsVentes] = @IDEtatsVentes " _
            & ""
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        selectCommand.Parameters.AddWithValue("@IDEtatsVentes", clsEtatsVentesPara.IDEtatsVentes)
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader(CommandBehavior.SingleRow)
            If reader.Read Then
                With clsEtatsVentes
                    .IDEtatsVentes = System.Convert.ToInt32(reader("IDEtatsVentes"))
                    .IDPeriode = If(IsDBNull(reader("IDPeriode")), Nothing, CType(reader("IDPeriode"), Int32?))
                    .CheminRapport = System.Convert.ToString(reader("CheminRapport"))
                    .NomFichier = If(IsDBNull(reader("NomFichier")), Nothing, reader("NomFichier").ToString)
                    .DateFrom = If(IsDBNull(reader("DateFrom")), Nothing, CType(reader("DateFrom"), DateTime?))
                    .DateTo = If(IsDBNull(reader("DateTo")), Nothing, CType(reader("DateTo"), DateTime?))
                End With
            Else
                clsEtatsVentes = Nothing
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return clsEtatsVentes
    End Function

    Public Function Add(ByVal clsEtatsVentes As EtatsVentes) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim insertStatement As String _
            = "INSERT " _
            & "     [EtatsVentes] " _
            & "     ( " _
            & "     [IDPeriode] " _
            & "    ,[CheminRapport] " _
            & "    ,[NomFichier] " _
            & "    ,[DateFrom] " _
            & "    ,[DateTo] " _
            & "     ) " _
            & "VALUES " _
            & "     ( " _
            & "     @IDPeriode " _
            & "    ,@CheminRapport " _
            & "    ,@NomFichier " _
            & "    ,@DateFrom " _
            & "    ,@DateTo " _
            & "     ) " _
            & ""
        Dim insertCommand As New SqlCommand(insertStatement, connection)
        insertCommand.CommandType = CommandType.Text
        insertCommand.Parameters.AddWithValue("@IDPeriode", IIf(clsEtatsVentes.IDPeriode.HasValue, clsEtatsVentes.IDPeriode, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@CheminRapport", clsEtatsVentes.CheminRapport)
        insertCommand.Parameters.AddWithValue("@NomFichier", IIf(Not IsNothing(clsEtatsVentes.NomFichier), clsEtatsVentes.NomFichier, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@DateFrom", IIf(clsEtatsVentes.DateFrom.HasValue, clsEtatsVentes.DateFrom, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@DateTo", IIf(clsEtatsVentes.DateTo.HasValue, clsEtatsVentes.DateTo, DBNull.Value))
        Try
            connection.Open()
            Dim count As Integer = insertCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

    Public Function Update(ByVal clsEtatsVentesOld As EtatsVentes,
           ByVal clsEtatsVentesNew As EtatsVentes) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim updateStatement As String _
            = "UPDATE " _
            & "     [EtatsVentes] " _
            & "SET " _
            & "     [IDPeriode] = @NewIDPeriode " _
            & "    ,[CheminRapport] = @NewCheminRapport " _
            & "    ,[NomFichier] = @NewNomFichier " _
            & "    ,[DateFrom] = @NewDateFrom " _
            & "    ,[DateTo] = @NewDateTo " _
            & "WHERE " _
            & "     [IDEtatsVentes] = @OldIDEtatsVentes " _
            & ""
        Dim updateCommand As New SqlCommand(updateStatement, connection)
        updateCommand.CommandType = CommandType.Text
        updateCommand.Parameters.AddWithValue("@NewIDPeriode", IIf(clsEtatsVentesNew.IDPeriode.HasValue, clsEtatsVentesNew.IDPeriode, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewCheminRapport", clsEtatsVentesNew.CheminRapport)
        updateCommand.Parameters.AddWithValue("@NewNomFichier", IIf(Not IsNothing(clsEtatsVentesNew.NomFichier), clsEtatsVentesNew.NomFichier, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewDateFrom", IIf(clsEtatsVentesNew.DateFrom.HasValue, clsEtatsVentesNew.DateFrom, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewDateTo", IIf(clsEtatsVentesNew.DateTo.HasValue, clsEtatsVentesNew.DateTo, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@OldIDEtatsVentes", clsEtatsVentesOld.IDEtatsVentes)

        Try
            connection.Open()
            Dim count As Integer = updateCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

    Public Function Delete(ByVal clsEtatsVentes As EtatsVentes) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim deleteStatement As String _
            = "DELETE FROM " _
            & "     [EtatsVentes] " _
            & "WHERE " _
            & "     [IDEtatsVentes] = @OldIDEtatsVentes " _
            & ""
        Dim deleteCommand As New SqlCommand(deleteStatement, connection)
        deleteCommand.CommandType = CommandType.Text
        deleteCommand.Parameters.AddWithValue("@OldIDEtatsVentes", clsEtatsVentes.IDEtatsVentes)

        Try
            connection.Open()
            Dim count As Integer = deleteCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

End Class

