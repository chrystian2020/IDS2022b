Imports System.Data.SqlClient

Public Class AppointmentsData

    Public Function SelectAll() As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [UniqueID] " _
            & "    ,[Type] " _
            & "    ,[StartDate] " _
            & "    ,[EndDate] " _
            & "    ,[AllDay] " _
            & "    ,[Subject] " _
            & "    ,[Location] " _
            & "    ,[Description] " _
            & "    ,[Status] " _
            & "    ,[Label] " _
            & "    ,[ResourceID] " _
            & "    ,[ResourceIDs] " _
            & "    ,[ReminderInfo] " _
            & "    ,[RecurrenceInfo] " _
            & "    ,[CustomField1] " _
            & "FROM " _
            & "     [Appointments] " _
            & ""
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Dim ds As New DataSet
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function

    Public Function Select_Record(ByVal clsAppointmentsPara As Appointments) As Appointments
        Dim clsAppointments As New Appointments
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [UniqueID] " _
            & "    ,[Type] " _
            & "    ,[StartDate] " _
            & "    ,[EndDate] " _
            & "    ,[AllDay] " _
            & "    ,[Subject] " _
            & "    ,[Location] " _
            & "    ,[Description] " _
            & "    ,[Status] " _
            & "    ,[Label] " _
            & "    ,[ResourceID] " _
            & "    ,[ResourceIDs] " _
            & "    ,[ReminderInfo] " _
            & "    ,[RecurrenceInfo] " _
            & "    ,[CustomField1] " _
            & "FROM " _
            & "     [Appointments] " _
            & "WHERE " _
            & "     [UniqueID] = @UniqueID " _
            & ""
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        selectCommand.Parameters.AddWithValue("@UniqueID", clsAppointmentsPara.UniqueID)
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader(CommandBehavior.SingleRow)
            If reader.Read Then
                With clsAppointments
                    .UniqueID = System.Convert.ToInt32(reader("UniqueID"))
                    .Type = If(IsDBNull(reader("Type")), Nothing, CType(reader("Type"), Int32?))
                    .StartDate = If(IsDBNull(reader("StartDate")), Nothing, CType(reader("StartDate"), DateTime?))
                    .EndDate = If(IsDBNull(reader("EndDate")), Nothing, CType(reader("EndDate"), DateTime?))
                    .AllDay = If(IsDBNull(reader("AllDay")), Nothing, CType(reader("AllDay"), Boolean?))
                    .Subject = If(IsDBNull(reader("Subject")), Nothing, reader("Subject").ToString)
                    .Location = If(IsDBNull(reader("Location")), Nothing, reader("Location").ToString)
                    .Description = If(IsDBNull(reader("Description")), Nothing, reader("Description").ToString)
                    .Status = If(IsDBNull(reader("Status")), Nothing, CType(reader("Status"), Int32?))
                    .Label = If(IsDBNull(reader("Label")), Nothing, CType(reader("Label"), Int32?))
                    .ResourceID = If(IsDBNull(reader("ResourceID")), Nothing, CType(reader("ResourceID"), Int32?))
                    .ResourceIDs = If(IsDBNull(reader("ResourceIDs")), Nothing, reader("ResourceIDs").ToString)
                    .ReminderInfo = If(IsDBNull(reader("ReminderInfo")), Nothing, reader("ReminderInfo").ToString)
                    .RecurrenceInfo = If(IsDBNull(reader("RecurrenceInfo")), Nothing, reader("RecurrenceInfo").ToString)
                    .CustomField1 = If(IsDBNull(reader("CustomField1")), Nothing, reader("CustomField1").ToString)
                End With
            Else
                clsAppointments = Nothing
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return clsAppointments
    End Function

    Public Function Add(ByVal clsAppointments As Appointments) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim insertStatement As String _
            = "INSERT " _
            & "     [Appointments] " _
            & "     ( " _
            & "     [Type] " _
            & "    ,[StartDate] " _
            & "    ,[EndDate] " _
            & "    ,[AllDay] " _
            & "    ,[Subject] " _
            & "    ,[Location] " _
            & "    ,[Description] " _
            & "    ,[Status] " _
            & "    ,[Label] " _
            & "    ,[ResourceID] " _
            & "    ,[ResourceIDs] " _
            & "    ,[ReminderInfo] " _
            & "    ,[RecurrenceInfo] " _
            & "    ,[CustomField1] " _
            & "     ) " _
            & "VALUES " _
            & "     ( " _
            & "     @Type " _
            & "    ,@StartDate " _
            & "    ,@EndDate " _
            & "    ,@AllDay " _
            & "    ,@Subject " _
            & "    ,@Location " _
            & "    ,@Description " _
            & "    ,@Status " _
            & "    ,@Label " _
            & "    ,@ResourceID " _
            & "    ,@ResourceIDs " _
            & "    ,@ReminderInfo " _
            & "    ,@RecurrenceInfo " _
            & "    ,@CustomField1 " _
            & "     ) " _
            & ""
        Dim insertCommand As New SqlCommand(insertStatement, connection)
        insertCommand.CommandType = CommandType.Text
        insertCommand.Parameters.AddWithValue("@Type", IIf(clsAppointments.Type.HasValue, clsAppointments.Type, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@StartDate", IIf(clsAppointments.StartDate.HasValue, clsAppointments.StartDate, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@EndDate", IIf(clsAppointments.EndDate.HasValue, clsAppointments.EndDate, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@AllDay", IIf(clsAppointments.AllDay.HasValue, clsAppointments.AllDay, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Subject", IIf(Not IsNothing(clsAppointments.Subject), clsAppointments.Subject, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Location", IIf(Not IsNothing(clsAppointments.Location), clsAppointments.Location, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Description", IIf(Not IsNothing(clsAppointments.Description), clsAppointments.Description, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Status", IIf(clsAppointments.Status.HasValue, clsAppointments.Status, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Label", IIf(clsAppointments.Label.HasValue, clsAppointments.Label, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@ResourceID", IIf(clsAppointments.ResourceID.HasValue, clsAppointments.ResourceID, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@ResourceIDs", IIf(Not IsNothing(clsAppointments.ResourceIDs), clsAppointments.ResourceIDs, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@ReminderInfo", IIf(Not IsNothing(clsAppointments.ReminderInfo), clsAppointments.ReminderInfo, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@RecurrenceInfo", IIf(Not IsNothing(clsAppointments.RecurrenceInfo), clsAppointments.RecurrenceInfo, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@CustomField1", IIf(Not IsNothing(clsAppointments.CustomField1), clsAppointments.CustomField1, DBNull.Value))
        Try
            connection.Open()
            Dim count As Integer = insertCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

    Public Function Update(ByVal clsAppointmentsOld As Appointments,
           ByVal clsAppointmentsNew As Appointments) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim updateStatement As String _
            = "UPDATE " _
            & "     [Appointments] " _
            & "SET " _
            & "     [Type] = @NewType " _
            & "    ,[StartDate] = @NewStartDate " _
            & "    ,[EndDate] = @NewEndDate " _
            & "    ,[AllDay] = @NewAllDay " _
            & "    ,[Subject] = @NewSubject " _
            & "    ,[Location] = @NewLocation " _
            & "    ,[Description] = @NewDescription " _
            & "    ,[Status] = @NewStatus " _
            & "    ,[Label] = @NewLabel " _
            & "    ,[ResourceID] = @NewResourceID " _
            & "    ,[ResourceIDs] = @NewResourceIDs " _
            & "    ,[ReminderInfo] = @NewReminderInfo " _
            & "    ,[RecurrenceInfo] = @NewRecurrenceInfo " _
            & "    ,[CustomField1] = @NewCustomField1 " _
            & "WHERE " _
            & "     [UniqueID] = @OldUniqueID " _
            & ""
        Dim updateCommand As New SqlCommand(updateStatement, connection)
        updateCommand.CommandType = CommandType.Text
        updateCommand.Parameters.AddWithValue("@NewType", IIf(clsAppointmentsNew.Type.HasValue, clsAppointmentsNew.Type, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewStartDate", IIf(clsAppointmentsNew.StartDate.HasValue, clsAppointmentsNew.StartDate, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewEndDate", IIf(clsAppointmentsNew.EndDate.HasValue, clsAppointmentsNew.EndDate, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewAllDay", IIf(clsAppointmentsNew.AllDay.HasValue, clsAppointmentsNew.AllDay, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewSubject", IIf(Not IsNothing(clsAppointmentsNew.Subject), clsAppointmentsNew.Subject, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewLocation", IIf(Not IsNothing(clsAppointmentsNew.Location), clsAppointmentsNew.Location, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewDescription", IIf(Not IsNothing(clsAppointmentsNew.Description), clsAppointmentsNew.Description, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewStatus", IIf(clsAppointmentsNew.Status.HasValue, clsAppointmentsNew.Status, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewLabel", IIf(clsAppointmentsNew.Label.HasValue, clsAppointmentsNew.Label, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewResourceID", IIf(clsAppointmentsNew.ResourceID.HasValue, clsAppointmentsNew.ResourceID, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewResourceIDs", IIf(Not IsNothing(clsAppointmentsNew.ResourceIDs), clsAppointmentsNew.ResourceIDs, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewReminderInfo", IIf(Not IsNothing(clsAppointmentsNew.ReminderInfo), clsAppointmentsNew.ReminderInfo, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewRecurrenceInfo", IIf(Not IsNothing(clsAppointmentsNew.RecurrenceInfo), clsAppointmentsNew.RecurrenceInfo, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewCustomField1", IIf(Not IsNothing(clsAppointmentsNew.CustomField1), clsAppointmentsNew.CustomField1, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@OldUniqueID", clsAppointmentsOld.UniqueID)

        Try
            connection.Open()
            Dim count As Integer = updateCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

    Public Function Delete(ByVal clsAppointments As Appointments) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim deleteStatement As String _
            = "DELETE FROM " _
            & "     [Appointments] " _
            & "WHERE " _
            & "     [UniqueID] = @OldUniqueID " _
            & ""
        Dim deleteCommand As New SqlCommand(deleteStatement, connection)
        deleteCommand.CommandType = CommandType.Text

        Try
            connection.Open()
            Dim count As Integer = deleteCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

End Class
 
