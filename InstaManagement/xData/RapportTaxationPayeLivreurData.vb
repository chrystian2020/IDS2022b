Imports System.Data.SqlClient

Public Class RapportTaxationPayeLivreurData


    Public Function SelectAllDataByDateRapport(piIDRapport As String) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDRapportTaxation] " _
            & "    ,[IDPeriode] " _
            & "    ,[IDLivreur] " _
            & "    ,[Livreur] " _
            & "    ,[CheminRapport] " _
            & "    ,[NomFichier] " _
            & "    ,[Email] " _
            & "    ,[DateFrom] " _
            & "    ,[DateTo] " _
            & "FROM " _
            & "     [RapportTaxationPayeLivreur] " _
            & " WHERE  IDLivreur IS NOT NULL"

        If piIDRapport.ToString.Trim <> "" Then
            selectStatement = selectStatement & " AND IDRapportTaxation IN (" & piIDRapport & ")"
        End If

        selectStatement = selectStatement & " AND IDPeriode = " & go_Globals.IDPeriode


        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function


    Public Function SelectAllByDate(pdDateFrom As DateTime, pdDateTo As DateTime) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDRapportTaxation] " _
            & "    ,[IDPeriode] " _
            & "    ,[IDLivreur] " _
            & "    ,[Livreur] " _
            & "    ,[CheminRapport] " _
            & "    ,[NomFichier] " _
            & "    ,[Email] " _
            & "    ,[DateFrom] " _
            & "    ,[DateTo] " _
            & "FROM " _
            & "     [RapportTaxationPayeLivreur] " _
            & " WHERE (DateFrom  >= '" & pdDateFrom & "' AND DateTo <= '" & pdDateTo & "') AND IDLivreur IS NOT NULL " _
            & "AND IDPeriode = " & go_Globals.IDPeriode


        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function


    Public Function SelectAll() As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [RapportTaxationPayeLivreur].[IDRapportTaxation] " _
            & "    ,[RapportTaxationPayeLivreur].[IDPeriode] " _
            & "    ,[RapportTaxationPayeLivreur].[IDLivreur] " _
            & "    ,[RapportTaxationPayeLivreur].[Livreur] " _
            & "    ,[RapportTaxationPayeLivreur].[CheminRapport] " _
            & "    ,[RapportTaxationPayeLivreur].[NomFichier] " _
            & "    ,[RapportTaxationPayeLivreur].[Email] " _
            & "    ,[RapportTaxationPayeLivreur].[DateFrom] " _
            & "    ,[RapportTaxationPayeLivreur].[DateTo] " _
            & " FROM " _
            & "     [RapportTaxationPayeLivreur] " _
            & ""
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function

    Public Function Select_Record(ByVal clsRapportTaxationPayeLivreurPara As RapportTaxationPayeLivreur) As RapportTaxationPayeLivreur
        Dim clsRapportTaxationPayeLivreur As New RapportTaxationPayeLivreur
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDRapportTaxation] " _
            & "    ,[IDPeriode] " _
            & "    ,[IDLivreur] " _
            & "    ,[Livreur] " _
            & "    ,[CheminRapport] " _
            & "    ,[NomFichier] " _
            & "    ,[Email] " _
            & "    ,[DateFrom] " _
            & "    ,[DateTo] " _
            & "FROM " _
            & "     [RapportTaxationPayeLivreur] " _
            & "WHERE " _
            & "     [IDRapportTaxation] = @IDRapportTaxation " _
            & ""
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        selectCommand.Parameters.AddWithValue("@IDRapportTaxation", clsRapportTaxationPayeLivreurPara.IDRapportTaxation)
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader(CommandBehavior.SingleRow)
            If reader.Read Then
                With clsRapportTaxationPayeLivreur
                    .IDRapportTaxation = System.Convert.ToInt32(reader("IDRapportTaxation"))
                    .IDPeriode = If(IsDBNull(reader("IDPeriode")), Nothing, CType(reader("IDPeriode"), Int32?))
                    .IDLivreur = If(IsDBNull(reader("IDLivreur")), Nothing, CType(reader("IDLivreur"), Int32?))
                    .Livreur = If(IsDBNull(reader("Livreur")), Nothing, reader("Livreur").ToString)
                    .CheminRapport = If(IsDBNull(reader("CheminRapport")), Nothing, reader("CheminRapport").ToString)
                    .NomFichier = If(IsDBNull(reader("NomFichier")), Nothing, reader("NomFichier").ToString)
                    .Email = If(IsDBNull(reader("Email")), Nothing, reader("Email").ToString)
                    .DateFrom = If(IsDBNull(reader("DateFrom")), Nothing, CType(reader("DateFrom"), DateTime?))
                    .DateTo = If(IsDBNull(reader("DateTo")), Nothing, CType(reader("DateTo"), DateTime?))
                End With
            Else
                clsRapportTaxationPayeLivreur = Nothing
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return clsRapportTaxationPayeLivreur
    End Function

    Public Function Add(ByVal clsRapportTaxationPayeLivreur As RapportTaxationPayeLivreur) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim insertStatement As String _
            = "INSERT " _
            & "     [RapportTaxationPayeLivreur] " _
            & "     ( " _
            & "     [IDPeriode] " _
            & "    ,[IDLivreur] " _
            & "    ,[Livreur] " _
            & "    ,[CheminRapport] " _
            & "    ,[NomFichier] " _
            & "    ,[Email] " _
            & "    ,[DateFrom] " _
            & "    ,[DateTo] " _
            & "     ) " _
            & "VALUES " _
            & "     ( " _
            & "     @IDPeriode " _
            & "    ,@IDLivreur " _
            & "    ,@Livreur " _
            & "    ,@CheminRapport " _
            & "    ,@NomFichier " _
            & "    ,@Email " _
            & "    ,@DateFrom " _
            & "    ,@DateTo " _
            & "     ) " _
            & ""
        Dim insertCommand As New SqlCommand(insertStatement, connection)
        insertCommand.CommandType = CommandType.Text
        insertCommand.Parameters.AddWithValue("@IDPeriode", IIf(clsRapportTaxationPayeLivreur.IDPeriode.HasValue, clsRapportTaxationPayeLivreur.IDPeriode, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@IDLivreur", IIf(clsRapportTaxationPayeLivreur.IDLivreur.HasValue, clsRapportTaxationPayeLivreur.IDLivreur, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Livreur", IIf(Not IsNothing(clsRapportTaxationPayeLivreur.Livreur), clsRapportTaxationPayeLivreur.Livreur, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@CheminRapport", IIf(Not IsNothing(clsRapportTaxationPayeLivreur.CheminRapport), clsRapportTaxationPayeLivreur.CheminRapport, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@NomFichier", IIf(Not IsNothing(clsRapportTaxationPayeLivreur.NomFichier), clsRapportTaxationPayeLivreur.NomFichier, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Email", IIf(Not IsNothing(clsRapportTaxationPayeLivreur.Email), clsRapportTaxationPayeLivreur.Email, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@DateFrom", IIf(clsRapportTaxationPayeLivreur.DateFrom.HasValue, clsRapportTaxationPayeLivreur.DateFrom, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@DateTo", IIf(clsRapportTaxationPayeLivreur.DateTo.HasValue, clsRapportTaxationPayeLivreur.DateTo, DBNull.Value))
        Try
            connection.Open()
            Dim count As Integer = insertCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

    Public Function Update(ByVal clsRapportTaxationPayeLivreurOld As RapportTaxationPayeLivreur,
           ByVal clsRapportTaxationPayeLivreurNew As RapportTaxationPayeLivreur) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim updateStatement As String _
            = "UPDATE " _
            & "     [RapportTaxationPayeLivreur] " _
            & "SET " _
            & "     [IDPeriode] = @NewIDPeriode " _
            & "    ,[IDLivreur] = @NewIDLivreur " _
            & "    ,[Livreur] = @NewLivreur " _
            & "    ,[CheminRapport] = @NewCheminRapport " _
            & "    ,[NomFichier] = @NewNomFichier " _
            & "    ,[Email] = @NewEmail " _
            & "    ,[DateFrom] = @NewDateFrom " _
            & "    ,[DateTo] = @NewDateTo " _
            & "WHERE " _
            & "     [IDRapportTaxation] = @OldIDRapportTaxation " _
            & ""
        Dim updateCommand As New SqlCommand(updateStatement, connection)
        updateCommand.CommandType = CommandType.Text
        updateCommand.Parameters.AddWithValue("@NewIDPeriode", IIf(clsRapportTaxationPayeLivreurNew.IDPeriode.HasValue, clsRapportTaxationPayeLivreurNew.IDPeriode, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewIDLivreur", IIf(clsRapportTaxationPayeLivreurNew.IDLivreur.HasValue, clsRapportTaxationPayeLivreurNew.IDLivreur, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewLivreur", IIf(Not IsNothing(clsRapportTaxationPayeLivreurNew.Livreur), clsRapportTaxationPayeLivreurNew.Livreur, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewCheminRapport", IIf(Not IsNothing(clsRapportTaxationPayeLivreurNew.CheminRapport), clsRapportTaxationPayeLivreurNew.CheminRapport, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewNomFichier", IIf(Not IsNothing(clsRapportTaxationPayeLivreurNew.NomFichier), clsRapportTaxationPayeLivreurNew.NomFichier, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewEmail", IIf(Not IsNothing(clsRapportTaxationPayeLivreurNew.Email), clsRapportTaxationPayeLivreurNew.Email, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewDateFrom", IIf(clsRapportTaxationPayeLivreurNew.DateFrom.HasValue, clsRapportTaxationPayeLivreurNew.DateFrom, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewDateTo", IIf(clsRapportTaxationPayeLivreurNew.DateTo.HasValue, clsRapportTaxationPayeLivreurNew.DateTo, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@OldIDRapportTaxation", clsRapportTaxationPayeLivreurOld.IDRapportTaxation)

        Try
            connection.Open()
            Dim count As Integer = updateCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function


    Public Function DeletePeriode(pIDPeriode As Integer) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim deleteStatement As String _
            = "DELETE FROM " _
            & "     [RapportTaxationPayeLivreur] " _
            & "WHERE " _
            & "     [IDPeriode] = " & pIDPeriode

        Dim deleteCommand As New SqlCommand(deleteStatement, connection)
        deleteCommand.CommandType = CommandType.Text
        Try
            connection.Open()
            Dim count As Integer = deleteCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

    Public Function Delete(ByVal clsRapportTaxationPayeLivreur As RapportTaxationPayeLivreur) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim deleteStatement As String _
            = "DELETE FROM " _
            & "     [RapportTaxationPayeLivreur] " _
            & "WHERE " _
            & "     [IDRapportTaxation] = @OldIDRapportTaxation " _
            & ""
        Dim deleteCommand As New SqlCommand(deleteStatement, connection)
        deleteCommand.CommandType = CommandType.Text
        deleteCommand.Parameters.AddWithValue("@OldIDRapportTaxation", clsRapportTaxationPayeLivreur.IDRapportTaxation)
        Try
            connection.Open()
            Dim count As Integer = deleteCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

End Class
 
