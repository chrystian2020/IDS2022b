Imports System.Data.SqlClient

Public Class CommissionLineData
    Public Function DeleteParCommission(ByVal clsCommissionLine As CommissionLine) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim deleteStatement As String _
            = "DELETE FROM " _
            & "     [CommissionLine] " _
            & "WHERE " _
            & "     [IDCommission] = @OldIDCommission "

        Dim deleteCommand As New SqlCommand(deleteStatement, connection)
        deleteCommand.CommandType = CommandType.Text
        deleteCommand.Parameters.AddWithValue("@OldIDCommission", clsCommissionLine.IDCommission)

        Try
            connection.Open()
            Dim count As Integer = deleteCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

    Public Function SelectAllByDateCommission(piIDCommission As Integer) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDCommissionLine] " _
            & "    ,[IDCommission] " _
            & "    ,[CommissionDate] " _
            & "    ,[CommissionNumber] " _
            & "    ,[IDLivreur] " _
            & "    ,[Livreur] " _
            & "    ,[OrganisationID] " _
            & "    ,[Organisation] " _
            & "    ,[Code] " _
            & "    ,[Km] " _
            & "    ,[Description] " _
            & "    ,[Unite] " _
            & "    ,[Prix] " _
            & "    ,[Montant] " _
            & "    ,[Credit] " _
            & "    ,[DateFrom] " _
            & "    ,[DateTo] " _
            & "    ,[NombreGlacier] " _
            & "    ,[Glacier] " _
            & "    ,[IDPeriode] " _
            & "    ,[TypeAjout] " _
            & "    ,[Ajout] " _
            & "    ,[IsRestaurant] " _
            & "FROM " _
            & "     [CommissionLine] " _
            & " WHERE  IDCommission = " & piIDCommission & " And IDPeriode = " & go_Globals.IDPeriode
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function
    Public Function SelectAllByID(piIDCommission As Integer) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDCommissionLine] " _
            & "    ,[IDCommission] " _
            & "    ,[CommissionDate] " _
            & "    ,[CommissionNumber] " _
            & "    ,[IDLivreur] " _
            & "    ,[Livreur] " _
            & "    ,[OrganisationID] " _
            & "    ,[Organisation] " _
            & "    ,[Code] " _
            & "    ,[Km] " _
            & "    ,[Description] " _
            & "    ,[Unite] " _
            & "    ,[Prix] " _
            & "    ,[Montant] " _
            & "    ,[Credit] " _
            & "    ,[DateFrom] " _
            & "    ,[DateTo] " _
            & "    ,[NombreGlacier] " _
            & "    ,[Glacier] " _
            & "    ,[IDPeriode] " _
            & "    ,[TypeAjout] " _
            & "    ,[Ajout] " _
            & "    ,[IsRestaurant] " _
            & "FROM " _
            & "     [CommissionLine] " _
            & " WHERE IDCommission =" & piIDCommission
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function

    Public Function DeleteByPeriodLivreurCommissionLivreur(ByVal clsCommissionLine As CommissionLine) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim deleteStatement As String _
            = "DELETE FROM " _
            & " [CommissionLine] " _
            & " WHERE " _
            & " IDPeriode = @OldIDPeriode " _
            & " AND IDLivreur = @OldIDLivreur AND Ajout <> 1 AND IsRestaurant = 0"

        Dim deleteCommand As New SqlCommand(deleteStatement, connection)
        deleteCommand.CommandType = CommandType.Text
        deleteCommand.Parameters.AddWithValue("@OldIDPeriode", clsCommissionLine.IDPeriode)
        deleteCommand.Parameters.AddWithValue("@OldIDLivreur", clsCommissionLine.IDLivreur)

        Try
            connection.Open()
            Dim count As Integer = deleteCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function


    Public Function DeleteByPeriodLivreurCommission(ByVal clsCommissionLine As CommissionLine) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim deleteStatement As String _
            = "DELETE FROM " _
            & " [CommissionLine] " _
            & " WHERE " _
            & " IDPeriode = @OldIDPeriode " _
            & " AND IDLivreur = @OldIDLivreur AND Ajout <> 1 AND IsRestaurant = 1"

        Dim deleteCommand As New SqlCommand(deleteStatement, connection)
        deleteCommand.CommandType = CommandType.Text
        deleteCommand.Parameters.AddWithValue("@OldIDPeriode", clsCommissionLine.IDPeriode)
        deleteCommand.Parameters.AddWithValue("@OldIDLivreur", clsCommissionLine.IDLivreur)

        Try
            connection.Open()
            Dim count As Integer = deleteCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

    Public Function DeleteByPeriodCommissionLivreur(ByVal clsCommissionLine As CommissionLine) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim deleteStatement As String _
            = "DELETE FROM " _
            & "     [CommissionLine] " _
            & " WHERE " _
            & " IDPeriode = @OldIDPeriode AND Ajout <> 1 AND IsRestaurant = 0"

        Dim deleteCommand As New SqlCommand(deleteStatement, connection)
        deleteCommand.CommandType = CommandType.Text
        deleteCommand.Parameters.AddWithValue("@OldIDPeriode", clsCommissionLine.IDPeriode)

        Try
            connection.Open()
            Dim count As Integer = deleteCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function
    Public Function DeleteByPeriodCommission(ByVal clsCommissionLine As CommissionLine) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim deleteStatement As String _
            = "DELETE FROM " _
            & "     [CommissionLine] " _
            & " WHERE " _
            & " IDPeriode = @OldIDPeriode AND Ajout <> 1 AND IsRestaurant = 1"

        Dim deleteCommand As New SqlCommand(deleteStatement, connection)
        deleteCommand.CommandType = CommandType.Text
        deleteCommand.Parameters.AddWithValue("@OldIDPeriode", clsCommissionLine.IDPeriode)

        Try
            connection.Open()
            Dim count As Integer = deleteCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function
    Public Function SelectAll() As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
             & "     [IDCommissionLine] " _
            & "    ,[IDCommission] " _
            & "    ,[CommissionDate] " _
            & "    ,[CommissionNumber] " _
            & "    ,[IDLivreur] " _
            & "    ,[Livreur] " _
            & "    ,[OrganisationID] " _
            & "    ,[Organisation] " _
            & "    ,[Code] " _
            & "    ,[Km] " _
            & "    ,[Description] " _
            & "    ,[Unite] " _
            & "    ,[Prix] " _
            & "    ,[Montant] " _
            & "    ,[Credit] " _
            & "    ,[DateFrom] " _
            & "    ,[DateTo] " _
            & "    ,[NombreGlacier] " _
            & "    ,[Glacier] " _
            & "    ,[IDPeriode] " _
            & "    ,[TypeAjout] " _
            & "    ,[Ajout] " _
            & "    ,[IsRestaurant] " _
            & "FROM " _
            & "     [CommissionLine] " _
            & ""
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function

    Public Function Select_Record(ByVal clsCommissionLinePara As CommissionLine) As CommissionLine
        Dim clsCommissionLine As New CommissionLine
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
                 & "     [IDCommissionLine] " _
            & "    ,[IDCommission] " _
            & "    ,[CommissionDate] " _
            & "    ,[CommissionNumber] " _
            & "    ,[IDLivreur] " _
            & "    ,[Livreur] " _
            & "    ,[OrganisationID] " _
            & "    ,[Organisation] " _
            & "    ,[Code] " _
            & "    ,[Km] " _
            & "    ,[Description] " _
            & "    ,[Unite] " _
            & "    ,[Prix] " _
            & "    ,[Montant] " _
            & "    ,[Credit] " _
            & "    ,[DateFrom] " _
            & "    ,[DateTo] " _
            & "    ,[NombreGlacier] " _
            & "    ,[Glacier] " _
            & "    ,[IDPeriode] " _
            & "    ,[TypeAjout] " _
            & "    ,[Ajout] " _
            & "    ,[IsRestaurant] " _
            & "FROM " _
            & "     [CommissionLine] " _
            & "WHERE " _
            & "     [IDCommissionLine] = @IDCommissionLine " _
            & ""
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        selectCommand.Parameters.AddWithValue("@IDCommissionLine", clsCommissionLinePara.IDCommissionLine)
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader(CommandBehavior.SingleRow)
            If reader.Read Then
                With clsCommissionLine
                    .IDCommissionLine = System.Convert.ToInt32(reader("IDCommissionLine"))
                    .IDCommission = If(IsDBNull(reader("IDCommission")), Nothing, CType(reader("IDCommission"), Int32?))
                    .CommissionDate = If(IsDBNull(reader("CommissionDate")), Nothing, CType(reader("CommissionDate"), DateTime?))
                    .CommissionNumber = If(IsDBNull(reader("CommissionNumber")), Nothing, reader("CommissionNumber").ToString)
                    .IDLivreur = If(IsDBNull(reader("IDLivreur")), Nothing, CType(reader("IDLivreur"), Int32?))
                    .Livreur = If(IsDBNull(reader("Livreur")), Nothing, reader("Livreur").ToString)
                    .OrganisationID = If(IsDBNull(reader("OrganisationID")), Nothing, CType(reader("OrganisationID"), Int32?))
                    .Organisation = If(IsDBNull(reader("Organisation")), Nothing, reader("Organisation").ToString)
                    .Code = If(IsDBNull(reader("Code")), Nothing, reader("Code").ToString)
                    .Km = If(IsDBNull(reader("Km")), Nothing, CType(CType(reader("Km"), Double?), Decimal?))
                    .Description = If(IsDBNull(reader("Description")), Nothing, reader("Description").ToString)
                    .Unite = If(IsDBNull(reader("Unite")), Nothing, CType(CType(reader("Unite"), Double?), Decimal?))
                    .Prix = If(IsDBNull(reader("Prix")), Nothing, CType(reader("Prix"), Decimal?))
                    .Montant = If(IsDBNull(reader("Montant")), Nothing, CType(reader("Montant"), Decimal?))
                    .Credit = If(IsDBNull(reader("Credit")), Nothing, CType(reader("Credit"), Decimal?))
                    .DateFrom = If(IsDBNull(reader("DateFrom")), Nothing, CType(reader("DateFrom"), DateTime?))
                    .DateTo = If(IsDBNull(reader("DateTo")), Nothing, CType(reader("DateTo"), DateTime?))
                    .NombreGlacier = If(IsDBNull(reader("NombreGlacier")), Nothing, CType(reader("NombreGlacier"), Int32?))
                    .Glacier = If(IsDBNull(reader("Glacier")), Nothing, CType(reader("Glacier"), Decimal?))
                    .IDPeriode = If(IsDBNull(reader("IDPeriode")), Nothing, CType(reader("IDPeriode"), Int32?))
                    .TypeAjout = If(IsDBNull(reader("TypeAjout")), Nothing, CType(reader("TypeAjout"), Int32?))
                    .Ajout = If(IsDBNull(reader("Ajout")), Nothing, CType(reader("Ajout"), Boolean?))
                    .IsRestaurant = If(IsDBNull(reader("IsRestaurant")), Nothing, CType(reader("IsRestaurant"), Boolean?))
                End With
            Else
                clsCommissionLine = Nothing
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return clsCommissionLine
    End Function

    Public Function Add(ByVal clsCommissionLine As CommissionLine) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim insertStatement As String _
            = "INSERT " _
            & "     [CommissionLine] " _
            & "     ( " _
            & "     [IDCommission] " _
            & "    ,[CommissionDate] " _
            & "    ,[CommissionNumber] " _
            & "    ,[IDLivreur] " _
            & "    ,[Livreur] " _
            & "    ,[OrganisationID] " _
            & "    ,[Organisation] " _
            & "    ,[Code] " _
            & "    ,[Km] " _
            & "    ,[Description] " _
            & "    ,[Unite] " _
            & "    ,[Prix] " _
            & "    ,[Montant] " _
            & "    ,[Credit] " _
            & "    ,[DateFrom] " _
            & "    ,[DateTo] " _
            & "    ,[NombreGlacier] " _
            & "    ,[Glacier] " _
            & "    ,[IDPeriode] " _
            & "    ,[TypeAjout] " _
            & "    ,[Ajout] " _
            & "    ,[IsRestaurant] " _
            & "     ) " _
            & "VALUES " _
            & "     ( " _
            & "     @IDCommission " _
            & "    ,@CommissionDate " _
            & "    ,@CommissionNumber " _
            & "    ,@IDLivreur " _
            & "    ,@Livreur " _
            & "    ,@OrganisationID " _
            & "    ,@Organisation " _
            & "    ,@Code " _
            & "    ,@Km " _
            & "    ,@Description " _
            & "    ,@Unite " _
            & "    ,@Prix " _
            & "    ,@Montant " _
            & "    ,@Credit " _
            & "    ,@DateFrom " _
            & "    ,@DateTo " _
            & "    ,@NombreGlacier " _
            & "    ,@Glacier " _
            & "    ,@IDPeriode " _
            & "    ,@TypeAjout " _
            & "    ,@Ajout " _
            & "    ,@IsRestaurant " _
            & "     ) " _
            & ""
        Dim insertCommand As New SqlCommand(insertStatement, connection)
        insertCommand.CommandType = CommandType.Text
        insertCommand.Parameters.AddWithValue("@IDCommission", IIf(clsCommissionLine.IDCommission.HasValue, clsCommissionLine.IDCommission, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@CommissionDate", IIf(clsCommissionLine.CommissionDate.HasValue, clsCommissionLine.CommissionDate, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@CommissionNumber", IIf(Not IsNothing(clsCommissionLine.CommissionNumber), clsCommissionLine.CommissionNumber, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@IDLivreur", IIf(clsCommissionLine.IDLivreur.HasValue, clsCommissionLine.IDLivreur, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Livreur", IIf(Not IsNothing(clsCommissionLine.Livreur), clsCommissionLine.Livreur, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@OrganisationID", IIf(clsCommissionLine.OrganisationID.HasValue, clsCommissionLine.OrganisationID, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Organisation", IIf(Not IsNothing(clsCommissionLine.Organisation), clsCommissionLine.Organisation, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Code", IIf(Not IsNothing(clsCommissionLine.Code), clsCommissionLine.Code, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Km", IIf(clsCommissionLine.Km.HasValue, clsCommissionLine.Km, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Description", IIf(Not IsNothing(clsCommissionLine.Description), clsCommissionLine.Description, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Unite", IIf(clsCommissionLine.Unite.HasValue, clsCommissionLine.Unite, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Prix", IIf(clsCommissionLine.Prix.HasValue, clsCommissionLine.Prix, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Montant", IIf(clsCommissionLine.Montant.HasValue, clsCommissionLine.Montant, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Credit", IIf(clsCommissionLine.Credit.HasValue, clsCommissionLine.Credit, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@DateFrom", IIf(clsCommissionLine.DateFrom.HasValue, clsCommissionLine.DateFrom, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@DateTo", IIf(clsCommissionLine.DateTo.HasValue, clsCommissionLine.DateTo, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@NombreGlacier", IIf(clsCommissionLine.NombreGlacier.HasValue, clsCommissionLine.NombreGlacier, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Glacier", IIf(clsCommissionLine.Glacier.HasValue, clsCommissionLine.Glacier, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@IDPeriode", IIf(clsCommissionLine.IDPeriode.HasValue, clsCommissionLine.IDPeriode, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@TypeAjout", IIf(clsCommissionLine.TypeAjout.HasValue, clsCommissionLine.TypeAjout, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Ajout", IIf(clsCommissionLine.Ajout.HasValue, clsCommissionLine.Ajout, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@IsRestaurant", IIf(clsCommissionLine.IsRestaurant.HasValue, clsCommissionLine.IsRestaurant, DBNull.Value))
        Try
            connection.Open()
            Dim count As Integer = insertCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

    Public Function Update(ByVal clsCommissionLineOld As CommissionLine,
           ByVal clsCommissionLineNew As CommissionLine) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim updateStatement As String _
            = "UPDATE " _
            & "     [CommissionLine] " _
            & "SET " _
            & "     [IDCommission] = @NewIDCommission " _
            & "    ,[CommissionDate] = @NewCommissionDate " _
            & "    ,[CommissionNumber] = @NewCommissionNumber " _
            & "    ,[IDLivreur] = @NewIDLivreur " _
            & "    ,[Livreur] = @NewLivreur " _
            & "    ,[OrganisationID] = @NewOrganisationID " _
            & "    ,[Organisation] = @NewOrganisation " _
            & "    ,[Code] = @NewCode " _
            & "    ,[Km] = @NewKm " _
            & "    ,[Description] = @NewDescription " _
            & "    ,[Unite] = @NewUnite " _
            & "    ,[Prix] = @NewPrix " _
            & "    ,[Montant] = @NewMontant " _
            & "    ,[Credit] = @NewCredit " _
            & "    ,[DateFrom] = @NewDateFrom " _
            & "    ,[DateTo] = @NewDateTo " _
            & "    ,[NombreGlacier] = @NewNombreGlacier " _
            & "    ,[Glacier] = @NewGlacier " _
            & "    ,[IDPeriode] = @NewIDPeriode " _
            & "    ,[TypeAjout] = @NewTypeAjout " _
            & "    ,[Ajout] = @NewAjout " _
            & "    ,[IsRestaurant] = @NewIsRestaurant " _
            & "WHERE " _
            & "     [IDCommissionLine] = @OldIDCommissionLine " _
            & ""
        Dim updateCommand As New SqlCommand(updateStatement, connection)
        updateCommand.CommandType = CommandType.Text
        updateCommand.Parameters.AddWithValue("@NewIDCommission", IIf(clsCommissionLineNew.IDCommission.HasValue, clsCommissionLineNew.IDCommission, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewCommissionDate", IIf(clsCommissionLineNew.CommissionDate.HasValue, clsCommissionLineNew.CommissionDate, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewCommissionNumber", IIf(Not IsNothing(clsCommissionLineNew.CommissionNumber), clsCommissionLineNew.CommissionNumber, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewIDLivreur", IIf(clsCommissionLineNew.IDLivreur.HasValue, clsCommissionLineNew.IDLivreur, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewLivreur", IIf(Not IsNothing(clsCommissionLineNew.Livreur), clsCommissionLineNew.Livreur, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewOrganisationID", IIf(clsCommissionLineNew.OrganisationID.HasValue, clsCommissionLineNew.OrganisationID, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewOrganisation", IIf(Not IsNothing(clsCommissionLineNew.Organisation), clsCommissionLineNew.Organisation, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewCode", IIf(Not IsNothing(clsCommissionLineNew.Code), clsCommissionLineNew.Code, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewKm", IIf(clsCommissionLineNew.Km.HasValue, clsCommissionLineNew.Km, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewDescription", IIf(Not IsNothing(clsCommissionLineNew.Description), clsCommissionLineNew.Description, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewUnite", IIf(clsCommissionLineNew.Unite.HasValue, clsCommissionLineNew.Unite, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewPrix", IIf(clsCommissionLineNew.Prix.HasValue, clsCommissionLineNew.Prix, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewMontant", IIf(clsCommissionLineNew.Montant.HasValue, clsCommissionLineNew.Montant, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewCredit", IIf(clsCommissionLineNew.Credit.HasValue, clsCommissionLineNew.Credit, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewDateFrom", IIf(clsCommissionLineNew.DateFrom.HasValue, clsCommissionLineNew.DateFrom, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewDateTo", IIf(clsCommissionLineNew.DateTo.HasValue, clsCommissionLineNew.DateTo, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewNombreGlacier", IIf(clsCommissionLineNew.NombreGlacier.HasValue, clsCommissionLineNew.NombreGlacier, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewGlacier", IIf(clsCommissionLineNew.Glacier.HasValue, clsCommissionLineNew.Glacier, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewIDPeriode", IIf(clsCommissionLineNew.IDPeriode.HasValue, clsCommissionLineNew.IDPeriode, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewTypeAjout", IIf(clsCommissionLineNew.TypeAjout.HasValue, clsCommissionLineNew.TypeAjout, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewAjout", IIf(clsCommissionLineNew.Ajout.HasValue, clsCommissionLineNew.Ajout, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewIsRestaurant", IIf(clsCommissionLineNew.IsRestaurant.HasValue, clsCommissionLineNew.IsRestaurant, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@OldIDCommissionLine", clsCommissionLineOld.IDCommissionLine)
        Try
            connection.Open()
            Dim count As Integer = updateCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

    Public Function Delete(ByVal clsCommissionLine As CommissionLine) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim deleteStatement As String _
            = "DELETE FROM " _
            & "     [CommissionLine] " _
            & "WHERE " _
            & "     [IDCommissionLine] = @OldIDCommissionLine " _
            & ""
        Dim deleteCommand As New SqlCommand(deleteStatement, connection)
        deleteCommand.CommandType = CommandType.Text
        deleteCommand.Parameters.AddWithValue("@OldIDCommissionLine", clsCommissionLine.IDCommissionLine)
        Try
            connection.Open()
            Dim count As Integer = deleteCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

End Class

