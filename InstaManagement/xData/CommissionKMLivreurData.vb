Imports System.Data.SqlClient

Public Class CommissionKMLivreurData
    Public Function SelectAllByTimeKM(piOrganisation As Integer, pDateCompare As DateTime, pdblDistance As Double) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT  * " _
            & "FROM " _
            & "     [CommissionKMLivreur]  WHERE OrganisationID = @Organisation " _
            & " AND (CAST(@DateCompare as time) between CAST(HeureDepart as time) and CAST(HeureFin as time)) " _
            & " AND (@KM between KMFrom and KMTo)"


        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        selectCommand.Parameters.AddWithValue("@Organisation", piOrganisation)
        selectCommand.Parameters.AddWithValue("@DateCompare", pDateCompare)
        selectCommand.Parameters.AddWithValue("@KM", pdblDistance)

        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Erreur")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function
    Public Function SelectAllByKM(piOrganisationID As Integer, pdblDistance As Double) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT  * " _
            & "FROM " _
            & "     [CommissionKMLivreur]  WHERE OrganisationID = @OrganisationID " _
            & " AND (@KM between KMFrom and KMTo)"


        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        selectCommand.Parameters.AddWithValue("@OrganisationID", piOrganisationID)
        selectCommand.Parameters.AddWithValue("@KM", pdblDistance)

        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Erreur")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function
    Public Function SelectAllByName(piOrganisation As Integer) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDCommissionKMLivreur] " _
            & "    ,[OrganisationID] " _
            & "    ,[Organisation] " _
            & "    ,[KMFrom] " _
            & "    ,[KMTo] " _
            & "    ,[Montant] " _
            & "    ,[ExtraChargeParKM] " _
            & "    ,[PerKMNumber] " _
            & "    ,[MetodeCalculation] " _
            & "    ,[HeureDepart] " _
            & "    ,[HeureFin] " _
            & "FROM " _
            & "     [CommissionKMLivreur] " _
            & " WHERE OrganisationID =" & piOrganisation
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Erreur")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function
    Public Function SelectAll() As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDCommissionKMLivreur] " _
            & "    ,[OrganisationID] " _
            & "    ,[Organisation] " _
            & "    ,[KMFrom] " _
            & "    ,[KMTo] " _
            & "    ,[Montant] " _
            & "    ,[ExtraChargeParKM] " _
            & "    ,[PerKMNumber] " _
            & "    ,[MetodeCalculation] " _
            & "    ,[HeureDepart] " _
            & "    ,[HeureFin] " _
            & "FROM " _
            & "     [CommissionKMLivreur] " _
            & ""
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function

    Public Function Select_Record(ByVal clsCommissionKMLivreurPara As CommissionKMLivreur) As CommissionKMLivreur
        Dim clsCommissionKMLivreur As New CommissionKMLivreur
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDCommissionKMLivreur] " _
            & "    ,[OrganisationID] " _
            & "    ,[Organisation] " _
            & "    ,[KMFrom] " _
            & "    ,[KMTo] " _
            & "    ,[Montant] " _
            & "    ,[ExtraChargeParKM] " _
            & "    ,[PerKMNumber] " _
            & "    ,[MetodeCalculation] " _
            & "    ,[HeureDepart] " _
            & "    ,[HeureFin] " _
            & "FROM " _
            & "     [CommissionKMLivreur] " _
            & "WHERE " _
            & "     [IDCommissionKMLivreur] = @IDCommissionKMLivreur " _
            & ""
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        selectCommand.Parameters.AddWithValue("@IDCommissionKMLivreur", clsCommissionKMLivreurPara.IDCommissionKMLivreur)
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader(CommandBehavior.SingleRow)
            If reader.Read Then
                With clsCommissionKMLivreur
                    .IDCommissionKMLivreur = System.Convert.ToInt32(reader("IDCommissionKMLivreur"))
                    .OrganisationID = System.Convert.ToInt32(reader("OrganisationID"))
                    .Organisation = If(IsDBNull(reader("Organisation")), Nothing, reader("Organisation").ToString)
                    .KMFrom = If(IsDBNull(reader("KMFrom")), Nothing, CType(CType(reader("KMFrom"), Double?), Decimal?))
                    .KMTo = If(IsDBNull(reader("KMTo")), Nothing, CType(CType(reader("KMTo"), Double?), Decimal?))
                    .Montant = If(IsDBNull(reader("Montant")), Nothing, CType(reader("Montant"), Decimal?))
                    .ExtraChargeParKM = If(IsDBNull(reader("ExtraChargeParKM")), Nothing, CType(reader("ExtraChargeParKM"), Decimal?))
                    .PerKMNumber = If(IsDBNull(reader("PerKMNumber")), Nothing, CType(reader("PerKMNumber"), Decimal?))
                    .MetodeCalculation = If(IsDBNull(reader("MetodeCalculation")), Nothing, CType(reader("MetodeCalculation"), Int32?))
                    .HeureDepart = If(IsDBNull(reader("HeureDepart")), Nothing, CType(reader("HeureDepart"), DateTime?))
                    .HeureFin = If(IsDBNull(reader("HeureFin")), Nothing, CType(reader("HeureFin"), DateTime?))
                End With
            Else
                clsCommissionKMLivreur = Nothing
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return clsCommissionKMLivreur
    End Function

    Public Function Add(ByVal clsCommissionKMLivreur As CommissionKMLivreur) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim insertStatement As String _
            = "INSERT " _
            & "     [CommissionKMLivreur] " _
            & "     ( " _
            & "     [OrganisationID] " _
            & "    ,[Organisation] " _
            & "    ,[KMFrom] " _
            & "    ,[KMTo] " _
            & "    ,[Montant] " _
            & "    ,[ExtraChargeParKM] " _
            & "    ,[PerKMNumber] " _
            & "    ,[MetodeCalculation] " _
            & "    ,[HeureDepart] " _
            & "    ,[HeureFin] " _
            & "     ) " _
            & "VALUES " _
            & "     ( " _
            & "     @OrganisationID " _
            & "    ,@Organisation " _
            & "    ,@KMFrom " _
            & "    ,@KMTo " _
            & "    ,@Montant " _
            & "    ,@ExtraChargeParKM " _
            & "    ,@PerKMNumber " _
            & "    ,@MetodeCalculation " _
            & "    ,@HeureDepart " _
            & "    ,@HeureFin " _
            & "     ) " _
            & ""
        Dim insertCommand As New SqlCommand(insertStatement, connection)
        insertCommand.CommandType = CommandType.Text
        insertCommand.Parameters.AddWithValue("@OrganisationID", IIf(clsCommissionKMLivreur.OrganisationID.HasValue, clsCommissionKMLivreur.OrganisationID, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Organisation", IIf(Not IsNothing(clsCommissionKMLivreur.Organisation), clsCommissionKMLivreur.Organisation, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@KMFrom", IIf(clsCommissionKMLivreur.KMFrom.HasValue, clsCommissionKMLivreur.KMFrom, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@KMTo", IIf(clsCommissionKMLivreur.KMTo.HasValue, clsCommissionKMLivreur.KMTo, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Montant", IIf(clsCommissionKMLivreur.Montant.HasValue, clsCommissionKMLivreur.Montant, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@ExtraChargeParKM", IIf(clsCommissionKMLivreur.ExtraChargeParKM.HasValue, clsCommissionKMLivreur.ExtraChargeParKM, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@PerKMNumber", IIf(clsCommissionKMLivreur.PerKMNumber.HasValue, clsCommissionKMLivreur.PerKMNumber, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@MetodeCalculation", IIf(clsCommissionKMLivreur.MetodeCalculation.HasValue, clsCommissionKMLivreur.MetodeCalculation, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@HeureDepart", IIf(clsCommissionKMLivreur.HeureDepart.HasValue, clsCommissionKMLivreur.HeureDepart, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@HeureFin", IIf(clsCommissionKMLivreur.HeureFin.HasValue, clsCommissionKMLivreur.HeureFin, DBNull.Value))
        Try
            connection.Open()
            Dim count As Integer = insertCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

    Public Function Update(ByVal clsCommissionKMLivreurOld As CommissionKMLivreur,
           ByVal clsCommissionKMLivreurNew As CommissionKMLivreur) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim updateStatement As String _
            = "UPDATE " _
            & "     [CommissionKMLivreur] " _
            & "SET " _
            & "     [OrganisationID] = @NewOrganisationID " _
            & "    ,[Organisation] = @NewOrganisation " _
            & "    ,[KMFrom] = @NewKMFrom " _
            & "    ,[KMTo] = @NewKMTo " _
            & "    ,[Montant] = @NewMontant " _
            & "    ,[ExtraChargeParKM] = @NewExtraChargeParKM " _
            & "    ,[PerKMNumber] = @NewPerKMNumber " _
            & "    ,[MetodeCalculation] = @NewMetodeCalculation " _
            & "    ,[HeureDepart] = @NewHeureDepart " _
            & "    ,[HeureFin] = @NewHeureFin " _
            & "WHERE " _
            & "     [IDCommissionKMLivreur] = @OldIDCommissionKMLivreur " _
            & ""
        Dim updateCommand As New SqlCommand(updateStatement, connection)
        updateCommand.CommandType = CommandType.Text
        updateCommand.Parameters.AddWithValue("@NewOrganisationID", IIf(clsCommissionKMLivreurNew.OrganisationID.HasValue, clsCommissionKMLivreurNew.OrganisationID, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewOrganisation", IIf(Not IsNothing(clsCommissionKMLivreurNew.Organisation), clsCommissionKMLivreurNew.Organisation, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewKMFrom", IIf(clsCommissionKMLivreurNew.KMFrom.HasValue, clsCommissionKMLivreurNew.KMFrom, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewKMTo", IIf(clsCommissionKMLivreurNew.KMTo.HasValue, clsCommissionKMLivreurNew.KMTo, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewMontant", IIf(clsCommissionKMLivreurNew.Montant.HasValue, clsCommissionKMLivreurNew.Montant, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewExtraChargeParKM", IIf(clsCommissionKMLivreurNew.ExtraChargeParKM.HasValue, clsCommissionKMLivreurNew.ExtraChargeParKM, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewPerKMNumber", IIf(clsCommissionKMLivreurNew.PerKMNumber.HasValue, clsCommissionKMLivreurNew.PerKMNumber, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewMetodeCalculation", IIf(clsCommissionKMLivreurNew.MetodeCalculation.HasValue, clsCommissionKMLivreurNew.MetodeCalculation, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewHeureDepart", IIf(clsCommissionKMLivreurNew.HeureDepart.HasValue, clsCommissionKMLivreurNew.HeureDepart, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewHeureFin", IIf(clsCommissionKMLivreurNew.HeureFin.HasValue, clsCommissionKMLivreurNew.HeureFin, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@OldIDCommissionKMLivreur", clsCommissionKMLivreurOld.IDCommissionKMLivreur)

        Try
            connection.Open()
            Dim count As Integer = updateCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

    Public Function Delete(ByVal clsCommissionKMLivreur As CommissionKMLivreur) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim deleteStatement As String _
            = "DELETE FROM " _
            & "     [CommissionKMLivreur] " _
            & "WHERE " _
            & "     [IDCommissionKMLivreur] = @OldIDCommissionKMLivreur " _
            & ""
        Dim deleteCommand As New SqlCommand(deleteStatement, connection)
        deleteCommand.CommandType = CommandType.Text
        deleteCommand.Parameters.AddWithValue("@OldIDCommissionKMLivreur", clsCommissionKMLivreur.IDCommissionKMLivreur)

        Try
            connection.Open()
            Dim count As Integer = deleteCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

End Class

