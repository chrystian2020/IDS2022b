Imports System.Data.SqlClient

Public Class NomFichierImportData
    Public Function SelectAllNomFichierImport(pIDPeriode As Integer) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDNomFichierImport] " _
            & "    ,[NomFichierImport] " _
            & "FROM " _
            & "     [NomFichierImport] " _
            & " WHERE IDPeriode = " & pIDPeriode & "  ORDER BY IDNomFichierImport DESC"

        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function

    Public Function CheckIfNomFichierImportExist(psNomFichierImport As String) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDNomFichierImport] " _
            & "    ,[NomFichierImport] " _
            & "    ,[IDPeriode] " _
            & "FROM " _
            & "     [NomFichierImport] " _
            & " WHERE NomFichierImport= '" & psNomFichierImport.ToString.Trim & "'"
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function

    Public Function SelectAllByDate(pdDateFrom As DateTime, pdDateTo As DateTime) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDNomFichierImport] " _
            & "    ,[NomFichierImport] " _
            & "    ,[IDPeriode] " _
            & "FROM " _
            & "     [NomFichierImport] " _
            & " WHERE (DateFrom  >= '" & pdDateFrom & "' AND DateTo <= '" & pdDateTo & "') AND IDPeriode = " & go_Globals.IDPeriode
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function
    Public Function SelectAll() As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDNomFichierImport] " _
            & "    ,[NomFichierImport] " _
            & "    ,[IDPeriode] " _
            & "FROM " _
            & "     [NomFichierImport] " _
            & ""

        If Not IsNothing(go_Globals.IDPeriode) Then
            selectStatement = selectStatement & " AND IDPeriode = " & go_Globals.IDPeriode
        End If


        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function

    Public Function Select_Record(ByVal clsNomFichierImportPara As NomFichierImport) As NomFichierImport
        Dim clsNomFichierImport As New NomFichierImport
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDNomFichierImport] " _
            & "    ,[NomFichierImport] " _
            & "    ,[IDPeriode] " _
            & "    ,[DateFrom] " _
            & "    ,[DateTo] " _
            & "FROM " _
            & "     [NomFichierImport] " _
            & "WHERE " _
            & "     [IDNomFichierImport] = @IDNomFichierImport " _
            & ""
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        selectCommand.Parameters.AddWithValue("@IDNomFichierImport", clsNomFichierImportPara.IDNomFichierImport)
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader(CommandBehavior.SingleRow)
            If reader.Read Then
                With clsNomFichierImport
                    .IDNomFichierImport = System.Convert.ToInt32(reader("IDNomFichierImport"))
                    .NomFichierImport = If(IsDBNull(reader("NomFichierImport")), Nothing, reader("NomFichierImport").ToString)
                    .IDPeriode = If(IsDBNull(reader("IDPeriode")), Nothing, CType(reader("IDPeriode"), Int32?))
                    .DateFrom = If(IsDBNull(reader("DateFrom")), Nothing, CType(reader("DateFrom"), DateTime?))
                    .DateTo = If(IsDBNull(reader("DateTo")), Nothing, CType(reader("DateTo"), DateTime?))
                End With
            Else
                clsNomFichierImport = Nothing
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return clsNomFichierImport
    End Function

    Public Function Add(ByVal clsNomFichierImport As NomFichierImport) As Integer
        Dim connection As SqlConnection = DB.GetConnection
        Dim insertStatement As String _
            = "INSERT " _
            & "     [NomFichierImport] " _
            & "     ( " _
            & "     [NomFichierImport] " _
            & "    ,[IDPeriode] " _
            & "    ,[DateFrom] " _
            & "    ,[DateTo] " _
            & "     ) " _
            & "VALUES " _
            & "     ( " _
            & "     @NomFichierImport " _
            & "    ,@IDPeriode " _
            & "    , @DateFrom " _
            & "    ,@DateTo " _
            & "     ) ;SELECT SCOPE_IDENTITY()" _
            & ""
        Dim insertCommand As New SqlCommand(insertStatement, connection)
        insertCommand.CommandType = CommandType.Text
        insertCommand.Parameters.AddWithValue("@NomFichierImport", IIf(Not IsNothing(clsNomFichierImport.NomFichierImport), clsNomFichierImport.NomFichierImport, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@IDPeriode", IIf(clsNomFichierImport.IDPeriode.HasValue, clsNomFichierImport.IDPeriode, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@DateFrom", IIf(clsNomFichierImport.DateFrom.HasValue, clsNomFichierImport.DateFrom, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@DateTo", IIf(clsNomFichierImport.DateTo.HasValue, clsNomFichierImport.DateTo, DBNull.Value))
        Try
            connection.Open()
            Dim count As Integer = insertCommand.ExecuteScalar()
            If count > 0 Then
                Return count
            Else
                Return 0
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

    Public Function Update(ByVal clsNomFichierImportOld As NomFichierImport,
           ByVal clsNomFichierImportNew As NomFichierImport) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim updateStatement As String _
            = "UPDATE " _
            & "     [NomFichierImport] " _
            & "SET " _
            & "     [NomFichierImport] = @NewNomFichierImport " _
            & "    ,[IDPeriode] = @NewIDPeriode " _
            & "WHERE " _
            & "     [IDNomFichierImport] = @OldIDNomFichierImport " _
            & ""
        Dim updateCommand As New SqlCommand(updateStatement, connection)
        updateCommand.CommandType = CommandType.Text
        updateCommand.Parameters.AddWithValue("@NewNomFichierImport", IIf(Not IsNothing(clsNomFichierImportNew.NomFichierImport), clsNomFichierImportNew.NomFichierImport, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewIDPeriode", IIf(clsNomFichierImportNew.IDPeriode.HasValue, clsNomFichierImportNew.IDPeriode, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@OldIDNomFichierImport", clsNomFichierImportOld.IDNomFichierImport)

        Try
            connection.Open()
            Dim count As Integer = updateCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

    Public Function Delete(ByVal clsNomFichierImport As NomFichierImport) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim deleteStatement As String _
            = "DELETE FROM " _
            & "     [NomFichierImport] " _
            & "WHERE " _
            & "     [IDNomFichierImport] = @OldIDNomFichierImport " _
            & ""
        Dim deleteCommand As New SqlCommand(deleteStatement, connection)
        deleteCommand.CommandType = CommandType.Text
        deleteCommand.Parameters.AddWithValue("@OldIDNomFichierImport", clsNomFichierImport.IDNomFichierImport)
        Try
            connection.Open()
            Dim count As Integer = deleteCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

End Class
 
