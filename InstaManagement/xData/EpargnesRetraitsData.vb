Imports System.Data.SqlClient

Public Class EpargnesRetraitsData

    Public Function SelectAllByLivreurIDPeriode(pIDLivreurID As Integer, pIDPeriode As Integer) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDRetrait] " _
            & "    ,[IDEpargne] " _
            & "    ,[IDLivreur] " _
            & "    ,[Livreur] " _
            & "    ,[DateRetrait] " _
            & "    ,[Retrait] " _
            & "    ,[Periode] " _
            & "FROM " _
            & "     [EpargnesRetraits] " _
            & " WHERE IDLivreur =" & pIDLivreurID & " AND Periode =" & pIDPeriode

        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function
    Public Function SelectAllByID(pIDEpargne As Integer) As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDRetrait] " _
            & "    ,[IDEpargne] " _
            & "    ,[IDLivreur] " _
            & "    ,[Livreur] " _
            & "    ,[DateRetrait] " _
            & "    ,[Retrait] " _
            & "    ,[Periode] " _
            & "FROM " _
            & "     [EpargnesRetraits] " _
            & " WHERE IDEpargne =" & pIDEpargne

        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function

    Public Function SelectAll() As DataTable
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDRetrait] " _
            & "    ,[IDEpargne] " _
            & "    ,[IDLivreur] " _
            & "    ,[Livreur] " _
            & "    ,[DateRetrait] " _
            & "    ,[Retrait] " _
            & "    ,[Periode] " _
            & "FROM " _
            & "     [EpargnesRetraits] " _
            & ""
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        Dim dt As New DataTable
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader()
            If reader.HasRows Then
                dt.Load(reader)
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return dt
    End Function

    Public Function Select_Record(ByVal clsEpargnesRetraitsPara As EpargnesRetraits) As EpargnesRetraits
        Dim clsEpargnesRetraits As New EpargnesRetraits
        Dim connection As SqlConnection = DB.GetConnection
        Dim selectStatement As String _
            = "SELECT " _
            & "     [IDRetrait] " _
            & "    ,[IDEpargne] " _
            & "    ,[IDLivreur] " _
            & "    ,[Livreur] " _
            & "    ,[DateRetrait] " _
            & "    ,[Retrait] " _
            & "    ,[Periode] " _
            & "FROM " _
            & "     [EpargnesRetraits] " _
            & "WHERE " _
            & "     [IDRetrait] = @IDRetrait " _
            & ""
        Dim selectCommand As New SqlCommand(selectStatement, connection)
        selectCommand.CommandType = CommandType.Text
        selectCommand.Parameters.AddWithValue("@IDRetrait", clsEpargnesRetraitsPara.IDRetrait)
        Try
            connection.Open()
            Dim reader As SqlDataReader = selectCommand.ExecuteReader(CommandBehavior.SingleRow)
            If reader.Read Then
                With clsEpargnesRetraits
                    .IDRetrait = System.Convert.ToInt32(reader("IDRetrait"))
                    .IDEpargne = System.Convert.ToInt32(reader("IDEpargne"))
                    .IDLivreur = If(IsDBNull(reader("IDLivreur")), Nothing, CType(reader("IDLivreur"), Int32?))
                    .Livreur = If(IsDBNull(reader("Livreur")), Nothing, reader("Livreur").ToString)
                    .DateRetrait = If(IsDBNull(reader("DateRetrait")), Nothing, CType(reader("DateRetrait"), DateTime?))
                    .Retrait = If(IsDBNull(reader("Retrait")), Nothing, CType(reader("Retrait"), Decimal?))
                    .Periode = System.Convert.ToInt32(reader("Periode"))
                End With
            Else
                clsEpargnesRetraits = Nothing
            End If
            reader.Close()
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return clsEpargnesRetraits
    End Function

    Public Function Add(ByVal clsEpargnesRetraits As EpargnesRetraits) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim insertStatement As String _
            = "INSERT " _
            & "     [EpargnesRetraits] " _
            & "     ( " _
            & "     [IDEpargne] " _
            & "    ,[IDLivreur] " _
            & "    ,[Livreur] " _
            & "    ,[DateRetrait] " _
            & "    ,[Retrait] " _
            & "    ,[Periode] " _
            & "     ) " _
            & "VALUES " _
            & "     ( " _
            & "     @IDEpargne " _
            & "    ,@IDLivreur " _
            & "    ,@Livreur " _
            & "    ,@DateRetrait " _
            & "    ,@Retrait " _
            & "    ,@Periode " _
            & "     ) " _
            & ""
        Dim insertCommand As New SqlCommand(insertStatement, connection)
        insertCommand.CommandType = CommandType.Text
        insertCommand.Parameters.AddWithValue("@IDEpargne", clsEpargnesRetraits.IDEpargne)
        insertCommand.Parameters.AddWithValue("@IDLivreur", IIf(clsEpargnesRetraits.IDLivreur.HasValue, clsEpargnesRetraits.IDLivreur, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Livreur", IIf(Not IsNothing(clsEpargnesRetraits.Livreur), clsEpargnesRetraits.Livreur, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@DateRetrait", IIf(clsEpargnesRetraits.DateRetrait.HasValue, clsEpargnesRetraits.DateRetrait, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Retrait", IIf(clsEpargnesRetraits.Retrait.HasValue, clsEpargnesRetraits.Retrait, DBNull.Value))
        insertCommand.Parameters.AddWithValue("@Periode", clsEpargnesRetraits.Periode)
        Try
            connection.Open()
            Dim count As Integer = insertCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

    Public Function Update(ByVal clsEpargnesRetraitsOld As EpargnesRetraits,
           ByVal clsEpargnesRetraitsNew As EpargnesRetraits) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim updateStatement As String _
            = "UPDATE " _
            & "     [EpargnesRetraits] " _
            & "SET " _
            & "     [IDEpargne] = @NewIDEpargne " _
            & "    ,[IDLivreur] = @NewIDLivreur " _
            & "    ,[Livreur] = @NewLivreur " _
            & "    ,[DateRetrait] = @NewDateRetrait " _
            & "    ,[Retrait] = @NewRetrait " _
            & "    ,[Periode] = @NewPeriode " _
            & "WHERE " _
            & "     [IDRetrait] = @OldIDRetrait " _
            & ""
        Dim updateCommand As New SqlCommand(updateStatement, connection)
        updateCommand.CommandType = CommandType.Text
        updateCommand.Parameters.AddWithValue("@NewIDEpargne", clsEpargnesRetraitsNew.IDEpargne)
        updateCommand.Parameters.AddWithValue("@NewIDLivreur", IIf(clsEpargnesRetraitsNew.IDLivreur.HasValue, clsEpargnesRetraitsNew.IDLivreur, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewLivreur", IIf(Not IsNothing(clsEpargnesRetraitsNew.Livreur), clsEpargnesRetraitsNew.Livreur, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewDateRetrait", IIf(clsEpargnesRetraitsNew.DateRetrait.HasValue, clsEpargnesRetraitsNew.DateRetrait, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewRetrait", IIf(clsEpargnesRetraitsNew.Retrait.HasValue, clsEpargnesRetraitsNew.Retrait, DBNull.Value))
        updateCommand.Parameters.AddWithValue("@NewPeriode", clsEpargnesRetraitsNew.Periode)
        updateCommand.Parameters.AddWithValue("@OldIDRetrait", clsEpargnesRetraitsOld.IDRetrait)

        Try
            connection.Open()
            Dim count As Integer = updateCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

    Public Function Delete(ByVal clsEpargnesRetraits As EpargnesRetraits) As Boolean
        Dim connection As SqlConnection = DB.GetConnection
        Dim deleteStatement As String _
            = "DELETE FROM " _
            & "     [EpargnesRetraits] " _
            & "WHERE " _
            & "     [IDRetrait] = @OldIDRetrait " _
            & ""
        Dim deleteCommand As New SqlCommand(deleteStatement, connection)
        deleteCommand.CommandType = CommandType.Text
        deleteCommand.Parameters.AddWithValue("@OldIDRetrait", clsEpargnesRetraits.IDRetrait)

        Try
            connection.Open()
            Dim count As Integer = deleteCommand.ExecuteNonQuery()
            If count > 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As SqlException
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error")
        Finally
            connection.Close()
        End Try
        Return True
    End Function

End Class

